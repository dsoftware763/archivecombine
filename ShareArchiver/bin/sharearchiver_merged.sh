#!/bin/sh
# Startup script for sharearchiver merged app
#
# chkconfig: 2345 95 05
#   following line is for Redhat chkconfig runlevel & sequencing
# chkconfig: 345 95 05
#   following lines are for SuSE chkconfig runlevel & sequencing
### BEGIN INIT INFO
# Provides: jboss
# Required-Start: $local_fs $remote_fs $network $syslog
# Required-Stop: $local_fs $remote_fs $network $syslog
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: Start/Stop JBoss AS v7.0.0
### END INIT INFO
#
#source some script files in order to set and export environmental variables
#as well as add the appropriate executables to $PATH
export JAVA_HOME=/opt/java/j2sdk
export JAVA_VM=$JAVA_HOME/bin/java
export PATH=$JAVA_HOME/bin:$PATH

export JBOSS_HOME=/opt/jboss-7.1.1
export PATH=#JBOSS_HOME/bin:$PATH
export MODULEPATH=

function start(){
echo "Starting JBoss 7"
sh /etc/init.d/iptables save
sh /etc/init.d/iptables stop
sh ${JBOSS_HOME}/bin/standalone.sh -b 0.0.0.0 >/dev/null 2>/dev/null &
sleep 10
echo "Starting JBoss 7 complete at ${JBOSS_HOME}"

}
function stop(){
echo "Stopping JBoss 7"
sh ${JBOSS_HOME}/bin/jboss-cli.sh --connect --command=:shutdown
}

function restart(){
stop
# give stuff some time to stop before we restart
sleep 60
# protect against any services that can�t stop before we restart
su -l jboss -c �killall java�
start
}
case "$1" in
start)
# echo "Starting JBoss AS 7.0.0"
# sudo -u jboss sh ${JBOSS_HOME}/bin/standalone.sh -b 0.0.0.0 > /dev/null
start
;;
stop)
# echo �Stopping JBoss AS 7.0.0"
# sudo -u jboss sh ${JBOSS_HOME}/bin/jboss-admin.sh �connect command=:shutdown
stop
;;
restart)
# echo �Restarting JBoss AS 7.0.0?
restart
;;
*)

echo "Usage: /etc/init.d/jboss {start|stop|restart}"
exit 1
;;

esac
exit 0
