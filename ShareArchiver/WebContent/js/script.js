$(document).ready(function(){
	$(".left ul li").click(function(){
		$(".left li").removeClass("active").children().removeClass("active");
		$(this).addClass("active");
	});
	
	$("#header > ul > li > a").each(function(){
		if ( $(this).next("ul").length ) $(this).addClass("open");
	});
	
	$("#header > ul > li > a.open").mouseover(function(){
		$(this).next("ul").toggle();
		$(this).removeClass("open").addClass("close").addClass("active");
	});
	
	$("#header > ul ul").mouseleave(function(){
		$(this).hide();
		$(this).prev("a.close").addClass("open").removeClass("close").removeClass("active");
	});
	
	$(".stylized tbody tr:odd").addClass("odd");	
	$(".search-result li:odd").addClass("odd");	
});



 function showDetailsPanel(){
	document.getElementById("explorerGrid:searchUpperDiv").style.height='136px'; //175px
	
	
	document.getElementById("explorerGrid:iconDiv").style.display='none';
    document.getElementById("explorerGrid:detailsDiv").style.display='block';
  
  }
 
 function hideDetailsPanel(){
	 document.getElementById("explorerGrid:searchUpperDiv").style.height='448px';//175px
	 
	 
	 document.getElementById("explorerGrid:iconDiv").style.display='block';
	 document.getElementById("explorerGrid:detailsDiv").style.display='none';
	 
	 
 }
 
 function highlightRow(row){	
		
	 if(row.style.backgroundColor=="" || row.style.backgroundColor=='rgb(255, 255, 255)' ){		 
		 row.style.backgroundColor='#f1f1f1';		
	 }
	
 }
 
 function lowlightRow(row){
	 
      if(row.style.backgroundColor!="rgb(252, 211, 99)"){
    	 row.style.backgroundColor='#ffffff';
	 }

 }
 
 function selectRow(gridID, selectedRow){ 
	var grid=gridID;
	
	var id;
	for(var i=0;i<200;i++){
		id=grid+":"+i;
		
		if(document.getElementById(id)!=null){
			document.getElementById(id).style.backgroundColor='#ffffff';			
		}
	}

	document.getElementById(grid+":"+selectedRow).style.backgroundColor='#fcd363';
 }

 function selectVersionRow(gridID, selectedRow){
//	 alert("called selected " +gridID+":"+selectedRow) ;
	var grid=gridID;
	
	var id;
	for(var i=0;i<200;i++){
		id=grid+":"+i;
		
		if(document.getElementById(id)!=null){
			document.getElementById(id).style.backgroundColor='#ffffff';			
		}
	}
	
	document.getElementById(grid+":"+selectedRow).style.backgroundColor='#fcd363';
	//deselect All other rows
	var grid="explorerGrid:searchGrid";
	
	var id;
	for(var i=0;i<200;i++){
		id=grid+":"+i;
		
		if(document.getElementById(id)!=null){				
			document.getElementById(id).style.backgroundColor='#ffffff';			
		}
	}
 }
 
 function deSelectRow(gridID, selectedRow){ 
	 //alert("called " + gridID);
		var grid=gridID;
		
		var id;
		for(var i=0;i<200;i++){
			id=grid+":"+i;
			
			if(document.getElementById(id)!=null){				
				document.getElementById(id).style.backgroundColor='#ffffff';			
			}
		}
		
//		document.getElementById(grid+":"+selectedRow).style.backgroundColor='#fcd363';
	 } 
 
 //----------------for job status page job re-run
//-------------------------------------------------------------------------- toggleRunNowTableFunc()
 function toggleRunNowTableFunc(state){	    
	      var hideRow='HIDE';
	      var showRow='SHOW';
		
	      if( state == hideRow ){
	         document.getElementById("divRunNow").style.display = 'none';
	         document.getElementById("divReRunLabel").style.display = 'none';
	      }else{
	         document.getElementById("divRunNow").style.display = 'block';
	         document.getElementById("divReRunLabel").style.display = 'block';
	      }
	    }
 //-------------------------------------------------------------------------- toggleRunNowFunc()
 function toggleRunNowFunc(state) {

     //var hideRow='HIDE';
     //var showRow='SHOW';
     
     var hideRow='true';
     var showRow='false';
     if( state != hideRow ){
       document.getElementById("execTimeStartDiv").style.display = 'none';
     }else{
       document.getElementById("execTimeStartDiv").style.display = 'block';
     }
 }
 
 //---------------------------for export
 
 function addRemoveSelectedPath(){
//	 alert("hi ..... addRemoveSelectedPath");
	 
	 var array = document.getElementsByTagName("input");
	 
	 var path ="";	 
	 for(var i = 0; i < array.length; i++)
	 {	 

	    if(array[i].type == "checkbox")
	    {
	    	

	       //if(array[i].className == YOUR_CLASS_NAME)
	    	if(array[i].checked==true)
	        {
	    		var tempPath = array[i].id+"";
		    	//alert(tempPath);
		    	//alert(tempPath.lastIndexOf(":"));
	    		if(tempPath){
	    			tempPath = tempPath.substring(0, tempPath.lastIndexOf(":")+1)+"nodePath";
	    		
		    	
		    	//alert("after sub: "+tempPath);
	         //array[i].checked = true;		    	
	    		path = path + encodeURIComponent(document.getElementById(tempPath).value) +"|";
	    		}
	    		//alert("value = " + document.getElementById(tempPath).value);
	        }
	    }
	 }
	 var exportNodePath = document.getElementById("explorerForm:exportNodePath");
	 exportNodePath.value=path;
		//alert("Path: " + path);	 
 }


 function addRemoveSelectedPathSearch(){
	 //alert("hi ..... there");
	 
	 var array = document.getElementsByTagName("input");
	 var exportNodePath = document.getElementById("searchForm:exportNodePath");	 
	 
	 var path ="";	 
	 path=exportNodePath.value;
	 //alert("b4 : "+path);
	 for(var i = 0; i < array.length; i++)
	 {	 		 

	    if(array[i].type == "checkbox" && array[i].id.indexOf("searchForm:searchResults", 0) != -1)
	    {
	    	
	       //if(array[i].className == YOUR_CLASS_NAME)
	    	if(array[i].checked==true)
	        {
	    		var tempPath = array[i].id+"";
	    		tempPath = tempPath.substring(0, tempPath.lastIndexOf(":")+1)+"nodePath";

		    	var tempValue = encodeURIComponent(document.getElementById(tempPath).value);
		    	
		    //	alert("index="+path.indexOf(tempValue, 0) + " of " + tempValue);
		    	//add the checked value ONY if not added
		    	if(path.indexOf(tempValue, 0)==-1){
		    		path = path + encodeURIComponent(document.getElementById(tempPath).value) +"|";
		    	}
	    		
	    		//alert("value = " + document.getElementById(tempPath).value);
	        }else if(array[i].checked==false){
	        	var tempPath = array[i].id+"";
		    	tempPath = tempPath.substring(0, tempPath.lastIndexOf(":")+1)+"nodePath";
		    	
		    	var tempValue = encodeURIComponent(document.getElementById(tempPath).value);
		    	//alert("index="+path.indexOf(tempValue, 0) + " of " + tempValue);
		    	if(path.indexOf(tempValue, 0)>-1){
		    		//alert("removing: " + tempValue);
		    		path=path.replace(tempValue+"|", ""); //remove the unchecked value if added
		    	}
		    	//uncheck the select All checkBox
		    	 var selectAll = document.getElementById("searchForm:selectAll");
		    	 selectAll.checked=false;		    	 
	        }
	    }
	 }
	 //var exportNodePath = document.getElementById("searchForm:exportNodePath");
	 exportNodePath.value=path;
	//	alert("Path: " + path);	 
 }
 
 function selectAll(){
	
	 var selectAll = document.getElementById("searchForm:selectAll");
	 var exportNodePath = document.getElementById("searchForm:exportNodePath");
	 if(selectAll==null || exportNodePath==null){
		 return;
	 }
	 if(selectAll.checked==true){
		 var array = document.getElementsByTagName("input");
		 for(var i = 0; i < array.length; i++)
		 {
			 if(array[i].type == "checkbox" && array[i].id.indexOf("searchForm:searchResults", 0) != -1){
				 array[i].checked = true;
			 }
		 }		 	 
		 exportNodePath.value="ALL";
	 }else{
		 var array = document.getElementsByTagName("input");
		 for(var i = 0; i < array.length; i++)
		 {
			 if(array[i].type == "checkbox" && array[i].id.indexOf("searchForm:searchResults", 0) != -1){
				 array[i].checked = false;
			 }
		 }
		 exportNodePath.value="";
	 }
 }
 
 //---------------------------for SP archive job
 
 function addRemoveSelectedSPLib(){
//	 alert("hi ..... addRemoveSelectedLib");
	 
	 var array = document.getElementsByTagName("input");
	 
	 var path ="";	 
	 for(var i = 0; i < array.length; i++)
	 {	 

	    if(array[i].type == "checkbox")
	    {
	    	

	       //if(array[i].className == YOUR_CLASS_NAME)
	    	//alert(array[i].checked);
	    	if(array[i].checked==true)
	        {	    		
	    		var tempPath = array[i].id+"";	    		
//		    	alert(tempPath +", " + tempPath.indexOf("tree:", 0));
		    	
		    	
		    				    		
//		    	alert(tempPath.lastIndexOf(":"));
		    	if(tempPath.indexOf("tree:", 0)!=-1)
		    	{
		    		var tempIdPath = tempPath;
		    		tempPath = tempPath.substring(0, tempPath.lastIndexOf(":")+1)+"nodePath";
		    		tempIdPath = tempPath.substring(0, tempPath.lastIndexOf(":")+1)+"nodeGuid";
			     //	alert("after sub: "+tempPath + ", " + tempIdPath);
		         //array[i].checked = true;		    	
		    		path = path + encodeURIComponent(document.getElementById(tempPath).value) + ", " + document.getElementById(tempIdPath).value +"|";
//		    		alert("value = " + document.getElementById(tempPath).value + ", " + document.getElementById(tempIdPath).value);
		    	}
	        }
	    	if(array[i].checked==false){
	        	var tempPath = array[i].id+"";
	        	if(tempPath.indexOf("tree:", 0)!=-1)
		    	{
	        		//un check the select all checkbox
			    	 var selectAll = document.getElementById("selectAll");
			    	 if(selectAll!=null){
			    		 selectAll.checked=false;
			    	 }
		    	}
	        }
	    }
	 }
	 var exportNodePath = document.getElementById("exportNodePath");
	 exportNodePath.value=path;
//		alert("Path: " + path);	 
 }
 
 //------------------------to change font color on stub page
 function changeFont(){
	 var fieldValue = document.getElementById("stubAccess:hiddenPercent").value;
	 var progressLabel =  document.getElementById("stubAccess:progressLabel");
//	 alert(document.getElementById("stubAccess:hiddenPercent").value);
	 if(fieldValue){
		 fieldValue = parseInt(fieldValue);
		 if(fieldValue==100){
//			 alert("its 100");
			 progressLabel.style.color="green";
			 
		 }
//		 alert(fieldValue);
	 }
	 //alert(document.getElementById('stubAccess:hiddenPercent').value);
 }
 
 //--------------------for a rich component
 function closePopup(){
//	 #{rich:component("popupExport")}.hide(); return false;
 }
 
 function loadZeroClipboard(){
//	 alert("loading clipbooard");
	 var client = new ZeroClipboard( document.getElementById("click-to-copy"), {
		  moviePath: "js/ZeroClipboard.swf"
		} );

		client.on( "load", function(client) {
		  // alert( "movie is loaded" );

		  client.on( "complete", function(client, args) {
		    // `this` is the element that was clicked
		    //this.style.display = "none";
//		    alert("URL copied to clipboard");
			  document.getElementById("copy-message").innerHTML="Copied to clipboard";
		  } );
		} );
 }
 
 function loadZeroClipboardSearch(){
//	 alert("loading clipbooard");

		 var client = new ZeroClipboard( document.getElementById("click-to-copy-search"), {
			  moviePath: "js/ZeroClipboard.swf"
			} );

			client.on( "load", function(client) {
			  // alert( "movie is loaded" );

			  client.on( "complete", function(client, args) {
			    // `this` is the element that was clicked
			    //this.style.display = "none";
//			    alert("URL copied to clipboard");
				document.getElementById("copy-message-search").innerHTML="Copied to clipboard";
			  } );
			} );
 }
 
 function loadZeroClipboardSearchId(){
//	 alert("loading clipbooard");

		 var client = new ZeroClipboard( document.getElementById("click-to-copy-search-id"), {
			  moviePath: "js/ZeroClipboard.swf"
			} );

			client.on( "load", function(client) {
			  // alert( "movie is loaded" );

			  client.on( "complete", function(client, args) {
			    // `this` is the element that was clicked
			    //this.style.display = "none";
//			    alert("URL copied to clipboard");
				  document.getElementById("copy-message-search-id").innerHTML="Copied to clipboard";
			  } );
			} );
 }
