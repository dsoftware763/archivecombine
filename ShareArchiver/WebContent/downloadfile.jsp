<%@page import="com.virtualcode.util.VirtualcodeUtil"%>
<%@page import="com.virtualcode.vo.SystemCodeType"%>
<%@page import="com.virtualcode.common.VCSConstants"%>
<%@page errorPage="ErrorLink.faces" %>

<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="com.virtualcode.services.ServiceManager"%>
<%@page import="com.virtualcode.repository.FileExplorerService"%>

<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.BufferedInputStream"%>
<%@page import="java.net.MalformedURLException"%>
<%@page import="java.io.IOException"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.net.URL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
    
    <style>
    html, body, div, h1, h2, h3, h4, h5, h6, ul, ol, p, form, fieldset, table, th, td, input { 
		margin: 0; 
		padding: 0; 
		color: #ff1413;
		font-family: verdana,helvetica,arial,sans-serif;
		font-size:13px;
		font-stretch:normal;
		line-height:20px;
		text-align: center;
		
	}
	
	
    
    </style>

<%
            org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger("com.virtualcode.downloadfile");
            String nodeID = request.getParameter("nodeID");
            String version = request.getParameter("version");
            log.info("before decode nodeID -->"+nodeID);
            boolean isArchive =  true;
            com.virtualcode.util.ResourcesUtil ru = com.virtualcode.util.ResourcesUtil.getResourcesUtil();

            String fileName = "";
			String username = (String)session.getAttribute(VCSConstants.SESSION_USER_NAME);
			String authType = (String)session.getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE);
			//added for SP web part to download documents after authorization
			if(username==null || username.trim().equals("")){
				username = request.getRemoteUser();
				if(username!=null && !(username.trim().equals(""))){
					username = username.substring(username.indexOf("\\")+1);
					authType="ad";
				}
			}
			
			System.out.println("username:" + username);
            try {
            	if(nodeID.startsWith("na-")){
            		VirtualcodeUtil util = VirtualcodeUtil.getInstance();
            		String str = util.getURL(nodeID.substring(3)); 
            		if(str!=null && str.length()>0)            		
            			nodeID=com.virtualcode.util.StringUtils.decodeUrlAdvanced(str.substring(3));
            		else
            			nodeID=com.virtualcode.util.StringUtils.decodeUrlAdvanced(nodeID.substring(3));
            		System.out.println(nodeID);
            		nodeID = nodeID.substring(2).replaceAll("\\\\", "/");
            		isArchive =  false;
            	}else if(nodeID.startsWith("nar-")){
            		VirtualcodeUtil util = VirtualcodeUtil.getInstance();
//             		String str = util.getURL(nodeID.substring(3)); 
//             		if(str!=null && str.length()>0)            		
//             			nodeID=com.virtualcode.util.StringUtils.decodeUrlAdvanced(str.substring(3));
//             		else
            			nodeID=com.virtualcode.util.StringUtils.decodeUrlAdvanced(nodeID.substring(4));
            		System.out.println(nodeID);
//             		nodeID = nodeID.substring(2).replaceAll("\\\\", "/");
            		isArchive =  false;
            	}
            	else{
                 	nodeID=com.virtualcode.util.StringUtils.decodeUrl(nodeID);
                 	isArchive = true;
                }
                //nodeID  =   "archive/FS/vc6.network.vcsl/Everyone/try.esp";
                log.info("session username -->"+username);
                log.info("session authtype -->"+authType);
                log.info("after decode nodeID -->"+nodeID);
                
                fileName = nodeID.substring(nodeID.lastIndexOf("/") + 1);
                log.debug("Filename Before decode : " + fileName);
                fileName =  new String(fileName.getBytes("UTF-8"), "ISO-8859-1");
                if(version!= null && version.equals("1")){
//            	String tempName = fileName;
     			if(fileName.indexOf("-")!=-1){
     				fileName = fileName.substring(0, fileName.lastIndexOf("-")).trim() + fileName.substring(fileName.lastIndexOf("-")+4).trim();
     			}
             }
//                 System.out.println(fileName);
                log.debug("Filename after decode : " + fileName);
            } catch (Exception ex) {
            	ex.printStackTrace();
                log.error("Unable to get filename from URL, err: " + ex.toString());
                throw new Exception();
            }

          
           InputStream is = null;
           int contentLength =0;
           
            try{
            	  
            	  boolean authenticate = false;
                  WebApplicationContext webAppContext=WebApplicationContextUtils.getWebApplicationContext(getServletContext());				  
                  ServiceManager serviceManager=(ServiceManager)webAppContext.getBean("serviceManager");
                  FileExplorerService explorerService=serviceManager.getFileExplorerService();
                  if(authType!=null && authType.equals("db")){
                        authenticate = true;
                   }else{
                   		authenticate = explorerService.authorizeStub(nodeID, username);
                   }
                  //authenticate = true;
                  if(!authenticate){
                  	//authenticate on smb Path
                  	authenticate = explorerService.authorizeSmbFile(nodeID, username);
                  }
                  if(!authenticate){
                  	out.println("<br>");
                  	out.println(ru.getCodeValue("accessDeniedMessage", SystemCodeType.GENERAL)+"<br>");
                  	
                  	out.println("username ["+ username +"] not authorized for this file.");
                  	//response.sendRedirect("StubAccess.faces");
                  	return;
                  	 //throw new Exception("user not authorized:[" + username +"]");
                  }
                  String spMode = request.getParameter("mode");
                  	
		        	String mode = ru.getCodeValue("stub_access_mode", "General");// 1 =  view, 2 =  download and 3 = restore
               		if((spMode!=null && spMode.equalsIgnoreCase("download")) || (mode!=null && mode.equals("2"))){
	            	   	com.virtualcode.vo.CustomTreeNode node = explorerService.getNodeByUUID(nodeID);
	            	   	if(node==null)
	            			   node = explorerService.getNodeByPath(nodeID);
	            	   
	            	   	if(node!=null){
	            			nodeID = node.getPath();
	            		   	fileName = nodeID.substring(nodeID.lastIndexOf("/") + 1);
	            	   }
            	   }
            	   javax.jcr.Binary binaryStream = null;
                  
	                binaryStream =explorerService.downloadFile(nodeID, false);
	                  
	                   if(binaryStream!=null){
			               is=binaryStream.getStream();
			               contentLength=Math.round(binaryStream.getSize());
			            }else{
			            	out.println("File not available");
			            	response.sendRedirect("ErrorLink.xHtml");			            	
			            	throw new Exception();
			            }
		          
             }catch(Exception ex){
                ex.printStackTrace();
             }
          
         
            try {
            		if(is==null ){out.println("File not available");
            		        response.sendRedirect("ErrorLink.xHtml");
			            	throw new Exception();
			            	
			            }
                   ServletOutputStream outs = response.getOutputStream();
                   String contentType ;//= uCon.getContentType();

                    String mimeType = URLConnection.guessContentTypeFromName(fileName);
                    if (mimeType == null) {
                        if (fileName.endsWith(".doc")) {
                            mimeType = "application/msword";
                        } else if (fileName.endsWith(".xls")) {
                            mimeType = "application/vnd.ms-excel";
                        } else if (fileName.endsWith(".ppt")) {
                            mimeType = "application/mspowerpoint";
                        } else {
                            mimeType = "application/octet-stream";
                        }
                    }
                    //byte[] bytes = IOUtils.toByteArray(is);
                   
                    response.setContentLength(contentLength);
                    response.setContentType(mimeType);
                    response.setHeader("Content-type",mimeType );
                    response.setHeader("Content-Disposition", "attachement;filename=\"" + fileName + "\"");

                    log.debug("contentLength: " + contentLength);
                    log.debug("contentType: " + mimeType);

                   int a = 256;
                    try {
	                    String tempSize	=	ru.getCodeValue("BUFF_SIZE", SystemCodeType.GENERAL);
			            int buffSize	=	8192;//defaultBufferSize
			            if(tempSize!=null && !tempSize.isEmpty()) {
			            	try {
			            		buffSize	=	Integer.parseInt(tempSize);
			            	}catch(Exception ex) {
			            		log.error(" Error to parse BUFF_SIZE: "+tempSize);
			            	}
			            }
			            log.debug(" buffered size is: "+buffSize);
			            
                           byte[] buffer = new byte[buffSize];
				           while ((a = is.read(buffer, 0, buffSize)) != -1) {
				                 outs.write(buffer, 0, a);
				            }
                         
                    } catch (IOException ioe) {
                        log.debug("io excpetion 1 " + ioe.getMessage(),ioe);
                    }

                    try {
                        is.close();
                        outs.flush();
                        log.debug("5555");
                        outs.close();
                    } catch (IOException e) {
                        log.debug("io exception 2 " + e.getMessage(),e);
                    }


            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                log.debug("malformed url exception " + e.getMessage(),e);
                throw new Exception();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                log.debug("io expection 3 " + e.getMessage(),e);
                throw new Exception();
            }



%>