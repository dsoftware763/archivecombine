<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ page import="com.virtualcode.common.VCSConstants"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'index.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
    ShareArchiver. <br>
  </body>
  
  <% //code uncommented
    if(session!=null && session.getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE)!=null){
       String authType=session.getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE).toString();
       
       if(authType.equals("ad")){
           response.sendRedirect("Browse.faces");
       }
       
       else if(authType.equals("db")){
            String role= session.getAttribute(VCSConstants.SESSION_USER_ROLE_NAME).toString();
            if(role.equals(VCSConstants.ROLE_ADMIN)){
               response.sendRedirect("Status.faces");
            }else if(role.equals(VCSConstants.ROLE_PRIVILEGED)){
               response.sendRedirect("Browse.faces");
            }else{
                response.sendRedirect("browse.faces");
            }
       }
       else{
           response.sendRedirect("login.faces");
       }
     
     }else{      
	      response.sendRedirect("Browse.faces");
     } 
     
     
     //response.sendRedirect("Browse.faces");
  
  
   %>
</html>
