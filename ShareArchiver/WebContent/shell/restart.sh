#!/bin/bash

function web_restart()
{
echo '=====================================================' `date` '==========================================================='
echo "stopping tomcat"
sh /opt/tomcat/bin/./catalina.sh stop
#--- Required by RedHat
rm /var/lock/subsys/sharearchiver
echo "stopping tomcat complete"

echo "starting tomcat"
#--- Increase the max number of file handles
#ulimit -n 34096
echo "Starting ShareArchiver..."
#sleep 5
#rm -fdr /opt/tomcat/work
echo "Starting Tomcat..."
cd /opt/tomcat/bin
sh ./catalina.sh start
echo "Tomcat Started"
echo "sleep 3"
#sleep 3
echo "sleep done"
sh /opt/ZuesAgent/bin/testwrapper stop >> /opt/tomcat/logs/ZuesAgent.log 2>&1
/opt/ZuesAgent/bin/testwrapper start >> /opt/tomcat/logs/ZuesAgent.log 2>&1
echo "done."
echo "starting tomcat complete"
}

web_restart | tee -a /opt/tomcat/logs/web_restart.log