/*
 Create Database Schema for sharearchiver
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE  IF NOT EXISTS `sharearchiver`  DEFAULT CHARACTER SET utf8;

USE `sharearchiver`;


DROP TABLE IF EXISTS `agent`;
CREATE TABLE `agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(11) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `login` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `type` varchar(3) NOT NULL,
  `allowed_ips` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `document_category`;
CREATE TABLE `document_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `document_type`;
CREATE TABLE `document_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_category_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `value` (`value`),
  UNIQUE KEY `name` (`name`),
  KEY `FK5E81EFDE39F1FF2E` (`document_category_id`),
  CONSTRAINT `FK5E81EFDE39F1FF2E` FOREIGN KEY (`document_category_id`) REFERENCES `document_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `drive_letters`;
CREATE TABLE `drive_letters` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `drive_letter` varchar(5),
  `share_path` varchar(500) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `restore_username` varchar(20) DEFAULT NULL,
  `restore_password` varchar(20) DEFAULT NULL,
  `restore_domain` varchar(30) DEFAULT NULL,
  `warning_limit` int(11) DEFAULT '50',
  `critical_limit` int(11) DEFAULT '90',
  `analyze_share` BOOL DEFAULT FALSE,
  `trigger_job_on_threshhold` BOOL DEFAULT FALSE,
  `is_file_share` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `error_code`;
CREATE TABLE `error_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `job`;
CREATE TABLE `job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_type` varchar(20) NOT NULL,
  `active` int(11) NOT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `exec_time_start` datetime DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `policy_id` int(11) NOT NULL,
  `sp_site_id` varchar(255) DEFAULT NULL,
  `sp_path` varchar(255) DEFAULT NULL,
  `process_current_published_version` bit(1) NOT NULL,
  `process_legacy_draft_version` bit(1) NOT NULL,
  `process_legacy_published_version` bit(1) NOT NULL,
  `process_archive` bit(1) DEFAULT NULL,
  `process_hidden` bit(1) DEFAULT NULL,
  `process_read_only` bit(1) DEFAULT NULL,
  `ready_to_execute` bit(1) DEFAULT NULL,
  `preserve_file_paths` bit(1) DEFAULT NULL,
  `export_duplicates` bit(1) DEFAULT NULL,
  `active_action_type` varchar(20) DEFAULT NULL,
  `is_share_analysis` tinyint(1) DEFAULT '0',
  `is_threshhold_job` BOOL NOT NULL DEFAULT FALSE,
  `tags` varchar(255) DEFAULT NULL,
  `current_status` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `FK19BBD746CF237` (`agent_id`),
  KEY `FK19BBDF4302607` (`policy_id`),
  CONSTRAINT `FK19BBD746CF237` FOREIGN KEY (`agent_id`) REFERENCES `agent` (`id`),
  CONSTRAINT `FK19BBDF4302607` FOREIGN KEY (`policy_id`) REFERENCES `policy` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE `job` ADD `is_scheduled` BOOL NOT NULL ,
ADD `exec_interval` DOUBLE NOT NULL ;

DROP TABLE IF EXISTS `job_document`;
CREATE TABLE `job_document` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `doc_id` varchar(40) NOT NULL,
  `doc_sp_path` varchar(1000) NOT NULL,
  `last_accessed` datetime NOT NULL,
  `error_code_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `last_modified` datetime NOT NULL,
  `last_modified_by` varchar(250) NOT NULL,
  `schedular_insert_date` datetime NOT NULL,
  `size` int(11) NOT NULL,
  `status` varchar(30) NOT NULL,
  `title` varchar(250) NOT NULL,
  `version` varchar(10) DEFAULT NULL,
  `created_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7B974B5D7AED60DE` (`error_code_id`),
  KEY `FK7B974B5D4257962D` (`job_id`),
  CONSTRAINT `FK7B974B5D4257962D` FOREIGN KEY (`job_id`) REFERENCES `job` (`id`),
  CONSTRAINT `FK7B974B5D7AED60DE` FOREIGN KEY (`error_code_id`) REFERENCES `error_code` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `job_sp_doc_lib`;
CREATE TABLE `job_sp_doc_lib` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `guid` varchar(255) DEFAULT NULL,
  `name` varchar(1000) NOT NULL,
  `type` varchar(4) DEFAULT NULL,
  `destination_path` varchar(1000) DEFAULT NULL,
  `recursive` bit(1) DEFAULT NULL,
  `site` varchar(255) DEFAULT NULL,
  `drive_path` varchar(1000) DEFAULT NULL,
  `content_types` varchar(1000) DEFAULT NULL,
  `tags` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKB7FBF4DE4257962D` (`job_id`),
  CONSTRAINT `FKB7FBF4DE4257962D` FOREIGN KEY (`job_id`) REFERENCES `job` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `job_sp_doc_lib_excluded_path`;
CREATE TABLE `job_sp_doc_lib_excluded_path` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(1000) NOT NULL,
  `sp_doc_lib_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKC21E37D9B917C635` (`sp_doc_lib_id`),
  CONSTRAINT `FKC21E37D9B917C635` FOREIGN KEY (`sp_doc_lib_id`) REFERENCES `job_sp_doc_lib` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `job_statistics`;
CREATE TABLE `job_statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `execution_id` char(16) NOT NULL,
  `job_id` int(11) NOT NULL,
  `job_end_time` datetime NOT NULL,
  `job_start_time` datetime NOT NULL,
  `skipped_processfile_skipped_hiddenlected` bigint(20) NOT NULL DEFAULT '0',
  `skipped_processfile_skipped_isdir` bigint(20) NOT NULL DEFAULT '0',
  `skipped_processfile_skipped_mismatch_age` bigint(20) NOT NULL DEFAULT '0',
  `skipped_processfile_skipped_mismatch_extension` bigint(20) NOT NULL DEFAULT '0',
  `skipped_processfile_skipped_mismatch_lastaccesstime` bigint(20) NOT NULL DEFAULT '0',
  `skipped_processfile_skipped_mismatch_lastmodifiedtime` bigint(20) NOT NULL DEFAULT '0',
  `skipped_processfile_skipped_read_only` bigint(20) NOT NULL DEFAULT '0',
  `skipped_processfile_skipped_symbollink` bigint(20) NOT NULL DEFAULT '0',
  `skipped_processfile_skipped_temporary` bigint(20) NOT NULL DEFAULT '0',
  `skipped_processfile_skipped_too_large` bigint(20) NOT NULL DEFAULT '0',
  `total_archived` bigint(20) NOT NULL DEFAULT '0',
  `already_archived` BIGINT( 200 ) NOT NULL DEFAULT '0',
  `total_duplicated` bigint(20) NOT NULL DEFAULT '0',
  `total_evaluated` bigint(20) NOT NULL DEFAULT '0',
  `total_failed_archiving` bigint(20) NOT NULL DEFAULT '0',
  `total_failed_deduplication` bigint(20) NOT NULL DEFAULT '0',
  `total_failed_evaluating` bigint(20) NOT NULL DEFAULT '0',
  `total_failed_stubbing` bigint(20) NOT NULL DEFAULT '0',
  `total_stubbed` bigint(20) NOT NULL DEFAULT '0',
  `archived_volume` bigint(20) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK1DE1A2E54257962D` (`job_id`),
  CONSTRAINT `FK1DE1A2E54257962D` FOREIGN KEY (`job_id`) REFERENCES `job` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `job_status`;
CREATE TABLE `job_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  `error_code_id` int(11) NOT NULL,
  `exec_start_time` datetime NOT NULL,
  `is_current` tinyint(1) NOT NULL,
  `job_id` int(11) NOT NULL,
  `previous_job_status_id` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `execution_id` char(16) DEFAULT NULL,
  `action_by_user` varchar(50) DEFAULT NULL,
  `action_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FKA085F2B47AED60DE` (`error_code_id`),
  KEY `FKA085F2B44257962D` (`job_id`),
  CONSTRAINT `FKA085F2B44257962D` FOREIGN KEY (`job_id`) REFERENCES `job` (`id`),
  CONSTRAINT `FKA085F2B47AED60DE` FOREIGN KEY (`error_code_id`) REFERENCES `error_code` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `monitor`;
CREATE TABLE `monitor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `node_permission_job`;
CREATE TABLE `node_permission_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) DEFAULT NULL,
  `command` varchar(500) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `registerdate` varchar(50) DEFAULT NULL,
  `startdate` varchar(50) DEFAULT NULL,
  `finisheddate` varchar(50) DEFAULT NULL,
  `secureinfo` varchar(100) DEFAULT NULL,
  `sids` varchar(500) DEFAULT NULL,
  `denysids` varchar(500) DEFAULT NULL,
  `sharesids` varchar(500) DEFAULT NULL,
  `denysharesids` varchar(500) DEFAULT NULL,
  `isrecursive` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `policy`;
CREATE TABLE `policy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `document_age` varchar(255) NOT NULL,
  `last_accessed_date` varchar(255) NOT NULL,
  `last_modified_date` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `size_larger_than` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `policy_document_type`;
CREATE TABLE `policy_document_type` (
  `document_type_id` int(11) NOT NULL,
  `policy_id` int(11) NOT NULL,
  PRIMARY KEY (`policy_id`,`document_type_id`),
  KEY `FK6AD36791832F3FAE` (`document_type_id`),
  KEY `FK6AD36791F4302607` (`policy_id`),
  CONSTRAINT `FK6AD36791832F3FAE` FOREIGN KEY (`document_type_id`) REFERENCES `document_type` (`id`),
  CONSTRAINT `FK6AD36791F4302607` FOREIGN KEY (`policy_id`) REFERENCES `policy` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `shiro_role`;
CREATE TABLE `shiro_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `shiro_role_permissions`;
CREATE TABLE `shiro_role_permissions` (
  `shiro_role_id` bigint(20) DEFAULT NULL,
  `permissions_string` varchar(255) DEFAULT NULL,
  KEY `FK389B46C98BA4B1D` (`shiro_role_id`),
  CONSTRAINT `FK389B46C98BA4B1D` FOREIGN KEY (`shiro_role_id`) REFERENCES `shiro_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `shiro_user`;
CREATE TABLE `shiro_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `lock_status` varchar(8) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `last_password_change` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `shiro_user_permissions`;
CREATE TABLE `shiro_user_permissions` (
  `shiro_user_id` bigint(20) DEFAULT NULL,
  `permissions_string` varchar(255) DEFAULT NULL,
  KEY `FK34555A9EADE50EFD` (`shiro_user_id`),
  CONSTRAINT `FK34555A9EADE50EFD` FOREIGN KEY (`shiro_user_id`) REFERENCES `shiro_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `shiro_user_roles`;
CREATE TABLE `shiro_user_roles` (
  `shiro_role_id` bigint(20) NOT NULL,
  `shiro_user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`shiro_user_id`,`shiro_role_id`),
  KEY `FKBA221057ADE50EFD` (`shiro_user_id`),
  KEY `FKBA2210578BA4B1D` (`shiro_role_id`),
  CONSTRAINT `FKBA2210578BA4B1D` FOREIGN KEY (`shiro_role_id`) REFERENCES `shiro_role` (`id`),
  CONSTRAINT `FKBA221057ADE50EFD` FOREIGN KEY (`shiro_user_id`) REFERENCES `shiro_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `smb_message`;
CREATE TABLE `smb_message` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `error` varchar(255) NOT NULL,
  `success` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `system_code`;
CREATE TABLE `system_code` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `codename` VARCHAR(200) NOT NULL,
  `codevalue` VARCHAR(1000) DEFAULT NULL,
  `codetype` VARCHAR(300) DEFAULT NULL,
  `ETD` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `editable` INT(11) DEFAULT NULL,
  `viewable` INT(11) DEFAULT NULL,
  `display_name` VARCHAR(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE_CODE_NAME` (`codename`)
) ENGINE=MYISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `system_info`;
CREATE TABLE `system_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `repo_stats`;
CREATE TABLE `repo_stats` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `repo_path` varchar(200) DEFAULT NULL,
  `total_folders` bigint(200) DEFAULT NULL,
  `total_files` bigint(200) DEFAULT NULL,
  `fs_docs` bigint(200) DEFAULT NULL,
  `sp_docs` bigint(200) DEFAULT NULL,
  `used_space` double DEFAULT NULL,
  `duplicate_data_size` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE `repo_stats` ADD `duplicate_files` BIGINT( 200 ) NOT NULL DEFAULT '0' AFTER `total_files`; 

DROP TABLE IF EXISTS `system_alerts`;
CREATE TABLE `system_alerts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `alert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `alert_type` varchar(50) DEFAULT NULL,
  `alert_message` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `share_analysis_stats` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `share_id` INT(11) NOT NULL COMMENT 'share_mapping id',
  `document_cat_name` VARCHAR(200) NOT NULL COMMENT 'document_category name',
  `total_volume` BIGINT(20) UNSIGNED DEFAULT 0 COMMENT 'accumulated size in mbs',
  `LM_3` BIGINT(20) DEFAULT 0 COMMENT 'accumulated size of files older than 3 months in mbs',
  `LM_6` BIGINT(20) DEFAULT 0 COMMENT 'accumulated size of files older than 6 months in mbs',
  `LM_12` BIGINT(20) DEFAULT 0,
  `LM_24` BIGINT(20) DEFAULT 0,
  `LM_36` BIGINT(20) DEFAULT 0,
  `LA_3` BIGINT(20) DEFAULT 0 COMMENT 'accumulated size of files acccessed more than 3 months in mbs',
  `LA_6` BIGINT(20) DEFAULT 0 COMMENT 'accumulated size of files accessed more than 6 months in mbs',
  `LA_12` BIGINT(20) DEFAULT 0,
  `LA_24` BIGINT(20) DEFAULT 0,
  `LA_36` BIGINT(20) DEFAULT 0,
  `CD_3` BIGINT(20) DEFAULT 0 COMMENT 'accumulated size of files created more than 3 months in mbs',
  `CD_6` BIGINT(20) DEFAULT 0 COMMENT 'accumulated size of files created more than 6 months in mbs',
  `CD_12` BIGINT(20) DEFAULT 0,
  `CD_24` BIGINT(20) DEFAULT 0,
  `CD_36` BIGINT(20) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `FK_share_analysis_stats` (`share_id`),
  CONSTRAINT `FK_share_analysis_stats` FOREIGN KEY (`share_id`) REFERENCES `drive_letters` (`ID`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE `page_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shiro_role_id` bigint(20) DEFAULT NULL,
  `page_uri` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_SHIRO_ROLE_PAGE_RIGHTS` (`shiro_role_id`),
  CONSTRAINT `FK_SHIRO_ROLE_PAGE_RIGHTS` FOREIGN KEY (`shiro_role_id`) REFERENCES `shiro_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `role_features`( 
    `id` INT NOT NULL AUTO_INCREMENT ,
    `name` VARCHAR(200) NOT NULL , 
    `display_name` VARCHAR(400) NULL , 
    `admin_allowed` BOOL DEFAULT FALSE , 
    `priv_allowed` BOOL DEFAULT FALSE,
    `ldap_allowed` BOOL DEFAULT FALSE , 
    PRIMARY KEY (`id`) ) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
ALTER IGNORE TABLE `role_features` ADD CONSTRAINT UNIQUE KEY `UNIQUE_NAME` (`name`);    

CREATE TABLE `files_processed_size` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total_processed_files` bigint(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `link_access_users`;
CREATE TABLE `link_access_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recipient` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `sender` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `link_access_documents`;
CREATE TABLE `link_access_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid_documents` varchar(500) DEFAULT NULL,
  `link_access_users_id` int(11) DEFAULT NULL,
  `shared_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sender` varchar(100) DEFAULT NULL,
  `hash` varchar(100) DEFAULT NULL,
  `identifier` bigint(20) DEFAULT NULL,
  `link_expire_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_LINK_USERS_DOCS` (`link_access_users_id`),
  CONSTRAINT `FK_LINK_USERS_DOCS` FOREIGN KEY (`link_access_users_id`) REFERENCES `link_access_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `data_guardian`;
CREATE TABLE `data_guardian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_guardian_email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ldap_groups`;
CREATE TABLE `ldap_groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(100) DEFAULT NULL,
  `restrict_group` int(1) DEFAULT '0',
  `enable_transcript` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS del_policy;
CREATE TABLE `del_policy` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`name` VARCHAR( 50 ) NOT NULL ,
`modified_date` VARCHAR( 255 ) NOT NULL ,
`access_date` VARCHAR( 255 ) NOT NULL ,
`archival_date` VARCHAR( 255 ) NOT NULL ,
`file_type` VARCHAR( 255 ) NOT NULL ,
`file_size` VARCHAR( 50 ) NOT NULL
) ENGINE = InnoDB ;

DROP TABLE IF EXISTS del_job;
CREATE TABLE `del_job` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`action_type` VARCHAR( 20 ) NOT NULL ,
`name` VARCHAR( 50 ) NOT NULL ,
`del_policy_id` INT NOT NULL,
KEY `FKdelpolicyondeljob` (`del_policy_id`),
CONSTRAINT `FKdelpolicyondeljob` FOREIGN KEY (`del_policy_id`) REFERENCES `del_policy` (`id`)
) ENGINE = InnoDB ;

DROP TABLE IF EXISTS del_job_filedetails;
CREATE TABLE `del_job_filedetails` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`uuid` VARCHAR( 255 ) NOT NULL ,
`status` VARCHAR( 50 ) NOT NULL ,
`description` VARCHAR( 255 ) NOT NULL,
`del_job_id` INT NOT NULL,
KEY `FKdeljobondeljobfiledetails` (`del_job_id`),
CONSTRAINT `FKdeljobondeljobfiledetails` FOREIGN KEY (`del_job_id`) REFERENCES `del_job` (`id`)
) ENGINE = InnoDB ;

/*******************end of create table script*********************/

/*----------------- for insertion of default data in different tables-----------------*/

/*Data for the table `agent` */

insert  into `agent`(`id`,`active`,`description`,`login`,`name`,`password`,`type`,`allowed_ips`)
             values (1,1,'for logging into archive app','bitarch','bitarch','bitarch','non','none'),
			        (2,1,'for repository login','admin','admin','admin','non','none'),
					(6,0,NULL,'sharearchiver','Sharearchiver','sharearchiver','FS','127.0.0.1');

/*Data for the table `document_ccategory` */

insert into `document_category` (`id`, `description`, `name`) values('1','Microsoft Office Documents','Office');
insert into `document_category` (`id`, `description`, `name`) values('2','Pdf files','Adobe PDF');
insert into `document_category` (`id`, `description`, `name`) values('3','Image files','Images');
insert into `document_category` (`id`, `description`, `name`) values('4','Video files','Video');
insert into `document_category` (`id`, `description`, `name`) values('5','Audio files','Audio');
insert into `document_category` (`id`, `description`, `name`) values('6','Other files','Other');
insert into `document_category` (`id`, `description`, `name`) values('7','Common text documents','Text Files');
insert into `document_category` (`id`, `description`, `name`) values('8','flash Files','Flash');
insert into `document_category` (`id`, `description`, `name`) values('9','Data Base Files','Data Base');
insert into `document_category` (`id`, `description`, `name`) values('10','Web files','Web');
insert into `document_category` (`id`, `description`, `name`) values('11','Executable Files','Executable');
insert into `document_category` (`id`, `description`, `name`) values('12','Compressed Files','Compressed');
insert into `document_category` (`id`, `description`, `name`) values('13','Backup Files','Backup ');
insert into `document_category` (`id`, `description`, `name`) values('14','Settings Files','Settings');
insert into `document_category` (`id`, `description`, `name`) values('15','Data Files','Data');
insert into `document_category` (`id`, `description`, `name`) values('16','System Files','System');
insert into `document_category` (`id`, `description`, `name`) values('17','Misc Files','Miscellaneous');
insert into `document_category` (`id`, `description`, `name`) values('18','3D image Files','3D image');

/*Data for the table `document_type` */

insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('1','1','PowerPoint Open XML Presentation','.pptx');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('2','1','Microsoft Word Document','.doc');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('3','1','Microsoft Excel Open XML Spreadsheet','.xlsx');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('4','1','Microsoft Word Open XML Document','.docx');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('5','1','Microsoft PowerPoint ','.ppt');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('6','1','Microsoft Publisher','.pub');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('7','1','Microsoft Excel Spreadsheet','.xls');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('8','1','Microsoft Visio','.vsd');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('9','2','Adobe PDF file','.pdf');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('10','3','JPE','.jpe');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('11','3','Tagged Image File Format','.tiff');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('12','3','JPEG Image','.jpg');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('13','3','Portable Network Graphic','.png');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('14','3','Graphical Interchange Format File','.gif');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('15','3','JPEG','.jpeg');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('16','3','Tagged Image File','.tif');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('17','3','Bitmap','.bmp');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('18','4','MPEG','.mpeg');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('19','4','Windows Media Videos','.wmv');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('20','4','FLV','.flv');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('21','4','FLA','.fla');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('22','4','QuickTime','.mov');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('23','4','MPG','.mpg');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('24','4','AVI','.avi');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('25','5','CD Digital Audio File','.cdda');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('26','5','RKAU Audio','.rka');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('27','5','Lossless Audio File','.la');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('28','5','Audacity Audio File','.au');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('29','5','Windows Media Audio File','.wma');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('30','5','WAVE Audio File','.wav');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('31','5','WavPack Audio File','.wv');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('32','5','Audio Interchange File Format','.aiff');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('33','5','PAC','.pac');

insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('43','7','Plain Text File','*.txt');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('44','8','Flash','*.swf');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('45','8','Thumbnails','*.db');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('46','7','Log File','.log');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('47','7','Outlook Mail Message','.msg');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('48','7','OpenDocument Text Document','.odt');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('49','7','LaTeX Source Document','.tex');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('50','7','WordPerfect Document','.wpd');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('51','7','Microsoft Works Word Processor Document','.wps');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('52','9','Database File','.db');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('53','9','Database-File','.dbf');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('54','9','Microsoft Access Database','.mdb');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('55','9','Program Database','.pdb');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('56','9','Structured Query Language Data File','.sql');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('57','10','Active Server Page','.asp');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('58','10','Active Server Page Extended File','.aspx');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('59','10','Internet Security Certificate','.cer');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('60','10','ColdFusion Markup File','.cfm');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('61','10','Certificate Signing Request File','.csr');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('62','10','JavaScript File','.js');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('63','10','Java Server Page','.jsp');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('64','10','HP Source Code File','.php');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('65','10','Rich Site Summary','.rss');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('66','11','Android Package File','.apk');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('67','11','Mac OS X Application','.app');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('68','11','DOS Batch File','.bat');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('69','11','Common Gateway Interface Script','.cgi');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('70','11','DOS Command File','.com');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('71','11','Windows Executable File','.exe');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('72','11','Java Archive File','.jar');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('73','11','Program Information File','.pif');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('74','11','VBScript File','.vb');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('75','12','7-Zip Compressed File','.7z');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('76','12','Comic Book RAR Archive','.cbr');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('77','12','Debian Software Package','.deb');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('78','12','Mac OS X Installer Package','.pkg');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('79','12','Red Hat Package Manager File','.rpm');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('80','12','StuffIt Archive','.sit');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('81','12','StuffIt X Archive','.sitx');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('82','12','Extended Zip File','.zipx');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('83','13','Backup File','.bak');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('84','13','Temporary File','.tmp');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('85','14','Configuration File','.cfg');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('86','14','Windows Initialization File','.ini');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('87','14','Outlook Profile File','.prf');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('88','15','Amazon Kindle eBook File','.azw');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('89','15','Comma Separated Values File','.csv');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('90','15','Data File','.dat');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('91','15','Open eBook File','.epub');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('92','15','Gerber File','.gbr');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('93','15','GEDCOM Genealogy Data File','.ged');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('94','15','Keynote Presentation','.key');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('95','15','PowerPoint Slide Show','.pps');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('96','15','Standard Data File','.sdf');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('97','15','vCard File','.vcf');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('98','15','XML File','.xml');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('99','16','Windows Cabinet File','.cab');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('100','16','Windows Control Panel Item','.cpl');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('101','16','Windows Cursor','.cur');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('102','16','Dynamic Link Library','.dll');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('103','16','Windows Memory Dump','.dmp');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('104','16','Device Driver','.drv');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('105','16','Mac OS X Icon Resource File','.icns');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('106','16','Icon File','.ico');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('107','16','Windows File Shortcut','.lnk');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('108','16','Windows System File','.sys');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('109','17','iCalendar File','.ics');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('110','17','Windows Installer Package','.msi');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('111','17','Partially Downloaded File','.part');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('112','18','Rhino 3D Model','.3dm');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('113','18','3D Studio Scene','.3ds');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('114','18','3ds Max Scene File','.max');
insert into `document_type` (`id`, `document_category_id`, `name`, `value`) values('115','18','Wavefront 3D Object File','.obj');

INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Text Files'),'Rich Text Format File','.rtf');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Text Files'),'Pages Document','.pages');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Text Files'),'Text Document','.txt');

INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Data Base'),'Access 2007 Database File','.accdb');

INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Web'),'Hypertext Markup Language File','.htm');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Web'),'Hypertext_Markup Language File','.html');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Web'),'Extensible Hypertext Markup Language File','.xhtml');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Web'),'Cascading Style Sheet','.css');

INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Executable'),'Windows Script File','.wsf');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Executable'),'Windows Gadget','.gadget');

INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Compressed'),'Zipped File','.zip');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Compressed'),'Compressed Tarball File','.tar.gz');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Compressed'),'WinRAR Compressed Archive','.rar');

INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Compressed'),'Gzip 1','.gz');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Compressed'),'Gzip 2','.tgz');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Compressed'),'TAR','.tar');

INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Miscellaneous'),'Chrome Partially Downloaded File','.crdownload');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Miscellaneous'),'BitTorrent File','.torrent');


INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Audio'),'MP3 Audio File','.mp3');

INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Audio'),'Interchange File Format','.iif');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Audio'),'Media Playlist File','..m3u');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Audio'),'MPEG-4 Audio File','.m4a');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Audio'),'Real Audio File','.ra');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Audio'),'MPEG-2 Audio File','.mpa');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Audio'),'Advanced Audio Coding File','.aac');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Audio'),'Adobe Flash Protected Audio File','.f4a');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Audio'),'Ogg Vorbis Audio File','.oga');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Audio'),'Ogg_Vorbis Audio File','.ogg');

INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'3GPP2 Multimedia File','.3g2');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'3GPP Multimedia File','.3gp');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'Advanced Systems Format File','.asf');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'Microsoft ASF Redirector File','.asx');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'iTunes Video File','.m4v');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'MPEG-4 Video File','.mp4');

INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'Real Media File','.rm');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'SubRip Subtitle File','.srt');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'Shockwave Flash Movie','.swf');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'DVD Video Object File','.vob');

INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'Flash MP4 Video File','.f4v');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'3GPP_Multimedia File','.3gp2');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'Ripped Video Data File','.264');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'3GPP Media File','.3gpp');


INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Images'),'Adobe Photoshop Document','.psd');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Images'),'Adobe Illustrator File','.ai');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Images'),'Encapsulated PostScript File','.eps');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Images'),'Scalable Vector Graphics File','.svg');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Images'),'PostScript File','.ps');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Images'),'YUV Encoded Image File','.yuv');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Images'),'Thumbnail Image File','.thm');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Images'),'Targa Graphic','.tga');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Images'),'PaintShop Pro Image','.pspimage');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Images'),'DirectDraw Surface','.dds');



/*Data for the table `policy` */

insert into `policy`(`id`,`active`,`description`,`document_age`,`last_accessed_date`,`last_modified_date`,`name`,`size_larger_than`) 
             values (0,0,null,'0,0','0,0','0,0','DefaultPolicy','0,KB');
             
/*Data for the table `error_code` */
             
insert  into `error_code`(`id`,`description`) 
                  values (0,'success'),(1,'failed'),(2,'canceled');

/*Data for the table `shiro_role` */

insert  into `shiro_role`(`id`,`name`) 
                  values (1,'ADMINISTRATOR'),(2,'PRIVILEGED'), (3, 'EMAIL');

/*Data for the table `shiro_role_permission` */

insert  into `shiro_role_permissions`(`shiro_role_id`,`permissions_string`) 
                              values (2,'searchDocuments:*'),(1,'*:*'),(2,'shiroUser:changePassword'),(2,'shiroUser:updatePassword'),
							         (2,'shiroUser:cancelPasswordUpdation'),(2,'job:save_export'),(2,'job:show_export');

/*Data for the table `shiro_user` */

insert  into `shiro_user`(`id`,`email`,`first_name`,`last_name`,`lock_status`,`password_hash`,`username`) 
       values (9,'priv@sharearchiver.com','priv','priv', 'Active','3b9aa142fefa44aab83ab1c0909f6929fd53656b895ec2fc0e47d412ea62ba54','priv'),
		      (15,'admin@sharearchiver.com','admin','admin','Active','8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918','admin');

/*Data for the table `shiro_user_roles` */

insert  into `shiro_user_roles`(`shiro_role_id`,`shiro_user_id`) 
                        values (2,9),(1,15);

/*Data for the table `system_code` */

insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('1','companyName','archive','General','2013-01-18 12:30:16','1','1','Company Name');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('2','enable.stub.authorization','no','General','2013-01-18 12:30:16','1','1','Enable Stub Authorization');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('3','delay.time','60','General','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('4','repeat.time','120','General','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('5','bindstr','','activedirauthenticator','2013-01-18 12:30:16','1','1','Bind String');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('6','dns.servers','','activedirauthenticator','2013-01-18 12:30:16','1','1','DNS Server');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('7','service.password','','activedirauthenticator','2013-01-18 12:30:16','1','1','Service Password');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('8','domain.name','','activedirauthenticator','2013-01-18 12:30:16','1','1','Domain Name');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('9','restore.password','','General','2013-01-18 12:30:16','1','1','Restore Password');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('10','MAX_SEARCH_RESULTS','2000','General','2013-01-18 12:30:16','1','1','Maximum Search Results');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('11','http.parameter.username.name','username','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('12','http.parameter.password.name','password','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('13','http.parameter.logout.name','logout','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('14','http.parameter.anonymous.name','anon','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('15','fallback.location','/ShareArchiver/login.faces','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('16','excludes','/ShareArchiver/login.faces','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('17','jespa.log.path','/jespa.log','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('18','jespa.log.level','4','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('19','jespa.account.canonicalForm','3','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('20','jespa.bindstr','','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('21','jespa.domain.dns.name','','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('22','jespa.dns.servers','','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('23','jespa.dns.site','','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('24','jespa.service.acctname','','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('25','jespa.service.password','','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('26','JobStatistickeepAliveTime','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('27','ArchiverThreadPoolSize','1','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('28','ExportEvalutormaxPoolSize','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('29','ExportEvalutorkeepAliveTime','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('30','EvalutorThreadPoolSize','1','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('31','MAX_NO_OF_TRIES','3','ZuesAgent','2013-01-18 12:30:16','1','1','Maximum Tries to Write');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('32','EvalutorpoolSize','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('33','ArchivermaxPoolSize','50','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('34','ExportEvalutorThreadPoolSize','1','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('35','EvalutormaxPoolSize','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('36','StubbermaxPoolSize','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('37','ExporterQueueSize','100','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('38','ExportEvalutorTimeAliveUnit','DAYS','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('39','ExportermaxPoolSize','50','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('40','portName','BasicHttpBinding_IDASService','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('41','ExporterThreadPoolSize','1','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('42','AgentName','Sharearchiver','ZuesAgent','2013-01-18 12:30:16','1','1','Agent Name');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('43','ExportJobStatisticQueueSize','100','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('44','DeleteFiles','1','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('45','StubberTimeAliveUnit','DAYS','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('46','ExporterpoolSize','50','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('47','EvalutorkeepAliveTime','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('48','StubberkeepAliveTime','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('49','portType','IDASService','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('50','JobStatisticpoolSize','2','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('51','StubberThreadPoolSize','1','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('52','ExportEvalutorpoolSize','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('53','ExportEvalutorQueueSize','0','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('54','EvaluatorInterThreadTimeOut','30000','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('55','JobStatisticThreadPoolSize','1','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('56','ExporterTimeAliveUnit','DAYS','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('57','IntraThreadTimeOut','10000','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('58','ArchiverTimeAliveUnit','DAYS','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('59','JobStatisticQueueSize','100','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('60','EvalutorQueueSize','0','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('61','maxFileSize','100MB','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('62','AgentLogin','sharearchiver','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('63','mapShareInfo','0','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('64','ExportJobStatisticThreadPoolSize','1','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('65','StubberQueueSize','100','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('66','webServiceURL','http://sharepoint.virtualcode.co.uk:8087/DasSPAgentWS/DASService.svc','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('67','JobStatisticTimeAliveUnit','DAYS','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('68','DASUrl','http://sharearchiver:8080/ShareArchiver','ZuesAgent','2013-01-18 12:30:16','1','1','Application Url');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('69','ExportJobStatistickeepAliveTime','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('70','ArchiverQueueSize','3','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('71','StubPath','\\\\\\\\sharearchiver\\\\Icons','ZuesAgent','2013-01-18 12:30:16','1','1','Stub Path');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('72','ConversionPattern','%d [%t] %-5p [%-35F:%-25M:%-6L] %-C -%m%n','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('73','ArchiverkeepAliveTime','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('74','targetNamespace','http://tempuri.org/','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('75','StubberpoolSize','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('76','EvalutorTimeAliveUnit','DAYS','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('77','threshold','debug','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('78','maxBackupIndex','100','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('79','ExporterkeepAliveTime','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('80','RunnerInterThreadTimeOut','30000','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('81','ArchiverpoolSize','2','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('82','ExportJobStatisticTimeAliveUnit','DAYS','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('83','AgentPassword','sharearchiver','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('84','JobStatisticmaxPoolSize','5','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('85','ExportJobStatisticpoolSize','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('86','CM','1','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('87','ExportJobStatisticmaxPoolSize','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('88','AdderInterThreadTimeOut','15000','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('89','service.acctname','','activedirauthenticator','2013-01-18 12:30:16','1','1','Service Account Name');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('90','authentication.mode','1','General','2013-01-18 12:30:16','1','1','Authentication Mode');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('91','authentication.enable.sso','no','General','2013-01-18 12:30:16','1','1','Enable SSO Authentication');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('92','dns.site','','activedirauthenticator','2013-01-18 12:30:16','1','1','DNS Site');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('93','restore.username','','General','2013-01-18 12:30:16','1','1','Resotre User Name');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('94','page.size','10','activedirauthenticator','2013-01-18 12:30:16','1','1','LDAP Fetch Size');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('95','restore.domain_name','','General','2013-01-18 12:30:16','1','1','Restore Domain Name');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('96','enable.priv.export','no','General','2013-01-18 12:30:16','1','1','Enable Priv Export');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('97','MAX_RESULTS_TO_EXPORT','500','General','2013-01-18 12:30:16','1','1','Maximum Export Results');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('98','enable.priv.report','no','General','2013-01-18 12:30:16','1','1','Enable Priv Report');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('99','enable.stub.driveLetter','no','General','2013-01-18 12:30:16','1','1','Enable Drive Letters');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('100','enable.document.tagging','no','General','2013-01-18 12:30:16','1','1','Enable Document Tagging');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('101','show.tags.searchResults','no','General','2013-01-18 12:30:16','1','1','Enable Tags in Search Results');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('103','show.user.auditHistory','5','General','2013-01-18 12:30:16','1','1','User Audit History');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('104','logBasePath','/opt/tomcat/jobLogs/','ZuesAgent','2013-01-18 12:30:16','1','1','Job Logs Path');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('105','indexesBasePath','/opt/tomcat/indexes/','ZuesAgent','2013-01-18 12:30:16','1','1','Job Indexes Path');

insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('106','accessDeniedMessage','Access Denied. Please contact your systems administrator','General','2013-01-18 12:30:16','1','1','Access Denied Message');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('107','documentNotFoundMessage','Document not found. Please contact your systems administrator','General','2013-01-18 12:30:16','1','1','Document Not Found Message');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('108','enable_sharepoint','no','General','2013-01-18 12:30:16','1','1','Enable SharePoint Archiving');
insert into `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('109','enable_folder_based_search','no','General','2013-02-11 13:03:16','1','1','Enable Search in a specific folder');
INSERT INTO `system_code` (`id`, `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('110','logLevel', 'INFO', 'ZuesAgent', '2013-02-11 13:03:16', '1', '1', 'Level for Agent Log');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_video_streaming','no','General','2013-04-11 13:03:16','1','1','Enable video streaming');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_job_status_email','no','General','2013-05-11 13:03:16','1','1','Enable Job Status Email');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_links_sharing_in_email','no','General','2013-08-11 13:03:16','1','1','Enable links sharing in email');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_file_sharing_in_email','no','General','2013-08-11 13:03:16','1','1','Enable file sharing in email');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('mail_from','','General','2013-05-11 13:03:16','1','1','Email from');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('mail_to','','General','2013-05-11 13:03:16','1','1','Email to');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('mail_username','','General','2013-05-11 13:03:16','1','1','Email username');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('mail_password','','General','2013-05-11 13:03:16','1','1','Email password');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('smpt_host','','General','2013-05-11 13:03:16','1','1','SMPT Host');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('smpt_port','25','General','2013-05-11 13:03:16','1','1','SMPT Port');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_content_type_search','no','General','2013-05-20 13:03:16','1','1','Enable Content Type Search');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('mail_sec_protocol','SSL','General','2013-06-13 10:06:16','1','1','Email Security Protocol');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('max_attachment_size','10','General','2013-07-22 11:48:16','1','1','Maximum Attachment Size');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('stub_access_mode','1','General','2013-06-13 15:33:00','1','1','Stub Access Mode');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_print_archiving','no','General','2013-06-13 15:33:00','1','1','Enable Print Archiving');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_archiving_tab', 'no', 'General','2013-07-04 11:50:00', '1', '1', 'Enable stubbing tab');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_stubbing_tab', 'no', 'General','2013-07-04 11:50:00', '1', '1', 'Enable stubbing tab');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_evaluation_tab', 'no', 'General','2013-07-04 11:50:00', '1', '1', 'Enable evaluation tab');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_analysis_tab', 'no', 'General','2013-07-04 11:50:00', '1', '1', 'Enable analysis tab');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_export_tab', 'no', 'General','2013-07-04 11:50:00', '1', '1', 'Enable export tab');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_active_archiving_tab', 'no', 'General', '2013-07-04 11:50:00', '1', '1', 'Enable active archiving tab');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_dynamic_archiving_tab', 'no', 'General', '2013-07-04 11:50:00', '1', '1', 'Enable active archiving tab');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_retention_tab', 'no', 'General', '2013-07-04 11:50:00', '1', '1', 'Enable retention tab');
INSERT INTO `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) VALUES('enable_scheduling_tab', 'no', 'General', '2013-07-08 11:50:00', '1', '1', 'Enable scheduling tab');
insert into `system_code` (`codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('sh_license','af98cff45c7dbeddc7a516ccbf44d3c2661316325ebf31259400b6c315351a191eaa968b6d408baa39f159d52ecfd23c','General','2013-09-10 18:24:42','1','1','license str');
INSERT INTO `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) VALUES ('LazyExport', 'true', 'General', '2013-07-08 11:50:00', '1', '1', 'Lazy Export');
INSERT INTO `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) VALUES ('LazyArchiving', 'true', 'General', '2013-07-08 11:50:00', '1', '1', 'Lazy Archiving');

INSERT INTO `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) VALUES ('enable_thumbnails_search', 'no', 'General', '2013-10-23 11:50:00', '1', '1', 'Enable thumbnails on search');
INSERT INTO `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) VALUES ('enable_thumbnails_stub', 'no', 'General', '2013-10-23 11:50:00', '1', '1', 'Enable thumbnails on Stub');
INSERT INTO `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) VALUES ('enable_thumbnails_summary', 'no', 'General', '2013-10-23 11:50:00', '1', '1', 'Enable thumbnails on Summary');
INSERT INTO `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) VALUES ('enable_thumbnails_list', 'no', 'General', '2013-10-23 11:50:00', '1', '1', 'Enable thumbnails on list');

INSERT INTO `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) VALUES ('show_path_in_search_results', 'no', 'General', '2013-10-11 05:43:00', '1', '1', 'Show file path in search results');
INSERT INTO `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) VALUES ('show_excerpt_in_search_results', 'no', 'General', '2013-10-11 05:43:00', '1', '1', 'Show excerpt in search results');
INSERT INTO `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) VALUES ('include_linked_files', 'no', 'General', '2013-11-25 05:43:00', '1', '1', 'Show linked files in search');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('enable_default_email_client','no','General','2013-11-12 11:49:16','1','1','Enable Default Client for Email');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('calculate_folder_size','no','General','2013-011-29 02:38:00','1','1','Calculate folder size on FE');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('max_no_of_versions','5','General','2013-11-21 12:28:00','1','1','Max No. of versions to display');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('enable_cifs_file_sharing','no','General','2013-12-26 11:38:00','1','1','Enable CIFS File Sharing');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('copy_target_file_to_archive','yes','General','2013-12-26 11:38:00','1','1','Copy Target File to archive');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('maxArchiveTries','3','ZuesAgent','2014-01-06 11:38:00','1','1','Number of reTries for failed archivals');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('MAX_ANALYSIS_RESULTS','99999','General','2013-01-18 12:30:16','1','1','Maximum Analysis Results');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('MAX_BATCH_SIZE','2000','General','2014-01-22 04:39:00','1','1','Max batch size');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('enable_file_sharing_security','no','General','2014-02-03 13:43:00','1','1','Enable security on links');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('BUFF_SIZE','8192','General','2014-02-10 04:39:00','1','1','Archiving Buffer Size');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('BUFF_SIZE_W','16392','General','2014-02-10 04:49:00','1','1','Restore Buffer Size (Write)');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('emulate_email_sender','no','General','2014-02-26 14:06:00','1','1','Emulate sender in file sharing');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('company_name','','Company','2014-03-05 02:54:00','1','1','Company name');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('company_prefered_name','','Company','2014-03-05 02:54:00','1','1','Preferred Name');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('company_address','','Company','2014-03-05 02:54:00','1','1','Company Address');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('company_telephone','','Company','2014-03-05 02:54:00','1','1','Telephone');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('company_support_contact','','Company','2014-03-05 02:54:00','1','1','Support Contact ');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('company_support_email','','Company','2014-03-05 02:54:00','1','1','Support Email');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('restrict_cifs_files_sharing_ad','no','General','2014-03-18 00:00:00','1','1','Restrict AD groups file sharing(cifs)');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('restrict_archive_files_sharing_ad','no','General','2014-03-18 00:00:00','1','1','Restrict AD groups file sharing (archive)');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('shared_link_validity','7','General','2014-03-18 00:00:00','1','1','Shared link valid upto');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('guardian_from_address','','General','2014-03-20 00:00:00','1','1','Guardian from address');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('server_warning_limit','90','General','2014-03-31 00:00:00','1','1','Server warning limit');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('server_critical_limit','95','General','2014-03-31 00:00:00','1','1','Server Critical limit');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('enable_withoutStub_tab','no','General','2014-04-24 00:00:00','1','1','Enable Archive and Delete tab');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('enable_repo_search','no','General','2014-06-09 00:00:00','1','1','Enable Repo search');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('enable_cifs_search','no','General','2014-06-09 00:00:00','1','1','Enable Cifs search');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('enable_file_explorer_for_admin_cm','no','General','2014-07-04 00:00:00','1','1','Enable FExplorer for admin in compatiabiity mode');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('password_expiry_time','30','AccountPolicy','2014-07-04 00:00:00','1','1','Enable Password Timeout (Days)');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('no_of_tries_befor_account_lock','3','AccountPolicy','2014-07-04 00:00:00','1','1','No of tries before account lock ');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('account_lock_timeout_period','5','AccountPolicy','2014-07-04 00:00:00','1','1','Password lock time out period (minutes)');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('password_minimum_length','8','AccountPolicy','2014-07-04 00:00:00','1','1','Minimum password length');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('enforce_caps_in_passwords','no','AccountPolicy','2014-07-04 00:00:00','1','1','Enforce CAPS in Passwords');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('enforce_numbers_in_passwords','no','AccountPolicy','2014-07-04 00:00:00','1','1','Enforce numbers in Passwords');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('enforce_special_chars_in_passwords','no','AccountPolicy','2014-07-04 00:00:00','1','1','Enforce special chars in Passwords');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('path_for_logos','/opt/tomcat/sa_configs/logos','General','2014-07-21 00:00:00','1','1','Path for logos');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('path_for_configs','/opt/tomcat/sa_configs','General','2014-07-22 00:00:00','1','1','Path for Configs');
igs');



/*Data for the table `repo_stats` * /

insert  into `repo_stats`(`id`,`repo_path`,`total_folders`,`total_files`,`fs_docs`,`sp_docs`,`used_space`,`duplicate_data_size`) values (1,'/opt/sa_repository',0,0,0,0,0,0);

/*Data for table `page_rights`*/

INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'AddAgent.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'AddDocumentCategory.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'AddDocumentType.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'AddDriveLetter.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'AddJob.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'AddPolicy.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'AddUser.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'Agents.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'ChangePassword.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'ChangePassword.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'defaultTemplate.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'DocumentCategories.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'DocumentTypes.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'DriveLetters.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'EditAgent.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'EditDriveLetter.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'EditExportJob.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'EditGlobalConfig.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'EditJob.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'EditLdap.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'EditLDAPConfig.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'EditPermissions.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'EditPolicy.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'EditUser.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'Error.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'extPageTemplate.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'extPageTemplate2.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'FileExplorer.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'GlobalCodes.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'Jobs.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'JobStatus.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'LDAPCodes.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'ManageSystem.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'Policies.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'showDocumentCategory.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'showDocumentType.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'statistics.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'Status.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'System.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'SystemCodes.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'Users.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'ViewAgent.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'ViewDriveLetter.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'ViewJob.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'ViewPolicy.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'ViewUser.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'EditCompanyConfig.faces');

INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'Browse.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'stubTemplate.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'StubAccess.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'Search.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'FExplorerExtContent.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'SearchContent.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'Help.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'RestoreStub.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'UserPreferences.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'UserFeatures.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'PdfViewer.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'PdfViewer.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'Error.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'Error.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (3, 'Error.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (3, 'EmailDocumentAccess.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (3, 'ChangePassword.faces');

INSERT INTO page_rights (shiro_role_id ,page_uri)VALUES ('1', 'DelPolicy.faces');
INSERT INTO page_rights (shiro_role_id ,page_uri)VALUES ('1', 'AddDelPolicy.faces');
INSERT INTO page_rights (shiro_role_id ,page_uri)VALUES ('1', 'EditDelPolicy.faces');
INSERT INTO page_rights (shiro_role_id ,page_uri)VALUES ('1', 'DelJob.faces');
INSERT INTO page_rights (shiro_role_id ,page_uri)VALUES ('1', 'AddDelJob.faces');
INSERT INTO page_rights (shiro_role_id ,page_uri)VALUES ('1', 'EditDelJobs.faces');
INSERT INTO page_rights (shiro_role_id ,page_uri)VALUES ('1', 'ViewDJob.faces');
INSERT INTO page_rights (shiro_role_id ,page_uri)VALUES ('1', 'AccountPolicy.faces');
INSERT INTO page_rights (shiro_role_id ,page_uri)VALUES ('2', 'UserFeatures.faces');
INSERT INTO page_rights (shiro_role_id ,page_uri)VALUES ('1', 'ToolsDownloader.faces');
	
	
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('email_link_sp','Email document link - Search page','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('email_doc_sp','Email document as attachment -  Search page','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('download_sp','Download icon - Search page','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('restore_file_sp','Restore Icon - Search page','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('media_streaming_sp','Media streaming Icon - Search page','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('email_link','Email document link - File explorer','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('email_doc','Email document as attachment - File explorer','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('download','Download Icon - File explorer','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('restore_file','Restore Icon - File explorer','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('restore_folder','Restore folder - File explorer','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('media_streaming','Media streaming - File explorer','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('document_viewing','Online doc viewer icon - File explorer','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('document_viewing_sp','Online doc viewer icon - Search page','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('content_type_search_sp','Content type search','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('document_tagging','Document tagging','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('path_based_search_sp','Path based search','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('show_tags_sp','Show tags in search','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('archive_date_search_sp','Archive Date search','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('show_document_url','Show Complete document URL','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('search_by_id_sp','Enable Search by UUID','0','0','0');
						
commit;