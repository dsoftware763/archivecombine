/*change DB character set to utf8 (work only for new tables) */
ALTER DATABASE sharearchiver CHARACTER SET utf8;

/*change character set for existing tables to utf8*/
ALTER TABLE agent CONVERT TO CHARACTER SET utf8;
ALTER TABLE document_category CONVERT TO CHARACTER SET utf8;
ALTER TABLE document_type CONVERT TO CHARACTER SET utf8;
ALTER TABLE drive_letters CONVERT TO CHARACTER SET utf8;
ALTER TABLE error_code CONVERT TO CHARACTER SET utf8;
ALTER TABLE job CONVERT TO CHARACTER SET utf8;
ALTER TABLE job_document CONVERT TO CHARACTER SET utf8;
ALTER TABLE job_sp_doc_lib CONVERT TO CHARACTER SET utf8;
ALTER TABLE job_sp_doc_lib_excluded_path CONVERT TO CHARACTER SET utf8;
ALTER TABLE job_statistics CONVERT TO CHARACTER SET utf8;
ALTER TABLE job_status CONVERT TO CHARACTER SET utf8;
ALTER TABLE message CONVERT TO CHARACTER SET utf8;
ALTER TABLE monitor CONVERT TO CHARACTER SET utf8;
ALTER TABLE node_permission_job CONVERT TO CHARACTER SET utf8;
ALTER TABLE policy CONVERT TO CHARACTER SET utf8;
ALTER TABLE policy_document_type CONVERT TO CHARACTER SET utf8;
ALTER TABLE repo_stats CONVERT TO CHARACTER SET utf8;
ALTER TABLE shiro_role CONVERT TO CHARACTER SET utf8;
ALTER TABLE shiro_role_permissions CONVERT TO CHARACTER SET utf8;
ALTER TABLE shiro_user CONVERT TO CHARACTER SET utf8;
ALTER TABLE shiro_user_permissions CONVERT TO CHARACTER SET utf8;
ALTER TABLE shiro_user_roles CONVERT TO CHARACTER SET utf8;
ALTER TABLE system_alerts CONVERT TO CHARACTER SET utf8;
ALTER TABLE system_code CONVERT TO CHARACTER SET utf8;
ALTER TABLE system_info CONVERT TO CHARACTER SET utf8;



/*for job scheduling */
ALTER TABLE `job` ADD `is_scheduled` BOOL NOT NULL DEFAULT FALSE;
ALTER TABLE `job` ADD `is_threshhold_job` BOOL NOT NULL DEFAULT FALSE;
ALTER TABLE `job` ADD `exec_interval` DOUBLE NOT NULL DEFAULT 0;
ALTER TABLE `job` ADD `preserve_file_paths` bit(1) DEFAULT NULL;
ALTER TABLE `job` ADD `export_duplicates` bit(1) DEFAULT NULL;
ALTER TABLE `job` ADD `active_action_type` varchar(20) DEFAULT NULL;
ALTER TABLE `job` ADD `is_share_analysis` tinyint(1) DEFAULT '0';
ALTER TABLE `job` ADD `tags` varchar(255) DEFAULT NULL;
ALTER TABLE `job` ADD `current_status` varchar(25) DEFAULT NULL;

ALTER TABLE `job_statistics` ADD `archived_volume` bigint(20) DEFAULT '0';

ALTER TABLE `job_sp_doc_lib` ADD `site` varchar(255) DEFAULT NULL;
ALTER TABLE `job_sp_doc_lib` ADD `drive_path` varchar(1000) DEFAULT NULL;
ALTER TABLE `job_sp_doc_lib` ADD `content_types` varchar(1000) DEFAULT NULL;
ALTER TABLE `job_sp_doc_lib` ADD `tags` varchar(500) DEFAULT NULL;

ALTER TABLE `job_status` ADD `action_by_user` varchar(50) DEFAULT NULL;
ALTER TABLE `job_status` ADD `action_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP;

/*for domain, username and password for drive letters*/
ALTER TABLE `drive_letters` ADD `restore_domain` varchar(30) DEFAULT NULL;
ALTER TABLE `drive_letters` ADD `restore_username` varchar(20) DEFAULT NULL;
ALTER TABLE `drive_letters` ADD `restore_password` varchar(20) DEFAULT NULL;
ALTER TABLE `drive_letters` ADD `is_file_share` tinyint(1) DEFAULT '1';


/*for system_code display name*/
ALTER TABLE `system_code` ADD display_name varchar(1000) DEFAULT NULL;
ALTER TABLE `system_code` MODIFY codename VARCHAR(200) NOT NULL;
ALTER IGNORE TABLE `system_code` ADD CONSTRAINT UNIQUE KEY `UNIQUE_CODE_NAME` (`codename`);

/*for drive_letters drive_letter*/
ALTER TABLE drive_letters MODIFY drive_letter VARCHAR(5);

ALTER TABLE `link_access_documents` ADD `sender` varchar(100) DEFAULT NULL;
ALTER TABLE `link_access_documents` ADD `hash` varchar(100) DEFAULT NULL;
ALTER TABLE `link_access_documents` ADD `identifier` bigint(20) DEFAULT NULL;
ALTER TABLE `link_access_documents` ADD`link_expire_date` timestamp NULL DEFAULT NULL;

/*New column for user last password change*/
ALTER TABLE `shiro_user` ADD `last_password_change` timestamp NULL DEFAULT CURRENT_TIMESTAMP;

/*New entry for new user type*/

INSERT INTO shiro_role VALUE (3, 'EMAIL');


/*Data for the table `system_code` */

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('companyName','archive','General','2013-01-18 12:30:16','1','1','Company Name');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable.stub.authorization','no','General','2013-01-18 12:30:16','1','1','Enable Stub Authorization');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('delay.time','60','General','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('repeat.time','120','General','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('bindstr','','activedirauthenticator','2013-01-18 12:30:16','1','1','Bind String');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('dns.servers','','activedirauthenticator','2013-01-18 12:30:16','1','1','DNS Server');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('service.password','','activedirauthenticator','2013-01-18 12:30:16','1','1','Service Password');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('domain.name','','activedirauthenticator','2013-01-18 12:30:16','1','1','Domain Name');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('restore.password','','General','2013-01-18 12:30:16','1','1','Restore Password');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('MAX_SEARCH_RESULTS','2000','General','2013-01-18 12:30:16','1','1','Maximum Search Results');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('MAX_ANALYSIS_RESULTS','99999','General','2013-01-18 12:30:16','1','1','Maximum Analysis Results');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('http.parameter.username.name','username','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('http.parameter.password.name','password','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('http.parameter.logout.name','logout','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('http.parameter.anonymous.name','anon','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('fallback.location','/ShareArchiver/login.faces','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('excludes','/ShareArchiver/login.faces','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('jespa.log.path','/jespa.log','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('jespa.log.level','4','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('jespa.account.canonicalForm','3','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('jespa.bindstr','','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('jespa.domain.dns.name','','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('jespa.dns.servers','','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('jespa.dns.site','','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('jespa.service.acctname','','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('jespa.service.password','','jespa','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('JobStatistickeepAliveTime','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ArchiverThreadPoolSize','1','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ExportEvalutormaxPoolSize','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ExportEvalutorkeepAliveTime','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('EvalutorThreadPoolSize','1','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('MAX_NO_OF_TRIES','3','ZuesAgent','2013-01-18 12:30:16','1','1','Maximum Tries to Write');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('EvalutorpoolSize','2','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ArchivermaxPoolSize','5','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ExportEvalutorThreadPoolSize','1','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('EvalutormaxPoolSize','3','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('StubbermaxPoolSize','3','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ExporterQueueSize','100','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ExportEvalutorTimeAliveUnit','DAYS','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ExportermaxPoolSize','50','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('portName','BasicHttpBinding_IDASService','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ExporterThreadPoolSize','1','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('AgentName','Sharearchiver','ZuesAgent','2013-01-18 12:30:16','1','1','Agent Name');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ExportJobStatisticQueueSize','100','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('DeleteFiles','1','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('StubberTimeAliveUnit','DAYS','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ExporterpoolSize','50','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('EvalutorkeepAliveTime','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('StubberkeepAliveTime','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('portType','IDASService','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('JobStatisticpoolSize','2','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('StubberThreadPoolSize','1','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ExportEvalutorpoolSize','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ExportEvalutorQueueSize','0','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('EvaluatorInterThreadTimeOut','30000','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('JobStatisticThreadPoolSize','1','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ExporterTimeAliveUnit','DAYS','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('IntraThreadTimeOut','10000','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ArchiverTimeAliveUnit','DAYS','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('JobStatisticQueueSize','100','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('EvalutorQueueSize','0','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('maxFileSize','100MB','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('AgentLogin','sharearchiver','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('mapShareInfo','0','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ExportJobStatisticThreadPoolSize','1','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('StubberQueueSize','100','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('webServiceURL','http://sharepoint.virtualcode.co.uk:8087/DasSPAgentWS/DASService.svc','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('JobStatisticTimeAliveUnit','DAYS','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('DASUrl','http://sharearchiver:8080/ShareArchiver','ZuesAgent','2013-01-18 12:30:16','1','1','Application Url');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ExportJobStatistickeepAliveTime','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ArchiverQueueSize','3','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('StubPath','\\\\\\\\sharearchiver\\\\Icons','ZuesAgent','2013-01-18 12:30:16','1','1','Stub Path');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ConversionPattern','%d [%t] %-5p [%-35F:%-25M:%-6L] %-C -%m%n','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ArchiverkeepAliveTime','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('targetNamespace','http://tempuri.org/','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('StubberpoolSize','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('EvalutorTimeAliveUnit','DAYS','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('threshold','debug','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('maxBackupIndex','100','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ExporterkeepAliveTime','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('RunnerInterThreadTimeOut','30000','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ArchiverpoolSize','2','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ExportJobStatisticTimeAliveUnit','DAYS','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('AgentPassword','sharearchiver','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('JobStatisticmaxPoolSize','5','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ExportJobStatisticpoolSize','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('CM','1','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('ExportJobStatisticmaxPoolSize','10','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('AdderInterThreadTimeOut','15000','ZuesAgent','2013-01-18 12:30:16','0','0','');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('service.acctname','','activedirauthenticator','2013-01-18 12:30:16','1','1','Service Account Name');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('authentication.mode','1','General','2013-01-18 12:30:16','1','1','Authentication Mode');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('authentication.enable.sso','no','General','2013-01-18 12:30:16','1','1','Enable SSO Authentication');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('dns.site','','activedirauthenticator','2013-01-18 12:30:16','1','1','DNS Site');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('restore.username','','General','2013-01-18 12:30:16','1','1','Resotre User Name');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('page.size','10','activedirauthenticator','2013-01-18 12:30:16','1','1','LDAP Fetch Size');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('restore.domain_name','','General','2013-01-18 12:30:16','1','1','Restore Domain Name');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable.priv.export','no','General','2013-01-18 12:30:16','1','1','Enable Priv Export');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('MAX_RESULTS_TO_EXPORT','500','General','2013-01-18 12:30:16','1','1','Maximum Export Results');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable.priv.report','no','General','2013-01-18 12:30:16','1','1','Enable Priv Report');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable.stub.driveLetter','no','General','2013-01-18 12:30:16','1','1','Enable Drive Letters');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable.document.tagging','no','General','2013-01-18 12:30:16','1','1','Enable Document Tagging');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('show.tags.searchResults','no','General','2013-01-18 12:30:16','1','1','Enable Tags in Search Results');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('show.user.auditHistory','5','General','2013-01-18 12:30:16','1','1','User Audit History');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('logBasePath','/opt/tomcat/jobLogs/','ZuesAgent','2013-01-18 12:30:16','1','1','Job Logs Path');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('indexesBasePath','/opt/tomcat/indexes/','ZuesAgent','2013-01-18 12:30:16','1','1','Job Indexes Path');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('accessDeniedMessage','Access Denied. Please contact your systems administrator','General','2013-01-18 12:30:16','1','1','Access Denied Message');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('documentNotFoundMessage','Document not found. Please contact your systems administrator','General','2013-01-18 12:30:16','1','1','Document Not Found Message');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_sharepoint','no','General','2013-01-18 12:30:16','1','1','Enable SharePoint Archiving');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_folder_based_search','no','General','2013-02-11 13:03:16','1','1','Enable Search in a specific folder');
INSERT INTO `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('logLevel', 'INFO', 'ZuesAgent', '2013-02-11 13:03:16', '1', '1', 'Level for Agent Log');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_video_streaming','no','General','2013-04-11 13:03:16','1','1','Enable video streaming');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_job_status_email','no','General','2013-05-11 13:03:16','1','1','Enable Job Status Email');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_links_sharing_in_email','no','General','2013-08-11 13:03:16','1','1','Enable links sharing in email');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_file_sharing_in_email','no','General','2013-08-11 13:03:16','1','1','Enable file sharing in email');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('mail_from','','General','2013-05-11 13:03:16','1','1','Email from');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('mail_to','','General','2013-05-11 13:03:16','1','1','Email to');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('mail_username','','General','2013-05-11 13:03:16','1','1','Email username');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('mail_password','','General','2013-05-11 13:03:16','1','1','Email password');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('mail_sec_protocol','SSL','General','2013-06-13 10:06:16','1','1','Email Security Protocol');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('smpt_host','','General','2013-05-11 13:03:16','1','1','SMPT Host');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('smpt_port','25','General','2013-05-11 13:03:16','1','1','SMPT Port');


insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('max_attachment_size','10','General','2013-07-22 11:48:16','1','1','Maximum Attachment Size');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_content_type_search','no','General','2013-05-20 13:03:16','1','1','Enable Content Type Search');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('stub_access_mode','1','General','2013-06-13 15:33:00','1','1','Stub Access Mode');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_print_archiving','no','General','2013-06-13 15:33:00','1','1','Enable Print Archiving');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_archiving_tab', 'no', 'General','2013-07-04 11:50:00', '1', '1', 'Enable stubbing tab');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_stubbing_tab', 'no', 'General','2013-07-04 11:50:00', '1', '1', 'Enable stubbing tab');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_evaluation_tab', 'no', 'General','2013-07-04 11:50:00', '1', '1', 'Enable evaluation tab');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_analysis_tab', 'no', 'General','2013-07-04 11:50:00', '1', '1', 'Enable analysis tab');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_export_tab', 'no', 'General','2013-07-04 11:50:00', '1', '1', 'Enable export tab');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_active_archiving_tab', 'no', 'General', '2013-07-04 11:50:00', '1', '1', 'Enable active archiving tab');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_dynamic_archiving_tab', 'no', 'General', '2013-07-04 11:50:00', '1', '1', 'Enable active archiving tab');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('enable_retention_tab', 'no', 'General', '2013-07-04 11:50:00', '1', '1', 'Enable retention tab');
INSERT INTO `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) VALUES('enable_scheduling_tab', 'no', 'General', '2013-07-08 11:50:00', '1', '1', 'Enable scheduling tab');
insert into `system_code` (`codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values('sh_license','af98cff45c7dbeddc7a516ccbf44d3c2661316325ebf31259400b6c315351a191eaa968b6d408baa39f159d52ecfd23c','General','2013-09-10 18:24:42','1','1','license str');
INSERT INTO `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) VALUES ('LazyExport', 'true', 'General', '2013-07-08 11:50:00', '1', '1', 'Lazy Export');
INSERT INTO `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) VALUES ('LazyArchiving', 'true', 'General', '2013-07-08 11:50:00', '1', '1', 'Lazy Archiving');

INSERT INTO `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) VALUES ('enable_thumbnails_search', 'no', 'General', '2013-10-23 11:50:00', '1', '1', 'Enable thumbnails on search');
INSERT INTO `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) VALUES ('enable_thumbnails_stub', 'no', 'General', '2013-10-23 11:50:00', '1', '1', 'Enable thumbnails on Stub');
INSERT INTO `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) VALUES ('enable_thumbnails_summary', 'no', 'General', '2013-10-23 11:50:00', '1', '1', 'Enable thumbnails on Summary');
INSERT INTO `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) VALUES ('enable_thumbnails_list', 'no', 'General', '2013-10-23 11:50:00', '1', '1', 'Enable thumbnails on list');
INSERT INTO `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) VALUES ('show_path_in_search_results', 'no', 'General', '2013-10-11 05:43:00', '1', '1', 'Show file path in search results');
INSERT INTO `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) VALUES ('show_excerpt_in_search_results', 'no', 'General', '2013-10-11 05:43:00', '1', '1', 'Show excerpt in search results');
INSERT INTO `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) VALUES ('include_linked_files', 'no', 'General', '2013-11-25 05:43:00', '1', '1', 'Show linked files in search');


insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('enable_default_email_client','no','General','2013-11-12 11:49:16','1','1','Enable Default Client for Email');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('calculate_folder_size','no','General','2013-011-29 02:38:00','1','1','Calculate folder size on FE');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('max_no_of_versions','5','General','2013-11-21 12:28:00','1','1','Max No. of versions to display');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('enable_cifs_file_sharing','no','General','2013-12-26 11:38:00','1','1','Enable CIFS File Sharing');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('copy_target_file_to_archive','yes','General','2013-12-26 11:38:00','1','1','Copy Target File to archive');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('maxArchiveTries','3','ZuesAgent','2014-01-06 11:38:00','1','1','Number of reTries for failed archivals');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('MAX_BATCH_SIZE','2000','General','2014-01-22 04:39:00','1','1','Max batch size');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('enable_file_sharing_security','no','General','2014-02-03 13:43:00','1','1','Enable security on links');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('BUFF_SIZE','8192','General','2014-02-10 04:39:00','1','1','Archiving Buffer Size (Read)');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('BUFF_SIZE_W','16392','General','2014-02-10 04:49:00','1','1','Restore Buffer Size (Write)');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('emulate_email_sender','no','General','2014-02-26 14:06:00','1','1','Emulate sender in file sharing');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('company_name','','Company','2014-03-05 02:54:00','1','1','Company name');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('company_prefered_name','','Company','2014-03-05 02:54:00','1','1','Preferred Name');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('company_address','','Company','2014-03-05 02:54:00','1','1','Company Address');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('company_telephone','','Company','2014-03-05 02:54:00','1','1','Telephone');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('company_support_contact','','Company','2014-03-05 02:54:00','1','1','Support Contact ');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('company_support_email','','Company','2014-03-05 02:54:00','1','1','Support Email');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('restrict_cifs_files_sharing_ad','no','General','2014-03-18 00:00:00','1','1','Restrict AD groups file sharing(cifs)');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('restrict_archive_files_sharing_ad','no','General','2014-03-18 00:00:00','1','1','Restrict AD groups file sharing (archive)');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('shared_link_validity','7','General','2014-03-18 00:00:00','1','1','Shared link valid upto');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('guardian_from_address','','General','2014-03-20 00:00:00','1','1','Guardian from address');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('server_warning_limit','90','General','2014-03-31 00:00:00','1','1','Server warning limit');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('server_critical_limit','95','General','2014-03-31 00:00:00','1','1','Server Critical limit');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('enable_withoutStub_tab','no','General','2014-04-24 00:00:00','1','1','Enable Archive and Delete tab');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('enable_repo_search','no','General','2014-06-09 00:00:00','1','1','Enable Repo search');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('enable_cifs_search','no','General','2014-06-09 00:00:00','1','1','Enable Cifs search');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('enable_file_explorer_for_admin_cm','no','General','2014-06-27 00:00:00','1','1','Enable FExplorer for admin in compatiabiity mode');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('password_expiry_time','30','AccountPolicy','2014-07-04 00:00:00','1','1','Enable Password Timeout (Days)');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('no_of_tries_befor_account_lock','3','AccountPolicy','2014-07-04 00:00:00','1','1','No of tries before account lock ');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('account_lock_timeout_period','5','AccountPolicy','2014-07-04 00:00:00','1','1','Password lock time out period (minutes)');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('password_minimum_length','8','AccountPolicy','2014-07-04 00:00:00','1','1','Minimum password length');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('enforce_caps_in_passwords','no','AccountPolicy','2014-07-04 00:00:00','1','1','Enforce CAPS in Passwords');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('enforce_numbers_in_passwords','no','AccountPolicy','2014-07-04 00:00:00','1','1','Enforce numbers in Passwords');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('enforce_special_chars_in_passwords','no','AccountPolicy','2014-07-04 00:00:00','1','1','Enforce special chars in Passwords');

insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('path_for_logos','/opt/tomcat/sa_configs/logos','General','2014-07-21 00:00:00','1','1','Path for logos');
insert into `system_code` ( `codename`, `codevalue`, `codetype`, `ETD`, `editable`, `viewable`, `display_name`) values ('path_for_configs','/opt/tomcat/sa_configs','General','2014-07-22 00:00:00','1','1','Path for Configs');



/*Data for the table `repo_stats` */

delete from system_code where codename='application.version.number';

ALTER TABLE `repo_stats` ADD `duplicate_files` BIGINT( 200 ) NOT NULL DEFAULT '0' AFTER `total_files`;

ALTER TABLE job_statistics MODIFY job_end_time DATETIME DEFAULT NULL;
ALTER TABLE job_statistics MODIFY skipped_processfile_skipped_hiddenlected BIGINT(20) DEFAULT '0';
ALTER TABLE job_statistics MODIFY skipped_processfile_skipped_isdir BIGINT(20) DEFAULT '0';
ALTER TABLE job_statistics MODIFY skipped_processfile_skipped_mismatch_age BIGINT(20) DEFAULT '0';
ALTER TABLE job_statistics MODIFY skipped_processfile_skipped_mismatch_extension BIGINT(20) DEFAULT '0';

ALTER TABLE job_statistics MODIFY skipped_processfile_skipped_mismatch_lastaccesstime BIGINT(20) DEFAULT '0';
ALTER TABLE job_statistics MODIFY skipped_processfile_skipped_mismatch_lastmodifiedtime BIGINT(20) DEFAULT '0';
ALTER TABLE job_statistics MODIFY skipped_processfile_skipped_read_only BIGINT(20) DEFAULT '0';
ALTER TABLE job_statistics MODIFY skipped_processfile_skipped_symbollink BIGINT(20) DEFAULT '0';
ALTER TABLE job_statistics MODIFY skipped_processfile_skipped_temporary BIGINT(20) DEFAULT '0';

ALTER TABLE job_statistics MODIFY skipped_processfile_skipped_too_large BIGINT(20) DEFAULT '0';
ALTER TABLE job_statistics MODIFY total_archived BIGINT(20) DEFAULT '0';
ALTER TABLE job_statistics MODIFY total_duplicated BIGINT(20) DEFAULT '0';


ALTER TABLE job_statistics MODIFY total_evaluated BIGINT(20) DEFAULT '0';
ALTER TABLE job_statistics MODIFY total_failed_archiving BIGINT(20) DEFAULT '0';
ALTER TABLE job_statistics MODIFY total_failed_deduplication BIGINT(20) DEFAULT '0';

ALTER TABLE job_statistics MODIFY total_failed_evaluating BIGINT(20) DEFAULT '0';
ALTER TABLE job_statistics MODIFY total_failed_stubbing BIGINT(20) DEFAULT '0';
ALTER TABLE job_statistics MODIFY total_stubbed BIGINT(20) DEFAULT '0';

ALTER TABLE `job_statistics` ADD `already_archived` BIGINT( 20 ) NOT NULL DEFAULT '0' AFTER `total_archived` ;

CREATE TABLE `share_analysis_stats` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `share_id` INT(11) NOT NULL COMMENT 'share_mapping id',
  `document_cat_name` VARCHAR(200) NOT NULL COMMENT 'document_category name',
  `total_volume` BIGINT(20) UNSIGNED DEFAULT 0 COMMENT 'accumulated size in mbs',
  `three_months_older_vol` BIGINT(20) DEFAULT 0 COMMENT 'accumulated size of files older than 3 months in mbs',
  `six_months_older_vol` BIGINT(20) DEFAULT 0 COMMENT 'accumulated size of files older than 6 months in mbs',
  `one_year_older_vol` BIGINT(20) DEFAULT 0,
  `two_year_older_vol` BIGINT(20) DEFAULT 0,
  `three_year_older_vol` BIGINT(20) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `FK_share_analysis_stats` (`share_id`),
  CONSTRAINT `FK_share_analysis_stats` FOREIGN KEY (`share_id`) REFERENCES `drive_letters` (`ID`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE `page_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shiro_role_id` bigint(20) DEFAULT NULL,
  `page_uri` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_SHIRO_ROLE_PAGE_RIGHTS` (`shiro_role_id`),
  CONSTRAINT `FK_SHIRO_ROLE_PAGE_RIGHTS` FOREIGN KEY (`shiro_role_id`) REFERENCES `shiro_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `files_processed_size` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total_processed_files` bigint(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

ALTER TABLE  `drive_letters`  ADD COLUMN `warning_limit` INT DEFAULT '50' ;
ALTER TABLE  `drive_letters`  ADD COLUMN `critical_limit` INT DEFAULT '90';
ALTER TABLE  `drive_letters`  ADD COLUMN `analyze_share` BOOL DEFAULT FALSE;
ALTER TABLE `drive_letters`   ADD COLUMN  `trigger_job_on_threshhold` BOOL DEFAULT FALSE;
/*Data for table `page_rights`*/

INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'AddAgent.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'AddDocumentCategory.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'AddDocumentType.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'AddDriveLetter.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'AddJob.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'AddPolicy.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'AddUser.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'Agents.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'ChangePassword.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'ChangePassword.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'defaultTemplate.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'DocumentCategories.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'DocumentTypes.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'DriveLetters.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'EditAgent.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'EditDriveLetter.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'EditExportJob.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'EditGlobalConfig.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'EditJob.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'EditLdap.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'EditLDAPConfig.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'EditPermissions.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'EditPolicy.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'EditUser.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'Error.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'extPageTemplate.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'extPageTemplate2.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'FileExplorer.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'GlobalCodes.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'Jobs.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'JobStatus.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'LDAPCodes.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'ManageSystem.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'Policies.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'showDocumentCategory.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'showDocumentType.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'statistics.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'Status.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'System.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'SystemCodes.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'Users.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'ViewAgent.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'ViewDriveLetter.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'ViewJob.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'ViewPolicy.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'ViewUser.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'EditCompanyConfig.faces');

INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'Browse.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'stubTemplate.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'StubAccess.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'Search.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'FExplorerExtContent.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'SearchContent.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'Help.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'RestoreStub.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'UserPreferences.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'UserFeatures.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'PdfViewer.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'PdfViewer.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (1, 'Error.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (2, 'Error.faces');

INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (3, 'Error.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (3, 'EmailDocumentAccess.faces');
INSERT INTO page_rights(shiro_role_id, page_uri) VALUES (3, 'ChangePassword.faces');


INSERT INTO page_rights (shiro_role_id ,page_uri)VALUES ('1', 'DelPolicy.faces');
INSERT INTO page_rights (shiro_role_id ,page_uri)VALUES ('1', 'AddDelPolicy.faces');
INSERT INTO page_rights (shiro_role_id ,page_uri)VALUES ('1', 'EditDelPolicy.faces');
INSERT INTO page_rights (shiro_role_id ,page_uri)VALUES ('1', 'DelJob.faces');
INSERT INTO page_rights (shiro_role_id ,page_uri)VALUES ('1', 'AddDelJob.faces');
INSERT INTO page_rights (shiro_role_id ,page_uri)VALUES ('1', 'EditDelJobs.faces');
INSERT INTO page_rights (shiro_role_id ,page_uri)VALUES ('1', 'ViewDJob.faces');

INSERT INTO page_rights (shiro_role_id ,page_uri)VALUES ('1', 'AccountPolicy.faces');
INSERT INTO page_rights (shiro_role_id ,page_uri)VALUES ('2', 'UserFeatures.faces');
INSERT INTO page_rights (shiro_role_id ,page_uri)VALUES ('1', 'ToolsDownloader.faces');
						
/* release 1.9 changes */

ALTER TABLE share_analysis_stats CHANGE three_months_older_vol  LM_3 BIGINT(20) NULL;
ALTER TABLE share_analysis_stats CHANGE six_months_older_vol  LM_6 BIGINT(20) NULL;
ALTER TABLE share_analysis_stats CHANGE one_year_older_vol  LM_12 BIGINT(20) NULL;
ALTER TABLE share_analysis_stats CHANGE two_year_older_vol  LM_24 BIGINT(20) NULL;
ALTER TABLE share_analysis_stats CHANGE three_year_older_vol  LM_36 BIGINT(20) NULL;

ALTER TABLE share_analysis_stats ADD COLUMN LA_3  BIGINT(20) NULL;
ALTER TABLE share_analysis_stats ADD COLUMN LA_6  BIGINT(20) NULL;
ALTER TABLE share_analysis_stats ADD COLUMN LA_12  BIGINT(20) NULL;
ALTER TABLE share_analysis_stats ADD COLUMN LA_24  BIGINT(20) NULL;
ALTER TABLE share_analysis_stats ADD COLUMN LA_36  BIGINT(20) NULL;

ALTER TABLE share_analysis_stats ADD COLUMN CD_3  BIGINT(20) NULL;
ALTER TABLE share_analysis_stats ADD COLUMN CD_6  BIGINT(20) NULL;
ALTER TABLE share_analysis_stats ADD COLUMN CD_12  BIGINT(20) NULL;
ALTER TABLE share_analysis_stats ADD COLUMN CD_24  BIGINT(20) NULL;
ALTER TABLE share_analysis_stats ADD COLUMN CD_36  BIGINT(20) NULL;
/* role features table */
CREATE TABLE `role_features`( 
    `id` INT NOT NULL AUTO_INCREMENT ,
    `name` VARCHAR(200) NOT NULL , 
    `display_name` VARCHAR(400) NULL , 
    `admin_allowed` BOOL DEFAULT FALSE , 
    `priv_allowed` BOOL DEFAULT FALSE,
    `ldap_allowed` BOOL DEFAULT FALSE , 
    PRIMARY KEY (`id`) ) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
ALTER IGNORE TABLE `role_features` ADD CONSTRAINT UNIQUE KEY `UNIQUE_NAME` (`name`);  

insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('email_link_sp','Email document link - Search page','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('email_doc_sp','Email document as attachment -  Search page','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('download_sp','Download icon - Search page','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('restore_file_sp','Restore Icon - Search page','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('media_streaming_sp','Media streaming Icon - Search page','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('email_link','Email document link - File explorer','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('email_doc','Email document as attachment - File explorer','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('download','Download Icon - File explorer','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('restore_file','Restore Icon - File explorer','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('restore_folder','Restore folder - File explorer','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('media_streaming','Media streaming - File explorer','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('document_viewing','Online doc viewer icon - File explorer','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('document_viewing_sp','Online doc viewer icon - Search page','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('content_type_search_sp','Content type search','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('document_tagging','Document tagging','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('path_based_search_sp','Path based search','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('show_tags_sp','Show tags in search','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('archive_date_search_sp','Archive Date search','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('show_document_url','Show Complete document URL','0','0','0');
insert into `role_features` (`name`, `display_name`, `admin_allowed`, `priv_allowed`, `ldap_allowed`) values('search_by_id_sp','Enable Search by UUID','0','0','0');

CREATE TABLE `link_access_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recipient` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `sender` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

CREATE TABLE `link_access_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid_documents` varchar(500) DEFAULT NULL,
  `link_access_users_id` int(11) DEFAULT NULL,
  `shared_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sender` varchar(100) DEFAULT NULL,
  `hash` varchar(100) DEFAULT NULL,
  `identifier` bigint(20) DEFAULT NULL,
  `link_expire_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_LINK_USERS_DOCS` (`link_access_users_id`),
  CONSTRAINT `FK_LINK_USERS_DOCS` FOREIGN KEY (`link_access_users_id`) REFERENCES `link_access_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

CREATE TABLE `data_guardian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_guardian_email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `ldap_groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(100) DEFAULT NULL,
  `restrict_group` int(1) DEFAULT '0',
  `enable_transcript` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

CREATE TABLE `del_policy` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`name` VARCHAR( 50 ) NOT NULL ,
`modified_date` VARCHAR( 255 ) NOT NULL ,
`access_date` VARCHAR( 255 ) NOT NULL ,
`archival_date` VARCHAR( 255 ) NOT NULL ,
`file_type` VARCHAR( 255 ) NOT NULL ,
`file_size` VARCHAR( 50 ) NOT NULL
) ENGINE = InnoDB ;

CREATE TABLE `del_job` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`action_type` VARCHAR( 20 ) NOT NULL ,
`name` VARCHAR( 50 ) NOT NULL ,
`del_policy_id` INT NOT NULL,
KEY `FKdelpolicyondeljob` (`del_policy_id`),
CONSTRAINT `FKdelpolicyondeljob` FOREIGN KEY (`del_policy_id`) REFERENCES `del_policy` (`id`)
) ENGINE = InnoDB ;

CREATE TABLE `del_job_filedetails` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`uuid` VARCHAR( 255 ) NOT NULL ,
`status` VARCHAR( 50 ) NOT NULL ,
`description` VARCHAR( 255 ) NOT NULL,
`del_job_id` INT NOT NULL,
KEY `FKdeljobondeljobfiledetails` (`del_job_id`),
CONSTRAINT `FKdeljobondeljobfiledetails` FOREIGN KEY (`del_job_id`) REFERENCES `del_job` (`id`)
) ENGINE = InnoDB ;

commit;



