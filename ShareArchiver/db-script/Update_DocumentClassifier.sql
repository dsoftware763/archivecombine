DELETE FROM document_type WHERE VALUE = '.htm'  and document_category_id=(select id from document_category where name='Other');
DELETE FROM document_type WHERE VALUE = '.html' and document_category_id=(select id from document_category where name='Other');
DELETE FROM document_type WHERE VALUE = '.gz'   and document_category_id=(select id from document_category where name='Other');
DELETE FROM document_type WHERE VALUE = '.tgz'  and document_category_id=(select id from document_category where name='Other');
DELETE FROM document_type WHERE VALUE = '.tar'  and document_category_id=(select id from document_category where name='Other');
DELETE FROM document_type WHERE VALUE = '.rtf'  and document_category_id=(select id from document_category where name='Other');
DELETE FROM document_type WHERE VALUE = '.txt'  and document_category_id=(select id from document_category where name='Other');

DELETE FROM document_type WHERE VALUE = '.rar' and document_category_id=(select id from document_category where name='Other');
DELETE FROM document_type WHERE VALUE = '.zip' and document_category_id=(select id from document_category where name='Other');

INSERT INTO document_category(description, NAME)VALUES('Data Base Files','Data Base');
INSERT INTO document_category(description, NAME)VALUES('Web files','Web');
INSERT INTO document_category(description, NAME)VALUES('Executable Files','Executable');
INSERT INTO document_category(description, NAME)VALUES('Compressed Files','Compressed');
INSERT INTO document_category(description, NAME)VALUES('Backup Files','Backup');
INSERT INTO document_category(description, NAME)VALUES('Settings Files','Settings');
INSERT INTO document_category(description, NAME)VALUES('Data Files','Data');
INSERT INTO document_category(description, NAME)VALUES('System Files','System');
INSERT INTO document_category(description, NAME)VALUES('Misc Files','Miscellaneous');
INSERT INTO document_category(description, NAME)VALUES('3D image Files','3D image');


INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Text Files'),'Rich Text Format File','.rtf');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Text Files'),'Pages Document','.pages');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Text Files'),'Log File','.log');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Text Files'),'Outlook Mail Message','.msg');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Text Files'),'OpenDocument Text Document','.odt');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Text Files'),'LaTeX Source Document','.tex');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Text Files'),'WordPerfect Document','.wpd');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Text Files'),'Microsoft Works Word Processor Document','.wps');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Text Files'),'Text Document','.txt');

INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Data Base'),'Access 2007 Database File','.accdb');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Data Base'),'Database File','.db');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Data Base'),'Database-File','.dbf');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Data Base'),'Microsoft Access Database','.mdb');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Data Base'),'Program Database','.pdb');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Data Base'),'Structured Query Language Data File','.sql');


INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Web'),'Active Server Page','.asp');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Web'),'Active Server Page Extended File','.aspx');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Web'),'Internet Security Certificate','.cer');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Web'),'ColdFusion Markup File','.cfm');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Web'),'Certificate Signing Request File','.csr');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Web'),'JavaScript File','.js');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Web'),'Java Server Page','.jsp');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Web'),'HP Source Code File','.php');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Web'),'Rich Site Summary','.rss');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Web'),'Hypertext Markup Language File','.htm');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Web'),'Hypertext_Markup Language File','.html');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Web'),'Extensible Hypertext Markup Language File','.xhtml');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Web'),'Cascading Style Sheet','.css');


INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Executable'),'Android Package File','.apk');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Executable'),'Mac OS X Application','.app');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Executable'),'DOS Batch File','.bat');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Executable'),'Common Gateway Interface Script','.cgi');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Executable'),'DOS Command File','.com');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Executable'),'Windows Executable File','.exe');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Executable'),'Java Archive File','.jar');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Executable'),'Program Information File','.pif');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Executable'),'VBScript File','.vb');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Executable'),'Windows Script File','.wsf');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Executable'),'Windows Gadget','.gadget');


INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Compressed'),'7-Zip Compressed File','.7z');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Compressed'),'Comic Book RAR Archive','.cbr');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Compressed'),'Debian Software Package','.deb');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Compressed'),'Mac OS X Installer Package','.pkg');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Compressed'),'Red Hat Package Manager File','.rpm');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Compressed'),'StuffIt Archive','.sit');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Compressed'),'StuffIt X Archive','.sitx');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Compressed'),'Extended Zip File','.zipx');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Compressed'),'Zipped File','.zip');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Compressed'),'Compressed Tarball File','.tar.gz');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Compressed'),'WinRAR Compressed Archive','.rar');

INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Compressed'),'Gzip 1','.gz');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Compressed'),'Gzip 2','.tgz');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Compressed'),'TAR','.tar');

INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Backup'),'VBScript File','.vb');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Backup'),'Backup File','.bak');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Backup'),'Temporary File','.tmp');


INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Settings'),'Configuration File','.cfg');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Settings'),'Windows Initialization File','.ini');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Settings'),'Outlook Profile File','.prf');


INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Data'),'Amazon Kindle eBook File','.azw');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Data'),'Comma Separated Values File','.csv');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Data'),'Data File','.dat');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Data'),'Open eBook File','.epub');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Data'),'Gerber File','.gbr');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Data'),'GEDCOM Genealogy Data File','.ged');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Data'),'Keynote Presentation','.key');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Data'),'PowerPoint Slide Show','.pps');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Data'),'Standard Data File','.sdf');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Data'),'vCard File','.vcf');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Data'),'XML File','.xml');



INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='System'),'Windows Cabinet File','.cab');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='System'),'Windows Control Panel Item','.cpl');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='System'),'Windows Cursor','.cur');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='System'),'Dynamic Link Library','.dll');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='System'),'Windows Memory Dump','.dmp');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='System'),'Device Driver','.drv');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='System'),'Mac OS X Icon Resource File','.icns');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='System'),'Icon File','.ico');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='System'),'Windows File Shortcut','.lnk');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='System'),'Windows System File','.sys');


INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Miscellaneous'),'iCalendar File','.ics');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Miscellaneous'),'Windows Installer Package','.msi');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Miscellaneous'),'Partially Downloaded File','.part');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Miscellaneous'),'Chrome Partially Downloaded File','.crdownload');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Miscellaneous'),'BitTorrent File','.torrent');


INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='3D image'),'Rhino 3D Model','.3dm');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='3D image'),'3D Studio Scene','.3ds');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='3D image'),'3ds Max Scene File','.max');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='3D image'),'Wavefront 3D Object File','.obj');


INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Audio'),'MP3 Audio File','.mp3');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Audio'),'Audio Interchange File Format','.aif');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Audio'),'Interchange File Format','.iif');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Audio'),'Media Playlist File','..m3u');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Audio'),'MPEG-4 Audio File','.m4a');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Audio'),'Real Audio File','.ra');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Audio'),'MPEG-2 Audio File','.mpa');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Audio'),'Advanced Audio Coding File','.aac');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Audio'),'Adobe Flash Protected Audio File','.f4a');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Audio'),'Ogg Vorbis Audio File','.oga');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Audio'),'Ogg_Vorbis Audio File','.ogg');



INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'3GPP2 Multimedia File','.3g2');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'3GPP Multimedia File','.3gp');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'Advanced Systems Format File','.asf');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'Microsoft ASF Redirector File','.asx');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'iTunes Video File','.m4v');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'MPEG-4 Video File','.mp4');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'MPEG Video File','.mpg');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'Real Media File','.rm');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'SubRip Subtitle File','.srt');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'Shockwave Flash Movie','.swf');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'DVD Video Object File','.vob');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'Audio Video Interleave File','.avi');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'Flash MP4 Video File','.f4v');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'3GPP_Multimedia File','.3gp2');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'Apple QuickTime Movie','.mov');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'Ripped Video Data File','.264');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'3GPP Media File','.3gpp');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Video'),'Windows Media Video File','.wmv');


INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Images'),'Adobe Photoshop Document','.psd');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Images'),'Adobe Illustrator File','.ai');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Images'),'Encapsulated PostScript File','.eps');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Images'),'Scalable Vector Graphics File','.svg');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Images'),'PostScript File','.ps');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Images'),'YUV Encoded Image File','.yuv');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Images'),'Thumbnail Image File','.thm');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Images'),'Targa Graphic','.tga');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Images'),'PaintShop Pro Image','.pspimage');
INSERT INTO document_type(document_category_id,NAME, VALUE)VALUES((SELECT id FROM document_category d WHERE d.name='Images'),'DirectDraw Surface','.dds');

commit;

