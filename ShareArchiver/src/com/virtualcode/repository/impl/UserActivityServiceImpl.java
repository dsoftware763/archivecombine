package com.virtualcode.repository.impl;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.log4j.Logger;
import org.springmodules.jcr.JcrTemplate;

import com.virtualcode.common.CustomNodeType;
import com.virtualcode.common.CustomProperty;
import com.virtualcode.common.VCSConstants;
import com.virtualcode.repository.UserActivityService;
import com.virtualcode.services.SystemCodeService;
import com.virtualcode.vo.SystemCode;

public class UserActivityServiceImpl implements UserActivityService {
	
		
	Logger log=Logger.getLogger(UserActivityServiceImpl.class);
	
	SimpleDateFormat sdf=new SimpleDateFormat(VCSConstants.SRC_DATE_FORMAT);
	private String companyName="TestCompany";
	private String USERS_NODE="Users";
	private static int history_counter=5;
    public UserActivityServiceImpl(){
    	
    }
    
   	
	private Node  getCompanyNode() {
         log.debug("getCompanyName: Started");
         Node companyNode=null;          
		 SystemCode systemCode = systemCodeService.getSystemCodeByName("companyName");
				
	    try {
	    	 if(systemCode!=null){
				  this.companyName = systemCode.getCodevalue();
	    	 }else{
	    		 this.companyName=null;
	    	 }
     	
     	    log.debug("getCompanyName: 1");
     	    Node root=session.getRootNode();
            if (root.hasNode(companyName) || session.nodeExists("/" + companyName)) {
                 companyNode = root.getNode(companyName);
           
	             log.debug("Parent Node:"+companyNode.getName());
	             log.debug("Parent Node Path:"+companyNode.getPath());
	            
            
	         } else {
	             companyNode = root.addNode(companyName, "nt:folder");             
	             log.debug(companyName + "not exist: creating it first time with name: " + companyNode.getName());
	         }
	     } catch (RepositoryException ex) {
	         log.error(companyName + " not accessible");
	         companyNode=null;
	     }
 
        return companyNode;
	}
	
	public Node getPathToAddUser() {
        Node rootNode=null;
		try{
	
			 rootNode=getCompanyNode();
			 
			 if (!rootNode.hasNode(USERS_NODE)) {
		            rootNode = rootNode.addNode(USERS_NODE, "nt:folder");
			 }else{
				 rootNode=rootNode.getNode(USERS_NODE);
			 }
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try {
				session.save();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		return rootNode;
	}
       
    public Node userLogin(String userName,String userIP,String httpSessionId,long sessionCreatTime){
    	Node rootNode=null,userNode=null;
 		try{ 	
 			 rootNode = getPathToAddUser();
 			 
 			 if(!rootNode.hasNode(userName)){
 				userNode= rootNode.addNode(userName, CustomNodeType.VC_USER);
 			 }else{
 				userNode=rootNode.getNode(userName);
 			 }
 			 
 			userNode.setProperty(CustomProperty.VC_USER_NAME, userName);
 			userNode.setProperty(CustomProperty.VC_USER_IP, userIP);
 			
 			recordLoginActivity(userNode,getActivityId(userName, httpSessionId,sessionCreatTime));
 			System.out.println("***************** user node path-->"+userNode.getPath());
 			 
 			
 		}catch(Exception ex){
 			ex.printStackTrace();
 		}finally{
 			try {
				session.save();
			}catch(Exception e) {
				e.printStackTrace();
			}
 		}
 		
 		//printAllUsers();
 		
 		return userNode;
    } 
    
    public void userLogout(String userName,String userIP,String httpSessionId,long sessionCreateTime){
    	Node rootNode=null,userNode=null;
 		try{ 	
 			 rootNode = getPathToAddUser();
 			 
 			 if(rootNode.hasNode(userName)){ 				
 				 userNode=rootNode.getNode(userName);
 				recordLogoutActivity(userNode,getActivityId(userName, httpSessionId,sessionCreateTime));
 			 }else{
 				 System.out.println("No User Exists in repository with name "+userName);
 			 }
 			//printAllUsers();
 		}catch(Exception ex){
 			ex.printStackTrace();
 		}
    } 
    
    public void markOrphanSessions(){//called at server startup
    	Node rootNode=null,userNode=null,activityNode=null;
 		try{ 	
 			 rootNode = getPathToAddUser();
 			 NodeIterator usersNodesIte=rootNode.getNodes();
 			 NodeIterator activityNodesIte=null;
 			
 			 while(usersNodesIte!=null && usersNodesIte.hasNext()){
 				userNode=usersNodesIte.nextNode();
 				 
 				activityNodesIte=userNode.getNodes();
 				while(activityNodesIte!=null && activityNodesIte.hasNext()){
 					activityNode=activityNodesIte.nextNode();
 					if(activityNode.hasProperty(CustomProperty.VC_USER_LOGOUT_TIME)){
 						System.out.println("Setting Logout time for user: -->"+userNode.getName());
 						activityNode.setProperty(CustomProperty.VC_USER_LOGOUT_TIME,sdf.format(new Date()));
 					}
 				}
 			 }
 			 
 			
 		}catch(Exception ex){
 			ex.printStackTrace();
 		}
    } 
    
       
    private void recordLoginActivity(Node userNode,String activityId){
    	
    	Node activityNode=null;
    	
    	try{ 	
			 			 
			 if(!userNode.hasNode(activityId)){
				 if(userNode.hasNodes()){
				    NodeIterator activityIte=userNode.getNodes();
				    int count=0;
				    //Date createTime=new Date(),tempTime=null;
				    //String remActName=null;
				    //String createTimeStr;
				    while(activityIte.hasNext()){
				    	
				    	activityNode=activityIte.nextNode();
				    	/*createTimeStr=activityNode.getName();
				    	
				    	if(activityNode.hasProperty(CustomProperty.VC_USER_LOGIN_TIME) && activityNode.getProperty(CustomProperty.VC_USER_LOGIN_TIME).getValue()!=null ){
				    		createTimeStr=activityNode.getProperty(CustomProperty.VC_USER_LOGIN_TIME).getValue().getString();
				    		tempTime=VCSUtil.getDate(createTimeStr,VCSConstants.SRC_DATE_FORMAT);
				    		if(tempTime.before(createTime)){
				    			createTime=tempTime;
				    			remActName=activityNode.getName();
				    		}
				    	}
				    	*/
				    	count++;
				    }
				   // System.out.println("Activities Count-->"+count +"----"+remActName);
				    activityIte=userNode.getNodes();
				    if(count>=history_counter && activityIte.hasNext() ){
				    	activityNode=activityIte.nextNode();
				    	System.out.println("Activity removed.."+activityNode.getProperty(CustomProperty.VC_USER_LOGIN_TIME).getValue().getString());
				    	activityNode.remove();
				    }
				 }
				 activityNode= userNode.addNode(activityId, CustomNodeType.VC_USER_ACTIVITY);
			 }else{
				 activityNode=userNode.getNode(activityId);
			 }
			 			 
			activityNode.setProperty(CustomProperty.VC_USER_LOGIN_TIME, sdf.format(new Date()));
					
			System.out.println("***************** Activity Node path-->"+activityNode.getPath());
			 
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try {
				session.save();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
    }
    
    
    private void recordLogoutActivity(Node userNode,String activityId){
    	
    	Node activityNode=null;
    	
    	try{ 	
			 if(userNode.hasNode(activityId)){
				 activityNode=userNode.getNode(activityId);
				 activityNode.setProperty(CustomProperty.VC_USER_LOGOUT_TIME, sdf.format(new Date()));					
				 System.out.println("***************** User Logout-->"+userNode+":");					 
			 }
			 
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try {
				session.save();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
    }
	
   
    public void recordDocAccessedActivity(String userName,String userIP,String httpSessionId,long sessionCreateTime,String documentPath){
    	
    	System.out.println("Recording Document Access Activity:--->"+"user:"+userName+" --docPath-->"+documentPath);
    	Node rootNode=null,userNode=null,activityNode=null;
 		try{ 	
 			 rootNode = getPathToAddUser();
 			 System.out.println("Recording Document Access Activity:--->2");
 			 if(!rootNode.hasNode(userName)){
 				 userLogin(userName, userIP, httpSessionId, sessionCreateTime);
 				 userNode=rootNode.getNode(userName);
 			 }else{
 				userNode=rootNode.getNode(userName); 
 			 }
 			 if(userNode!=null){
 				System.out.println("Recording Document Access Activity:--->3");
 				
 				String activityId=getActivityId(userName, httpSessionId,sessionCreateTime);
 				if(!userNode.hasNode(activityId)){//theoretically this should never run.
 					System.out.println("User Login Activity Not recorded,but trying to record doc acess.....**********");
 					recordLoginActivity(userNode, activityId);
 					activityNode=userNode.getNode(activityId);
 				     
 				}else{
 					System.out.println("record exists against -->"+activityId);
 					activityNode=userNode.getNode(activityId);
 				}
 				
 				System.out.println("Recording Document Access Activity:--->4");
 					
 				if(activityNode!=null && activityNode.hasProperty(CustomProperty.VC_USER_ACCESSED_DOCS) && activityNode.getProperty(CustomProperty.VC_USER_ACCESSED_DOCS).getValue()!=null){
 					System.out.println("User Activity Node ID--->"+activityId);
 					Property accessedDocP=activityNode.getProperty(CustomProperty.VC_USER_ACCESSED_DOCS);
 	 				Property accessedDocLIdxP=activityNode.getProperty(CustomProperty.VC_USER_ACCESSED_DOCS_L_INDEX);
 	 				
 					String accessedDocPStr=accessedDocP.getValue().getString();
 					String[] accessedDocPArray=accessedDocPStr.split("___");
 					
 					//------
 					List<String> accessedDocPList=new ArrayList<String>();
 					for(int k=0;accessedDocPArray!=null && k<accessedDocPArray.length;k++){
 						if(accessedDocPArray[k]!=null && ! accessedDocPArray[k].equals(""))
 							accessedDocPList.add(accessedDocPArray[k]);
 					}
 					int index=0;
 					
 					if(accessedDocLIdxP.getValue()!=null){
 						index=(int)accessedDocLIdxP.getValue().getLong();
 						index=index%history_counter;
 					}
 					
 					System.out.println("index to add..."+index);
 					if(accessedDocPList.size()>=history_counter){
 						accessedDocPList.set(index,documentPath);
 					}else{
 						accessedDocPList.add(index,documentPath);
 					}
 										 						
 					 accessedDocPStr="";
 					 index++;
 					 
 					 for(int i=0;i<accessedDocPList.size();i++){
 						 accessedDocPStr+=accessedDocPList.get(i).toString()+"___";
 						 System.out.println("accessedDocPList--->"+accessedDocPList.get(i).toString());
 					 }	
 					//-------
 					System.out.println("downloaded doc string "+accessedDocPStr);					  
 					 if(accessedDocPStr!="" && accessedDocPStr.endsWith("___"))
 					    accessedDocPStr=accessedDocPStr.substring(0,accessedDocPStr.length()-3);
					
 					activityNode.setProperty(CustomProperty.VC_USER_ACCESSED_DOCS, accessedDocPStr);
 					activityNode.setProperty(CustomProperty.VC_USER_ACCESSED_DOCS_L_INDEX, index);
					}else{
						System.out.println("recording first activity.. -->"+activityId);
						activityNode.setProperty(CustomProperty.VC_USER_ACCESSED_DOCS, documentPath);
 					    activityNode.setProperty(CustomProperty.VC_USER_ACCESSED_DOCS_L_INDEX, 1);
					}			
 				 	
 				   int count=0;
 				    
 				   if(activityNode.hasProperty(CustomProperty.VC_USER_ACCESSED_DOCS_COUNT)){
	 					Property docsCountP=activityNode.getProperty(CustomProperty.VC_USER_ACCESSED_DOCS_COUNT);
	 					
	 					if(docsCountP.getValue()!=null){
	 					count=(int)docsCountP.getValue().getLong();
	 					
	 					}
 				   }
 					 					
 				  activityNode.setProperty(CustomProperty.VC_USER_ACCESSED_DOCS_COUNT,++count); 					
 			}
 			System.out.println("Recording Document Access Activity:--->5");
 			
 		}catch(Exception ex){
 			ex.printStackTrace();
 		}finally{
 			try {
				session.save();
			}catch(Exception e) {
				e.printStackTrace();
			}
 		} 		
 		
    }  
    
    
    public void recordDownloadActivity(String userName,String userIP,String httpSessionId,long sessionCreateTime,String documentPath){
    	Node rootNode=null,userNode=null,activityNode=null;
 		try{ 	
 			 rootNode = getPathToAddUser();
 			 
 			if(!rootNode.hasNode(userName)){
				 userLogin(userName, userIP, httpSessionId, sessionCreateTime);
				 userNode=rootNode.getNode(userName);
			 }else{
				userNode=rootNode.getNode(userName); 
			 }
			 
 			 if(userNode!=null){
 				 
 				userNode=rootNode.getNode(userName);
 				String activityId=getActivityId(userName, httpSessionId,sessionCreateTime);
 				if(!userNode.hasNode(activityId)){//theoretically this should never run.
 					recordLoginActivity(userNode, activityId);
 					activityNode=userNode.getNode(activityId);
 				}else{
 					activityNode=userNode.getNode(activityId);
 				}
 				
 					
 				if(activityNode!=null && activityNode.hasProperty(CustomProperty.VC_USER_DOWNLOADED_DOCS) && activityNode.getProperty(CustomProperty.VC_USER_DOWNLOADED_DOCS).getValue()!=null){
 					
 					Property downloadedDocP=activityNode.getProperty(CustomProperty.VC_USER_DOWNLOADED_DOCS);
 	 				Property downloadedDocLIdxP=activityNode.getProperty(CustomProperty.VC_USER_DOWNLOADED_DOCS_L_INDEX);
 	 				
 					String downloadedDocPStr=downloadedDocP.getValue().getString();
 					String[] downloadedDocPArray=downloadedDocPStr.split("___");
 					//------
 					List<String> downloadedDocPList=new ArrayList<String>();
 					for(int k=0;downloadedDocPArray!=null && k<downloadedDocPArray.length;k++){
 						if(downloadedDocPArray[k]!=null && ! downloadedDocPArray[k].equals(""))
 							downloadedDocPList.add(downloadedDocPArray[k]);
 					}
 					int index=0;
 					
 					if(downloadedDocLIdxP.getValue()!=null){
 						index=(int)downloadedDocLIdxP.getValue().getLong();
 						index=index%history_counter;
 							 						
 					}
 					
 					System.out.println("index to add..."+index);
 					if(downloadedDocPList.size()>=history_counter){
 						downloadedDocPList.set(index,documentPath);
 					}else{
 						downloadedDocPList.add(index,documentPath);
 					}
 										 						
 					 downloadedDocPStr="";
 					 index++;
 					  for(int i=0;i<downloadedDocPList.size();i++){
 						 downloadedDocPStr+=downloadedDocPList.get(i)+"___";
 					  }	
 					  System.out.println("downloaded doc string "+downloadedDocPStr);
 					//-------
 					  if(downloadedDocPStr!="" && downloadedDocPStr.endsWith("___"))
 						 downloadedDocPStr=downloadedDocPStr.substring(0,downloadedDocPStr.length()-3);
					
 					activityNode.setProperty(CustomProperty.VC_USER_DOWNLOADED_DOCS, downloadedDocPStr);
 					activityNode.setProperty(CustomProperty.VC_USER_DOWNLOADED_DOCS_L_INDEX, index);
					}else{
						activityNode.setProperty(CustomProperty.VC_USER_DOWNLOADED_DOCS, documentPath);
 					    activityNode.setProperty(CustomProperty.VC_USER_DOWNLOADED_DOCS_L_INDEX, 1);
					}			
 				    
 				    int count=0;	
 				    if(activityNode.hasProperty(CustomProperty.VC_USER_DOWNLOADED_DOCS_COUNT)){
	 					Property docsCountP=activityNode.getProperty(CustomProperty.VC_USER_DOWNLOADED_DOCS_COUNT);
	 					if(docsCountP.getValue()!=null){
	 					count=(int)docsCountP.getValue().getLong();
	 					
	 					}
 				    }
 					 					
 					activityNode.setProperty(CustomProperty.VC_USER_DOWNLOADED_DOCS_COUNT,++count); 					
 					
 		   } 			  		
 			
 		}catch(Exception ex){
 			ex.printStackTrace();
 		}finally{
 			try {
				session.save();
			}catch(Exception e) {
				e.printStackTrace();
			}
 		} 		
 		
    }  
    
    
    public void recordSearchActivity(String userName,String userIP,String httpSessionId,long sessionCreateTime,String searchQuery,int searchResCount){
    	Node rootNode=null,userNode=null,activityNode=null;
 		try{ 	
 			 rootNode = getPathToAddUser();
 			 
 			 if(rootNode.hasNode(userName)){
 				 
 				userNode=rootNode.getNode(userName);
 				String activityId=getActivityId(userName, httpSessionId,sessionCreateTime);
 				if(!userNode.hasNode(activityId)){//theoretically this should never run.
 					recordLoginActivity(userNode, activityId);
 					activityNode=userNode.getNode(activityId);
 				}else{
 					activityNode=userNode.getNode(activityId);
 				}
 				
 				
 				if(activityNode.hasProperty(CustomProperty.VC_USER_SEARCH) && activityNode.getProperty(CustomProperty.VC_USER_SEARCH).getValue()!=null){
 					Property searchProperty=activityNode.getProperty(CustomProperty.VC_USER_SEARCH);
 	 				Property searchLIdxP=activityNode.getProperty(CustomProperty.VC_USER_SEARCH_L_INDEX);	
 					
 					String searchPStr=searchProperty.getValue().getString();
	 					String[] searchPArray=searchPStr.split("___");
	 					List<String> searchPList=new ArrayList();
	 					for(int k=0;searchPArray!=null && k<searchPArray.length;k++){
	 						if(searchPArray[k]!=null && ! searchPArray[k].equals(""))
	 						  searchPList.add(searchPArray[k]);
	 					}
	 					int index=0;
	 					
	 					if(searchLIdxP.getValue()!=null){
	 						index=(int)searchLIdxP.getValue().getLong();
	 						index=index%history_counter;
	 							 						
	 					}
	 					
	 					System.out.println("index to add..."+index);
	 					if(searchPList.size()>=history_counter){
	 					  searchPList.set(index,searchQuery+"==="+searchResCount);
	 					}else{
	 						searchPList.add(index,searchQuery+"==="+searchResCount);
	 					}
	 					System.out.println("decode.. searchQuery+resultsCount:-->"+searchPList.toString());	 						
	 					 searchPStr="";
	 					 index++;
	 					  for(int i=0;i<searchPList.size();i++){
	 						 searchPStr+=searchPList.get(i)+"___";
	 					  }	
	 					 System.out.println("after encodeing & concat:-->"+searchPStr);
	 					  if(searchPStr!="" && searchPStr.endsWith("___"))
	 						 searchPStr=searchPStr.substring(0,searchPStr.length()-3);
 					
	 					activityNode.setProperty(CustomProperty.VC_USER_SEARCH, searchPStr);
	 					activityNode.setProperty(CustomProperty.VC_USER_SEARCH_L_INDEX, index);
 					}else{
 						
 						String tempStr=searchQuery+"==="+searchResCount;
 						System.out.println("decode.. searchQuery+resultsCount:-->"+tempStr);
 						activityNode.setProperty(CustomProperty.VC_USER_SEARCH,tempStr );
	 					activityNode.setProperty(CustomProperty.VC_USER_SEARCH_L_INDEX, 1);
	 					
 					}					
 		   
 			 } 			  		
 			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try {
				session.save();
			}catch(Exception e) {
				e.printStackTrace();
			}
		} 		
 		
    }  
    
    
    
    private String getActivityId(String userName,String httpSessionId,long sessionCreationTime){
        System.out.println("username: "+userName+" sessionId:"+httpSessionId+"_"+sessionCreationTime);
    	return userName+"_"+httpSessionId+"_"+sessionCreationTime;
    	
    }
		
	public void printAllUsers(){
		Node node,activityNode;
		try{
			node=session.getRootNode();
			node=node.getNode(companyName+"/"+USERS_NODE+"/");
			NodeIterator iterator=node.getNodes();
			NodeIterator activityIterator=null;
			while(iterator.hasNext()){
				node=iterator.nextNode();
				System.out.println("************** Repo User**********"+node.getPath());
				System.out.println("************** Repo User**********"+node.getName());
				
				activityIterator=node.getNodes();
				
				while(activityIterator.hasNext()){
					activityNode=activityIterator.nextNode();
					
					if(activityNode.hasProperty(CustomProperty.VC_USER_LOGIN_TIME))
					System.out.println(node.getName()+"******Login******** "+activityNode.getProperty(CustomProperty.VC_USER_LOGIN_TIME).getValue().getString());
					
					if(activityNode.hasProperty(CustomProperty.VC_USER_LOGOUT_TIME) && activityNode.getProperty(CustomProperty.VC_USER_LOGOUT_TIME).getValue()!=null)
					System.out.println(node.getName()+"******Logout******** "+activityNode.getProperty(CustomProperty.VC_USER_LOGOUT_TIME).getValue().getString());
					
					if(activityNode.hasProperty(CustomProperty.VC_USER_DOWNLOADED_DOCS) && activityNode.getProperty(CustomProperty.VC_USER_DOWNLOADED_DOCS).getValue()!=null)
					System.out.println(node.getName()+"********Downloaded Docs****** "+activityNode.getProperty(CustomProperty.VC_USER_DOWNLOADED_DOCS).getValue().getString());
					
					if(activityNode.hasProperty(CustomProperty.VC_USER_DOWNLOADED_DOCS_COUNT) && activityNode.getProperty(CustomProperty.VC_USER_DOWNLOADED_DOCS_COUNT).getValue()!=null)
					System.out.println(node.getName()+"******Downloade Docs Count******** "+activityNode.getProperty(CustomProperty.VC_USER_DOWNLOADED_DOCS_COUNT).getValue().getLong());
					
					if(activityNode.hasProperty(CustomProperty.VC_USER_ACCESSED_DOCS) && activityNode.getProperty(CustomProperty.VC_USER_ACCESSED_DOCS).getValue()!=null){
					  System.out.println(node.getName()+"******Accessed Docs ******** "+activityNode.getProperty(CustomProperty.VC_USER_ACCESSED_DOCS).getValue().getString());
					  
					}
					
					if(activityNode.hasProperty(CustomProperty.VC_USER_ACCESSED_DOCS_COUNT) && activityNode.getProperty(CustomProperty.VC_USER_ACCESSED_DOCS_COUNT).getValue()!=null)
					  System.out.println(node.getName()+"******Acc Doc count ******** "+activityNode.getProperty(CustomProperty.VC_USER_ACCESSED_DOCS_COUNT).getValue().getLong());
					
					if(activityNode.hasProperty(CustomProperty.VC_USER_SEARCH) && activityNode.getProperty(CustomProperty.VC_USER_SEARCH).getValue()!=null){
						
						String allSearchStr=activityNode.getProperty(CustomProperty.VC_USER_SEARCH).getValue().getString();
						System.out.println("allSearchStr-->"+allSearchStr);
						String[] allSearchArr=allSearchStr.split("___");
						String[] indivSearchArr;
						String searchStr;
						if(allSearchArr==null){
							searchStr=allSearchStr;
							System.out.println("searchStr(null)-->"+searchStr);
							indivSearchArr=searchStr.split("===");
							if(indivSearchArr!=null && indivSearchArr.length==2)
							System.out.println("******search query ******** "+indivSearchArr[0]+" ----results--->"+indivSearchArr[1]);
							
						}
						else{
							for(int i=0;allSearchArr!=null && i< allSearchArr.length;i++){
								searchStr=allSearchArr[i];
								System.out.println("searchStr(not null)-->"+searchStr);
								indivSearchArr=searchStr.split("===");
								if(indivSearchArr!=null && indivSearchArr.length==2)
								System.out.println("******search query ******** "+indivSearchArr[0]+" ----results--->"+indivSearchArr[1]);
							}
						}
						
						
					}
					
					
				}
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		
	}
    
    
	
	 private void logError(Exception ex) {
        log.error("logError: " + ex.getMessage());

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        log.error(sw.toString()); // stack trace as a string
        
   }
	 	 
	
    private Session session;
	
	private JcrTemplate jcrTemplate;	
	
	public void setJcrTemplate(JcrTemplate jcrTemplate) {
		this.jcrTemplate = jcrTemplate;	
			
		try{
			this.session=jcrTemplate.getSessionFactory().getSession();
		}catch(Exception ex){
			ex.printStackTrace();
		}
			
	}
		
    private SystemCodeService systemCodeService;
    
	public void setSystemCodeService(SystemCodeService systemCodeService) {
		this.systemCodeService = systemCodeService;
		
	}
	
	
	

}
