package com.virtualcode.repository.impl;

import javax.jcr.Session;

import org.springmodules.jcr.JcrTemplate;

import com.virtualcode.repository.FileSizeRunnerService;
import com.virtualcode.repository.monitor.FileSizeRunnerMonitor;
import com.virtualcode.schedular.FileSizeRunner;

public class FileSizeRunnerServiceImpl implements FileSizeRunnerService{

	@Override
	public void startFileSizeRunner() {
		// TODO Auto-generated method stub
		FileSizeRunnerMonitor fileSizeRunnerMonitor = FileSizeRunnerMonitor.getInstance();
		fileSizeRunnerMonitor.setRunning(true);
		fileSizeRunnerMonitor.setStopped(false);
		FileSizeRunner fileSizeRunner = new FileSizeRunner(session);
		Thread runner = new Thread(fileSizeRunner);
		runner.start();
	}

	@Override
	public void stopFileSizeRunner() {
		// TODO Auto-generated method stub
		FileSizeRunnerMonitor fileSizeRunnerMonitor = FileSizeRunnerMonitor.getInstance();
		fileSizeRunnerMonitor.setStopped(true);
		fileSizeRunnerMonitor.setRunning(false);
	}
	
	private Session session;
	private JcrTemplate jcrTemplate;
	
	
	public void setJcrTemplate(JcrTemplate jcrTemplate) {
		this.jcrTemplate = jcrTemplate;	
			
		try{
			session=jcrTemplate.getSessionFactory().getSession();
		}catch(Exception ex){
			ex.printStackTrace();
		}
			
	}

}
