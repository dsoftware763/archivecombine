package com.virtualcode.repository.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.NumericRangeQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.WildcardQuery;

import com.virtualcode.agent.das.dataIndexer.IndexerManager;
import com.virtualcode.agent.das.searcher.SearchEngine;
import com.virtualcode.common.SearchException;
import com.virtualcode.common.StandardError;
import com.virtualcode.repository.AnalysisService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.services.SystemCodeService;
import com.virtualcode.vo.SearchResult;
import com.virtualcode.vo.SystemCode;

public class AnalysisServiceImpl  implements AnalysisService{

	Logger log=Logger.getLogger(SearchServiceImpl.class);
//	private ResourceBundle bundle = ResourceBundle.getBundle("config");
        
	@Override
	public SearchResult searchDocument(String spDocLibID, String authorName,String minSize, String maxSize, 
			String createStartDate, String createEndDate, String modifiedStartDate, String modifiedEndDate, 
			String uncPath, String documentName, String documentExtension, String securitySids, String sortMode, String sortOption, 
			String tags, String targetFolder,String contentType, int currentPage, int itemsPerPage, Integer maxSearchRes) throws Exception {
				
        System.out.println(" ****************      Going to Execute Query: "+new Date()+"  **************************");
   
        SearchResult result;
        

        List<Query> queriesList	=	new ArrayList<Query>();

        if (spDocLibID == null) {//atleast ONE spDocLibID should be selected to search In
            throw new Exception(StandardError.EMPTY_QUERY);
        } else {

            try {
                if (documentName != null && (documentName.trim()).length() > 0) {
                    documentName = documentName.replaceAll("'", "''");
                    queriesList.add(new WildcardQuery(new Term(IndexerManager.Constants.FILE_NAME, documentName)));
                }
                
                if (documentExtension != null && (documentExtension.trim()).length() > 0) {
                	String docExts[]	=	documentExtension.split(",");
                	BooleanQuery boolQuery = new BooleanQuery();
                	for(int c=0; c<docExts.length; c++) {
                		Query q=	new WildcardQuery(new Term(IndexerManager.Constants.FILE_NAME, docExts[c]));
                		boolQuery.add(q, BooleanClause.Occur.SHOULD);
                	}
                	queriesList.add(boolQuery);
                }

                if (uncPath != null && (uncPath.trim()).length() > 3) {
                	
                	uncPath	=	uncPath.substring(2).replace("\\", "/");//skip the first two BK_SLASHES and replace remaining with FWD_SLASHES 
                	uncPath	=	uncPath	+ ((uncPath.endsWith("/"))?"":"/");//append terminating SLASH (if not already exist)
                	
                	String pathTxt	=	"*@"+uncPath+"*";
                	//System.out.println("pathTxt: "+pathTxt);
                    queriesList.add(new WildcardQuery(new Term(IndexerManager.Constants.FILE_PATH, pathTxt)));
                }
                
                if(minSize!=null && minSize.trim().length()>0 && minSize.matches("-?\\d+(\\.\\d+)?")) {

                	if(maxSize!=null && maxSize.trim().length()>0 && maxSize.matches("-?\\d+(\\.\\d+)?")) {//if both size ranges

                		Double miS	=	(Double.parseDouble(minSize))*1024*1024;
                		Double mxS	=	(Double.parseDouble(maxSize))*1024*1024;
                    	queriesList.add(
                    			NumericRangeQuery.newLongRange(
                    					IndexerManager.Constants.FILE_SIZE, 
                    					miS.longValue(), 
                    					mxS.longValue(), 
                    					true, true));                	
                    } else {//only Min Size is specified
                    	
                		Double miS	=	(Double.parseDouble(minSize))*1024*1024;
                    	queriesList.add(	
                    			NumericRangeQuery.newLongRange(
            					IndexerManager.Constants.FILE_SIZE, 
            					miS.longValue(), 
            					null, 
            					true, true));
                    }
                		                	
                } else if(maxSize!=null && maxSize.trim().length()>0 && maxSize.matches("-?\\d+(\\.\\d+)?")) {//only max Size is specified

            		Double mxS	=	(Double.parseDouble(maxSize))*1024*1024;
                	queriesList.add(
                			NumericRangeQuery.newLongRange(
                					IndexerManager.Constants.FILE_SIZE, 
                					null, 
                					mxS.longValue(), 
                					true, true));                	
                }

                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                if (createStartDate != null && (createStartDate.trim()).length() > 0) {//if start date is specified

                    if (createEndDate != null && (createEndDate.trim()).length() > 0) {//if start & End both are specified by user
                    	queriesList.add(
                			NumericRangeQuery.newLongRange(
                					IndexerManager.Constants.DATE_CREATE, 
                					df.parse(createStartDate).getTime(), 
                					df.parse(createEndDate).getTime(), 
                					true, true));
                    	
                    } else {//if only start date is speified by user
                    	queriesList.add(
                    			NumericRangeQuery.newLongRange(
                    					IndexerManager.Constants.DATE_CREATE, 
                    					df.parse(createStartDate).getTime(), 
                    					null, 
                    					true, true));
                    }

                } else if (createEndDate != null && (createEndDate.trim()).length() > 0) {//if only End date is specified
                    queriesList.add(
                			NumericRangeQuery.newLongRange(
                					IndexerManager.Constants.DATE_MODIFY, 
                					null, 
                					df.parse(createEndDate).getTime(), 
                					true, true));
                }
                
                if (modifiedStartDate != null && (modifiedStartDate.trim()).length() > 0) {//if start date is specified

                    if (modifiedEndDate != null && (modifiedEndDate.trim()).length() > 0) {//if start & End both are specified by user
                    	queriesList.add(
                			NumericRangeQuery.newLongRange(
                					IndexerManager.Constants.DATE_MODIFY, 
                					df.parse(modifiedStartDate).getTime(), 
                					df.parse(modifiedEndDate).getTime(), 
                					true, true));
                    	
                    } else {//if only start date is speified by user
                    	queriesList.add(
                    			NumericRangeQuery.newLongRange(
                    					IndexerManager.Constants.DATE_MODIFY, 
                    					df.parse(modifiedStartDate).getTime(), 
                    					null, 
                    					true, true));
                    }

                } else if (modifiedEndDate != null && (modifiedEndDate.trim()).length() > 0) {//if only End date is specified
                    queriesList.add(
                			NumericRangeQuery.newLongRange(
                					IndexerManager.Constants.DATE_MODIFY, 
                					null, 
                					df.parse(modifiedEndDate).getTime(), 
                					true, true));
                }
                
	            if(maxSearchRes<=0) {
	                SystemCodeService systemCodeService=serviceManager.getSystemCodeService();
		       		SystemCode systemCode = systemCodeService.getSystemCodeByName("MAX_ANALYSIS_RESULTS");
					 
					if(systemCode!=null){
						try{
					        maxSearchRes=new Integer(systemCode.getCodevalue());
					     }catch(Exception ex){
					       maxSearchRes=9999999;
					     }
					}
                }
               
                System.out.println("MAX_SEARCH_RESULTS:"+maxSearchRes);
                
                int sortType	=	SortField.STRING_VAL;
                if(sortOption.equals(IndexerManager.Constants.FILE_SIZE))
                	sortType	=	SortField.LONG;
                else if(sortOption.equals(IndexerManager.Constants.DATE_ACCESS) || sortOption.equals(IndexerManager.Constants.DATE_CREATE) || sortOption.equals(IndexerManager.Constants.DATE_MODIFY))
                	sortType	=	SortField.LONG;
                
                boolean sortOrder	=	true;
                if(sortMode.equals("Asc"))
                	sortOrder	=	false;
                
                SearchEngine searchEngine	=	new SearchEngine(spDocLibID, maxSearchRes, sortOption, sortType, sortOrder);
                result	=	searchEngine.performSearch(
                		queriesList,
                		null,//occurList (BooleanClause.Occur.MUST)
                		currentPage, 
                		itemsPerPage);

                System.out.println(" Query execution was successfull ");

            } catch (Exception ex) {
                log.error(ex.toString());               
                throw new SearchException(ex.toString());
            }
        }//else
        System.out.println(" ****************  LUCENE Query Executed: "+new Date()+"  **************************");
        return result;//getFormattedResults(searchResult, statement,null, currentPage, itemsPerPage);	
	}
	
    private ServiceManager serviceManager;	
	public void setServiceManager(ServiceManager serviceManager) {
		this.serviceManager = serviceManager;
	}

}
