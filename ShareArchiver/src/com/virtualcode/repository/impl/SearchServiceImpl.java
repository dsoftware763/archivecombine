package com.virtualcode.repository.impl;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.jcr.query.RowIterator;

import org.apache.log4j.Logger;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.surround.parser.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.NumericRangeQuery;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.util.Version;
import org.bouncycastle.ocsp.RespID;
import org.springmodules.jcr.JcrTemplate;

import com.virtualcode.agent.das.archivePolicy.dto.SPDocLib;
import com.virtualcode.agent.das.dataIndexer.IndexerManager;
import com.virtualcode.agent.das.searcher.SearchEngine;
import com.virtualcode.common.CustomProperty;
import com.virtualcode.common.SearchException;
import com.virtualcode.common.StandardError;
import com.virtualcode.repository.SearchService;
import com.virtualcode.services.DriveLettersService;
import com.virtualcode.services.JobService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.services.SystemCodeService;
import com.virtualcode.util.AdvSearchUtil;
import com.virtualcode.util.ISO8601Utilities;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.SpringApplicationContext;
import com.virtualcode.util.StringUtils;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.DriveLetters;
import com.virtualcode.vo.JobSpDocLib;
import com.virtualcode.vo.SearchDocument;
import com.virtualcode.vo.SearchResult;
import com.virtualcode.vo.SystemCode;
import com.virtualcode.vo.SystemCodeType;

import edu.emory.mathcs.backport.java.util.Arrays;

public class SearchServiceImpl  implements SearchService{

	Logger log=Logger.getLogger(SearchServiceImpl.class);
//	private ResourceBundle bundle = ResourceBundle.getBundle("config");
        
	@Override
	public SearchResult searchDocument(String searchFor, String textPart, String authorName,
			String modifiedBy, String modifiedStartDate,
			String modifiedEndDate, String documentName, String securitySids,
			String sortMode, String sortOption, String tags, String targetFolder,String contentType, int currentPage, int itemsPerPage, Long fileSizeStart, Long fileSizeEnd,
			String archivedStartDate, String archivedEndDate) throws Exception {
		
		javax.jcr.Repository repo=session.getRepository();
		
        System.out.println(" ****************      Going to Execute Query: "+new Date()+"  **************************");
   
        RowIterator searchResult = null;
        String INPUT_DATE_FORMAT = "dd-MM-yyyy";
        SimpleDateFormat inputDF = new SimpleDateFormat(INPUT_DATE_FORMAT);
//        String iso8601Format = "yyyy-MM-dd'T'HH:mm:ssz";
        String strStratModifiedDate = "";
        String strEndModifiedDate = "";
        String strFileSizeStart = "";
        String strFileSizeEnd = "";
        String strStartArchivedDate = "";
        String strEndArchivedDate = "";
       // String queryLanguage = Query.JCR_SQL2;
        String  statement = "SELECT excerpt(.) FROM nt:base WHERE";


        if (textPart == null && authorName == null && modifiedBy == null
                && modifiedStartDate == null && modifiedEndDate == null && documentName == null
                && tags==null && fileSizeStart==null && fileSizeEnd==null) {
            throw new Exception(StandardError.EMPTY_QUERY);
        } else {

            try {
                /**
                 * Checking Repository values can be removed later on
                 */
                Value[] values = repo.getDescriptorValues(repo.QUERY_LANGUAGES);
                for (Value value : values) {
                    System.out.println("Query Language : " + value.getString());
                }

                values = repo.getDescriptorValues(repo.QUERY_JOINS);
                for (Value value : values) {
                    System.out.println("Query JOINS : " + value.getString());
                }

                /**
                 * Preparing Query Statement
                 */
               
                String queryCondition = "";


                if ((textPart != null && (textPart.trim()).length() > 0)) {
                    queryCondition = setQueryCondition(queryCondition, "CONTAINS(., '" + textPart.replaceAll("'", "''") + "')");

                }
                if (documentName != null && (documentName.trim()).length() > 0) {
                    documentName = documentName.replace("*", "%").replaceAll("'", "''");

                    String partialQuery = " (";
                    String docNames[] = documentName.split(",");
                    for (int i = 0; i < docNames.length; i++) {
                        docNames[i] = docNames[i].trim();
                        if (docNames[i].length() > 0 && !(docNames[i].startsWith("-"))) {
                        	partialQuery += " lower(jcr:title) like '" + docNames[i].toLowerCase() + "' OR ";
                        }
                    }
                    if(partialQuery.length()>3){	//error check
	                    partialQuery = partialQuery.substring(0, partialQuery.length() - 3) + " )";
	                    queryCondition = setQueryCondition(queryCondition, partialQuery);
                    }
                    partialQuery = " (";
                    boolean exclude = false;
                    for (int i = 0; i < docNames.length; i++) {
                    	if (docNames[i].length() > 0 && docNames[i].startsWith("-")){
                        	//ignore the first '-'
                        	partialQuery += "NOT lower(jcr:title) like '" + docNames[i].trim().substring(1).toLowerCase() + "' AND ";
                        	exclude = true;
                        }
                    }
                    if(partialQuery.length()>3 && exclude){	//error check                    	
	                    partialQuery = partialQuery.substring(0, partialQuery.length() - 4) + " )";
	                    queryCondition = setQueryCondition(queryCondition, partialQuery);
                    }
                }
                //excluded document type
                /*if (excludedDocumentName != null && (excludedDocumentName.trim()).length() > 0) {
                	excludedDocumentName = excludedDocumentName.replace("*", "%").replaceAll("'", "''");

                    String partialQuery = " (";
                    String docNames[] = excludedDocumentName.split(",");
                    for (int i = 0; i < docNames.length; i++) {
                        docNames[i] = docNames[i].trim();
                        if (docNames[i].length() > 0) {
                        	partialQuery += "NOT lower(jcr:title) like '" + docNames[i].toLowerCase() + "' OR ";
                        }
                    }
                    partialQuery = partialQuery.substring(0, partialQuery.length() - 3) + " )";
                    queryCondition = setQueryCondition(queryCondition, partialQuery);
                }*/
                
                if (authorName != null && (authorName.trim()).length() > 0) {
                    queryCondition = setQueryCondition(queryCondition, "lower(jcr:createdBy) = '" + authorName.replaceAll("'", "''").toLowerCase() + "'");
                }
                if (modifiedBy != null && (modifiedBy.trim()).length() > 0) {
                    queryCondition = setQueryCondition(queryCondition, "lower(jcr:lastModifiedBy) ='" + modifiedBy.replaceAll("'", "''").toLowerCase() + "'");
                }

                //--------------------------------


                if(securitySids!=null && ! securitySids.trim().equals("")){
                    String[] securitySidsArr=securitySids.split(",");
                    String ldapQuery=" ( "
                            + "(";
                    for(int i=0;securitySidsArr!=null&& i<securitySidsArr.length; i++) {
                        ldapQuery   +=  ""
                                + " ("
                                + " lower(" + CustomProperty.VC_ALLOWED_SIDS + ") like '%" + securitySidsArr[i].replaceAll("'", "''").toLowerCase() + "%'"
                                + " AND (";
                                for(int j=0;j<securitySidsArr.length;j++){
                                  ldapQuery+= " lower(" + CustomProperty.VC_S_ALLOWED_SIDS + ") like '%" + securitySidsArr[j].replaceAll("'", "''").toLowerCase() + "%'";
                                  ldapQuery+=" OR";
                                }
                                ldapQuery   =   ldapQuery.substring(0,ldapQuery.length()-3);// to remove last OR
                                ldapQuery+= ")";
                                ldapQuery+= " ) OR";    
                                
                    }
                    
                    ldapQuery   =   ldapQuery.substring(0,ldapQuery.length()-3);//to remove last OR
                    ldapQuery   +=   ")";

                    ldapQuery   +=  "AND (";

                    for(int i=0; securitySidsArr!=null && i<securitySidsArr.length; i++) {
                        ldapQuery   +=  ""
                                + " ("
                                + " NOT (lower(" + CustomProperty.VC_DENIED_SIDS + ") like '%" + securitySidsArr[i].replaceAll("'", "''").toLowerCase() + "%')"
                                + " AND"
                                + " NOT (lower(" + CustomProperty.VC_S_DENIED_SIDS + ") like '%" + securitySidsArr[i].replaceAll("'", "''").toLowerCase() + "%')"
                                + " ) and";
                    }
                    ldapQuery   =   ldapQuery.substring(0,ldapQuery.length()-4);//to remove last AND
                    ldapQuery   +=   ")"
                            + ")";
              
                   queryCondition=setQueryCondition(queryCondition, ldapQuery);
                }
                            

                //--------------------------------------------------
                if (modifiedStartDate != null && (modifiedStartDate.trim()).length() > 0) {
                    try {
                        strStratModifiedDate = ISO8601Utilities.formatDateTime(inputDF.parse(modifiedStartDate));
                        queryCondition = setQueryCondition(queryCondition, "jcr:lastModified >= TIMESTAMP '" + strStratModifiedDate + "'");

                    } catch (Exception ex) {
                        strStratModifiedDate = null;
                        System.out.println(ex);
                        throw new SearchException(StandardError.DATE_PARSING_ERROR);
                    }
                }

                if (modifiedEndDate != null && (modifiedEndDate.trim()).length() > 0) {
                    try {
                        strEndModifiedDate = ISO8601Utilities.formatDateTime(inputDF.parse(modifiedEndDate));
                        queryCondition = setQueryCondition(queryCondition, "jcr:lastModified <= TIMESTAMP '" + strEndModifiedDate + "' ");
                    } catch (Exception ex) {
                        strEndModifiedDate = null;
                        System.out.println(ex);
                        throw new SearchException(StandardError.DATE_PARSING_ERROR);
                    }
                }
                
                if(tags != null && tags.trim().length()>0){
                	tags = tags.replace("*", "%").replaceAll("'", "''").trim();
                	queryCondition =  setQueryCondition(queryCondition,"lower(" + CustomProperty.VC_DOC_TAGS + ")"+ " like '%|%" + tags.replaceAll("'", "''").toLowerCase() + "%'");
                }
                //for searchin a specific folder
                if(targetFolder != null && targetFolder.trim().length()>0){
//                	queryCondition =  setQueryCondition(queryCondition,"ISDESCENDANTNODE('" +targetFolder.replaceAll("'", "''").toLowerCase() + "')");
                	queryCondition =  setQueryCondition(queryCondition,"jcr:path like '" + targetFolder.replaceAll("'", "''") + "/%'");
                }

                if(contentType != null && contentType.trim().length()>0){
               	   queryCondition =  setQueryCondition(queryCondition,"lower("+CustomProperty.VC_SP_DOC_CONTENT_TYPE+") = '" + contentType.toLowerCase().replaceAll("'", "''") +"'");
                }

                
                if("DISPLAY".equalsIgnoreCase(searchFor)) {//if search is performed from UI or WS (not from Removal Job)
	                if(queryCondition != null && queryCondition.length() > 0){
	                	ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
	                	if(ru.getCodeValue("include_linked_files", SystemCodeType.GENERAL).equals("no"))  {              	
	                		queryCondition = setQueryCondition(queryCondition, "vc:linkedToNode IS NULL"); // exclude linked files
	                	}
	                	
	                }
                }
                
                if(queryCondition != null && queryCondition.length() > 0){
               		queryCondition = setQueryCondition(queryCondition, "jcr:primaryType = 'vc:fs_resource'"); // exclude linked files
                }
                
                
	            if(queryCondition != null && queryCondition.length() > 0){
//	               	ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();	               	
	               	queryCondition = setQueryCondition(queryCondition, "(vc:isRemoved IS NULL OR vc:isRemoved ='no')"); // exclude removed files	               	
//	               	queryCondition = setQueryCondition(queryCondition, "vc:isRemoved NOT like 'yes'"); // exclude linked files
	            }
                
                
                System.out.println("0 range of "+fileSizeEnd);
                //--------------------------------------------------
                if (fileSizeStart!=null && fileSizeStart.compareTo(new Long(0)) > 0) {
                	if (fileSizeEnd!=null && fileSizeEnd.compareTo(new Long(1024)) > 0) {
                		System.out.println("1 range of "+fileSizeEnd.toString());
                		queryCondition = setQueryCondition(queryCondition, "vc:fileSize BETWEEN " + fileSizeStart + " AND "+fileSizeEnd);
                	} else {
                		System.out.println("2 range of ");
                		//if end is not specified, than 2000GB is the max file Size
                		Long maxRange		=	9000l*1024*1024*1024;
                		queryCondition = setQueryCondition(queryCondition, "vc:fileSize BETWEEN " + fileSizeStart + " AND "+maxRange);
                	}
                } else if (fileSizeEnd!=null && fileSizeEnd.compareTo(new Long(0)) > 0) {
                	System.out.println("3 range of "+fileSizeEnd.toString());
                	queryCondition = setQueryCondition(queryCondition, "vc:fileSize BETWEEN 0 AND "+fileSizeEnd);
                }
                
                //--------------------------------------------------archive date searchis not working properly.. So commented it...
                if (archivedStartDate != null && (archivedStartDate.trim()).length() > 0) {
                    try {
                        strStartArchivedDate = ISO8601Utilities.formatDateTime(inputDF.parse(archivedStartDate));
                        queryCondition = setQueryCondition(queryCondition, "vc:archiveDate >= TIMESTAMP '" + strStartArchivedDate + "'");
//                        queryCondition = setQueryCondition(queryCondition, "jcr:created IS NULL");

                    } catch (Exception ex) {
                    	strStartArchivedDate = null;
                        System.out.println(ex);
                        throw new SearchException(StandardError.DATE_PARSING_ERROR);
                    }
                }
                
                if (archivedEndDate != null && (archivedEndDate.trim()).length() > 0) {
                    try {
                        strEndArchivedDate = ISO8601Utilities.formatDateTime(inputDF.parse(archivedEndDate));
                        queryCondition = setQueryCondition(queryCondition, "vc:archiveDate <= TIMESTAMP '" + strEndArchivedDate + "' ");
                    } catch (Exception ex) {
                    	strEndArchivedDate = null;
                        System.out.println(ex);
                        throw new SearchException(StandardError.DATE_PARSING_ERROR);
                    }
                } 

                //NumericRangeQuery nrqry = NumericRangeQuery.newLongRange(
    			//		CustomProperty.VC_FILE_SIZE, 
    			//		fileSizeStart, 
    			//		fileSizeEnd, 
    			//		true, true);   
                //Query q = new QueryParser(Version.LUCENE_CURRENT, "title", analyzer);
                
                //queryCondition = setQueryCondition(queryCondition, nrqry.toString());
                
                if (queryCondition != null && queryCondition.length() > 0) {
                    statement += queryCondition;
                    
                    if(sortMode!=null && !sortMode.isEmpty())
                       statement+=" order by "+sortOption+" "+sortMode.toLowerCase();//jcr:title  asc

                } else {
                    throw new SearchException(StandardError.EMPTY_QUERY);
                }

                //statement = "SELECT excerpt(.) FROM nt:resource WHERE jcr:name='"+textPart+"'";
                //statement = "SELECT excerpt(.) FROM nt:resource WHERE   ( lower(jcr:title) like '%'  ) AND vc:linkedToNode IS NULL AND vc:fileSize BETWEEN 1 AND 5242880 order by jcr:title desc";
                
                System.out.println(" Query String: \n" + statement);
                QueryManager qm = null;
                qm = session.getWorkspace().getQueryManager();
                Query qry = qm.createQuery(statement, Query.SQL);
                
                if("DISPLAY".equalsIgnoreCase(searchFor)) {//if search is performed from UI or WS (not from Removal Job)
	                Long maxSearchRes=2000l;
	                
	                 SystemCodeService systemCodeService=serviceManager.getSystemCodeService();
		       		 SystemCode systemCode = systemCodeService.getSystemCodeByName("MAX_SEARCH_RESULTS");
		       		 
		       		 if(systemCode!=null){
		       			try{
		                    maxSearchRes=Long.parseLong(systemCode.getCodevalue());
		                 }catch(Exception ex){
		                   maxSearchRes=2000l;
		                 }
		       		 }
	               
	                System.out.println("MAX_SEARCH_RESULTS:"+maxSearchRes);
	                qry.setLimit(maxSearchRes);
                } // else searchFor = REMOVAL , than search for All by not setting the maxSearchResults limit...
                
                QueryResult r = qry.execute();
                searchResult = r.getRows();
                System.out.println(" Query execution was successfull ");


            } catch (Exception ex) {
                log.error(ex.toString());               
                throw new SearchException(ex.toString());
            }
        }//else
        System.out.println(" ****************  LUCENE Query Executed: "+new Date()+"  **************************");
        return getFormattedResults(searchResult, statement,null, currentPage, itemsPerPage, searchFor);	
	}
	

	@Override
	public SearchResult searchCifsDocument(String spDocLibID, String authorName,String minSize, String maxSize, 
			String createStartDate, String createEndDate, String modifiedStartDate, String modifiedEndDate, 
			String uncPath, String documentName, String documentExtension, String securitySids, String sortMode, String sortOption, 
			String tags, String targetFolder,String contentType, int currentPage, int itemsPerPage, Integer maxSearchRes) throws Exception {
		// TODO Auto-generated method stub
		
		  System.out.println(" ****************      Going to Execute Query: "+new Date()+"  **************************");
		   
	        SearchResult result = null;
	        

	        List<org.apache.lucene.search.Query> queriesList	=	new ArrayList<org.apache.lucene.search.Query>();
	        List<Occur> occurList	=	new ArrayList<Occur>();
//	        String documentExtension = null;	//temporarily set to null
//	        if (spDocLibID == null) {//atleast ONE spDocLibID should be selected to search In
//	            throw new Exception(StandardError.EMPTY_QUERY);
//	        } else {

	            try {
	                if (documentName != null && (documentName.trim()).length() > 0) {
	                    documentName = documentName.replaceAll("'", "''");
	                    queriesList.add(new WildcardQuery(new Term(IndexerManager.Constants.FILE_NAME, documentName)));
	                    occurList.add(BooleanClause.Occur.MUST);
	                }
	                
	                if (documentExtension != null && (documentExtension.trim()).length() > 0) {
	                	String docExts[]	=	documentExtension.split(",");
	                	BooleanQuery boolQuery = new BooleanQuery();
	                	for(int c=0; c<docExts.length; c++) {
	                		org.apache.lucene.search.Query q=	new WildcardQuery(new Term(IndexerManager.Constants.FILE_NAME, docExts[c]));
	                		boolQuery.add(q, BooleanClause.Occur.SHOULD);
	                	}
	                	queriesList.add(boolQuery);
	                	occurList.add(BooleanClause.Occur.MUST);
	                }

	                if (uncPath != null && (uncPath.trim()).length() > 3) {
	                	
	                	uncPath	=	uncPath.substring(2).replace("\\", "/");//skip the first two BK_SLASHES and replace remaining with FWD_SLASHES 
	                	uncPath	=	uncPath	+ ((uncPath.endsWith("/"))?"":"/");//append terminating SLASH (if not already exist)
	                	
	                	String pathTxt	=	"*@"+uncPath+"*";
	                	//System.out.println("pathTxt: "+pathTxt);
	                    queriesList.add(new WildcardQuery(new Term(IndexerManager.Constants.FILE_PATH, pathTxt)));
	                    occurList.add(BooleanClause.Occur.MUST);
	                }
	                if(minSize.trim().equals("0")) minSize=null;
	                if(maxSize.trim().equals("0")) maxSize=null;
	                if(minSize!=null && minSize.trim().length()>0 && minSize.matches("-?\\d+(\\.\\d+)?")) {

	                	if(maxSize!=null && maxSize.trim().length()>0 && maxSize.matches("-?\\d+(\\.\\d+)?")) {//if both size ranges

	                		Double miS	=	(Double.parseDouble(minSize));//*1024*1024; further conversion not needed, size is already converted to bytes
	                		Double mxS	=	(Double.parseDouble(maxSize));//*1024*1024;	                		
	                    	queriesList.add(
	                    			NumericRangeQuery.newLongRange(
	                    					IndexerManager.Constants.FILE_SIZE, 
	                    					miS.longValue(), 
	                    					mxS.longValue(), 
	                    					true, true));    
	                    	occurList.add(BooleanClause.Occur.MUST);
	                    	
	                    } else {//only Min Size is specified
	                    	
	                		Double miS	=	(Double.parseDouble(minSize));//*1024*1024;
	                    	queriesList.add(	
	                    			NumericRangeQuery.newLongRange(
	            					IndexerManager.Constants.FILE_SIZE, 
	            					miS.longValue(), 
	            					null, 
	            					true, true));
	                    	occurList.add(BooleanClause.Occur.MUST);
	                    }
	                		                	
	                } else if(maxSize!=null && maxSize.trim().length()>0 && maxSize.matches("-?\\d+(\\.\\d+)?")) {//only max Size is specified

	            		Double mxS	=	(Double.parseDouble(maxSize));//*1024*1024;
	                	queriesList.add(
	                			NumericRangeQuery.newLongRange(
	                					IndexerManager.Constants.FILE_SIZE, 
	                					null, 
	                					mxS.longValue(), 
	                					true, true));  
	                	occurList.add(BooleanClause.Occur.MUST);
	                }

	                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
	                if (createStartDate != null && (createStartDate.trim()).length() > 0) {//if start date is specified

	                    if (createEndDate != null && (createEndDate.trim()).length() > 0) {//if start & End both are specified by user
	                    	queriesList.add(
	                			NumericRangeQuery.newLongRange(
	                					IndexerManager.Constants.DATE_CREATE, 
	                					df.parse(createStartDate).getTime(), 
	                					df.parse(createEndDate).getTime(), 
	                					true, true));
	                    	occurList.add(BooleanClause.Occur.MUST);
	                    	
	                    } else {//if only start date is speified by user
	                    	queriesList.add(
	                    			NumericRangeQuery.newLongRange(
	                    					IndexerManager.Constants.DATE_CREATE, 
	                    					df.parse(createStartDate).getTime(), 
	                    					null, 
	                    					true, true));
	                    	occurList.add(BooleanClause.Occur.MUST);
	                    }

	                } else if (createEndDate != null && (createEndDate.trim()).length() > 0) {//if only End date is specified
	                    queriesList.add(
	                			NumericRangeQuery.newLongRange(
	                					IndexerManager.Constants.DATE_MODIFY, 
	                					null, 
	                					df.parse(createEndDate).getTime(), 
	                					true, true));
	                    occurList.add(BooleanClause.Occur.MUST);
	                }
	                
	                if (modifiedStartDate != null && (modifiedStartDate.trim()).length() > 0) {//if start date is specified

	                    if (modifiedEndDate != null && (modifiedEndDate.trim()).length() > 0) {//if start & End both are specified by user
	                    	queriesList.add(
	                			NumericRangeQuery.newLongRange(
	                					IndexerManager.Constants.DATE_MODIFY, 
	                					df.parse(modifiedStartDate).getTime(), 
	                					df.parse(modifiedEndDate).getTime(), 
	                					true, true));
	                    	occurList.add(BooleanClause.Occur.MUST);
	                    	
	                    } else {//if only start date is speified by user
	                    	queriesList.add(
	                    			NumericRangeQuery.newLongRange(
	                    					IndexerManager.Constants.DATE_MODIFY, 
	                    					df.parse(modifiedStartDate).getTime(), 
	                    					null, 
	                    					true, true));
	                    	occurList.add(BooleanClause.Occur.MUST);
	                    }

	                } else if (modifiedEndDate != null && (modifiedEndDate.trim()).length() > 0) {//if only End date is specified
	                    queriesList.add(
	                			NumericRangeQuery.newLongRange(
	                					IndexerManager.Constants.DATE_MODIFY, 
	                					null, 
	                					df.parse(modifiedEndDate).getTime(), 
	                					true, true));
	                    occurList.add(BooleanClause.Occur.MUST);
	                }
	                
	                System.out.println("matching sids: " + securitySids);
	                if(securitySids!=null && ! securitySids.trim().equals("")){
	                    String[] securitySidsArr=securitySids.split(",");
	                    BooleanQuery boolQuery = new BooleanQuery();

	                    for(int i=0;securitySidsArr!=null&& i<securitySidsArr.length; i++) {
	                		org.apache.lucene.search.Query q=	(new WildcardQuery(new Term(IndexerManager.Constants.ALLOWED_SIDS, "*"+securitySidsArr[i].trim()+"*")));
	                		boolQuery.add(q, BooleanClause.Occur.SHOULD);
	                	}
	                	queriesList.add(boolQuery);
	                	occurList.add(BooleanClause.Occur.MUST);

	                	boolQuery = new BooleanQuery();
	                	for(int i=0;securitySidsArr!=null&& i<securitySidsArr.length; i++) {
	                		org.apache.lucene.search.Query q=	(new WildcardQuery(new Term(IndexerManager.Constants.SHARE_ALLOWED_SIDS, "*"+securitySidsArr[i].trim()+"*")));
	                		boolQuery.add(q, BooleanClause.Occur.SHOULD);
	                	}
	                	queriesList.add(boolQuery);
	                	occurList.add(BooleanClause.Occur.MUST);

		                boolQuery = new BooleanQuery();	
	                	for(int i=0;securitySidsArr!=null&& i<securitySidsArr.length; i++) {
	                		org.apache.lucene.search.Query q=	(new WildcardQuery(new Term(IndexerManager.Constants.DENY_SIDS, "*"+securitySidsArr[i].trim()+"*")));
	                		boolQuery.add(q, BooleanClause.Occur.SHOULD);
	                	}
	                	queriesList.add(boolQuery);
	                	occurList.add(BooleanClause.Occur.MUST_NOT);
	                	
	                	boolQuery = new BooleanQuery();
	                	for(int i=0;securitySidsArr!=null&& i<securitySidsArr.length; i++) {
	                		org.apache.lucene.search.Query q=	(new WildcardQuery(new Term(IndexerManager.Constants.SHARE_DENY_SIDS, "*"+securitySidsArr[i].trim()+"*")));
	                		boolQuery.add(q, BooleanClause.Occur.SHOULD);
	                	}
	                	queriesList.add(boolQuery);
	                	occurList.add(BooleanClause.Occur.MUST_NOT);
	                }

		            if(maxSearchRes<=0) {
		                SystemCodeService systemCodeService=serviceManager.getSystemCodeService();
			       		SystemCode systemCode = systemCodeService.getSystemCodeByName("MAX_ANALYSIS_RESULTS");
						 
						if(systemCode!=null){
							try{
						        maxSearchRes=new Integer(systemCode.getCodevalue());
						     }catch(Exception ex){
						       maxSearchRes=9999999;
						     }
						}
	                }
	               
	                System.out.println("MAX_SEARCH_RESULTS:"+maxSearchRes);
	                
	                int sortType	=	SortField.STRING_VAL;
	                if(sortOption.equals(IndexerManager.Constants.FILE_SIZE))
	                	sortType	=	SortField.LONG;
	                else if(sortOption.equals(IndexerManager.Constants.DATE_ACCESS) || sortOption.equals(IndexerManager.Constants.DATE_CREATE) || sortOption.equals(IndexerManager.Constants.DATE_MODIFY))
	                	sortType	=	SortField.LONG;
	                
	                boolean sortOrder	=	true;
	                if(sortMode.equals("Asc"))
	                	sortOrder	=	false;
	                if(spDocLibID!=null){
	                
		                SearchEngine searchEngine	=	new SearchEngine(spDocLibID, maxSearchRes, sortOption, sortType, sortOrder);
		                result	=	searchEngine.performSearch(
		                		queriesList,
		                		occurList,
		                		currentPage, 
		                		itemsPerPage);
	                }else{
	                	int totalHits = 0;
	            		DriveLettersService driveLettersService = (DriveLettersService) SpringApplicationContext.getBean("driveLettersService");
	            		JobService jobService = (JobService) SpringApplicationContext.getBean("jobService");
	            		List<DriveLetters> tempList = driveLettersService.getAll();
	            		List<DriveLetters> driveLettersList	=	jobService.getSpDocLibByDriveLetters(tempList);
	            		SearchResult tempResult = null;
	            		List<SearchDocument> docList = new ArrayList<SearchDocument>();
	            		Set<String> spDocLibList = new HashSet<String>();
	            		for(DriveLetters driveLetter: driveLettersList){
	            			spDocLibList.add(driveLetter.getSpDocLibId()+"");
	            		}
	            		List<JobSpDocLib> libsList = jobService.getLibsByNameLike(""); 	// to fetch all analysis job libs
	            		for(JobSpDocLib lib: libsList){
	            			spDocLibList.add(lib.getId()+"");
	            		}
	            		for(String lib:spDocLibList){
//	            			System.out.println(lib);
//	            		for(DriveLetters driveLetter: driveLettersList){
	            			try{
		            			SearchEngine searchEngine	=	new SearchEngine(lib, maxSearchRes, sortOption, sortType, sortOrder);
		            			tempResult	=	searchEngine.performSearch(
				                		queriesList,
				                		occurList,
				                		currentPage, 
				                		itemsPerPage);
		            			if(tempResult!=null){
		            				List<SearchDocument> tempDocList = tempResult.getResults();            				
		            				docList.addAll(tempDocList);
		            				totalHits += tempResult.getTotalRecordsFound();
		            			}
	            			}catch (Exception e) {
								// TODO: handle exception
	            				System.out.println("Exception occured in index:" + lib);
	            				System.out.println("Error: " + e.getMessage());
							}
	            		}
	            		if(docList!=null && docList.size()>0){
	            			result = tempResult;
	            			result.setResults(docList);
	            			result.setTotalRecordsFound(totalHits);	            			
	            		}
	            			
	                }
//	                List<SearchDocument> finalList = result.getResults();
//	                for(SearchDocument doc: finalList)
//	                {
//	                	System.out.println("checking doc: " + doc.getDocumentName() + " with security sids: " + doc.getSecurityAllowSids());
//	                	System.out.println("checking doc: " + doc.getDocumentName() + " with shared sids: " + doc.getShareAllowSids());
//	                	System.out.println("checking doc: " + doc.getDocumentName() + " with deny sids: " + doc.getSecurityDenySids());
//	                	System.out.println("checking doc: " + doc.getDocumentName() + " with deny share sids: " + doc.getShareDenySids());
//	                }
//	                securitySids="s-1";
	                //manually handling results to apply permissions	
	             /*   if(securitySids!=null && securitySids.trim().length()>0){
	                	String[] sidsArr = securitySids.split(",");
	                    boolean authorize = false;
		                List<SearchDocument> finalList = result.getResults();
		                List<SearchDocument> finalAuthorizedList = new ArrayList<SearchDocument>();
		                for(SearchDocument doc: finalList)
		                {
//		                	System.out.println("checking doc: " + doc.getDocumentName() + " with sids: " + doc.getSecurityAllowSids());
//		                	System.out.println("checking doc: " + doc.getDocumentName() + " with sids: " + doc.getShareAllowSids());
//		                	System.out.println("checking doc: " + doc.getDocumentName() + " with sids: " + doc.getSecurityDenySids());
//		                	System.out.println("checking doc: " + doc.getDocumentName() + " with sids: " + doc.getShareDenySids());
		                	
		                	if(doc.getSecurityDenySids()!=null){
		                        boolean checkComplete = false;
		                        for (int i = 0; i < sidsArr.length && !checkComplete; i++) {
		                            String memberGrp = sidsArr[i];
		                            log.debug("Checking for member Group.... :" + memberGrp);
		                            System.out.println("Checking for member Group for deny.... :" + memberGrp);
		                            List notAllowedSids = Arrays.asList(doc.getSecurityDenySids().split(","));
		                            List sharedNotAllowedSids = Arrays.asList(doc.getShareDenySids().split(","));
		                            
		                            // User is member of not AllowedSID Normal OR Shared
		                            if (isMemberOf(notAllowedSids, memberGrp) || isMemberOf(sharedNotAllowedSids, memberGrp)) {
		                                checkComplete = true;
		                                authorize = false;
		                            }
		                        }//for(int i=0;i<userGroup.size() && !checkComplete;i++){
//		                  modified to allow access if any group is found in any allowed SID (one group in shared allow and one in security allow)
		                        boolean found = false;
		                        for (int i = 0; i < sidsArr.length  && !checkComplete; i++) {

		                        	String memberGrp = sidsArr[i];
		                            log.debug("Checking for member Group.... :" + memberGrp);                    
//		                            List allowedSids = (List) sidsLists.get(0);
		                            List sharedAllowedSids = Arrays.asList(doc.getShareAllowSids().split(","));

//		                            if(isMemberOf(allowedSids, memberGrp) || isMemberOf(sharedAllowedSids, memberGrp)){
		                            if (isMemberOf(sharedAllowedSids, memberGrp)) {
		                                found = true;
//		                                authorize = true;
		                                log.debug("one SID found, breaking loop for share allow group: " + memberGrp);                        

		                                break;
		                            }                    
		                        }//for(int i=0;i<userGroup.size() && !checkComplete;i++){
		                        
		                        log.debug("found: " + found);
		                        //System.out.println("found: " + found);
		                        if(!found){
		                             checkComplete = true;
		                        }

		                        for (int i = 0; i < sidsArr.length && !checkComplete; i++) {
		                             String memberGrp = sidsArr[i];
		                            log.debug("Checking for member Group.... :" + memberGrp);                    
		                            List allowedSids = Arrays.asList(doc.getSecurityAllowSids().split(","));
//		                            List sharedAllowedSids = (List) sidsLists.get(2);

//		                            if(isMemberOf(allowedSids, memberGrp) || isMemberOf(sharedAllowedSids, memberGrp)){
		                            if ((found) && isMemberOf(allowedSids, memberGrp)) {                        
		                                log.debug("one SID found, breaking loop for security allow group: " + memberGrp);

		                                checkComplete = true;
		                                authorize = true;
		                            }
		                        }//for(int i=0;i<userGroup.size() && !checkComplete;i++){
		                	}		 
		                	if(authorize)
			                	finalAuthorizedList.add(doc);
		                }
		                result.setResults(finalAuthorizedList);		                
	                }*/
	                
	                

	                System.out.println(" Query execution was successfull ");

	            } catch (Exception ex) {
	                log.error(ex.toString());               
	                throw new SearchException(ex.toString());
	            }
//	        }//else
	        System.out.println(" ****************  LUCENE Query Executed: "+new Date()+"  **************************");
	        return result;//getFormattedResults(searchResult, statement,null, currentPage, itemsPerPage);
	}
	
	@Override
	public SearchDocument searchDocument(String spDocLibID, String documentName, String modifiedStartDate) {
		// TODO Auto-generated method stub
		
		  System.out.println(" ****************      Going to Execute Query: "+new Date()+"  **************************");
		   
	        SearchResult result = null;
	        

	        List<org.apache.lucene.search.Query> queriesList	=	new ArrayList<org.apache.lucene.search.Query>();
//	        String documentExtension = null;	//temporarily set to null
//	        if (spDocLibID == null) {//atleast ONE spDocLibID should be selected to search In
//	            throw new Exception(StandardError.EMPTY_QUERY);
//	        } else {

	            try {
	                if (documentName != null && (documentName.trim()).length() > 0) {
	                    documentName = documentName.replaceAll("'", "''");
	                    queriesList.add(new WildcardQuery(new Term(IndexerManager.Constants.FILE_NAME, documentName)));
	                }
//	                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy"); 12 Aug 2013 05:31 PM
	                SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
	                if (modifiedStartDate != null && (modifiedStartDate.trim()).length() > 0) {//if start date is specified

//	                    if (modifiedEndDate != null && (modifiedEndDate.trim()).length() > 0) {//if start & End both are specified by user
	                    	queriesList.add(
	                			NumericRangeQuery.newLongRange(
	                					IndexerManager.Constants.DATE_MODIFY, 
	                					df.parse(modifiedStartDate).getTime(), 
	                					null, 
	                					true, true));
	                    	
//	                    } else {//if only start date is speified by user
//	                    	queriesList.add(
//	                    			NumericRangeQuery.newLongRange(
//	                    					IndexerManager.Constants.DATE_MODIFY, 
//	                    					df.parse(modifiedStartDate).getTime(), 
//	                    					null, 
//	                    					true, true));
////	                    }

	                } 
	                String sortOption = IndexerManager.Constants.DATE_MODIFY;
	                int sortType	=	SortField.STRING_VAL;
//	                if(sortOption.equals(IndexerManager.Constants.FILE_SIZE))
//	                	sortType	=	SortField.LONG;
//	                else 
	                if(sortOption.equals(IndexerManager.Constants.DATE_ACCESS) || sortOption.equals(IndexerManager.Constants.DATE_CREATE) || sortOption.equals(IndexerManager.Constants.DATE_MODIFY))
	                	sortType	=	SortField.LONG;
	                
	                boolean sortOrder	=	false;
//	                if(sortMode.equals("Asc"))
//	                	boolean sortOrder	=	false;
	                
	                SearchEngine searchEngine	=	new SearchEngine(spDocLibID, 2000, sortOption, sortType, sortOrder);
	                result	=	searchEngine.performSearch(
	                		queriesList,
	                		null,//occurList (BooleanClause.Occur.MUST)
	                		1, 
	                		100);
	                
	                if(result!=null && result.getResults().size()>0)
	                	return result.getResults().get(0);
	            }catch (Exception e) {
					// TODO: handle exception
	            	System.out.println("Exception occured: " + e.getMessage());
	            	
				}
		return null;
	}	
	
    private boolean isMemberOf(List sidList, String sidToSearch) {

        boolean found = false;
        try {
            Collections.sort(sidList);

            found = ((Collections.binarySearch(sidList, sidToSearch) < 0) ? false : true);
        } catch (Exception ex) {
            log.error("Error while searching SID in List " + ex);
        }

        return found;
    }
	
	private SearchResult getFormattedResults(RowIterator rowIterator,String searchQuery,String secureInfo, int currentPage, int itemsPerPage, String searchFor){		
		
		List<SearchDocument> searchDocList = new ArrayList<SearchDocument>();
        SearchResult searchResult = new SearchResult();
        DateFormat dateFormat = SimpleDateFormat.getDateInstance(SimpleDateFormat.LONG);
               
        String excerptString;
        javax.jcr.query.Row row;
        javax.jcr.Node node;
        javax.jcr.Node resource;
        SearchDocument searchDoc;
        String size =null;
        String sizeToDisplay =null;
               
        if(rowIterator==null){
        	return null;
        }
        System.out.println(" ****************  START Getting Contents From Repository: "+new Date()+"  **************************");
        int ctr	=	0;
        int startLimit	=	(currentPage-1)*itemsPerPage;
        int endLimit	=	startLimit+itemsPerPage;
//        System.out.println(rowIterator.getSize());
		while (rowIterator.hasNext()) {
             try {
            	 ctr	=	ctr+1;
            	 if(ctr==1) {//if its first row of found indexes
            		 if(currentPage>1) {//if its not the first page
            			 rowIterator.skip(startLimit);
            			 ctr	=	startLimit;
            			 //System.out.println("skip "+startLimit+" and ctr becomes "+ctr);
            			 continue;
            		 }
            		 
            	 } else if(ctr>endLimit) {//iterate upto end of indexes, to count actual number of indexed results...
            		 rowIterator.nextRow();
            		 //System.out.println("skipping ctr="+ctr);
            		 continue;
            	 }
            	 //System.out.println("processing ctr="+ctr);
            	 
                 row = rowIterator.nextRow();
                 node = row.getNode().getParent();
                 resource = row.getNode();
                 System.out.println("parent: " + resource.getProperty("jcr:primaryType").getString() + ", " + resource.getPath());
//                 System.out.println("counter " + ctr);
                 //String fileName=resource.getProperty(Property.JCR_TITLE).getString();
//                 System.out.println(node.getProperty("jcr:path"));
//                 System.out.println("-------------------------------"+node.getProperty(Property.JCR_TITLE));                 
                 String fileName = node.getName();
//                 String nodeType = node.getPrimaryNodeType().getName();
//                 if(nodeType.equalsIgnoreCase("nt:folder")){
////                	 System.out.println("node is a folder, decreasing count -- " + fileName);
//                	 ctr = ctr-1;
//                	 continue;
//                 }
                	 
                
                if (resource.hasProperty(CustomProperty.VC_FILE_SIZE)) {
                    double length = resource.getProperty(CustomProperty.VC_FILE_SIZE).getLong();
                    size = String.valueOf(Math.round(Math.ceil(length / 1024d))) ;
                    DecimalFormat df = new DecimalFormat("#.##");
                    
                    length = length / 1024d;
                    sizeToDisplay = df.format(length)+" KB";
                    //convert to MBs
                    if(length>1024d){
                   	 length = length / 1024d;
                   	sizeToDisplay = df.format(length)+" MB";
                    }
                  //convert to GBs
                    if(length>1024d){
                   	 length = length / 1024d;
                   	sizeToDisplay = df.format(length)+" GB";
                    }
                    
                } else if (resource.hasProperty("jcr:data")) {
                    double length = resource.getProperty("jcr:data").getLength();
                    size = String.valueOf(Math.round(Math.ceil(length / 1024d))) ;
                    DecimalFormat df = new DecimalFormat("#.##");
                    
                    length = length / 1024d;
                    sizeToDisplay = df.format(length)+" KB";
                    //convert to MBs
                    if(length>1024d){
                   	 length = length / 1024d;
                   	 sizeToDisplay = df.format(length)+" MB";
                    }
                  //convert to GBs
                    if(length>1024d){
                   	 length = length / 1024d;
                   	 sizeToDisplay = df.format(length)+" GB";
                    }
                }
                
                 
                 //added to show correct size for linked node
                 /*
                 if(node.getNode(Property.JCR_CONTENT).hasProperty(CustomProperty.VC_LINKED_TO)) {//if Custom Linked Node
	                   Property prop    =   node.getNode(Property.JCR_CONTENT).getProperty(CustomProperty.VC_LINKED_TO);
	                   String refNodeID =   prop.getString();
	                   
	                   Node refNode =  session.getNodeByIdentifier(refNodeID);                   
	                   
	                //    innerIt = refNode.getNodes("jcr:content");
	                 //   while (innerIt != null && innerIt.hasNext()) {
	                        
	                        Node innerNode = refNode;//innerIt.nextNode();
	                        if (innerNode.hasProperty("jcr:data")) {
	                            System.out.println(innerNode.getName() + " , contains JCR:DATA Property So calculating Size & MimeType");	                            
	                            size = "";

	                            double length = innerNode.getProperty("jcr:data").getLength();
	                            size = String.valueOf(Math.round(Math.ceil(length / 1000d)));

	                        }//if (innerNode.hasProperty("jcr:data"))
	                    //}
	               } */

                 ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
                 
                 searchDoc = new SearchDocument();
                 searchDoc.setDocumentName(fileName);
                 
                 if("DISPLAY".equalsIgnoreCase(searchFor)) {//if search is performed from UI or WS (not from Removal Job)
	                 String showPathInSearchResults = ru.getCodeValue("show_path_in_search_results", SystemCodeType.GENERAL);
	                 if(showPathInSearchResults!=null && showPathInSearchResults.equals("yes")){                	 
	                	 searchDoc.setDocumentPath(VCSUtil.getUNCPath(node.getPath()));	                	 
//	                	 System.out.println(searchDoc.getRepoPath()+"------------------------");
	                 }else{
	                	 searchDoc.setDocumentPath(node.getPath());
	                 }
	                 searchDoc.setRepoPath(node.getPath());
	                 
                 } else {//in case of Deletion/Retention Job, show the UNC path...
                	 searchDoc.setDocumentPath(VCSUtil.getUNCPath(node.getPath()));
                 }
                 searchDoc.setDocumentSize(size);
                 searchDoc.setDocumentSizeToDisplay(sizeToDisplay);
                 searchDoc.setUid(node.getIdentifier());
                 try{
                	 searchDoc.setCreatedDate(dateFormat.format(resource.getProperty(CustomProperty.VC_CREATION_DATE).getDate().getTime()));
                 }catch(PathNotFoundException ex){
                	 searchDoc.setCreatedDate("");
                 }

                 searchDoc.setArchivedDate(dateFormat.format(node.getProperty(Property.JCR_CREATED).getDate().getTime()));
                 
                 try{
                 searchDoc.setMimeType(resource.getProperty("jcr:mimeType").getString());
                 }catch(Exception ex){};
                 searchDoc.setLastModificationDate(dateFormat.format(resource.getProperty(Property.JCR_LAST_MODIFIED).getDate().getTime()));
                 searchDoc.setModifiedBy(resource.getProperty(Property.JCR_LAST_MODIFIED_BY).getString());
                
                 searchDoc.setDocumentUrl("downloadfile.jsp?nodeID=" + StringUtils.encodeUrl(node.getPath().substring(1)) + "&secureInfo=" + secureInfo);
//                 searchDoc.setDocumentUrl("SearchFileDetails.faces?nodeID=" + StringUtils.encodeUrl(node.getPath().substring(1)) + "&secureInfo=" + secureInfo);
                 String showExcerpt = ru.getCodeValue("show_excerpt_in_search_results", SystemCodeType.GENERAL);
                 if(showExcerpt!=null && showExcerpt.equals("yes")){
	                 excerptString = getValueExcerpt(row, resource);
	                 if (excerptString != null && excerptString.length() > 0) {
	                     searchDoc.setExcerptString(excerptString);
	                 }
                 }
                 if(resource.hasProperty(CustomProperty.VC_DOC_TAGS)){
                     Property prop = resource.getProperty(CustomProperty.VC_DOC_TAGS);
                     String documentTags = prop.getString();
                     log.debug("documentTags " + documentTags);
                     
                     if(documentTags!=null && !documentTags.isEmpty()) {
	                     searchDoc.setTagUser(documentTags.substring(0, documentTags.indexOf("|")));
	//	                    2012-12-20 15:17:47.0
	                     String documentTagTimeStamp = documentTags.substring(documentTags.indexOf("|")+1, documentTags.lastIndexOf("|"));
	                     SimpleDateFormat sdf=new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");	   
	                     Date date = null;
	                     try{
	                    	 date = sdf.parse(documentTagTimeStamp);
	                    	 sdf=new SimpleDateFormat("d MMM yyyy HH:mm:ss");
	                         documentTagTimeStamp = sdf.format(date);
	                     }catch(java.text.ParseException pe){
	                    	 System.out.println(pe);
	//                    	 sdf=new SimpleDateFormat("d MMM yyyy HH:mm:ss");
	//                    	 date = sdf.parse(documentTagTimeStamp);
	                     }
	                     
	                     
	                     searchDoc.setTagTimeStamp(documentTagTimeStamp);
	                     searchDoc.setTags(documentTags.substring(documentTags.lastIndexOf("|")+1));
	                     //{searchController.selectedDocument.tags.substring(0, searchController.selectedDocument.tags.indexOf("|"))}, <b> Added on: </b>#{searchController.selectedDocument.tags.substring(searchController.selectedDocument.tags.indexOf("|")+1, searchController.selectedDocument.tags.lastIndexOf("|"))} , <b>Tag:</b> #{searchController.selectedDocument.tags.substring(searchController.selectedDocument.tags.lastIndexOf("|")+1)}
	                     boolean enablePdfView = VCSUtil.isPdfViewable(searchDoc.getDocumentName().toLowerCase());
                     }
                 }
                 searchDocList.add(searchDoc);
             } catch (Exception exp) {
                 log.warn("Serious Error to look into, File is skiped in Searh: " + exp.getMessage());
                 exp.printStackTrace();
             }
         }
            
		 searchResult.setSearchQuery(searchQuery);
		 
         searchResult.setResults(searchDocList);

         searchResult.setTotalRecordsFound(new Integer(ctr));
         
         System.out.println(" ****************  END Getting Contents From Repository: "+new Date()+"  **************************");
         return searchResult;
				
	}
	
	public RowIterator searchForExport(String query){
		RowIterator searchResults = null;
		System.out.println(" Query String: \n" + query);
        QueryManager qm = null;
        try{
	        qm = session.getWorkspace().getQueryManager();
	        Query q = qm.createQuery(query, Query.SQL);
	        Long maxSearchRes=2000l;
	        
	         SystemCodeService systemCodeService=serviceManager.getSystemCodeService();
	   		 SystemCode systemCode = systemCodeService.getSystemCodeByName("MAX_SEARCH_RESULTS");
	   		 
	   		 if(systemCode!=null){
	   			try{
	                maxSearchRes=Long.parseLong(systemCode.getCodevalue());
	             }catch(Exception ex){
	               maxSearchRes=2000l;
	             }
	   		 }
	       
	        System.out.println("MAX_SEARCH_RESULTS:"+maxSearchRes);
	        q.setLimit(maxSearchRes);
	        
	        QueryResult r = q.execute();
	        searchResults = r.getRows();
	        System.out.println(" Query execution was successfull ");
        }catch(RepositoryException re){
        	System.out.println("error in search for export: " + re.getMessage());
        	re.printStackTrace();
        }
		return searchResults;
		
	}
	
	public List<SearchDocument> searchForReport(String query){
		RowIterator rowIterator = searchForExport(query);		
		return getFormattedResults(rowIterator);
	}
	
	private List<SearchDocument> getFormattedResults(RowIterator rowIterator){
		List<SearchDocument> searchDocList = new ArrayList<SearchDocument>();

        DateFormat dateFormat = SimpleDateFormat.getDateInstance(SimpleDateFormat.LONG);
               

        javax.jcr.query.Row row;
        javax.jcr.Node node;
        javax.jcr.Node resource;
        SearchDocument searchDoc;
        String size =null;
               
        if(rowIterator==null){
        	return null;
        }
        System.out.println(" ****************  START Getting Contents From Repository: "+new Date()+"  **************************");
		while (rowIterator.hasNext()) {
          try {            	 
            	 
                 row = rowIterator.nextRow();
                 node = row.getNode().getParent();
                 resource = row.getNode();

                if (resource.hasProperty("jcr:data")) {
                     double length = resource.getProperty("jcr:data").getLength();
                     size = String.valueOf(Math.round(Math.ceil(length / 1000d))) ;
                 }
                 
                
                 searchDoc = new SearchDocument();
                 searchDoc.setUid(node.getIdentifier());	//            	<field name="uid" class="java.lang.String"/>
                 searchDoc.setDocumentPath(node.getPath());//            	<field name="documentPath" class="java.lang.String"/>
                 searchDoc.setDocumentSize(size); //            	<field name="documentSize" class="java.lang.String"/>
                 
                 try{
                	 searchDoc.setCreatedDate(dateFormat.format(resource.getProperty(CustomProperty.VC_CREATION_DATE).getDate().getTime())); //            	<field name="createdDate" class="java.lang.String"/>
                 }catch(PathNotFoundException ex){
                	 searchDoc.setCreatedDate("");
                 }
                 searchDoc.setArchivedDate(dateFormat.format(node.getProperty(Property.JCR_CREATED).getDate().getTime())); //            	<field name="archivedDate" class="java.lang.String"/>                 
                 searchDoc.setLastModificationDate(dateFormat.format(resource.getProperty(Property.JCR_LAST_MODIFIED).getDate().getTime())); //            	<field name="lastModificationDate" class="java.lang.String"/>
                 
                 searchDocList.add(searchDoc);
             } catch (Exception exp) {
                 log.warn("Serious Error to look into, File is skiped in Searh: " + exp.getMessage());
                 exp.printStackTrace();
             }
         }
         
         System.out.println(" ****************  END Getting Contents From Repository: "+new Date()+"  **************************");
         
         return searchDocList;
	}
	
	 private String getValueExcerpt(javax.jcr.query.Row row, javax.jcr.Node resource) {
	        String excerptString = null;
	        try {
	            Value excerpt = row.getValue("rep:excerpt(.)");
	            excerptString = excerpt.getString().replace(resource.getParent().getName(), "");
	            //System.out.println("before substring-->"+excerptString);
	            excerptString = excerptString.substring(excerptString.indexOf("@") + 1);
	            //System.out.println("after substring-->"+excerptString);
	            excerptString = excerptString.replaceAll("�", "&pound;");
	        } catch (Exception ex) {
	            System.out.println("Error in Getting Value Excerpt");
	        }
	        return excerptString;
	    }
	
	
	 private String setQueryCondition(String conditionStatement, String value) {
	        if (conditionStatement != null && conditionStatement.length() > 0) {
	            return conditionStatement + " AND " + value;
	        } else {
	            return conditionStatement + "  " + value;
	        }
	    }

    private String setSubQueryCondition(String conditionStatement, String value) {
        if (conditionStatement != null && conditionStatement.length() > 0) {
            return conditionStatement + " OR " + value;
        } else {
            return conditionStatement + "  " + value;
        }
    }
	
	
	private Session session;
	
	private JcrTemplate jcrTemplate;
	
	
	public void setJcrTemplate(JcrTemplate jcrTemplate) {
		this.jcrTemplate = jcrTemplate;	
			
		try{
			session=jcrTemplate.getSessionFactory().getSession();
		}catch(Exception ex){
			ex.printStackTrace();
		}
			
	}
	
    private ServiceManager serviceManager;	
    private DriveLettersService driveLettersService;
    private JobService jobService;
    
	public void setServiceManager(ServiceManager serviceManager) {
		this.serviceManager = serviceManager;
		this.driveLettersService = this.serviceManager.getDriveLettersService();
		this.jobService = this.serviceManager.getJobService();
	}

}
