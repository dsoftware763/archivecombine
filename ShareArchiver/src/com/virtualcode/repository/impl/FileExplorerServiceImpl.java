package com.virtualcode.repository.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.faces.context.FacesContext;
import javax.jcr.Binary;
import javax.jcr.ItemNotFoundException;
import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.jcr.nodetype.NodeType;

import jcifs.smb.ACE;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.springmodules.jcr.JcrTemplate;

import com.virtualcode.agent.das.utils.AuthorizationUtil;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.cifs.Acl;
import com.virtualcode.cifs.CifsUtil;
import com.virtualcode.cifs.SingleFileArchiver;
import com.virtualcode.cifs.SmbWriter;
import com.virtualcode.cifs.Synchronizer;
import com.virtualcode.common.AdvancedSecureURL;
import com.virtualcode.common.SecureURL;
import com.virtualcode.common.UncFile;
import com.virtualcode.common.UpdateNodePermissionJob;
import com.virtualcode.common.CustomNodeType;
import com.virtualcode.common.CustomProperty;
import com.virtualcode.common.FileTypeMapper;
import com.virtualcode.common.SID;
import com.virtualcode.common.VCSConstants;
import com.virtualcode.repository.FileExplorerService;
import com.virtualcode.security.impl.VirtualCodeAuthorizatorImpl;
import com.virtualcode.services.DriveLettersService;
import com.virtualcode.services.LinkAccessDocumentsService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.services.SystemCodeService;
import com.virtualcode.util.JespaUtil;
import com.virtualcode.util.RepositoryBrowserUtil;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.StringUtils;
import com.virtualcode.util.VirtualcodeUtil;
import com.virtualcode.utils.SecurityPermissionsUtil;
import com.virtualcode.vo.CustomTreeNode;
import com.virtualcode.vo.DriveLetters;
import com.virtualcode.vo.LinkAccessDocuments;
import com.virtualcode.vo.SystemCode;
import com.virtualcode.vo.SystemCodeType;

public class FileExplorerServiceImpl  implements FileExplorerService{

	Logger log = Logger.getLogger("com.virtualcode.repository.impl.FileExplorerServiceImpl");

	public List<CustomTreeNode> getRootNodes(){
		List<CustomTreeNode> rootNodes=new ArrayList<CustomTreeNode>();
		//		Date timeStart = new Date();
		long startTime = System.currentTimeMillis();
		//		log.debug("get root nodes started ");
		try{			
			Map<String,String> mappingList=getDriveLettersMap();	
			Node root=session.getRootNode();

			SystemCodeService systemCodeService=serviceManager.getSystemCodeService();
			SystemCode systemCode = systemCodeService.getSystemCodeByName("companyName");
			String companyName="TestCompany";
			if(systemCode!=null){
				companyName = systemCode.getCodevalue();
			}


			javax.jcr.NodeIterator firstItr = root.getNodes(companyName);
			for (; (firstItr != null && firstItr.hasNext()); ) { //loop for FS / SP etc...
				javax.jcr.Node firstLvl = firstItr.nextNode();

				javax.jcr.NodeIterator secItr = firstLvl.getNodes();

				for (; (secItr != null && secItr.hasNext()); ) { //loop for ip.ip.ip.ip OR mypc-name
					javax.jcr.Node secLvl = secItr.nextNode();

					System.out.println("2ndLvlNode....--->"+secLvl.getName());
					if(secLvl.getName().equalsIgnoreCase("Users")){
						System.out.println("Ignoring User Level Nodes in root nodes....");
						continue;//move to next level
					}

					javax.jcr.NodeIterator thrdItr = secLvl.getNodes();
					for (; (thrdItr != null && thrdItr.hasNext());) { //loop for SharedNames / Drives...
						javax.jcr.Node thrdLvl = thrdItr.nextNode();
						String serverName   =   thrdLvl.getName();//

						javax.jcr.NodeIterator fourthItr = thrdLvl.getNodes();
						for (; (fourthItr != null && fourthItr.hasNext());) { //loop for SharedNames / Drives...
							javax.jcr.Node fourthLvl = fourthItr.nextNode();
							//flag to confirm if the node is to added in view
							//a node will only be added if it contains at least one file/folder
							boolean addNode = false;

							String nodePath     =   fourthLvl.getPath().substring(1);//Skip the First FWD_SLASH
							String nodeName     =   fourthLvl.getName();
							String hasChilds    =   (fourthLvl.hasNodes())?"1":"0";
							NodeIterator it = fourthLvl.getNodes();
							System.out.println("----------------node: " +  nodeName);
							while(it.hasNext()){
								Node node = it.nextNode();

								System.out.println(node + " of type " + node.getPrimaryNodeType().getName());
								if(node.getPrimaryNodeType().getName().equalsIgnoreCase("nt:folder") 
										|| node.getPrimaryNodeType().getName().equalsIgnoreCase("nt:file")
										|| node.getPrimaryNodeType().getName().equalsIgnoreCase("nt:linkedFile"))
								{
									addNode = true;
									break;
								}
								
								if(node.getName().equalsIgnoreCase("thumbs.db.")){
									addNode = false;
									break;
								}
							}

							if(!addNode) continue;

							System.out.println("Mapping for: "+nodePath+": "+mappingList.toString());

							if(mappingList.containsKey(nodePath.toLowerCase())) {
								String driveLtr =   (String)mappingList.get(nodePath.toLowerCase());
								nodeName    =   driveLtr +" ("+ nodeName + ")";
							} else {
								nodeName    =   nodeName +" ("+ serverName + ")";
							}

							CustomTreeNode customTreeNode=copyNodeAllProperties(fourthLvl, new CustomTreeNode());
							if(nodeName!=null && ! nodeName.trim().equals("")){
								customTreeNode.setName(nodeName);
							}
							rootNodes.add(customTreeNode);

						}
					}

				}
			}


		}catch (Exception e) {
			e.printStackTrace();
		}
		//		log.debug("getRootNodes completed in ....... " + (System.currentTimeMillis() -  startTime));
		return sort(rootNodes);
	}

	public String getStubMetaData(String secureID) {
		String outputXML	=	null;

		try {
			if(secureID!=null && !secureID.isEmpty()) {
				secureID	=	secureID.substring(secureID.lastIndexOf("/")+1);
				System.out.println("SecureID "+secureID);

				SecureURL secureURL = SecureURL.getInstance();
				String nodeID	=	secureURL.getDecryptedURL(secureID);
				nodeID	=	nodeID.substring(nodeID.indexOf("/")+1, nodeID.indexOf("?"));
				System.out.println("nodeID "+nodeID);


				CustomTreeNode node = this.getNodeByUUID(nodeID);
				if(node!=null){
					//nodeID = node.getPath();
					//fileName = nodeID.substring(nodeID.lastIndexOf("/") + 1);

					/*
					 Binary binaryStream =this.downloadFile(nodeID);

					 if(binaryStream!=null){
						 contentLength	=	Math.round(binaryStream.getSize());
					 }*/

					SimpleDateFormat df	=	new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
					long createDate	=	0l;
					long modifDate		=	0l;
					long lAccessDate	=	0l;

					if(node.getCreatedDate()!=null && !node.getCreatedDate().isEmpty()) {
						createDate	=	df.parse(node.getCreatedDate()).getTime();
					}
					if(node.getLastModified()!=null && !node.getLastModified().isEmpty()) {
						modifDate	=	df.parse(node.getLastModified()).getTime();
					}
					if(node.getLastAccessed()!=null && !node.getLastAccessed().isEmpty()) {
						lAccessDate	=	df.parse(node.getLastAccessed()).getTime();
					}

					outputXML	=	"<node id='"+nodeID+"'>" +
							"<createDate>"+createDate+"</createDate>" +
							"<modificationDate>"+modifDate+"</modificationDate>" +
							"<lastAccessDate>"+lAccessDate+"</lastAccessDate>" +
							"<size>"+node.getSizeinKbs()+"</size>" +
							"<mimeType>"+node.getMimeType()+"</mimeType>" +
							"<name>"+StringEscapeUtils.escapeHtml(node.getName())+"</name>" +
							"<owner>"+StringEscapeUtils.escapeHtml(node.getTagUser())+"</owner>" +				 		
							"</node>";
				} else {
					outputXML	=	"Not_Found";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			outputXML	=	"Error_occured";
		}

		return outputXML;
	}

	public Map<String,String> getAllShareNames(){

		Map<String, String> nodes = new HashMap<String, String>();
		try{
			//Map<String,String> mappingList=getDriveLettersMap();	
			Node root=session.getRootNode();

			SystemCodeService systemCodeService=serviceManager.getSystemCodeService();
			SystemCode systemCode = systemCodeService.getSystemCodeByName("companyName");
			String companyName="TestCompany";
			if(systemCode!=null){
				companyName = systemCode.getCodevalue();
			}


			javax.jcr.NodeIterator firstItr = root.getNodes(companyName);
			for (; (firstItr != null && firstItr.hasNext());) { //loop for FS / SP etc...
				javax.jcr.Node firstLvl = firstItr.nextNode();

				javax.jcr.NodeIterator secItr = firstLvl.getNodes();
				for (; (secItr != null && secItr.hasNext());) { //loop for ip.ip.ip.ip OR mypc-name
					javax.jcr.Node secLvl = secItr.nextNode();

					System.out.println("2ndLvlNode....--->"+secLvl.getName());

					if(secLvl.getName().equalsIgnoreCase("Users")){
						System.out.println("Ignoring User Level Nodes in root nodes....");
						continue;//move to next level
					}

					if(secLvl.getName().equalsIgnoreCase("SP")){
						System.out.println("Ignoring SharePoint root nodes....");
						break;
					}
					javax.jcr.NodeIterator thrdItr = secLvl.getNodes();
					for (; (thrdItr != null && thrdItr.hasNext());) { //loop for SharedNames / Drives...
						javax.jcr.Node thrdLvl = thrdItr.nextNode();
						String serverName   =   thrdLvl.getName();//

						javax.jcr.NodeIterator fourthItr = thrdLvl.getNodes();
						for (; (fourthItr != null && fourthItr.hasNext());) { //loop for SharedNames / Drives...
							javax.jcr.Node fourthLvl = fourthItr.nextNode();

							String nodePath     =   fourthLvl.getPath().substring(1);//Skip the First FWD_SLASH
							String nodeName     =   fourthLvl.getName();
							String hasChilds    =   (fourthLvl.hasNodes())?"1":"0";
							String unc = "//";

							String[] elements = StringUtils.getPathElements(nodePath);

							for(int i =2;i<elements.length;i++){
								unc+=elements[i]+"/";
							}
							//System.out.println("Mapping for: "+nodePath+": "+mappingList.toString());

							//                         if(mappingList.containsKey(nodePath.toLowerCase())) {
							//                             String driveLtr =   (String)mappingList.get(nodePath.toLowerCase());
							//                             nodeName    =   driveLtr +" ("+ nodeName + ")";
							//                         } else {
							//                             nodeName    =   nodeName +" ("+ serverName + ")";
							//                         }

							//                         CustomTreeNode customTreeNode=copyNodeAllProperties(fourthLvl, new CustomTreeNode());
							//                         if(nodeName!=null && ! nodeName.trim().equals("")){
							//                        	 customTreeNode.setName(nodeName);
							//                         }
							nodes.put(unc.replaceAll("/", "\\\\"),nodePath);
						}
					}

				}
			}


		}catch (Exception e) {
			e.printStackTrace();
		}
		return nodes;
	}



	public Map<String,String> getDriveLettersMap(){
		Map<String,String> map=new HashMap<String,String>();
		List<DriveLetters> list=driveLettersService.getAll();

		for(int i=0;list!=null && i<list.size();i++){
			String sharePath = list.get(i).getSharePath().toLowerCase();
			if(sharePath.startsWith("\\\\")){
				ResourcesUtil ru = ResourcesUtil.getResourcesUtil();				
				sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase() + "/fs/" + sharePath.substring(2).replaceAll("\\\\", "/");
			}
			if(sharePath.endsWith("/"))
				sharePath = sharePath.substring(0, sharePath.lastIndexOf("/"));
			if(list.get(i).getDriveLetter()!=null && list.get(i).getDriveLetter().trim().length()>0)
				map.put(sharePath, list.get(i).getDriveLetter());	//only add if drive letter is not empty
		}
		return map;
	}

	public CustomTreeNode getCompanyNode(){

		try{
			ResourceBundle bundle = VirtualcodeUtil.getResourceBundle();// ResourceBundle.getBundle("config");
			String companyName = bundle.getString("companyName");
			javax.jcr.Node root = session.getRootNode();
			Node jcrNode=root.getNode(companyName);
			return copyNodeAllProperties(jcrNode, new CustomTreeNode());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public CustomTreeNode getNodeByUUIDOfJcr(String uuid) {
		try{
			if(uuid.indexOf("/")==0)
				uuid=uuid.substring(1);

			Node jcrNode= session.getNodeByIdentifier(uuid);
			return copyNodeAllProperties(jcrNode.getParent(), new CustomTreeNode());
		}catch (Exception e) {
			// not found by identifier
			//e.printStackTrace();
		}
		return null;

	}

	@Override
	public CustomTreeNode getNodeByUUID(String uuid) {
		try{
			if(uuid.indexOf("/")==0)
				uuid=uuid.substring(1);

			Node jcrNode= session.getNodeByIdentifier(uuid);
			return copyNodeAllProperties(jcrNode, new CustomTreeNode());
		}catch (Exception e) {
			// not found by identifier
			//e.printStackTrace();
		}
		return null;

	}


	public CustomTreeNode getNodeByPath(String nodePath) {

		boolean found = false;

		try {

			try{
				if(nodePath.indexOf("/")==0)
					nodePath=nodePath.substring(1);

				Node jcrNode=session.getRootNode();
				jcrNode=jcrNode.getNode(nodePath);
				found = true;
				return copyNodeAllProperties(jcrNode, new CustomTreeNode());

			}catch(Exception ex){

			}

			//if not found then check for case-insensitivity
			if(!found){
				log.info("path not found, checking using case-insensitivity");
				try {
					Node jcrNode=session.getRootNode();

					Node parent = jcrNode;
					String[] pathElements = com.virtualcode.util.StringUtils.getPathElements(nodePath);
					String element = null;
					Node node = null;
					for (int i = 0; i < pathElements.length; i++) {
						element = pathElements[i];
						if (element.length() == 0) {
							continue;
						}
						found = false;
						//element = URLDecoder.decode(element, "utf-8");
						log.debug("Element: " + element);
						if (!parent.hasNode(element)) {
							//if child not exist, then check for Case-Insensitivity first
							boolean exists = false;
							NodeIterator ni = parent.getNodes();
							while (ni.hasNext()) {//compare with all children
								String tempName = ni.nextNode().getName();
								if (element.equalsIgnoreCase(tempName)) {
									element = tempName;
									exists = true;
									break;//terminate the iterations if matched with any
								}
							}

							if (exists) {
								node = parent.getNode(element);
								found = true;
							}else{
								return null;	// not found after case insensitivity
							}
							parent = node;
							System.out.println(parent.getPath());


						} else {
							//log.debug("Node exist");
							node = parent.getNode(element);
							parent = node;
							found = true;
						}
						log.debug("Element after case matching: " + element);
						if(!found)
							return null;
					}
					return copyNodeAllProperties(parent, new CustomTreeNode());
				} catch (RepositoryException e) {
					return null;
				} catch(Exception e){
					e.printStackTrace();
				}
			}

		}catch (Exception e) {
			return null;
		}
		return null;	
	}

	public CustomTreeNode getNodeByPath(String nodePath, boolean version) {

		boolean found = false;

		try {

			try{
				if(nodePath.indexOf("/")==0)
					nodePath=nodePath.substring(1);

				Node jcrNode=session.getRootNode();
				jcrNode=jcrNode.getNode(nodePath);
				found = true;
				return copyNodeFewerProperties(jcrNode, new CustomTreeNode());

			}catch(Exception ex){

			}

			//if not found then check for case-insensitivity
			if(!found){
				log.info("path not found, checking using case-insensitivity");
				try {
					Node jcrNode=session.getRootNode();

					Node parent = jcrNode;
					String[] pathElements = com.virtualcode.util.StringUtils.getPathElements(nodePath);
					String element = null;
					Node node = null;
					for (int i = 0; i < pathElements.length; i++) {
						element = pathElements[i];
						if (element.length() == 0) {
							continue;
						}
						found = false;
						//element = URLDecoder.decode(element, "utf-8");
						log.debug("Element: " + element);
						if (!parent.hasNode(element)) {
							//if child not exist, then check for Case-Insensitivity first
							boolean exists = false;
							NodeIterator ni = parent.getNodes();
							while (ni.hasNext()) {//compare with all children
								String tempName = ni.nextNode().getName();
								if (element.equalsIgnoreCase(tempName)) {
									element = tempName;
									exists = true;
									break;//terminate the iterations if matched with any
								}
							}

							if (exists) {
								node = parent.getNode(element);
								found = true;
							}else{
								return null;	// not found after case insensitivity
							}
							parent = node;
							System.out.println(parent.getPath());


						} else {
							//log.debug("Node exist");
							node = parent.getNode(element);
							parent = node;
							found = true;
						}
						log.debug("Element after case matching: " + element);
						if(!found)
							return null;
					}
					return copyNodeFewerProperties(parent, new CustomTreeNode());
				} catch (RepositoryException e) {
					return null;
				} catch(Exception e){
					e.printStackTrace();
				}
			}

		}catch (Exception e) {
			return null;
		}
		return null;	
	}

	@Override
	public List<CustomTreeNode> getFirstLevelDirctories(String nodePath) {
		//		log.debug("getFirstLevelDirctories started ....................");
		long startTime = System.currentTimeMillis();
		if(nodePath.indexOf("/")==0)
			nodePath=nodePath.substring(1);
		Node jcrNode=null;
		List<CustomTreeNode> list=new ArrayList<CustomTreeNode>();
		try {
			jcrNode=session.getRootNode();
			jcrNode=jcrNode.getNode(nodePath);
			NodeIterator ite=jcrNode.getNodes();

			while(ite.hasNext()){
				jcrNode=ite.nextNode();
				if(jcrNode.getPrimaryNodeType().getName().equalsIgnoreCase("nt:folder")){
					list.add(copyNodeAllProperties(jcrNode, new CustomTreeNode()));
				}
			}

		} catch (RepositoryException e) {		
			e.printStackTrace();
		}
		//		log.debug("getFirstLevelDirctories completed in :" + (System.currentTimeMillis()-startTime));
		return sort(list);

	}


	@Override
	public List<CustomTreeNode> getFirstLevelNodes(String nodePath) {
		if(nodePath.indexOf("/")==0)
			nodePath=nodePath.substring(1);
		Node jcrNode=null;
		List<CustomTreeNode> list=new ArrayList<CustomTreeNode>();
		try {
			jcrNode=session.getRootNode();
			jcrNode=jcrNode.getNode(nodePath);
			NodeIterator ite=jcrNode.getNodes();
			CustomTreeNode customTreeNode;
			RepositoryBrowserUtil repoBrwUtil;
			FileTypeMapper fileTypeMapper;
			while(ite.hasNext()){
				jcrNode=ite.nextNode();
				String nodeType = jcrNode.getPrimaryNodeType().getName();
				boolean isFileNode = nodeType.equalsIgnoreCase("nt:file");
				boolean isFolderNode = nodeType.equalsIgnoreCase("nt:folder");
				boolean isLinkedNode = nodeType.equalsIgnoreCase("nt:linkedFile");
				if(isFileNode || isFolderNode || isLinkedNode){	//avoid adding vc_resource_permission node to the content side
					repoBrwUtil = RepositoryBrowserUtil.getInstance();
					//check if node is removed
					System.out.println("checking if node is removed");
					if(isFileNode){
						Node node = jcrNode.getNode("jcr:content");
						if(node.hasProperty(CustomProperty.VC_IS_REMOVED)){
							Property prop = node.getProperty(CustomProperty.VC_IS_REMOVED);
							System.out.println(prop.getString());
							if(prop.getString().equalsIgnoreCase("yes")){
								continue;
							}
						}						
					}
					
					if(jcrNode.getName().equalsIgnoreCase("thumbs.db")){//avoid adding Thumbs.db
						continue;
					}
					customTreeNode=copyNodeAllProperties(jcrNode, new CustomTreeNode());

					//dont show version docs in Explorer
					if(customTreeNode.getVersionOf()!=null && !customTreeNode.getVersionOf().isEmpty())
						continue;

					fileTypeMapper = repoBrwUtil.getFileTypeMapperFromName(customTreeNode.getName(), jcrNode.getPrimaryNodeType().getName().equalsIgnoreCase("nt:folder"));
					customTreeNode.setIconUrl(fileTypeMapper.getIconName());
					customTreeNode.setFileTypeDesc(fileTypeMapper.getTypeText());
					ResourcesUtil ru = ResourcesUtil.getResourcesUtil();

					if(isFolderNode){
						if(ru.getCodeValue("calculate_folder_size", SystemCodeType.GENERAL)!=null && ru.getCodeValue("calculate_folder_size", SystemCodeType.GENERAL).equals("yes"))
							customTreeNode.setFileFoldersCount(getFileFoldersCount(customTreeNode.getPath()));
					}


					list.add(customTreeNode);
				}
			}

		} catch (RepositoryException e) {			
			e.printStackTrace();
		}
		return sort(list);

	}

	public String getFileFoldersCount(String nodePath){
		if(nodePath.indexOf("/")==0)
			nodePath=nodePath.substring(1);
		Node jcrNode=null;
		int files=0;
		int folders=0;
		try {
			jcrNode=session.getRootNode();
			jcrNode=jcrNode.getNode(nodePath);
			NodeIterator ite=jcrNode.getNodes();

			while(ite.hasNext()){
				jcrNode=ite.nextNode();
				String nodeType = jcrNode.getPrimaryNodeType().getName();
				boolean isFolderNode = nodeType.equalsIgnoreCase("nt:folder");
				boolean isFileNode = nodeType.equalsIgnoreCase("nt:file");                
				boolean isLinkedNode = nodeType.equalsIgnoreCase("nt:linkedFile");

				if(isFolderNode){
					folders++;
				}else if(isFileNode || isLinkedNode){
					files++;
				}else{
					continue;
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		String result="";
		if(folders>0 && files>0){
			result=folders +" folders,"+files +" files";
		}else if(folders>0 && files==0){
			result=folders +" folders";
		}else if(folders==0 && files>0){
			result+=files +" files";
		}else{
			result=null;
		}
		if(files==1){
			result=result.replaceAll("files", "file");
		}
		if(folders==1){
			result=result.replaceAll("folders", "folder");
		}

		return result;
	}


	public double getFolderSize(String nodePath){
		if(nodePath.indexOf("/")==0)
			nodePath=nodePath.substring(1);
		Node jcrNode=null;
		double size=0;
		try {
			jcrNode=session.getRootNode();
			jcrNode=jcrNode.getNode(nodePath);

			NodeIterator ite=jcrNode.getNodes();
			NodeIterator ite2;
			Node innerNode;
			while(ite.hasNext()){
				jcrNode=ite.nextNode();

				ite2=jcrNode.getNodes("jcr:content");
				while(ite2!=null && ite2.hasNext()){

					innerNode=(Node)ite2.next();
					if (innerNode.hasProperty("jcr:data")) {
						size+= innerNode.getProperty("jcr:data").getLength()/(1024d*1024d);
					}else{continue;}
				}				   

			}//node properties loop


		}catch(Exception ex){
			ex.printStackTrace();
		}
		size= (Math.round(size*100.0))/100.0;
		return size;

	}



	private List<CustomTreeNode> sort(List<CustomTreeNode> list){

		Collections.sort(list, new Comparator() {

			public int compare(Object o1, Object o2) {
				CustomTreeNode node1=(CustomTreeNode)o1;
				CustomTreeNode node2=(CustomTreeNode)o2;

				return node1.getName().compareToIgnoreCase(node2.getName());
			}
		});

		return list;

	}


	public CustomTreeNode getParentNode(CustomTreeNode node) {
		try{
			String nodePath=node.getPath();

			if(nodePath.indexOf("/")==0)
				nodePath=nodePath.substring(1);

			Node jcrNode=session.getRootNode();
			jcrNode=jcrNode.getNode(nodePath);
			return  copyNodeAllProperties(jcrNode.getParent(), new CustomTreeNode());

		}catch (Exception e) {
			e.printStackTrace();
		}			

		return null;
	}


	public String getNodeType(Node node) {
		try{
			return node.getPrimaryNodeType().getName();//nt:folder,nt:file
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	private CustomTreeNode copyNodeFewerProperties(javax.jcr.Node node,com.virtualcode.vo.CustomTreeNode customNode){

		if(node==null || customNode==null){
			return null;
		}
		String lastModified=null,documentCreateDate=null,lastAccessedDate=null,archiveDate=null,nodeName=null,nodeType=null,nodePath=null;

		String mimeType=null,fileSize=null;
		NodeIterator innerIt = null;
		Node innerNode = null;
		boolean isAllowedNodeType = true;
		boolean isValidPath = true;
		boolean linkedFile=false;
		String documentTags = null;String documentTagUser = null; String documentTagTimeStamp = null;

		try{

			nodeType = node.getPrimaryNodeType().getName();
			nodePath = node.getPath();
			nodeName=node.getName();
			boolean isFileNode = nodeType.equalsIgnoreCase("nt:file");
			boolean isFolderNode = nodeType.equalsIgnoreCase("nt:folder");
			boolean isLinkedNode = nodeType.equalsIgnoreCase("nt:linkedFile");

			customNode.setName(nodeName);
			customNode.setType(node.getPrimaryNodeType().getName());

			customNode.setPath(nodePath);
			customNode.setUuid(node.getIdentifier());


			if (node.hasProperty(Property.JCR_CREATED)) {
				Property prop = node.getProperty(Property.JCR_CREATED);
				Calendar c = prop.getDate();
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				archiveDate = df.format(c.getTime());
				log.debug("Archived Date " + archiveDate);
			}             

			innerIt = node.getNodes("jcr:content");

			if (!isFolderNode) {

				while (innerIt != null && innerIt.hasNext()) {
					log.debug(nodeName + " , contains JCR:CONTENT Node");
					innerNode = innerIt.nextNode();

					NodeType nti[] = innerNode.getMixinNodeTypes();

					for (int i = 0; i < nti.length; i++) {
						String temp = nti[i].getName();
						if (CustomNodeType.VC_STATUS_RESOURCE.equals(temp)) {//if its a STATS_NODE
							isAllowedNodeType = false;
							break;
						}
					}

					if (innerNode != null && innerNode.hasProperty(Property.JCR_LAST_MODIFIED)) {
						Property prop = innerNode.getProperty(Property.JCR_LAST_MODIFIED);
						Calendar c = prop.getDate();
						SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
						lastModified = df.format(c.getTime());
						log.debug("lastModified On " + lastModified);
					}


					if (innerNode.hasProperty(CustomProperty.VC_CREATION_DATE)) {
						Property prop = innerNode.getProperty(CustomProperty.VC_CREATION_DATE);
						Calendar c = prop.getDate();
						SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
						documentCreateDate = df.format(c.getTime());
						log.debug("document created Date " + documentCreateDate);
					}        
					if (innerNode.hasProperty(CustomProperty.VC_ACCESSED_ON)) {
						Property prop = innerNode.getProperty(CustomProperty.VC_ACCESSED_ON);
						Calendar c = prop.getDate();
						SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
						lastAccessedDate = df.format(c.getTime());
						log.debug("document accessed Date " + lastAccessedDate);
					} 
					
					if(innerNode.hasProperty(CustomProperty.VC_DOC_TAGS)){
						Property prop = innerNode.getProperty(CustomProperty.VC_DOC_TAGS);
						documentTags = prop.getString();
						log.debug("documentTags " + documentTags);
						if(documentTags.contains("|")) {
							documentTagUser = documentTags.substring(0, documentTags.indexOf("|"));
							documentTagTimeStamp = documentTags.substring(documentTags.indexOf("|")+1, documentTags.lastIndexOf("|"));
							//	                    2012-12-20 15:17:47.0
							SimpleDateFormat sdf=new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");	                    
							Date date=null;
							try{
								date = sdf.parse(documentTagTimeStamp);
							}catch(Exception ex){	                    	
							}
							sdf=new SimpleDateFormat("d MMM yyyy HH:mm:ss");
							try{
								documentTagTimeStamp = sdf.format(date);
							}catch(Exception ex){}
							documentTags = documentTags.substring(documentTags.lastIndexOf("|")+1);
						} else {
							documentTags	=	"";
						}
					}


					if (innerNode.hasProperty("jcr:data")) {
						log.debug(innerNode.getName() + " , contains JCR:DATA Property So calculating Size & MimeType");
						mimeType = "";
						fileSize = "";

						if (innerNode.hasProperty("jcr:mimeType")) {
							mimeType = innerNode.getProperty("jcr:mimeType").getString();
						}
						double length = innerNode.getProperty("jcr:data").getLength();
						fileSize = String.valueOf(Math.round(Math.ceil(length / 1024d))) + " KB";

					}//if (innerNode.hasProperty("jcr:data"))

					if (!isAllowedNodeType) {
						return null;//Skip the iteration, if current nodeType is restricted
					}


				}//while(innerIt != null && innerIt.hasNext()) {
			}


			if (isValidPath && (isFolderNode || isFileNode || isLinkedNode)) {
				String rowID = com.virtualcode.util.StringUtils.encodeUrl(nodePath);

				if (isLinkedNode && node.hasProperty(Property.JCR_CONTENT)) {
					Property prop = node.getProperty(Property.JCR_CONTENT);
					String refNodeID = prop.getString();
					Node refNode = session.getNodeByIdentifier(refNodeID);
					nodePath = refNode.getParent().getPath();
					log.debug("nt:linked Node: "+nodePath);

				} else if(isFileNode && node.hasNode(Property.JCR_CONTENT) && node.getNode(Property.JCR_CONTENT).hasProperty(CustomProperty.VC_LINKED_TO)) {//if Custom Linked Node
					Property prop    =   node.getNode(Property.JCR_CONTENT).getProperty(CustomProperty.VC_LINKED_TO);
					String refNodeID =   prop.getString();
					linkedFile=true;
					Node refNode =  session.getNodeByIdentifier(refNodeID);
					nodePath =   refNode.getParent().getPath();
					log.debug("nt:file Custom Linked Node: "+nodePath);
					//    innerIt = refNode.getNodes("jcr:content");
					//   while (innerIt != null && innerIt.hasNext()) {
					log.debug(nodeName + " , contains JCR:CONTENT Node");
					innerNode = refNode;//innerIt.nextNode();
					if (innerNode.hasProperty("jcr:data")) {
						log.debug(innerNode.getName() + " , contains JCR:DATA Property So calculating Size & MimeType");	                            
						fileSize = "";

						double length = innerNode.getProperty("jcr:data").getLength();
						fileSize = String.valueOf(Math.round(Math.ceil(length / 1000d))) + " KB";

					}//if (innerNode.hasProperty("jcr:data"))
					//}
				} else {
					log.debug("File Node Path: "+nodePath);
				}



				RepositoryBrowserUtil repoBrwUtil = RepositoryBrowserUtil.getInstance();
				FileTypeMapper fileTypeMapper = repoBrwUtil.getFileTypeMapperFromName(nodeName, isFolderNode);

				customNode.setFileTypeDesc(fileTypeMapper.getTypeText());
				customNode.setIconUrl(fileTypeMapper.getIconName());
				
				customNode.setTagUser(documentTagUser);
				customNode.setTagTimeStamp(documentTagTimeStamp);
				customNode.setTags(documentTags);

				customNode.setCreatedDate(documentCreateDate);  
				customNode.setArchivedDate(archiveDate);
				customNode.setLastModified(lastModified);
				customNode.setLastAccessed(lastAccessedDate);

				customNode.setDownloadUrl(nodePath);

				customNode.setMimeType(mimeType);
				customNode.setFileSize(fileSize);
				customNode.setLinkedNode(linkedFile);

			}

		}catch (Exception e) {
			e.printStackTrace();
		}
		//System.out.println("Custom Node ---<"+customNode.toString());
		return customNode;
	}



	private CustomTreeNode copyNodeAllProperties(javax.jcr.Node node,com.virtualcode.vo.CustomTreeNode customNode){

		if(node==null || customNode==null){
			return null;
		}
		String lastModified=null,documentCreateDate=null,lastAccessedDate=null,archiveDate=null,nodeName=null,nodeType=null,nodePath=null;
		String sids =null,denySids=null,shareSids = null, denyShareSids=null;
		String documentTags = null;String documentTagUser = null; String documentTagTimeStamp = null;
		String securityAllowGroupNames=null,securityDenyGroupNames=null ,shareAllowGroupNames=null, shareDenyGroupNames=null;
		String mimeType=null,fileSize=null;
		NodeIterator innerIt = null;
		Node innerNode = null;
		boolean isAllowedNodeType = true;
		boolean isValidPath = true;
		boolean linkedFile=false;
		String contentType=null;
		String spDocUrl=null;
		String archivingMode = null;
		List<CustomTreeNode> nextVersions	=	new ArrayList<CustomTreeNode>();
		String versionOf	=	null;
		
		String documentArchiveDate = null;
		
		String printerName = null, machineName = null, username = null, totalPages = null;

		try{

			nodeType = node.getPrimaryNodeType().getName();
			nodePath = node.getPath();
			nodeName=node.getName();
			boolean isFileNode = nodeType.equalsIgnoreCase("nt:file");
			boolean isFolderNode = nodeType.equalsIgnoreCase("nt:folder");
			boolean isLinkedNode = nodeType.equalsIgnoreCase("nt:linkedFile");

			customNode.setName(nodeName);
			customNode.setType(node.getPrimaryNodeType().getName());

			customNode.setPath(nodePath);
			customNode.setUuid(node.getIdentifier());

			if (node.hasProperty(Property.JCR_LAST_MODIFIED)) {
				Property prop = node.getProperty(Property.JCR_LAST_MODIFIED);
				Calendar c = prop.getDate();
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				lastModified = df.format(c.getTime());
				log.debug("lastModified On " + lastModified);
			}

			if (node.hasProperty(Property.JCR_CREATED)) {
				Property prop = node.getProperty(Property.JCR_CREATED);
				Calendar c = prop.getDate();
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				archiveDate = df.format(c.getTime());
				log.debug("Archived Date " + archiveDate);
			}             

			innerIt = node.getNodes("jcr:content");

			if (isFolderNode) {
				if (node.hasNode(CustomNodeType.VC_RESOURCE_PERMISSIONS)) {
					node = node.getNode(CustomNodeType.VC_RESOURCE_PERMISSIONS);
					if (node.hasProperty(CustomProperty.VC_ALLOWED_SIDS)) {
						Property prop = node.getProperty(CustomProperty.VC_ALLOWED_SIDS);
						sids = prop.getString();
						log.debug("Folder AllowedSIDs " + sids);
					}

					if (node.hasProperty(CustomProperty.VC_DENIED_SIDS)) {
						Property prop = node.getProperty(CustomProperty.VC_DENIED_SIDS);
						denySids = prop.getString();
						log.debug("Folder DeniedSIDs " + denySids);
					}

					if (node.hasProperty(CustomProperty.VC_S_ALLOWED_SIDS)) {
						Property prop = node.getProperty(CustomProperty.VC_S_ALLOWED_SIDS);
						shareSids = prop.getString();
						log.debug("Folder AllowedShareSIDs " + shareSids);
					}

					if (node.hasProperty(CustomProperty.VC_S_DENIED_SIDS)) {
						Property prop = node.getProperty(CustomProperty.VC_S_DENIED_SIDS);
						denyShareSids = prop.getString();
						log.debug("Folder DenyShareSIDs " + denyShareSids);
					}
				}
			} else {
				while (innerIt != null && innerIt.hasNext()) {
					log.debug(nodeName + " , contains JCR:CONTENT Node");
					innerNode = innerIt.nextNode();

					NodeType nti[] = innerNode.getMixinNodeTypes();

					for (int i = 0; i < nti.length; i++) {
						String temp = nti[i].getName();
						if (CustomNodeType.VC_STATUS_RESOURCE.equals(temp)) {//if its a STATS_NODE
							isAllowedNodeType = false;
							break;
						}
					}

					if (innerNode != null && innerNode.hasProperty(Property.JCR_LAST_MODIFIED)) {
						Property prop = innerNode.getProperty(Property.JCR_LAST_MODIFIED);
						Calendar c = prop.getDate();
						SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
						lastModified = df.format(c.getTime());
						log.debug("lastModified On " + lastModified);
					}


					if (innerNode.hasProperty(CustomProperty.VC_CREATION_DATE)) {
						Property prop = innerNode.getProperty(CustomProperty.VC_CREATION_DATE);
						Calendar c = prop.getDate();
						SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
						documentCreateDate = df.format(c.getTime());
						log.debug("document created Date " + documentCreateDate);
					}
					
					if (innerNode.hasProperty(CustomProperty.VC_ARCHIVE_DATE)) {
						Property prop = innerNode.getProperty(CustomProperty.VC_ARCHIVE_DATE);
						Calendar c = prop.getDate();
						SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
						documentArchiveDate = df.format(c.getTime());
						log.debug("document archive Date " + documentArchiveDate);
					} 

					if (innerNode.hasProperty(CustomProperty.VC_ACCESSED_ON)) {
						Property prop = innerNode.getProperty(CustomProperty.VC_ACCESSED_ON);
						Calendar c = prop.getDate();
						SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
						lastAccessedDate = df.format(c.getTime());
						log.debug("document last accessed Date " + lastAccessedDate);
					} 

					if (innerNode.hasProperty("jcr:data")) {
						log.debug(innerNode.getName() + " , contains JCR:DATA Property So calculating Size & MimeType");
						mimeType = "";
						fileSize = "";

						if (innerNode.hasProperty("jcr:mimeType")) {
							mimeType = innerNode.getProperty("jcr:mimeType").getString();
						}
						double length = innerNode.getProperty("jcr:data").getLength();
						//convert to KB                        
						//                        length = Math.ceil(length / 1024d);	//1000d changed to 1024d for conversion to KBs
						//                        fileSize = length + " KB";
						//                        //convert to MB
						//                        if(length>1024d){
						//                        	length = Math.ceil(length / 1024d);
						//                        	fileSize = length + " MB";
						//                        }
						//                      //convert to GB
						//                        if(length>1024d){
						//                        	length = Math.ceil(length / 1024d);
						//                        	fileSize = length + " GB";
						//                        }

						DecimalFormat df = new DecimalFormat("#.##");
						customNode.setSizeinKbs(Math.round(length)+"");
						length = length / 1000d;
						fileSize = df.format(length)+" KB";                        

						//convert to MBs
						if(length>1024d){
							length = length / 1024d;
							fileSize = df.format(length)+" MB";
						}
						//convert to GBs
						if(length>1024d){
							length = length / 1024d;
							fileSize = df.format(length)+" GB";
						}




					}//if (innerNode.hasProperty("jcr:data"))

					if (!isAllowedNodeType) {
						return null;//Skip the iteration, if current nodeType is restricted
					}

					if (innerNode.hasProperty(CustomProperty.VC_ALLOWED_SIDS)) {
						Property prop = innerNode.getProperty(CustomProperty.VC_ALLOWED_SIDS);
						sids = prop.getString();
						log.debug("AllowedSIDs " + sids);
					}

					if (innerNode.hasProperty(CustomProperty.VC_DENIED_SIDS)) {
						Property prop = innerNode.getProperty(CustomProperty.VC_DENIED_SIDS);
						denySids = prop.getString();
						log.debug("DeniedSIDs " + denySids);
					}

					if (innerNode.hasProperty(CustomProperty.VC_S_ALLOWED_SIDS)) {
						Property prop = innerNode.getProperty(CustomProperty.VC_S_ALLOWED_SIDS);
						shareSids = prop.getString();
						log.debug("AllowedShareSIDs " + shareSids);
					}

					if (innerNode.hasProperty(CustomProperty.VC_S_DENIED_SIDS)) {
						Property prop = innerNode.getProperty(CustomProperty.VC_S_DENIED_SIDS);
						denyShareSids = prop.getString();
						log.debug("DenyShareSIDs " + denyShareSids);
					}

					if (innerNode.hasProperty(CustomProperty.VC_SP_DOC_CONTENT_TYPE)) {
						Property prop = innerNode.getProperty(CustomProperty.VC_SP_DOC_CONTENT_TYPE);
						contentType = prop.getString();
						log.debug("contentType " + contentType);
					}
					if (innerNode.hasProperty(CustomProperty.VC_SP_URL)) {
						Property prop = innerNode.getProperty(CustomProperty.VC_SP_URL);
						spDocUrl = prop.getString();
						log.debug("spDocUrl " + spDocUrl);
					}

					if(innerNode.hasProperty(CustomProperty.VC_DOC_URL)){
						Property prop = innerNode.getProperty(CustomProperty.VC_DOC_URL);
						System.out.println("--------------------");
						Value[] values = prop.getValues();                    	
						for (Value v : values) {
							System.out.println("Value: " + v.getString());
							log.debug("Value: " + v.getString());
						}
						System.out.println("--------------------");


					}

					if(innerNode.hasProperty(CustomProperty.VC_DOC_TAGS)){
						Property prop = innerNode.getProperty(CustomProperty.VC_DOC_TAGS);
						documentTags = prop.getString();
						log.debug("documentTags " + documentTags);
						if(documentTags.contains("|")) {
							documentTagUser = documentTags.substring(0, documentTags.indexOf("|"));
							documentTagTimeStamp = documentTags.substring(documentTags.indexOf("|")+1, documentTags.lastIndexOf("|"));
							//	                    2012-12-20 15:17:47.0
							SimpleDateFormat sdf=new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");	                    
							Date date=null;
							try{
								date = sdf.parse(documentTagTimeStamp);
							}catch(Exception ex){	                    	
							}
							sdf=new SimpleDateFormat("d MMM yyyy HH:mm:ss");
							try{
								documentTagTimeStamp = sdf.format(date);
							}catch(Exception ex){}
							documentTags = documentTags.substring(documentTags.lastIndexOf("|")+1);
						} else {
							documentTags	=	"";
						}
					}

					if(innerNode.hasProperty(CustomProperty.VC_ARCHIVING_MODE)){
						Property prop = innerNode.getProperty(CustomProperty.VC_ARCHIVING_MODE);
						archivingMode = prop.getString();
						log.debug("archivingMode " + archivingMode);
					}
					
					if(innerNode.hasProperty(CustomProperty.VC_PRINTER_NAME)){
						Property prop = innerNode.getProperty(CustomProperty.VC_PRINTER_NAME);
						printerName = prop.getString();
						log.debug("printerName " + printerName);
					}

					if(innerNode.hasProperty(CustomProperty.VC_MACHINE_NAME)){
						Property prop = innerNode.getProperty(CustomProperty.VC_MACHINE_NAME);
						machineName = prop.getString();
						log.debug("machineName " + machineName);
					}
					
					if(innerNode.hasProperty(CustomProperty.VC_USER_NAME)){
						Property prop = innerNode.getProperty(CustomProperty.VC_USER_NAME);
						username = prop.getString();
						log.debug("printerName " + username);
					}
					
					if(innerNode.hasProperty(CustomProperty.VC_TOTAL_PAGES)){
						Property prop = innerNode.getProperty(CustomProperty.VC_TOTAL_PAGES);
						totalPages = prop.getString();
						log.debug("printerName " + totalPages);
					}
					
					//                    if(innerNode.hasProperty(CustomProperty.VC_NEXT_VERSIONS)) {
					//                    	Property prop = innerNode.getProperty(CustomProperty.VC_NEXT_VERSIONS);
					//                        String nextVerIDs	=	prop.getString();
					//                        log.debug("NextVersionIDs " + nextVerIDs);
					//                        
					//                        if(nextVerIDs!=null && !nextVerIDs.isEmpty()) {
					//	                        String[] temp	=	nextVerIDs.split(",");
					//	                        
					//	                        for(int i=0; i<temp.length; i++) {
					//	                        	try {
					//		                        	Node	tmpNode	=	session.getNodeByIdentifier(temp[i].trim());
					//		                        	if(tmpNode!=null) {
					//		                        		tmpNode	=	tmpNode.getParent();
					//		                        		CustomTreeNode nextVersion	=	copyNodeAllProperties(tmpNode, new CustomTreeNode() );
					//		                        		nextVersion.setVersion(true);
					//		                        		nextVersions.add(nextVersion);
					//		                        	}
					//	                        	} catch(Exception e) {
					//	                        		log.debug("Version ID is problamatic: "+temp[i]);
					//	                        		e.printStackTrace();
					//	                        	}
					//	                        }
					//	                    }
					//                    }

					if(innerNode.hasProperty(CustomProperty.VC_VERSION_OF)){
						Property prop = innerNode.getProperty(CustomProperty.VC_VERSION_OF);
						versionOf = prop.getString();
						log.debug("VersionOf " + versionOf);
					}

				}//while(innerIt != null && innerIt.hasNext()) {
			}


			if (isValidPath && (isFolderNode || isFileNode || isLinkedNode)) {
				String rowID = com.virtualcode.util.StringUtils.encodeUrl(nodePath);

				if (isLinkedNode && node.hasProperty(Property.JCR_CONTENT)) {
					Property prop = node.getProperty(Property.JCR_CONTENT);
					String refNodeID = prop.getString();
					Node refNode = session.getNodeByIdentifier(refNodeID);
					//nodePath = refNode.getParent().getPath();
					log.debug("nt:linked Node: "+nodePath);

				} else if(isFileNode && node.hasNode(Property.JCR_CONTENT) && node.getNode(Property.JCR_CONTENT).hasProperty(CustomProperty.VC_LINKED_TO)) {//if Custom Linked Node
					Property prop    =   node.getNode(Property.JCR_CONTENT).getProperty(CustomProperty.VC_LINKED_TO);
					String refNodeID =   prop.getString();
					linkedFile=true;
					Node refNode =  session.getNodeByIdentifier(refNodeID);
					// nodePath =   refNode.getParent().getPath();
					log.debug("nt:file Custom Linked Node: "+nodePath);
					//    innerIt = refNode.getNodes("jcr:content");
					//   while (innerIt != null && innerIt.hasNext()) {
					log.debug(nodeName + " , contains JCR:CONTENT Node");
					innerNode = refNode;//innerIt.nextNode();
					if (innerNode.hasProperty("jcr:data")) {
						log.debug(innerNode.getName() + " , contains JCR:DATA Property So calculating Size & MimeType");	                            
						fileSize = "";

						double length = innerNode.getProperty("jcr:data").getLength();

						DecimalFormat df = new DecimalFormat("#.##");
						customNode.setSizeinKbs(Math.round(length)+"");
						length = length / 1000d;
						fileSize = df.format(length)+" KB";                        

						//convert to MBs
						if(length>1024d){
							length = length / 1024d;
							fileSize = df.format(length)+" MB";
						}
						//convert to GBs
						if(length>1024d){
							length = length / 1024d;
							fileSize = df.format(length)+" GB";
						}
						//	                            fileSize = String.valueOf(Math.round(Math.ceil(length / 1000d))) + " KB";

					}//if (innerNode.hasProperty("jcr:data"))
					//}
				} else {
					log.debug("File Node Path: "+nodePath);
				}

				nodePath=com.virtualcode.util.StringUtils.encodeUrl(nodePath);
				log.debug("nodePath  encoded --->"+nodePath);

				nodeName=com.virtualcode.util.StringUtils.escapeXmlSpecialChars(nodeName);

				//------------------sids----------------------

				JespaUtil util = JespaUtil.getInstance();
				securityAllowGroupNames = util.getSIDsDescription(sids);
				securityDenyGroupNames = util.getSIDsDescription(denySids);
				shareAllowGroupNames = util.getSIDsDescription(shareSids);
				shareDenyGroupNames = util.getSIDsDescription(denyShareSids);

				/*log.debug("sids " + sids);
                    log.debug("denySids " + denySids);
                    log.debug("shareSids " + shareSids);
                    log.debug("denyShareSids " + denyShareSids);

                    System.out.println("sids " + sids);
                    System.out.println("denySids " + denySids);
                    System.out.println("shareSids " + shareSids);
                    System.out.println("denyShareSids " + denyShareSids); */


				RepositoryBrowserUtil repoBrwUtil = RepositoryBrowserUtil.getInstance();
				FileTypeMapper fileTypeMapper = repoBrwUtil.getFileTypeMapperFromName(nodeName, isFolderNode);

				customNode.setFileTypeDesc(fileTypeMapper.getTypeText());
				customNode.setIconUrl(fileTypeMapper.getIconName());

				//                    if(nextVersions!=null && nextVersions.size()>0)
				//                    {
				//                    	int totalVersions = nextVersions.size();
				//                    	CustomTreeNode tempNode = nextVersions.get(totalVersions-1);
				//                        customNode.setCreatedDate(tempNode.getCreatedDate());
				//                        customNode.setArchivedDate(tempNode.getArchivedDate());
				//                        customNode.setLastModified(tempNode.getLastModified());
				//                        customNode.setLastAccessed(tempNode.getLastAccessed());
				//                        
				//                        customNode.setDownloadUrl(tempNode.getDownloadUrl());
				//                        
				//                        customNode.setFileSize(tempNode.getFileSize());
				//                    }else{

				customNode.setCreatedDate(documentCreateDate);  
				customNode.setArchivedDate(archiveDate);
				customNode.setLastModified(lastModified);
				customNode.setLastAccessed(lastAccessedDate);

				customNode.setDownloadUrl(nodePath);

				customNode.setFileSize(fileSize);
				//                    }

				//                	customNode.setDownloadUrl(nodePath);


				customNode.setSecurityAllowSIDs(sids);
				customNode.setSecurityDenySIDs(denySids);
				customNode.setShareAllowSIDs(shareSids);
				customNode.setShareDenySIDs(denyShareSids);

				customNode.setSecurityAllowGroupNames(securityAllowGroupNames);
				customNode.setSecurityDenyGroupNames(securityDenyGroupNames);
				customNode.setShareAllowGroupNames(shareAllowGroupNames);
				customNode.setShareDenyGroupNames(shareDenyGroupNames);

				customNode.setMimeType(mimeType);
				//                	customNode.setFileSize(fileSize);
				customNode.setLinkedNode(linkedFile);

				customNode.setTagUser(documentTagUser);
				customNode.setTagTimeStamp(documentTagTimeStamp);
				customNode.setTags(documentTags);
				customNode.setContentType(contentType);
				customNode.setSpDocUrl(spDocUrl);
				customNode.setArchivingMode(archivingMode);     
				//valid for print archived docs only
				customNode.setPrinterName(printerName);
				customNode.setMachineName(machineName);
				customNode.setUsername(username);
				customNode.setTotalPages(totalPages);
				//ignore if linked file
				if(node.hasNode(Property.JCR_CONTENT) && !node.getNode(Property.JCR_CONTENT).hasProperty(CustomProperty.VC_LINKED_TO)
						&& innerNode!=null && innerNode.hasProperty(CustomProperty.VC_NEXT_VERSIONS)) {
					Property prop = innerNode.getProperty(CustomProperty.VC_NEXT_VERSIONS);
					String nextVerIDs	=	prop.getString();
					log.debug("NextVersionIDs " + nextVerIDs);

					if(nextVerIDs!=null && !nextVerIDs.isEmpty()) {                        	
						String[] temp	=	nextVerIDs.split(",");
						nextVersions.add(copyNodeFewerProperties(node, new CustomTreeNode()));


						for(int i=0; i<temp.length; i++) {
							try {
								Node	tmpNode	=	session.getNodeByIdentifier(temp[i].trim());
								if(tmpNode!=null) {
									if(!tmpNode.getIdentifier().equals(node.getIdentifier()))
										tmpNode	=	tmpNode.getParent();		                        		
									CustomTreeNode nextVersion	=	copyNodeAllProperties(tmpNode, new CustomTreeNode() );
									nextVersion.setVersion(true);
									nextVersions.add(nextVersion);
								}
							} catch(Exception e) {
								log.debug("Version ID is problamatic: "+temp[i]);
								e.printStackTrace();
							}
						}
					}
				}
				customNode.setNextVersions(nextVersions);
				customNode.setVersionOf(versionOf);
				if(nextVersions!=null && nextVersions.size()>0){


					int totalVersions = nextVersions.size();
					CustomTreeNode tempNode = nextVersions.get(totalVersions-1);
					customNode.setCreatedDate(tempNode.getCreatedDate());
					customNode.setArchivedDate(tempNode.getArchivedDate());
					customNode.setLastModified(tempNode.getLastModified());
					customNode.setLastAccessed(tempNode.getLastAccessed());

					customNode.setDownloadUrl(tempNode.getDownloadUrl());

					customNode.setFileSize(tempNode.getFileSize());
				}
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
		//System.out.println("Custom Node ---<"+customNode.toString());
		return customNode;
	}

	public List<CustomTreeNode> getAllVersions(String uuid){
		try{
			Node jcrNode= session.getNodeByIdentifier(uuid);
			NodeIterator innerIt = jcrNode.getNodes("jcr:content");
			if(innerIt==null || !innerIt.hasNext()){
				jcrNode = jcrNode.getParent();
				innerIt = jcrNode.getNodes("jcr:content");
			}
			List<CustomTreeNode> nextVersions = new ArrayList<CustomTreeNode>();

			while(innerIt!=null && innerIt.hasNext()){
				Node innerNode = innerIt.nextNode();
				//check if its a version node itself

				if(innerNode.hasProperty(CustomProperty.VC_VERSION_OF)){
					String versionOf;
					Property prop = innerNode.getProperty(CustomProperty.VC_VERSION_OF);
					versionOf = prop.getString();
					log.debug("VersionOf " + versionOf);
					nextVersions.addAll(getAllVersions(versionOf));
				}
				if(innerNode.hasProperty(CustomProperty.VC_NEXT_VERSIONS)) {
					Property prop = innerNode.getProperty(CustomProperty.VC_NEXT_VERSIONS);
					String nextVerIDs	=	prop.getString();
					log.debug("NextVersionIDs " + nextVerIDs);

					if(nextVerIDs!=null && !nextVerIDs.isEmpty()) {                        	
						String[] temp	=	nextVerIDs.split(",");
						nextVersions.add(copyNodeFewerProperties(jcrNode, new CustomTreeNode()));


						for(int i=0; i<temp.length; i++) {
							try {
								Node	tmpNode	=	session.getNodeByIdentifier(temp[i].trim());
								if(tmpNode!=null) {
									if(!tmpNode.getIdentifier().equals(jcrNode.getIdentifier()))
										tmpNode	=	tmpNode.getParent();		                        		
									CustomTreeNode nextVersion	=	copyNodeAllProperties(tmpNode, new CustomTreeNode() );
									nextVersion.setVersion(true);
									nextVersion.setName(jcrNode.getName());
									nextVersions.add(nextVersion);
								}
							} catch(Exception e) {
								log.debug("Version ID is problamatic: "+temp[i]);
								e.printStackTrace();
							}
						}
					}
				}
			}
			return nextVersions;
		}catch(RepositoryException re){
			re.printStackTrace();
		}
		return null;
	}

	public Binary downloadFile(String filePath){

		Binary binaryStream=null;

		Node root;


		try {          

			root = session.getRootNode();

			filePath    =   filePath.replace("//", "/");//if somewhere // exists
			if(filePath.indexOf("/")==0)
				filePath    =   filePath.substring(1);//skip first /

			log.info("File path becomes: " + filePath);
			Node file = root.getNode(filePath);
			if(file!=null ){
				log.info("Node  name  "+file.getName());
				log.info("Node  path  "+file.getPath());

				String nodeType=file.getPrimaryNodeType().getName();
				log.debug("Node Type "+nodeType);
				boolean isLinkedNode = nodeType.equalsIgnoreCase("nt:linkedFile");

				log.debug(file.getNode(Property.JCR_CONTENT).hasProperty(CustomProperty.VC_LINKED_TO));
				if (isLinkedNode && file.hasProperty(Property.JCR_CONTENT)) {
					Property prop   = file.getProperty(Property.JCR_CONTENT);
					String refNodeID   =   prop.getString();
					file    =   session.getNodeByIdentifier(refNodeID).getParent();
				} else if(nodeType.equalsIgnoreCase("nt:file") && file.getNode(Property.JCR_CONTENT).hasProperty(CustomProperty.VC_LINKED_TO)) {

					log.debug("nt:file has propeorty of "+CustomProperty.VC_LINKED_TO);
					Property prop  =   file.getNode(Property.JCR_CONTENT).getProperty(CustomProperty.VC_LINKED_TO);
					String refNodeID   =   (prop!=null)?prop.getString():null;
					if(refNodeID!=null) {
						log.debug("a linked node of type nt:file");
						file    =   session.getNodeByIdentifier(refNodeID).getParent();
					}
				}

				if(file.hasNode("jcr:content")){

					log.info("Node has content");

					Node node=file.getNode("jcr:content");
//					int count = 0;
//					String latestId = "";
//					//to get latest file content version against parent file
//					if(file.hasNode(Property.JCR_CONTENT) && !file.getNode(Property.JCR_CONTENT).hasProperty(CustomProperty.VC_LINKED_TO)
//							&& node!=null && node.hasProperty(CustomProperty.VC_NEXT_VERSIONS)) {
//						Property prop = node.getProperty(CustomProperty.VC_NEXT_VERSIONS);
//						String nextVerIDs	=	prop.getString();
//						log.debug("NextVersionIDs " + nextVerIDs);
//
//						if(nextVerIDs!=null && !nextVerIDs.isEmpty()) {                        	
//							String[] temp	=	nextVerIDs.split(",");
////							nextVersions.add(copyNodeFewerProperties(node, new CustomTreeNode()));
//
//
//							for(int i=0; i<temp.length; i++) {
//								try {
//									Node	tmpNode	=	session.getNodeByIdentifier(temp[i].trim());
//									if(tmpNode!=null) {
////										if(!tmpNode.getIdentifier().equals(node.getIdentifier()))
////											tmpNode	=	tmpNode.getParent();		                        		
////										CustomTreeNode nextVersion	=	copyNodeAllProperties(tmpNode, new CustomTreeNode() );
////										nextVersion.setVersion(true);
//										latestId = tmpNode.getIdentifier();
////										node = tmpNode;
//										count+=1;
////										nextVersions.add(nextVersion);
//									}
//								} catch(Exception e) {
//									log.debug("Version ID is problamatic: "+temp[i]);
//									e.printStackTrace();
//								}
//							}
//						}						
//					}
//					customNode.setNextVersions(nextVersions);
//					customNode.setVersionOf(versionOf);
//					if(nextVersions!=null && nextVersions.size()>0){
//
//
//						int totalVersions = nextVersions.size();
//						CustomTreeNode tempNode = nextVersions.get(totalVersions-1);
//						customNode.setCreatedDate(tempNode.getCreatedDate());
//						customNode.setArchivedDate(tempNode.getArchivedDate());
//						customNode.setLastModified(tempNode.getLastModified());
//						customNode.setLastAccessed(tempNode.getLastAccessed());
//
//						customNode.setDownloadUrl(tempNode.getDownloadUrl());
//
//						customNode.setFileSize(tempNode.getFileSize());
//					}
//				}

					log.debug("Node to retrieve data===>"+node.getPath());
					if(node.hasProperty("jcr:data")){
						log.info("Node has data");
						Property dataProperty=node.getProperty("jcr:data");
						log.info("File Size --->"+dataProperty.getBinary().getSize());
						binaryStream= dataProperty.getBinary();
					}else{
						log.error("Node hasn't data");
					}

				}else{
					log.error("node has not content ");
				}

			}

		}catch(Exception ex){
			log.debug(ex.getMessage(), ex);
		}

		return binaryStream;
	}

	public Binary downloadFile(String filePath, boolean isArchive){

		Binary binaryStream=null;

		Node root;
		boolean found = false;


		try {          

			root = session.getRootNode();

			filePath    =   filePath.replace("//", "/");//if somewhere // exists
			if(filePath.indexOf("/")==0)
				filePath    =   filePath.substring(1);//skip first /

			log.info("File path becomes: " + filePath);
			Node file = root.getNode(filePath);
			if(file!=null ){
				log.info("Node  name  "+file.getName());
				log.info("Node  path  "+file.getPath());

				String nodeType=file.getPrimaryNodeType().getName();
				log.debug("Node Type "+nodeType);
				boolean isLinkedNode = nodeType.equalsIgnoreCase("nt:linkedFile");

				log.debug(file.getNode(Property.JCR_CONTENT).hasProperty(CustomProperty.VC_LINKED_TO));
				if (isLinkedNode && file.hasProperty(Property.JCR_CONTENT)) {
					Property prop   = file.getProperty(Property.JCR_CONTENT);
					String refNodeID   =   prop.getString();
					file    =   session.getNodeByIdentifier(refNodeID).getParent();
				} else if(nodeType.equalsIgnoreCase("nt:file") && file.getNode(Property.JCR_CONTENT).hasProperty(CustomProperty.VC_LINKED_TO)) {

					log.debug("nt:file has propeorty of "+CustomProperty.VC_LINKED_TO);
					Property prop  =   file.getNode(Property.JCR_CONTENT).getProperty(CustomProperty.VC_LINKED_TO);
					String refNodeID   =   (prop!=null)?prop.getString():null;
					if(refNodeID!=null) {
						log.debug("a linked node of type nt:file");
						file    =   session.getNodeByIdentifier(refNodeID).getParent();
					}
				}

				if(file.hasNode("jcr:content")){

					log.info("Node has content");

					Node node=file.getNode("jcr:content");

					log.debug("Node to retrieve data===>"+node.getPath());
					if(node.hasProperty("jcr:data")){
						log.info("Node has data");
						Property dataProperty=node.getProperty("jcr:data");
						log.info("File Size --->"+dataProperty.getBinary().getSize());
						binaryStream= dataProperty.getBinary();
					}else{
						log.error("Node hasn't data");
					}

				}else{
					log.error("node has not content ");
				}             
				return binaryStream;

			}

		}catch(Exception ex){
			log.debug(ex.getMessage(), ex);
		}
		if(!found){

			//filePath=com.virtualcode.util.StringUtils.decodeUrl(filePath);
			String message = "Error while restore";


			DriveLetters driveLetter = null;
			List<DriveLetters> driveLetterList=driveLettersService.getAll();

			for(DriveLetters letter: driveLetterList){
				String sharePath = letter.getSharePath().toLowerCase();
				sharePath = sharePath.substring(2).replaceAll("\\\\", "/");
				if(sharePath.startsWith("\\\\")){
					//						ResourcesUtil ru = ResourcesUtil.getResourcesUtil();				
					//						sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase() + "/fs/" + sharePath.substring(2).replaceAll("\\\\", "/");
				}
				//					if(sharePath.endsWith("/"))
				//						sharePath = sharePath.substring(0, sharePath.lastIndexOf("/"));
				System.out.println(filePath +", " + sharePath);
				if(filePath.toLowerCase().contains(sharePath)){
					driveLetter = letter;
					break;
				}
			}
			ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
			String domain = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
			String username = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
			String password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
			if(driveLetter!=null){
				domain =  driveLetter.getRestoreDomain();
				username = driveLetter.getRestoreUsername();
				password = driveLetter.getRestorePassword();
			}
			AdvancedSecureURL advancedSecureURL = AdvancedSecureURL.getInstance();
			
			
			LinkAccessDocumentsService linkAccessDocumentsService = serviceManager.getLinkAccessDocumentsService();
			LinkAccessDocuments linkAccessDocument = null;
			try {
				System.out.println("\\\\"+filePath.replaceAll("/", "\\\\"));
				System.out.println();
				linkAccessDocument = linkAccessDocumentsService.getLinkAccessDocumentsByUuidDocuments("na-"+advancedSecureURL.getEncryptedURL("\\\\"+filePath.replaceAll("/", "\\\\")));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			String oldHash = null;
			if(linkAccessDocument!=null){
				oldHash = linkAccessDocument.getHash();
			}
			VirtualcodeUtil util = VirtualcodeUtil.getInstance();
			

			String searchPath = "smb://"+ domain +";"+ username +":"+ password +"@"+filePath.replaceAll("\\\\", "/");
			SmbFile smbFile;
			try {
				smbFile = new SmbFile(searchPath);
				if(smbFile.isFile()){
					System.out.println(smbFile.getContentLength());
					System.out.println(smbFile.getName());
					try {
						if(!oldHash.equals(util.SHAsum((smbFile.getLastModified()+"").getBytes())))
							return null;
					} catch (NoSuchAlgorithmException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch(NullPointerException nex){
						nex.printStackTrace();
					}
					UncFile uncFile = new UncFile(smbFile.getInputStream(), smbFile.length(), smbFile.getName());
					return uncFile;
				}
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SmbException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch(Exception ex){
				ex.printStackTrace();
			}

		}

		return null;
	}


	//changed this to return sids
	@Override
	public List loadPermissions(String nodeID, ArrayList<SID> secAllGroups,ArrayList<SID> securityAllowGroups, ArrayList<SID> securityDenyGroups,
			//public CustomTreeNode loadPermissions(String nodeID, ArrayList<SID> secAllGroups,ArrayList<SID> securityAllowGroups, ArrayList<SID> securityDenyGroups,
			ArrayList<SID> shareAllGroups, ArrayList<SID> shareAllowGroups,ArrayList<SID> shareDenyGroups) {
		List sidsList = new ArrayList();
		Node root;
		String nodeName = null;
		String sids = null;
		String denySids =null;
		String shareSids = null;
		String denyShareSids = null;
		String sids_orig = null;
		String denySids_orig = null;
		String shareSids_orig = null;
		String denyShareSids_orig = null;

		String nodePath = null;
		String nodeType = null;
		boolean isFileNode = false;
		boolean isFolderNode = false;

		ArrayList<SID> secAllUsers = new ArrayList<SID>();
		ArrayList<SID> shareAllUsers = new ArrayList<SID>();

		try {
			root = session.getRootNode();

			if(root!=null){
				nodeID    =   nodeID.replace("//", "/");//if somewhere // exists
				if(nodeID.indexOf("/")==0)
					nodeID    =   nodeID.substring(1);//skip first /

					root = root.getNode(nodeID);
					nodeName = root.getName();
					nodeType = root.getPrimaryNodeType().getName();
					nodePath = root.getPath();
					log.debug("nodeName " + nodeName);
					log.debug("nodeType " + nodeType);
					log.debug("nodePath " + nodePath);
					isFileNode = nodeType.equalsIgnoreCase("nt:file");
					isFolderNode = nodeType.equalsIgnoreCase("nt:folder");

					if (isFolderNode) {

						if (root.hasNode(CustomNodeType.VC_RESOURCE_PERMISSIONS)) {

							root = root.getNode(CustomNodeType.VC_RESOURCE_PERMISSIONS);

							if (root.hasProperty(CustomProperty.VC_ALLOWED_SIDS)) {
								Property prop = root.getProperty(CustomProperty.VC_ALLOWED_SIDS);
								sids = prop.getString();
								log.debug("Folder AllowedSIDs " + sids);
							}
							if (root.hasProperty(CustomProperty.VC_DENIED_SIDS)) {
								Property prop = root.getProperty(CustomProperty.VC_DENIED_SIDS);
								denySids = prop.getString();
								log.debug("Folder DeniedSIDs " + denySids);
							}
							if (root.hasProperty(CustomProperty.VC_S_ALLOWED_SIDS)) {
								Property prop = root.getProperty(CustomProperty.VC_S_ALLOWED_SIDS);
								shareSids = prop.getString();
								log.debug("Folder AllowedShareSIDs " + shareSids);
							}
							if (root.hasProperty(CustomProperty.VC_S_DENIED_SIDS)) {
								Property prop = root.getProperty(CustomProperty.VC_S_DENIED_SIDS);
								denyShareSids = prop.getString();
								log.debug("Folder DenyShareSIDs " + denyShareSids);
							}
						}



					} else // its File Node
					{

						NodeIterator innerIt = null;
						Node innerNode = null;
						if (isFileNode) {
							innerIt = root.getNodes("jcr:content");
						}

						while (innerIt != null && innerIt.hasNext()) {
							log.debug(nodeName + " , contains JCR:CONTENT Node");
							innerNode = innerIt.nextNode();

							if (innerNode.hasProperty(CustomProperty.VC_ALLOWED_SIDS)) {
								Property prop = innerNode.getProperty(CustomProperty.VC_ALLOWED_SIDS);
								sids = prop.getString();
								log.debug("AllowedSIDs " + sids);
							}
							if (innerNode.hasProperty(CustomProperty.VC_DENIED_SIDS)) {
								Property prop = innerNode.getProperty(CustomProperty.VC_DENIED_SIDS);
								denySids = prop.getString();
								log.debug("DeniedSIDs " + denySids);
							}
							if (innerNode.hasProperty(CustomProperty.VC_S_ALLOWED_SIDS)) {
								Property prop = innerNode.getProperty(CustomProperty.VC_S_ALLOWED_SIDS);
								shareSids = prop.getString();
								log.debug("AllowedShareSIDs " + shareSids);
							}
							if (innerNode.hasProperty(CustomProperty.VC_S_DENIED_SIDS)) {
								Property prop = innerNode.getProperty(CustomProperty.VC_S_DENIED_SIDS);
								denyShareSids = prop.getString();
								log.debug("DenyShareSIDs " + denyShareSids);
							}

						}//while(innerIt != null && innerIt.hasNext()) {
					}

					log.debug("sids ................. " + sids);
					log.debug("denySids ................. " + denySids);
					log.debug("shareSids ................. " + shareSids);
					log.debug("denyShareSids ................. " + denyShareSids);

					sids_orig = sids;
					denySids_orig = denySids;
					shareSids_orig = shareSids;
					denyShareSids_orig = denyShareSids;

					log.debug("................ Original SID's Value in ViewNode.JSP................");
					log.debug("sids_orig " + sids_orig);
					log.debug("denySids_orig " + denySids_orig);
					log.debug("shareSids_orig " + shareSids_orig);
					log.debug("denyShareSids_orig " + denyShareSids_orig);




					//Preparing Display Data

					JespaUtil util = JespaUtil.getInstance();
					securityAllowGroups = util.getSIDsObjects(sids);
					securityDenyGroups = util.getSIDsObjects(denySids);
					shareAllowGroups = util.getSIDsObjects(shareSids);
					shareDenyGroups = util.getSIDsObjects(denyShareSids);


					sids = util.getSIDsDescriptionFromList(securityAllowGroups);
					denySids = util.getSIDsDescriptionFromList(securityDenyGroups);
					shareSids = util.getSIDsDescriptionFromList(shareAllowGroups);
					denyShareSids = util.getSIDsDescriptionFromList(shareDenyGroups);

					log.debug("sids " + sids);
					log.debug("denySids " + denySids);
					log.debug("shareSids " + shareSids);
					log.debug("denyShareSids " + denyShareSids);


					Map groupSIDs = util.getGroups();                  
					Iterator it = groupSIDs.entrySet().iterator();
					while (it.hasNext()) {
						Map.Entry pairs = (Map.Entry) it.next();
						secAllGroups.add(new SID(pairs.getKey().toString(), pairs.getValue().toString()));
						shareAllGroups.add(new SID(pairs.getKey().toString(), pairs.getValue().toString()));
						System.out.println(pairs.getKey() + " = " + pairs.getValue());

						Collections.sort(secAllGroups, new Comparator() {

							public int compare(Object o1, Object o2) {
								SID first = (SID) o1;
								SID second = (SID) o2;
								return first.getDescription().compareToIgnoreCase(second.getDescription());
							}
						});
						Collections.sort(shareAllGroups, new Comparator() {

							public int compare(Object o1, Object o2) {
								SID first = (SID) o1;
								SID second = (SID) o2;
								return first.getDescription().compareToIgnoreCase(second.getDescription());
							}
						});

					}

					Iterator ite = securityAllowGroups.iterator();
					while (ite.hasNext()) {
						log.debug("Removing ...");
						SID sid = (SID) ite.next();
						log.debug("Removed From ALL  ... " + secAllGroups.remove(sid));

					}

					//handle userSids

					Map userSIDs = util.getUsers();
					it = userSIDs.entrySet().iterator();
					while (it.hasNext()) {
						Map.Entry pairs = (Map.Entry) it.next();
						secAllUsers.add(new SID(pairs.getKey().toString(), pairs.getValue().toString(), true));
						shareAllUsers.add(new SID(pairs.getKey().toString(), pairs.getValue().toString(), true));
						System.out.println(pairs.getKey() + " = " + pairs.getValue());

						Collections.sort(secAllUsers, new Comparator() {

							public int compare(Object o1, Object o2) {
								SID first = (SID) o1;
								SID second = (SID) o2;
								return first.getDescription().compareToIgnoreCase(second.getDescription());
							}
						});
						Collections.sort(shareAllUsers, new Comparator() {

							public int compare(Object o1, Object o2) {
								SID first = (SID) o1;
								SID second = (SID) o2;
								return first.getDescription().compareToIgnoreCase(second.getDescription());
							}
						});

					}

					log.debug("Removed ... " + secAllGroups.removeAll(securityAllowGroups));
					log.debug("Removed ... " + secAllGroups.removeAll(securityDenyGroups));
					//trial and error to fix the issue with repeated/duplicate groups
					log.debug("Removed from shared ... " + shareAllGroups.removeAll(shareAllowGroups));
					log.debug("Removed from shared ... " + shareAllGroups.removeAll(shareDenyGroups));
					//remove users from list that are in allowed/denied
					log.debug("Removed ... " + secAllUsers.removeAll(securityAllowGroups));
					log.debug("Removed ... " + secAllUsers.removeAll(securityDenyGroups));

					log.debug("Removed from shared ... " + shareAllUsers.removeAll(shareAllowGroups));
					log.debug("Removed from shared ... " + shareAllUsers.removeAll(shareDenyGroups));
					sidsList.add(secAllGroups);
					sidsList.add(securityAllowGroups);
					sidsList.add(securityDenyGroups);
					sidsList.add(shareAllowGroups);
					sidsList.add(shareDenyGroups);
					sidsList.add(secAllUsers);
					sidsList.add(shareAllUsers);


			}




		}catch(Exception ex){
			ex.printStackTrace();
		}

		return sidsList;
	}


	@Override
	public boolean savePermissions(String path,	ArrayList<SID> securityAllowGroups,	ArrayList<SID> securityDenyGroups, ArrayList<SID> shareAllowGroups,	ArrayList<SID> shareDenyGroups, boolean isRecursive, boolean isbatchUpdate, boolean merge) {

		Node root;
		boolean success = false;
		try {
			root = session.getRootNode();			  
			if(root!=null){
				path=path.replace("//", "/");//if somewhere // exists
				if(path.indexOf("/")==0)
					path=path.substring(1);//skip first /

				root = root.getNode(path);
				String nodeType = root.getPrimaryNodeType().getName();
				System.out.println("savePermissions() ---- node name:"+root.getPath());
				String allowSidsString = StringUtils.convertArrayToString(securityAllowGroups);
				System.out.println("savePermissions() ----"+allowSidsString);
				String denySidsString = StringUtils.convertArrayToString(securityDenyGroups);
				System.out.println("savePermissions() ----"+denySidsString);
				String shareAllowSidsString = StringUtils.convertArrayToString(shareAllowGroups);
				System.out.println("savePermissions() ----"+shareAllowSidsString);
				String shareDenySidsString = StringUtils.convertArrayToString(shareDenyGroups);
				System.out.println("savePermissions() ----"+shareAllowSidsString);
				boolean isFolderNode = nodeType.equalsIgnoreCase("nt:folder");

				if(isbatchUpdate){
					System.out.println("------------------------------------------Creating batch job");
					String loginContext="dXNlcm5hbWU9Yml0YXJjaCZwYXNzd29yZD1iaXRhcmNo";	//hard coded for now
					VirtualcodeUtil vcUtil=VirtualcodeUtil.getInstance();
					UpdateNodePermissionJob job=new UpdateNodePermissionJob();
					job.setName("ID-"+System.currentTimeMillis());
					job.setCommand(path);
					job.setStatus(UpdateNodePermissionJob.QUEUED_JOB);
					job.setRegisterDate(DateFormat.getInstance().format(new Date()));
					job.setSecureInfo(loginContext);
					job.setSids(allowSidsString);
					job.setDenySids(denySidsString);
					job.setShareSids(shareAllowSidsString);
					job.setDenyShareSids(shareDenySidsString);
					job.setRecursive((isRecursive==true)?1:0);
					vcUtil.addUpdatePermissionJobToQueue(job);
				}else{

					if(isFolderNode){
						if(isRecursive){
							SecurityPermissionsUtil.updateFolderPermissionsRecursively(root, allowSidsString, denySidsString, shareAllowSidsString, shareDenySidsString);                		  
							System.out.println("recursive update success");
						}else{
							SecurityPermissionsUtil.updateFolderPermissions(root, allowSidsString, denySidsString, shareAllowSidsString, shareDenySidsString);                		  
							System.out.println("update success");
						}
						//System.out.println(updateFolderNodePermissions(root, allowSidsString, denySidsString, shareAllowSidsString, shareDenySidsString)+"--saved");
					}else{
						System.out.println(SecurityPermissionsUtil.updateFileNodePermissions(root, allowSidsString, denySidsString, shareAllowSidsString, shareDenySidsString)+"--saved");                	  
					}
				}
				success = true;
				session.save();
				//return success;
			}
		}catch(Exception ex){
			ex.printStackTrace();
			//return success;
		}
		return success;
	}

	@Override
	public boolean syncPermissions(String path,	String securityAllowGroups,	String securityDenyGroups, String shareAllowGroups,	String shareDenyGroups, boolean isRecursive, boolean isbatchUpdate, boolean merge) {

		Node root;
		boolean success = false;
		try {
			root = session.getRootNode();			  
			if(root!=null){
				path=path.replace("//", "/");//if somewhere // exists
				if(path.indexOf("/")==0)
					path=path.substring(1);//skip first /

				root = root.getNode(path);
				String nodeType = root.getPrimaryNodeType().getName();
				System.out.println("savePermissions() ---- node name:"+root.getPath());
				String allowSidsString = securityAllowGroups;
				System.out.println("savePermissions() ----"+allowSidsString);
				String denySidsString = securityDenyGroups;
				System.out.println("savePermissions() ----"+denySidsString);
				String shareAllowSidsString = shareAllowGroups;
				System.out.println("savePermissions() ----"+shareAllowSidsString);
				String shareDenySidsString = shareDenyGroups;
				System.out.println("savePermissions() ----"+shareAllowSidsString);
				boolean isFolderNode = nodeType.equalsIgnoreCase("nt:folder");

				if(isbatchUpdate){
					System.out.println("------------------------------------------Creating batch job");
					String loginContext="dXNlcm5hbWU9Yml0YXJjaCZwYXNzd29yZD1iaXRhcmNo";	//hard coded for now
					VirtualcodeUtil vcUtil=VirtualcodeUtil.getInstance();
					UpdateNodePermissionJob job=new UpdateNodePermissionJob();
					job.setName("ID-"+System.currentTimeMillis());
					job.setCommand(path);
					job.setStatus(UpdateNodePermissionJob.QUEUED_JOB);
					job.setRegisterDate(DateFormat.getInstance().format(new Date()));
					job.setSecureInfo(loginContext);
					job.setSids(allowSidsString);
					job.setDenySids(denySidsString);
					job.setShareSids(shareAllowSidsString);
					job.setDenyShareSids(shareDenySidsString);
					job.setRecursive((isRecursive==true)?1:0);
					vcUtil.addUpdatePermissionJobToQueue(job);
				}else{

					if(isFolderNode){
						if(isRecursive){
							SecurityPermissionsUtil.updateFolderPermissionsRecursively(root, allowSidsString, denySidsString, shareAllowSidsString, shareDenySidsString);                		  
							System.out.println("recursive update success");
						}else{
							SecurityPermissionsUtil.updateFolderPermissions(root, allowSidsString, denySidsString, shareAllowSidsString, shareDenySidsString);                		  
							System.out.println("update success");
						}
						//System.out.println(updateFolderNodePermissions(root, allowSidsString, denySidsString, shareAllowSidsString, shareDenySidsString)+"--saved");
					}else{
						System.out.println(SecurityPermissionsUtil.updateFileNodePermissions(root, allowSidsString, denySidsString, shareAllowSidsString, shareDenySidsString)+"--saved");                	  
					}
				}
				success = true;
				session.save();
				//return success;
			}
		}catch(Exception ex){
			ex.printStackTrace();
			//return success;
		}
		return success;
	}

	@Override
	public boolean syncPermissions(String path, boolean isRecursive, boolean isbatchUpdate, boolean isMerge) {
		log.debug("-----------------in sync permission for path: " + path);
		Node root;
		boolean success = false;
		Synchronizer synchronizer;
		String destPath="";
		try {
			DriveLetters driveLetter = null;
			List<DriveLetters> driveLetterList=driveLettersService.getAll();
			for(DriveLetters letter: driveLetterList){
				String sharePath = letter.getSharePath();
				if(sharePath.startsWith("\\\\")){
					ResourcesUtil ru = ResourcesUtil.getResourcesUtil();				
					sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL)+ "/FS/" + sharePath.substring(2).replaceAll("\\\\", "/");
				}
				System.out.println("comparing: " + path.toLowerCase() +" and " + sharePath.toLowerCase());
				if(path.toLowerCase().contains(sharePath.toLowerCase())){	// converted to lower case for case-insensitivity
					driveLetter = letter;
					break;
				}
			}
			if(driveLetter==null){
				driveLetter = new DriveLetters();
			}
			synchronizer = new Synchronizer(driveLetter.getRestoreDomain(), driveLetter.getRestoreUsername(), driveLetter.getRestorePassword());
			root = session.getRootNode();			  
			if(root!=null){
				path=path.replace("//", "/");//if somewhere // exists
				if(path.indexOf("/")==0)
					path=path.substring(1);//skip first /

				root = root.getNode(path);
				String[] pathElements = com.virtualcode.util.StringUtils.getPathElements(path);                  
				for (int i = 0; i < pathElements.length; i++) {
					System.out.println("elements: "+pathElements[i]);
					if (i > 1) {
						destPath += pathElements[i] + "/";
					}
				}
				ArrayList<String> sidList = synchronizer.getAllPermissions(destPath);
				if(sidList!=null && sidList.size()>0){
					String nodeType = root.getPrimaryNodeType().getName();
					System.out.println("savePermissions() ---- node name:"+root.getPath());
					String allowSidsString = sidList.get(0);
					System.out.println("savePermissions() ----"+allowSidsString);
					String denySidsString = sidList.get(1);
					System.out.println("savePermissions() ----"+denySidsString);
					String shareAllowSidsString = sidList.get(2);
					System.out.println("savePermissions() ----"+shareAllowSidsString);
					String shareDenySidsString = sidList.get(3);
					System.out.println("savePermissions() ----"+shareDenySidsString);
					boolean isFolderNode = nodeType.equalsIgnoreCase("nt:folder");

					if(isMerge){
						ArrayList<String> allSidsList = getAlNodeSids(root);
						if(allSidsList!=null && allSidsList.size()>0){
							System.out.println("before merge");
							System.out.println("allow sids: " + allowSidsString);
							System.out.println("deny Sids: " + denySidsString);
							System.out.println("share Sids:" + shareAllowSidsString);
							System.out.println("share Deny Sids: "+shareDenySidsString);
							allowSidsString = StringUtils.removeDuplicates(allowSidsString + "," + allSidsList.get(0));
							denySidsString = StringUtils.removeDuplicates(denySidsString +"," + allSidsList.get(1));
							shareAllowSidsString = StringUtils.removeDuplicates(shareAllowSidsString+ "," + allSidsList.get(2));
							shareDenySidsString = StringUtils.removeDuplicates(shareDenySidsString+ "," + allSidsList.get(3));
							System.out.println("after merge");
							System.out.println("allow sids: " + allowSidsString);
							System.out.println("deny Sids: " + denySidsString);
							System.out.println("share Sids:" + shareAllowSidsString);
							System.out.println("share Deny Sids: "+shareDenySidsString);
							//                    		  allowSidsString+="," + allSidsList.get(0);
							//                    		  denySidsString+="," + allSidsList.get(1);
							//                    		  shareAllowSidsString+="," + allSidsList.get(2);
							//                    		  shareDenySidsString+="," +allSidsList.get(3);
						}
					}

					if(isbatchUpdate && !isRecursive){
						System.out.println("------------------------------------------Creating batch job");
						String loginContext="dXNlcm5hbWU9Yml0YXJjaCZwYXNzd29yZD1iaXRhcmNo";	//hard coded for now
						VirtualcodeUtil vcUtil=VirtualcodeUtil.getInstance();
						UpdateNodePermissionJob job=new UpdateNodePermissionJob();
						job.setName("ID-"+System.currentTimeMillis());
						job.setCommand(path);
						job.setStatus(UpdateNodePermissionJob.QUEUED_JOB);
						job.setRegisterDate(DateFormat.getInstance().format(new Date()));
						job.setSecureInfo(loginContext);
						job.setSids(allowSidsString);
						job.setDenySids(denySidsString);
						job.setShareSids(shareAllowSidsString);
						job.setDenyShareSids(shareDenySidsString);
						job.setRecursive((isRecursive==true)?1:0);
						vcUtil.addUpdatePermissionJobToQueue(job);
					}else{	
						//		                  if(isFolderNode){
						if(isRecursive){
							recursiveSyncPermissions(root, isbatchUpdate, isMerge);
							System.out.println("recursive update success");
						}else{
							SecurityPermissionsUtil.updateFolderPermissions(root, allowSidsString, denySidsString, shareAllowSidsString, shareDenySidsString);                		  
							System.out.println("update success");
						}
						//System.out.println(updateFolderNodePermissions(root, allowSidsString, denySidsString, shareAllowSidsString, shareDenySidsString)+"--saved");
						//		                  }else{
						//		                	  System.out.println(SecurityPermissionsUtil.updateFileNodePermissions(root, allowSidsString, denySidsString, shareAllowSidsString, shareDenySidsString)+"--saved");                	  
						//		                  }
					}
					success = true;
					session.save();
					//return success;
				}else{
					log.debug("sids not found for path:" + destPath);
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
			//return success;
		}

		log.debug("sync success: " + success);
		return success;
	}

	public boolean recursiveSyncPermissions(Node node, boolean isbatchUpdate, boolean isMerge){
		String folderPath = "";
		boolean success = false;
		Synchronizer synchronizer;
		String destPath="";
		NodeIterator it = null;
		try {
			folderPath = node.getPath().substring(1);
			DriveLetters driveLetter = null;
			List<DriveLetters> driveLetterList=driveLettersService.getAll();
			for(DriveLetters letter: driveLetterList){
				String sharePath = letter.getSharePath();
				if(sharePath.startsWith("\\\\")){
					ResourcesUtil ru = ResourcesUtil.getResourcesUtil();				
					sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL)+ "/FS/" + sharePath.substring(2).replaceAll("\\\\", "/");
				}
				System.out.println("comparing: " + folderPath.toLowerCase() +" and " + sharePath.toLowerCase());
				if(folderPath.toLowerCase().contains(sharePath.toLowerCase())){	// converted to lower case for case-insensitivity
					driveLetter = letter;
					break;
				}
			}
			if(driveLetter==null){
				driveLetter = new DriveLetters();
			}
			synchronizer = new Synchronizer(driveLetter.getRestoreDomain(), driveLetter.getRestoreUsername(), driveLetter.getRestorePassword());

			String[] pathElements = com.virtualcode.util.StringUtils.getPathElements(folderPath);                  
			for (int i = 0; i < pathElements.length; i++) {
				System.out.println("elements: "+pathElements[i]);
				if (i > 1) {
					destPath += pathElements[i] + "/";
				}
			}
			ArrayList<String> sidList = synchronizer.getAllPermissions(destPath);
			if(sidList!=null && sidList.size()>0){
				String nodeType = node.getPrimaryNodeType().getName();
				System.out.println("savePermissions() ---- node name:"+node.getPath());
				String allowSidsString = sidList.get(0);
				System.out.println("savePermissions() ----"+allowSidsString);
				String denySidsString = sidList.get(1);
				System.out.println("savePermissions() ----"+denySidsString);
				String shareAllowSidsString = sidList.get(2);
				System.out.println("savePermissions() ----"+shareAllowSidsString);
				String shareDenySidsString = sidList.get(3);
				System.out.println("savePermissions() ----"+shareAllowSidsString);
				boolean isFolderNode = nodeType.equalsIgnoreCase("nt:folder");
				//if merge is true, then merge with existing permissions
				if(isMerge){
					ArrayList<String> allSidsList = getAlNodeSids(node);
					if(allSidsList!=null && allSidsList.size()>0){
						System.out.println("before merge");
						System.out.println(allowSidsString);
						System.out.println(denySidsString);
						System.out.println(shareAllowSidsString);
						System.out.println(shareDenySidsString);
						allowSidsString = StringUtils.removeDuplicates(allowSidsString + "," + allSidsList.get(0));
						denySidsString = StringUtils.removeDuplicates(denySidsString +"," + allSidsList.get(1));
						shareAllowSidsString = StringUtils.removeDuplicates(shareAllowSidsString+ "," + allSidsList.get(2));
						shareDenySidsString = StringUtils.removeDuplicates(shareDenySidsString+ "," + allSidsList.get(3));
						System.out.println("after merge");
						System.out.println(allowSidsString);
						System.out.println(denySidsString);
						System.out.println(shareAllowSidsString);
						System.out.println(shareDenySidsString);
						//                		  allowSidsString+="," + allSidsList.get(0);
						//                		  denySidsString+="," + allSidsList.get(1);
						//                		  shareAllowSidsString+="," + allSidsList.get(2);
						//                		  shareDenySidsString+="," +allSidsList.get(3);
					}
				}

				if(isbatchUpdate){
					System.out.println("------------------------------------------Creating batch job");
					String loginContext="dXNlcm5hbWU9Yml0YXJjaCZwYXNzd29yZD1iaXRhcmNo";	//hard coded for now
					VirtualcodeUtil vcUtil=VirtualcodeUtil.getInstance();
					UpdateNodePermissionJob job=new UpdateNodePermissionJob();
					job.setName("ID-"+System.currentTimeMillis());
					job.setCommand(folderPath);
					job.setStatus(UpdateNodePermissionJob.QUEUED_JOB);
					job.setRegisterDate(DateFormat.getInstance().format(new Date()));
					job.setSecureInfo(loginContext);
					job.setSids(allowSidsString);
					job.setDenySids(denySidsString);
					job.setShareSids(shareAllowSidsString);
					job.setDenyShareSids(shareDenySidsString);
					//                      job.setRecursive((isRecursive==true)?1:0);
					vcUtil.addUpdatePermissionJobToQueue(job);
				}else{
					if(isFolderNode){
						SecurityPermissionsUtil.updateFolderPermissions(node, allowSidsString, denySidsString, shareAllowSidsString, shareDenySidsString);
						//going for recursive call
						it = node.getNodes();
						for (; (it != null && it.hasNext());) {
							try {
								node = it.nextNode();
								if (node.getPrimaryNodeType().getName().equalsIgnoreCase("nt:folder") || node.getPrimaryNodeType().getName().equalsIgnoreCase("nt:file")) {
									recursiveSyncPermissions(node, isbatchUpdate, isMerge);
								} 
							} catch (Exception ex) {
								ex.printStackTrace();
								log.error("Exception while iterating node Ex: " + ex);
							}
						}//                for (; it.hasNext();) {
					}
				}
				success = true;
				session.save();
				System.out.println("update success");

			}
			//		  }
		}catch(Exception ex){
			ex.printStackTrace();
			//return success;
		}
		return success;		
	}

	public CustomTreeNode createShareFolder(String path){

		try {			
			path = path.substring(2).replaceAll("\\\\", "/");
			CustomTreeNode node = getNodeByPath(path);
			if(node==null){

				String[] pathElements = StringUtils.getPathElements(path.replaceAll("\\\\", "/"));
				Node root  =  session.getRootNode();
				Node parent = null;
				ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();


				String companyName=ru.getCodeValue("companyName", "General");
				if(companyName==null){
					companyName = "TestCompany";
				}

				if (root.hasNode(companyName) || session.nodeExists("/" + companyName)) {
					parent = root.getNode(companyName);		               
				} else {
					parent = root.addNode(companyName, "nt:folder");

					log.debug(companyName + "not exist: creating it first time with name: " + parent.getName());	
				}

				//now add the agent type
				if (parent.hasNode("FS")) {
					parent = parent.getNode("FS");		               
				} else {
					parent = parent.addNode("FS", "nt:folder");

					log.debug("FS not exist: creating it first time with name: " + parent.getName());	
				}

				//create the share
				for(String element:pathElements)
				{
					if(!parent.hasNode(element)){
						//if child not exist, then check for Case-Insensitivily first
						boolean exists = false;
						NodeIterator ni = parent.getNodes();
						while (ni.hasNext()) {//compare with all children
							String tempName = ni.nextNode().getName();
							if (element.equalsIgnoreCase(tempName)) {
								element = tempName;
								exists = true;
								break;//terminate the iterations if matched with any
							}
						}

						if (!exists) {
							parent = parent.addNode(element, "nt:folder");
						} else {
							parent = parent.getNode(element);
						}
						//	            		parent = parent.addNode(element, "nt:folder");

					}else{
						parent = parent.getNode(element);
					}
				}

				session.save();
				return copyNodeAllProperties(parent , new CustomTreeNode());				
			}else{
				//return the existing node
				return node;
			}

		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		


		return null;
	}

	public ArrayList<String> getAlNodeSids(Node node){

		String sids = null;
		String denySids =null;
		String shareSids = null;
		String denyShareSids = null;
		String[] sids_array = null;
		String[] denySids_array = null;
		String[] shareSids_array = null;
		String[] denyShareSids_array = null;

		ArrayList<String> allSidsList = null;

		Set<String> set = new HashSet<String>();



		try {
			if (node.hasNode(CustomNodeType.VC_RESOURCE_PERMISSIONS)) {

				node = node.getNode(CustomNodeType.VC_RESOURCE_PERMISSIONS);

				if (node.hasProperty(CustomProperty.VC_ALLOWED_SIDS)) {
					Property prop = node.getProperty(CustomProperty.VC_ALLOWED_SIDS);
					sids = prop.getString();
					log.debug("Folder AllowedSIDs " + sids);
				}
				if (node.hasProperty(CustomProperty.VC_DENIED_SIDS)) {
					Property prop = node.getProperty(CustomProperty.VC_DENIED_SIDS);
					denySids = prop.getString();
					log.debug("Folder DeniedSIDs " + denySids);
				}
				if (node.hasProperty(CustomProperty.VC_S_ALLOWED_SIDS)) {
					Property prop = node.getProperty(CustomProperty.VC_S_ALLOWED_SIDS);
					shareSids = prop.getString();
					log.debug("Folder AllowedShareSIDs " + shareSids);
				}
				if (node.hasProperty(CustomProperty.VC_S_DENIED_SIDS)) {
					Property prop = node.getProperty(CustomProperty.VC_S_DENIED_SIDS);
					denyShareSids = prop.getString();
					log.debug("Folder DenyShareSIDs " + denyShareSids);
				}

				//			    sids_array = sids.split(",");
				//			    denySids_array = denySids.split(",");
				//			    shareSids_array = shareSids.split(",");
				//			    denyShareSids_array = denyShareSids.split(",");

				//			    System.out.println(sids_array);
				//			    System.out.println(denySids_array);
				//			    System.out.println(shareSids_array);
				//			    System.out.println(denyShareSids_array);

				allSidsList = new ArrayList<String>();
				allSidsList.add(sids);
				allSidsList.add(denySids);
				allSidsList.add(shareSids);
				allSidsList.add(denyShareSids);
			}
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logError(e);
		}

		return allSidsList;
	}


	public String updateTags(String uuid, String tags){
		try{
			Node node = session.getNodeByIdentifier(uuid);
			if(node == null){
				return null;				
			}
			String documentTags = "";
			String nodeType = node.getPrimaryNodeType().getName();
			String nodePath = node.getPath();
			String nodeName=node.getName();
			boolean isFileNode = nodeType.equalsIgnoreCase("nt:file");
			boolean isFolderNode = nodeType.equalsIgnoreCase("nt:folder");
			boolean isLinkedNode = nodeType.equalsIgnoreCase("nt:linkedFile");
			NodeIterator innerIt = node.getNodes("jcr:content");
			Node innerNode = null;
			log.debug("adding tag: " + tags);
			while (innerIt != null && innerIt.hasNext()) {
				log.debug(nodeName + " , contains JCR:CONTENT Node");
				innerNode = innerIt.nextNode();
				if(innerNode.hasProperty(CustomProperty.VC_DOC_TAGS)){
					Property prop = innerNode.getProperty(CustomProperty.VC_DOC_TAGS);
					documentTags = prop.getString();
					innerNode.setProperty(CustomProperty.VC_DOC_TAGS, tags);
					log.debug("documentTags " + documentTags); 
				}else{
					innerNode.setProperty(CustomProperty.VC_DOC_TAGS, tags);
				}
			}
			session.save();
			return tags;

		}catch(RepositoryException ex){
			ex.printStackTrace();

		}


		return null;
	}

	public String updateEmailUserList(String uuid, String email){
		try{
			email = email.trim();
			Node node = session.getNodeByIdentifier(uuid);
			if(node == null){
				return null;				
			}
			String attachedEmails = "";
			String nodeType = node.getPrimaryNodeType().getName();
			String nodePath = node.getPath();
			String nodeName=node.getName();
			boolean isFileNode = nodeType.equalsIgnoreCase("nt:file");
			boolean isFolderNode = nodeType.equalsIgnoreCase("nt:folder");
			boolean isLinkedNode = nodeType.equalsIgnoreCase("nt:linkedFile");
			NodeIterator innerIt = node.getNodes("jcr:content");
			Node innerNode = null;
			log.debug("adding email: " + email);
			while (innerIt != null && innerIt.hasNext()) {
				log.debug(nodeName + " , contains JCR:CONTENT Node");
				innerNode = innerIt.nextNode();
				if(innerNode.hasProperty(CustomProperty.VC_EMAIL_USER_LIST)){
					Property prop = innerNode.getProperty(CustomProperty.VC_EMAIL_USER_LIST);
					attachedEmails = prop.getString();
					if(attachedEmails!=null)
					{
						if(!attachedEmails.contains(email))
							attachedEmails = attachedEmails + ","+ email;	//ignore if already added
					}
					else
						attachedEmails = email;
					innerNode.setProperty(CustomProperty.VC_EMAIL_USER_LIST, attachedEmails);
					log.debug("emailUserList " + attachedEmails); 
				}else{
					innerNode.setProperty(CustomProperty.VC_EMAIL_USER_LIST, email);
				}
			}
			session.save();
			return email;

		}catch(RepositoryException ex){
			ex.printStackTrace();

		}


		return null;
	}

	public String updateDocumentAccessLogs(String uuid, String email, String ipAddress){
		try{
			email = email.trim();
			Node node = session.getNodeByIdentifier(uuid);
			if(node == null){
				return null;				
			}
			String accessLogs = "";
			String nodeType = node.getPrimaryNodeType().getName();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			String logString = email+ "|" + ipAddress.trim()+ "|" + df.format(new Date());
			String nodeName=node.getName();
			boolean isFileNode = nodeType.equalsIgnoreCase("nt:file");
			boolean isFolderNode = nodeType.equalsIgnoreCase("nt:folder");
			boolean isLinkedNode = nodeType.equalsIgnoreCase("nt:linkedFile");
			NodeIterator innerIt = node.getNodes("jcr:content");
			Node innerNode = null;
			log.debug("adding accessLogs: " + accessLogs);
			while (innerIt != null && innerIt.hasNext()) {
				log.debug(nodeName + " , contains JCR:CONTENT Node");
				innerNode = innerIt.nextNode();

				if(innerNode.hasProperty(CustomProperty.VC_ACCESS_LOGS)){
					Property prop = innerNode.getProperty(CustomProperty.VC_ACCESS_LOGS);
					accessLogs = prop.getString();
					if(accessLogs!=null)
					{
						accessLogs = accessLogs + ","+ logString;
					}
					else
						accessLogs = logString;
					innerNode.setProperty(CustomProperty.VC_ACCESS_LOGS, accessLogs);
					log.debug("accessLogs " + accessLogs); 
				}else{
					accessLogs = logString;
					innerNode.setProperty(CustomProperty.VC_ACCESS_LOGS, accessLogs);
				}
			}
			session.save();
			return accessLogs;

		}catch(RepositoryException ex){
			ex.printStackTrace();

		}


		return null;
	}

	private Session session;
	private JcrTemplate jcrTemplate;

	private DriveLettersService driveLettersService;
	private ServiceManager serviceManager;
	
	private boolean winStubRestore	=	false;



	public void setServiceManager(ServiceManager serviceManager) {
		this.serviceManager = serviceManager;
		this.driveLettersService=serviceManager.getDriveLettersService();
	}

	public void setJcrTemplate(JcrTemplate jcrTemplate) {
		this.jcrTemplate = jcrTemplate;	

		try{
			session=jcrTemplate.getSessionFactory().getSession();
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	@Override
	public String restoreContentsOfStub(String secureID, String restoreLocation, String userName, boolean winStubRestore) {
		String outputXML	=	null;

		try {
			if(secureID!=null && !secureID.isEmpty() && restoreLocation!=null && !restoreLocation.isEmpty() && userName!=null && !userName.isEmpty()) {

				secureID	=	secureID.substring(secureID.lastIndexOf("/")+1);
				System.out.println("SecureID "+secureID);

				SecureURL secureURL = SecureURL.getInstance();
				String nodeID	=	secureURL.getDecryptedURL(secureID);
				nodeID	=	nodeID.substring(nodeID.indexOf("/")+1, nodeID.indexOf("?"));
				System.out.println("nodeID "+nodeID);

				String filePath = null;
				CustomTreeNode selectedNode	=	null;
				System.out.println("node id : " + nodeID);
				
				//yawarz TEMP
				//nodeID	=	"ac872202-d147-48bb-a992-03698f354279";
				//nodeID	=	"25f54c0c-bcf2-44b7-8f75-75aacbf0d57e";
				//nodeID	=	"1405a5a8-c5bb-4dc1-8e87-37ae2fac7f8c";
				
				if (nodeID != null && !(nodeID.isEmpty())) {

					selectedNode = getNodeByUUID(nodeID);

					if (selectedNode == null && nodeID.contains("/")) {
						System.out.println("getNodeByUUID not found, going to search by path");
						selectedNode = getNodeByPath(nodeID);
					}
					
					if(selectedNode==null) {
						outputXML	=	"<response><status>FAILURE</status><message>File not exist</message></response>";
						return outputXML;
					}

					/*
					ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();

					if (selectedNode != null) {
						authorize = fileExplorerService.authorizeStub(nodeID, userName);
		//				 authorize=true;
						if (!authorize) {
							String accessDeniedMessage = ru.getCodeValue(
									"accessDeniedMessage", SystemCodeType.GENERAL);
							// addErrorMessage("Access Denied, Please contact your systems administrator",
							// "Access denied");
							addErrorMessage(accessDeniedMessage, accessDeniedMessage);
							return;
						}
					}else {
						String fileNotFoundMessage = ru.getCodeValue(
								"documentNotFoundMessage", SystemCodeType.GENERAL);
						// addErrorMessage("Requested file has not been found, you may try searching for the file in the Archive or contact your System Administrator for further assistance","File not found");
						addErrorMessage(fileNotFoundMessage, fileNotFoundMessage);
						return;
					}
					// authorize = true;

					if (!authorize) {
						String accessDeniedMessage = ru.getCodeValue("accessDeniedMessage",
								SystemCodeType.GENERAL);
						// addErrorMessage("Access Denied, Please contact your systems administrator",
						// "Access denied");
						addErrorMessage(accessDeniedMessage, accessDeniedMessage);
						return;
					}*/
				} else {
					outputXML	=	"<response><status>FAILURE</status><message>Node ID cant be empty</message></response>";
					return outputXML;
				}
				// restorePath=StringUtils.decodeUrl(restorePath);
				//							if (restorePath != null && restorePath.trim().length() > 0)
				//								filePath = restorePath; // will work only if stub_access_mode is
				// set to 3 (restore)

				//if (selectedNode != null)//its NOT null in this case...
				filePath = selectedNode.getPath();

				if (filePath != null && filePath.indexOf("/") == 0) {
					filePath = filePath.substring(1);
				}
				if (restoreLocation != null && restoreLocation.trim().length() > 0) {
					restoreLocation = restoreLocation.replaceAll("\\\\", "/");
					if (restoreLocation.startsWith("//")) {
						restoreLocation = restoreLocation.substring(2);
					} else {
						outputXML	=	"<response><status>FAILURE</status><message>Invalid UNC</message></response>";
						return outputXML;
					}
				} else {
					outputXML	=	"<response><status>FAILURE</status><message>Invalid UNC</message></response>";
					return outputXML;
				}
				if(SmbWriter.inCurRestoreList(filePath)){
					outputXML	=	"<response><status>FAILURE</status><message>File Restore is already in progress</message></response>";
					return outputXML;	//exit if the file is already being restored
				}
				/*if(testUNC().equals("failure")) {
					outputXML	=	"<response><status>FAILURE</status><message>Invalid UNC Path</message></response>";
					return outputXML;
				}*/

				this.winStubRestore=	winStubRestore;
				String message = restoreFile(filePath, restoreLocation, false);

				outputXML	=	"<response><status>SUCCESS</status><message>Service Invoked</message></response>";
				
			} else {
				outputXML	=	"<response><status>FAILURE</status><message>Incomplete Input Params</message></response>";
				return outputXML;
			}
		} catch (Exception e) {
			e.printStackTrace();
			outputXML	=	"<response><status>FAILURE</status><message>"+e.getMessage()+"</message></response>";
		}

		return outputXML;
	}

@Override
public String restoreFile(String filePath, String destPath, boolean isVersion) {
	// TODO Auto-generated method stub
	boolean success=false;


	//filePath=com.virtualcode.util.StringUtils.decodeUrl(filePath);
	String message = "Error while restore";

	ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
	DriveLetters driveLetter = null;
	List<DriveLetters> driveLetterList=driveLettersService.getAll();

	for(DriveLetters letter: driveLetterList){
		String sharePath = letter.getSharePath().toLowerCase();
		if(sharePath.startsWith("\\\\")){
							
			sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase() + "/fs/" + sharePath.substring(2).replaceAll("\\\\", "/");
		}
		if(sharePath.endsWith("/"))
			sharePath = sharePath.substring(0, sharePath.lastIndexOf("/"));		
		String compareStr = destPath.trim();
		if(!compareStr.contains("/fs/"))
			compareStr = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase()+"/fs/"+destPath.trim().toLowerCase();
		if(destPath!=null && compareStr.toLowerCase().contains(sharePath)){
			driveLetter = letter;
			break;
		}
	}
	try {
		if(driveLetter!=null){
			SmbWriter writer = new SmbWriter(filePath, destPath, driveLetter.getRestoreDomain(), driveLetter.getRestoreUsername(), driveLetter.getRestorePassword(), session, isVersion, this.winStubRestore);
			writer.start();
		}else{
			SmbWriter writer = new SmbWriter(filePath, destPath, null,null, null, session, isVersion, this.winStubRestore);
			writer.start();
		}
		//SmbWriter

	} catch (Exception e) {
		// TODO Auto-generated catch block				
		e.printStackTrace();
		return e.getMessage();
	}
	if(message.indexOf("File restored successfully")!=-1)
		success=true;
	return message;
}

public String restoreFile(String filePath, String destPath, String uid, boolean isVersion) {
	// TODO Auto-generated method stub
	boolean success=false;


	//filePath=com.virtualcode.util.StringUtils.decodeUrl(filePath);
	String message = "Error while restore";

	ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
	DriveLetters driveLetter = null;
	List<DriveLetters> driveLetterList=driveLettersService.getAll();

	for(DriveLetters letter: driveLetterList){
		String sharePath = letter.getSharePath().toLowerCase();
		if(sharePath.startsWith("\\\\")){
							
			sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase() + "/fs/" + sharePath.substring(2).replaceAll("\\\\", "/");
		}
		if(sharePath.endsWith("/"))
			sharePath = sharePath.substring(0, sharePath.lastIndexOf("/"));		
		String compareStr = destPath.trim();
		if(!compareStr.contains("/fs/"))
			compareStr = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase()+"/fs/"+destPath.trim().toLowerCase();
		if(destPath!=null && compareStr.toLowerCase().contains(sharePath)){
			driveLetter = letter;
			break;
		}
	}
	try {
		//get original file path (handling versions)
		Node node = getNodeByIdOrPath(uid);
		if(node!=null){
			filePath = node.getPath().substring(1);	
		}
		if(driveLetter!=null){
			SmbWriter writer = new SmbWriter(filePath, destPath, driveLetter.getRestoreDomain(), driveLetter.getRestoreUsername(), driveLetter.getRestorePassword(), session, isVersion, this.winStubRestore);
			writer.start();
		}else{
			SmbWriter writer = new SmbWriter(filePath, destPath, null,null, null, session, isVersion, this.winStubRestore);
			writer.start();
		}
		//SmbWriter

	} catch (Exception e) {
		// TODO Auto-generated catch block				
		e.printStackTrace();
		return e.getMessage();
	}
	if(message.indexOf("File restored successfully")!=-1)
		success=true;
	return message;
}

@Override
public boolean alreadyExistsForRestore(String destPath){
	boolean exists = false;
	
	ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
	
	String domain = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
	String username = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
	String password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);

	DriveLetters driveLetter = null;
	List<DriveLetters> driveLetterList=driveLettersService.getAll();

	for(DriveLetters letter: driveLetterList){
		String sharePath = letter.getSharePath().toLowerCase();
		if(sharePath.startsWith("\\\\")){							
			sharePath = sharePath.substring(2).replaceAll("\\\\", "/");
		}
		if(sharePath.endsWith("/"))
			sharePath = sharePath.substring(0, sharePath.lastIndexOf("/"));		
		String compareStr = destPath.trim();
		if(!compareStr.contains("/fs/"))
			compareStr = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase()+"/fs/"+destPath.trim().toLowerCase();
		if(destPath!=null && compareStr.toLowerCase().contains(sharePath)){
			driveLetter = letter;
			break;
		}
	}
	try {
		//get original file path (handling versions)
//		Node node = getNodeByIdOrPath(uid);
//		if(node!=null){
//			filePath = node.getPath().substring(1);	
//		}
		
		
		if(driveLetter!=null){
			domain = driveLetter.getRestoreDomain();
			username = driveLetter.getRestoreUsername();
			password = driveLetter.getRestorePassword();
//			SmbWriter writer = new SmbWriter(filePath, destPath, driveLetter.getRestoreDomain(), driveLetter.getRestoreUsername(), driveLetter.getRestorePassword(), session, isVersion, this.winStubRestore);
//			writer.start();
		}
		
		String smbPath = "smb://"+ domain +";"+ username +":"+ password +"@"+destPath.replaceAll("\\\\", "/");
		SmbFile smbFile = new SmbFile(smbPath);
		if(smbFile.exists())
			exists = true;
	} catch (Exception e) {
		// TODO Auto-generated catch block				
		e.printStackTrace();
	}
	return exists;
}


@Override
public String archiveFile(String filePath, boolean isVersion){

	boolean success=false;        
	if(filePath!=null && filePath.startsWith("\\\\")){
		filePath = filePath.substring(2).replaceAll("\\\\", "/");
		filePath =  filePath.replaceAll("//", "/");
	}
	//filePath=com.virtualcode.util.StringUtils.decodeUrl(filePath);
	String message = "Error while restore";


	DriveLetters driveLetter = null;
	List<DriveLetters> driveLetterList=driveLettersService.getAll();

	for(DriveLetters letter: driveLetterList){
		String sharePath = letter.getSharePath().toLowerCase();
		sharePath = sharePath.substring(2).replaceAll("\\\\", "/");
		if(sharePath.startsWith("\\\\")){
			//				ResourcesUtil ru = ResourcesUtil.getResourcesUtil();				
			//				sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase() + "/fs/" + sharePath.substring(2).replaceAll("\\\\", "/");
		}
		//			if(sharePath.endsWith("/"))
		//				sharePath = sharePath.substring(0, sharePath.lastIndexOf("/"));			
		if(filePath.toLowerCase().contains(sharePath)){
			driveLetter = letter;
			break;
		}
	}
	try {
		if(driveLetter!=null){
			SingleFileArchiver archiver = new SingleFileArchiver(filePath, null, driveLetter.getRestoreDomain(), driveLetter.getRestoreUsername(), driveLetter.getRestorePassword(), session, isVersion);
			archiver.start();
		}else{
			SingleFileArchiver archiver = new SingleFileArchiver(filePath, null, null,null, null, session, isVersion);
			archiver.start();
		}
		//SmbWriter

	} catch (Exception e) {
		// TODO Auto-generated catch block				
		e.printStackTrace();
		return e.getMessage();
	}
	if(message.indexOf("File restored successfully")!=-1)
		success=true;
	return message;

}

@Override
public boolean authorizeStub(String nodePath, String username) {
	// TODO Auto-generated method stub
	try {			
		Node node = getNodeByIdOrPath(nodePath);			
		List<Acl> sidsList = getNodeSIDs(node);
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		String enable_stub_authorization = ru.getCodeValue("enable.stub.authorization", SystemCodeType.GENERAL);//resBundle.getString("enable.stub.authorization");
		if (enable_stub_authorization != null && enable_stub_authorization.equalsIgnoreCase("yes")) {                            
			VirtualCodeAuthorizatorImpl authorizator = VirtualCodeAuthorizatorImpl.getInstance();
			return authorizator.authorize(username, sidsList);
		}else{
			return true;
		}
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return false;
}

@Override
public boolean authorizeSmbFile(String nodePath, String username) {
	// TODO Auto-generated method stub
	try {			

		//				List<Acl> sidsList = new ArrayList<Acl>();

		DriveLetters driveLetter = null;
		List<DriveLetters> driveLetterList=driveLettersService.getAll();

		for(DriveLetters letter: driveLetterList){
			String sharePath = letter.getSharePath().toLowerCase();
			if(sharePath.startsWith("\\\\")){
				//   					ResourcesUtil ru = ResourcesUtil.getResourcesUtil();				
				//   					sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase() + "/fs/" + sharePath.substring(2).replaceAll("\\\\", "/");
			}
			//   				if(remotePath.endsWith("/"))
			//   					sharePath = sharePath.substring(0, sharePath.lastIndexOf("/"));
			if(!nodePath.endsWith("\\"))
				nodePath = nodePath+ "\\";
			System.out.println(sharePath + ", " + nodePath.toLowerCase());
			if(nodePath.toLowerCase().contains(sharePath)){
				driveLetter = letter;
				break;
			}
		}
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		String domain = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
		String user = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
		String password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
		if(driveLetter!=null){
			domain =  driveLetter.getRestoreDomain();
			user = driveLetter.getRestoreUsername();
			password = driveLetter.getRestorePassword();
		}

		String searchPath = "smb://"+ domain +";"+ user +":"+ password +"@"+nodePath.replaceAll("\\\\", "/");
		SmbFile smbFile;
		try {
			smbFile = new SmbFile(searchPath);
			if(smbFile.isFile()){
				HashMap aclAttribs   =   CifsUtil.getAclAttribs(getACL(smbFile));
				List sidsList = new ArrayList();

				ArrayList<String> allowAces = new ArrayList<String>();
				ArrayList<String> denyAces = new ArrayList<String>();
				ArrayList<String> shareAllowAces = new ArrayList<String>();
				ArrayList<String> shareDenyAces = new ArrayList<String>();

				if (aclAttribs.get("vc:shareAllowedSIDs")!=null) {
					String[] allowSids = aclAttribs.get("vc:shareAllowedSIDs").toString().split(",");
					shareAllowAces.addAll(Arrays.asList(allowSids));
				}
				sidsList.add(shareAllowAces);

				if (aclAttribs.get("vc:shareDeniedSIDs")!=null) {	                        
					String[] denySids = aclAttribs.get("vc:shareDeniedSIDs").toString().split(",");
					shareDenyAces.addAll(Arrays.asList(denySids));
				}
				sidsList.add(shareDenyAces);

				if (aclAttribs.get("vc:allowedSIDs")!=null) {	                        
					String[] allowSids = aclAttribs.get("vc:allowedSIDs").toString().split(",");
					allowAces.addAll(Arrays.asList(allowSids));
				}
				sidsList.add(allowAces);

				if (aclAttribs.get("vc:deniedSIDs")!=null) {
					String[] denySids = aclAttribs.get("vc:deniedSIDs").toString().split(",");
					denyAces.addAll(Arrays.asList(denySids));
				}
				sidsList.add(denyAces);
				String enable_stub_authorization = ru.getCodeValue("enable.stub.authorization", SystemCodeType.GENERAL);//resBundle.getString("enable.stub.authorization");
				if (enable_stub_authorization != null && enable_stub_authorization.equalsIgnoreCase("yes")) {                            
					VirtualCodeAuthorizatorImpl authorizator = VirtualCodeAuthorizatorImpl.getInstance();
					return authorizator.authorize(username, sidsList);
				}else{
					return true;
				}	   				

			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SmbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return false;
}

@Override
public boolean authorizeSmbFile(SmbFile smbFile, String username) {
	// TODO Auto-generated method stub
	try {			

		//				List<Acl> sidsList = new ArrayList<Acl>();

		//			 DriveLetters driveLetter = null;
		//	   	      List<DriveLetters> driveLetterList=driveLettersService.getAll();
		//	   	      
		//	   	      for(DriveLetters letter: driveLetterList){
		//	   				String sharePath = letter.getSharePath().toLowerCase();
		//	   				if(sharePath.startsWith("\\\\")){
		//	//   					ResourcesUtil ru = ResourcesUtil.getResourcesUtil();				
		//	//   					sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase() + "/fs/" + sharePath.substring(2).replaceAll("\\\\", "/");
		//	   				}
		//	//   				if(remotePath.endsWith("/"))
		//	//   					sharePath = sharePath.substring(0, sharePath.lastIndexOf("/"));
		//	   				if(!nodePath.endsWith("\\"))
		//	   					nodePath = nodePath+ "\\";
		//	   				System.out.println(sharePath + ", " + nodePath.toLowerCase());
		//	   	      	if(nodePath.toLowerCase().contains(sharePath)){
		//	   	      		driveLetter = letter;
		//	   	      		break;
		//	   	      	}
		//	   	      }
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		//	        	String domain = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
		//	       		String user = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
		//	       		String password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
		//	       		if(driveLetter!=null){
		//	       			domain =  driveLetter.getRestoreDomain();
		//	       			user = driveLetter.getRestoreUsername();
		//	       			password = driveLetter.getRestorePassword();
		//	       		}
		//	       		
		//	       		String searchPath = "smb://"+ domain +";"+ user +":"+ password +"@"+nodePath.replaceAll("\\\\", "/");
		//	   			SmbFile smbFile;
		try {
			//					smbFile = new SmbFile(searchPath);
			if(smbFile.isFile()){
				HashMap aclAttribs   =   CifsUtil.getAclAttribs(getACL(smbFile));
				List sidsList = new ArrayList();

				ArrayList<String> allowAces = new ArrayList<String>();
				ArrayList<String> denyAces = new ArrayList<String>();
				ArrayList<String> shareAllowAces = new ArrayList<String>();
				ArrayList<String> shareDenyAces = new ArrayList<String>();

				if (aclAttribs.get("vc:shareAllowedSIDs")!=null) {
					String[] allowSids = aclAttribs.get("vc:shareAllowedSIDs").toString().split(",");
					shareAllowAces.addAll(Arrays.asList(allowSids));
				}
				sidsList.add(shareAllowAces);

				if (aclAttribs.get("vc:shareDeniedSIDs")!=null) {	                        
					String[] denySids = aclAttribs.get("vc:shareDeniedSIDs").toString().split(",");
					shareDenyAces.addAll(Arrays.asList(denySids));
				}
				sidsList.add(shareDenyAces);

				if (aclAttribs.get("vc:allowedSIDs")!=null) {	                        
					String[] allowSids = aclAttribs.get("vc:allowedSIDs").toString().split(",");
					allowAces.addAll(Arrays.asList(allowSids));
				}
				sidsList.add(allowAces);

				if (aclAttribs.get("vc:deniedSIDs")!=null) {
					String[] denySids = aclAttribs.get("vc:deniedSIDs").toString().split(",");
					denyAces.addAll(Arrays.asList(denySids));
				}
				sidsList.add(denyAces);
				String enable_stub_authorization = ru.getCodeValue("enable.stub.authorization", SystemCodeType.GENERAL);//resBundle.getString("enable.stub.authorization");
				//	                    if (enable_stub_authorization != null && enable_stub_authorization.equalsIgnoreCase("yes")) {                            
				VirtualCodeAuthorizatorImpl authorizator = VirtualCodeAuthorizatorImpl.getInstance();
				return authorizator.authorize(username, sidsList);
				//	                    }else{
				//	                    	return true;
				//	                    }	   				

			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SmbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return false;
}

//	@Override
//	public boolean authorizeSmbDir(String nodePath, String username) {
//		// TODO Auto-generated method stub
//		try {			
//		        
////				List<Acl> sidsList = new ArrayList<Acl>();
//
//		        DriveLetters driveLetter = null;
//		        List<DriveLetters> driveLetterList=driveLettersService.getAll();
//		        
//		        for(DriveLetters letter: driveLetterList){
//					String sharePath = letter.getSharePath().toLowerCase();
//					if(sharePath.startsWith("\\\\")){
//						ResourcesUtil ru = ResourcesUtil.getResourcesUtil();				
//						sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase() + "/fs/" + sharePath.substring(2).replaceAll("\\\\", "/");
//					}
//					if(sharePath.endsWith("/"))
//						sharePath = sharePath.substring(0, sharePath.lastIndexOf("/"));			
//		        	if(nodePath.toLowerCase().contains(sharePath)){
//		        		driveLetter = letter;
//		        		break;
//		        	}
//		        }
//	        	ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
//	        	String domain = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
////	       		String username = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
//	       		String password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
//	       		if(driveLetter!=null){
//	       			domain =  driveLetter.getRestoreDomain();
////	       			username = driveLetter.getRestoreUsername();
//	       			password = driveLetter.getRestorePassword();
//	       		}
//	       		
//	       		String searchPath = "smb://"+ domain +";"+ username +":"+ password +"@"+nodePath.replaceAll("\\\\", "/");
//	   			SmbFile smbFile;
//				try {
//					smbFile = new SmbFile(searchPath);
//		   			if(smbFile.isDirectory()){
//		   				HashMap aclAttribs   =   CifsUtil.getAclAttribs(getACL(smbFile));
//		   				List sidsList = new ArrayList();
//		   				
//	                    ArrayList<String> allowAces = new ArrayList<String>();
//	                    ArrayList<String> denyAces = new ArrayList<String>();
//	                    ArrayList<String> shareAllowAces = new ArrayList<String>();
//	                    ArrayList<String> shareDenyAces = new ArrayList<String>();
//		   				
//	                    if (aclAttribs.get("vc:shareAllowedSIDs")!=null) {
//	                        String[] allowSids = aclAttribs.get("vc:shareAllowedSIDs").toString().split(",");
//	                        shareAllowAces.addAll(Arrays.asList(allowSids));
//	                    }
//	                    sidsList.add(shareAllowAces);
//
//	                    if (aclAttribs.get("vc:shareDeniedSIDs")!=null) {	                        
//	                        String[] denySids = aclAttribs.get("vc:shareDeniedSIDs").toString().split(",");
//	                        shareDenyAces.addAll(Arrays.asList(denySids));
//	                    }
//	                    sidsList.add(shareDenyAces);
//
//	                    if (aclAttribs.get("vc:allowedSIDs")!=null) {	                        
//	                        String[] allowSids = aclAttribs.get("vc:allowedSIDs").toString().split(",");
//	                        allowAces.addAll(Arrays.asList(allowSids));
//	                    }
//	                    sidsList.add(allowAces);
//
//	                    if (aclAttribs.get("vc:deniedSIDs")!=null) {
//	                        String[] denySids = aclAttribs.get("vc:deniedSIDs").toString().split(",");
//	                        denyAces.addAll(Arrays.asList(denySids));
//	                    }
//	                    sidsList.add(denyAces);
//	                    String enable_stub_authorization = ru.getCodeValue("enable.stub.authorization", SystemCodeType.GENERAL);//resBundle.getString("enable.stub.authorization");
////	                    if (enable_stub_authorization != null && enable_stub_authorization.equalsIgnoreCase("yes")) {                            
//	                        VirtualCodeAuthorizatorImpl authorizator = VirtualCodeAuthorizatorImpl.getInstance();
//	                        return authorizator.authorize(username, sidsList);
////	                    }else{
////	                    	return true;
////	                    }	   				
//		   			
//		   			}
//				} catch (MalformedURLException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (SmbException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return false;
//	}

public Node getNodeByIdOrPath(String nodePath){
	Node node = null;
	try {
		node = session.getNodeByIdentifier(nodePath);

	} catch (Exception e) {
		log.debug("Node by UUID not found,so assuming path");	
	}
	if(node == null){
		try {
			if(nodePath.indexOf("/")==0)
				nodePath=nodePath.substring(1);

			node=session.getRootNode();
			node=node.getNode(nodePath);
		} catch (ItemNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			node = null;
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			node = null;
		}
	}
	return node;
}

/*Input Parameter:
 * 1. file path
 * 2. encoded credentials
 * Returns:  List Object with four different lists ( Allow SID, Deny SID, Shared Allow SID, Shared Deny SID)
 */
public ArrayList<Acl> getNodeSIDs(Node file) {
	//Session session = null;
	Node root = null;
	ArrayList sids = new ArrayList();

	try {

		if (file != null) {

			String nodeName = file.getName();

			//Get the properties of this Node
			NodeIterator innerIt = null;
			Node innerNode = null;
			innerIt = file.getNodes("jcr:content");
			if (innerIt == null) {
				throw new RepositoryException("Content node not found");
			} else if (innerIt.getSize() < 1) {
				throw new RepositoryException("Content node not found");
			}
			while (innerIt != null && innerIt.hasNext()) {

				ArrayList<String> allowAces = new ArrayList<String>();
				ArrayList<String> denyAces = new ArrayList<String>();
				ArrayList<String> shareAllowAces = new ArrayList<String>();
				ArrayList<String> shareDenyAces = new ArrayList<String>();

				log.debug(nodeName + " , contains JCR:CONTENT Node");
				innerNode = innerIt.nextNode();
				if (innerNode.hasProperty(CustomProperty.VC_ALLOWED_SIDS)) {

					Property prop = innerNode.getProperty(CustomProperty.VC_ALLOWED_SIDS);
					String[] allowSids = prop.getString().split(",");
					allowAces.addAll(Arrays.asList(allowSids));
				}
				sids.add(allowAces);

				if (innerNode.hasProperty(CustomProperty.VC_DENIED_SIDS)) {
					Property prop = innerNode.getProperty(CustomProperty.VC_DENIED_SIDS);
					String[] denySids = prop.getString().split(",");
					denyAces.addAll(Arrays.asList(denySids));
				}
				sids.add(denyAces);

				if (innerNode.hasProperty(CustomProperty.VC_S_ALLOWED_SIDS)) {
					Property prop = innerNode.getProperty(CustomProperty.VC_S_ALLOWED_SIDS);
					String[] sharedAllowSids = prop.getString().split(",");
					shareAllowAces.addAll(Arrays.asList(sharedAllowSids));
				}
				sids.add(shareAllowAces);

				if (innerNode.hasProperty(CustomProperty.VC_S_DENIED_SIDS)) {
					Property prop = innerNode.getProperty(CustomProperty.VC_S_DENIED_SIDS);
					String[] sharedDenySids = prop.getString().split(",");
					shareDenyAces.addAll(Arrays.asList(sharedDenySids));
				}
				sids.add(shareDenyAces);
			}
		} else {
			throw new RepositoryException("File not found");
		}
	} catch (RepositoryException ex) {
		//this.logError(ex);
		ex.printStackTrace();
	} finally {
		try {
			if (session != null) {
				session.save();
				log.debug("Repo Session saved Successfully!");

				// session.logout();
				log.debug("User logout Successfully from Repo Session!");
			} else {
				log.error("Handled Error: Repo Session not exist");
			}
		} catch (Exception ex) {
			//this.logError(ex);
			ex.printStackTrace();

		}
	}
	return sids;
}

public Double getPathExportSize(String path){
	if(1==1){
		return 0d;
	}
	List<Node> nodeList = getAllChildren(path);
	//    	Node node = getNodeByPathForExport(path);
	Double fileSize = 0.0d;

	if(nodeList!=null && nodeList.size()>0){
		for(Node node:nodeList){
			String nodeType;
			try {
				nodeType = node.getPrimaryNodeType().getName();
			} catch (RepositoryException e) {
				// TODO Auto-generated catch block
				this.logError(e);
				e.printStackTrace();
				nodeType="";
			}
			if(node!=null){
				if(nodeType.equalsIgnoreCase("nt:folder")){
					try {
						fileSize = getTotalFolderSize(node, fileSize);
					} catch (RepositoryException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					CustomTreeNode customNode = copyNodeFewerProperties(node, new CustomTreeNode());
					if(customNode!=null && customNode.getFileSize()!=null){
						//		    				System.out.println(customNode.getFileSize()+"----------------------"+" name: " + customNode.getName());
						fileSize = fileSize + new Double(customNode.getFileSize().replace(" KB", ""));
					}
					//		    			nodeList.remove(node);
					//		    			if(){
					//		    				
					//		    			}

				}

			}
		}
	}
	//    	System.out.println(fileSize);
	return Math.ceil(fileSize);
}

public Double getTotalFolderSize(Node node, Double fileSize) throws RepositoryException{
	//    	Long fileSize = 0l;
	List<Node> nodeList = getAllChildren(node.getPath());
	for(Node child:nodeList){
		String nodeType;
		try {
			nodeType = child.getPrimaryNodeType().getName();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			this.logError(e);
			e.printStackTrace();
			nodeType="";
		}
		if(nodeType.equals("nt:folder")){
			fileSize = getTotalFolderSize(child, fileSize);
		}else{
			CustomTreeNode customNode = copyNodeFewerProperties(child, new CustomTreeNode());
			if(customNode!=null && customNode.getFileSize()!=null){
				//    				System.out.println(customNode.getFileSize()+"----------------------"+" name: " + customNode.getName());
				fileSize = fileSize + new Double(customNode.getFileSize().replace(" KB", ""));
				//    					return Math.ceil(fileSize);
			}
		}
	}
	return Math.ceil(fileSize);
}

public Long getPathExportStubsSize(String path){
	List<Node> nodeList = getAllChildren(path);
	//    	Node node = getNodeByPathForExport(path);
	Long fileSize = 0l;
	if(nodeList!=null && nodeList.size()>0){
		for(Node node:nodeList){
			String nodeType;
			try {
				nodeType = node.getPrimaryNodeType().getName();
			} catch (RepositoryException e) {
				// TODO Auto-generated catch block
				this.logError(e);
				e.printStackTrace();
				nodeType="";
			}
			if(node!=null){
				if(nodeType.equalsIgnoreCase("nt:folder")){
					try {
						fileSize = getTotalFolderStubsSize(node, fileSize);
					} catch (RepositoryException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else if(!nodeType.equalsIgnoreCase("vc:resource_permissions")){
					//CustomTreeNode customNode = copyNodeFewerProperties(node, new CustomTreeNode());
					//if(customNode!=null && customNode.getFileSize()!=null){
					//System.out.println(customNode.getFileSize()+"----------------------"+" name: " + customNode.getName());
					fileSize = fileSize + 1l;
					//}


					//		    			}

				}

			}
		}
	}
	//    	System.out.println(fileSize);
	return fileSize;
}

public Long getTotalFolderStubsSize(Node node, Long fileSize) throws RepositoryException{
	//    	Long fileSize = 0l;
	List<Node> nodeList = getAllChildren(node.getPath());
	for(Node child:nodeList){
		String nodeType;
		try {
			nodeType = child.getPrimaryNodeType().getName();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			this.logError(e);
			e.printStackTrace();
			nodeType="";
		}
		if(nodeType.equals("nt:folder")){
			fileSize = getTotalFolderStubsSize(child, fileSize);
		}else{
			//    			CustomTreeNode customNode = copyNodeFewerProperties(child, new CustomTreeNode());
			//    			if(customNode!=null && customNode.getFileSize()!=null){
			//    				System.out.println(customNode.getFileSize()+"----------------------"+" name: " + customNode.getName());
			fileSize = fileSize + 1l;	//just increase teh suze by 1 in case its a stub only
			//    					return Math.ceil(fileSize);
			//    			}
		}
	}
	return fileSize;
}    

public Long getRestoreProgress(Long startTime){        
	//            return com.virtualcode.cifs.SmbWriter.getProgress();
	return 0l;
}

public ArrayList<Node> getAllChildren(String parent) {

	ArrayList<Node> children = null;
	Node root;
	//	        secureInfo = layerAttribs;
	//	        log.debug("Secure Info extracted in RepositryOpManager: " + secureInfo);
	//	            un comment in production
	//	        SimpleCredentials creds = SecurityUtil.decodeSecureInfo(secureInfo);
	//	          un comment in development
	//	        SimpleCredentials creds = new SimpleCredentials("admin", "admin".toCharArray());

	try {

		root = session.getRootNode();

		Node desiredNode = getNodeByPathForExport(parent);//(root, parent);

		if (desiredNode != null) {

			String nodeType = desiredNode.getPrimaryNodeType().getName();
			children = new ArrayList<Node>();

			if(nodeType.equalsIgnoreCase("nt:folder")) {//if its a folder path, having children

				log.debug("desired path is a folder");

				NodeIterator it = desiredNode.getNodes();
				if (it != null) {

					while (it.hasNext()) {
						Node node = it.nextNode();

						//		                        Document document	= this.populateDocumentVO(node, secureInfo);

						children.add(node);
					}
				} else {
					new RepositoryException("no children found at the given path");
				}

			} else {//if its a file / linked

				log.info("returning desired file");
				//	            		Document document	= this.populateDocumentVO(desiredNode, secureInfo);

				children.add(desiredNode);
			}
		} else {
			new RepositoryException("Invalid path");
		}

	} catch (LoginException ex) {
		System.out.println(ex);
		ex.printStackTrace();
		this.logError(ex);
	} catch (RepositoryException ex) {
		System.out.println(ex);
		ex.printStackTrace();
		this.logError(ex);
	} catch (Exception ex) {
		System.out.println(ex);
		ex.printStackTrace();
		this.logError(ex);
		log.error(ex.getMessage());
	} 
	return children;
}

public Acl getACL(SmbFile smbFile) throws IOException {
	Acl acl =   null;

	//Get the Original Security ACL
	ACE[] aceList    =   smbFile.getSecurity();
	String documentPath = smbFile.getPath();

	String mapShareInfo =   Utility.GetProp("mapShareInfo");
	if ("1".equals(mapShareInfo)) {//if has to retrieve the ShareSecurity from file
		//String mapFilePath  =   Utility.GetProp("mapFilePath");
		String mapFilePath = "/mapingFile.properties";
		URL url 	= getClass().getResource(mapFilePath);
		if(url!=null)
			mapFilePath	=	url.getPath();

		//System.out.println("Full doc path: "+documentPath);
		String entryFor     =   documentPath.substring(0, Utility.nthOccurrence(documentPath, '/', 1));//
		//System.out.println("Get entry from mapFilePath: "+entryFor);
		String mapingEntry  =   Utility.getMapingEntry(mapFilePath, entryFor);

		//if(mapingEntry!=null) {
		//Transform the original ACL into customized ACL
		acl =   AuthorizationUtil.transformACE(aceList, mapingEntry);
		//}
	} else {//if get original ShareSecurity, instead of maping file...
		//Get the Share Security ACL
		ACE[] aceShareList    =   smbFile.getShareSecurity(true);

		//Transform the original ACL into customized ACL
		acl =   AuthorizationUtil.transformACE(aceList, aceShareList);
	}

	return acl;
}

//modified form of getNodeByPath method to return simple node instead of CustomTreeNode
public Node getNodeByPathForExport(String nodePath) {

	boolean found = false;

	try {

		try{
			if(nodePath.indexOf("/")==0)
				nodePath=nodePath.substring(1);

			Node jcrNode=session.getRootNode();
			jcrNode=jcrNode.getNode(nodePath);
			found = true;
			return jcrNode;

		}catch(Exception ex){

		}

		//if not found then check for case-insensitivity
		if(!found){
			log.info("path not found, checking using case-insensitivity");
			try {
				Node jcrNode=session.getRootNode();

				Node parent = jcrNode;
				String[] pathElements = com.virtualcode.util.StringUtils.getPathElements(nodePath);
				String element = null;
				Node node = null;
				for (int i = 0; i < pathElements.length; i++) {
					element = pathElements[i];
					if (element.length() == 0) {
						continue;
					}
					found = false;
					//element = URLDecoder.decode(element, "utf-8");
					log.debug("Element: " + element);
					if (!parent.hasNode(element)) {
						//if child not exist, then check for Case-Insensitivity first
						boolean exists = false;
						NodeIterator ni = parent.getNodes();
						while (ni.hasNext()) {//compare with all children
							String tempName = ni.nextNode().getName();
							if (element.equalsIgnoreCase(tempName)) {
								element = tempName;
								exists = true;
								break;//terminate the iterations if matched with any
							}
						}

						if (exists) {
							node = parent.getNode(element);
							found = true;
						}else{
							return null;	// not found after case insensitivity
						}
						parent = node;
						//		                    System.out.println(parent.getPath());


					} else {
						//log.debug("Node exist");
						node = parent.getNode(element);
						parent = node;
						found = true;
					}
					log.debug("Element after case matching: " + element);
					if(!found)
						return null;
				}
				return parent;
			} catch (RepositoryException e) {
				return null;
			} catch(Exception e){
				e.printStackTrace();
			}
		}

	}catch (Exception e) {
		return null;
	}
	return null;	
}

private void logError(Exception ex) {
	log.error("logError: " + ex.getMessage());

	StringWriter sw = new StringWriter();
	PrintWriter pw = new PrintWriter(sw);
	ex.printStackTrace(pw);
	log.error(sw.toString()); // stack trace as a string

}

}
