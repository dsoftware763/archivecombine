package com.virtualcode.repository.impl;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;

import javax.jcr.Binary;
import javax.jcr.ItemNotFoundException;
import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import javax.jcr.Value;
import javax.jcr.ValueFactory;
import javax.jcr.ValueFormatException;
import javax.jcr.nodetype.NodeDefinition;
import javax.jcr.nodetype.NodeType;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import javax.xml.ws.WebServiceException;

import org.apache.jackrabbit.api.management.DataStoreGarbageCollector;
import org.apache.jackrabbit.core.NodeImpl;
import org.apache.jackrabbit.core.SessionImpl;
import org.apache.jackrabbit.core.id.NodeId;
import org.apache.log4j.Logger;
import org.springmodules.jcr.JcrTemplate;

import com.virtualcode.agent.das.archivePolicy.dto.JobStatistics;
import com.virtualcode.agent.das.archivePolicy.dto.SPDocLib;
import com.virtualcode.agent.das.dataArchiver.JobStarter;
import com.virtualcode.agent.das.dataRetainer.RetentionHandler;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.cifs.Document;
import com.virtualcode.common.CustomNodeType;
import com.virtualcode.common.CustomProperty;
import com.virtualcode.common.SecureURL;
import com.virtualcode.common.VCSConstants;
import com.virtualcode.repository.UploadDocumentService;
import com.virtualcode.repository.monitor.StatusMonitor;
import com.virtualcode.services.MailService;
import com.virtualcode.services.SystemCodeService;
import com.virtualcode.util.JespaUtil;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.SecurityUtil;
import com.virtualcode.util.SpringApplicationContext;
import com.virtualcode.util.StringUtils;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.CustomTreeNode;
import com.virtualcode.vo.SystemCode;
import com.virtualcode.vo.SystemCodeType;

public class UploadDocumentServiceImpl implements UploadDocumentService {
	
		
	Logger log=Logger.getLogger(UploadDocumentServiceImpl.class);
	
//	private ResourceBundle bundle = ResourceBundle.getBundle("config");
	//private String transID	=	"default";
    
    
    private SecureURL secureURL = SecureURL.getInstance();
    
    private StatusMonitor statusObj = StatusMonitor.getInstance();
    
    
    public UploadDocumentServiceImpl(){
    	
    }
    
//    private void startThreadToSave(String fileName, StatusMonitor statsObj) {
//        Timer timer = new Timer();
//        timer.schedule(new StatsSavingTask(fileName, statsObj), 1 * 1000, 10 * 1000);//Start after 1 seconds, and repeats every 10sec
//    }
    
    private static RepositoryUtil repoUtil		=	null;
    private final static int MAX_SESSIONS	=	50;
    
    private synchronized RepositoryUtil getRepository() {
    	if (repoUtil == null) {
    	    try {
    	    	repoUtil	=	new RepositoryUtil(MAX_SESSIONS);
    	    } catch (Exception e) {
    	    	throw new RuntimeException(e);
    	    }
    	}
	    return repoUtil;
    }
    
    public long removeAllDocuments(Set<SPDocLib> allPaths, String encodedSecureInfo, String executionID, Logger logger) {

    	long removedCount	=	0l;
    	try {
        	//javax.jcr.Repository repo=session.getRepository();
        	RepositoryUtil	ru	=	getRepository();	//initializes SessionManager and Repostiory instance
        	
            SimpleCredentials creds = SecurityUtil.decodeSecureInfo(encodedSecureInfo);

            if (repository == null || creds == null) {
                throw new LoginException("Repository object isn't initialized");
            }
            
            //Session session = repo.login(creds);
            Session session = ru.getSession(repository,  "default",creds);//new SimpleCredentials("username"+Math.random(), "password".toCharArray()));
            
            logger.info("B4 removal folders: "+this.statusObj.getTotalFolders() +" files: "+this.statusObj.getUniqueFiles());
            long count	=	0l;
            for (SPDocLib p : allPaths) {
            	
            	count++;
            	//Terminate the current Thread, if CANCEL is requested by User
            	//check for cancellation after each 50 files (to keep optimum performance)
    	    	if(count%50==0 && RetentionHandler.isInterupted(executionID)) {
    	    		logger.info("Interupted by User to Cancle the job");
    	    		break;//break the iterations further
    	    	}
            	
            	String uid	=	p.getDestinationPath();
            	if(p.isRecursive()) {//if marked for removal by the Approval authority
	            	removedCount	+=	this.removeDocument(session, uid, logger);
	            	
            	} else {
            		logger.info("Not marked for removal: "+uid);
            	}
            }//ending loop
            
            logger.info("After removal folders: "+this.statusObj.getTotalFolders() +" files: "+this.statusObj.getUniqueFiles());

            //Run the Garbage collector to permanently delete files from Data Store
            ////////////////////////////
            logger.info("Garbage collector started at: "+System.currentTimeMillis());
            //JackrabbitRepositoryFactory rf = new RepositoryFactoryImpl();

            //JackrabbitRepository rep = (JackrabbitRepository) repository;
            //RepositoryManager rm = rf.getRepositoryManager(rep);

            DataStoreGarbageCollector gc = ((SessionImpl)session).createDataStoreGarbageCollector();//rm.createDataStoreGarbageCollector();
            try {
                gc.mark();
                gc.sweep();
            } finally {
                gc.close();
            }

            //session.logout();
            //////////rm.stop();
            logger.info("Garbage collected at: "+System.currentTimeMillis());
            ///////////
            
        } catch (LoginException ex) {
            this.logError(ex, executionID);
            logger.error(ex.getMessage());
            
        } catch (RepositoryException ex) {
            this.logError(ex, executionID);
            logger.error(ex.getMessage());
            
        } finally {
        	////yawarz - ALIT ///if(parent!=null) {
        	////yawarz - ALIT ///releaseLock(parent.getIdentifier());
        	////yawarz - ALIT ///}
        }
    	
		return removedCount;    	
    }

	private long removeDocument(Session session, String uid, Logger logger) throws RepositoryException {

		long result	=	0l;
    	//uid	=	"a18166f0-0b00-46aa-82a5-c3782294e673";
		logger.info("Removing: "+uid);
		try {
            Node targetNode = session.getNodeByIdentifier(uid);//throws PathNotFoundException
	        if(targetNode!=null) {
	        	logger.info("Target node: "+targetNode.getPrimaryNodeType().getName()+" "+targetNode.getPath());
	        	
	        	this.removeNode(session, targetNode, logger);
	        	result	=	1l;
	        	
	        } else {
	        	logger.info("Not Found: "+uid);
	        	result	=	0;
	        }

        } catch(PathNotFoundException pne) {
        	logger.info("Node already removed or Not exist: "+uid+" - "+pne.getMessage());
        	result	=	0;
        	
        } catch(ItemNotFoundException inf) {
        	logger.info("Node already removed or Not exist: "+uid+" - "+inf.getMessage());
        	result	=	0;
        }
		return result;
	}
	
	private void removeNode(Session session, Node targetNode, Logger logger) throws RepositoryException {

    	if("nt:file".equalsIgnoreCase(targetNode.getPrimaryNodeType().getName())) {//if a nt:file is being deleted
    		
			logger.info("case of nt:file");
			long nodeSize	=	0l;
			try {//Get and load the Node size (in byte length)
				NodeIterator content = targetNode.getNodes("jcr:content");
		        if (content != null && content.hasNext()) {
		        	logger.debug("it has content");
		            Node contentNode = content.nextNode();

		            try {
		            	//code modified to get 
			            if(contentNode.hasProperty(CustomProperty.VC_LINKED_TO)) {//if its a LinkedNode (pointing to some Original)
			            	
			            	logger.debug("nt:file has propeorty of "+CustomProperty.VC_LINKED_TO);
			            	Property prop  =   contentNode.getProperty(CustomProperty.VC_LINKED_TO);
			            	String refNodeID   =   (prop!=null)?prop.getString():null;
			            	
			            	if(refNodeID!=null) {
			            		logger.debug("retrieve orignal node by ID");
			            		
			            		String allLinkNodes	=	null;
			            		//modified due to error
			            		Node origNode    =   globalSession.getNodeByIdentifier(refNodeID);//.getParent();//throws ItemNotFoundExp
			            		
			            		//prepare the list of Available links pointing to same Original
			            		if(origNode.hasProperty(CustomProperty.VC_LINK_NODES)){
									Property prp = origNode.getProperty(CustomProperty.VC_LINK_NODES);
									allLinkNodes   =   (prp!=null)?prp.getString():null;
									logger.info("origNode contains "+allLinkNodes);
								}

			            		if(allLinkNodes!=null && !allLinkNodes.isEmpty()) {//remove the current LinkedNode from references
			            			
			            			//Update the property of Orignal node (by removing nodeID of cur linked from VC_DOC_URL)
			            			logger.info("replace/remove "+targetNode.getIdentifier()+" from linkNodesList of origNode");
			            			allLinkNodes.replaceAll(targetNode.getIdentifier(), "");
			            			
			            			//set updated linkNodesList in origNode
			            			logger.debug("modified value: "+allLinkNodes);
			            			origNode.setProperty(CustomProperty.VC_LINK_NODES, allLinkNodes);
			            		}

			            		//if origNode is also marked for removal AND all Linked to this orignal are Removed..
			            		if((allLinkNodes==null || allLinkNodes.isEmpty()) &&
			            				origNode.hasProperty(CustomProperty.VC_IS_REMOVED) &&
			            				"yes".equalsIgnoreCase(origNode.getProperty(CustomProperty.VC_IS_REMOVED).toString())){
			            			
			            			logger.info("orig node is qualified to be removed." + origNode.getIdentifier());
			            			origNode = origNode.getParent();//modified
			            			
			            			Node parent	=	origNode.getParent();
			            			
			            			origNode.remove();
			            			origNode	=	null;
			            			session.save();
			            			
			            			if(!parent.hasNodes()) {//if parent folder is empty
			                    		removeNode(session, parent, logger);//try to remove parent folder (if become empty)
			                    	}
			            		}
			            	}
			            } else {
			            	logger.debug("targetnode is not a linked node..");
			            }
			            
		            } catch (ItemNotFoundException infe) {
		            	logger.info("Original NOT found for the linked, so remove linked");
		            }
		            
		            if (contentNode.hasProperty("jcr:data")) {
		            	nodeSize	=	contentNode.getProperty("jcr:data").getLength();
	            	} else {
	            		logger.warn("size NOT found for targetNode");
	            	}
		        }
			} catch (RepositoryException re) {
				logger.error("Exception to getNode size: "+re.getMessage());
				LoggingManager.printStackTrace(re, logger);
			}			
			
			Node parent	=	targetNode.getParent();
			Node content = targetNode.getNode("jcr:content");//modified
			
			String allLinkNodes	=	null;
//			if(targetNode.hasProperty(CustomProperty.VC_LINK_NODES)){
//				Property prp = targetNode.getProperty(CustomProperty.VC_LINK_NODES);
			if(content.hasProperty(CustomProperty.VC_LINK_NODES)){
				Property prp = content.getProperty(CustomProperty.VC_LINK_NODES);				
				allLinkNodes   =   (prp!=null)?prp.getString():null;
				logger.info("targetNode contains "+allLinkNodes);
			}
    		
    		if(allLinkNodes==null || allLinkNodes.isEmpty()){// There isn't any link node, pointing to this

    			logger.info("removing targetNode from JR");
    			targetNode.remove();
    			targetNode	=	null;
            	session.save();
            	
            	//increment the NEGATIVE value (in case of removal/decrement)
            	this.statusObj.incUniqueFilesCounter("FS", -1, -nodeSize);
            	
    		} else {
    			//Mark targetNode as REMOVED
    			logger.info("marking targetNode as removed ");
//    			targetNode.setProperty(CustomProperty.VC_IS_REMOVED, "yes");modified
    			content.setProperty(CustomProperty.VC_IS_REMOVED, "yes");
    			session.save();
    			targetNode	=	null;
    			
    			this.statusObj.incUniqueFilesCounter("FS", -1, -nodeSize);
    		}
    		
        	if(!parent.hasNodes()) {//if parent folder is empty
        		removeNode(session, parent, logger);//try to remove parent folder (if become empty)
        	}
			
		} else if("nt:folder".equalsIgnoreCase(targetNode.getPrimaryNodeType().getName())) {//if target is a nt:folder
			logger.info("case of nt:folder");
			Node parent	=	targetNode.getParent();
			
			targetNode.remove();
			targetNode=null;
        	session.save();

        	//decrement by 1 folder
        	this.statusObj.incTotalFolders(-1l);
        	
			if(!parent.hasNodes()) {//if parent folder is empty
				removeNode(session, parent, logger);	
			}
			
		} else {//if a file is being deleted
			logger.info("case of jcr_content");

			long nodeSize	=	0l;
			try {//Get and load the Node size (in byte length)
				NodeIterator content = targetNode.getNodes("jcr:content");
		        if (content != null && content.hasNext()) {
		        	logger.debug("it has content");
		            Node contentNode = content.nextNode();
	
		            if (contentNode.hasProperty("jcr:data")) {
		            	nodeSize	=	contentNode.getProperty("jcr:data").getLength();
	            	} else {
	            		logger.warn("size NOT found for targetNode");
	            	}
		        }
			} catch (RepositoryException re) {
				logger.error("Exception to getNode size: "+re.getMessage());
			}
			
			Node parent	=	targetNode.getParent();
			
			targetNode.remove();
			targetNode=null;
        	session.save();
        	
        	//increment the NEGATIVE value (in case of removal/decrement)
        	this.statusObj.incUniqueFilesCounter("FS", -1, -nodeSize);
        	
        	if(!parent.hasNodes()) {//if parent folder is empty
				removeNode(session, parent, logger);	
			}
    	}
	}

    public  String uploadDocumentByBuiltInAgent(String[] pathElements, FileTaskInterface task, HashMap layerAttribs, HashMap aclAttribs, String transID) throws Exception {
        
		//synchronized (this.getClass()) {
		if(transID==null || transID.isEmpty())
			transID	=	"remote";
		
        log.info(transID + " uploadDocument: started - builtin Agent");
        
        String url = null;
        String fileName = null;
        
       
      
        Node root = null;
        String secureInfo = "";
        Node parent	=	null;
       
        try {
        	//javax.jcr.Repository repo=session.getRepository();
        	RepositoryUtil	ru	=	getRepository();	//initializes SessionManager and Repostiory instance
        	
            secureInfo = (layerAttribs != null) ? (String) layerAttribs.get(VCSConstants.SECURE_INFO) : "";
            log.debug(transID + " Secure Info extracted in RepositryOpManager: " + secureInfo);
            SimpleCredentials creds = SecurityUtil.decodeSecureInfo(secureInfo);

            if (repository == null || creds == null) {
                throw new LoginException("Repository object isn't initialized");
            }
            layerAttribs.remove(VCSConstants.SECURE_INFO);
            log.debug(transID + " Secure Info removed from Hashtable");

            //Session session = repo.login(creds);
            Session session = ru.getSession(repository,  "default",creds);//new SimpleCredentials("username"+Math.random(), "password".toCharArray()));
            
            root = session.getRootNode();
            String path = root.getPath();
            parent = this.getCompanyNode(session, root);//Get the companyName

            //Retrieve current statistics of Repository, from CustomNode
            ValueFactory valueFactory = session.getValueFactory();
            //HashMap repoStats =   this.statusObj.getValuesOfStatusNode(root);// at this stage, parent node is containing /VC/

            //Add the path tree, into Repository            
	            parent = this.addFoldersToMakeTree(parent, pathElements, layerAttribs, session);//, repoStats);
	
	            //Start Proces to ADD FILE TO REPOSITORY
	            fileName = pathElements[pathElements.length - 1];
	            log.debug(transID + " File Name: " + fileName);
	            log.debug(transID + " Parent Node: " + parent.getName() + " of type: " + parent.getDefinition().getDeclaringNodeType().getName());
	
	            ResourcesUtil resUtil	=	ResourcesUtil.getResourcesUtil();
	            String tempSize	=	resUtil.getCodeValue("BUFF_SIZE", SystemCodeType.GENERAL);
	            int buffSize	=	8192;//defaultBufferSize
	            if(tempSize!=null && !tempSize.isEmpty()) {
	            	try {
	            		buffSize	=	Integer.parseInt(tempSize);
	            	}catch(Exception ex) {
	            		log.error(transID + " Error to parse BUFF_SIZE: "+tempSize);
	            	}
	            }
	            log.debug(transID + " buffered size is: "+buffSize);
	            BufferedInputStream istream	=	new BufferedInputStream(task.getCIFSFile().getFileObj().getInputStream(), buffSize);
	            while(true) {	//do untill the lock is acquired and node added..
	            	
	            	//Verify if the current execution is not stopped by Admin...
	            	if(JobStarter.isInterupted(task.getExecID())) {
	            		return null;//Terminate the current Thread, if CANCEL is requested by User
	            	}
	            	
		            if(acquireLock(parent.getIdentifier())) {//if lock acquired successfully
		                session.refresh(true);
			            //Insert the File     OR     Create Link(if already exist) into Repository
			            url = this.addFileToParentNode(parent, fileName, istream, valueFactory, layerAttribs, aclAttribs, session, transID);//, repoStats);
		               
			            if(!saveTheSession(session,url,transID)) {
			            	log.warn(transID + " SESSION NOT SAVED for.."+fileName);
			            	url	=	null;
			            	/*
			            	if(session!=null)
			            		session.logout();
			            	
			            	//release the lock, after session save
				            releaseLock(parent.getIdentifier());
				            
			            	return null;*/
			            }
			            
			            //release the lock, after session save
			            releaseLock(parent.getIdentifier());
			            break;
			            
		            } else {
		            	log.info(transID + " waiting to acquireLock for "+parent.getIdentifier());
		            	Thread.sleep(2000);	            	
		            }
	            }//end of while loop
	            
	            if(istream!=null){//release the fileStream instance, to avoid the SmbExceptions
		            istream.close();
		            istream=	null;
	            }
	            
	            if(session!=null)
            		session.logout();
	            /*
	            try {
	                 session.save();
	                 log.debug("Session Saved....");	
	                   
	            } catch (Exception ex) {
	            	log.debug("************Session Save Failure ******************************....");
	                this.logError(ex, transID);
	                return null;
	            }*/
            
            //}
            log.debug(transID + " After adding file to parent url ---->"+url);
            //String wspName = session.getWorkspace().getName();

            if (url != null && !url.isEmpty()) {
            	
            	//if(url.startsWith("AE|")) {//in case of BuiltIn Agent, Mark that file of current task already exist (to show in infolog)
            		task.setSecureRepoPath(url.substring(0, 2));//if its starting with AE / LN etc...
            	//}
            	
                url = this.prepareIDOfNode(url, secureInfo, transID);
                log.debug(transID + " After preparing url ---->"+url);
            }/* else{
            	return null;
            }*/
        } catch (LoginException ex) {
            this.logError(ex, transID);
            url	=	null;
            
        } catch (RepositoryException ex) {
            this.logError(ex, transID);
            url	=	null;
            
        } finally {
        	if(parent!=null) {
        		releaseLock(parent.getIdentifier());
        	}
        }
    	
        return url;
		//}
    }

	public  String uploadDocument(String[] pathElements, InputStream in, HashMap layerAttribs, HashMap aclAttribs) throws Exception {
        
		//synchronized (this.getClass()) {
		
        log.info("uploadDocument: started - synchronized");
        
        String url = null;
        String fileName = null;
        String transID	=	"remote";
       
      
        Node root = null;
        String secureInfo = "";
        Node parent	=	null;
       
        try {
        	//javax.jcr.Repository repo=session.getRepository();
        	RepositoryUtil	ru	=	getRepository();	//initializes SessionManager and Repostiory instance
        	
            secureInfo = (layerAttribs != null) ? (String) layerAttribs.get(VCSConstants.SECURE_INFO) : "";
            log.debug("Secure Info extracted in RepositryOpManager: " + secureInfo);
            SimpleCredentials creds = SecurityUtil.decodeSecureInfo(secureInfo);

            if (repository == null || creds == null) {
                throw new LoginException("Repository object isn't initialized");
            }
            layerAttribs.remove(VCSConstants.SECURE_INFO);
            log.debug("Secure Info removed from Hashtable");

            //Session session = repo.login(creds);
            Session session = ru.getSession(repository,  "default",creds);//new SimpleCredentials("username"+Math.random(), "password".toCharArray()));
            
            root = session.getRootNode();
            String path = root.getPath();
            parent = this.getCompanyNode(session, root);//Get the companyName

            //Retrieve current statistics of Repository, from CustomNode
            ValueFactory valueFactory = session.getValueFactory();
            //HashMap repoStats =   this.statusObj.getValuesOfStatusNode(root);// at this stage, parent node is containing /VC/

            //Add the path tree, into Repository            
	            parent = this.addFoldersToMakeTree(parent, pathElements, layerAttribs, session);//, repoStats);
	
	            //Start Proces to ADD FILE TO REPOSITORY
	            fileName = pathElements[pathElements.length - 1];
	            log.debug("File Name: " + fileName);
	            log.debug("Parent Node: " + parent.getName() + " of type: " + parent.getDefinition().getDeclaringNodeType().getName());
	
	            while(true) {	//do untill the lock is acquired and node added..
		            if(acquireLock(parent.getIdentifier())) {//if lock acquired successfully
		                session.refresh(true);
			            //Insert the File     OR     Create Link(if already exist) into Repository
			            url = this.addFileToParentNode(parent, fileName, in, valueFactory, layerAttribs, aclAttribs, session, transID);//, repoStats);
		               
			            if(!saveTheSession(session,url,transID)) {
			            	log.warn("SESSION NOT SAVED for.."+fileName);
			            	url	=	null;
			            	/*
			            	if(session!=null)
			            		session.logout();
			            	
			            	//release the lock, after session save
				            releaseLock(parent.getIdentifier());
				            
			            	return null;*/
			            }
			            
			            //release the lock, after session save
			            releaseLock(parent.getIdentifier());
			            break;
			            
		            } else {
		            	log.info("waiting to acquireLock for "+parent.getIdentifier());
		            	Thread.sleep(2000);	            	
		            }
	            }//end of while loop
	            
	            if(session!=null)
            		session.logout();
	            /*
	            try {
	                 session.save();
	                 log.debug("Session Saved....");	
	                   
	            } catch (Exception ex) {
	            	log.debug("************Session Save Failure ******************************....");
	                this.logError(ex, transID);
	                return null;
	            }*/
            
            //}
            log.debug("After adding file to parent url ---->"+url);
            //String wspName = session.getWorkspace().getName();

            if (url != null && !url.isEmpty()) {             
                url = this.prepareIDOfNode(url, secureInfo, transID);
                log.debug("After preparing url ---->"+url);
            }else{
            	return null;
            }
        } catch (LoginException ex) {
            this.logError(ex, transID);
            url	=	null;
            
        } catch (RepositoryException ex) {
            this.logError(ex, transID);
            url	=	null;
            
        } finally {
        	if(parent!=null) {
        		releaseLock(parent.getIdentifier());
        	}
        }
    	
        return url;
		//}
    }
	
	private static HashMap<String, Boolean> locksList	=	new HashMap<String, Boolean>();
	
	private boolean acquireLock(String id) throws RepositoryException {
		
		synchronized(this.getClass()) {
			
			boolean result	=	false;
			if(locksList.containsKey(id)) {
				result	=	false;
			
			} else {
				result	=	true;
				locksList.put(id, result);
			}
			
			return result;
		}
	}
	
	private void releaseLock(String id) {
		//synchronized(this.getClass()) {
			locksList.remove(id);
		//}
	}
	
	 private String prepareIDOfNode(String url, String secureInfo, String transID) {

	        String output = null;
	        log.debug("******************  DUMP NODE Start   ******************");
	        //dump(root);

	        if (url != null) {

	            log.debug("url(original): " + url);
	            String[] temp = url.split("\\|");
	            log.debug("Splitted: [0]" + temp[0] + ", [1]" + temp[1]);

	            //url = temp[0] + "|" + "/repository/" + wspName + temp[1];
	            url = temp[1];//Skip the CODE Appending functionality.. untill the Agent is sync with it

	            log.debug("url(actual): " + url);
	            url = encodeURL(url);
	            log.debug("url(encoded): " + url);

	            url = url + "?secureInfo=" + secureInfo;
	            log.debug("*** Document URL after adding SecureInfo: " + url);


	            //encrypt URL
	            try {
	                log.debug("secureURL.getEncryptedURL(url) ---> "+secureURL.getEncryptedURL(url));
	                url = VCSConstants.SECURE_GET_SERVLET_PATH + secureURL.getEncryptedURL(url);
	            	
	                log.debug(transID + " secureURL.final(url) ---> "+url);
	                output = url;
	            } catch (Exception e) {//if some error occured to Encrypt URL
	                this.logError(e,transID);
	                output = VCSConstants.DD_ERROR;
	            }
	        }
	        log.debug("#####  Output  ##### :" + output);

	        return output;
	    }
	    

	
	private Node getCompanyNode(Session session, Node root) {
        log.debug("getCompanyName: Started");
        Node parent = null;
        
         SystemCode systemCode = systemCodeService.getSystemCodeByName("companyName");
		 String companyName="TestCompany";
		 if(systemCode!=null){
			  companyName = systemCode.getCodevalue();
		 }
        
        try {
        	
        	synchronized (this.getClass()) {
        	log.debug("getCompanyName: 1");
            if (root.hasNode(companyName) || session.nodeExists("/" + companyName)) {
                parent = root.getNode(companyName);
                //log.debug("getCompanyName: 2");
                //log.debug("Parent Node:"+parent.getName());
                //log.debug("Parent Node Path:"+parent.getPath());
                //log.debug("**********11111111111***********");
               
            } else {
                parent = root.addNode(companyName, "nt:folder");
                
                log.debug(companyName + "not exist: creating it first time with name: " + parent.getName());
                
                if(!saveTheSession(session,parent.getIdentifier(),null))
                	parent	=	root;
            }
        	}
        } catch (RepositoryException ex) {
            log.error(companyName + " not accessible");
            this.logError(ex, null);
            parent = root;
        }
        //log.debug("********222222222222222*********");
        return parent;
    }
	
	
	
	private Node addFoldersToMakeTree(Node parent, String[] pathElements, HashMap layerAttribs, Session session) throws Exception {
		
		synchronized(this.getClass()) {
			
        String agentType = null;
        log.debug("Path Elements --->"+pathElements.toString());
        agentType = (layerAttribs != null) ? (String) layerAttribs.get(VCSConstants.AGENT_TYPE) : "FS";

        if (agentType == null || !(agentType.equals("FS") || agentType.equals("SP"))) {
            agentType = "SP";
            layerAttribs.put(VCSConstants.AGENT_TYPE, agentType);
        }

        if (!parent.hasNode(agentType)) {
            parent = parent.addNode(agentType, "nt:folder");
            //repoStats   =   this.statusObj.incTotalFolders(repoStats, 1);
           	this.statusObj.incTotalFolders(1);
            

        } else {
            parent = parent.getNode(agentType);
        }

        //Checking  and Adding Agent Name node under FS Node.
        String agentName = null;
        agentName = (layerAttribs != null) ? (String) layerAttribs.get(VCSConstants.AGENT_NAME) : null;

        if (agentType != null && (agentType.equals("FS") && agentName != null)) {
            if (!parent.hasNode(agentName)) {
            	    parent = parent.addNode(agentName, "nt:folder");
            	            		
	                //repoStats   =   this.statusObj.incTotalFolders(repoStats, 1);
	                this.statusObj.incTotalFolders(1);
            	

            } else {
                parent = parent.getNode(agentName);
            }
        }
        log.debug("Parent Path: " + parent.getPath());

        String element = null;
        Node node = null;
        // ADD FOLDERS TO REPOSITORY BASED ON SOURCE PATH
        int i = 0;
        for (; i < pathElements.length - 1; i++) {
            element = pathElements[i];
            if (element.length() == 0) {
                continue;
            }
            //element = URLDecoder.decode(element, "utf-8");

            log.info("Adding Tree Folder: " + element);
            //log.info("******After Escaped Illegal chars: "+Text.escapeIllegalJcrChars(element));


            if (!parent.hasNode(element)) {
                //if child not exist, then check for Case-Insensitivily first
                boolean exists = false;
                NodeIterator ni = parent.getNodes();
                while (ni.hasNext()) {//compare with all children
                    String tempName = ni.nextNode().getName();
                    if (element.equalsIgnoreCase(tempName)) {
                        element = tempName;
                        exists = true;
                        break;//terminate the iterations if matched with any
                    }
                }

                if (!exists) {
                	node = parent.addNode(element, "nt:folder");
                	 //repoStats   =   this.statusObj.incTotalFolders(repoStats, 1);
                     this.statusObj.incTotalFolders(1);
                    

                } else {
                    node = parent.getNode(element);
                }

                log.debug("Added Node: " + node.getName() + ", Modified Case:" + element);
                parent = node;
            } else {
                //log.debug("Node exist");
                node = parent.getNode(element);
                parent = node;
            }
            log.debug("Element after case matching: " + element);
        }
        
        
            if(!saveTheSession(session,null,null))
            	log.warn("SERIOUS ERROR OCCURED, to Cause IllegalStateException");
       

        return parent;
		}
    }
    
	
	private String addFileToParentNode(Node parent, String fileName, InputStream istream, ValueFactory valueFactory, HashMap layerAttribs, HashMap aclAttribs, Session session, String transID) throws RepositoryException {
        log.info("addFileToParentNode: Started");
        NodeDefinition nd = parent.getDefinition();
        NodeType parentNodeType = nd.getDeclaringNodeType();
        String nodeTypeName = parentNodeType.getName();
        String filePath = null;
        log.debug(transID + " Node Type Name: " + nodeTypeName);
        String agentType = (String) layerAttribs.get(VCSConstants.AGENT_TYPE);
        Node file = null;
        Node resource = null;
        java.util.Set<String> set = layerAttribs.keySet();

        boolean isLinkedNode=false;
      //  String createDate = (String) layerAttribs.get("CreatedOn");
       // System.out.println("date is : " + createDate);

        Node baseFile   =   null;
        boolean isSameNameCase  =   false;
        if (parent.hasNode(fileName)) {//Case to create name format of Versions
            log.info(transID + " Item " + fileName + " already exists in " + parent.getPath());
            Node tempFile	=	null;
            try {
            	tempFile	=	parent.getNode(fileName);
                //Get the ref of Node, whose Version is to be created
            	baseFile    =   tempFile.getNode(Property.JCR_CONTENT);//this can throw PathNotFoundException
	            isSameNameCase  =   true;
	                
                ResourcesUtil ru  		=   ResourcesUtil.getResourcesUtil();
                String isLazyArchiving	=   ru.getCodeValue("LazyArchiving", SystemCodeType.GENERAL);
                
                //modified to reset the IS_REMOVED attribute
                System.out.println("node already exists " + fileName);
                if(baseFile.hasProperty(CustomProperty.VC_IS_REMOVED)){
                	baseFile.setProperty(CustomProperty.VC_IS_REMOVED, "no");
                	System.out.println("is removed set to false");
                }
                
                if(isSameNameCase && baseFile!=null && !"false".equals(isLazyArchiving)) {//if its the case of Same name, then check for SameMetaData
                    //log.info("case of LAZY arhiving");
                    
                    String rpModifDate	=	"";
                    if (baseFile.hasProperty(Property.JCR_LAST_MODIFIED)) {
                        rpModifDate	=	baseFile.getProperty(Property.JCR_LAST_MODIFIED).getString().substring(0,19).replaceAll("[a-zA-Z]", " ");
                    }
                    String rpCreatDate	=	"";
                    if (baseFile.hasProperty(CustomProperty.VC_CREATION_DATE)) {
                        rpCreatDate	=	baseFile.getProperty(CustomProperty.VC_CREATION_DATE).getString().substring(0,19).replaceAll("[a-zA-Z]", " ");
                    }
                    
                    String strModifiedOn = ((String) layerAttribs.get("ModifiedOn")).substring(0,19).replaceAll("[a-zA-Z]", " ");//2013-09-17T21:08:06.186+0500
                    String strCreationDate = ((String) layerAttribs.get("CreatedOn")).substring(0,19).replaceAll("[a-zA-Z]", " ");//2013-09-17T21:08:06.186+0500

                    log.info(transID + " comparing Modif Dte "+rpModifDate+" | "+strModifiedOn);
            		log.info(transID + " comparing Creat Dte "+rpCreatDate+" | "+strCreationDate);
            		
            		if(rpModifDate.equals(strModifiedOn) && rpCreatDate.equals(strCreationDate)) {//if file with MetaData, already exists at desired location
            			return "AE|"+baseFile.getParent().getIdentifier();//terminate further Archiving flow...
            		}
                    
                }              
            
                //processSameNameFile, if LazyArchiving is FALSE or MetaData is not matched
                fileName    =   processSameNameFile(parent, fileName, 1, transID);//this will throw an exception
                
            } catch (PathNotFoundException pnf) {
                // if file exists, but the content doesn't exist, the better way to handle is
                // to remove this file earlier at the time of addtion, when the exception occured in adding resource
                log.warn(transID + " Resource not found: " + pnf.getMessage());
                if (tempFile != null) {
                	log.warn(transID + " Removing old/exsiting node... "+tempFile.getIdentifier());//to continue further by insertring new Entry
                	if(tempFile.hasNodes()) {
                		log.debug(transID + " childNodes exist for " + tempFile.getName());
                		NodeIterator ni	=	tempFile.getNodes();
                		if(ni.hasNext()) {
                			log.warn(transID + " removing child node");
                			ni.remove();
                		}
                	}
                	tempFile.removeShare();
                	tempFile	=	null;            	
                }
            }
            
        }
        //create file node
       
        file = parent.addNode(fileName, NodeType.NT_FILE);
        
        log.debug(transID + " node file added: "+fileName);
        
        Binary binaryValue = valueFactory.createBinary(istream);
        byte[] hash = getMD5Hash(binaryValue.getStream());
        log.debug(transID + " Hash: " + new HexBinaryAdapter().marshal(hash));

        if (hash == null) {
            throw new RepositoryException("MD5 has couldn't be calculated for the file.");
        }

        NodeId nodeId = new NodeId(hash);
        log.debug(transID + " Node ID:  " + nodeId.toString());

        NodeImpl nodeImpl = (NodeImpl) file;

        String docURL = null;
        if (layerAttribs != null) {
            docURL = (String) layerAttribs.get("SPURL");
        }
        
        String contentType = null;
        if (layerAttribs != null) {
            contentType = (String) layerAttribs.get("contentType");//only in metadata of sharepoint
        }
        System.out.println("******"+fileName+"------>"+contentType);
        
        try {
        	
	            resource = (agentType.equals("FS"))
	                    ? nodeImpl.addNodeWithUuid(Property.JCR_CONTENT, CustomNodeType.VC_FS_RESOURCE, nodeId.toString())
	                    : nodeImpl.addNodeWithUuid(Property.JCR_CONTENT, CustomNodeType.VC_SP_RESOURCE, nodeId.toString());
        				
	            resource.addMixin(NodeType.MIX_SHAREABLE);
        	
            log.debug(transID + " Adding resource...");

        } catch (Exception ex) {//If Duplication occurs, then insert the linkedNode only and Terminate...
            //this.logError(ex, transID);
            log.debug(transID + " Duplicate content for " + fileName);
            if (file != null) {
            	  file.remove();
            	  file    =   null;
            }
                        
            if(isSameNameCase) {//if the duplicate have same name at same location
            	
            	Node existingNode = session.getNodeByIdentifier(nodeId.toString());//original node can be at different location
            	if(baseFile!=null && !baseFile.getPath().equalsIgnoreCase(existingNode.getPath())) {
            		//if original file is at different location and a link exist with same name here
            		
            		existingNode	=	session.getNode(baseFile.getPath());
            		log.info(transID + " already linked node exist for same content at: "+existingNode.getPath());
            		
            	}else{
            		log.info(transID + " already archived with same content at: "+existingNode.getPath());
            	}
                
            	//Node existingNode = session.getNodeByIdentifier(nodeId.toString());//this will create a bug in a scenario of re-Archiving linkedNode
            	//Node existingNode	=	parent.getNode(fileName).getNode(Property.JCR_CONTENT);                
                
                //So not create new instance of linked / Version etc
                filePath	=	existingNode.getParent().getIdentifier();//file.getPath();
                filePath    =   "AE|" + filePath;
                
            } else {//if name / location is not same for Duplicate Content..
                try {
                    Node existingNode = session.getNodeByIdentifier(nodeId.toString());
                    log.info(transID + " Adding linked node");
                    //Create the New linkedNode
                    //creation date for the linked node

                    filePath = this.createLinkedNode(existingNode, parent, fileName, docURL, valueFactory, aclAttribs, layerAttribs, agentType,contentType,transID);
                   
                    //filePath = "LN|" + file.getPath();
                   
                    this.statusObj.incDuplicateDataSize(binaryValue.getSize());
                    this.statusObj.incDupFilesCounter(agentType, 1);
                    isLinkedNode=true;
                   
                } catch (ItemNotFoundException infe) {
                    log.error(transID + " Item not found while checking existing node. Ex: " + infe.getMessage());
                    this.logError(infe, transID);
                    //infe.printStackTrace();
                    filePath = null;
                }
                //return filePath;//LN| is already appended in the method of createLinkedNode...
            }
            
            //commit changes for other sessions as well
            if(!saveTheSession(session,filePath, transID)){
            		this.statusObj.incDuplicateDataSize(-(binaryValue.getSize()));
                    this.statusObj.incDupFilesCounter(agentType, -1);
            	
            	return null;
            }
            else
            	return filePath;
        }
        
        
        ///////Perform the Below line operation, to complete the Succesfull insertion of New Node//////

        //Update the Repository Counter, as its newFile insertion
        //repoStats   =   this.statusObj.incFilesCounter(repoStats, agentType, 1, binaryValue.getSize());
       
            this.statusObj.incUniqueFilesCounter(agentType, 1, binaryValue.getSize());
            
            //Insert the File Data in this
	        resource.setProperty(Property.JCR_DATA, binaryValue);    
	        
	        //set file size to custom property
	        resource.setProperty(CustomProperty.VC_FILE_SIZE, binaryValue.getSize());
	        
        /* dont set the property of LinkedTo in case of Original nt:file 
        if(resource.hasProperty(CustomProperty.VC_LINKED_TO)) {//Mark it as Not-Linked
            String emptyValue   =   null;
            resource.setProperty(CustomProperty.VC_LINKED_TO, emptyValue);
            log.info("Marked as File Node: " + resource.getPath());

        } else {
            log.warn("File node is not marked as File Node: " + resource.getPath());
        }
        */
        
        if(isSameNameCase && baseFile!=null) {//if its the case of Same Name - Version document
            log.info(transID + " Setting Base File UID in latest version");
           			
            resource.setProperty(CustomProperty.VC_VERSION_OF, baseFile.getIdentifier());
                       
            log.info(transID + " Appending latest verionz UID in the very first document");
            String itsNextVersions  =   "";
            if(baseFile.hasProperty(CustomProperty.VC_NEXT_VERSIONS)) {
                Property prop   =   baseFile.getProperty(CustomProperty.VC_NEXT_VERSIONS);
                itsNextVersions  =   prop.getString() + ",";
            }
            itsNextVersions =   itsNextVersions + resource.getIdentifier();
            log.info(transID + " Property becomes: "+itsNextVersions);
           
			baseFile.setProperty(CustomProperty.VC_NEXT_VERSIONS, itsNextVersions);
           
        }
        
        log.debug(transID + " Layer Attributes: " + layerAttribs);

        if (layerAttribs != null) {
        	 setCustomPropertiesOfNode(resource, fileName, aclAttribs, layerAttribs, docURL, agentType,contentType, transID);
         }
        
        ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
        if(ru.getCodeValue(SystemCodeType.GENERAL, "enable_active_archiving_notification")!=null){
        	boolean enableNotification = ru.getCodeValue(SystemCodeType.GENERAL, "enable_active_archiving_notification").equals("yes")?true:false;
        	
            String strArchivingMode = (String) layerAttribs.get("archivingMode");
            if (strArchivingMode==null)
            	strArchivingMode = VCSConstants.ARCHIVE_MODE_NORMAL;
            if(strArchivingMode.equalsIgnoreCase(VCSConstants.ARCHIVE_MODE_NORMAL))
            {
            	sendNotificationEmail(file.getIdentifier(), file.getPath(), (String)layerAttribs.get("lastModifiedByUser"));
            }            
        }
        
        //commit changes for other sessions as well
        if(!saveTheSession(session,file.getIdentifier(), transID)){
        	this.statusObj.incUniqueFilesCounter(agentType,-1,-(binaryValue.getSize()));
        	return null;
        }
        else
        	return "NN|" + file.getIdentifier();//.getPath
    }
	
	private boolean sendNotificationEmail(String docId, String docUrl, String username){
		if(username==null || username.length()<1)
			return false;
//		mailser
		MailService mailService = (MailService) SpringApplicationContext.getBean("mailService");
		if(mailService==null)
			return false;
//		if(username !=null && username.length()>0){			
//			username = username.substring(0,1).toUpperCase() + username.substring(1);		
//		}       
//		String messageBody="Dear, <br/>";		
//		if(emailNameLink!=null && ! emailNameLink.isEmpty()){messagebody="Dear "+emailNameLink+",<br/><br/>";}
		
		String messageBody = "Dear " + username +", <br/>";
		
		messageBody+= "Following document has been archived actively: <br/><br/>";
		
		messageBody+="Document ID: "  + docId +"<br/>";
		messageBody+="Document URL: " + docUrl + "<br/><br/>";
		messageBody+="Thank You";
		
		JespaUtil util = JespaUtil.getInstance();
		String email = util.getUserEmail(username);
		
		boolean success = false;

		success=mailService.sendMail(email, "Document Archived", messageBody,null, null);
		
		return success;		
	}
	
	private boolean saveTheSession(Session session,String fileId, String transID) {
		boolean result	=	true;
		try {
            if (session != null && session.hasPendingChanges()) {
               	session.save();            	
                log.debug(transID + " Repo Session saved Successfully!");

            } else {
                log.debug(transID + " Handled Error: Repo Session not exist or can't be saved:"+fileId);
            }
        } catch (Exception ex) {
            this.logError(ex, transID);
            result	=	false;
            log.warn(transID + " Exception in saving session for "+fileId);
            
            if(fileId!=null && !fileId.startsWith("AE")) {//No need to rollback transaction, if its the case of AExist...
                log.debug(transID + " Going to remove Temporary File ..."+fileId);
            	String temp=fileId;
            	if(fileId.indexOf("|")>=0)
            	   temp=	fileId.substring(fileId.indexOf("|")+1);
            	if(temp!=null && !temp.isEmpty()) {
            		
            		try{
            			log.debug(transID + " Temporary File Id after substring... "+temp);
            			session.refresh(false);
            			
            			log.debug(transID + " session refreshed");
            			Node tempNode=globalSession.getNodeByIdentifier(temp);
            			log.debug(transID + " "+tempNode.getPath());
            			
            			if(tempNode!=null){
            				
            				if("nt:file".equalsIgnoreCase(tempNode.getParent().getPrimaryNodeType().getName())) {
            					//if some problem occured to save the session of Properties/JCR_Content node
            					//than also delete the nt:file node as well...
            					
            					Node parent	=	tempNode.getParent();
            					log.debug(transID + " removing nt:file of id "+parent.getIdentifier());
            					
            					tempNode.remove();
            					tempNode	=	null;
            					parent.remove();
            					parent		=	null;
            					
            				} else {//if tempNode is a nt:folder or nt:file itself...
            					tempNode.remove();
                				tempNode=null;
            				}
            				
            				globalSession.save();
            				log.debug(transID + " Temporary File(s) removed...");            				
            			}
            		    
            		}catch(Exception ex22){
            			log.debug(transID + " Error in removing Temporary File..."+ex22.getMessage());
            			ex22.printStackTrace();
            		}
            	} else {
            		log.debug(transID + " Not valid ID" + temp);
            	}
            }
            
        }
		return result;
	}

	
	 private void setCustomPropertiesOfNode(Node resource, String fileName, HashMap aclAttribs, HashMap layerAttribs, String docURL, String agentType,String contentType, String transID) throws RepositoryException {
	        
	        //if(resource.hasProperty(Property.JCR_MIMETYPE)) {
	        //String fileName = (String) layerAttribs.get("Title");
	            String mimeType = URLConnection.guessContentTypeFromName(fileName);
	            if (mimeType == null) {
	                if (fileName.endsWith(".doc")) {
	                    mimeType = "application/msword";
	                } else if (fileName.endsWith(".xls")) {
	                    mimeType = "application/vnd.ms-excel";
	                } else if (fileName.endsWith(".ppt")) {
	                    mimeType = "application/mspowerpoint";
	                } else {
	                    mimeType = "application/octet-stream";
	                }
	            }
	            
	            resource.setProperty(Property.JCR_MIMETYPE, mimeType);
	        //}
	        
	        Calendar c = Calendar.getInstance();
	        SimpleDateFormat srcDateFormat = new SimpleDateFormat(VCSConstants.SRC_DATE_FORMAT);

	            String modifiedBy = (String) layerAttribs.get("ModifiedBy");
	            if (modifiedBy == null) {
	                modifiedBy = "";
	            }
	            resource.setProperty(Property.JCR_LAST_MODIFIED_BY, modifiedBy);

	            String strModifiedOn = (String) layerAttribs.get("ModifiedOn");
	            Date modifiedOn = null;
	            if (strModifiedOn != null) {
	                try {
	                    modifiedOn = srcDateFormat.parse(strModifiedOn);
	                    c.setTime(modifiedOn);
	                    resource.setProperty(Property.JCR_LAST_MODIFIED, c);
	                } catch (ParseException ex) {
	                    this.logError(ex, transID);
	                }
	            }

	            String title = (String) layerAttribs.get("Title");
	            if (title == null) {
	                title = fileName;
	            }
	            resource.setProperty(Property.JCR_TITLE, title);
	            log.debug(transID + " new title: " + title);
	            
//	            String docURL = (String) layerAttribs.get("SPURL");
	            String[] multiURL = new String[1];
	            multiURL[0] = docURL;
	            resource.setProperty(CustomProperty.VC_DOC_URL, multiURL);

	            if (agentType.equals("SP")) {
	                //Populate the SP related Properties here
	                resource.setProperty(CustomProperty.VC_SP_URL, docURL);
	                resource.setProperty(CustomProperty.VC_SP_DOC_CONTENT_TYPE, contentType);
	                String docVersion = (String) layerAttribs.get("DocVersion");
	                if (docVersion == null) {
	                    docVersion = "1.0";
	                }

	                resource.setProperty(CustomProperty.VC_DOC_VERSION, docVersion);
	                //temp add
	                setAllSIDs(resource, aclAttribs, transID);

	            } else if (agentType.equals("FS")) {
	                //Populate the FS related Properties here
	                String strAccessedOn = (String) layerAttribs.get("AccessedOn");
	                if (strAccessedOn != null) {
	                    try {
	                        Date accessedOn = srcDateFormat.parse(strAccessedOn);
	                        c.setTime(accessedOn);
	                        resource.setProperty(CustomProperty.VC_ACCESSED_ON, c);
	                    } catch (ParseException ex) {
	                        this.logError(ex, transID);
	                    }
	                }
	                //String strCreationDate = (String) layerAttribs.get("CreatedOn");
	                String strCreationDate = (String) layerAttribs.get("CreatedOn");
	                if (strCreationDate != null) {
	                    try {
	                        Date creationDate = srcDateFormat.parse(strCreationDate);
	                        c.setTime(creationDate);
	                        resource.setProperty(CustomProperty.VC_CREATION_DATE, c);
	                    } catch (ParseException ex) {
	                        this.logError(ex, transID);
	                    }
	                }
	                
	                String strArchivingMode = (String) layerAttribs.get("archivingMode");
	                if (strArchivingMode==null)
	                	strArchivingMode = VCSConstants.ARCHIVE_MODE_NORMAL;
	                
	                resource.setProperty(CustomProperty.VC_ARCHIVING_MODE, strArchivingMode);
	                
	                String strTags = (String) layerAttribs.get("tags");
	                if (strTags!=null && strTags.length()>0){
	                	SimpleDateFormat sdf=new SimpleDateFormat("d MMM yyyy HH:mm:ss");
	               		String timestamp =sdf.format(new Date());
//	               		admin|2014-02-25|tag
//	                	strTags = "";
	                	resource.setProperty(CustomProperty.VC_DOC_TAGS, "ShareArchiver|"+timestamp +"|"+strTags);
	                }
	                //set archive date as custom property 
                        c.setTime(new Date());
                        resource.setProperty(CustomProperty.VC_ARCHIVE_DATE, c);
                        
	                String strPrinterName = (String) layerAttribs.get("printerName");
	                if (strPrinterName!=null && strPrinterName.length()>0){
	                	resource.setProperty(CustomProperty.VC_PRINTER_NAME, strPrinterName);
	                }

	                String strMachineName = (String) layerAttribs.get("machineName");
	                if (strMachineName!=null && strMachineName.length()>0){
	                	resource.setProperty(CustomProperty.VC_MACHINE_NAME, strMachineName);
	                }
	                
	                String strUserName = (String) layerAttribs.get("username");
	                if (strUserName!=null && strUserName.length()>0){
	                	resource.setProperty(CustomProperty.VC_USER_NAME, strUserName);
	                }
	                
	                String strTotalPages = (String) layerAttribs.get("totalPages");
	                if(strTotalPages!=null && strTotalPages.length()>0){
	                	resource.setProperty(CustomProperty.VC_TOTAL_PAGES, strTotalPages);
	                }

	                setAllSIDs(resource, aclAttribs, transID);
	                /*
	                String allowedSids = (String) aclAttribs.get(CustomProperty.VC_ALLOWED_SIDS);
	                if (allowedSids != null) {
	                    resource.setProperty(CustomProperty.VC_ALLOWED_SIDS, allowedSids);
	                }

	                String deniedSids = (String) aclAttribs.get(CustomProperty.VC_DENIED_SIDS);
	                if (deniedSids != null) {
	                    resource.setProperty(CustomProperty.VC_DENIED_SIDS, deniedSids);
	                }

	                String shareAllowedSids = (String) aclAttribs.get(CustomProperty.VC_S_ALLOWED_SIDS);
	                if (shareAllowedSids != null) {
	                    resource.setProperty(CustomProperty.VC_S_ALLOWED_SIDS, shareAllowedSids);
	                }

	                String shareDeniedSids = (String) aclAttribs.get(CustomProperty.VC_S_DENIED_SIDS);
	                if (shareDeniedSids != null) {
	                    resource.setProperty(CustomProperty.VC_S_DENIED_SIDS, shareDeniedSids);
	                }*/
	            }
	    }
	    
	    
	    private String processSameNameFile(Node parent, String fileName, int ctr, String transID) throws PathNotFoundException, RepositoryException {
	        
	        String newName =   fileName.substring(0, fileName.lastIndexOf("."));
	        String fileExt  =   fileName.substring(fileName.lastIndexOf(".")+1);
	        String versionFormater  =   " - v"+ctr;
	        
	        newName =   newName + versionFormater +"."+ fileExt;
	        
	        if(parent.hasNode(newName)) {
	            fileName    =   processSameNameFile(parent, fileName, ++ctr, transID);
	            log.info(transID + " Proposed name "+newName+" already exist");
	            
	        } else {
	            fileName    =   newName;
	            log.info(transID + " final proposed name "+fileName);
	        }
	        
	        return fileName;
	    }

	    
	    
	    private void setAllSIDs(Node resource, HashMap aclAttribs, String transID) {

	        try {
	            String allowedSids = (String) aclAttribs.get(CustomProperty.VC_ALLOWED_SIDS);
	            if (allowedSids != null) {
	                log.info(transID + " Allowed SIDs : "+allowedSids);
	                resource.setProperty(CustomProperty.VC_ALLOWED_SIDS, allowedSids);
	            }

	            String deniedSids = (String) aclAttribs.get(CustomProperty.VC_DENIED_SIDS);
	            if (deniedSids != null) {
	                log.info(transID + " Denied SIDs : "+deniedSids);
	                resource.setProperty(CustomProperty.VC_DENIED_SIDS, deniedSids);
	            }

	            String shareAllowedSids = (String) aclAttribs.get(CustomProperty.VC_S_ALLOWED_SIDS);
	            if (shareAllowedSids != null) {
	                log.info(transID + " Share Allowed SIDs : "+shareAllowedSids);
	                resource.setProperty(CustomProperty.VC_S_ALLOWED_SIDS, shareAllowedSids);
	            }

	            String shareDeniedSids = (String) aclAttribs.get(CustomProperty.VC_S_DENIED_SIDS);
	            if (shareDeniedSids != null) {
	                log.info(transID + " Share Denied SIDs : "+shareDeniedSids);
	                resource.setProperty(CustomProperty.VC_S_DENIED_SIDS, shareDeniedSids);
	            }
	        } catch (RepositoryException rex) {
	            log.error(transID + " Exception while assigning SIDs: " + rex);
	            rex.printStackTrace();
	        }
	    }
	    
	
	    private String encodeURL(String url) {
	        if (url == null || url.length() < 2) {
	            return null;
	        }

	        String[] tokens = url.split("/");//.substring(1)
	        String[] correctedTokens = new String[tokens.length];
	        int i = 0;

	        try {
	            for (String t : tokens) {
	                correctedTokens[i++] = URLEncoder.encode(t, "utf-8").replaceAll("[+]", "%20");
	            }
	        } catch (UnsupportedEncodingException ex) {
	            this.logError(ex, null);
	        }
	        StringBuilder stb = new StringBuilder();
	        for (String t : correctedTokens) {
	            log.debug(" token added --> "+t);
	            stb.append('/').append(t);
	        }
	        return stb.toString();
	  }
	    
	    private String createLinkedNode(Node existingNode, Node parentOfLinkedNode, String fileName, String docURL, ValueFactory valueFactory, HashMap aclAttribs, HashMap layerAttribs, String agentType,String contentType, String transID) throws RepositoryException {
	        //private Node createLinkedNode(Node existingNode, Node parentOfLinkedNode, String fileName, String docURL, ValueFactory valueFactory, String strCreationDate) throws RepositoryException {
	            log.info(transID + " createLinkedNode: Started");
	            String filePath = null;
	            Node linkedFile = null;
	            Value refExisitNode = null;

	            log.debug(transID + " Existing node path: " + existingNode.getPath());
	            //Now LinkedNode ID will be returned back for Stub instead of Existing Node ID...
	            ///////////////filePath = existingNode.getParent().getIdentifier();//.getPath();

	            refExisitNode = valueFactory.createValue(existingNode, true);
              
				linkedFile = parentOfLinkedNode.addNode(fileName, NodeType.NT_FILE);
	        	
	            log.debug(transID + " Linked Node created: "+linkedFile.getName());
	            
	            //jcr_content property does not exist for NT_FILE..  
	            //linkedFile.setProperty(Property.JCR_CONTENT, refExisitNode);
	            
	            log.debug(transID + " Linked File Path is: " + linkedFile.getPath());
	            filePath    =   linkedFile.getIdentifier();
	            log.debug(transID + " Return the Linked File ID " + filePath);

	            try {
	            	Node resource;
	            	  
		                resource = (agentType.equals("FS"))
		                        ? linkedFile.addNode(Property.JCR_CONTENT, CustomNodeType.VC_FS_RESOURCE)
		                        : linkedFile.addNode(Property.JCR_CONTENT, CustomNodeType.VC_SP_RESOURCE);
	            	   
		                log.info(transID + " JCR Content in NodeImpl created for linkedNode: " + resource.getIdentifier());
	            	  
		                resource.addMixin(NodeType.MIX_SHAREABLE);
		               
	            	
	                
	                //Binary data =   "01".getBytes();//Data will not be stored in case of linked Node..  ;-)
	                String tempStr  =   "Linked to "+existingNode.getIdentifier()+"";
	                InputStream in  =    new ByteArrayInputStream(tempStr.getBytes());
	                Binary binaryValue = valueFactory.createBinary(in);
	                
	                resource.setProperty(Property.JCR_DATA, binaryValue);
	                
	                //if(resource.hasProperty(CustomProperty.VC_LINKED_TO)) {//Mark it as linked
	                 resource.setProperty(CustomProperty.VC_LINKED_TO, refExisitNode);
	                 System.out.println("id for new linked file: " + linkedFile.getIdentifier());
	                 log.info(transID + " Marked as Linked Node: " + linkedFile.getPath());
	                 
	                 //add linked node id to original node content
	                 
	                 String linkedNodeIds = "";
	                 Node parentNode = existingNode;//.getParent();
	                 log.debug("node: " + parentNode);
	                 if(parentNode.hasProperty(CustomProperty.VC_LINK_NODES)){
							Property prop = parentNode.getProperty(CustomProperty.VC_LINK_NODES);
							linkedNodeIds = prop.getString();
							log.debug("Folder linkedNodeIds before " + linkedNodeIds);
							
							if(!linkedNodeIds.contains(linkedFile.getIdentifier()))
							{
								linkedNodeIds += "," + linkedFile.getIdentifier();
							}

							log.debug("Folder linkedNodeIds after" + linkedNodeIds);						

	                 }else{
	                	 linkedNodeIds = linkedFile.getIdentifier();
	                 }
	                 parentNode.setProperty(CustomProperty.VC_LINK_NODES, linkedNodeIds);
	                 log.debug("Folder linkedNodeIds after" + linkedNodeIds);
	               
	                //} else {
	                //    log.warn("Custom Linked node is not marked as LInked: " + linkedFile.getPath());
	                //}
	                
	                if (layerAttribs != null) {
	                	 setCustomPropertiesOfNode(resource, fileName, aclAttribs, layerAttribs, docURL, agentType,contentType, transID);
	                	
	                }
	                log.info(transID + " After setting properties including sids" + linkedFile.getPath());
	                
	            } catch (RepositoryException e) {
	                log.error(transID + " Ouch..");
	                this.logError(e, transID);
	                throw e;            
	            }
	            
	            //Set the property in ExistingDoc
	            log.debug(transID + " Setting property " + CustomProperty.VC_DOC_URL + ": " + docURL);
	            log.debug(transID + " Node type name: " + existingNode.getDefinition().getName());
	            VCSUtil.dump(existingNode);
	            Property multiValueURLProperty = existingNode.getProperty(CustomProperty.VC_DOC_URL);
	            VCSUtil.dumpProperty(multiValueURLProperty);
	            Value[] values = this.addValuetoMultiValueProperty(multiValueURLProperty, valueFactory.createValue(docURL));
	           
	            existingNode.setProperty(CustomProperty.VC_DOC_URL, values);
	            
	            log.debug(transID + " Values of DOCURL....");
	            for (Value v : values) {
	                log.debug(transID + " Value: " + v.getString());
	            }

//	            Calendar c = Calendar.getInstance();
//	            if (strCreationDate != null) {
//	                try {
//	                    SimpleDateFormat srcDateFormat = new SimpleDateFormat(SRC_DATE_FORMAT);
//	                    Date creationDate = srcDateFormat.parse(strCreationDate);
//	                    c.setTime(creationDate);
//	                    linkedFile.setProperty(CustomProperty.VC_CREATION_DATE, c);
//	                } catch (ParseException ex) {
//	                    this.logError(ex, transID);
//	                    ex.printStackTrace();
//	                }
//	            }
//	            return linkedFile;//Return the path of Existing original Node
	            
	            
	            return "LN|" + filePath;//Return the path of Existing original Node
	   }
 
	    private Value[] addValuetoMultiValueProperty(Property p, Value v) {
	        Value[] values = null;
	        try {
	            values = p.getValues();
	            for (Value vi : values) {
	                if (vi.getString().equals(v.getString())) {
	                    log.debug(" Property already exists");
	                    return values;
	                }
	            }
	            ArrayList<Value> arrayList = new ArrayList();
	            arrayList.addAll(Arrays.asList(values));
	            arrayList.add(v);
	            values = arrayList.toArray(values);

	        } catch (ValueFormatException ex) {
	            this.logError(ex, null);
	        } catch (RepositoryException ex) {
	            this.logError(ex, null);
	        }
	        return values;
	  }  
	    
	  private  byte[] getMD5Hash(InputStream in) {
	        byte[] hash = null;
	        try {

	            MessageDigest md5 = MessageDigest.getInstance("MD5");
	            int chunkSize = 1024 * 4;
	            byte[] chunk = new byte[chunkSize];
	            int available = 0;
	            while ((available = in.available()) > 0) {
	                if (available > chunkSize) {
	                    in.read(chunk);
	                    md5.update(chunk);
	                } else {
	                    in.read(chunk, 0, available);
	                    md5.update(chunk, 0, available);
	                }
	            }
	            hash = md5.digest();
	            in.close();

	        } catch (IOException ex) {
	            log.error(ex);
	        } catch (NoSuchAlgorithmException ex) {
	            log.error(ex);
	        }
	        return hash;
	    }
 
	
	
	 private void logError(Exception ex, String transID) {
        log.error(transID + " logError: " + ex.getMessage());

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        log.error(transID + " "+ sw.toString()); // stack trace as a string
        
   }
	 
	 
	 public ArrayList<Document> getAllChildren(String parent, String layerAttribs) {

	        String secureInfo = "";	        
	        ArrayList<Document> children = null;
	        Node root;
	        secureInfo = layerAttribs;
	        log.debug("Secure Info extracted in RepositryOpManager: " + secureInfo);
//	            un comment in production
	        SimpleCredentials creds = SecurityUtil.decodeSecureInfo(secureInfo);
//	          un comment in development
//	        SimpleCredentials creds = new SimpleCredentials("admin", "admin".toCharArray());

	        try {
	         
	            root = globalSession.getRootNode();

	            Node desiredNode = searchByPath(root, parent);

	            if (desiredNode != null) {
	            	
	            	String nodeType = desiredNode.getPrimaryNodeType().getName();
	            	children = new ArrayList<Document>();
	            	
	            	if(nodeType.equalsIgnoreCase("nt:folder")) {//if its a folder path, having children

	            		log.debug("desired path is a folder");
	            		
		                NodeIterator it = desiredNode.getNodes();
		                if (it != null) {
		                    
		                    while (it.hasNext()) {
		                        Node node = it.nextNode();
		                        
		                        Document document	= this.populateDocumentVOs(node, secureInfo);
	
		                        children.add(document);
		                    }
		                } else {
		                    new RepositoryException("no children found at the given path");
		                }
		                
	            	} else {//if its a file / linked
	            		
	            		log.info("Populating desired File VO");
	            		Document document	= this.populateDocumentVOs(desiredNode, secureInfo);
	            		
                        children.add(document);
	            	}
	            } else {
	                new RepositoryException("Invalid path");
	            }

	        } catch (LoginException ex) {
	            System.out.println(ex);
	            ex.printStackTrace();
	            this.logError(ex, null);
	        } catch (RepositoryException ex) {
	            System.out.println(ex);
	            ex.printStackTrace();
	            this.logError(ex, null);
	        } catch (Exception ex) {
	            System.out.println(ex);
	            ex.printStackTrace();
	            this.logError(ex, null);
	        } 
	        return children;
	    }
	 
	 public  String documentExistsByProperties(String fileContextPath, HashMap layerAttribs) throws Exception{
		 System.out.println("checking de-duplication");
		 log.debug("Checking de-duplication");
		 boolean exists =  false;
		 String url = null;
		 RepositoryUtil	ru	=	getRepository();	//initializes SessionManager and Repostiory instance
		 String[] pathElements = StringUtils.getPathElements(fileContextPath);
         if (pathElements == null || pathElements.length < 1) {
             throw new WebServiceException("Context Path of the file could not be parsed");
         }
		 String secureInfo = (layerAttribs != null) ? (String) layerAttribs.get(VCSConstants.SECURE_INFO) : "";
         log.debug("Secure Info extracted in RepositryOpManager: " + secureInfo);
         SimpleCredentials creds = SecurityUtil.decodeSecureInfo(secureInfo);
		 Session session = ru.getSession(repository,  "default",creds);//new SimpleCredentials("username"+Math.random(), "password".toCharArray()));
		 Node node = null;
         Node root = session.getRootNode();
         
       //check if same path exists
         try {
			node =  root.getNode(fileContextPath);
			exists = true;
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
         if(!exists){
        	 boolean found = false;
	         String path = root.getPath();
	         Node parent = this.getCompanyNode(session, root);//Get the companyName
				log.info("path not found, checking using case-insensitivity");
				System.out.println("path not found, checking using case-insensitivity");
				try {
//					Node jcrNode=session.getRootNode();

//					Node parent = jcrNode;
//					String[] pathElements = com.virtualcode.util.StringUtils.getPathElements(nodePath);
					String element = null;
//					Node node = null;
					for (int i = 0; i < pathElements.length; i++) {
						element = pathElements[i];
						if (element.length() == 0) {
							continue;
						}
						found = false;
						//element = URLDecoder.decode(element, "utf-8");
						log.debug("Element: " + element);
						if (!parent.hasNode(element)) {
							//if child not exist, then check for Case-Insensitivity first
							found = false;
							NodeIterator ni = parent.getNodes();
							while (ni.hasNext()) {//compare with all children
								String tempName = ni.nextNode().getName();
								if (element.equalsIgnoreCase(tempName)) {
									element = tempName;
									found = true;
									break;//terminate the iterations if matched with any
								}
							}

							if (found) {
								node = parent.getNode(element);
//								found = true;
							}else{
//								return null;	// not found after case insensitivity
								exists = false;
								break;
							}
							parent = node;
							System.out.println(parent.getPath());


						} else {
							//log.debug("Node exist");
							node = parent.getNode(element);
							parent = node;
							exists = true;
						}
						log.debug("Element after case matching: " + element);
//						if(!found)
//							return null;
					}
//					return copyNodeAllProperties(parent, new CustomTreeNode());
				} catch (RepositoryException e) {
//					return null;
				} catch(Exception e){
					e.printStackTrace();
				}
         }
         
         //if node is present, check for metadata
         if(exists){

                     String rpModifDate	=	"";
                     if (node.hasProperty(Property.JCR_LAST_MODIFIED)) {
                         rpModifDate	=	node.getProperty(Property.JCR_LAST_MODIFIED).getString().substring(0,19).replaceAll("[a-zA-Z]", " ");
                     }
                     String rpCreatDate	=	"";
                     if (node.hasProperty(CustomProperty.VC_CREATION_DATE)) {
                         rpCreatDate	=	node.getProperty(CustomProperty.VC_CREATION_DATE).getString().substring(0,19).replaceAll("[a-zA-Z]", " ");
                     }
                     
                     String rpFileSize = "";
                     
                     if(node.hasProperty(CustomProperty.VC_FILE_SIZE)){
                    	 rpFileSize = node.getProperty(CustomProperty.VC_FILE_SIZE).getLong()+"";
                     }
                     
                     String strModifiedOn = ((String) layerAttribs.get("ModifiedOn")).substring(0,19).replaceAll("[a-zA-Z]", " ");//2013-09-17T21:08:06.186+0500
                     String strCreationDate = ((String) layerAttribs.get("CreatedOn")).substring(0,19).replaceAll("[a-zA-Z]", " ");//2013-09-17T21:08:06.186+0500
                     String strFileSize = (String) layerAttribs.get("fileSize");

                    log.info(" comparing Modif Date "+rpModifDate+" | "+strModifiedOn);
             		log.info(" comparing Creat Date "+rpCreatDate+" | "+strCreationDate);
             		log.info(" comparing File Size "+rpFileSize+" | "+strFileSize);
             		
             		if(rpModifDate.equals(strModifiedOn) && rpCreatDate.equals(strCreationDate) && rpFileSize.equals(strFileSize)) {//if file with MetaData, already exists at desired location
             			exists = true;
             			url = node.getIdentifier();
             		}else{
             			exists = false;
             		}
             }
         if (url != null && !url.isEmpty()) {             
        	 String transID	=	"remote";
             url = this.prepareIDOfNode(url, secureInfo, transID);
             log.debug("After preparing url ---->"+url);
         }else{
         	return null;
         }
        
		 return url;
	 }
	 
	 
	 private Document populateDocumentVOs(Node node, String secureInfo) throws Exception {

         String nodeType = node.getPrimaryNodeType().getName();

         boolean isFileNode = nodeType.equalsIgnoreCase("nt:file");
         boolean isFolderNode = nodeType.equalsIgnoreCase("nt:folder");
         boolean isLinkedNode = nodeType.equalsIgnoreCase("nt:linkedFile");
         String url = "";
         String stubUrl = "";

         Document document = new Document();
         document.setId(node.getIdentifier());
         document.setTitle(node.getName());
         document.setFolderYN(isFolderNode);
         document.setLinkedNodeYN(isLinkedNode);
         document.setFileYN(isFileNode);
         document.setUrl(node.getPath());

         if (isLinkedNode || isFileNode) {

             url = node.getPath();   //the characters before | i.e. FN| are not sent in the final encoded URL
             // in case of a linked node, send the url of the original node
             if (isLinkedNode) {
                 log.debug("is linked node" + node.getPath());
                 String UID = node.getProperty("jcr:content").getString();

                 node = globalSession.getNodeByIdentifier(UID);
                 log.debug("Path of refference: " + node.getParent().getPath() + " and name = " + node.getName());
                 //System.out.println("Path of refference: " + node.getPath() + " and name = " + node.getName());
                 //omitting the /jcr:content part from the URL
                 url = node.getParent().getPath();  //the characters before | i.e. LN| are not sent in the final encoded URL
             }
             //in case of vc linked node
             if(isFileNode && node.getNode(Property.JCR_CONTENT).hasProperty(CustomProperty.VC_LINKED_TO)) {
                 
                 log.debug("nt:file has propeorty of "+CustomProperty.VC_LINKED_TO);
                 Property prop  =   node.getNode(Property.JCR_CONTENT).getProperty(CustomProperty.VC_LINKED_TO);
                 String refNodeID   =   (prop!=null)?prop.getString():null;
                 if(refNodeID!=null) {
                     log.debug("a linked node of type nt:file");
                     node    =   globalSession.getNodeByIdentifier(refNodeID).getParent();
                 }
             }

             //String wspName = session.getWorkspace().getName();
             if (url != null) {
                 //url = this.prepareURLOfNode(url, wspName, secureInfo);

                 //This is the URL for Export Job only, so modified it accordingly
                 //url = url.replaceAll("[/]SecureGet[/]", "/downloadfile.jsp?nodeID=");
             	//prepare stub URL of node
             	stubUrl = this.prepareIDOfNode("FN|" +url, secureInfo, null);
                                                 
                 //url =   "/downloadfile.jsp?nodeID="+secureURL.getEncryptedURL(url)+"&secureInfo="+secureInfo;
               
					url =   "/DownloadFile?nodeID="+secureURL.getEncryptedURL(url)+"&secureInfo="+secureInfo;
             
                 log.debug("url(encoded): " + url);
                 
                 
                 
                 //url =   url.replaceAll("[/]SecureGet[/]", "/SecureGet1/");
                 log.debug("Final url: " + url);
             }
             document.setStubURL(stubUrl);
             document.setSecureURL(url);
         }

         NodeIterator content = node.getNodes("jcr:content");
         if (content != null && content.hasNext()) {
        	 log.debug("it has content");
             Node contentNode = content.nextNode();

             if (contentNode.hasProperty(Property.JCR_MIMETYPE)) {
                 document.setMimeType(contentNode.getProperty(Property.JCR_MIMETYPE).getString());
             }
             if (contentNode.hasProperty(Property.JCR_LAST_MODIFIED_BY)) {
                 document.setModifiedBy(contentNode.getProperty(Property.JCR_LAST_MODIFIED_BY).getString());
             }
             if (contentNode.hasProperty(Property.JCR_LAST_MODIFIED)) {
                 document.setModifiedOn(contentNode.getProperty(Property.JCR_LAST_MODIFIED).getString().substring(0,19).replaceAll("[a-zA-Z]", " "));
             }
//             doc URL not required
//             if (contentNode.hasProperty(CustomProperty.VC_DOC_URL)) {
//                 //multivalue property
//                 Value[] values = contentNode.getProperty(CustomProperty.VC_DOC_URL).getValues();
//                 String val = "";
//                 for (int i = 0; i < values.length; i++) {
//                     val += values[i].getString() + ", ";    // get only the last property
//                 }
//                 System.out.println(val + " and " + nodeType);
//                 document.setUrl(val);
//             }
             if (contentNode.hasProperty(CustomProperty.VC_ACCESSED_ON)) {
                 document.setAccessedOn(contentNode.getProperty(CustomProperty.VC_ACCESSED_ON).getString().substring(0,19).replaceAll("[a-zA-Z]", " "));
             }
             if (contentNode.hasProperty(Property.JCR_CREATED_BY)) {
                 document.setAuthor(contentNode.getProperty(Property.JCR_CREATED_BY).getString());
             }
             if (contentNode.hasProperty(CustomProperty.VC_CREATION_DATE)) {
                 document.setCreatedOn(contentNode.getProperty(CustomProperty.VC_CREATION_DATE).getString().substring(0,19).replaceAll("[a-zA-Z]", " "));
             }
             if (contentNode.getIdentifier()!=null) {//required in Export/Restore job types
                 document.setHashValue(contentNode.getIdentifier());
             }
             if (contentNode.hasProperty("jcr:data")) {
                 document.setSize(contentNode.getProperty("jcr:data").getLength());
             }

         } else {
        	 log.debug("invalid node desired");
//             document.setFolderYN(true);
         }
         
         return document;
	 }
	 
	 public String downloadDocument(String path, String layerAttribs) {

	        InputStream is = null;
	        String secureInfo = "";
	      
	        String url = "";
//	        ByteArrayOutputStream out = new ByteArrayOutputStream();

	        Node root;
	        secureInfo = (layerAttribs != null) ? (String) layerAttribs : "";
	        log.debug("Secure Info extracted in RepositryOpManager: " + secureInfo);
//	            un comment in production
	        SimpleCredentials creds = SecurityUtil.decodeSecureInfo(secureInfo);

//	        SimpleCredentials creds = new SimpleCredentials("admin", "admin".toCharArray());

	        try {
	           
	            root = globalSession.getRootNode();


	            Node file = null;

	            file = searchByPath(root, path);
	            log.debug("File to download" + file.getPath());

	            if (file != null) {
//	                System.out.println("File to download : " + file.getPath() + " id = " + file.getIdentifier());
	                String nodeType = file.getPrimaryNodeType().getName();
	                boolean isLinkedNode = nodeType.equalsIgnoreCase("nt:linkedFile");
	                boolean isFolderNode = nodeType.equalsIgnoreCase("nt:folder");
	                boolean isFileNode = nodeType.equalsIgnoreCase("nt:file");
//	                if the file is linked node, then get the UID from
	                if (isFolderNode) {
	                    new RepositoryException("Node content not found");
	                }
	                if (isLinkedNode) {
	                    log.debug("is linked node");
	                    String UID = file.getProperty("jcr:content").getString();

	                    file = globalSession.getNodeByIdentifier(UID);
	                    log.debug("Path of refference: " + file.getPath() + " and name = " + file.getName());
	                    System.out.println("Path of refference: " + file.getPath() + " and name = " + file.getName());

	                }
//	                url = file.getPath();
	                Node content = null;
	                if (isLinkedNode) {
//	                    content = file;
	                    url = "LN|" + file.getPath();
	                } else {
//	                    content = file.getNode("jcr:content");
	                    url = "NN|" + file.getPath();
	                }
	                String wspName = globalSession.getWorkspace().getName();

	                if (url != null) {
	                    url = this.prepareURLOfNode(url, wspName, secureInfo);

	                    //Now update the new values into Repository
	                    //this.statusObj.saveValuesOfStatus(repoStats);
	                }

	                if (content != null) {
	                    Binary bin = content.getProperty("jcr:data").getBinary();
	                    is = bin.getStream();
////	                    System.out.println("Stream size " + bin.getSize());
	                    log.debug("size of stream: " + bin.getSize());
	                    ;
//	                    int a;
//	                    byte[] buffer = new byte[4096];
//	                    while ((a = is.read(buffer, 0, 4096)) != -1) {
//	                        out.write(buffer, 0, a);
//	                    }
//	                    out.flush();
	                } else {
//	                    new RepositoryException("Node content not found");
	                }
	            } else {
	                new RepositoryException("Invalid path");
	            }

	        } catch (LoginException ex) {
	            System.out.println(ex);
	            ex.printStackTrace();
	            this.logError(ex, null);
	        } catch (RepositoryException ex) {
	            System.out.println(ex);
	            ex.printStackTrace();
	            this.logError(ex, null);
	        } catch (Exception ex) {
	            System.out.println(ex);
	            ex.printStackTrace();
	            this.logError(ex, null);
	        } 

	        return url;
	    }
	 

	    private Node searchByPath(Node parent, String path) throws Exception {


	        String element = null;
	        Node node = null;

	        int i = 0;
	        String[] pathElements = StringUtils.getPathElements(path);
	        for (; i < pathElements.length; i++) {
	            element = pathElements[i];
	            if (element.length() == 0) {
	                continue;

	            }

	            log.debug("Element: " + element);
	            if (!parent.hasNode(element)) {

	                parent = null;
	                break;
	            } else {

	                node = parent.getNode(element);
	                parent = node;
	            }
	        }

	        return parent;
	    }

	 
	 
	    private String prepareURLOfNode(String url, String wspName, String secureInfo) {

	        String output = null;
	        log.debug("******************  DUMP NODE Start   ******************");
	        //dump(root);

	        if (url != null && wspName != null) {

	            log.debug("url(original): " + url);
	            String[] temp = url.split("\\|");
	            log.debug("Splitted: [0]" + temp[0] + ", [1]" + temp[1]);

	            //url = temp[0] + "|" + "/repository/" + wspName + temp[1];
	            url = "/repository/" + wspName + temp[1];//Skip the CODE Appending functionality.. untill the Agent is sync with it

	            log.debug("url(actual): " + url);
	            url = encodeURL(url);
	            log.debug("url(encoded): " + url);

	            url = url + "?secureInfo=" + secureInfo;
	            log.debug("*** Document URL after adding SecureInfo: " + url);


	            //encrypt URL
	            try {
	                log.debug("secureURL.getEncryptedURL(url) ---> "+secureURL.getEncryptedURL(url));
	             
					    url = VCSConstants.SECURE_GET_SERVLET_PATH + secureURL.getEncryptedURL(url);
	            	
	                log.debug("secureURL.final(url) ---> "+url);
	                output = url;
	            } catch (Exception e) {//if some error occured to Encrypt URL
	                this.logError(e, null);
	                output = VCSConstants.DD_ERROR;
	            }
	        }
	        log.debug("#####  Output  ##### :" + output);

	        return output;
	    }

	     
		
    private Session globalSession	=	null;
    private Repository repository	=	null;
	    
	private JcrTemplate jcrTemplate;
	
	
	public void setJcrTemplate(JcrTemplate jcrTemplate) {
		this.jcrTemplate = jcrTemplate;	
			
		try{
			this.globalSession=jcrTemplate.getSessionFactory().getSession();//its a readonly session for Downloading/Export Jobs etc
			repository	=	globalSession.getRepository();
			
		}catch(Exception ex){
			//ex.printStackTrace();
		}
			
	}
	
	
    private SystemCodeService systemCodeService;

	public void setSystemCodeService(SystemCodeService systemCodeService) {
		this.systemCodeService = systemCodeService;
	}
	
	
	
	
	

}
