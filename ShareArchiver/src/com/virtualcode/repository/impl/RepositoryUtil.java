package com.virtualcode.repository.impl;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.jcr.Repository;
import javax.jcr.Session;

import org.apache.commons.collections.map.LRUMap;
import org.apache.log4j.Logger;

public class RepositoryUtil {
	private static Map<Integer, Session> sessionMap;
	private static Logger logger = Logger.getLogger(RepositoryUtil.class);
	
	protected int size(){
		return ((sessionMap == null)?0:sessionMap.size());
	}
	
	public RepositoryUtil(int maxConcurrentSessions){
		reinitializeSessionMapResources(maxConcurrentSessions);
		logger.debug("Adding a shutdown hook for JCR session cleanup");
		Runtime.getRuntime().addShutdownHook(new Thread(){
			@Override
			public void run() {
				logger.debug("Starting the shutdown hook");
				cleanJcrSessions();
				logger.debug("Shutdown hook finished running");
			}
		});
		logger.debug("shutdown hook added for JCR session cleanup");
	}
	
	private void cleanJcrSessions(){
		logger.debug("Starting the JCR session cleanup through shutdown hook invokation");
		synchronized (sessionMap) {
			for (Integer aKey : sessionMap.keySet()) {
				try {
					sessionMap.get(aKey).logout();
					logger.debug("Released a session...");
				} catch (Exception e) {
					logger.error(e, e);
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void reinitializeSessionMapResources(int size){
	    logger.debug("Initializing Session Map");
		sessionMap = (Map<Integer,Session>)(new ConcurrentHashMap<Integer, Session>(new LRUMap(size)));
		logger.debug("Succesfully initialized Session Map");
	}
    public Session getSession(	Repository repository,
    								String workspace,
    								javax.jcr.Credentials credentials 
    								) {
   	long threadId = Thread.currentThread().getId();
   	int sessionKeyId = new SessionKey(workspace,threadId).hashCode();
   	logger.debug("Session map is managing " + ((sessionMap == null)?0:sessionMap.size()) + " sessions"); 
   	synchronized (sessionMap) {
		if (sessionMap.containsKey(sessionKeyId)) {
			System.out.println("Session Reused...");
			Session session = sessionMap.get(sessionKeyId);//threadId
			if (session.isLive()) {
				String[] locks = session.getLockTokens();
				for (String aLock : locks){
					session.removeLockToken(aLock);
				}
				return session;
			}
		}
		try {
			Session session = repository
					.login(credentials, workspace);
			sessionMap.put(sessionKeyId, session);
			return session;
		} catch (Exception e) {
			logger.debug(e.getMessage());
			throw new RuntimeException(e);
		}
	}
   }
   class SessionKey{
	   int hashCode = -1;
	   SessionKey(String workspace, Long thread){
		   workspace = (workspace == null)? "default" : workspace;
		   thread = (thread == null)? UUID.randomUUID().clockSequence() : thread;
		   hashCode =
		   17 * workspace.hashCode() +
		   19 * thread.hashCode();
	   }
		@Override
		public int hashCode() {
			return hashCode;
		}
   }
}

