package com.virtualcode.repository.impl;

import javax.jcr.Session;

import org.springmodules.jcr.JcrTemplate;

import com.virtualcode.repository.ArchiveDateRunnerService;
import com.virtualcode.repository.monitor.ArchiveDateRunnerMonitor;
import com.virtualcode.schedular.ArchiveDateRunner;

public class ArchiveDateRunnerServiceImpl implements ArchiveDateRunnerService{

	@Override
	public void startFileSizeRunner() {
		// TODO Auto-generated method stub
		ArchiveDateRunnerMonitor archiveDateRunnerMonitor = ArchiveDateRunnerMonitor.getInstance();
		archiveDateRunnerMonitor.setRunning(true);
		archiveDateRunnerMonitor.setStopped(false);
		ArchiveDateRunner archiveDateRunner = new ArchiveDateRunner(session);
		Thread runner = new Thread(archiveDateRunner);
		runner.start();
	}

	@Override
	public void stopFileSizeRunner() {
		// TODO Auto-generated method stub
		ArchiveDateRunnerMonitor archiveDateRunnerMonitor = ArchiveDateRunnerMonitor.getInstance();
		archiveDateRunnerMonitor.setStopped(true);
		archiveDateRunnerMonitor.setRunning(false);
	}
	
	private Session session;
	private JcrTemplate jcrTemplate;
	
	
	public void setJcrTemplate(JcrTemplate jcrTemplate) {
		this.jcrTemplate = jcrTemplate;	
			
		try{
			session=jcrTemplate.getSessionFactory().getSession();
		}catch(Exception ex){
			ex.printStackTrace();
		}
			
	}

}
