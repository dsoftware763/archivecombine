package com.virtualcode.repository.monitor;

import java.util.List;

import org.apache.log4j.Logger;

import com.virtualcode.services.FilesProcessedSizeService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.util.SpringApplicationContext;
import com.virtualcode.vo.FilesProcessedSize;

public class FileSizeRunnerMonitor {
	static final Logger log = Logger.getLogger("FILESIZERUNNER");
	
	private Long currentFileCount;
	
	private FilesProcessedSize filesProcessedSize;
	
	private static FileSizeRunnerMonitor fileSizeRunnerMonitor;
	
	private boolean stopped;
	
	private boolean running;
	
	public static FileSizeRunnerMonitor getInstance(){
		if(fileSizeRunnerMonitor==null)
			fileSizeRunnerMonitor = new FileSizeRunnerMonitor();
		return fileSizeRunnerMonitor;
	}
	
	private FileSizeRunnerMonitor(){
        ServiceManager serviceManager = (ServiceManager)SpringApplicationContext.getBean("serviceManager");
		FilesProcessedSizeService filesProcessedSizeService = serviceManager.getFilesProcessedSizeService();
		
		List<FilesProcessedSize> list =  filesProcessedSizeService.getAll();
		if(list!=null && list.size()>0)
		{
			filesProcessedSize = list.get(0);
			currentFileCount = filesProcessedSize.getTotalProcessedFiles();
		}else{
			filesProcessedSize = new FilesProcessedSize();
			currentFileCount = 0l;
		}
		running = false;
		stopped = false;
	}
	
	public void saveCurrentStatus(){
		log.debug("saving count: " + filesProcessedSize.getTotalProcessedFiles());
		ServiceManager serviceManager = (ServiceManager)SpringApplicationContext.getBean("serviceManager");
		FilesProcessedSizeService filesProcessedSizeService = serviceManager.getFilesProcessedSizeService();
		filesProcessedSizeService.saveFileProcessedSize(filesProcessedSize);
		log.debug("count saved successfully");
	}

	public Long getCurrentFileCount() {
		return currentFileCount;
	}

	public void setCurrentFileCount(Long currentFileCount) {
		this.currentFileCount = currentFileCount;
		this.filesProcessedSize.setTotalProcessedFiles(currentFileCount);
	}

	public FileSizeRunnerMonitor getFileSizeRunnerMonitor() {
		return fileSizeRunnerMonitor;
	}

	public void setFileSizeRunnerMonitor(FileSizeRunnerMonitor fileSizeRunnerMonitor) {
		this.fileSizeRunnerMonitor = fileSizeRunnerMonitor;
	}

	public FilesProcessedSize getFilesProcessedSize() {
		return filesProcessedSize;
	}

	public void setFilesProcessedSize(FilesProcessedSize filesProcessedSize) {
		this.filesProcessedSize = filesProcessedSize;
	}

	public boolean isStopped() {
		return stopped;
	}

	public void setStopped(boolean stopped) {
		this.stopped = stopped;
	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}


}
