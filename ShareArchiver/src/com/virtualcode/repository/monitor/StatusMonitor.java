/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.repository.monitor;

import java.text.DecimalFormat;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.log4j.Logger;

import com.google.common.util.concurrent.AtomicDouble;
import com.virtualcode.services.RepoStatsService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.services.SystemCodeService;
import com.virtualcode.util.SpringApplicationContext;
import com.virtualcode.util.VirtualcodeUtil;
import com.virtualcode.vo.RepoStats;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.ResourceBundle;
import java.util.Timer;

/**
 *
 * @author se7
 */
public class StatusMonitor {
	
	
	
    static final Logger log = Logger.getLogger(StatusMonitor.class);
        
    private AtomicLong ttlFolders;
    private AtomicLong uniqueFiles;
    private AtomicLong dupFiles;
    private AtomicLong fsDocs;
    private AtomicLong spDocs;
//    private AtomicLong usedSpace;
//    private AtomicLong duplicateDataSize;
    //changed to support stats saving float
    private AtomicDouble usedSpace;
    private AtomicDouble duplicateDataSize;
    
    private static StatusMonitor statusObj = null;
    
    //private static RepoStatsService repoStatsService;
    //private ServiceManager serviceManager;
    /*
    private StatusMonitor(ServiceManager serviceManager){
    System.out.println("hi");	
    }*/
   
    
    /*public void setServiceManager(ServiceManager serviceManager) {
		this.serviceManager = serviceManager;
	//	repoStatsService = this.serviceManager.getRepoStatsService();
	}*/

	public static StatusMonitor getInstance(){
    	//setRepoStatsService(repoStatsService);		
    	  if(statusObj==null){
    		  ResourceBundle bundle = VirtualcodeUtil.getResourceBundle();// ResourceBundle.getBundle("config");    			
    		  String  repoStatusFile=bundle.getString("REPOSITORY_HOME") + "/repoStats.csv";
    		  System.out.println("Initializing status monitor...");
    		  statusObj = new StatusMonitor(repoStatusFile);//new RepositoryStatusManager(this.repo);
    		  statusObj.startThreadToSave(repoStatusFile, statusObj);
    	  }
    	  return statusObj;
    }
    
//    public static void setRepoStatsService(RepoStatsService repoStatsService1) {
//		//repoStatsService = repoStatsService1;
//	}

	private void startThreadToSave(String fileName, StatusMonitor statsObj) {// For time being till development stopped to decrease logs size.
        Timer timer = new Timer();
       timer.schedule(new StatsSavingTask(fileName, statsObj), 1 * 1000, 10 * 1000);//Start after 1 seconds, and repeats every 10sec
    }
    
    private StatusMonitor(String fileName) {
        //This constructor will always be called from the singleton of RepositoryOpManager, that have
        //application level scope... So this class also behave as Singleton
        log.debug("Initializing Stats Monitoring: ");
        ttlFolders  =   new AtomicLong();
        uniqueFiles =   new AtomicLong();
        dupFiles	=	new AtomicLong();
        fsDocs      =   new AtomicLong();
        spDocs      =   new AtomicLong();
        usedSpace   =   new AtomicDouble();
        duplicateDataSize=new AtomicDouble();
        this.populateFromFile(fileName);
        
        this.logCurrentStats();
        
    }
    
    public void incTotalFolders(long num) {
        log.info("incTotalFolders: Started");

        log.debug("TotalFolders: "+ this.ttlFolders.addAndGet(num));
        //this.logCurrentStats();
    }

    public void incUniqueFilesCounter(String agentType, long num, long fileSize) {
        log.info("incFilesCounter: Started");
        
        this.uniqueFiles.addAndGet(num);
        //this.usedSpace.addAndGet(fileSize);
        //changed to store size in MB
        float mbUnit = (1024 * 1024);
        DecimalFormat twoDForm = new DecimalFormat("#.##");	//round to two decimal points
        double fileSizeDouble = Double.valueOf(twoDForm.format(fileSize/mbUnit));  //convert to MB
            
        this.usedSpace.addAndGet(fileSizeDouble); //convert to MB
        
        if("FS".equals(agentType)) {
            this.fsDocs.addAndGet(num);
        } else {
            this.spDocs.addAndGet(num);
        }
        //this.logCurrentStats();
    }
    
    public void incDupFilesCounter(String agentType, long num) {
        this.dupFiles.addAndGet(num);
        //this.logCurrentStats();
    }

    public void incDuplicateDataSize(long fileSize) {
        log.info("incDuplicateDataSize: Started");
        //this.duplicateDataSize.addAndGet(fileSize);        
        //changed to store size in MB
        float mbUnit = (1024 * 1024);
        DecimalFormat twoDForm = new DecimalFormat("#.##");	//round to two decimal points
        double fileSizeDouble = Double.valueOf(twoDForm.format(fileSize/mbUnit));     //convert to MB   
          
        this.duplicateDataSize.addAndGet(fileSizeDouble);
        //this.logCurrentStats();
    }

    
    public void logCurrentStats() {
        log.debug("RepoStats: ttlFolders="+this.getTotalFolders() +", ttlFiles="+this.getUniqueFiles()+", fsFiles="+this.getFSDocs()+", spFiles="+this.getSPDocs()+", usedSpace="+this.getUsedSpace()+",duplicateDataSize="+this.getDuplicateDataSize());
    }
    
    private void populateFromFile(String fileName) {
        
        log.debug("Stats initializing from file: "+fileName);
//        try {
         //   BufferedReader br = new BufferedReader(new FileReader(fileName));
        //changed on new requirement to populate repostats from DB 
        com.virtualcode.services.RepoStatsService repoStatsService = (RepoStatsService) SpringApplicationContext.getBean("repoStatsService");
            RepoStats repoStats = repoStatsService.getLatestStats();
            //String strLine = null;
            StringTokenizer st = null;
            //int lineNumber = 0, tokenNumber = 0;
            
            if(repoStats!=null){
            	ttlFolders.addAndGet(repoStats.getTotalFolders());
            	uniqueFiles.addAndGet(repoStats.getUniqueFiles());
            	dupFiles.addAndGet(repoStats.getDupFiles());
            	 fsDocs  =   new AtomicLong(repoStats.getFsDocs());
            	 spDocs.addAndGet(repoStats.getSpDocs());
            	 usedSpace.addAndGet(repoStats.getUsedSpace());
            	 duplicateDataSize.addAndGet(repoStats.getDuplicateDataSize());
            }

           /* while( (fileName = br.readLine()) != null) {
                //lineNumber++;
                //break comma separated line using ","
                st = new StringTokenizer(fileName, ",");

                //while(st.hasMoreTokens()) {
                    //display csv values
                    //tokenNumber++;
                    //System.out.println("Line # " + lineNumber + 
                    //                        ", Token # " + tokenNumber 
                    //                        + ", Token : "+ st.nextToken());
                //}
                //reset token number
                //tokenNumber = 0;
                
                long    temp    =   0;
                if(st.hasMoreTokens()) {
                    temp    =   Long.parseLong(st.nextToken());
                    ttlFolders.addAndGet(temp);
                }
                if(st.hasMoreTokens()) {
                    temp    =   Long.parseLong(st.nextToken());
                    ttlFiles.addAndGet(temp);
                }
                if(st.hasMoreTokens()) {
                    temp    =   Long.parseLong(st.nextToken());
                    fsDocs  =   new AtomicLong(temp);
                }
                if(st.hasMoreTokens()) {
                    temp    =   Long.parseLong(st.nextToken());
                    spDocs.addAndGet(temp);
                }
                if(st.hasMoreTokens()) {
                    temp    =   Long.parseLong(st.nextToken());
                    usedSpace.addAndGet(temp);
                }
                 if(st.hasMoreTokens()) {
                    temp    =   Long.parseLong(st.nextToken());
                    duplicateDataSize.addAndGet(temp);
                }
            }*/
//        } catch (FileNotFoundException e) {
//            log.error("FileNotFoundException "+fileName);
//        } catch (IOException e) {
//            log.error("IOException for accessing "+fileName);
//        }
    }
    public long getUniqueFiles() {
        return this.uniqueFiles.get();
    }
    public long getDupFiles() {
    	return this.dupFiles.get();
    }
    public long getTotalFolders() {
        return this.ttlFolders.get();
    }
    public long getFSDocs() {
        return this.fsDocs.get();
    }
    public long getSPDocs() {
        return this.spDocs.get();
    }
    public double getUsedSpace() {
        return this.usedSpace.get();
    }

    public double getDuplicateDataSize() {
        return this.duplicateDataSize.get();
    }
   
}
