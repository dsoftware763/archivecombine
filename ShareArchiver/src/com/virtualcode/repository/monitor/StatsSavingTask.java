/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.repository.monitor;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.TimerTask;
import org.apache.log4j.Logger;

import com.virtualcode.services.RepoStatsService;
import com.virtualcode.util.SpringApplicationContext;
import com.virtualcode.vo.FilesProcessedSize;


/**
 *
 * @author se7
 */
public class StatsSavingTask extends TimerTask {
    
    static final Logger log = Logger.getLogger(StatsSavingTask.class);
    private StatusMonitor statsObj;
    private String fileName;
    
    private FileSizeRunnerMonitor fileSizeRunnerMonitor;
    private ArchiveDateRunnerMonitor archiveDateRunnerMonitor;
    
    public StatsSavingTask(String fileName, StatusMonitor statsObj) {
        this.statsObj   =   statsObj;
        this.fileName   =   fileName;
    }
    
    public void run() {
        log.info("Executing Stats Saving Task.");

//        try {
            log.info("Writing the Status Values in " + this.fileName);
            //FileWriter writer = new FileWriter(this.fileName);
            com.virtualcode.services.RepoStatsService repoStatsService = (RepoStatsService) SpringApplicationContext.getBean("repoStatsService");
            com.virtualcode.vo.RepoStats repoStats = repoStatsService.getLatestStats();
            if(repoStats==null){
            	repoStats = new com.virtualcode.vo.RepoStats();
            }
            repoStats.setTotalFolders(this.statsObj.getTotalFolders());
            repoStats.setUniqueFiles(this.statsObj.getUniqueFiles());
            repoStats.setDupFiles(this.statsObj.getDupFiles());
            repoStats.setFsDocs(this.statsObj.getFSDocs());
            repoStats.setSpDocs(this.statsObj.getSPDocs());
            DecimalFormat twoDForm = new DecimalFormat("#.##");	//round to two decimal points
            double fileSizeDouble = Double.valueOf(twoDForm.format(this.statsObj.getUsedSpace()));  //convert to MB
            repoStats.setUsedSpace(fileSizeDouble);
            fileSizeDouble = Double.valueOf(twoDForm.format(this.statsObj.getDuplicateDataSize()));  //convert to MB
            repoStats.setDuplicateDataSize(fileSizeDouble);
            
            repoStatsService.saveRepoStats(repoStats);

            /*writer.append(this.statsObj.getTotalFolders()+",");
            writer.append(this.statsObj.getTotalFiles()+",");
            writer.append(this.statsObj.getFSDocs()+",");
            writer.append(this.statsObj.getSPDocs()+",");
            writer.append(this.statsObj.getUsedSpace()+",");
            writer.append(this.statsObj.getDuplicateDataSize()+"");
            writer.append("\n");
            
            writer.flush();
            writer.close();
        }
        catch(IOException e) {
            e.printStackTrace();
        }*/
        this.statsObj.logCurrentStats();
        
        //to save filesSizeProcessed info
        log.info("Executing Stats Saving Task for file size process");
        fileSizeRunnerMonitor = FileSizeRunnerMonitor.getInstance();
        fileSizeRunnerMonitor.saveCurrentStatus();
        
        archiveDateRunnerMonitor = ArchiveDateRunnerMonitor.getInstance();
        archiveDateRunnerMonitor.saveCurrentStatus();
    }
}