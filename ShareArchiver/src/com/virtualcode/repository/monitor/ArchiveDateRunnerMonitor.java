package com.virtualcode.repository.monitor;

import java.util.List;

import org.apache.log4j.Logger;

import com.virtualcode.services.FilesProcessedSizeService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.util.SpringApplicationContext;
import com.virtualcode.vo.FilesProcessedSize;

public class ArchiveDateRunnerMonitor {
	static final Logger log = Logger.getLogger("ARCHIVEDATERUNNER");
	
	private Long currentFileCount;
	
	private FilesProcessedSize filesProcessedSize;
	
	private static ArchiveDateRunnerMonitor archiveDateRunnerMonitor;
	
	private boolean stopped;
	
	private boolean running;
	
	public static ArchiveDateRunnerMonitor getInstance(){
		if(archiveDateRunnerMonitor==null)
			archiveDateRunnerMonitor = new ArchiveDateRunnerMonitor();
		return archiveDateRunnerMonitor;
	}
	
	private ArchiveDateRunnerMonitor(){
        ServiceManager serviceManager = (ServiceManager)SpringApplicationContext.getBean("serviceManager");
		FilesProcessedSizeService filesProcessedSizeService = serviceManager.getFilesProcessedSizeService();
		
		List<FilesProcessedSize> list =  filesProcessedSizeService.getAll();
		if(list!=null && list.size()>1)
		{
			filesProcessedSize = list.get(1);
			currentFileCount = filesProcessedSize.getTotalProcessedFiles();
		}else{
			filesProcessedSize = new FilesProcessedSize();
			currentFileCount = 0l;
		}
		running = false;
		stopped = false;
	}
	
	public void saveCurrentStatus(){
		log.debug("saving count: " + filesProcessedSize.getTotalProcessedFiles());
		ServiceManager serviceManager = (ServiceManager)SpringApplicationContext.getBean("serviceManager");
		FilesProcessedSizeService filesProcessedSizeService = serviceManager.getFilesProcessedSizeService();
		filesProcessedSizeService.saveFileProcessedSize(filesProcessedSize);
		log.debug("count saved successfully");
	}

	public Long getCurrentFileCount() {
		return currentFileCount;
	}

	public void setCurrentFileCount(Long currentFileCount) {
		this.currentFileCount = currentFileCount;
		this.filesProcessedSize.setTotalProcessedFiles(currentFileCount);
	}

	public ArchiveDateRunnerMonitor getFileSizeRunnerMonitor() {
		return archiveDateRunnerMonitor;
	}

	public void setFileSizeRunnerMonitor(ArchiveDateRunnerMonitor fileSizeRunnerMonitor) {
		this.archiveDateRunnerMonitor = fileSizeRunnerMonitor;
	}

	public FilesProcessedSize getFilesProcessedSize() {
		return filesProcessedSize;
	}

	public void setFilesProcessedSize(FilesProcessedSize filesProcessedSize) {
		this.filesProcessedSize = filesProcessedSize;
	}

	public boolean isStopped() {
		return stopped;
	}

	public void setStopped(boolean stopped) {
		this.stopped = stopped;
	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}


}
