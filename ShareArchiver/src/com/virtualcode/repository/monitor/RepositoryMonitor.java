/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.repository.monitor;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.jcr.LoginException;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.jcr.query.RowIterator;

import org.apache.log4j.Logger;

import com.virtualcode.util.VirtualcodeUtil;
import com.virtualcode.vo.AgentDocument;
import com.virtualcode.vo.OSStatus;
import com.virtualcode.vo.RepositoryStatus;

/**
 *
 * @author AtiqurRehman
 */
public class RepositoryMonitor {

    static final Logger log = Logger.getLogger(RepositoryMonitor.class);
    //private ResourceBundle bundle   = ResourceBundle.getBundle("com.virtualcode.resources.sharearchiver");
    //private String companyName      = bundle.getString("companyName");
    private final String REPOSITORY_STATUS_ONLINE = "ONLINE";
    private final String REPOSITORY_STATUS_OFFLINE = "OFFLINE";
    //private final String REPOSITORY_BOOTSTRAP_PATH = "../webapps/archive/WEB-INF/bootstrap.properties";
    private final String REPOSITORY_HOME = "REPOSITORY_HOME";
    private final String REPOSITORY_VERSION = "REPOSITORY_VERSION";
    private final String SOFTWARE_VERSION = "SOFTWARE_VERSION";
    private static RepositoryMonitor repositoryMonitor;
    private String repositoryPath = "";
    private String softwareVersion = "";
    private String repositoryVersion = "";
    private String repoIndexPath = "/repository/index";
    private String wsIndexPath = "/workspaces/default/index";
    private String secIndexPath = "/workspaces/security/index";

    public static RepositoryMonitor getInstance() {
        if (repositoryMonitor == null) {
            repositoryMonitor = new RepositoryMonitor();
            repositoryMonitor.init();
        }
        return repositoryMonitor;
    }

    private void init() {
        /*** bootstrap is moved to com.virtualcode.resources.bootstrap
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(REPOSITORY_BOOTSTRAP_PATH));
            repositoryPath = (String) properties.getProperty(REPOSITORY_HOME);
            repositoryVersion = (String) properties.getProperty(REPOSITORY_VERSION);
            softwareVersion = (String) properties.getProperty(SOFTWARE_VERSION);
        } catch (IOException e) {
            log.error("init: " + e);
        }
         *
         */
        
        ResourceBundle  bundle  = VirtualcodeUtil.getResourceBundle();//  ResourceBundle.getBundle("config");
        repositoryPath          = bundle.getString(REPOSITORY_HOME);
        repositoryVersion       = bundle.getString(REPOSITORY_VERSION);
        softwareVersion         = bundle.getString(SOFTWARE_VERSION);
    }

    /*
     * calculateRepositoryStatus() is obseleted, bcoz it calculates Status by traversing whole repository.
     * getRepositoryStatus() is its replacement
     * /
    public RepositoryStatus calculateRepositoryStatusOBSELETED(Repository repository) {
        RepositoryStatus repositoryStatus = new RepositoryStatus();
        log.debug("Repository Monitor Start calculation Status");
        try {

            repositoryStatus = getTotalFilesAndFolders(repository, repositoryStatus);
            repositoryStatus = getAgentDocuments(repository, repositoryStatus);
            repositoryStatus.setRepositoryStatus(REPOSITORY_STATUS_ONLINE);
            try {
                long timeConsumed = System.currentTimeMillis();
                /**
                 *As getting size from file system taking long so currently setting commenting it to 0 .
                 * repositoryStatus.setRepositorySize(getFolderSize(repositoryPath));
                 * /
                repositoryStatus.setRepositorySize(0);

                timeConsumed = timeConsumed - System.currentTimeMillis();
                log.debug("Time Consumed to calculate repository size: " + timeConsumed);

                timeConsumed = System.currentTimeMillis();

                /**
                 *As getting size from file system taking long so currently setting commenting it to 0 .
                 * long indexSize = getFolderSize(repositoryPath + repoIndexPath);
                 * indexSize += getFolderSize(repositoryPath + wsIndexPath);
                 * indexSize += getFolderSize(repositoryPath + secIndexPath);
                 * repositoryStatus.setRepositoryIndexSize(indexSize);
                 * /
                timeConsumed = timeConsumed - System.currentTimeMillis();
                log.debug("Time Consumed to calculate repository Index size: " + timeConsumed);
                repositoryStatus.setRepositoryIndexSize(0);

            } catch (Exception ex) {
                log.error("Error while calculating repository Size " + ex);
            }
            repositoryStatus.setOsStatus(getOSStatus());
            repositoryStatus.setRepositoryVersion(repositoryVersion);
            repositoryStatus.setSoftwareVersion(softwareVersion);

        } catch (Exception ex) {
            repositoryStatus.setRepositoryStatus(REPOSITORY_STATUS_OFFLINE);
            log.error("Error while getting repository status :Ex " + ex);
        }
        return repositoryStatus;
    }*/

    public RepositoryStatus getRepositoryStatus(Repository repository) {
        RepositoryStatus repositoryStatus = new RepositoryStatus();
        log.debug("Repository Monitor Start getting Status");

        //Session session = null;
        try {
            //SimpleCredentials creds = new SimpleCredentials("admin", "admin".toCharArray());
            //session = repository.login(creds);
            //Node root   =   session.getRootNode();
            
            //RepositoryStatusManager repoStatsManager    =   new RepositoryStatusManager(repository);
            //HashMap repoStats   =   repoStatsManager.getValuesOfStatusNode(root);
           
            StatusMonitor   statusObj=StatusMonitor.getInstance()  ;// =   opManager.getStatusObj();

            //long temp   =   (Long)repoStats.get(CustomProperty.VC_TOTAL_FILES);
            repositoryStatus.setUniqueFiles(statusObj.getUniqueFiles());
            repositoryStatus.setDupFiles(statusObj.getDupFiles());
            //temp   =   (Long)repoStats.get(CustomProperty.VC_TOTAL_FOLDERS);
            repositoryStatus.setTotalFolders(statusObj.getTotalFolders());

            ArrayList list  =   new ArrayList();
            AgentDocument doc   =   new AgentDocument();
            //temp    =   (Long)repoStats.get(CustomProperty.VC_FS_DOCS);
            doc.setAgentType("File Server");
            doc.setTotalDocuments(statusObj.getFSDocs());
            list.add(doc);
            
            //temp    =   (Long)repoStats.get(CustomProperty.VC_SP_DOCS);
            doc   =   new AgentDocument();
            doc.setAgentType("Share Point");
            doc.setTotalDocuments(statusObj.getSPDocs());
            list.add(doc);
            repositoryStatus.setAgentDocuments(list);

            //temp    =   (Long)repoStats.get(CustomProperty.VC_USED_SPACE);
            repositoryStatus.setRepositorySize(statusObj.getUsedSpace());

            repositoryStatus.setRepositoryStatus(REPOSITORY_STATUS_ONLINE);

            repositoryStatus.setRepositoryIndexSize(0);

            repositoryStatus.setOsStatus(getOSStatus());
            repositoryStatus.setRepositoryVersion(repositoryVersion);
            repositoryStatus.setSoftwareVersion(softwareVersion);
            repositoryStatus.setDuplicateDataSize(statusObj.getDuplicateDataSize());
            File file = new File(repositoryPath);
            
            
        	long totalSpace = file.getTotalSpace(); //total disk space in bytes.
        	long usableSpace = file.getUsableSpace(); ///unallocated / free disk space in bytes.
        	long freeSpace = file.getFreeSpace(); //unallocated / free disk space in bytes.
        	
        	double doublePercentage =  ((freeSpace+0.0)/(totalSpace+0.0))*100.0;
        	
        	double totalSpaceDouble = (totalSpace+0.0)/(1024.0*1024.0);
        	double freeSpaceDouble = (freeSpace+0.0)/(1024.0*1024.0);
        	
        	DecimalFormat df = new DecimalFormat("#.##");   
        	
        	String totalSpaceStr = df.format(totalSpaceDouble) + " MB";
        	
        	if(totalSpaceDouble>1024.0){
        		totalSpaceDouble = totalSpaceDouble/1024.0;
        		totalSpaceStr = df.format(totalSpaceDouble)+" GB";        		
        	}
        	if(totalSpaceDouble>1024.0){
        		totalSpaceDouble = totalSpaceDouble/1024.0;
        		totalSpaceStr = df.format(totalSpaceDouble) + " TB";        		
        	}
        	       	
        	String freeSpaceStr = df.format(freeSpaceDouble) + " MB";
        	if(freeSpaceDouble>1024.0){
        		freeSpaceDouble = freeSpaceDouble/1024.0;
        		freeSpaceStr = df.format(freeSpaceDouble) + " GB";
        	}
        	if(freeSpaceDouble>1024.0){
        		freeSpaceDouble = freeSpaceDouble/1024.0;
        		freeSpaceStr = df.format(freeSpaceDouble) + " TB";
        	}
        	
        	     	
        	
        	
       	
        	long percentage = Math.round(doublePercentage);
//        	System.out.println(percentage);
        	repositoryStatus.setTotalSpace(totalSpaceStr);
        	repositoryStatus.setFreeSpace(freeSpaceStr);
        	repositoryStatus.setTotalFreeSpaceOnDisk(100-percentage);
//        	repositoryStatus.setTotalFreeSpaceOnDisk(100-5);
            

        } catch (Exception ex) {
            repositoryStatus.setRepositoryStatus(REPOSITORY_STATUS_OFFLINE);
            log.error("Error while getting repository status :Ex " + ex);
        }
        return repositoryStatus;
    }

    /**
     *
     * @return
     * @throws Exception
     */
    private RepositoryStatus getAgentDocuments(Repository repository, RepositoryStatus repoStatus) {

        log.debug("Start Calulcating Agent Based Documents");

        long totalDocuments = 0;
        Session session = null;
        try {
            session = repository.login();
            String statement = "SELECT * FROM nt:file WHERE jcr:path='/VC/FS/%'";
            log.debug(" Query String for FS: \n" + statement);

            QueryManager qm = null;
            qm = session.getWorkspace().getQueryManager();
            Query q = qm.createQuery(statement, Query.SQL);
            QueryResult r = q.execute();
            RowIterator rows = r.getRows();
            while (rows != null && rows.hasNext()) {
                Object obj = rows.next();
                totalDocuments++;
            }
            log.debug(" FS agent has totalDocument: \n" + totalDocuments);

            ArrayList list = new ArrayList();
            AgentDocument doc = new AgentDocument();

            doc.setAgentType("FS");
            doc.setTotalDocuments(totalDocuments);
            list.add(doc);

            totalDocuments = 0;
            doc = new AgentDocument();
            statement = "SELECT * FROM nt:file WHERE jcr:path='/VC/SP/%'";
            log.debug(" Query String for SP: \n" + statement);
            q = qm.createQuery(statement, Query.SQL);
            r = q.execute();
            rows = r.getRows();
            while (rows != null && rows.hasNext()) {
                Object obj = rows.next();
                totalDocuments++;
            }
            log.debug(" SP agent has totalDocument: \n" + totalDocuments);

            doc.setAgentType("SP");
            doc.setTotalDocuments(totalDocuments);
            list.add(doc);
            repoStatus.setAgentDocuments(list);
            log.debug("Agent Document has been calculated");
        } catch (LoginException ex) {
            log.error("Calculating Total Documents : EX " + ex);
        } catch (RepositoryException ex) {
            log.error("Calculating Total Documents : EX " + ex);
        } finally {
            if (session != null) {
                try {
                    session.logout();
                } catch (Exception e) {
                    log.warn("unable to logout session EX: " + e);
                }
            }
        }

        return repoStatus;
    }

    /**
     * This method will return Operating Status Object with status indicators
     * @return
     * @throws Exception
     */
    private OSStatus getOSStatus() throws Exception {

        log.debug("Start Calulcating OS Status");
        OSStatus osStatus = new OSStatus();
        Runtime runtime = Runtime.getRuntime();

        osStatus.setFreeMemory(runtime.freeMemory());
        osStatus.setMaxMemory(runtime.maxMemory());
        osStatus.setTotalMemory(runtime.totalMemory());

        osStatus.setOsArchitecture(System.getProperty("os.arch"));
        osStatus.setOsName(System.getProperty("os.name"));
        osStatus.setOsVersion(System.getProperty("os.version"));
        osStatus.setJavaVendor(System.getProperty("java.vendor"));
        osStatus.setJavaVersion(System.getProperty("java.version"));
        osStatus.setHostName(java.net.InetAddress.getLocalHost().getHostName());
        osStatus.setIpAddress(java.net.InetAddress.getLocalHost().getHostAddress());


        return osStatus;
    }

    /**
     * Method will return folder Size in Bytes based on given input path.
     * @param path
     * @return
     * @throws Exception
     */
    public long getFolderSize(String path) throws Exception {
        log.debug("Start Calulcating Size");
        long fileSizeByte = getFileSize(new File(path));
        return fileSizeByte;
    }

    /**
     * Recursive method used to calculate size
     * @param folder
     * @return
     * @throws Exception
     */
    public long getFileSize(File folder) throws Exception {
        long foldersize = 0;
        File[] filelist = folder.listFiles();
        for (int i = 0; i < filelist.length; i++) {
            if (filelist[i].isDirectory()) {
                foldersize += getFileSize(filelist[i]);
            } else {
                foldersize += filelist[i].length();
            }
        }
        return foldersize;
    }
    
    /*
    public RepositoryStatus getTotalFilesAndFolders(Repository repository, RepositoryStatus repoStatus) {
        log.debug(" Starting Calculating Total Files ");
        long totalDocuments = 0;
        Session session = null;
        try {
            session = repository.login();
            String statement = "SELECT * FROM nt:file";
            log.debug(" Query String : \n" + statement);

            QueryManager qm = null;
            qm = session.getWorkspace().getQueryManager();
            Query q = qm.createQuery(statement, Query.SQL);
            QueryResult r = q.execute();
            RowIterator rows = r.getRows();
            log.debug(" asdf: "+r.getColumnNames()[0]);
            
            while (rows != null && rows.hasNext()) {
                Object obj = rows.next();
                totalDocuments++;
            }
            repoStatus.setUniqueFiles(totalDocuments);
            repoStatus.setDupFiles(0);//////without calculation, its an assumption that there isn't any Duplicate File...  :(
            totalDocuments = 0;
            statement = "SELECT * FROM nt:folder";
            log.debug(" Query String: \n" + statement);
            q = qm.createQuery(statement, Query.SQL);
            r = q.execute();
            rows = r.getRows();
            while (rows != null && rows.hasNext()) {
                Object obj = rows.next();
                totalDocuments++;
            }
            repoStatus.setTotalFolders(totalDocuments);

            log.debug(" Number of Documents in Repository are:  " + totalDocuments);
        } catch (LoginException ex) {
            log.error("Calculating Total Documents : EX " + ex);
        } catch (RepositoryException ex) {
            log.error("Calculating Total Documents : EX " + ex);
        } finally {
            if (session != null) {
                try {
                    session.logout();
                } catch (Exception e) {
                    log.warn("unable to logout session EX: " + e);
                }
            }
        }
        return repoStatus;
    }*/
}
