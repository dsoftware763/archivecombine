package com.virtualcode.repository;

public interface FileSizeRunnerService {
	public void startFileSizeRunner();
	public void stopFileSizeRunner();
}
