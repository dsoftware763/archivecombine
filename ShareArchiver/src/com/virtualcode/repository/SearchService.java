package com.virtualcode.repository;

import java.util.List;

import javax.jcr.query.RowIterator;

import com.virtualcode.vo.SearchDocument;
import com.virtualcode.vo.SearchResult;

public interface SearchService {
	
		
	public SearchResult searchDocument(String searchFor, String textPart, String authorName, String modifiedBy, String modifiedStartDate, 
			String modifiedEndDate, String documentName, String securitySids,
			String sortMode, String sortOption, String tags, String targetFolder,String contentType, 
			int currentPage, int itemsPerPage, Long fileSizeStart, Long fileSSizeEnd, String archivedStartDate, String archivedEndDate)throws Exception;
	
	public RowIterator searchForExport(String query);
	
	public List<SearchDocument> searchForReport(String query);

//	public SearchResult searchCifsDocument(String spDocLibID, String authorName, String modifiedBy, String modifiedStartDate, 
//			String modifiedEndDate, String documentName, String securitySids,
//			String sortMode, String sortOption, String targetFolder); 
	
	public SearchResult searchCifsDocument(String spDocLibID, String authorName,String minSize, String maxSize, 
			String createStartDate, String createEndDate, String modifiedStartDate, String modifiedEndDate, 
			String uncPath, String documentName, String documentExtension, String securitySids, String sortMode, String sortOption, 
			String tags, String targetFolder,String contentType, int currentPage, int itemsPerPage, Integer maxSearchRes)throws Exception;
	
	public SearchDocument searchDocument(String spDocLibID, String documentName, String modifiedStartDate);
	
}
