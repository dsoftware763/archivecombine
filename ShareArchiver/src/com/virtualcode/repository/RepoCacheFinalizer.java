package com.virtualcode.repository;

import java.io.File;

import org.springmodules.jcr.jackrabbit.RepositoryFactoryBean;

public class RepoCacheFinalizer{
	
	public RepoCacheFinalizer(String repoHome){
	  
		try{
			if(repoHome.endsWith("/"))
				repoHome=repoHome.substring(0, repoHome.length());
			if(repoHome!=null){
				repoHome=repoHome+"/repository/nodetypes/custom_nodetypes.xml";
			}
			File file=new File(repoHome);
			if(file.exists())
			  file.delete();			
		}catch(Exception ex){
			ex.printStackTrace();
		}		
		 
	}
	
	
	

}
