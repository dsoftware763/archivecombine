package com.virtualcode.repository;

public interface ArchiveDateRunnerService {
	public void startFileSizeRunner();
	public void stopFileSizeRunner();
}
