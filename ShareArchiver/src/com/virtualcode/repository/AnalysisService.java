package com.virtualcode.repository;

import com.virtualcode.vo.SearchResult;

public interface AnalysisService {
	
		
	public SearchResult searchDocument(String spDocLibID, String authorName, String minSize, String maxSize, 
			String createStartDate, String createEndDate, String modifiedStartDate, String modifiedEndDate, 
			String uncPath, String documentName, String documentExtension, String securitySids, String sortMode, String sortOption, 
			String tags, String targetFolder,String contentType, int currentPage, int itemsPerPage,
			Integer maxSearchResults)throws Exception;	
}
