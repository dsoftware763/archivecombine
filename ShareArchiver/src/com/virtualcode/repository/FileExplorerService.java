package com.virtualcode.repository;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.jcr.Binary;
import javax.jcr.Node;

import jcifs.smb.SmbFile;

import com.virtualcode.common.SID;
import com.virtualcode.vo.CustomTreeNode;

public interface FileExplorerService {
	
	public List<CustomTreeNode> getRootNodes();	

	public String getStubMetaData(String secureID);
	
	public Map<String,String> getDriveLettersMap();
	
	public CustomTreeNode getCompanyNode();
	
	public CustomTreeNode getNodeByUUIDOfJcr(String uuid);
	
	public CustomTreeNode getNodeByUUID(String uuid);
	
	public CustomTreeNode getNodeByPath(String nodePath);
	
	public CustomTreeNode getNodeByPath(String nodePath, boolean version);
	
	public List<CustomTreeNode> getAllVersions(String uuid);
	
	public List<CustomTreeNode> getFirstLevelDirctories(String nodePath);
	
	public List<CustomTreeNode> getFirstLevelNodes(String nodePath);
	
	public String getFileFoldersCount(String nodePath);
	
	public CustomTreeNode getParentNode(CustomTreeNode node);
	
	public String getNodeType(Node node);
	
	public Binary downloadFile(String filePath);
	
	public Binary downloadFile(String filePath, boolean isArchive);
	
	public String restoreFile(String filePath, String destPath, boolean isVersion);
	
	public String restoreFile(String filePath, String destPath, String uid, boolean isVersion);
	
	public String restoreContentsOfStub(String secureID, String destPath, String userName, boolean winStubRestore);
	
	public boolean alreadyExistsForRestore(String filePath);
	
	public String archiveFile(String filePath, boolean isVersion);
	
	public boolean authorizeStub(String filePath, String username);
	
	public boolean authorizeSmbFile(SmbFile file, String username);
	
	public boolean authorizeSmbFile(String nodePath, String username);
	
//	public boolean authorizeSmbDir(String filePath, String username);
	
	public List loadPermissions(String path,ArrayList<SID>secAllGroups,ArrayList<SID>securityAllowGroups,ArrayList<SID>securityDenyGroups,ArrayList<SID>shareAllGroups,ArrayList<SID>shareAllowGroups,ArrayList<SID>shareDenyGroups);
	
	public boolean savePermissions(String path,ArrayList<SID>securityAllowGroups,ArrayList<SID>securityDenyGroups,ArrayList<SID>shareAllowGroups,ArrayList<SID>shareDenyGroups, boolean isRecursive, boolean isbatchUpdate, boolean merge);
	
	public boolean syncPermissions(String path,String securityAllowGroups,String securityDenyGroups,String shareAllowGroups,String shareDenyGroups, boolean isRecursive, boolean isbatchUpdate, boolean merge);
	
	public boolean syncPermissions(String path, boolean isRecursive, boolean isbatchUpdate, boolean merge);
	
	public Map<String, String> getAllShareNames();
	
	public String updateTags(String uuid, String tags);
	
	public String updateEmailUserList(String uuid, String email);
	
	public String updateDocumentAccessLogs(String uuid, String email, String ipAddress);
	
	public Long getRestoreProgress(Long startTime);
	
	public Double getPathExportSize(String path);
 
	public Long getPathExportStubsSize(String path);

	public CustomTreeNode createShareFolder(String path); 
	
	public double getFolderSize(String nodePath);
	
}