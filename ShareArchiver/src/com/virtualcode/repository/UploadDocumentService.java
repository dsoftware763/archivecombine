package com.virtualcode.repository;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import org.apache.log4j.Logger;

import com.virtualcode.agent.das.archivePolicy.dto.SPDocLib;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.cifs.Document;

public interface UploadDocumentService {
	
	public String uploadDocument(String[] pathElements, InputStream in, HashMap layerAttribs, HashMap aclAttribs) throws Exception;
	
	public String uploadDocumentByBuiltInAgent(String[] pathElements, FileTaskInterface task, HashMap layerAttribs, HashMap aclAttribs, String transID) throws Exception;

	public String downloadDocument(String path, String layerAttribs);
		
	public ArrayList<Document> getAllChildren(String parent, String layerAttribs);
	
	public long removeAllDocuments(Set<SPDocLib> allPaths, String encodedSecureInfo, String transID, Logger logger);
	
	public String documentExistsByProperties(String fileContextPath, HashMap layerAttribs)throws Exception;
}
