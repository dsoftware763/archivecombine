package com.virtualcode.repository;

import javax.jcr.Node;

public interface UserActivityService {
	
	
    public Node userLogin(String userName,String userIP,String httpSessionId,long sessionCreateTime);
    
    public void userLogout(String userName,String userIP,String httpSessionId,long sessionCreateTime);
    
    public void markOrphanSessions();
		
    public void recordSearchActivity(String userName,String userIP,String httpSessionId,long sessionCreateTime,String searchQuery,int searchResCount);
    
    public void recordDocAccessedActivity(String userName,String userIP,String httpSessionId,long sessionCreateTime,String documentPath);
    
    public void recordDownloadActivity(String userName,String userIP,String httpSessionId,long sessionCreateTime,String documentPath);
    

}
