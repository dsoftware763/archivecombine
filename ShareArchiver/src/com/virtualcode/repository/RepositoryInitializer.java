package com.virtualcode.repository;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.jcr.LoginException;
import javax.jcr.NamespaceException;
import javax.jcr.NamespaceRegistry;
import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.PropertyType;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.UnsupportedRepositoryOperationException;
import javax.jcr.nodetype.NoSuchNodeTypeException;
import javax.jcr.nodetype.NodeType;
import javax.jcr.nodetype.NodeTypeManager;
import javax.jcr.nodetype.NodeTypeTemplate;
import javax.jcr.nodetype.PropertyDefinition;
import javax.jcr.nodetype.PropertyDefinitionTemplate;

import org.apache.log4j.Logger;
import org.springmodules.jcr.JcrTemplate;

import com.virtualcode.common.CustomNodeType;
import com.virtualcode.common.CustomProperty;
import com.virtualcode.common.PLFormatException;
import com.virtualcode.repository.monitor.StatusMonitor;
import com.virtualcode.schedular.FileSizeRunner;
import com.virtualcode.services.MailService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.services.SystemCodeService;
import com.virtualcode.util.License;
import com.virtualcode.util.PLVManager;
import com.virtualcode.vo.SystemCode;

public class RepositoryInitializer {
	
	private Logger log=Logger.getLogger(RepositoryInitializer.class);
	
	private JcrTemplate jcrTemplate;
	private Session  session;
	private ServiceManager serviceManager;   
	
	public RepositoryInitializer(JcrTemplate jcrTemplate,ServiceManager serviceManager){
		this.jcrTemplate=jcrTemplate;
		
		try{
			session=jcrTemplate.getSessionFactory().getSession();
			this.serviceManager=serviceManager;
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		createNodeTypeRegistry();
		try{
		    UserActivityService userActivityService=serviceManager.getUserActivityService();
		    userActivityService.markOrphanSessions();//close open user sessions in repository.
		}catch(Exception ex){
			ex.printStackTrace();
		}
//		FileSizeRunner fileSizeRunner = new FileSizeRunner(session);
//		Thread runner = new Thread(fileSizeRunner);
//		runner.start();
//		runner.start();
		StatusMonitor statusMonitor=StatusMonitor.getInstance();
	}

	 private void createNodeTypeRegistry() {
	        log.info("testNodeRegistry: Started");
	        
	        SystemCode systemCode = serviceManager.getSystemCodeService().getSystemCodeByName("application.version.number");
			if(systemCode!=null){
				log.info("Application version: " + systemCode.getCodevalue());				
			}
	        
	        try {
	        	
	            String vcURI = "http://virtualcode.com/custom_node_types/vc";
	            NamespaceRegistry nsRegister = session.getWorkspace().getNamespaceRegistry();
	            try {
	            	
	            	
	                nsRegister.getPrefix(vcURI);  // throws NamespaceException, if spURI doesn't exist.
	            } catch (NamespaceException nse) {
	                log.debug("vc namespace doesn't exist. Exception: " + nse.getMessage());
	                log.debug("Creating namespace 'vc' and registering it to URI: " + vcURI);
	                nsRegister.registerNamespace("vc", vcURI);
	            }
	            	           
	            NodeTypeManager ntManager = session.getWorkspace().getNodeTypeManager();
	            this.setupTypeVCResource(ntManager);
	            this.setupTypeFSResource(ntManager);
	            this.setupTypeSPResource(ntManager);
	            this.setupTypeResourcePermission(ntManager);
	            this.setupTypeStatusResource(ntManager);

	            /* User & User Activity Nodes Defs */
	            this.setupTypeUser(ntManager);
	            this.setupTypeUserActivity(ntManager);
	            
	            this.displayNodeTypeDefinition(ntManager, CustomNodeType.VC_SP_RESOURCE);

	            //Create the CompanyNode, if the repo initiated first time.. to avoid SNS issue
	            Node temp = session.getRootNode();
	            temp = this.getCompanyNode(session, temp);
	        } catch (LoginException ex) {
	            this.logError(ex);
	        } catch (RepositoryException ex) {
	            this.logError(ex);
	        } finally {
	            if (session != null) {
	                try {
	                    session.save();
	                } catch (Exception ex) {
	                    this.logError(ex);
	                }
	                //session.logout(); singletion session
	            }
	        }
	    }
	 
	 
	 
	 private void setupTypeVCResource(NodeTypeManager ntManager) throws UnsupportedRepositoryOperationException, NoSuchNodeTypeException, RepositoryException {
	        if (!ntManager.hasNodeType(CustomNodeType.VC_RESOURCE_BASE)) {
	            NodeTypeTemplate vcResTemplate = ntManager.createNodeTypeTemplate();
	            vcResTemplate.setName(CustomNodeType.VC_RESOURCE_BASE);

	            vcResTemplate.setDeclaredSuperTypeNames(new String[]{NodeType.NT_RESOURCE, NodeType.MIX_TITLE});
	            vcResTemplate.setAbstract(true);

	            // SPURL
	            PropertyDefinitionTemplate p = ntManager.createPropertyDefinitionTemplate();
	            p.setName(CustomProperty.VC_DOC_URL);
	            p.setRequiredType(PropertyType.STRING);
	            p.setAutoCreated(false);
	            p.setMandatory(false);
	            p.setMultiple(true);

	            List<PropertyDefinition> defs = vcResTemplate.getPropertyDefinitionTemplates();
	            defs.add(p);

	            ntManager.registerNodeType(vcResTemplate, true);
	        }
	    }
	 
	 
	 private void setupTypeUser(NodeTypeManager ntManager) throws UnsupportedRepositoryOperationException, NoSuchNodeTypeException, RepositoryException {
	        if (!ntManager.hasNodeType(CustomNodeType.VC_USER)) {
	            NodeTypeTemplate vcResTemplate = ntManager.createNodeTypeTemplate();
	            vcResTemplate.setName(CustomNodeType.VC_USER);

	            vcResTemplate.setDeclaredSuperTypeNames(new String[]{NodeType.NT_FOLDER, NodeType.MIX_TITLE});
	           
	            // SPURL
	            PropertyDefinitionTemplate p1 = ntManager.createPropertyDefinitionTemplate();
	            p1.setName(CustomProperty.VC_USER_NAME);
	            p1.setRequiredType(PropertyType.STRING);
	            p1.setAutoCreated(false);
	            p1.setMandatory(false);
	            p1.setMultiple(false);
	            p1.setFullTextSearchable(false);
	            
	            // SPURL
	            PropertyDefinitionTemplate p2 = ntManager.createPropertyDefinitionTemplate();
	            p2.setName(CustomProperty.VC_USER_IP);
	            p2.setRequiredType(PropertyType.STRING);
	            p2.setAutoCreated(false);
	            p2.setMandatory(false);
	            p2.setMultiple(false);

	            List<PropertyDefinition> defs = vcResTemplate.getPropertyDefinitionTemplates();
	            defs.add(p1);
	            defs.add(p2); 
	            ntManager.registerNodeType(vcResTemplate, true);
	        }
	    }

	    
	 private void setupTypeUserActivity(NodeTypeManager ntManager) throws UnsupportedRepositoryOperationException, NoSuchNodeTypeException, RepositoryException {
	        if (!ntManager.hasNodeType(CustomNodeType.VC_USER_ACTIVITY)) {
	            NodeTypeTemplate vcResTemplate = ntManager.createNodeTypeTemplate();
	            vcResTemplate.setName(CustomNodeType.VC_USER_ACTIVITY);
               
	            vcResTemplate.setDeclaredSuperTypeNames(new String[]{NodeType.MIX_TITLE,CustomNodeType.VC_USER});
	           
	            // SPURL
	            PropertyDefinitionTemplate p1 = ntManager.createPropertyDefinitionTemplate();
	            p1.setName(CustomProperty.VC_USER_LOGIN_TIME);
	            p1.setRequiredType(PropertyType.STRING);
	            p1.setAutoCreated(false);
	            p1.setMandatory(false);
	            p1.setMultiple(false);
	            
	            // SPURL
	            PropertyDefinitionTemplate p2 = ntManager.createPropertyDefinitionTemplate();
	            p2.setName(CustomProperty.VC_USER_LOGOUT_TIME);
	            p2.setRequiredType(PropertyType.STRING);
	            p2.setAutoCreated(false);
	            p2.setMandatory(false);
	            p2.setMultiple(false);
	            
	            PropertyDefinitionTemplate p3 = ntManager.createPropertyDefinitionTemplate();
	            p3.setName(CustomProperty.VC_USER_SEARCH);
	            p3.setRequiredType(PropertyType.STRING);
	            p3.setAutoCreated(false);
	            p3.setMandatory(false);
	            p3.setMultiple(false);
	            
	            
	            PropertyDefinitionTemplate p4 = ntManager.createPropertyDefinitionTemplate();
	            p4.setName(CustomProperty.VC_USER_SEARCH_L_INDEX);
	            p4.setRequiredType(PropertyType.LONG);
	            p4.setAutoCreated(false);
	            p4.setMandatory(false);
	            p4.setMultiple(false);
	          	            
	            PropertyDefinitionTemplate p5 = ntManager.createPropertyDefinitionTemplate();
	            p5.setName(CustomProperty.VC_USER_ACCESSED_DOCS);
	            p5.setRequiredType(PropertyType.STRING);
	            p5.setAutoCreated(false);
	            p5.setMandatory(false);
	            p5.setMultiple(false);
	            
	            PropertyDefinitionTemplate p6 = ntManager.createPropertyDefinitionTemplate();
	            p6.setName(CustomProperty.VC_USER_ACCESSED_DOCS_COUNT);
	            p6.setRequiredType(PropertyType.LONG);
	            p6.setAutoCreated(false);
	            p6.setMandatory(false);
	            p6.setMultiple(false);

	            
	            PropertyDefinitionTemplate p9= ntManager.createPropertyDefinitionTemplate();
	            p9.setName(CustomProperty.VC_USER_ACCESSED_DOCS_L_INDEX);
	            p9.setRequiredType(PropertyType.LONG);
	            p9.setAutoCreated(false);
	            p9.setMandatory(false);
	            p9.setMultiple(false);
	            
	            PropertyDefinitionTemplate p7 = ntManager.createPropertyDefinitionTemplate();
	            p7.setName(CustomProperty.VC_USER_DOWNLOADED_DOCS);
	            p7.setRequiredType(PropertyType.STRING);
	            p7.setAutoCreated(false);
	            p7.setMandatory(false);
	            p7.setMultiple(false);
	            
	            
	            PropertyDefinitionTemplate p8 = ntManager.createPropertyDefinitionTemplate();
	            p8.setName(CustomProperty.VC_USER_DOWNLOADED_DOCS_COUNT);
	            p8.setRequiredType(PropertyType.LONG);
	            p8.setAutoCreated(false);
	            p8.setMandatory(false);
	            p8.setMultiple(false);

	            
	            PropertyDefinitionTemplate p10= ntManager.createPropertyDefinitionTemplate();
	            p10.setName(CustomProperty.VC_USER_DOWNLOADED_DOCS_L_INDEX);
	            p10.setRequiredType(PropertyType.LONG);
	            p10.setAutoCreated(false);
	            p10.setMandatory(false);
	            p10.setMultiple(false);
	            

	            List<PropertyDefinition> defs = vcResTemplate.getPropertyDefinitionTemplates();
	            defs.add(p1);
	            defs.add(p2); 
	            defs.add(p3);
	            defs.add(p4);
	            defs.add(p5);
	            defs.add(p6); 
	            defs.add(p7);
	            defs.add(p8); 
	            defs.add(p9); 
	            defs.add(p10); 
	            ntManager.registerNodeType(vcResTemplate, true);
	        }
	    }
	 
	 
	    /**
	     * To setup node type for Permission at folder level.
	     * @param ntManager
	     * @throws UnsupportedRepositoryOperationException
	     * @throws NoSuchNodeTypeException
	     * @throws RepositoryException
	     */
	    private void setupTypeResourcePermission(NodeTypeManager ntManager) throws UnsupportedRepositoryOperationException, NoSuchNodeTypeException, RepositoryException {
	        log.debug("setupTypeResourcePermission :: Started");
	        if (!ntManager.hasNodeType(CustomNodeType.VC_RESOURCE_PERMISSIONS)) {
	            log.debug("Setting Templates");
	            NodeTypeTemplate fsResTemplate = ntManager.createNodeTypeTemplate();
	            fsResTemplate.setName(CustomNodeType.VC_RESOURCE_PERMISSIONS);
	            fsResTemplate.setDeclaredSuperTypeNames(new String[]{NodeType.NT_FOLDER, NodeType.MIX_TITLE});

	            // Allowed SIDs
	            PropertyDefinitionTemplate p2 = ntManager.createPropertyDefinitionTemplate();
	            p2.setName(CustomProperty.VC_ALLOWED_SIDS);
	            p2.setRequiredType(PropertyType.STRING);
	            p2.setAutoCreated(false);
	            p2.setMandatory(false);
	            p2.setMultiple(false);

	            // Denied SIDs
	            PropertyDefinitionTemplate p3 = ntManager.createPropertyDefinitionTemplate();
	            p3.setName(CustomProperty.VC_DENIED_SIDS);
	            p3.setRequiredType(PropertyType.STRING);
	            p3.setAutoCreated(false);
	            p3.setMandatory(false);
	            p3.setMultiple(false);

	            // Share Allowed SIDs
	            PropertyDefinitionTemplate p4 = ntManager.createPropertyDefinitionTemplate();
	            p4.setName(CustomProperty.VC_S_ALLOWED_SIDS);
	            p4.setRequiredType(PropertyType.STRING);
	            p4.setAutoCreated(false);
	            p4.setMandatory(false);
	            p4.setMultiple(false);

	            // Share Denied SIDs
	            PropertyDefinitionTemplate p5 = ntManager.createPropertyDefinitionTemplate();
	            p5.setName(CustomProperty.VC_S_DENIED_SIDS);
	            p5.setRequiredType(PropertyType.STRING);
	            p5.setAutoCreated(false);
	            p5.setMandatory(false);
	            p5.setMultiple(false);

	            List<PropertyDefinition> defs = fsResTemplate.getPropertyDefinitionTemplates();
	            defs.add(p2);
	            defs.add(p3);
	            defs.add(p4);
	            defs.add(p5);

	            ntManager.registerNodeType(fsResTemplate, true);
	            log.debug("Node Registered");
	        }
	        log.debug("setupTypeResourcePermission :: Finished");
	    }

	    private void setupTypeStatusResource(NodeTypeManager ntManager) throws UnsupportedRepositoryOperationException, NoSuchNodeTypeException, RepositoryException {
	        //ntManager.unregisterNodeType(CustomNodeType.VC_STATUS_RESOURCE);
	        if (!ntManager.hasNodeType(CustomNodeType.VC_STATUS_RESOURCE)) {

	            NodeTypeTemplate statusResTemplate = ntManager.createNodeTypeTemplate();
	           
	            statusResTemplate.setName(CustomNodeType.VC_STATUS_RESOURCE);
	            statusResTemplate.setDeclaredSuperTypeNames(new String[]{CustomNodeType.VC_RESOURCE_BASE});
	            statusResTemplate.setMixin(true);

	            // Set the Property of totalFolders
	            PropertyDefinitionTemplate p1 = ntManager.createPropertyDefinitionTemplate();
	            p1.setName(CustomProperty.VC_TOTAL_FOLDERS);
	            p1.setRequiredType(PropertyType.LONG);
	            p1.setAutoCreated(false);
	            p1.setFullTextSearchable(false);
	            p1.setMandatory(false);
	            p1.setMultiple(false);

	            // Set the Property of totalFiles
	            PropertyDefinitionTemplate p2 = ntManager.createPropertyDefinitionTemplate();
	            p2.setName(CustomProperty.VC_TOTAL_FILES);
	            p2.setRequiredType(PropertyType.LONG);
	            p2.setAutoCreated(false);
	            p2.setFullTextSearchable(false);
	            p2.setMandatory(false);
	            p2.setMultiple(false);

	            // Set the Property of sharePointDocuments
	            PropertyDefinitionTemplate p3 = ntManager.createPropertyDefinitionTemplate();
	            p3.setName(CustomProperty.VC_SP_DOCS);
	            p3.setRequiredType(PropertyType.LONG);
	            p3.setAutoCreated(false);
	            p3.setFullTextSearchable(false);
	            p3.setMandatory(false);
	            p3.setMultiple(false);

	            // Set the Property of fileSysDocuments
	            PropertyDefinitionTemplate p4 = ntManager.createPropertyDefinitionTemplate();
	            p4.setName(CustomProperty.VC_FS_DOCS);
	            p4.setRequiredType(PropertyType.LONG);
	            p4.setAutoCreated(false);
	            p4.setFullTextSearchable(false);
	            p4.setMandatory(false);
	            p4.setMultiple(false);

	            // Set the Property of usedSpaceByRepo
	            PropertyDefinitionTemplate p5 = ntManager.createPropertyDefinitionTemplate();
	            p5.setName(CustomProperty.VC_USED_SPACE);
	            p5.setRequiredType(PropertyType.LONG);
	            p5.setAutoCreated(false);
	            p5.setFullTextSearchable(false);
	            p5.setMandatory(false);
	            p5.setMultiple(false);

	            List<PropertyDefinition> defs = statusResTemplate.getPropertyDefinitionTemplates();
	            defs.add(p1);
	            defs.add(p2);
	            defs.add(p3);
	            defs.add(p4);
	            defs.add(p5);

	            ntManager.registerNodeType(statusResTemplate, true);
	        }
	    }

	    private void setupTypeFSResource(NodeTypeManager ntManager) throws UnsupportedRepositoryOperationException, NoSuchNodeTypeException, RepositoryException {
//	        
//	        //Remove the old
//	        if(ntManager.hasNodeType(CustomNodeType.VC_FS_RESOURCE)) {
////	            ntManager.unregisterNodeType(CustomNodeType.VC_FS_RESOURCE);        
////	            log.info("VC_FS_RESOURCE Node Un-Registered");
//	        }
	        
	        if (!ntManager.hasNodeType(CustomNodeType.VC_FS_RESOURCE)) {
	            log.info("Node Starting to be registered");
	            
	            NodeTypeTemplate fsResTemplate = ntManager.createNodeTypeTemplate();
	            fsResTemplate.setName(CustomNodeType.VC_FS_RESOURCE);
	            fsResTemplate.setDeclaredSuperTypeNames(new String[]{CustomNodeType.VC_RESOURCE_BASE});

	            // Accessed On
	            PropertyDefinitionTemplate p1 = ntManager.createPropertyDefinitionTemplate();
	            p1.setName(CustomProperty.VC_ACCESSED_ON);
	            p1.setRequiredType(PropertyType.DATE);
	            p1.setAutoCreated(false);
	            p1.setMandatory(false);
	            p1.setMultiple(false);

	            // Creation date
	            PropertyDefinitionTemplate p2 = ntManager.createPropertyDefinitionTemplate();
	            p2.setName(CustomProperty.VC_CREATION_DATE);
	            p2.setRequiredType(PropertyType.DATE);
	            p2.setAutoCreated(false);
	            p2.setMandatory(false);
	            p2.setMultiple(false);
	            
	            // Allowed SIDs
	            PropertyDefinitionTemplate p3 = ntManager.createPropertyDefinitionTemplate();
	            p3.setName(CustomProperty.VC_ALLOWED_SIDS);
	            p3.setRequiredType(PropertyType.STRING);
	            p3.setAutoCreated(false);
	            p3.setMandatory(false);
	            p3.setMultiple(false);

	            // Denied SIDs
	            PropertyDefinitionTemplate p4 = ntManager.createPropertyDefinitionTemplate();
	            p4.setName(CustomProperty.VC_DENIED_SIDS);
	            p4.setRequiredType(PropertyType.STRING);
	            p4.setAutoCreated(false);
	            p4.setMandatory(false);
	            p4.setMultiple(false);

	            // Share Allowed SIDs
	            PropertyDefinitionTemplate p5 = ntManager.createPropertyDefinitionTemplate();
	            p5.setName(CustomProperty.VC_S_ALLOWED_SIDS);
	            p5.setRequiredType(PropertyType.STRING);
	            p5.setAutoCreated(false);
	            p5.setMandatory(false);
	            p5.setMultiple(false);

	            // Share Denied SIDs
	            PropertyDefinitionTemplate p6 = ntManager.createPropertyDefinitionTemplate();
	            p6.setName(CustomProperty.VC_S_DENIED_SIDS);
	            p6.setRequiredType(PropertyType.STRING);
	            p6.setAutoCreated(false);
	            p6.setMandatory(false);
	            p6.setMultiple(false);

	            // IsLinkedNode
	            PropertyDefinitionTemplate p7 = ntManager.createPropertyDefinitionTemplate();
	            p7.setName(CustomProperty.VC_LINKED_TO);
	            p7.setRequiredType(PropertyType.STRING);
	            p7.setAutoCreated(false);
	            p7.setMandatory(false);
	            p7.setMultiple(false);
	            
	            // itsNextVersions
	            PropertyDefinitionTemplate p8 = ntManager.createPropertyDefinitionTemplate();
	            p8.setName(CustomProperty.VC_NEXT_VERSIONS);
	            p8.setRequiredType(PropertyType.STRING);
	            p8.setAutoCreated(false);
	            p8.setMandatory(false);
	            p8.setMultiple(false);
	            
	            // versionOf
	            PropertyDefinitionTemplate p9 = ntManager.createPropertyDefinitionTemplate();
	            p9.setName(CustomProperty.VC_VERSION_OF);
	            p9.setRequiredType(PropertyType.STRING);
	            p9.setAutoCreated(false);
	            p9.setMandatory(false);
	            p9.setMultiple(false);
	            
	            // docTags
	            PropertyDefinitionTemplate p10 = ntManager.createPropertyDefinitionTemplate();
	            p10.setName(CustomProperty.VC_DOC_TAGS);
	            p10.setRequiredType(PropertyType.STRING);
	            p10.setAutoCreated(false);
	            p10.setMandatory(false);
	            p10.setMultiple(false);

	            // docTags
	            PropertyDefinitionTemplate p11 = ntManager.createPropertyDefinitionTemplate();
	            p11.setName(CustomProperty.VC_ARCHIVING_MODE);
	            p11.setRequiredType(PropertyType.STRING);
	            p11.setAutoCreated(false);
	            p11.setMandatory(false);
	            p11.setMultiple(false);	           
	            
	            // fileSize
	            PropertyDefinitionTemplate p12 = ntManager.createPropertyDefinitionTemplate();
	            p12.setName(CustomProperty.VC_FILE_SIZE);
	            p12.setRequiredType(PropertyType.LONG);
	            p12.setAutoCreated(false);
	            p12.setMandatory(false);
	            p12.setMultiple(false);
	            
	            // emailUserList
	            PropertyDefinitionTemplate p13 = ntManager.createPropertyDefinitionTemplate();
	            p13.setName(CustomProperty.VC_EMAIL_USER_LIST);
	            p13.setRequiredType(PropertyType.STRING);
	            p13.setAutoCreated(false);
	            p13.setMandatory(false);
	            p13.setMultiple(false);
	            
	          // acccessLogs
	            PropertyDefinitionTemplate p14 = ntManager.createPropertyDefinitionTemplate();
	            p14.setName(CustomProperty.VC_ACCESS_LOGS);
	            p14.setRequiredType(PropertyType.STRING);
	            p14.setAutoCreated(false);
	            p14.setMandatory(false);
	            p14.setMultiple(false);
	            
	         // archiveDate
	            PropertyDefinitionTemplate p15 = ntManager.createPropertyDefinitionTemplate();
	            p15.setName(CustomProperty.VC_ARCHIVE_DATE);
	            p15.setRequiredType(PropertyType.DATE);
	            p15.setAutoCreated(false);
	            p15.setMandatory(false);
	            p15.setMultiple(false);

	            //is removed
	            PropertyDefinitionTemplate p16 = ntManager.createPropertyDefinitionTemplate();
	            p16.setName(CustomProperty.VC_IS_REMOVED);
	            p16.setRequiredType(PropertyType.STRING);
	            p16.setAutoCreated(false);
	            p16.setMandatory(false);
	            p16.setMultiple(false);
	            
	          //linked nodes
	            PropertyDefinitionTemplate p17 = ntManager.createPropertyDefinitionTemplate();
	            p17.setName(CustomProperty.VC_LINK_NODES);
	            p17.setRequiredType(PropertyType.STRING);
	            p17.setAutoCreated(false);
	            p17.setMandatory(false);
	            p17.setMultiple(false);
	            
	            //is stubbed
//	            PropertyDefinitionTemplate p18 = ntManager.createPropertyDefinitionTemplate();
//	            p18.setName(CustomProperty.VC_IS_STUBBED);
//	            p18.setRequiredType(PropertyType.STRING);
//	            p18.setAutoCreated(false);
//	            p18.setMandatory(false);
//	            p18.setMultiple(false);
	            
	            //Printer name
	            PropertyDefinitionTemplate p18 = ntManager.createPropertyDefinitionTemplate();
	            p18.setName(CustomProperty.VC_PRINTER_NAME);
	            p18.setRequiredType(PropertyType.STRING);
	            p18.setAutoCreated(false);
	            p18.setMandatory(false);
	            p18.setMultiple(false);
	            
//	            machine name
	            PropertyDefinitionTemplate p19 = ntManager.createPropertyDefinitionTemplate();
	            p19.setName(CustomProperty.VC_MACHINE_NAME);
	            p19.setRequiredType(PropertyType.STRING);
	            p19.setAutoCreated(false);
	            p19.setMandatory(false);
	            p19.setMultiple(false);
	            
//	            username
	            PropertyDefinitionTemplate p20 = ntManager.createPropertyDefinitionTemplate();
	            p20.setName(CustomProperty.VC_USER_NAME);
	            p20.setRequiredType(PropertyType.STRING);
	            p20.setAutoCreated(false);
	            p20.setMandatory(false);
	            p20.setMultiple(false);
	            
//	            total pages
	            PropertyDefinitionTemplate p21 = ntManager.createPropertyDefinitionTemplate();
	            p21.setName(CustomProperty.VC_TOTAL_PAGES);
	            p21.setRequiredType(PropertyType.STRING);
	            p21.setAutoCreated(false);
	            p21.setMandatory(false);
	            p21.setMultiple(false);
	            
	            List<PropertyDefinition> defs = fsResTemplate.getPropertyDefinitionTemplates();
	            defs.add(p1);
	            defs.add(p2);
	            defs.add(p3);
	            defs.add(p4);
	            defs.add(p5);
	            defs.add(p6);
	            defs.add(p7);
	            defs.add(p8);
	            defs.add(p9);
	            defs.add(p10);
	            defs.add(p11);
	            defs.add(p12);
	            defs.add(p13);
	            defs.add(p14);
	            defs.add(p15);
	            defs.add(p16);
	            defs.add(p17);
//	            defs.add(p18);
	            defs.add(p18);
	            defs.add(p19);
	            defs.add(p20);
	            defs.add(p21);

	            ntManager.registerNodeType(fsResTemplate, true);
	        }
	        /* else {
	        //code added for testing only
	        //to check if the creatioDate property exists in the repo
	        NodeType nt = ntManager.getNodeType(CustomNodeType.VC_FS_RESOURCE);
	        PropertyDefinition[] definitions = nt.getPropertyDefinitions();
	        boolean hasProperty = false;
	        System.out.println("-----------------------------------Properties----------------------" + definitions.length);
	        for (int i = 0; i < definitions.length; i++) {
	        System.out.println(definitions[i].getName());
	        log.debug(definitions[i].getName());
	        if(definitions[i].getName().equals(CustomProperty.VC_CREATION_DATE)){
	        hasProperty = true;

	        //break;
	        }
	        }
	        if(!hasProperty){
	        System.out.println("does not have property creation date");
	        }
	        System.out.println("-----------------------------------Properties----------------------");

	        } */



	    }

	    private void setupTypeSPResource(NodeTypeManager ntManager) throws UnsupportedRepositoryOperationException, NoSuchNodeTypeException, RepositoryException {
//	        ntManager.unregisterNodeType(VC_SP_RESOURCE);
	        //Remove the old
//	        if(ntManager.hasNodeType(CustomNodeType.VC_SP_RESOURCE)) {
//	            ntManager.unregisterNodeType(CustomNodeType.VC_SP_RESOURCE);        
//	            log.info("VC_SP_RESOURCE Node Un-Registered");
//	        }
	        
	        if (!ntManager.hasNodeType(CustomNodeType.VC_SP_RESOURCE)) {
	            NodeTypeTemplate fsResTemplate = ntManager.createNodeTypeTemplate();
	            fsResTemplate.setName(CustomNodeType.VC_SP_RESOURCE);
	            fsResTemplate.setDeclaredSuperTypeNames(new String[]{CustomNodeType.VC_RESOURCE_BASE});

	            // SPURL
	            PropertyDefinitionTemplate p1 = ntManager.createPropertyDefinitionTemplate();
	            p1.setName(CustomProperty.VC_SP_URL);
	            p1.setRequiredType(PropertyType.STRING);
	            p1.setAutoCreated(false);
	            p1.setMandatory(false);
	            p1.setMultiple(false);

	            // DocVersion
	            PropertyDefinitionTemplate p2 = ntManager.createPropertyDefinitionTemplate();
	            p2.setName(CustomProperty.VC_DOC_VERSION);
	            p2.setRequiredType(PropertyType.STRING);
	            p2.setAutoCreated(false);
	            p2.setMandatory(false);
	            p2.setMultiple(false);

	            //modified 13 JULY 2012
	            //to add sids to sharepoint resource

	            // Allowed SIDs
	            PropertyDefinitionTemplate p3 = ntManager.createPropertyDefinitionTemplate();
	            p3.setName(CustomProperty.VC_ALLOWED_SIDS);
	            p3.setRequiredType(PropertyType.STRING);
	            p3.setAutoCreated(false);
	            p3.setMandatory(false);
	            p3.setMultiple(false);

	            // Denied SIDs
	            PropertyDefinitionTemplate p4 = ntManager.createPropertyDefinitionTemplate();
	            p4.setName(CustomProperty.VC_DENIED_SIDS);
	            p4.setRequiredType(PropertyType.STRING);
	            p4.setAutoCreated(false);
	            p4.setMandatory(false);
	            p4.setMultiple(false);

	            // Share Allowed SIDs
	            PropertyDefinitionTemplate p5 = ntManager.createPropertyDefinitionTemplate();
	            p5.setName(CustomProperty.VC_S_ALLOWED_SIDS);
	            p5.setRequiredType(PropertyType.STRING);
	            p5.setAutoCreated(false);
	            p5.setMandatory(false);
	            p5.setMultiple(false);

	            // Share Denied SIDs
	            PropertyDefinitionTemplate p6 = ntManager.createPropertyDefinitionTemplate();
	            p6.setName(CustomProperty.VC_S_DENIED_SIDS);
	            p6.setRequiredType(PropertyType.STRING);
	            p6.setAutoCreated(false);
	            p6.setMandatory(false);
	            p6.setMultiple(false);

	            // IsLinkedNode
	            PropertyDefinitionTemplate p7 = ntManager.createPropertyDefinitionTemplate();
	            p7.setName(CustomProperty.VC_LINKED_TO);
	            p7.setRequiredType(PropertyType.STRING);
	            p7.setAutoCreated(false);
	            p7.setMandatory(false);
	            p7.setMultiple(false);
	            
	            // itsNextVersions
	            PropertyDefinitionTemplate p8 = ntManager.createPropertyDefinitionTemplate();
	            p8.setName(CustomProperty.VC_NEXT_VERSIONS);
	            p8.setRequiredType(PropertyType.STRING);
	            p8.setAutoCreated(false);
	            p8.setMandatory(false);
	            p8.setMultiple(false);
	            
	            // versionOf
	            PropertyDefinitionTemplate p9 = ntManager.createPropertyDefinitionTemplate();
	            p9.setName(CustomProperty.VC_VERSION_OF);
	            p9.setRequiredType(PropertyType.STRING);
	            p9.setAutoCreated(false);
	            p9.setMandatory(false);
	            p9.setMultiple(false);
	            
	            // docTags
	            PropertyDefinitionTemplate p10 = ntManager.createPropertyDefinitionTemplate();
	            p10.setName(CustomProperty.VC_DOC_TAGS);
	            p10.setRequiredType(PropertyType.STRING);
	            p10.setAutoCreated(false);
	            p10.setMandatory(false);
	            p10.setMultiple(false);
	            
	            
	            PropertyDefinitionTemplate p11 = ntManager.createPropertyDefinitionTemplate();
	            p11.setName(CustomProperty.VC_SP_DOC_CONTENT_TYPE);
	            p11.setRequiredType(PropertyType.STRING);
	            p11.setAutoCreated(false);
	            p11.setMandatory(false);
	            p11.setMultiple(false);
	            
	            // fileSize
	            PropertyDefinitionTemplate p12 = ntManager.createPropertyDefinitionTemplate();
	            p12.setName(CustomProperty.VC_FILE_SIZE);
	            p12.setRequiredType(PropertyType.LONG);
	            p12.setAutoCreated(false);
	            p12.setMandatory(false);
	            p12.setMultiple(false);
	            
		         // archiveDate
	            PropertyDefinitionTemplate p13 = ntManager.createPropertyDefinitionTemplate();
	            p13.setName(CustomProperty.VC_ARCHIVE_DATE);
	            p13.setRequiredType(PropertyType.DATE);
	            p13.setAutoCreated(false);
	            p13.setMandatory(false);
	            p13.setMultiple(false);
	            
	            //is removed
	            PropertyDefinitionTemplate p14 = ntManager.createPropertyDefinitionTemplate();
	            p14.setName(CustomProperty.VC_IS_REMOVED);
	            p14.setRequiredType(PropertyType.STRING);
	            p14.setAutoCreated(false);
	            p14.setMandatory(false);
	            p14.setMultiple(false);
	            
	          //linked nodes
	            PropertyDefinitionTemplate p15 = ntManager.createPropertyDefinitionTemplate();
	            p15.setName(CustomProperty.VC_LINK_NODES);
	            p15.setRequiredType(PropertyType.STRING);
	            p15.setAutoCreated(false);
	            p15.setMandatory(false);
	            p15.setMultiple(false);
	            
//	            //is stubbed
//	            PropertyDefinitionTemplate p16 = ntManager.createPropertyDefinitionTemplate();
//	            p16.setName(CustomProperty.VC_IS_STUBBED);
//	            p16.setRequiredType(PropertyType.STRING);
//	            p16.setAutoCreated(false);
//	            p16.setMandatory(false);
//	            p16.setMultiple(false);
//	            
	         
	            List<PropertyDefinition> defs = fsResTemplate.getPropertyDefinitionTemplates();
	            defs.add(p1);
	            defs.add(p2);
	            defs.add(p3);
	            defs.add(p4);
	            defs.add(p5);
	            defs.add(p6);
	            defs.add(p7);
	            defs.add(p8);
	            defs.add(p9);
	            defs.add(p10);
	            defs.add(p11);
	            defs.add(p12);
	            defs.add(p13);
	            defs.add(p14);
	            defs.add(p15);
//	            defs.add(p16);

	            ntManager.registerNodeType(fsResTemplate, true);
	        }
	    }
	    
	    
	    private void displayNodeTypeDefinition(NodeTypeManager ntManager, String nodeTypeName) throws NoSuchNodeTypeException, RepositoryException {
	        NodeType nodeType = ntManager.getNodeType(nodeTypeName);

	        String name = nodeType.getName();
	        String primaryItem = nodeType.getPrimaryItemName();
	        log.debug("[" + name + "]");

	        NodeType[] superTypes = nodeType.getDeclaredSupertypes();
//	        log.debug("Super types of: " + nodeType.getName());
	        if (superTypes.length > 0) {
	            log.debug(" > ");
	        }
	        for (NodeType st : superTypes) {
	            name = st.getName();
	            log.debug(name + ", ");
	        }
	        log.debug("");
	        log.debug("primaryitem " + primaryItem);
	        log.debug("IsQueryable: " + nodeType.isQueryable());
	        log.debug("Attribute Types.");

	        for (PropertyDefinition p : nodeType.getPropertyDefinitions()) {
	            log.debug("p-Name: " + p.getName());
	            log.debug("p-searchable: " + p.isFullTextSearchable());
	        }
	    }

	 
	    private Node getCompanyNode(Session session, Node root) {
	        log.debug("getCompanyName: Started");
	        Node parent = null;
	        
	        SystemCodeService systemCodeService=serviceManager.getSystemCodeService();
			 SystemCode systemCode = systemCodeService.getSystemCodeByName("companyName");
			 String companyName="TestCompany";
			 if(systemCode!=null){
				  companyName = systemCode.getCodevalue();
			 }
	        
	        try {
	        	log.debug("getCompanyName: 1");
	            if (root.hasNode(companyName) || session.nodeExists("/" + companyName)) {
	                parent = root.getNode(companyName);
	                	                
	                log.debug("getCompanyName: 2");
	                log.debug("Parent Node:"+parent.getName());
	                log.debug("Parent Node Path:"+parent.getPath());
	                log.debug("**********11111111111***********");
	                
	                validateRepository(parent);
	               
	            } else {
	                parent = root.addNode(companyName, "nt:folder");
	                
	                log.debug(companyName + "not exist: creating it first time with name: " + parent.getName());
	                
	                commenceProductTrial();   
	            }
	        } catch (RepositoryException ex) {
	            log.error(companyName + " not accessible");
	            this.logError(ex);
	            parent = root;
	        }
	        log.debug("********222222222222222*********");
	        return parent;
	    }
	 
	    
	    private void validateRepository(Node companyNode){
	    	
	    	try{
		    	if (companyNode.hasProperty(Property.JCR_CREATED)) {
	                Property prop = companyNode.getProperty(Property.JCR_CREATED);
	                Calendar compNodeCal = prop.getDate();
	                Calendar currentDateCal = Calendar.getInstance();
	                currentDateCal.setTime(new Date());
	               
	                
	                if(compNodeCal.after(currentDateCal)){
	                	log.debug("Invalid Repository due to date tempring. " );
	                	System.out.println("InValid repository dates,possibly due to dates tempring");
	                }else{
	                	System.out.println("Valid repository dates");
	                }
	                	                
	                
	            }
	    	}catch(Exception ex){
	    		
	    	}
	    }
	 
	    private void commenceProductTrial(){
			try{
				SystemCodeService systemCodeService= serviceManager.getSystemCodeService();
				MailService mailService=serviceManager.getMailService();
				SystemCode systemCode= systemCodeService.getSystemCodeByName("sh_license");
	            if(systemCode!=null){	            	
	            	String licenseStr=systemCode.getCodevalue();
	            	PLVManager plvManager=null;
	            	try{
	            	 plvManager=PLVManager.getInstance();
	            	 License license=plvManager.readLicense(licenseStr);
	            	 license=plvManager.commenceTrial(license);
	            	 licenseStr=plvManager.generateLicense(license);	            	 
	            	 systemCode.setCodevalue(licenseStr);	
	            	 systemCodeService.update(systemCode);
	            	 if(license.getLicenseType().equalsIgnoreCase("Trial")){
	            		 String subject="ShareArchiver trial commenced today!";
	            		 String body="Thank you for trialing the ShareArchiver product, your trial commenced today and you have 14 days remaining till the trial period ends, <br/> if you have any questions, or require any support with the trial please do not hesitate to contact support@ShareArchiver.com";
	            		 mailService.sendMail(subject, body);            		 
	            	 }
	            	}catch(PLFormatException ex){
	            		ex.printStackTrace();
	            	}
	            }
				
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
			    
	 
	    private void logError(Exception ex) {
	        log.error("logError: " + ex.getMessage());

	        StringWriter sw = new StringWriter();
	        PrintWriter pw = new PrintWriter(sw);
	        ex.printStackTrace(pw);
	        log.error(sw.toString()); // stack trace as a string
	    }		
}
