/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.archivePolicy.dto;

/**
 *
 * @author faisal
 */
public class ExcludedPath  implements java.io.Serializable {

    /*
     * *************************************************************************
     * fields
     * *************************************************************************
     */
    //  static fields
    private static final long serialVersionUID = 105L;
    //properties
    private int id;



    /*
     * *************************************************************************
     * constructor
     * *************************************************************************
     */
    public ExcludedPath() {
    }

    /*
     * *************************************************************************
     * getter/setter
     * *************************************************************************
     */
    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
     public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    private String path;
    
}
