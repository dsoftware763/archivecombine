/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.archivePolicy.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author faisal nawaz
 */
public class Job implements java.io.Serializable {

    //  static fields
    private static final long serialVersionUID = 101L;

    //Fields
    private int id;
    private int active = 1;
    private String jobName;
    private String description;
    private Date execTimeStart;
    private String actionType;
    private String siteId;
    private String sitePath;
    private boolean processCurrentPublishedVersion = true;
    private boolean processLegacyPublishedVersion = true;
    private boolean processLegacyDraftVersion = true;
    private boolean processReadOnly = true;
    private boolean processArchive = true;
    private boolean processHidden = false;
    private boolean readyToExecute = true;
    private Policy policy;
    private Set<SPDocLib> spDocLibSet = new HashSet<SPDocLib>(0);
    private JobStatistics jobStatistics = new JobStatistics();
    private String activeActionType="ARCHIVEONLY";
    
    private String tags;
    
    private String currentStatus;

    /*
     * *************************************************************************
     * constructors
     * *************************************************************************
     */
    public Job() {
    }

    public Job(int id,
            int active, String jobName, String description, Date execTimeStart,
            String actionType, String siteId, String sitePath,
            boolean processCurrentPublishedVersion, boolean processLegacyPublishedVersion,
            boolean processLegacyDraftVersion, boolean processReadOnly,
            boolean processArchive, boolean processHidden, boolean readyToExecute,
            Policy policy,
            Set spDocLibSet
            ) {

        this.id = id;
        this.active = active;
        this.jobName = jobName;
        this.description = description;
        this.execTimeStart = execTimeStart;
        this.actionType = actionType;
        this.siteId = siteId;
        this.sitePath = sitePath;
        this.processCurrentPublishedVersion = processCurrentPublishedVersion;
        this.processLegacyPublishedVersion = processLegacyPublishedVersion;
        this.processLegacyDraftVersion = processLegacyDraftVersion;
        this.processReadOnly = processReadOnly;
        this.processArchive = processArchive;
        this.processHidden = processHidden;
        this.readyToExecute = readyToExecute;
        this.policy = policy;
        this.spDocLibSet = spDocLibSet;
       // this.jobStatistics = jobStatistics;
    }

    /*
     * *************************************************************************
     * getters
     * *************************************************************************
     */
    public Set<SPDocLib> getSpDocLibSet() {
        return spDocLibSet;
    }

    public JobStatistics getJobStatistics() {
        return jobStatistics;
    }

    public Policy getPolicy() {
        return policy;
    }

    public String getActionType() {
        return actionType;
    }

    public int getActive() {
        return active;
    }

    public String getDescription() {
        return description;
    }

    public Date getExecTimeStart() {
        return execTimeStart;
    }

    public int getId() {
        return id;
    }

    public String getJobName() {
        return jobName;
    }

    public boolean isProcessArchive() {
        return processArchive;
    }

    public boolean isProcessCurrentPublishedVersion() {
        return processCurrentPublishedVersion;
    }

    public boolean isProcessHidden() {
        return processHidden;
    }

    public boolean isProcessLegacyDraftVersion() {
        return processLegacyDraftVersion;
    }

    public boolean isProcessLegacyPublishedVersion() {
        return processLegacyPublishedVersion;
    }

    public boolean isProcessReadOnly() {
        return processReadOnly;
    }

    public boolean isReadyToExecute() {
        return readyToExecute;
    }

    public String getSiteId() {
        return siteId;
    }

    public String getSitePath() {
        return sitePath;
    }

    /*
     * *************************************************************************
     * setters
     * *************************************************************************
     */
    public void setSpDocLibSet(Set<SPDocLib> spDocLibSet) {
        this.spDocLibSet = spDocLibSet;
    }

//    public void setJobStatistics(JobStatistics jobStatistics) {
//        this.jobStatistics = jobStatistics;
//    }

    public void setPolicy(Policy policy) {
        this.policy = policy;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setExecTimeStart(Date execTimeStart) {
        this.execTimeStart = execTimeStart;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public void setProcessArchive(boolean processArchive) {
        this.processArchive = processArchive;
    }

    public void setProcessCurrentPublishedVersion(boolean processCurrentPublishedVersion) {
        this.processCurrentPublishedVersion = processCurrentPublishedVersion;
    }

    public void setProcessHidden(boolean processHidden) {
        this.processHidden = processHidden;
    }

    public void setProcessLegacyDraftVersion(boolean processLegacyDraftVersion) {
        this.processLegacyDraftVersion = processLegacyDraftVersion;
    }

    public void setProcessLegacyPublishedVersion(boolean processLegacyPublishedVersion) {
        this.processLegacyPublishedVersion = processLegacyPublishedVersion;
    }

    public void setProcessReadOnly(boolean processReadOnly) {
        this.processReadOnly = processReadOnly;
    }

    public void setReadyToExecute(boolean readyToExecute) {
        this.readyToExecute = readyToExecute;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public void setSitePath(String sitePath) {
        this.sitePath = sitePath;
    }

	public String getActiveActionType() {
		return activeActionType;
	}

	public void setActiveActionType(String activeActionType) {
		this.activeActionType = activeActionType;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}
}
