/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.archivePolicy.dto;

import com.virtualcode.agent.das.dataArchiver.TaskKPI;
import java.util.Date;

/**
 *
 * @author faisal nawaz
 */
public class JobStatistics implements java.io.Serializable {

    /*
     * *************************************************************************
     * field
     * *************************************************************************
     */


    //  static fields
    private static final long serialVersionUID = 102L;

    //
    private int id;
    private String executionID;
    private Date jobStartTime;
    private Date jobEndTime;
    //
    private long totalEvaluated=0L;
    private long totalDuplicated=0L;
    private long totalFreshArchived=0L;
    private long alreadyArchived=0L;
    private long totalStubbed=0L;
    //
    private long totalFailedArchiving=0L;
    private long totalFailedEvaluating=0L;
    private long totalFailedStubbing=0L;
    private long totalFailedDeduplication=0L;
    //
    private long skippedProcessFileSkippedReadOnly=0L;
    private long skippedProcessFileSkippedHiddenLected=0L;
    private long skippedProcessFileSkippedTooLarge=0L;
    private long skippedProcessFileSkippedTemporary=0L;
    private long skippedProcessFileSkippedSymbolLink=0L;
    private long skippedProcessFileSkippedIsDir=0L;
    private long skippedProcessFileSkippedMismatchAge=0L;
    private long skippedProcessFileSkippedMismatchLastAccessTime=0L;
    private long skippedProcessFileSkippedMismatchLastModifiedTime=0L;
    private long skippedProcessFileSkippedMismatchExtension=0L;
    
    private long archivedVolume=0L;

	/*
     * *************************************************************************
     * constructors
     * *************************************************************************
     */
    public JobStatistics() {
    }

    /*
     * *************************************************************************
     * getters
     * *************************************************************************
     */
    public String getExecutionID() {
        return executionID;
    }

    public int getId() {
        return id;
    }

    public Date getJobEndTime() {
        return jobEndTime;
    }

    public Date getJobStartTime() {
        return jobStartTime;
    }

    public long getSkippedProcessFileSkippedHiddenLected() {
        return skippedProcessFileSkippedHiddenLected;
    }

    public long getSkippedProcessFileSkippedIsDir() {
        return skippedProcessFileSkippedIsDir;
    }

    public long getSkippedProcessFileSkippedMismatchAge() {
        return skippedProcessFileSkippedMismatchAge;
    }

    public long getSkippedProcessFileSkippedMismatchExtension() {
        return skippedProcessFileSkippedMismatchExtension;
    }

    public long getSkippedProcessFileSkippedMismatchLastAccessTime() {
        return skippedProcessFileSkippedMismatchLastAccessTime;
    }

    public long getSkippedProcessFileSkippedMismatchLastModifiedTime() {
        return skippedProcessFileSkippedMismatchLastModifiedTime;
    }

    public long getSkippedProcessFileSkippedReadOnly() {
        return skippedProcessFileSkippedReadOnly;
    }

    public long getSkippedProcessFileSkippedSymbolLink() {
        return skippedProcessFileSkippedSymbolLink;
    }

    public long getSkippedProcessFileSkippedTemporary() {
        return skippedProcessFileSkippedTemporary;
    }

    public long getSkippedProcessFileSkippedTooLarge() {
        return skippedProcessFileSkippedTooLarge;
    }

    public long getTotalFreshArchived() {
        return totalFreshArchived;
    }

    public long getTotalDuplicated() {
        return totalDuplicated;
    }

    public long getTotalEvaluated() {
        return totalEvaluated;
    }

    public long getTotalFailedArchiving() {
        return totalFailedArchiving;
    }

    public long getTotalFailedDeduplication() {
        return totalFailedDeduplication;
    }

    public long getTotalFailedEvaluating() {
        return totalFailedEvaluating;
    }

    public long getTotalFailedStubbing() {
        return totalFailedStubbing;
    }

    public long getTotalStubbed() {
        return totalStubbed;
    }
    /*
     * *************************************************************************
     * setter
     * *************************************************************************
     */

    public void setExecutionID(String executionID) {
        this.executionID = executionID;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setJobEndTime(Date jobEndTime) {
        this.jobEndTime = jobEndTime;
    }

    public void setJobStartTime(Date jobStartTime) {
        this.jobStartTime = jobStartTime;
    }

    public void setSkippedProcessFileSkippedHiddenLected(long skippedProcessFileSkippedHiddenLected) {
        this.skippedProcessFileSkippedHiddenLected = skippedProcessFileSkippedHiddenLected;
    }

    public void setSkippedProcessFileSkippedIsDir(long skippedProcessFileSkippedIsDir) {
        this.skippedProcessFileSkippedIsDir = skippedProcessFileSkippedIsDir;
    }

    public void setSkippedProcessFileSkippedMismatchAge(long skippedProcessFileSkippedMismatchAge) {
        this.skippedProcessFileSkippedMismatchAge = skippedProcessFileSkippedMismatchAge;
    }

    public void setSkippedProcessFileSkippedMismatchExtension(long skippedProcessFileSkippedMismatchExtension) {
        this.skippedProcessFileSkippedMismatchExtension = skippedProcessFileSkippedMismatchExtension;
    }

    public void setSkippedProcessFileSkippedMismatchLastAccessTime(long skippedProcessFileSkippedMismatchLastAccessTime) {
        this.skippedProcessFileSkippedMismatchLastAccessTime = skippedProcessFileSkippedMismatchLastAccessTime;
    }

    public void setSkippedProcessFileSkippedMismatchLastModifiedTime(long skippedProcessFileSkippedMismatchLastModifiedTime) {
        this.skippedProcessFileSkippedMismatchLastModifiedTime = skippedProcessFileSkippedMismatchLastModifiedTime;
    }

    public void setSkippedProcessFileSkippedReadOnly(long skippedProcessFileSkippedReadOnly) {
        this.skippedProcessFileSkippedReadOnly = skippedProcessFileSkippedReadOnly;
    }

    public void setSkippedProcessFileSkippedSymbolLink(long skippedProcessFileSkippedSymbolLink) {
        this.skippedProcessFileSkippedSymbolLink = skippedProcessFileSkippedSymbolLink;
    }

    public void setSkippedProcessFileSkippedTemporary(long skippedProcessFileSkippedTemporary) {
        this.skippedProcessFileSkippedTemporary = skippedProcessFileSkippedTemporary;
    }

    public void setSkippedProcessFileSkippedTooLarge(long skippedProcessFileSkippedTooLarge) {
        this.skippedProcessFileSkippedTooLarge = skippedProcessFileSkippedTooLarge;
    }

    public void setTotalFreshArchived(long totalFreshArchived) {
        this.totalFreshArchived = totalFreshArchived;
    }

    public void setTotalDuplicated(long totalDuplicated) {
        this.totalDuplicated = totalDuplicated;
    }

    public void setTotalEvaluated(long totalEvaluated) {
        this.totalEvaluated = totalEvaluated;
    }

    public void setTotalFailedArchiving(long totalFailedArchiving) {
        this.totalFailedArchiving = totalFailedArchiving;
    }

    public void setTotalFailedDeduplication(long totalFailedDeduplication) {
        this.totalFailedDeduplication = totalFailedDeduplication;
    }

    public void setTotalFailedEvaluating(long totalFailedEvaluating) {
        this.totalFailedEvaluating = totalFailedEvaluating;
    }

    public void setTotalFailedStubbing(long totalFailedStubbing) {
        this.totalFailedStubbing = totalFailedStubbing;
    }

    public void setTotalStubbed(long totalStubbed) {
        this.totalStubbed = totalStubbed;
    }
    
    public long getAlreadyArchived() {
		return alreadyArchived;
	}

	public void setAlreadyArchived(long alreadyArchived) {
		this.alreadyArchived = alreadyArchived;
	}

	public long getArchivedVolume() {
    	if(archivedVolume > 0)
    		return Math.round(archivedVolume/(1024*1024));
    	else
    		return 0L;
	}

	public void setArchivedVolume(long archivedVolume) {
		this.archivedVolume = archivedVolume;
	}

    public void update(TaskKPI taskKPI) {
        totalEvaluated = totalEvaluated + 1;
        totalDuplicated = taskKPI.isDuplicate ? ++totalDuplicated : totalDuplicated;
        totalFreshArchived 	= ((!"NO".equals(taskKPI.isArchived) && !"AE".equals(taskKPI.isArchived)) || taskKPI.isExported) ? ++totalFreshArchived : totalFreshArchived;
        alreadyArchived		= ("AE".equals(taskKPI.isArchived)) ? ++alreadyArchived : alreadyArchived;
        totalStubbed = taskKPI.isStubbed ? ++totalStubbed : totalStubbed;

        totalFailedEvaluating = taskKPI.failEvaluation ? ++totalFailedEvaluating : totalFailedEvaluating;
        totalFailedArchiving = (taskKPI.failedArchiving || taskKPI.failedExporting) ? ++totalFailedArchiving : totalFailedArchiving;
        totalFailedDeduplication = taskKPI.failedDuplication ? ++totalFailedDeduplication : totalFailedDeduplication;
        totalFailedStubbing = taskKPI.failStubbing ? ++totalFailedStubbing : totalFailedStubbing;
        
        switch(taskKPI.evaluatorFlag){
            case FLAG_PROCESSFILE_SKIPPED_READ_ONLY:
                ++skippedProcessFileSkippedReadOnly;
                break;
            case FLAG_PROCESSFILE_SKIPPED_HIDDENLECTED:
                ++skippedProcessFileSkippedHiddenLected;
                break;
            case FLAG_PROCESSFILE_SKIPPED_SMALL_SIZE:
                ++skippedProcessFileSkippedTooLarge;
                break;
            case FLAG_PROCESSFILE_SKIPPED_TEMPORARY:
                ++skippedProcessFileSkippedTemporary;
                break;
            case FLAG_PROCESSFILE_SKIPPED_SYMBOLLINK:
                ++skippedProcessFileSkippedSymbolLink;
                break;
            case FLAG_PROCESSFILE_SKIPPED_ISDIR:
                ++skippedProcessFileSkippedIsDir;
                break;
            case FLAG_PROCESSFILE_SKIPPED_MISMATCH_AGE:
                ++skippedProcessFileSkippedMismatchAge;
                break;
            case FLAG_PROCESSFILE_SKIPPED_MISMATCH_LASTACCESSTIME:
                ++skippedProcessFileSkippedMismatchLastAccessTime;
                break;
            case FLAG_PROCESSFILE_SKIPPED_MISMATCH_LASTMODIFIEDTIME:
                ++skippedProcessFileSkippedMismatchLastModifiedTime;
                break;
            case FLAG_PROCESSFILE_SKIPPED_MISMATCH_EXTENSION:
                ++skippedProcessFileSkippedMismatchExtension;
                break;
        }

    }
}
