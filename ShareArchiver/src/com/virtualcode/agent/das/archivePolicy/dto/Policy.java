/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.archivePolicy.dto;

import com.virtualcode.agent.das.documentManagement.dto.DocumentType;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author faisal nawaz
 */
public class Policy implements java.io.Serializable{


    //  static fields
    private static final long serialVersionUID = 103L;

    //Fields
    private int id;
    private String policyName;
    private String description;
    private String documentAge;
    private String lastModifiedDate;
    private String lastAccessedDate;
    private String sizeLargerThan = "0";
    private int active = 1;
    private Set<DocumentType> documentTypeSet = new HashSet<DocumentType>(0);

    /*
     * *************************************************************************
     * constructors
     * *************************************************************************
     */
    public Policy() {
    }

    public Policy(int id,
            String policyName, String description,
            String documentAge, String lastModifiedDate,
            String lastAccessedDate, String sizeLargerThan, int active) {
        this.id = id;
        this.policyName = policyName;
        this.description = description;
        this.documentAge = documentAge;
        this.lastModifiedDate = lastModifiedDate;
        this.lastAccessedDate = lastAccessedDate;
        this.sizeLargerThan = sizeLargerThan;
        this.active = active;
    }

    /*
     * *************************************************************************
     * getters
     * *************************************************************************
     */
    public Set<DocumentType> getDocumentTypeSet() {
        return documentTypeSet;
    }

    public int getActive() {
        return active;
    }

    public String getDescription() {
        return description;
    }

    public String getDocumentAge() {
        return documentAge;
    }

    public int getId() {
        return id;
    }

    public String getLastAccessedDate() {
        return lastAccessedDate;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public String getPolicyName() {
        return policyName;
    }

    public String getSizeLargerThan() {
        return sizeLargerThan;
    }

    /*
     * *************************************************************************
     * setters
     * *************************************************************************
     */
    public void setDocumentTypeSet(Set<DocumentType> documentTypeSet) {
        this.documentTypeSet = documentTypeSet;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDocumentAge(String documentAge) {
        this.documentAge = documentAge;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLastAccessedDate(String lastAccessedDate) {
        this.lastAccessedDate = lastAccessedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }

    public void setSizeLargerThan(String sizeLargerThan) {
        this.sizeLargerThan = sizeLargerThan;
    }
}
