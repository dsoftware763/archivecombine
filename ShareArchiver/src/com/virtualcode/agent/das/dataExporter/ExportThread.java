/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataExporter;

import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.archivePolicy.dto.Job;

import org.apache.log4j.Logger;
/**
 *
 * @author YAwar
 */
public class ExportThread implements Runnable {
    
    private String executionID  =   null;
    private Job j   =   null;
    public ExportThread(Job j, String execID) {
        this.executionID =   execID;
        this.j= j;
    }
    
    @Override
    public void run() {
        Logger logger = LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_ERROR, this.executionID);

        try {
            if (j == null) {
                throw new Exception("Export Job is null, Ops");
            }

            ExportExecutors ee = new ExportExecutors();
            ee.startJob(j, this.executionID);
            ee = null;

        } catch (Exception ex) {

            logger.fatal("ExportStarter : " + ex);
            LoggingManager.printStackTrace(ex, logger);

        } finally {
            ////NO need to reset Bundles, bcoz these are being changing and involved only in the
            ////processing of Import/Archiving Jobs. So there reset in Export Job may raise
            ////some un-expected results bcoz of dual control
            ////Utility.resetUtilityResourceBundles();

            ////TEST COMMENT, to see affects...  
            ////LoggingManager.shutdownLogging(executionID);
            //System.gc();
        }
    }
}
