/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataExporter;


import com.virtualcode.agent.das.Executors.DataLayer;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.dataArchiver.JobStarter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
/**
 *
 * @author Abbas
 */
public class ExportStarter implements Runnable {
    
    private String AgentName    =   null;
    private String actionType   =   null;
    private static Logger logger    =   Logger.getLogger(ExportStarter.class);
    
    public static ArrayList<String> underProcessExpJobsList   =   new ArrayList<String>();
    private static List<Integer> interuptedJobIDsList  =   null;
    
    public ExportStarter(String AgentName) {
        this.AgentName  =   AgentName;
        this.actionType =   "EXPORT";
    }
    
    @Override
    public void run() {
        try {
            
            Thread.currentThread().sleep(14 * 1000);// 14 sec
            logger.info(AgentName + " waiting for Export Job...");
            
            while (true) {
                try {
                    
                    List<Job> jobsList  =   DataLayer.JobsbyAgent(AgentName, actionType);
                    if(jobsList!=null && !jobsList.isEmpty() && jobsList.size()>0) {
                        
                        logger.info(jobsList.size() + " new Export Jobs Found.");
                        
                        interuptedJobIDsList  =   DataLayer.interuptedJobs();
                        logger.info("Interupted number of Jobs: "+interuptedJobIDsList.size());
                        
                        for (Job j : jobsList) {
                            int jID = j.getId();
                            
                            logger.info("Export Job ID : " + jID);
                            if(underProcessExpJobsList.contains(jID+"")) {
                                logger.info("Already processing: "+jID);
                                
                            } else if(interuptedJobIDsList.contains(jID+"")) {
                                logger.warn("Job is Canceled / Interupted: "+jID);
                                
                            } else {
                                underProcessExpJobsList.add(jID+"");
                                String executionID = DataLayer.executionStarted(jID);
                                logger.info("Export Execution ID : " + executionID);

                                LoggingManager.configureLogger(jID, executionID,LoggingManager.EXP_PUBLIC_MSGS);
                                LoggingManager.configureLogger(jID, executionID,LoggingManager.EXP_ACTIVITY_DETAIL);
                                LoggingManager.configureLogger(jID, executionID,LoggingManager.EXP_ACTIVITY_ERROR);
                                LoggingManager.configureLogger(jID, executionID,LoggingManager.EXP_JOB_SUMMARY);
                                LoggingManager.configureLogger(jID, executionID,LoggingManager.EXP_TASK_SUMMARY);

                                ExportThread et =   new ExportThread(j, executionID);
                                Thread exp  =   new Thread(et);
                                exp.start();
                            }
                            logger.info("Seeking next export Job...");
                        }
                        
                        /*
                         * 
                         * 
                         */
                    } else {
                        logger.info("New Export Job Not Found");
                    }
                } catch(Exception ex) {//if WS is down at Server side, then dont' Terminate...
                    ex.getMessage();
                    ex.printStackTrace();
                    LoggingManager.printStackTrace(ex, logger);
                    
                } finally {
                    /*
                    fileEvaluator = null;
                    fileExporter = null;
                    statisticsCalculator = null;
                     */
                }
                Thread.currentThread().sleep(1000 * 45);//wait for 45sec
            }
            
        } catch (InterruptedException ex) {//if Thread.sleep is interupted
            ex.getMessage();
            ex.printStackTrace();
            LoggingManager.printStackTrace(ex, logger);
        }
    }
    
    public static boolean isInterupted(String execID) {
        
        String curJobID =   ExportExecutors.currentJobs.get(execID).getId()+"";
        boolean isInterupted =   false;
        
        synchronized(ExportStarter.class) {
            isInterupted =   ExportStarter.interuptedJobIDsList.contains(curJobID);
        }
        if(isInterupted)
            logger.warn("jobID("+curJobID+") is Interupted by User while executing: "+execID);
        
        return isInterupted;
    }
}