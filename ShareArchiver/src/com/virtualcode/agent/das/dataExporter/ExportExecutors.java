/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataExporter;

import com.virtualcode.agent.das.Executors.DataLayer;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.archivePolicy.dto.ExcludedPath;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.agent.das.fileSystems.FileServiceInterface;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.archivePolicy.dto.SPDocLib;
import com.virtualcode.agent.das.documentManagement.dto.DocumentType;
import com.virtualcode.agent.das.fileSystems.repo.RepoService;
import com.virtualcode.agent.das.threadsHandler.ExportTPEList;
import com.virtualcode.agent.das.utils.MyHashMap;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.log4j.Logger;

/**
 *
 * @author Abbas
 */
public class ExportExecutors {

    public static HashMap<String, Job> currentJobs = new HashMap<String, Job>();
    
    private static MyHashMap fileWalkedCtr    =   new MyHashMap();
    
    void startJob(Job job, String executionID) throws InterruptedException, Exception {
        Logger logger = LoggingManager.getLogger(LoggingManager.EXP_JOB_SUMMARY, executionID);
        Logger loginfo	=	LoggingManager.getLogger(LoggingManager.EXP_PUBLIC_MSGS, executionID);
        
        int errorCodeID = 0;
        int jID = -1;
        
        //initialize the counters with 0
        fileWalkedCtr.put(executionID, new AtomicLong(0));
        ExportEvaluater.resetExecutionCtr(executionID);
        FileExporter.resetExecutionCtr(executionID);
        TaskStatisticsExport.resetExecutionCtr(executionID);
        TaskStatisticsExport.resetFailedCtr(executionID);
        TaskStatisticsExport.resetComulativeSize(executionID);

        Job currentJob  =   null;
        try {          
            jID = (int) job.getId();

            currentJobs.put(executionID, job);
            currentJob  =   currentJobs.get(executionID);
            currentJob.getJobStatistics().setJobStartTime(new Date());
            
            String authString   = "username=" + Utility.GetProp("AgentLogin") + "&password=" + Utility.GetProp("AgentPassword") + "";
            String secureInfo   =   new String(Utility.encodetoBase64(authString.getBytes()));
            logger.info("Encoded Secure Info: "+secureInfo);
            
            if (executionID == null) {
            	loginfo.info("CODE[EE001]: System encountered an error to create job");
                throw new Exception("Execution ID is null");
            }
            
            
            // make/initiate the ThreadPools for current Execution of an Export Job
            ExportTPEList threadsPoolSet  =   new ExportTPEList(executionID, logger);
            
            logger.info("Job Info");
            logger.info("=======");
            logger.info("Execution ID " + executionID);
            logger.info("Job Name " + currentJob.getJobName());
            logger.info("Job Type " + currentJob.getActionType());
            Iterator iterator = job.getSpDocLibSet().iterator();
            while (iterator.hasNext()) {
                SPDocLib spDocLib = (SPDocLib) iterator.next();
                logger.info("\t Repo Path : " + spDocLib.getLibraryName());
                logger.info("\t Dest Path : " + spDocLib.getDestinationPath());
                logger.info("\t isRecursive: " + spDocLib.isRecursive());
                /* 
                 * Not requiret yet, bcoz Export Jobs are not supporting Exclude option
                System.out.println("job -> Paths -> excluded paths:");
                Iterator excludePathIterator = spDocLib.getExcludedPathSet().iterator();
                while (excludePathIterator.hasNext()) {
                    ExcludedPath excludePath = (ExcludedPath) excludePathIterator.next();
                    System.out.println("\t \t" + excludePath.getPath());
                }
                 */
            }

            logger.info("job -> policy name : " + job.getPolicy().getPolicyName());
            logger.info("job -> policy -> document-types: ");
            Iterator newIterator = job.getPolicy().getDocumentTypeSet().iterator();
            while (newIterator.hasNext()) {
                DocumentType documentType = (DocumentType) newIterator.next();
                logger.info("\t documentType name: " + documentType.getValue());
            }

            for (SPDocLib p : job.getSpDocLibSet()) {
                FileServiceInterface    fs  =   null;
                if("EXPORT".equalsIgnoreCase(currentJob.getActionType()) || "EXPORTSEARCHED".equalsIgnoreCase(currentJob.getActionType())) {//if this is Export Type Job
                    fs  =   new RepoService(secureInfo, p.getDestinationPath(), executionID);//"E:/TestFiles/Exported"
                } else {
                	loginfo.warn("CODE[EE002]: Invalid file path");
                    logger.error("Export module dont support the Job of type "+currentJob.getActionType());
                }
                
                //Prepare the list of Excluded sub-paths of current Path
                ArrayList<String> excPathList   =   new ArrayList();
                for(ExcludedPath ep : p.getExcludedPathSet()) {
                    excPathList.add(ep.getPath());
                }
                fs.walkFileTree(p.getLibraryName(), excPathList, p.isRecursive());
            }
            
            
            //Wait for shutting down the threads of current Execution
            threadsPoolSet.stopCurThreadPools();
            ////PROCEED FURTHER AFTER STOPPING THE THREAD POOL OF CUR JOB-EXEC
            
            
            //logger.info("Memory ... " + Runtime.getRuntime().totalMemory());
            logger.info("File Walked : " + fileWalkedCtr.get(executionID));
            fileWalkedCtr.put(executionID, new AtomicLong(0));//reset value to 0

            logger.info("");
            logger.info("Executors Report");
            logger.info("=================");
            logger.info("Repo Nodes Evaluted : " + ExportEvaluater.getExecutionCount(executionID));
            logger.info("Exported : " + FileExporter.getExecutionCount(executionID));
            logger.info("Failed : " + TaskStatisticsExport.getFailedCount(executionID));
            logger.info("Summary : " + TaskStatisticsExport.getExecutionCount(executionID));
            logger.info("Total Size : " + TaskStatisticsExport.getComulativeSize(executionID) + " bytes");

            currentJob.getJobStatistics().setJobEndTime(new Date());

            logger.info("Calling WS for executionCompleted with ID : " + executionID);

            boolean success = DataLayer.exportExecutionCompleted(jID, executionID, errorCodeID, currentJob.getJobStatistics());
            if (success) {
                logger.info("Successfull returned from execution Completed WS : " + jID);
            } else {
                throw new Exception("Oops.Error returned from execution Completed WS : " + jID);
            }

        } catch (Exception ex) {
            errorCodeID = 1;
            if (executionID != null) {

                if (currentJob == null) {
                	loginfo.error("CODE["+errorCodeID+"]: Fatal Error in export!");
                	
                    logger.info("Current Job is Null");
                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.exportExecutionCompleted(jID, executionID, errorCodeID, null);

                } else if (currentJob.getJobStatistics() == null) {
                	loginfo.error("CODE["+errorCodeID+"]: Fatal Error in export!");
                	
                    logger.info("currentJob.getJobStatistics() is Null");
                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.exportExecutionCompleted(jID, executionID, errorCodeID, null);
                } else {
                    currentJob.getJobStatistics().setJobEndTime(new Date());

                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.exportExecutionCompleted(jID, executionID, errorCodeID, currentJob.getJobStatistics());
                }
            }
            loginfo.error("CODE["+errorCodeID+"]: Fatal Error in execution of "+executionID);
            
            logger.fatal("ExecutionID: " + executionID);
            logger.fatal("JobID: " + jID);
            logger.fatal("ErrorID: " + errorCodeID);
            logger.fatal(ex.getMessage());
            LoggingManager.printStackTrace(ex, logger);
            LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_ERROR, executionID).error("Fatal : " + "\n" + ex.getMessage());

        } finally {
            currentJob = null;
            currentJobs.remove(executionID);
            ExportStarter.underProcessExpJobsList.remove(jID+"");
        }
    }
    
    public static void increment(String execID) {
        fileWalkedCtr.increment(execID);
    }
}
