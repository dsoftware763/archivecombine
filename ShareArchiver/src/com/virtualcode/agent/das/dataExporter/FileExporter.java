/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataExporter;

import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.fileSystems.repo.RepoTask;
import com.virtualcode.agent.das.threadsHandler.ExportTPEList;
import com.virtualcode.agent.das.utils.CommonJobUtils;
import com.virtualcode.agent.das.utils.MyHashMap;
import com.virtualcode.agent.das.utils.Utility;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.log4j.Logger;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;

/**
 *
 * @author Abbas
 */
public class FileExporter implements Runnable {

    public FileExporter(FileTaskInterface task) {
        this.task = task;
        logger  =   LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_DETAIL, this.task.getExecID());
        loginfo	=	LoggingManager.getLogger(LoggingManager.EXP_PUBLIC_MSGS, this.task.getExecID());
        
        if(MAX_NO_OF_TRIES==-1) {//set from *.properties file, if its the v first object of FileExporter
            String temp =   Utility.GetProp("MAX_NO_OF_TRIES");
            MAX_NO_OF_TRIES =   Integer.parseInt(temp);
        }
    }
    private static int MAX_NO_OF_TRIES =   -1;
    //private final static AtomicLong executionCount = new AtomicLong(0);
    private static MyHashMap executionCtr =   new MyHashMap();
    
    private FileTaskInterface task = null;
    //ThreadPoolExecutor nextExecutor = ExportStarter.statisticsCalculator;
    private Logger logger = null;
    private Logger loginfo	=	null;

    private boolean Process() {
        boolean output  =   false;
        String repoPath =   task.getSecureRepoPath();
        //"/SecureGet/f9b1fe21d2467eff1c9f32b5e0542ac83dc339e6d68a4b49f1864e492b4ac123818c98bf7a7910c570ccf7adb4f5e2684965f2bedd44330afdbb587d4f0fe17e8b60508bf0874e10f28e8a4c90392b1052d94bb6065228ce7517a2069d2fced56e38ccebe8ad485afe6c5280f1f152c9808bf63a5838a9593f08190627d1c1ea2fe6efbda17a3b0a0b72d93245247b44738f7bbbdf0f99d9";
        //SecureGet/f9b1fe21d2467eff1c9f32b5e0542ac83dc339e6d68a4b49f1864e492b4ac123818c98bf7a7910c555348f8bcb1b31cda47d73da488f4347fff482f2ad80303a0ca800a233c4a226987bb1fcf08bc630d25d218c8dcf2e5f93793c569b93e20336a3a5ea6c417780093a2d822e837d9126fcb178d5ae9c49beb85adeacfd87eb79b70813e50aa314c7bc8236b98474063097bfbf87716560752ee01cd7a2011f");
        repoPath        =   Utility.GetProp("DASUrl")+repoPath;
        logger.info(this.task.getUid().toString() + " Repo URL: " + repoPath);
        
        CommonJobUtils	cju	=	new CommonJobUtils(logger, loginfo, task);
        output	=	cju.fetchContentsForStub(repoPath, 
        		task.getDestPath(), 
        		((RepoTask)task).getDocument());//we know that, here the instance of Task is only of RepoTask...
        
        return output;
    }
    
    public static void resetExecutionCtr(String execID) {
        executionCtr.put(execID, new AtomicLong(0));
    }
    public static String getExecutionCount(String execID) {
        return executionCtr.get(execID);
    }

    @Override
    public void run() {

        try {

            task.getTaskKpi().exportStartTime = System.currentTimeMillis();
            String authString = "username=" + Utility.GetProp("AgentLogin") + "&password=" + Utility.GetProp("AgentPassword") + "";
            task.setEncodedString(new String(Utility.encodetoBase64(authString.getBytes())) );
             
            boolean successStatus =   false;
            int ctr =   1;
            while (!successStatus && ctr<=MAX_NO_OF_TRIES) {
            	if(ctr>1)
            		logger.info(this.task.getUid().toString() + " No of Try to Export: "+ctr);
                successStatus = Process();
                ctr++;
            }
            task.getTaskKpi().isExported    =   successStatus;
            task.getTaskKpi().exportEndTime = System.currentTimeMillis();
            /*
            if (task.getTaskKpi().isExported && FileExecutors.currentJob.getActionType().toLowerCase().trim().equals("stub")) {
                nextExecutor.execute(new FileStubber(task));
            } else 
             */
            if (task.getTaskKpi().isExported) {
                //ExportStarter.statisticsCalculator.execute(new TaskStatisticsExport(task));
                //ExportTPEList.getStatCalculator(task.getExecID()).execute(new TaskStatisticsExport(task));
                ExportTPEList.startStatCalculator(task.getExecID(), task);
                
            } else {
                throw new Exception("Unable to export the file");
            }

        } catch (Exception ex) {
            
        	loginfo.error("Failed to export file "+((task.getDestPath()!=null && task.getDestPath().contains("@"))?task.getDestPath().substring(task.getDestPath().lastIndexOf('@')):task.getDestPath()));
        	
            task.getTaskKpi().failedExporting = true;
            task.getTaskKpi().errorDetails = ex;
            //ExportStarter.statisticsCalculator.execute(new TaskStatisticsExport(task));
            //ExportTPEList.getStatCalculator(task.getExecID()).execute(new TaskStatisticsExport(task));
            ExportTPEList.startStatCalculator(task.getExecID(), task);
            
        } finally {
            //executionCount.addAndGet(1);
            executionCtr.increment(task.getExecID());
        }
    }
}