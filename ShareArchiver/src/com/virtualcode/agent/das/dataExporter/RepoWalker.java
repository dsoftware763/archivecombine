/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataExporter;

import com.virtualcode.agent.das.fileSystems.repo.RepoTask;
import com.virtualcode.agent.das.threadsHandler.ExportTPEList;
import com.virtualcode.cifs.Document;
//import com.virtualcode.agent.sa.export.Document;

/**
 *
 * @author Abbas
 */
public class RepoWalker {
    
    private String thisDocDestPath =   null;
    private String execID      =null;
    public RepoWalker(String thisDocDestPath, String execID) {
        this.thisDocDestPath   =   thisDocDestPath;
        this.execID =   execID;
    }
    
    public boolean visitFile(Document doc) {
        
        //ExportExecutors.fileWalkedCount += 1;
        ExportExecutors.increment(execID);//Update the Counter by 1, for current execID
        RepoTask task = new RepoTask(doc, this.thisDocDestPath, this.execID);
        //System.out.println("visiting RepoDoc: "+doc.getUrl());
        //ExportStarter.fileEvaluator.execute(new ExportEvaluater(task));
        //ExportTPEList.getFileEvaluator(this.execID).execute(new ExportEvaluater(task));
        ExportTPEList.startFileEvaluator(task.getExecID(), task);
        
        return true;
    }

    public void postVisitDirectory() {
    }

    public void preVisitDirectoryFailed() {
    }

    public void visitFileFailed() {
    }
}
