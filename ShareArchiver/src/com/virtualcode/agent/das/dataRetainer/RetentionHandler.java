package com.virtualcode.agent.das.dataRetainer;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.virtualcode.agent.das.Executors.DataLayer;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.dataArchiver.FileExecutors;
import com.virtualcode.agent.das.dataArchiver.JobStarter;
import com.virtualcode.agent.das.logging.LoggingManager;
/**
 *
 * @author Abbas
 */
public class RetentionHandler implements Runnable {
    
    private String AgentName    =   null;
    private static Logger logger   =   Logger.getLogger(RetentionHandler.class);
            
    public static ArrayList<String> underProcessJobsList   =   new ArrayList<String>();
    private static List<Integer> interuptedJobIDsList  =   null;
    
    public RetentionHandler(String AgentName) {
        this.AgentName  =   AgentName;
    }
    
    @Override
    public void run() {
        try {
        	
            Thread.currentThread().sleep(19 * 1000);// 19 sec
            logger.info(AgentName + " waiting for next Retension Job...");
            
            while (true) {
                try {
                    
                    List<Job> jobsList  =   DataLayer.JobsbyAgent(AgentName, "RETENSION");
                    if(jobsList!=null && !jobsList.isEmpty() && jobsList.size()>0) {
                        
                        logger.info(jobsList.size() + " new Retension Jobs Found.");
                        
                        interuptedJobIDsList  =   DataLayer.interuptedJobs();
                        logger.info("Interupted number of Jobs: "+interuptedJobIDsList.size());
                        
                        for (Job j : jobsList) {
                            int jID = j.getId();
                            
                            logger.info("Retension Job ID : " + jID +" for " +j.getActiveActionType());
                            if(underProcessJobsList.contains(jID+"")) {
                                logger.info("Already processing: "+jID);
                                
                            } else if(interuptedJobIDsList.contains(jID+"")) {
                                logger.warn("Job is Canceled / Interupted: "+jID);
                                
                            } else {
                                underProcessJobsList.add(jID+"");
                                String executionID = DataLayer.executionStarted(jID);
                                logger.info("Retension Execution ID : " + executionID);

                                LoggingManager.configureLogger(jID, executionID,LoggingManager.PUBLIC_MSGS);
                                LoggingManager.configureLogger(jID, executionID,LoggingManager.ACTIVITY_DETAIL);
                                //LoggingManager.configureLogger(jID, executionID,LoggingManager.ACTIVITY_ERROR);
                                LoggingManager.configureLogger(jID, executionID,LoggingManager.JOB_SUMMARY);
                                //LoggingManager.configureLogger(jID, executionID,LoggingManager.TASK_SUMMARY);

                                //if its to Mark only...
                                if("MARKONLY".equals(j.getActiveActionType())) {
	                                MarkForRemovalThread et =   new MarkForRemovalThread(j, executionID);
	                                Thread exp  =   new Thread(et);
	                                exp.start();
	                                
	                            } else if("REMOVENOW".equals(j.getActiveActionType())) {
	                            	
	                                RemovePermanentThread rmpt	=	new RemovePermanentThread(j, executionID);
	                                Thread exp	=	new Thread(rmpt);
	                                exp.start();
	                            }
                                
                            }
                            logger.info("Seeking next Retension Job...");
                        }

                    } else {
                        logger.info("New Retension Job Not Found");
                    }
                } catch(Exception ex) {//if WS is down at Server side, then dont' Terminate...
                    ex.getMessage();
                    ex.printStackTrace();
                    LoggingManager.printStackTrace(ex, logger);
                    
                } finally {
                }
                Thread.currentThread().sleep(1000 * 45);//wait for 45sec
            }
            
        } catch (InterruptedException ex) {//if Thread.sleep is interupted
            ex.getMessage();
            ex.printStackTrace();
            LoggingManager.printStackTrace(ex, logger);
        }
    }
    
    public static boolean isInterupted(String execID) {
        
    	Job job	=	MarkerExecutor.currentExecs.get(execID);//if running in MarkOnly Mode
    	if(job==null) {
    		job	=	RemoverExecutor.currentExecs.get(execID);//if running in Permanent removal mode
    	}
    	
        Integer curJobID =   job.getId();        
        boolean isInterupted =   false;
        
        synchronized(RetentionHandler.class) {
            isInterupted =   RetentionHandler.interuptedJobIDsList.contains(curJobID);
        }
        if(isInterupted)
            logger.warn("jobID("+curJobID+") is Interupted by User while executing: "+execID);
        
        return isInterupted;
    }
}
