/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataRetainer;

import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.virtualcode.agent.das.Executors.DataLayer;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.archivePolicy.dto.JobStatistics;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.repository.UploadDocumentService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.util.SpringApplicationContext;

/**
 *
 * @author Abbas
 */
public class RemoverExecutor {

    //save the job instance of current executions, to be used in Stats etc
    public static HashMap<String, Job> currentExecs = new HashMap<String, Job>();
        
    void startJob(Job job, String executionID) throws InterruptedException, Exception {
    	
        Logger logger = LoggingManager.getLogger(LoggingManager.JOB_SUMMARY, executionID);
        Logger loginfo	=	LoggingManager.getLogger(LoggingManager.PUBLIC_MSGS, executionID);
        
        int errorCodeID = 0;
        int jID = -1;
                
        Job currentJob  =   null;
        try {
            //Print the version of Agent, along with each Job
            //logger.info("Commercial Version ID = 1, Major Version ID = 1, Minor Version = 2");
            //logger.info("Commercial Version V 1.2.0, RD 16 Dec 2011");
            
            jID = (int) job.getId();

            currentExecs.put(executionID, job);
            currentJob = currentExecs.get(executionID);
            currentJob.getJobStatistics().setJobStartTime(new Date());
            
            if (executionID == null) {
                throw new Exception("Execution ID is null for removal");
            }
                        
            // make/initiate the ThreadPools for current Execution of an archive Job
          ///yawarz-ALIT///TPEList threadsPoolSet  =   new TPEList(executionID, logger);
            
            logger.info("Job Info");
            logger.info("=======");
            logger.info("Execution ID " + executionID);
            logger.info("Job Name " + currentJob.getJobName());
            logger.info("Job Type " + currentJob.getActionType());
            loginfo.info(currentJob.getJobName() + " started with ID="+executionID);
            
            logger.info("job -> policy name : " + job.getPolicy().getPolicyName());
            logger.info("job -> policy -> document-types: ");
                        
            String authString = "username=" + Utility.GetProp("AgentLogin") + "&password=" + Utility.GetProp("AgentPassword") + "";
            String encodedSecureInfo	=	new String(Utility.encodetoBase64(authString.getBytes()));

            ServiceManager serviceManager = (ServiceManager)SpringApplicationContext.getBean("serviceManager");
            UploadDocumentService uploadDocumentService	=	serviceManager.getUploadDocumentService();
            long removedCount	=	uploadDocumentService.removeAllDocuments(job.getSpDocLibSet(), encodedSecureInfo, executionID, LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL, executionID));

            logger.info("Memory ... " + Runtime.getRuntime().totalMemory());
                        
            logger.info("");
            logger.info("Executors Report");
            logger.info("=================");
          ///yawarz-ALIT///logger.info("CIFS Evaluted : " + CIFSEvaluater.getExecutionCount(executionID));
          ///yawarz-ALIT///logger.info("Archived : " + FileArchiver.getExecutionCount(executionID));
          ///yawarz-ALIT///logger.info("Stubbed : " + FileStubber.getExecutionCount(executionID));
          ///yawarz-ALIT///logger.info("Failed : " + TaskStatistic.getFailedCount(executionID));
          ///yawarz-ALIT///logger.info("Summary : " + TaskStatistic.getExecutionCount(executionID));
          ///yawarz-ALIT///logger.info("Total Size : " + TaskStatistic.getComulativeSize(executionID) + " bytes");

    		JobStatistics jobStatistics	=	currentJob.getJobStatistics();
    		jobStatistics.setTotalEvaluated(job.getSpDocLibSet().size());
    		jobStatistics.setTotalFreshArchived(removedCount);
    		jobStatistics.setTotalFailedArchiving(job.getSpDocLibSet().size()-removedCount);
    		jobStatistics.setTotalDuplicated(0);
    		jobStatistics.setAlreadyArchived(0);
    		jobStatistics.setTotalStubbed(0);
    		
    		jobStatistics.setTotalFailedEvaluating(0);
    		jobStatistics.setTotalFailedDeduplication(0);
    		jobStatistics.setTotalFailedStubbing(0);   
    		jobStatistics.setJobEndTime(new Date());

            logger.info("Calling WS for executionCompleted with ID : " + executionID);

            boolean success = DataLayer.executionCompleted(jID, executionID, errorCodeID, currentJob.getJobStatistics());
            if (success) {
                logger.info("Successfull returned from execution Completed WS : " + jID);
            } else {
                throw new Exception("Oops.Error returned from execution Completed WS : " + jID);
            }

        } catch (Exception ex) {
            errorCodeID = 1;
            if (executionID != null) {

                if (currentJob == null) {
                	loginfo.error("CODE["+errorCodeID+"]: Fatal Error!");
                	
                    logger.info("Current Job is Null");
                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.executionCompleted(jID, executionID, errorCodeID, null);

                } else if (currentJob.getJobStatistics() == null) {
                	loginfo.error("CODE["+errorCodeID+"]: Fatal Error!");
                	
                    logger.info("currentJob.getJobStatistics() is Null");
                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.executionCompleted(jID, executionID, errorCodeID, null);
                } else {
                    currentJob.getJobStatistics().setJobEndTime(new Date());
                    loginfo.error("CODE["+errorCodeID+"]: Fatal Error!");

                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.executionCompleted(jID, executionID, errorCodeID, currentJob.getJobStatistics());
                }
            }
            loginfo.error("CODE["+errorCodeID+"]: Fatal Error!");
            loginfo.error(ex.getMessage());
            
            logger.fatal("ExecutionID: " + executionID);
            logger.fatal("JobID: " + jID);
            logger.fatal("ErrorID: " + errorCodeID);
            
            logger.fatal(ex.getMessage());
            LoggingManager.printStackTrace(ex, logger);
            //LoggingManager.getLogger(LoggingManager.ACTIVITY_ERROR, executionID).error("Fatal : " + "\n" + ex.getMessage());
            LoggingManager.getLogger(LoggingManager.PUBLIC_MSGS, executionID).error("Fatal : " + "\n" + ex.getMessage());

        } finally {
            currentJob = null;
            currentExecs.remove(executionID);
            RetentionHandler.underProcessJobsList.remove(jID+"");//remove from the volatile list at Agent side...
        }
    }
}
