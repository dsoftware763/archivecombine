/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataRetainer;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.virtualcode.agent.das.Executors.DataLayer;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.archivePolicy.dto.JobStatistics;
import com.virtualcode.agent.das.archivePolicy.dto.Policy;
import com.virtualcode.agent.das.dataArchiver.JobStarter;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.repository.SearchService;
import com.virtualcode.services.JobService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.util.SpringApplicationContext;
import com.virtualcode.vo.JobSpDocLib;
import com.virtualcode.vo.SearchDocument;
import com.virtualcode.vo.SearchResult;

/**
 *
 * @author Abbas
 */
public class MarkerExecutor {

    //save the job instance of current executions, to be used in Stats etc
    public static HashMap<String, Job> currentExecs = new HashMap<String, Job>();
        
    void startJob(Job job, String executionID) throws InterruptedException, Exception {
        Logger logger = LoggingManager.getLogger(LoggingManager.JOB_SUMMARY, executionID);
        Logger loginfo	=	LoggingManager.getLogger(LoggingManager.PUBLIC_MSGS, executionID);
        
        int errorCodeID = 0;
        int jID = -1;
                
        Job currentJob  =   null;
        try {            
            jID = (int) job.getId();

            currentExecs.put(executionID, job);
            currentJob = currentExecs.get(executionID);
            currentJob.getJobStatistics().setJobStartTime(new Date());
            
            if (executionID == null) {
                throw new Exception("Execution ID is null");
            }
                        
            // make/initiate the ThreadPools for current Execution of an archive Job
          ///yawarz-ALIT///TPEList threadsPoolSet  =   new TPEList(executionID, logger);
            
            logger.info("Job Info");
            logger.info("=======");
            logger.info("Execution ID " + executionID);
            logger.info("Job Name " + currentJob.getJobName());
            logger.info("Job Type " + currentJob.getActiveActionType());
            loginfo.info(currentJob.getJobName() + " started with ID="+executionID);
            
            logger.info("job -> policy name : " + job.getPolicy().getPolicyName());
            logger.info("job -> policy -> document-types: ");
                        
            this.process(currentJob, executionID);            

            logger.info("Memory ... " + Runtime.getRuntime().totalMemory());
                        
            logger.info("");
            logger.info("Executors Report");
            logger.info("=================");
          ///yawarz-ALIT///logger.info("CIFS Evaluted : " + CIFSEvaluater.getExecutionCount(executionID));
          ///yawarz-ALIT///logger.info("Archived : " + FileArchiver.getExecutionCount(executionID));
          ///yawarz-ALIT///logger.info("Stubbed : " + FileStubber.getExecutionCount(executionID));
          ///yawarz-ALIT///logger.info("Failed : " + TaskStatistic.getFailedCount(executionID));
          ///yawarz-ALIT///logger.info("Summary : " + TaskStatistic.getExecutionCount(executionID));
          ///yawarz-ALIT///logger.info("Total Size : " + TaskStatistic.getComulativeSize(executionID) + " bytes");

            currentJob.getJobStatistics().setJobEndTime(new Date());
            logger.info("Calling WS for executionCompleted with ID : " + executionID);

            boolean success = DataLayer.executionCompleted(jID, executionID, errorCodeID, currentJob.getJobStatistics());
            if (success) {
                logger.info("Successfull returned from execution Completed WS : " + jID);
            } else {
                throw new Exception("Oops.Error returned from execution Completed WS : " + jID);
            }

        } catch (Exception ex) {
            errorCodeID = 1;
            if (executionID != null) {

                if (currentJob == null) {
                	loginfo.error("CODE["+errorCodeID+"]: Fatal Error!");
                	
                    logger.info("Current Job is Null");
                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.executionCompleted(jID, executionID, errorCodeID, null);

                } else if (currentJob.getJobStatistics() == null) {
                	loginfo.error("CODE["+errorCodeID+"]: Fatal Error!");
                	
                    logger.info("currentJob.getJobStatistics() is Null");
                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.executionCompleted(jID, executionID, errorCodeID, null);
                } else {
                    currentJob.getJobStatistics().setJobEndTime(new Date());
                    loginfo.error("CODE["+errorCodeID+"]: Fatal Error!");

                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.executionCompleted(jID, executionID, errorCodeID, currentJob.getJobStatistics());
                }
            }
            loginfo.error("CODE["+errorCodeID+"]: Fatal Error!");
            loginfo.error(ex.getMessage());
            
            logger.fatal("ExecutionID: " + executionID);
            logger.fatal("JobID: " + jID);
            logger.fatal("ErrorID: " + errorCodeID);
            
            logger.fatal(ex.getMessage());
            LoggingManager.printStackTrace(ex, logger);
            LoggingManager.getLogger(LoggingManager.PUBLIC_MSGS, executionID).error("Fatal : " + "\n" + ex.getMessage());

        } finally {
            currentJob = null;
            currentExecs.remove(executionID);
            RetentionHandler.underProcessJobsList.remove(jID+"");//remove from the volatile list at Agent side...
        }
    }
    
    private void process(Job curJob, String executionID) throws Exception {

    	Logger logger	= LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL, executionID);
    	
    	logger.info("Processing started for SearchService");
        ServiceManager serviceManager = (ServiceManager)SpringApplicationContext.getBean("serviceManager");
		SearchService searchService	=	serviceManager.getSearchService();
		
		//get the policy of current job
		Policy curPolicy	=	curJob.getPolicy();
		
		//prepare the search params on the basis of Policy
		int currentPage	=	1;
		int itemsPerPage	=	150;
		int totalPages	=	-1;
		
		//paging - to deal with huge searched results by utilizing paging
		int totalRecords=0;
		int totalRecordsSaved=0;
		for(currentPage=1; (currentPage<=totalPages || totalPages==-1); currentPage++) {
			
			//Terminate the current Thread, if CANCEL is requested by User
	    	if(RetentionHandler.isInterupted(executionID)) {
	    		logger.info("Interupted by User to Cancle the job");
	    		break;//break the iterations further
	    	}
	    	
			logger.info("Performing search of page: "+currentPage);
			
			String INPUT_DATE_FORMAT = "dd-MM-yyyy";
	        SimpleDateFormat inputDF = new SimpleDateFormat(INPUT_DATE_FORMAT);
	        
			String dAge = curPolicy.getDocumentAge();
            String laDate = curPolicy.getLastAccessedDate();
            String lmDate = curPolicy.getLastModifiedDate();
            String sz = curPolicy.getSizeLargerThan();
            Long docAge = ("0,0".equals(dAge))	?	System.currentTimeMillis()+86400000	:	Utility.calculateMilliSec(Utility.parseYear(dAge), Utility.parseMonth(dAge));
            Long docLM	= ("0,0".equals(lmDate))?	System.currentTimeMillis()+86400000	:	Utility.calculateMilliSec(Utility.parseYear(lmDate), Utility.parseMonth(lmDate));
            Long docLA	= ("0,0".equals(laDate))?	System.currentTimeMillis()+86400000	:	Utility.calculateMilliSec(Utility.parseYear(laDate), Utility.parseMonth(laDate));
            Long docSize = Utility.calculateSize(sz);
            
            String docAgeStr	=	inputDF.format(new Date(docAge));
            String docLMStr	=	inputDF.format(new Date(docLM));
            
            logger.info("DocAgeStr: "+docAgeStr);
            
    		//perform search, through searchServiceImpl...
    		SearchResult result	=	searchService.searchDocument("REMOVAL",
    				"*",//textPart, 
    				null,//authorName, 
    				null,//modifiedBy, 
    				null,//modifiedStartDate, 
    				docLMStr,//modifiedEndDate, 
    				null,//documentName, 
    				null,//securitySids, 
    				"Desc",//sortMode, 
    				"jcr:title",//sortOption, 
    				null,//tags, 
    				null,//targetFolder, 
    				null,//curPolicy.getDocumentTypeSet()+"",//contentType, 
    				currentPage, 
    				itemsPerPage, 
    				docSize,//fileSizeStart, 
    				null,//fileSSizeEnd, 
    				null,//archivedStartDate, 
    				docAgeStr//archivedEndDate
    				);
    		
    		//manage the totalPages and currentLoop's iterations (PAGING)
    		totalRecords	=	result.getTotalRecordsFound();
    		logger.info("Total records: "+totalRecords);
    		float t		=	((float)totalRecords)/itemsPerPage;
    		totalPages	=	(int)Math.ceil(t);	                		   
    		logger.info("totalPages: "+totalPages + " where devident: "+t);
    		
    		JobService jobService	=	serviceManager.getJobService();
    		com.virtualcode.vo.Job jobInstance=jobService.getJobById(curJob.getId());
			//jobInstance.setReadyToExecute(false);//no need to setReadyToExecute=false.. bcoz of lock in SearchHandler.java
			//logger.info("setReadyToExecute: "+jobInstance.getReadyToExecute());
   
    		Set<JobSpDocLib> allSpDocLibs	=	new HashSet<JobSpDocLib>();
    		List<SearchDocument> searchedDocs	=	result.getResults();
    		for(int i=0; searchedDocs!=null && i<searchedDocs.size(); i++) {
    			SearchDocument searchedDoc	=	searchedDocs.get(i);
	    		JobSpDocLib spDocLib	=	new JobSpDocLib();
	    		spDocLib.setJob(jobInstance);
	    		spDocLib.setName(searchedDoc.getDocumentName());// + " ["+searchedDoc.getDocumentSizeToDisplay()+"]"
	    		spDocLib.setSite(searchedDoc.getArchivedDate());
	    		spDocLib.setDrivePath(searchedDoc.getDocumentPath());
	    		spDocLib.setType("REPO");
	    		spDocLib.setDestinationPath(searchedDoc.getUid());
	    		spDocLib.setRecursive(true);//this is to set the mark=true by default
	    		
	    		logger.info("docName: "+ spDocLib.getName());
	    		allSpDocLibs.add(spDocLib);
	    		++totalRecordsSaved;
    		}
    		
    		logger.info("starting commit in DB");
    		jobInstance.setJobSpDocLibs(allSpDocLibs);
			jobService.saveJob(jobInstance);
			logger.info("save completed");
    		   
		}//end iteration for Pagining
		
		JobStatistics jobStatistics	=	curJob.getJobStatistics();
		jobStatistics.setTotalEvaluated(totalRecords);
		jobStatistics.setTotalFreshArchived(totalRecordsSaved);
		jobStatistics.setTotalFailedArchiving(totalRecords-totalRecordsSaved);
		jobStatistics.setTotalDuplicated(0);
		jobStatistics.setAlreadyArchived(0);
		jobStatistics.setTotalStubbed(0);
		
		jobStatistics.setTotalFailedEvaluating(0);
		jobStatistics.setTotalFailedDeduplication(0);
		jobStatistics.setTotalFailedStubbing(0);        
    }
}
