package com.virtualcode.agent.das.dataRetainer;

import org.apache.log4j.Logger;

import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.logging.LoggingManager;
/**
 *
 * @author YAwar
 */
public class MarkForRemovalThread implements Runnable {
    
    private String executionID  =   null;
    private Job curJob   =   null;
    private Logger logger	=	null;
    
    public MarkForRemovalThread(Job cJob, String execID) {
        this.executionID =   execID;
        this.curJob= cJob;
    }
    
    @Override
    public void run() {
		
        logger	= LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL, this.executionID);

        try {
            if (curJob == null) {
                throw new Exception("Retention Job is null for marking, Ops");
            }


            MarkerExecutor me = new MarkerExecutor();
            me.startJob(curJob, this.executionID);
            me = null;
            
        } catch (Exception ex) {

            logger.fatal("MarkForRemovalThread : " + ex);
            LoggingManager.printStackTrace(ex, logger);

        } finally {
        	//
        }
    }
}
