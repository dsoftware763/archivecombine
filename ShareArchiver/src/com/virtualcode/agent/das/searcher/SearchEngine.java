package com.virtualcode.agent.das.searcher;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TopFieldCollector;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.store.NIOFSDirectory;
import org.apache.lucene.util.Version;

import com.virtualcode.agent.das.dataIndexer.IndexerManager;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.util.StringUtils;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.SearchDocument;
import com.virtualcode.vo.SearchResult;

/**
 * 
 * @author Abbas
 */
public class SearchEngine {

	private IndexSearcher searcher;
	//private TopScoreDocCollector collector;
	private TopFieldCollector collector;
	private Analyzer standardAnalyzer;
	
	/** Creates a new instance of SearchEngine */
	public SearchEngine(String indexID, Integer maxResults, String sortField, int sortType, boolean sortOrder) throws IOException {
		
		//load and set the Base Path to save indexes
    	String indexLocation	=	Utility.GetProp("indexesBasePath");
    	if(!indexLocation.endsWith("/"))
    		indexLocation	=	indexLocation+"/";
    	indexLocation	=	indexLocation + indexID;
    	System.out.println("initializing SearchEngine at: "+indexLocation+" for "+indexID);
    	
		IndexReader ir = IndexReader.open(NIOFSDirectory.open(new File(indexLocation)), false);
		searcher = new IndexSearcher(ir);
		//collector = TopScoreDocCollector.create(maxResults, true);
		Sort sort	=	new Sort(new SortField(sortField, sortType, sortOrder));
		collector	=	TopFieldCollector.create(sort, maxResults, false, false, false, false);
		
		// Create instance of analyzer, which will be used to tokenize the input data
		standardAnalyzer = new StandardAnalyzer(Version.LUCENE_30);
	}

	public ScoreDoc[] performSingleSearch(String searchInField, String queryString) throws IOException, Exception {

		Query query = null;

		/*
    	if(searchInField.contains("date")) {
    		Date date = new SimpleDateFormat("dd/mm/yyyy", Locale.ENGLISH).parse(queryString);
    		
    		queryString	=	"";
    		query	=	new QueryParser(Version.LUCENE_40, searchInField, Indexer.analyzer).parse(queryString);
    	} else {*/
    		query = new WildcardQuery(new Term(searchInField, queryString));    		
    	//}
		
		searcher.search(query, collector);
		ScoreDoc[] hits = collector.topDocs().scoreDocs;
		
		// 4. display results
		System.out.println("Found " + hits.length + " hits.");
		for (int i = 0; i < hits.length; ++i) {
			int docId = hits[i].doc;
			Document d = searcher.doc(docId);
			System.out.println((i + 1) + ". " + d.get(IndexerManager.Constants.FILE_PATH) + " fileSize="+d.get(IndexerManager.Constants.FILE_SIZE)+" score="
					+ hits[i].score);
		}
		
		return hits;
	}
	
	public SearchResult performSearch(List<Query> queriesList, List<Occur> occurList, int currentPage, int itemsPerPage) throws ParseException, IOException{		

		List<SearchDocument> searchDocList = new ArrayList<SearchDocument>();
        SearchResult searchResult = new SearchResult();

        Query finalQuery =	null;
        if(queriesList!=null && queriesList.size()>0) {//if atleast, ONE filter is applied
        	BooleanQuery boolQuery = new BooleanQuery();
        	for(int i=0; i<queriesList.size(); i++) {
        		Query q=	(Query)queriesList.get(i);
        		if(occurList!=null && occurList.get(i)!=null)
        			boolQuery.add(q, occurList.get(i));//occurList can contain BooleanClause.Occur.MUST or BooleanClause.Occur.SHOULD etc
        		else
        			boolQuery.add(q, BooleanClause.Occur.MUST);
        	}
        	finalQuery	=	boolQuery;
        	//finalQuery	=	MultiFieldQueryParser.parse(Version.LUCENE_30, values, fields, flags, standardAnalyzer);
        	
        } else {
        	finalQuery = new WildcardQuery(new Term(IndexerManager.Constants.FILE_NAME, "*"));//match all indexed documents
        }

        //query = new WildcardQuery(new Term(IndexerManager.Constants.FILE_NAME, "*"));  
        System.out.println("query is: "+finalQuery.toString());
		searcher.search(finalQuery, collector);
		
		ScoreDoc[] hits = collector.topDocs().scoreDocs;
		
		if(hits==null) {
			return null;
		}
		
		int startLimit	=	(currentPage-1)*itemsPerPage;
        int endLimit	=	startLimit+itemsPerPage;
		
		// 4. display results
		System.out.println("Found " + hits.length + " hits.");
		DateFormat dFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a");//Date format
		long comulativeSize	=	0L;
		
		for (int ctr = 0; ctr < hits.length; ++ctr) {
			try {
				
				//get and sumup the fileSize in comulative size
				int docId = hits[ctr].doc;
				Document d = searcher.doc(docId);
				comulativeSize	+=	Long.parseLong(d.get(IndexerManager.Constants.FILE_SIZE));
				
				/* /DONT SKIP THE INITIAL PAGES RESULTS.. so that their size could be comuted 
				if(ctr==0) {//if its first row of found indexes
	           		 if(currentPage>1) {//if its not the first page
	           			 ctr	=	startLimit-1;
	           			 continue;
	           		 }
	           		 
	           	 } else if(ctr>=endLimit) {//iterate upto end of indexes, to count actual number of indexed results...
	           		 continue;
	           	 }*/
				if(ctr<startLimit || ctr>=endLimit) {//paging
					continue;
				}
				
				//System.out.println((ctr + 1) + ". " + d.get(IndexerManager.Constants.FILE_PATH) + " fileSize="+d.get(IndexerManager.Constants.FILE_SIZE)+" score="
				//		+ hits[ctr].score);
				//System.out.println(d.get(IndexerManager.Constants.DATE_ACCESS) + " dateModif="+d.get(IndexerManager.Constants.DATE_MODIFY)+" dateCreate="
				//		+ d.get(IndexerManager.Constants.DATE_CREATE));
				
				//SearchDocument searchDoc	=	populateVObySmbLoc(d, dFormat);
				SearchDocument searchDoc	=	populateVObyLucene(d, dFormat);
				
                searchDocList.add(searchDoc);
                
            } catch (Exception exp) {
                System.out.println("Serious Error to look into, File is skiped in Searh: " + exp.getMessage());
                exp.printStackTrace();
            }
		} 
		
		 searchResult.setSearchQuery("..SEARCH QUERY VALUE...");		 
         searchResult.setResults(searchDocList);
         searchResult.setTotalRecordsFound(new Integer(hits.length));
         searchResult.setComulativeSizeFound(comulativeSize);
         
         return searchResult;				
	}
	
	private SearchDocument populateVObyLucene(Document d, DateFormat dFormat) {
		String size	=	"";
		SearchDocument searchDoc	=	new SearchDocument();
		
		searchDoc.setDocumentName(d.get(IndexerManager.Constants.FILE_NAME));
		searchDoc.setCreatedDate(dFormat.format(new Date(Long.parseLong(d.get(IndexerManager.Constants.DATE_CREATE)))));
        searchDoc.setArchivedDate(dFormat.format(new Date(Long.parseLong(d.get(IndexerManager.Constants.DATE_CREATE)))));
        searchDoc.setLastModificationDate(dFormat.format(new Date(Long.parseLong(d.get(IndexerManager.Constants.DATE_MODIFY)))));	                
        searchDoc.setLastAccessDate(dFormat.format(new Date(Long.parseLong(d.get(IndexerManager.Constants.DATE_ACCESS)))));
        try{
            searchDoc.setMimeType(d.get(IndexerManager.Constants.FILE_NAME).substring(
            		d.get(IndexerManager.Constants.FILE_NAME).lastIndexOf(".")+1));
        }catch(NullPointerException ex){};
        size	=	d.get(IndexerManager.Constants.FILE_SIZE);
	
        String docPath	=	d.get(IndexerManager.Constants.FILE_PATH);
        if(docPath!=null && !docPath.isEmpty()) {
        	docPath	=	"\\\\" + (docPath.substring(docPath.indexOf("@")+1));
        	docPath	=	docPath.substring(0, docPath.lastIndexOf("/")+1).replace("/", "\\");
        }
        searchDoc.setDocumentPath(docPath);
        searchDoc.setUid(d.get(IndexerManager.Constants.FILE_PATH));//in case of Indexes, full qualified Path is the unique identification
		
        searchDoc.setDocumentSize(size);
        searchDoc.setDocumentSizeToDisplay(VCSUtil.getSizeToDisplay(size));
        searchDoc.setModifiedBy("Unknown");               
//        searchDoc.setDocumentUrl(d.get(IndexerManager.Constants.FILE_PATH)); // changed to add a download.jsp type URL
        String path = d.get(IndexerManager.Constants.FILE_PATH);
//        System.out.println("path: " + path);
        if(path!=null && path.trim().length()>0){
        	path = path.substring(path.indexOf("@")+1);
        }
        searchDoc.setSecurityAllowSids(d.get(IndexerManager.Constants.ALLOWED_SIDS));
        searchDoc.setShareAllowSids(d.get(IndexerManager.Constants.SHARE_ALLOWED_SIDS));
        searchDoc.setSecurityDenySids(d.get(IndexerManager.Constants.DENY_SIDS));
        searchDoc.setShareDenySids(d.get(IndexerManager.Constants.SHARE_DENY_SIDS));
        System.out.println("-------------file sids------------: " + searchDoc.getDocumentName());
        System.out.println("sec allow: "+searchDoc.getSecurityAllowSids());
        System.out.println("share allow: "+searchDoc.getShareAllowSids());
        System.out.println("sec deny: " + searchDoc.getSecurityDenySids());
        System.out.println("share deny: " + searchDoc.getShareDenySids());        
        System.out.println("-------------file sids------------");
        
//        System.out.println(d.get(IndexerManager.Constants.ALLOWED_SIDS));
//        searchDoc.setDocumentUrl("downloadfile.jsp?nodeID=" + StringUtils.encodeUrl(node.getPath().substring(1)) + "&secureInfo=" + secureInfo);
//         System.out.println("path 2: " + path);
        	
        searchDoc.setDocumentUrl("downloadfile.jsp?nodeID=nar-" + StringUtils.encodeUrlAdvanced(path));
        searchDoc.setArchive(false);
        
        return searchDoc;
	}
	
	private SearchDocument populateVObySmbLoc(Document d, DateFormat dFormat) throws SmbException {
		String size	=	"";
		SearchDocument searchDoc	=	new SearchDocument();
		SmbFile fileInstance	=	null;
		
		String docPath	=	d.get(IndexerManager.Constants.FILE_PATH);
		
		try {
			fileInstance	=	new SmbFile(docPath);
			if(!fileInstance.exists()) {
				fileInstance	=	null;
			}
		} catch (Exception e) {
			fileInstance	=	null;
			System.out.println("Err occured: "+e.getMessage());
			//e.printStackTrace();
		}
		
		if(fileInstance!=null && fileInstance.exists()) {	
			searchDoc.setDocumentName(fileInstance.getName());				
            searchDoc.setCreatedDate(dFormat.format(new Date(fileInstance.getDate())));
            searchDoc.setArchivedDate(dFormat.format(new Date(fileInstance.getLastAccess())));
            searchDoc.setLastModificationDate(dFormat.format(new Date(fileInstance.getLastModified())));
            try {
            	searchDoc.setMimeType(fileInstance.getName().substring(d.get(IndexerManager.Constants.FILE_NAME).lastIndexOf(".")));
            } catch (NullPointerException ne) {}
            size	=	""+fileInstance.length();
            
		} else {
			searchDoc.setDocumentName("NOT AVAILABLE");
			searchDoc.setCreatedDate(d.get(IndexerManager.Constants.DATE_CREATE));
            searchDoc.setArchivedDate(d.get(IndexerManager.Constants.DATE_CREATE));
            searchDoc.setLastModificationDate("Missing Info for "+
					((docPath.indexOf("@")>0)?"//"+docPath.substring(docPath.lastIndexOf("@")+1): docPath));	                
            try{
                searchDoc.setMimeType(d.get(IndexerManager.Constants.FILE_NAME).substring(
                		d.get(IndexerManager.Constants.FILE_NAME).lastIndexOf(".")));
            }catch(NullPointerException ex){};
            size	=	d.get(IndexerManager.Constants.FILE_SIZE);
		}
		
		searchDoc.setDocumentPath(docPath);
        searchDoc.setUid(d.get(IndexerManager.Constants.FILE_PATH));//in case of Indexes, full qualified Path is the unique identification
		
        searchDoc.setDocumentSize(size);
        searchDoc.setDocumentSizeToDisplay(VCSUtil.getSizeToDisplay(size));
        searchDoc.setModifiedBy("Unknown");               
        searchDoc.setDocumentUrl(d.get(IndexerManager.Constants.FILE_PATH));
        
        return searchDoc;
	}
	
	/*
	public SearchResult performSearch(String[] fields, String[] values, BooleanClause.Occur[] flags, int currentPage, int itemsPerPage) throws ParseException, IOException{		

		List<SearchDocument> searchDocList = new ArrayList<SearchDocument>();
        SearchResult searchResult = new SearchResult();

        Query query	=	null;
        if(fields!=null && values!=null && flags!=null &&
        		fields.length>0 && fields.length==values.length && values.length==flags.length) {
        	
        	query	=	MultiFieldQueryParser.parse(Version.LUCENE_30, values, fields, flags, standardAnalyzer);
        	
        } else {
        	query = new WildcardQuery(new Term(IndexerManager.Constants.FILE_NAME, "*"));//match all indexed documents
        }
        //query = new WildcardQuery(new Term(IndexerManager.Constants.FILE_NAME, "*"));  
        System.out.println("query is: "+query.toString());
		searcher.search(query, collector);
		
		ScoreDoc[] hits = collector.topDocs().scoreDocs;
		
		if(hits==null) {
			return null;
		}
		
		int startLimit	=	(currentPage-1)*itemsPerPage;
        int endLimit	=	startLimit+itemsPerPage;
		
		// 4. display results
		System.out.println("Found " + hits.length + " hits.");
		String size	=	"";
		String sizeToDisplay	=	"";
		SearchDocument searchDoc	=	null;
		for (int ctr = 0; ctr < hits.length; ++ctr) {
			try {
				if(ctr==0) {//if its first row of found indexes
	           		 if(currentPage>1) {//if its not the first page
	           			 ////rowIterator.skip(startLimit);
	           			 ctr	=	startLimit-1;
	           			 //System.out.println("skip "+startLimit+" and ctr becomes "+ctr);
	           			 continue;
	           		 }
	           		 
	           	 } else if(ctr>=endLimit) {//iterate upto end of indexes, to count actual number of indexed results...
	           		 ///rowIterator.nextRow();
	           		 //System.out.println("skipping ctr="+ctr);
	           		 continue;
	           	 }
					
				int docId = hits[ctr].doc;
				Document d = searcher.doc(docId);
				System.out.println((ctr + 1) + ". " + d.get(IndexerManager.Constants.FILE_PATH) + " fileSize="+d.get(IndexerManager.Constants.FILE_SIZE)+" score="
						+ hits[ctr].score);
				
				if (d.get(IndexerManager.Constants.FILE_SIZE)!=null) {
                    double length = Double.parseDouble(d.get(IndexerManager.Constants.FILE_SIZE));
                    size = String.valueOf(Math.round(Math.ceil(length / 1000d))) ;
                    DecimalFormat df = new DecimalFormat("#.##");
                    
                    length = length / 1000d;
                    sizeToDisplay = df.format(length)+" KB";
                    //convert to MBs
                    if(length>1024d){
                   	 length = length / 1024d;
                   	 sizeToDisplay = df.format(length)+" MB";
                    }
                  //convert to GBs
                    if(length>1024d){
                   	 length = length / 1024d;
                   	 sizeToDisplay = df.format(length)+" GB";
                    }
                }
				
				searchDoc = new SearchDocument();
                searchDoc.setDocumentName(d.get(IndexerManager.Constants.FILE_NAME));
                searchDoc.setDocumentPath(d.get(IndexerManager.Constants.FILE_PATH));
                searchDoc.setDocumentSize(size);
                searchDoc.setDocumentSizeToDisplay(sizeToDisplay);
                searchDoc.setUid(d.get(IndexerManager.Constants.FILE_PATH));//in case of Indexes, full qualified Path is the unique identification
                searchDoc.setCreatedDate(d.get(IndexerManager.Constants.DATE_CREATE));
                searchDoc.setArchivedDate(d.get(IndexerManager.Constants.DATE_CREATE));
                
                try{
	                searchDoc.setMimeType(d.get(IndexerManager.Constants.FILE_NAME).substring(
	                		d.get(IndexerManager.Constants.FILE_NAME).lastIndexOf(".")));
                }catch(Exception ex){};
                searchDoc.setLastModificationDate(d.get(IndexerManager.Constants.DATE_MODIFY));
                searchDoc.setModifiedBy("Unknown");               
                searchDoc.setDocumentUrl(d.get(IndexerManager.Constants.FILE_PATH));

                searchDocList.add(searchDoc);
                
            } catch (Exception exp) {
                System.out.println("Serious Error to look into, File is skiped in Searh: " + exp.getMessage());
                exp.printStackTrace();
            }
		} 
		
		 searchResult.setSearchQuery("..SEARCH QUERY VALUE...");		 
         searchResult.setResults(searchDocList);
         searchResult.setTotalRecordsFound(new Integer(hits.length));
         
         return searchResult;				
	}*/
	
	/*
	public ShareAnalysisStats calculateStats() throws IOException, Exception {

		BooleanQuery bQuery	=	new BooleanQuery();
		
		TermQuery term1	= new TermQuery(new Term(IndexerManager.Constants.FILE_NAME, "*.url"));
		bQuery.add(term1, BooleanClause.Occur.MUST);
		
		TermQuery term1	= new TermQuery(new Term(IndexerManager.Constants.FILE_NAME, "*.url"));
		bQuery.add(term1, BooleanClause.Occur.MUST);
		
		searcher.search(query, collector);
		ScoreDoc[] hits = collector.topDocs().scoreDocs;
		
		System.out.println("Found " + hits.length + " hits.");
		long totalVol	=	0;
		for (int i = 0; i < hits.length; ++i) {
			int docId = hits[i].doc;
			Document d = searcher.doc(docId);
			System.out.println((i + 1) + ". " + d.get(IndexerManager.Constants.FILE_PATH) + " fileSize="+d.get(IndexerManager.Constants.FILE_SIZE)+" score="
					+ hits[i].score);			
		}
		
		ShareAnalysisStats saStats	=	new ShareAnalysisStats();
		saStats.setTotalVolume((long)hits.length);
		
		return saStats;
	}*/
}
