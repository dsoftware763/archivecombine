package com.virtualcode.agent.das.searcher;

import org.apache.lucene.search.SortField;

import com.virtualcode.agent.das.dataIndexer.IndexerManager;


public class TestLucene {
	
	public static void main(String args[]) throws Exception {
		
	    String qry				=	"*to*";
	    String searchIn			=	"fileName";
	    
	    int maxResults	=	100;
	    
	    luceneSearcher("6", qry, searchIn, maxResults);
	}
	
	public static void luceneSearcher(String indexID, String qry, String searchIn, int maxResults) throws Exception {

		// =========================================================
		// Now search
		// =========================================================
		SearchEngine searchEngine	=	new SearchEngine(indexID, maxResults, IndexerManager.Constants.FILE_SIZE, SortField.LONG, true);
		
		//while (!qry.equalsIgnoreCase("q")) {
			try {
				//if (qry.equalsIgnoreCase("q")) {
				//	break;
				//}
				
				long startTime	=	System.currentTimeMillis();
				System.out.println("Search Started at "+ startTime);
				
				//perform search and print results
				searchEngine.performSingleSearch(searchIn, qry);
				
				System.out.println("Searched in "+ (System.currentTimeMillis()-startTime) + " mSec");

			} catch (Exception e) {
				System.out.println("Error searching " + qry + " : "
						+ e.getMessage());
				e.printStackTrace();
			}
		//}
	}
}
