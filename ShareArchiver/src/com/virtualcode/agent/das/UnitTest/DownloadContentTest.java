/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.UnitTest;

import com.virtualcode.controller.DataExportModule;
//import com.virtualcode.agent.das.utils.Utility;
//import com.virtualcode.agent.sa.export.DataExport;
//import com.virtualcode.agent.sa.export.DataExportService;
//import com.virtualcode.agent.sa.export.Document;
import com.virtualcode.cifs.Document;
//import com.virtualcode.ws.DataExport;
import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
//import java.util.Map;
//import javax.xml.ws.BindingProvider;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;
/**
 *
 * @author se7
 */
public class DownloadContentTest {
    public static void mainInCIFS(String[] args) throws Exception {
        // TODO code application logic here
        
        String secureInfo   =   "dXNlcm5hbWU9c2hhcmVhcmNoaXZlciZwYXNzd29yZD1zaGFyZWFyY2hpdmVy";
        List<Document>  docList =   getAllChildren("/TestCompany/FS/E/TestFiles/testLarge", secureInfo);
        
        for(int i=0; i<docList.size(); i++) {
            Document doc    =   docList.get(i);
            String fileURL  =   doc.getSecureURL();
            
            try {
                URL                url; 
                URLConnection      urlConn; 
                DataInputStream    dis;

                //"/SecureGet/f9b1fe21d2467eff1c9f32b5e0542ac83dc339e6d68a4b49f1864e492b4ac123818c98bf7a7910c570ccf7adb4f5e2684965f2bedd44330afdbb587d4f0fe17e8b60508bf0874e10f28e8a4c90392b1052d94bb6065228ce7517a2069d2fced56e38ccebe8ad485afe6c5280f1f152c9808bf63a5838a9593f08190627d1c1ea2fe6efbda17a3b0a0b72d93245247b44738f7bbbdf0f99d9";
                fileURL =   "http://localhost:8080/archive"+fileURL;
                url = new URL(fileURL); //SecureGet/f9b1fe21d2467eff1c9f32b5e0542ac83dc339e6d68a4b49f1864e492b4ac123818c98bf7a7910c555348f8bcb1b31cda47d73da488f4347fff482f2ad80303a0ca800a233c4a226987bb1fcf08bc630d25d218c8dcf2e5f93793c569b93e20336a3a5ea6c417780093a2d822e837d9126fcb178d5ae9c49beb85adeacfd87eb79b70813e50aa314c7bc8236b98474063097bfbf87716560752ee01cd7a2011f");
                System.out.println("final URL: " + fileURL);

                // Note:  a more portable URL: 
                //url = new URL(getCodeBase().toString() + "/ToDoList/ToDoList.txt");

                urlConn = url.openConnection(); 
                urlConn.setDoInput(true); 
                urlConn.setUseCaches(false);

                FileOutputStream fos=new FileOutputStream("E:\\TestFiles/Exported/"+doc.getTitle());
                dis = new DataInputStream(urlConn.getInputStream());
                /*
                int oneChar;
                while ((oneChar=dis.read()) != -1) {
                    fos.write(oneChar);
                }*/
                
                // Now copy bytes from the URL to the output stream
                byte[] buffer = new byte[4096];
                int bytes_read;

                while((bytes_read = dis.read(buffer)) != -1)
                    fos.write(buffer, 0, bytes_read);
                
                dis.close(); 
                fos.close();
            } catch (MalformedURLException mue) {
            } catch (IOException ioe) {
            }
        }
    }
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        
        String secureInfo   =   "dXNlcm5hbWU9c2hhcmVhcmNoaXZlciZwYXNzd29yZD1zaGFyZWFyY2hpdmVy";
        List<Document>  docList =   getAllChildren("/TestCompany/FS/E/TestFiles", secureInfo);
        
        for(int i=0; i<docList.size(); i++) {
            Document doc    =   docList.get(i);
            String fileURL  =   doc.getSecureURL();
            
            try {
                URL                url; 
                URLConnection      urlConn; 
                DataInputStream    dis;

                //"/SecureGet/f9b1fe21d2467eff1c9f32b5e0542ac83dc339e6d68a4b49f1864e492b4ac123818c98bf7a7910c570ccf7adb4f5e2684965f2bedd44330afdbb587d4f0fe17e8b60508bf0874e10f28e8a4c90392b1052d94bb6065228ce7517a2069d2fced56e38ccebe8ad485afe6c5280f1f152c9808bf63a5838a9593f08190627d1c1ea2fe6efbda17a3b0a0b72d93245247b44738f7bbbdf0f99d9";
                fileURL =   "http://localhost:8080/archive"+fileURL;
                String destPath =   "smb://network.vcsl;se7:s7*@vc7.network.vcsl/adTariz/Exported/";
                url = new URL(fileURL); //SecureGet/f9b1fe21d2467eff1c9f32b5e0542ac83dc339e6d68a4b49f1864e492b4ac123818c98bf7a7910c555348f8bcb1b31cda47d73da488f4347fff482f2ad80303a0ca800a233c4a226987bb1fcf08bc630d25d218c8dcf2e5f93793c569b93e20336a3a5ea6c417780093a2d822e837d9126fcb178d5ae9c49beb85adeacfd87eb79b70813e50aa314c7bc8236b98474063097bfbf87716560752ee01cd7a2011f");
                System.out.println("final URL: " + fileURL);

                // Note:  a more portable URL: 
                //url = new URL(getCodeBase().toString() + "/ToDoList/ToDoList.txt");

                urlConn = url.openConnection(); 
                urlConn.setDoInput(true); 
                urlConn.setUseCaches(false);

                SmbFile file   =   new SmbFile(destPath);
                if(!file.exists())
                    file.mkdirs();
                    
                System.out.println("Dest Path: " + destPath + doc.getTitle());
                SmbFileOutputStream fos=new SmbFileOutputStream(destPath+doc.getTitle());
                dis = new DataInputStream(urlConn.getInputStream());
                /*
                int oneChar;
                while ((oneChar=dis.read()) != -1) {
                    fos.write(oneChar);
                }*/
                
                // Now copy bytes from the URL to the output stream
                byte[] buffer = new byte[4096];
                int bytes_read;

                while((bytes_read = dis.read(buffer)) != -1)
                    fos.write(buffer, 0, bytes_read);
                
                dis.close(); 
                fos.close();
            } catch (MalformedURLException mue) {
            } catch (IOException ioe) {
            }
        }
    }
    /*
    private static void saveInNTFS(List<Document>  docList, String secureInfo) throws Exception {
        
        for(int i=1; i<docList.size(); i++) {
            Document doc    =   docList.get(i);
            
            System.out.println("visiting " + doc.getUrl());
            
            if(!doc.isFolderYN()) {
                System.out.println ("            starting: " + doc.getUrl());
                DataHandler dh = downloadContent(doc.getUrl(), secureInfo);
                StreamingDataHandler sdh = (StreamingDataHandler)dh;
                File file   =   null;
                try{
                    String destPath =   "E:\\TestFiles\\Exported\\"+doc.getTitle()+"";
                    System.out.println("saving at "+destPath);
                    file = new File(destPath);
                    sdh.moveTo(file);
                }catch(Exception ex){
                    System.out.println ("        exception: " + new Date());
                    ex.printStackTrace();
                }finally {
                    try {
                        sdh.close();
                        file    =   null;
                    } catch (IOException ie) {
                        
                    }
                }
                System.out.println ("            end time: " + new Date());
            } else {
                System.out.println("             not a file");
            }
        }
    }

    private static DataHandler downloadContent(java.lang.String path, java.lang.String layers) throws Exception {
        com.virtualcode.agent.sa.export.DataExportService service = new com.virtualcode.agent.sa.export.DataExportService();
        com.virtualcode.agent.sa.export.DataExport port = service.getDataExport();
        
        
        Map ctxt = ((BindingProvider) port).getRequestContext();
        ctxt.put(JAXWSProperties.HTTP_CLIENT_STREAMING_CHUNK_SIZE, 8192);
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,Utility.GetProp("DASUrl") + "/DataExport?wsdl");
        
        return port.downloadContent(path, layers);
    }
     * 
     */

    private static List<Document> getAllChildren(String parent, String layers) throws Exception {
        //DataExportService service = new DataExportService();
        //DataExport port = service.getDataExport();
        
        //Map ctxt = ((BindingProvider) port).getRequestContext();
        //ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,Utility.GetProp("DASUrl") + "/DataExport?wsdl");
        
        DataExportModule    port    =   new DataExportModule();
        return port.getAllChildren(parent, layers);
    }
}
