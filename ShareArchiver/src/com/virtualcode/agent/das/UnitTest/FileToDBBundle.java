/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.UnitTest;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author YAwar
 */
public class FileToDBBundle {
    public static void main(String args[]) {
        
        try {
            String type =   "ZuesAgent";
            FileInputStream is = new FileInputStream("D:/Sharearchiver/AbbaszWorkspace/"+type+".properties");
            ResourceBundle bdl = new PropertyResourceBundle(is);
            
            String query    =   "INSERT INTO system_code (codename, codevalue, codetype) VALUES ";
            Set<String> keys    =   bdl.keySet();
            Iterator<String> it =   keys.iterator();
            
            while(it.hasNext()) {
                
                String key      =   it.next();
                String value    =   bdl.getString(key).trim();
                
                query   += " ('"+key+"', '"+value+"', '"+type+"'),";
            }
            System.out.println(query);
            
            is.close();
            bdl = null;
            is = null;            

        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(FileToDBBundle.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
