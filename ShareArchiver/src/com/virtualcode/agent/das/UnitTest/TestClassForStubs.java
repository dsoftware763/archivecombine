/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.UnitTest;

import com.virtualcode.agent.das.fileSystems.cifs.CIFSFile;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSTask;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;

/**
 *
 * @author YAwar
 */
public class TestClassForStubs {
    public static void main(String args[]) throws Exception {
        String pathStr =   "smb://network.vcsl;se7:s7*@vc7.network.vcsl/adTariz/MergeUtility.zip";
        
        CIFSFile    cFile   =   new CIFSFile(pathStr);
        CIFSTask    cTask   =   new CIFSTask(cFile, "0L");
        
        //cTask.createInternetShortcut(pathStr, "http://www.etaxpk.com");
        
        pathStr =   "D:/Sharearchiver/TestFiles/myLog2.log";
        Path    path        =   FileSystems.getDefault().getPath(pathStr);
        //NTFSTask    nTask   =   new NTFSTask(path, Files.readAttributes(path, BasicFileAttributes.class));
        //nTask.createInternetShortcut(pathStr, "TEST");
        
        System.exit(0);
    }
}
