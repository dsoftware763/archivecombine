/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.UnitTest;

import com.virtualcode.agent.das.fileSystems.cifs.CIFSFile;
import com.virtualcode.agent.das.utils.Utility;
import java.io.File;
import java.net.MalformedURLException;
import java.util.LinkedList;
import java.util.StringTokenizer;
import jcifs.smb.ACE;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;

/**
 *
 * @author se7
 */
public class TestClassForCIFS {
    
    public static void main(String args[]) throws Exception {
        String path =   "smb://mazhar:newbuild@10.1.165.15:/mazhar/testfiles/Copy of Roadmap/";
        //path    =   "smb://vcsl95;administrator:cryo@fsln.virtualcode.co.uk/vc_share/Technical/";
        //path    =   "smb://vcsl95.local;administrator:cryo@10.1.165.217:445/vc_share/Technical/";
        //path    =    "smb://vcsl95.local;administrator:cryo@10.1.165.217/share/";
        //path    =   "smb://vcsl95.local;administrator:cryo@10.1.165.217/share/Agent212";
        //path      =     "smb://vcsl95.local;mazhar:password@snapserver.virtualcode.co.uk/";
        //path       =    "smb://vcsl95.local;mazhar:password@snapserver.virtualcode.co.uk/secgrpA/HP P3005 Laserjet/";
        //path        =   "smb://Bukhari:password@yawar-pc/temp/";
        path        =   "smb://network.vcsl;se6:jwdmuz1@vc6.network.vcsl/Everyone/";
        //path    =   "smb://vcsl95.local;administrator:cryo@10.1.165.217:445/FolderC/";
        //walkFileTree(path);        
        
        path    =   "C:/Users/YAwar/Desktop/RPs";
        System.out.println(getModifiedFileName(path, "abc.txt",0));
        //makeDirStructure(path, "/Company/Type/pcnAME/shareName/ahead/2/3/");
    }
    
    
    private static String getModifiedFileName(String destPath, String docName, int ver) throws MalformedURLException, SmbException {
        String fullName   =   destPath + "/" +docName;
        
        boolean isExist =   false;
        if(fullName.toLowerCase().contains("smb://")) {//if Dest Path is CIFS
            
            SmbFile file    =   new SmbFile(fullName);
            if(file.exists()) {  
                isExist =   true;
            }
            file    =   null;
            
        } else {//case of NTFS
           
            File file    =   new File(fullName);
            if(file.exists()) {  
                isExist =   true;
            }
            file    =   null;
        }
        
        if(isExist) {
            System.out.println("" +" Already exist "+fullName);
            String temp =   null;

            if(docName!=null && 
                    docName.contains(".")) {//case to deal with *.EXT

                temp =   docName.substring(0, docName.lastIndexOf("."))
                         +"_"+ System.currentTimeMillis()+ver + 
                         docName.substring(docName.lastIndexOf("."));

            } else {//if file name dont have extension

                System.out.println("".toString() +" Not have any Extension... ");
                temp =   docName +"_"+ System.currentTimeMillis()+ver;

            }
            //ReRun to see if this file still already exist...
            fullName    =   getModifiedFileName(destPath, temp, ++ver);
            System.out.println("" +" Full name becomes: "+fullName);
        }
        
        return fullName;
    }
    
    private static void makeDirStructure(String destPath, String repoPath) throws Exception {
        //synchronized (this) {
        
        StringTokenizer st = new StringTokenizer(repoPath, "/");
        int i=0;
        repoPath    =   "";
        while(st.hasMoreTokens()) {
            if(i<4) {
                st.nextToken();
            } else {
                repoPath    +=  "/" + st.nextToken();
            }
            i++;
        }
        System.out.println(repoPath);
        /*
            String cur  =   destPath+repoPath;
            System.err.println(cur);
            SmbFile file    =   new SmbFile(cur);
            if(!file.exists())
                file.mkdirs();
            
            cur =   destPath+repoPath+"/myTest";
            System.err.println(cur);
            SmbFile file2    =   new SmbFile(cur);
            if(!file2.exists())
                file2.mkdirs();
            
            cur =   destPath+"file4"+repoPath;
            System.err.println(cur);
            SmbFile file3    =   new SmbFile(cur);
            if(!file3.exists())
                file3.mkdirs();
            
            /*
            StringTokenizer st = new StringTokenizer(repoPath, "/");
            while(st.hasMoreTokens()) {
                System.out.println("mkdirs : " +destPath);

                String curDir   =   st.nextToken();
                destPath        =   destPath + "/" + curDir;
                SmbFile file    =   new SmbFile(destPath);
                if(!file.exists())
                    file.mkdir();
            }
             * 
             */
        //}
    }
    
    private static void walkFileTree(String strPath) throws Exception {
        
        //System.out.println("begin for"+strPath);
        
        CIFSFile cFile   =   new CIFSFile(strPath);
        //cFile.login("vcsl95", "administrator", "cryo");
        
        if(cFile.getFileObj().isDirectory()) {//Perform Recursion, if its a Directory
            System.out.println("Visiting Dir: "+cFile.getFileObj().getName());
         
            ACE[]   aceList =   cFile.getFileObj().getSecurity(false);
            for (ACE al : aceList) {
                System.out.println("PermD: " + al.getSID().toDisplayString());
            }
            
            LinkedList<String> cFileList =   cFile.getStrList();
            for(int i=0; i<cFileList.size(); i++) {
                walkFileTree(cFileList.get(i));
            }
        } else {//if its file
            //System.out.println("Visiting File: "+cFile.getFileObj().getOwnerUser(true).getAccountName());
            System.out.println("Visiting File: "+cFile.getFileObj().getName()+" - "+cFile.getFileObj().getLastModified());
            //long    time    =   Long.parseLong("1351560261407");
            //cFile.getFileObj().setCreateTime(time);
            //cFile.getFileObj().setLastModified(time);
            
            ACE[]   aceList =   cFile.getFileObj().getSecurity(false);
            for (ACE al : aceList) {
                System.out.println("Perm: " + al.getSID().toDisplayString());
            }
            
            //System.out.println("... "+cFile.getFileObj().length() + "|");
            
            //createInternetShortcut(strPath, "/Path of Repo SecureInfo/");
            
            //String fName    =   cFile.getFileObj().getName();
            //ACE[] acl   =   cFile.getFileObj().getSecurity(true);
            
            //for(int i=0; i<acl.length; i++){
            //    ACE ace =   acl[i];
            //    ace.toString();
            //}
            
            //SmbFileInputStream sis   =   new SmbFileInputStream(fName);
            //InputStream is  =   sis;
        }
    }
    
    private static String createInternetShortcut(String shortCutAt, String shortCutTo) throws Exception {
        
        shortCutTo = Utility.GetProp("DASUrl") + shortCutTo;
                
        String icon = Utility.GetIcon(shortCutAt);
        
        shortCutAt = shortCutAt + ".url";
        
        System.out.println("creating ShortCut at: " + shortCutAt);
        SmbFile shortCut        =   new SmbFile(shortCutAt);
        SmbFileOutputStream fw  = new SmbFileOutputStream(shortCut);
        //fw.write("Yawar Bukhari".getBytes());
        
        System.out.println("creating ShortCut to: "+shortCutTo);
        fw.write("[InternetShortcut]\n".getBytes());
        fw.write(("URL=" + shortCutTo + "\n").getBytes());
        
        if (!icon.isEmpty()) {
            fw.write(("IconFile=" + icon + "\n").getBytes());
            fw.write(("IconIndex=0" + "\n").getBytes());
        }
        
        fw.flush();
        fw.close();
        
        /////////////////if (Utility.GetProp("DeleteFiles").equals("1")) 
            /////////////////this.getCIFSFile().getFileObj().delete();
        
        fw = null;
        icon = null;
        shortCut = null;

        return shortCutTo;
    }
}