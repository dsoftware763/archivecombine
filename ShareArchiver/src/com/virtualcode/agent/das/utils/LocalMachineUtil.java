package com.virtualcode.agent.das.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 *
 * @author Abbas
 */
public class LocalMachineUtil {
    
    private static String ipAdr    =   null;
    
    public static void main(String args[]) {
        try{
            System.out.println("IP of my system is := "+LocalMachineUtil.getOwnIPAdr());
        }catch (Exception e){
            System.out.println("Exception caught ="+e.getMessage());
        }
    }
    
    public static String getOwnIPAdr() {
        if(ipAdr==null) {
            try {
                //Fetch the IP address on the basis of LocalHost Domain Name
                InetAddress ownIP   =   InetAddress.getLocalHost();
                ipAdr   =   ownIP.getHostName();//.getHostAddress();
                ownIP   =   null;
                
            } catch (UnknownHostException ue) {
                ue.printStackTrace();
            }
        }
        
        return ipAdr;
    }
}
