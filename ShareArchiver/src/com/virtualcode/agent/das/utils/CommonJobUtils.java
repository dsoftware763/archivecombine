package com.virtualcode.agent.das.utils;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSTask;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.cifs.Document;
import com.virtualcode.common.VCSConstants;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.vo.SystemCodeType;

import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;

public class CommonJobUtils {

	private Logger logger;
	private Logger loginfo;
	private FileTaskInterface task;
	
	public CommonJobUtils(Logger logger, Logger loginfo, FileTaskInterface task) {
		this.logger	=	logger;
		this.loginfo=	loginfo;
		this.task	=	task;
	}
	
	public boolean fetchContentsForStub(String repoPath, String destPath, Document doc) {

		boolean output	=	false;
        OutputStream fos    =   null;//its the Parent class of both SmbFileOutputStream and FileOutputStream
        DataInputStream dis     =   null;
        String destFile	=	null;
        
        try {
            //prepare the directory, where has to write the data stream
            destPath =   createDirectoryStructure(destPath);
            logger.info(this.task.getUid().toString() + " Exporting at: "+destPath);
            
            String fileName	=	this.task.getDocName();
            fileName	=	(fileName.endsWith(".url"))? fileName.substring(0, fileName.length()-4) : fileName;
            
            //destPath    =   "smb://network.vcsl;se7:s7*@vc7.network.vcsl/adTariz/";
            destFile =   getModifiedFileName(destPath, fileName, doc, 0);
            if(destFile!=null && destFile.startsWith("AE|")) {//if its already exist, and its a Lazy Export mode
            	loginfo.info("File already exists "+((destFile!=null && destFile.contains("@"))?destFile.substring(destFile.lastIndexOf('@')):destFile));
            	output	=	true;
            	return output;
            }
            
            //build connections to read file
            URL                url; 
            URLConnection      urlConn;             
            url     = new URL(repoPath); 
            urlConn = url.openConnection(); 
            urlConn.setDoInput(true); 
            urlConn.setUseCaches(false);
            dis =   new DataInputStream(urlConn.getInputStream());
            Long curSize =   0L;

            //get the configured BufferedWrite Size
            ResourcesUtil ru  	=   ResourcesUtil.getResourcesUtil();
            String tempSize	=	ru.getCodeValue("BUFF_SIZE_W", SystemCodeType.GENERAL);
            int buffSize	=	8192;//defaultBufferSize
            if(tempSize!=null && !tempSize.isEmpty()) {
            	try {
            		buffSize	=	Integer.parseInt(tempSize);
            	}catch(Exception ex) {
            		logger.warn(" Error to parse BUFF_SIZE_W: "+tempSize);
            	}
            }
            logger.info(this.task.getUid().toString() + " write_buffered is: "+buffSize);
            
            if(destPath.toLowerCase().contains("smb://")) {//if Dest Path is CIFS
                fos =   new SmbFileOutputStream(destFile);
            } else {
                fos =   new FileOutputStream(destFile);
            }
            // Now copy bytes from the URL to the output stream
            byte[] buffer = new byte[buffSize];
            int bytes_read;
            while((bytes_read = dis.read(buffer)) != -1) {
                fos.write(buffer, 0, bytes_read);
                curSize	=	curSize + bytes_read;
            }
            
            //set the Exported / Restored file size for Job Statistics
            if(curSize!=null && curSize.intValue() > 0) {//it can be NULL or 0, in case of Linked Nodes
                task.getTaskKpi().setFileSize(curSize);
            }
            
            //Set the resultant output to True
            output  	=   true;
            curSize		=	null;
            
            //Now DELETE the stub (if Exported with SUCCESS)
            try {
            	String stubPath =   destPath + "/" + fileName + ".url";
                //stubPath    =   stubPath.substring(0, stubPath.lastIndexOf(".")) + ".url";
                logger.info(this.task.getUid().toString() + " Deleting Stub: "+stubPath);
                
                if(stubPath.toLowerCase().contains("smb://")) {//if Dest Path is CIFS
                    SmbFile f =   new SmbFile(stubPath);
                    f.delete();
                    f = null;
                } else {
                    File f =   new File(stubPath);
                    f.delete();
                    f = null;
                }
            } catch (Exception ie) {
                //ie.printStackTrace();
                logger.error(this.task.getUid().toString() +" Deletion Failed: " +ie.getMessage());
            }
            
        } catch (IOException ioe) {
        	loginfo.error("Failed to export file "+((task.getDestPath()!=null && task.getDestPath().contains("@"))?task.getDestPath().substring(task.getDestPath().lastIndexOf('@')):task.getDestPath()));
        	
            logger.error(this.task.getUid().toString() +" : "+ ioe.getMessage());
            ioe.printStackTrace();
            output  =   false;
            
        } finally {
            if (dis!=null) 
                try {
                    dis.close();
                } catch (IOException e) {}
            if (fos!=null) 
                try {
                    fos.close();
                } catch (IOException e) {}
        }
        
        //Delete the recently downloaded incomplete file (if some problem occured to write it) 
        if(output==false && destFile!=null) {
            try {
            	loginfo.error("Error during export "+((task.getDestPath()!=null && task.getDestPath().contains("@"))?task.getDestPath().substring(task.getDestPath().lastIndexOf('@')):task.getDestPath()));
            	
            	logger.info(this.task.getUid().toString() + " Export Currupted, so DELETING.. "+destFile);
                if(destFile.toLowerCase().contains("smb://")) {//if Dest Path is CIFS
                    SmbFile f =   new SmbFile(destFile);
                    f.delete();
                    f = null;
                } else {
                    File f =   new File(destFile);
                    f.delete();
                    f = null;
                }
            } catch (Exception ie) {
                //ie.printStackTrace();
                logger.error(this.task.getUid().toString() +" Deletion of CURRUPT File Failed: " +ie.getMessage());
            }
            
        } else if(destFile!=null && doc!=null) {//apply the Orignial properties, those were archived in repository
        	try {
            	logger.info(this.task.getUid().toString() + " Reseting attribs, after Export.. ");
                if(destFile.toLowerCase().contains("smb://")) {//if Dest Path is CIFS
                    SmbFile f =   new SmbFile(destFile);
                    
                    SimpleDateFormat srcDateFormat = new SimpleDateFormat(VCSConstants.SRC_DATE_FORMAT);
            		Date mD	=	(doc.getModifiedOn()!=null)?srcDateFormat.parse(doc.getModifiedOn()):null;
            		Date lD	=	(doc.getAccessedOn()!=null)?srcDateFormat.parse(doc.getAccessedOn()):null;
            		Date cD	=	(doc.getCreatedOn()!=null)?srcDateFormat.parse(doc.getCreatedOn()):null;
            		
            		if(mD!=null) f.setLastModified(mD.getTime());
            		if(lD!=null) f.setAccessTime(lD.getTime());
            		if(cD!=null) f.setCreateTime(cD.getTime());
            		
                    f = null;
                } else {
                    File f =   new File(destFile);

                    SimpleDateFormat srcDateFormat = new SimpleDateFormat(VCSConstants.SRC_DATE_FORMAT);
            		Date mD	=	(doc.getModifiedOn()!=null)?srcDateFormat.parse(doc.getModifiedOn()):null;
            		Date lD	=	(doc.getAccessedOn()!=null)?srcDateFormat.parse(doc.getAccessedOn()):null;
            		Date cD	=	(doc.getCreatedOn()!=null)?srcDateFormat.parse(doc.getCreatedOn()):null;
            		
            		if(mD!=null) f.setLastModified(mD.getTime());
            		//if(lD!=null) f.setAccessTime(lD.getTime());
            		//if(cD!=null) f.setCreateTime(cD.getTime());
            		
                    f = null;
                }
            } catch (Exception ie) {
                //ie.printStackTrace();
                logger.error(this.task.getUid().toString() +" Attribute resetting failed: " +ie.getMessage());
                LoggingManager.printStackTrace(ie, logger);
            }
        	
        	destFile	=	null;
        }
        
        return output;
	}

	private int fileExistsStatus(String fullName, Document doc) throws SmbException, MalformedURLException,  IOException {
		int existsStatus	=   0;
        if(fullName.toLowerCase().contains("smb://")) {//if Dest Path is CIFS
        	
            logger.info(this.task.getUid().toString() +" File Name: "+fullName);
            synchronized (this.getClass()) {//Only one instance of following block should execute at a time                
                SmbFile file    =   new SmbFile(fullName);
                if(file.exists()) {
                	logger.info(this.task.getUid().toString() +" already exists with name:"+fullName);

                    ResourcesUtil ru  	=   ResourcesUtil.getResourcesUtil();
                    String isLazyExport	=   ru.getCodeValue("LazyExport", SystemCodeType.GENERAL);
                    
                	if(doc!=null && !"false".equals(isLazyExport)) {//if its Lazy Export
                		
                		SimpleDateFormat srcDateFormat = new SimpleDateFormat(VCSConstants.SRC_DATE_FORMAT);                		
                		String modifD	=	srcDateFormat.format(new Date(file.lastModified()));
                		String creatD	=	srcDateFormat.format(new Date(file.createTime()));
                		
                		logger.info(this.task.getUid().toString() +" comparing Modif Dte "+doc.getModifiedOn()+" | "+modifD);
                		logger.info(this.task.getUid().toString() +" comparing Creat Dte "+doc.getCreatedOn()+" | "+creatD);
                		
                		if(modifD.equals(doc.getModifiedOn()) && creatD.equals(doc.getCreatedOn())) {
                			existsStatus	=	2;//if file Already exist, and Dont want to re-write
                		} else {
                			existsStatus	=	1;
                		}
                		
                	} else {
                		existsStatus =   1;//file already exist, and want to rewrite with different Name
                	}
                }
                file    =   null;
            }
            
        } else {//case of NTFS
           
            File file    =   new File(fullName);
            if(file.exists()) {  
                ResourcesUtil ru  	=   ResourcesUtil.getResourcesUtil();
                String isLazyExport	=   ru.getCodeValue("LazyExport", SystemCodeType.GENERAL);
                
            	if(doc!=null && !"false".equals(isLazyExport)) {//if its Lazy Export

            		SimpleDateFormat srcDateFormat = new SimpleDateFormat(VCSConstants.SRC_DATE_FORMAT);
            		String modifD	=	srcDateFormat.format(new Date(file.lastModified()));
            		//String creatD	=	srcDateFormat.format(new Date(file.createTime()));
            		
            		logger.info(this.task.getUid().toString() +" comparing Modif Dte "+doc.getModifiedOn()+" | "+modifD);
            		//logger.info(this.task.getUid().toString() +" comparing Creat Dte "+doc.getCreatedOn()+" | "+creatD);
            		
            		if(modifD.equals(doc.getModifiedOn())) {// && creatD.equals(doc.getCreatedOn())) {
            			existsStatus	=	2;//if file Already exist, and Dont want to re-write
            		} else {
            			existsStatus	=	1;
            		}
            	} else {
            		existsStatus =   1;//file already exist, and want to rewrite with different Name
            	}
            }
            file    =   null;
        }
        return existsStatus;
	}

    private String getModifiedFileName(String destPath, String docName, Document doc, int ver) throws IOException {
    	String fullName   =   destPath + "/" +docName;
    	
    	int existsStatus	=	fileExistsStatus(fullName, doc);
    	if(existsStatus==0) {
        	//Do Normal flow, to download file...
    		
    	} else if(existsStatus==1) {//if fileExists, then write with different Name...
    		
            logger.info(this.task.getUid().toString() +" Already Exist: "+fullName);
            String temp =   null;

            if(docName!=null && 
                    docName.contains(".")) {//case to deal with *.EXT

                temp =   docName.substring(0, docName.lastIndexOf("."))
                         +"_"+System.currentTimeMillis()+ ver + 
                         docName.substring(docName.lastIndexOf("."));

            } else {//if file name dont have extension

                logger.info(this.task.getUid().toString() +" Not have any Extension... ");
                temp =   docName +"_"+ System.currentTimeMillis()+ ver;

            }
            //ReRun to see if this file still already exist...
            fullName    =   getModifiedFileName(destPath, temp, doc, ++ver);
            logger.info(this.task.getUid().toString() +" Full name becomes: "+fullName);
            
        } else if(existsStatus==2) {//if file exists with same MetaData, and we dont want to re-write it...
        	logger.debug(this.task.getUid().toString() + " Already Exist with same MetaData: "+ fullName);
        	fullName	=	"AE|"+fullName;//make the fullName empty
        }
        
        return fullName;
    }
    
    private String createDirectoryStructure(String destPath) {
        
        try {
            /** TEMPORARILY COMMENTED, bcoz now we are building DestPath in RepoWalker
            StringTokenizer st = new StringTokenizer(repoPath, "/");
            int i=0;
            repoPath    =   "";
            while(st.hasMoreTokens()) {
                if(i<4) {
                    st.nextToken();
                } else {
                    repoPath    +=  "/" + st.nextToken();
                }
                i++;
            }
            //logger.debug(repoPath);
            destPath    =   destPath + repoPath.replaceAll("[.]", "_");
            */
            
            if(destPath.toLowerCase().contains("smb://")) {//if Dest Path is CIFS
                
                synchronized (this.getClass()) {//Only one instance of following block should execute at a time
                    SmbFile file    =   new SmbFile(destPath);
                    if(!file.exists())
                        file.mkdirs();
                }
                /*
                synchronized (this) {
                    StringTokenizer st = new StringTokenizer(repoPath, "/");
                    while(st.hasMoreTokens()) {
                        System.out.println("mkdirs : " +destPath);
                        
                        String curDir   =   st.nextToken();
                        destPath        =   destPath + "/" + curDir;
                        SmbFile file    =   new SmbFile(destPath);
                        if(!file.exists())
                            file.mkdir();
                    }
                }
                 */                
            } else {
                File file   =   new File(destPath);
                if(!file.exists())
                    file.mkdirs();
            }
        } catch( MalformedURLException mue) {
            mue.printStackTrace();
            logger.error(this.task.getUid().toString() + " : " + mue.getMessage());
            
        } catch(SmbException mue) {
            mue.printStackTrace();
            logger.error(this.task.getUid().toString() + " : " + mue.getMessage());
        }
        return destPath;
    }

    public String createInternetShortcut(String shortCutAt, String shortCutTo, SmbFile fileObj) throws Exception {
        
        shortCutTo = Utility.GetProp("DASUrl") + shortCutTo;
                
        String icon = Utility.GetIcon(shortCutAt);
        
        shortCutAt = shortCutAt + ".url";
        
        //System.out.println("creating ShortCut at: " + shortCutAt);
        SmbFile shortCut        =   new SmbFile(shortCutAt);
        SmbFileOutputStream fw  =   new SmbFileOutputStream(shortCut);
        //fw.write("Yawar Bukhari".getBytes());
        
        //Write the URL and Icon etc on STUB
        fw.write("[InternetShortcut]\n".getBytes());
        fw.write(("URL=" + shortCutTo + "\n").getBytes());
        
        if (!icon.isEmpty()) {
            fw.write(("IconFile=" + icon + "\n").getBytes());
            fw.write(("IconIndex=0" + "\n").getBytes());
        }
        
        fw.flush();
        fw.close();
        
        //Apply the MetaData of Orignal File on STUB
        try {
        	shortCut.setAccessTime(fileObj.getLastAccess());
            shortCut.setCreateTime(fileObj.createTime());
            shortCut.setLastModified(fileObj.getLastModified());
        } catch(Exception e) {
            e.printStackTrace();
        }
        
        //Delete the Original file, if have to contain only Stub
        if (Utility.GetProp("DeleteFiles").equals("1")) 
            fileObj.delete();
         
        fw = null;
        icon = null;
        shortCut = null;

        return shortCutTo;
    }

    public String readDownloadPathFromStub(SmbFile fileObj) {
    	String repoPath	=	null;
    	DataInputStream in = null;
    	
    	try{
    		
			// Get the object of DataInputStream
			in	= new DataInputStream(fileObj.getInputStream());
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			
			String strLine;
			//Read File Line By Line
			while ((strLine = br.readLine()) != null) 	{
				
				logger.info(this.task.getUid().toString() + " : " + "stub line: "+strLine);
				if(strLine.startsWith("URL=")) {
					repoPath	=	strLine.substring(4) + "?mode=downloadFS";
					break;
				}
			}
			
		} catch (Exception e){
			
			logger.error(this.task.getUid().toString() + " : " + "Error: " + e.getMessage());
			loginfo.error(this.task.getUid().toString() + " : " + "Error to read stub!");
			
		} finally {
			if(in!=null) {
				try {
					in.close();
				} catch(Exception e) {}
			}
		}
    	return repoPath;
    }
}
