/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.utils;

import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.controller.DocumentManagementModule;
import com.virtualcode.vo.SystemCodeType;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javax.mail.internet.MimeUtility;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import com.virtualcode.cifs.Acl;
import com.virtualcode.util.ResourcesUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.PropertyResourceBundle;

/**
 *
 * author Administrator
 */
public class Utility {

    public static String zeroGUID = "00000000-0000-0000-0000-000000000000";
    private static DocumentManagementModule port    =   null;
    
    private static ResourceBundle mapBdl    =   null;
    private static ResourcesUtil   ru  =   null;
    
    private static Logger logger    =   Logger.getLogger(Utility.class);

    /**
     * <p>Returns an upper case hexadecimal <code>String</code> for the given
     * character.</p>
     *
     * param ch The character to convert.
     * return An upper case hexadecimal <code>String</code>
     */
    private static String hex(char ch) {
        return Integer.toHexString(ch).toUpperCase(Locale.ENGLISH);
    }

    public static String uploadDocument(String fileContextPath, FileTaskInterface task, String layers, Acl acl, String transID) throws Exception {
        
        port    = (port == null) ? new DocumentManagementModule() : port;
        return port.uploadDocument(fileContextPath, task, layers, acl, transID);
    }
    
    /*
    public static DataHandler downloadContent(java.lang.String path, java.lang.String layers) throws Exception {
        expService  = (expService==null)? new DataExportService() : expService;
        expPort     = (expPort==null)? expService.getDataExport(new WebServiceFeature[]{new MTOMFeature()}) : expPort;
        
        Map ctxt = ((BindingProvider) expPort).getRequestContext();
        ctxt.put(JAXWSProperties.HTTP_CLIENT_STREAMING_CHUNK_SIZE, 8192);
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,Utility.GetProp("DASUrl") + "/DataExport?wsdl");
        return expPort.downloadContent(path, layers);
    }
     * 
     */
    
    /*
     * *************************************************************************
     * Utility Function for executionCompleted
     * *************************************************************************
     */

    public static XMLGregorianCalendar convertDate(Date date) throws Exception {
        GregorianCalendar gc = (GregorianCalendar) GregorianCalendar.getInstance();
        gc.setTime(date);
        DatatypeFactory dataTypeFactory = null;
        dataTypeFactory = DatatypeFactory.newInstance();
        return dataTypeFactory.newXMLGregorianCalendar(gc);
    }

    public static BasicFileAttributes getAttributeObject(Path file) {
        try {
            BasicFileAttributeView view =
                    Files.getFileAttributeView(file, BasicFileAttributeView.class, LinkOption.NOFOLLOW_LINKS);
            
            return view.readAttributes();

        } catch (Exception ex) {
            logger.info("Exception:" + ex.toString());
        }
        return null;
    }
// its a use less function
    public static Logger getLogger(String name) {
        org.apache.log4j.Logger log = Logger.getLogger(name);
        try {
            Properties props = new Properties();
            props.setProperty("log4j.appender.file", "org.apache.log4j.RollingFileAppender");

            props.setProperty("log4j.appender.file.maxFileSize", Utility.GetProp("maxFileSize"));
            props.setProperty("log4j.appender.file.maxBackupIndex", Utility.GetProp("maxBackupIndex"));
            props.setProperty("log4j.appender.file.File", "ThreadLog/" + name + ".log");
            props.setProperty("log4j.appender.file.threshold", Utility.GetProp("threshold"));
            props.setProperty("log4j.appender.file.layout", "org.apache.log4j.PatternLayout");
            props.setProperty("log4j.appender.file.layout.ConversionPattern", Utility.GetProp("ConversionPattern"));
            //        props.setProperty("log4j.appender.stdout", "org.apache.log4j.ConsoleAppender");
            //props.setProperty("log4j.appender.stdout.Target","System.out");
            props.setProperty("log4j.logger." + name, "DEBUG, file");

            // props.setProperty("log4j.logger.LoadHandler","DEBUG, file");
            PropertyConfigurator.configure(props);
            props = null;
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        }
        return log;
    }

    public static String encodedURL(String url, String encoding) throws UnsupportedEncodingException {
        if (url == null || url.length() < 2) {
            return url;
        }

        String[] tokens = url.substring(1).split("/");
        String[] correctedTokens = new String[tokens.length];
        int i = 0;

        for (String t : tokens) {
            correctedTokens[i++] = URLEncoder.encode(t, encoding);
        }

        StringBuilder stb = new StringBuilder();
        for (String t : correctedTokens) {
            stb.append("/").append(t);
        }
        return stb.toString();
    }

    public static String toProperCase(String name) {
        name = name.toLowerCase();
        name = name.substring(0, 1).toUpperCase() + name.substring(1, name.length());
        return name;
    }

    public static void WriteFile(String fileName, byte[] data) throws Exception {
        String strFilePath = fileName;
        FileOutputStream fos = new FileOutputStream(strFilePath);
        fos.write(data);
        fos.close();
    }

    public static String now() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yy/MM/dd HH:mm:ss");
        return sdf.format(cal.getTime());

    }

//    public static Long age(int y, int m) {
//        /*Calendar now = new GregorianCalendar();
//        Calendar cal = Calendar.getInstance();
//        
//        cal.add(cal.YEAR, -1 * y);
//        cal.add(cal.MONTH, -1 * m);
//        int res = now.get(Calendar.YEAR) - cal.get(Calendar.YEAR);
//        if((cal.get(Calendar.MONTH) > now.get(Calendar.MONTH))
//        || (cal.get(Calendar.MONTH) == now.get(Calendar.MONTH)
//        && cal.get(Calendar.DAY_OF_MONTH) > now.get(Calendar.DAY_OF_MONTH)))
//        {
//        res--;
//        }
//        return res;
//         *
//         */
//        return y * 365 + m * 30 * 24 * 60 * 60 * 1000L;
//    }
    public static Long calculateMilliSec(int y, int m) {
        if (y + m == 0) {
            return 0L;
        } else {
            Calendar cal = Calendar.getInstance();
            cal.add(cal.YEAR, -1 * y);
            cal.add(cal.MONTH, -1 * m);
            return cal.getTimeInMillis();
        }
    }

    public static int parseYear(String yearMonth) throws Exception {
        String[] params = yearMonth.split(",");
        if (params.length > 1) {
            return Integer.parseInt(params[0]);
        } else {
            throw new Exception("Parse Year Not Correct Format: " + yearMonth);
        }
    }

    public static int parseMonth(String yearMonth) throws Exception {
        String[] params = yearMonth.split(",");
        if (params.length > 1) {
            return Integer.parseInt(params[1]);
        } else {
            throw new Exception("Parse Month Not Correct Format: " + yearMonth);
        }
    }

    public static long calculateSize(String size) throws Exception {
        String[] params = size.split(",");
        if (params.length > 1) {
            return (params[1].toUpperCase().equals("MB")) ? 1024 * 1024 * Long.parseLong(params[0]) : Long.parseLong(params[0]) * 1024;
        } else {
            throw new Exception("Calculate Size Not Correct Format: " + size);
        }
    }

    public static Date CurrDate() throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date = new java.util.Date();
        String dateStr = dateFormat.format(date);
        date = null;
        return dateFormat.parse(dateStr);

    }

    public static String GetProp(String prop) {
         
        /* As the config is moved into DB, so Schedular.properteis is not required now
        if(propBdl==null) {
            String filePath =   "Schedular.properties";
            
            FileInputStream is  =   null;
            //java.util.logging.LoggingManager.getLogger(Utility.class.getName()).log(Level.INFO, "Loading "+filePath+"...");
            try {
                is      = new FileInputStream(filePath);
                propBdl = new PropertyResourceBundle(is);
            } catch (IOException ie) {
                propBdl =   null;
                java.util.logging.LoggingManager.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ie);
            } finally {
                if(is!=null) 
                    try {
                        is.close();
                        is  =   null;
                    } catch(IOException ie) {}
            }
        }
        String toRet = propBdl.getString(prop).trim();
        */
// removed if condition to update agent modified properties  at runtime        
//        if(ru==null) {
            ru  =   ResourcesUtil.getResourcesUtil();
//        }
        String toRet    =   ru.getCodeValue(prop, SystemCodeType.ZUES_AGENT);
        
        return toRet;
    }
    
    public static String getMapingEntry(String mapFilePath, String entryFor) {
        
        String toRet = null;
        try {
            FileInputStream is  =   null;
            if(mapBdl==null) {
                //java.util.logging.LoggingManager.getLogger(Utility.class.getName()).log(Level.INFO, "Loading "+mapFilePath+"...");
                is      = new FileInputStream(mapFilePath);
                mapBdl  = new PropertyResourceBundle(is);
            }
            
            //toRet   =   bdl.getString(entryFor).trim();
            Enumeration<String> bdlEnum =   mapBdl.getKeys();//Get all Keys, i.e. entryFor
            String curKey   =   "";
            while(bdlEnum.hasMoreElements()) {
                curKey      =   bdlEnum.nextElement();
                if(curKey!=null && curKey.equalsIgnoreCase(entryFor.trim())) {//Compare all Keys with Path, to ignore the Case-Issue
                    toRet   =   mapBdl.getString(curKey);
                    break;
                }
            }
            
            if(is!=null) {
                is.close();
                is = null;
            }
            
        } catch (Exception ex) {
            toRet   =   null;
            //java.util.logging.Logger.getLogger(Utility.class.getName()).log(Level.INFO, "Entry in "+mapFilePath+" not found for " + entryFor);
        }
        return toRet;
    }
    
    public static void resetUtilityResourceBundles() {
        
        //Reset the values to NULL, so that the Static Var could read new file...
        //propBdl =   null;
        mapBdl  =   null;
    }
    
    public static int nthOccurrence(String str, char c, int n) {
        int pos = str.indexOf(c, 0);
        while (n-- > 0 && pos != -1)
            pos = str.indexOf(c, pos+1);
        return pos;
    }
    
    public static String GetPoolPropertyAsString(String prop) {
        /* as the config is moved into DB, so Schedular.properties not required now
        try {
            FileInputStream is = new FileInputStream("Schedular.properties");
            ResourceBundle bdl = new PropertyResourceBundle(is);
            String toRet = bdl.getString(prop).trim();
            is.close();

            bdl = null;
            is = null;
            return toRet;

        } catch (IOException ex) {
            java.util.logging.LoggingManager.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
         */
        return GetProp(prop);
    }

    public static int GetPoolSize(String prop) {
        int toRet = Runtime.getRuntime().availableProcessors();
        try {
            toRet = Integer.parseInt(GetProp(prop)) == 0 ? toRet : Integer.parseInt(GetProp(prop));
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        }
        return toRet;

    }

    public static int GetPoolProperty(String prop) {
        int toRet = 0;
        try {
            toRet = Integer.parseInt(GetProp(prop)) == 0 ? toRet : Integer.parseInt(GetProp(prop));
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        }
        return toRet;

    }

    public static String GetIcon(String fileName) throws Exception {
        String extension = FilenameUtils.getExtension(fileName).trim();
        String ApplicationStartPath = GetProp("StubPath");
        return (extension != null && extension.length() > 1)
                ? ApplicationStartPath + "\\" + extension + ".ico"
                : ApplicationStartPath + "\\app.ico";

    }

    public static String createChecksum(String filename) throws
            Exception {
        InputStream fis = new FileInputStream(filename);

        byte[] buffer = new byte[1024];
        MessageDigest complete = MessageDigest.getInstance("MD5");
        int numRead;
        do {
            numRead = fis.read(buffer);
            if (numRead > 0) {
                complete.update(buffer, 0, numRead);
            }
        } while (numRead != -1);
        fis.close();
        BigInteger i = new BigInteger(1, complete.digest());
        buffer = null;
        return String.format("%1$032X", i).toLowerCase();

    }

    public static byte[] encodetoBase64(byte[] b) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        OutputStream b64os = MimeUtility.encode(baos, "base64");
        b64os.write(b);
        b64os.close();
        byte[] res = new byte[baos.toByteArray().length];
        res = baos.toByteArray();
        baos.close();
        return res;
    }

    public static byte[] decodetoBase64(byte[] b) throws Exception {
        ByteArrayInputStream bais = new ByteArrayInputStream(b);
        InputStream b64is = MimeUtility.decode(bais, "base64");
        byte[] tmp = new byte[b.length];
        int n = b64is.read(tmp);
        byte[] res = new byte[n];
        System.arraycopy(tmp, 0, res, 0, n);
        b64is.close();
        bais.close();
        tmp = null;
        return res;
    }

    public static boolean getInterfacesAndMatchIP(String IP) {
        boolean bRet = false;
        if (IP.equals("127.0.0.1")) {
            logger.info("Local Loop Address is not Allowed");
            return bRet;
        }

        try {
            Enumeration e = NetworkInterface.getNetworkInterfaces();

            while (e.hasMoreElements()) {
                NetworkInterface ni = (NetworkInterface) e.nextElement();
                //logger.info("Net interface: "+ni.getName());

                Enumeration e2 = ni.getInetAddresses();

                while (e2.hasMoreElements()) {
                    InetAddress ip = (InetAddress) e2.nextElement();
                    if (ip.toString().contains(":")) {
                        continue;
                    }
                    logger.info("IP address: " + ip.toString().replace("/", ""));
                    if (IP.equals(ip.toString().replace("/", ""))) {
                        bRet = true;
                        break;
                    }

                }
                if (bRet) {
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bRet;
    }
    
    public static boolean containsIgnoreCase(ArrayList<String> list, String str) {
        
        if (list!=null && list.size()>0) {
            Iterator<String> itr=list.iterator();        
            while(itr.hasNext()) {
                if (itr.next().equalsIgnoreCase(str)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public static Timestamp convertDateToTimeStamp(Date date){
    	
    	Timestamp timestamp = new Timestamp(date.getTime());
    	return timestamp;
    }

    public static void main(String args[]) {
        ArrayList<String> list  =   new ArrayList<String>();
        list.add("TEST/test");
        list.add("TEST/ting");
        list.add("test/testing");
        list.add("TeST/ABC/testing/");
        
        if(containsIgnoreCase(list, null)) {
            logger.info("true");
        } else {
            logger.info("false");
        }
    }
    
    /*
    public static void main_old2(String args[]) {
        String mapEntry =   getMapingEntry("mapingFile.properties" , "10.1.165.17/share");
        logger.info("Map Entry: "+mapEntry);
        
        mapEntry =   getMapingEntry("mapingFile.properties" , "10.1.165.217/share2");
        logger.info("Map Entry: "+mapEntry);
        
        mapEntry =   getMapingEntry("mapingFile.properties" , "10.1.165.217/share");
        logger.info("Map Entry: "+mapEntry);
        
        mapEntry =   getMapingEntry("mapingFile.properties" , "10.1.165.17/share");
        logger.info("Map Entry: "+mapEntry);
        
        mapEntry =   getMapingEntry("mapingFile.properties" , "10.1.165.17/share");
        logger.info("Map Entry: "+mapEntry);
    }*/
    
    public static void main_Old(String args[]) {
        try {
            String unescapeStr = "http://10.1.165.20:8080/DAS0.2/repository/default/spserver01_42010/sites%20nn/vada/Shared%2520Documents/Cryoserver%2520Groupwise%2520Configuration.doc";
//            //  logger.info(URLDecoder.decode(unescapeStr,"Utf-8"));
            byte res1[] = encodetoBase64(unescapeStr.getBytes());
//
            logger.info(new String(res1));
            byte res2[] = decodetoBase64("dXNlcm5hbWU9bmF1bWFuJnBhc3N3b3JkPWE3MjZiODAyMGY3ZWRhNzdhM2IzOWEyOTZjZTJlMWJj".getBytes());
//
            logger.info(new String(res2));
//  
            logger.info(GetIcon("a.gif"));
            logger.info(GetIcon("a.gif.jpg"));
            logger.info(GetIcon("a."));
            logger.info(GetIcon("a"));
            logger.info(GetIcon("a \\ b . gif"));

//            logger.info("IP Match : " + getInterfacesAndMatchIP("192.168.30.249"));
//
//            logger.info("Agent ID : " + DasDBManager.getAgentID());


//            ArrayList<JobStatus> aList = DasDBManager.getReccurringAndScheduledJobs();
//            for(JobStatus s : aList)
//            {
//                logger.info("Job ID : " + s.archiveJobID);
//                
//            }

//            logger.info(StringEscapeUtils.unescapeJava("Y:\\csf\\FCS\\Sales\\Documentation\\July 07 \\u00A3.doc"));

            Runtime runtime = Runtime.getRuntime();

            int mb = 1024 * 1024;

            //Getting the runtime reference from system

            logger.info("##### Heap utilization statistics [MB] #####");

            //Print used memory
            logger.info("Used Memory:"
                    + (runtime.totalMemory() - runtime.freeMemory()) / mb);

            //Print free memory
            logger.info("Free Memory:"
                    + runtime.freeMemory() / mb);

            //Print total available memory
            logger.info("Total Memory:" + runtime.totalMemory() / mb);

            //Print Maximum available memory
            logger.info("Max Memory:" + runtime.maxMemory() / mb);


        } catch (Exception ex) {
            logger.info(ex);

        }
    }
}
