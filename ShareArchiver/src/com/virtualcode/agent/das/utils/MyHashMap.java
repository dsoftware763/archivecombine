/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.utils;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * @author YAwar
 */
public class MyHashMap {
    
    private HashMap<String, AtomicLong> hashMap;
    
    public MyHashMap() {
        hashMap =   new HashMap<String, AtomicLong>();
    }
    
    public void put(String key, AtomicLong value) {
        
        synchronized(this.getClass()) {
            hashMap.put(key, value);
        }
    }
    
    public String get(String key) {
        return ""+hashMap.get(key);
    }
    
    public void increment(String key) {
        
        synchronized(this.getClass()) {
            hashMap.get(key).incrementAndGet();
        }
    }
}
