/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.fileSystems.repo;

import com.virtualcode.controller.DataExportModule;
import com.virtualcode.agent.das.dataExporter.ExportStarter;
import com.virtualcode.agent.das.dataExporter.RepoWalker;
import com.virtualcode.agent.das.fileSystems.FileServiceInterface;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
//import com.virtualcode.agent.sa.export.DataExport;
//import com.virtualcode.agent.sa.export.DataExportService;
//import com.virtualcode.agent.sa.export.Document;
import com.virtualcode.cifs.Document;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Abbas
 */
public class RepoService implements FileServiceInterface {
    
    private Logger logger = null;
    private Logger loginfo	=	null;
    private String secureInfo   =   null;
    private String destPath     =   null;
    private String execID   =   null;
    
    public RepoService(String secureInfo, String destPath, String execID) {
        this.execID =   execID;
        logger  =   LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_DETAIL, this.execID);
        loginfo	=	LoggingManager.getLogger(LoggingManager.EXP_PUBLIC_MSGS, this.execID);
        
        this.secureInfo =   secureInfo;
        this.destPath   =   destPath;
    }
    
    public void walkFileTree(String strPath, ArrayList<String> excPathList, boolean isRecursive) {
        
        try {
            List<Document> docsList    =   getAllChildren(strPath, this.secureInfo);
            String thisLevelFiles   =   this.destPath;
            
            for(int i=0; i<docsList.size(); i++) {
                Document doc    = docsList.get(i);
                if(doc.isFolderYN() && isRecursive) {//if have to visit the main root path recursively
                    //logger.info("Visiting Dir: "+doc.getUrl());
                    String temp =   this.destPath + "/" + doc.getTitle();//.replaceAll("[.]", "_");
                    this.recursiveFileWalker(doc, excPathList, temp);

                } else {
                    this.visitFile(doc, thisLevelFiles);
                }
            }
        }catch (Exception e) {
        	loginfo.error("Error occured in exporting folder: "+((strPath!=null && strPath.contains("@"))?strPath.substring(strPath.lastIndexOf('@')):strPath));
            logger.error(e.getMessage());
        }
    }
    
    private void recursiveFileWalker(Document parentDoc, ArrayList<String> excPathList, String thisDocDestPath) throws Exception {
        
        //IF interupted by user than, Stop the Recursive Walk of Files
        if(ExportStarter.isInterupted(this.execID)) {
            logger.info("Folder Walker is being stopped for "+this.execID);
            return;
        }
            
        List<Document> docsList    =   getAllChildren(parentDoc.getUrl(), this.secureInfo);        
        for(int i=0; i<docsList.size(); i++) {
            Document doc    = docsList.get(i);
            if(doc.isFolderYN()) {
                //logger.info("Visiting Dir: "+doc.getUrl());
                if(Utility.containsIgnoreCase(excPathList, doc.getUrl()))
                    logger.info("Skip the Excluded Dir: " + doc.getUrl());
                
                else {    //if current is a folder and not in Excluded list
                    String temp =   thisDocDestPath + "/" + doc.getTitle();//.replaceAll("[.]", "_");
                    this.recursiveFileWalker(doc, excPathList, temp);
                }

            } else {
                this.visitFile(doc, thisDocDestPath);
            }
        }
    }
    
    private void visitFile(Document doc, String thisDocDestPath) {
        //logger.info("Visiting File "+doc.getTitle() + ", of size "+doc.getSize());
        //IF interupted by user than, Stop the Recursive Walk of Files
        if(ExportStarter.isInterupted(this.execID)) {
            logger.info("File Visitor is being stopped for "+this.execID +" - "+thisDocDestPath);
            return;
        }
        RepoWalker  rw  =   new RepoWalker(thisDocDestPath, this.execID);
        rw.visitFile(doc);
    }
    
    private static List<Document> getAllChildren(String parent, String layers) throws Exception {
        //DataExportService service = new DataExportService();
        //DataExport port = service.getDataExport();
        
        //Map ctxt = ((BindingProvider) port).getRequestContext();
        //ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,Utility.GetProp("DASUrl") + "/DataExport?wsdl");
        
        DataExportModule    port    =   new DataExportModule();
        return port.getAllChildren(parent, layers);
    }
    /*
    public String createInternetShortcut(String name, String target) throws Exception {
        
        //Path p = FileSystems.getDefault().getPath(name);
        
        
        target = Utility.GetProp("DASUrl") + target;
                
        String icon = Utility.GetIcon(name);
        
        name = name.replace(".", "") + ".url";
        
        FileWriter fw = new FileWriter(name);
        fw.write("[InternetShortcut]\n");
        fw.write("URL=" + target + "\n");
        
        if (!icon.isEmpty()) {
            fw.write("IconFile=" + icon + "\n");
            fw.write("IconIndex=0" + "\n");
        }
        
        fw.flush();
        fw.close();
        
        if (Utility.GetProp("DeleteFiles").equals("1"))  {
            //Files.delete(p);
            System.out.println("yawarz - Write Here to Del Smb File...");
        }
         
        fw = null;
        icon = name = null;

        return target;
    }
    */
}
