/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.fileSystems.repo;

import com.virtualcode.agent.das.dataArchiver.TaskKPI;
//import com.virtualcode.agent.sa.export.Document;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSFile;
//import com.virtualcode.agent.sa.Acl;
import com.virtualcode.cifs.Acl;
import com.virtualcode.cifs.Document;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;
import javax.activation.DataHandler;

/**
 *
 * @author Abbas
 */
public class RepoTask implements FileTaskInterface {

    private final Document doc;
    private String thisDocDestPath="";
    private UUID uid = null;
    private String encodedString = null;
    private TaskKPI taskKpi = new TaskKPI();
    private String execID   =   null;
    
    public Document getDocument() {
        return this.doc;
    }
    
    public CIFSFile getCIFSFile() {
    	return null;//Its only for CIFSTask (to be used in Archived & NewExport job)
    }
    
    public String getDocName() {
        return this.doc.getTitle();
    }
    
    public RepoTask(Document doc, String thisDocDestPath, String execID) {
        this.execID =   execID;
        
        if(uid == null)
            uid = UUID.randomUUID();
        this.doc        =   doc;
        this.thisDocDestPath   =   thisDocDestPath;
    }

    public String getExecID() {
        return this.execID;
    }
    
    public String getEncodedString() {
        return encodedString;
    }

    public void setEncodedString(String encodedString) {
        this.encodedString = encodedString;
    }

    public String getDestPath() {
        return this.thisDocDestPath;
    }

    public String getSecureRepoPath() {
        return this.doc.getSecureURL();
    }

    public void setSecureRepoPath(String repoPath) {
        this.doc.setSecureURL(repoPath);
    }

    public UUID getUid() {
        return uid;
    }

    public void setUid(UUID uid) {
        this.uid = uid;
    }
    
    public TaskKPI getTaskKpi() {
        return taskKpi;
    }

    public void setTaskKpi(TaskKPI taskKpi) {
        this.taskKpi = taskKpi;
    }
    
    public Date getLastAccessedDate() {
        long accessedOn =   Long.parseLong(this.doc.getAccessedOn());
        return new Date(accessedOn);
    }
    
    public Date getCreationDate() {
        long creationDate   =   Long.parseLong(this.doc.getModifiedOn());
        return new Date(creationDate);
    }
    
    public Date getLastModifiedDate() {
        long modifDate  =   Long.parseLong(this.doc.getModifiedOn());
        return new Date(modifDate);
    }
    
    public String getOwner() {
        return this.doc.getAuthor();
    }
    
    public String getURIPath() {
        return null;// only required in case of CIFSTask.class and NTFSTask.class
    }
    
    public String getPathStr() {
        return this.doc.getUrl();
    }
    
    public String getNodeHeirarchy() {
        return this.getPathStr();//in case of Export, its same as passed from Repo
    }
    
    public DataHandler getDataHandler() throws IOException {
        /*
        DataSource dataSource  =   null;
        try {
            
            InputStream is  =   this.cFile.getFileObj().getInputStream();
            dataSource      =   new CIFSDataSource(is);
            
        } catch  (Exception se) {
            throw new IOException("Cant read SmbFileInputStream for " + this.getPathStr());
        }
        
        return new DataHandler(dataSource);
         * 
         */
        return null;
    }
    /*    
    public String createInternetShortcut(String shortCutAt, String shortCutTo) throws Exception {

        shortCutTo = Utility.GetProp("DASUrl") + shortCutTo;
                
        String icon = Utility.GetIcon(shortCutAt);
        
        shortCutAt = shortCutAt + ".url";
        
        //System.out.println("creating ShortCut at: " + shortCutAt);
        SmbFile shortCut        =   new SmbFile(shortCutAt);
        SmbFileOutputStream fw  = new SmbFileOutputStream(shortCut);
        //fw.write("Yawar Bukhari".getBytes());
        
        //System.out.println("creating ShortCut to: "+shortCutTo);
        fw.write("[InternetShortcut]\n".getBytes());
        fw.write(("URL=" + shortCutTo + "\n").getBytes());
        
        if (!icon.isEmpty()) {
            fw.write(("IconFile=" + icon + "\n").getBytes());
            fw.write(("IconIndex=0" + "\n").getBytes());
        }
        
        fw.flush();
        fw.close();
        
        if (Utility.GetProp("DeleteFiles").equals("1")) 
            this.getCIFSFile().getFileObj().delete();
         
        fw = null;
        icon = null;
        shortCut = null;

        return shortCutTo;
         *
         
        return null;
    }*/
    
    public Acl getACL(String documentPath) throws IOException {
        Acl acl =   null;
        /*
        //Get the Original Security ACL
        ACE[] aceList    =   this.getCIFSFile().getFileObj().getSecurity();
        
        String mapShareInfo =   Utility.GetProp("mapShareInfo");
        if ("1".equals(mapShareInfo)) {//if has to retrieve the ShareSecurity from file
            String mapFilePath  =   Utility.GetProp("mapFilePath");
            
            //System.out.println("Full doc path: "+documentPath);
            String entryFor     =   documentPath.substring(0, Utility.nthOccurrence(documentPath, '/', 1));//
            //System.out.println("Get entry from mapFilePath: "+entryFor);
            String mapingEntry  =   Utility.getMapingEntry(mapFilePath, entryFor);
            
            //if(mapingEntry!=null) {
                //Transform the original ACL into customized ACL
                acl =   AuthorizationUtil.transformACE(aceList, mapingEntry);
            //}
        } else {//if get original ShareSecurity, instead of maping file...
            //Get the Share Security ACL
            ACE[] aceShareList    =   this.getCIFSFile().getFileObj().getShareSecurity(true);
            
            //Transform the original ACL into customized ACL
            acl =   AuthorizationUtil.transformACE(aceList, aceShareList);
        }
        */
        return acl;
    }

    public void closeCurrentTask(boolean syncLastAccessTime) {
    	/* mainly being used to reset the LastAccessTime of CIFS files
    	 * try {
    		this.getCIFSFile().resetLastAccessTime();
    	} catch (SmbException se) {
    		logger.warn(this.getExecID() +" Task NOT closed propertly");
    	}*/
    }
}
