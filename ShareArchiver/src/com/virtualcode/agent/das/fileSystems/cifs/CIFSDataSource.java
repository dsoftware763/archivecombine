/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.fileSystems.cifs;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.activation.DataSource;

/**
 *
 * @author se7
 */
public class CIFSDataSource implements DataSource {
    
    private InputStream is;
    
    public CIFSDataSource(InputStream sis) {
        this.is =   sis;
    }
    
    public String getContentType() {
        return "";
    }
    
    public InputStream getInputStream() {
        return this.is;
    }
    
    public String getName() {
        return "";
    }
    
    public OutputStream getOutputStream() throws IOException {
        throw new IOException("Cant do this...");
    }
}
