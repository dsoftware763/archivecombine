/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.fileSystems.cifs;

import com.virtualcode.agent.das.dataArchiver.FileExecutors;
import com.virtualcode.agent.das.dataIndexer.IndexStarter;
import com.virtualcode.agent.das.dataIndexer.IndexingExecutors;
import com.virtualcode.agent.das.fileSystems.FileServiceInterface;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.threadsHandler.IndexingTPEList;
import com.virtualcode.agent.das.threadsHandler.TPEList;
import com.virtualcode.agent.das.utils.Utility;
import java.util.ArrayList;
import java.util.LinkedList;
import org.apache.log4j.Logger;

/**
 *
 * @author Yawar
 */
public class CIFSIndexingService implements FileServiceInterface {
    
    private Logger logger = null;
    private Logger loginfo	=	null;
    private String execID   =   null;
    private String indexID	=	null;
    
    public CIFSIndexingService(String execID, String indexID) {
        this.execID =   execID;
        this.indexID	=	indexID;
        logger  =   LoggingManager.getLogger(LoggingManager.INDX_ACTIVITY_DETAIL, this.execID);
        loginfo	=	LoggingManager.getLogger(LoggingManager.INDX_PUBLIC_MSGS, this.execID);
    }
    
    public void walkFileTree(String strPath, ArrayList<String> excPathList, boolean isRecursive) {
        
        try {
            
            //IF interupted by user than, Stop the Recursive Walk of Files
            if(IndexStarter.isInterupted(this.execID)) {
                logger.info("File/Dir Walker is being stopped for "+this.execID);
                return;
            }
            
            CIFSFile cFile   =   new CIFSFile(strPath);
            //cFile.login("10.1.165.15", "mazhar", "newbuild");

            if(cFile.getFileObj().isDirectory() && isRecursive) {//Perform Recursion, if its a Directory
                logger.info("Visiting Dir: "+cFile.getFileObj().getName());

                /*if(cFile.getFileObj().getName().startsWith(".")) {//if its invalid directory
                    logger.warn("Skipped Dir..(bcoz of error in its Name)");
                } else*/ 
                
                if(Utility.containsIgnoreCase(excPathList,strPath)) {
                    logger.info("Skip the Excluded Dir: " + strPath);
                    
                } else {
                    LinkedList<String> cFileList =   cFile.getStrList();
                    for(int i=0; i<cFileList.size(); i++) {
                        this.walkFileTree(cFileList.get(i), excPathList, isRecursive);
                    }
                }
            } else {//if its file
                this.visitFile(cFile);
            }
            
        } catch (jcifs.smb.SmbAuthException sae){
        	loginfo.error("Data analyzer couldnt read file "+((strPath!=null && strPath.contains("@"))?strPath.substring(strPath.lastIndexOf('@')):strPath));
        	
            logger.error("Error: "+sae.getMessage());
            LoggingManager.printStackTrace(sae, logger);
            
        } catch (java.io.IOException ie) {
        	loginfo.error("Data analyzer couldnt read file "+((strPath!=null && strPath.contains("@"))?strPath.substring(strPath.lastIndexOf('@')):strPath));
        	
            logger.error("Error: "+ie.getMessage());
            LoggingManager.printStackTrace(ie, logger);
            
        } catch (Exception e) {
        	loginfo.error("Data analyzer failed for file "+((strPath!=null && strPath.contains("@"))?strPath.substring(strPath.lastIndexOf('@')):strPath));
        	
            logger.error("Error: "+e.getMessage());
            LoggingManager.printStackTrace(e, logger);
        }
    }
    
    private void visitFile(CIFSFile cFile) throws Exception {
        
    	logger.info("visiting : "+cFile.getFileObj().getPath());
    	
    	//IF interupted by user than, Stop the Recursive Walk of Files
        if(IndexStarter.isInterupted(this.execID)) {
            logger.info("File Visitor is being stopped for "+this.execID);
            cFile.resetLastAccessTime();
            return;
        }

        //CIFSWalker  cw  =   new CIFSWalker(this.execID);
        //cw.visitFile(cFile);
        
        IndexingExecutors.increment(execID);//Update the Counter by 1, for current execID
        CIFSTask task = new CIFSTask(cFile, execID);
        task.setIndexID(indexID);//to be used to fetch the IndexWriter instance, from the pool
        IndexingTPEList.startEvaluator(task.getExecID(), task);
    }
}
