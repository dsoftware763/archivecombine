package com.virtualcode.agent.das.fileSystems.cifs;

import com.virtualcode.agent.das.dataRestorer.RestoreStarter;
import com.virtualcode.agent.das.dataRestorer.RestoreExecutors;
import com.virtualcode.agent.das.fileSystems.FileServiceInterface;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.threadsHandler.RestoreTPEList;
import com.virtualcode.agent.das.utils.Utility;
import java.util.ArrayList;
import java.util.LinkedList;
import org.apache.log4j.Logger;

/**
 *
 * @author Yawar
 */
public class CIFSRestoreService implements FileServiceInterface {
    
    private Logger logger = null;
    private Logger loginfo	=	null;
    private String execID   =   null;
    
    public CIFSRestoreService(String execID) {
        this.execID =   execID;
        logger  =   LoggingManager.getLogger(LoggingManager.INDX_ACTIVITY_DETAIL, this.execID);
        loginfo	=	LoggingManager.getLogger(LoggingManager.INDX_PUBLIC_MSGS, this.execID);
    }
    
    public void walkFileTree(String strPath, ArrayList<String> excPathList, boolean isRecursive) {
        
        try {
            
            //IF interupted by user than, Stop the Recursive Walk of Files
            if(RestoreStarter.isInterupted(this.execID)) {
                logger.info("File/Dir Walker is being stopped for "+this.execID);
                return;
            }
            
            CIFSFile cFile   =   new CIFSFile(strPath);
            //cFile.login("10.1.165.15", "mazhar", "newbuild");

            if(cFile.getFileObj().isDirectory() && isRecursive) {//Perform Recursion, if its a Directory
                logger.info("Visiting Dir: "+cFile.getFileObj().getName());

                /*if(cFile.getFileObj().getName().startsWith(".")) {//if its invalid directory
                    logger.warn("Skipped Dir..(bcoz of error in its Name)");
                } else*/ 
                
                if(Utility.containsIgnoreCase(excPathList,strPath)) {
                    logger.info("Skip the Excluded Dir: " + strPath);
                    
                } else {
                    LinkedList<String> cFileList =   cFile.getStrList();
                    for(int i=0; i<cFileList.size(); i++) {
                        this.walkFileTree(cFileList.get(i), excPathList, isRecursive);
                    }
                }
            } else {//if its file
                this.visitFile(cFile);
            }
            
        } catch (jcifs.smb.SmbAuthException sae){
        	loginfo.error("Data restorer couldnt read file "+((strPath!=null && strPath.contains("@"))?strPath.substring(strPath.lastIndexOf('@')):strPath));
        	
            logger.error("Error: "+sae.getMessage());
            LoggingManager.printStackTrace(sae, logger);
            
        } catch (java.io.IOException ie) {
        	loginfo.error("Data restorer couldnt read file "+((strPath!=null && strPath.contains("@"))?strPath.substring(strPath.lastIndexOf('@')):strPath));
        	
            logger.error("Error: "+ie.getMessage());
            LoggingManager.printStackTrace(ie, logger);
            
        } catch (Exception e) {
        	loginfo.error("Data restorer failed for file "+((strPath!=null && strPath.contains("@"))?strPath.substring(strPath.lastIndexOf('@')):strPath));
        	
            logger.error("Error: "+e.getMessage());
            LoggingManager.printStackTrace(e, logger);
        }
    }
    
    private void visitFile(CIFSFile cFile) throws Exception {
        
    	logger.info("visiting : "+cFile.getFileObj().getPath());
    	
    	//IF interupted by user than, Stop the Recursive Walk of Files
        if(RestoreStarter.isInterupted(this.execID)) {
            logger.info("File Visitor is being stopped for "+this.execID);
            cFile.resetLastAccessTime();
            return;
        }

        //CIFSWalker  cw  =   new CIFSWalker(this.execID);
        //cw.visitFile(cFile);
        
        RestoreExecutors.increment(execID);//Update the Counter by 1, for current execID
        CIFSTask task = new CIFSTask(cFile, execID);
        RestoreTPEList.startEvaluator(task.getExecID(), task);
    }
}
