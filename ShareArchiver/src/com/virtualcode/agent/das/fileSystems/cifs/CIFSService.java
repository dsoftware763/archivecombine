/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.fileSystems.cifs;

import com.virtualcode.agent.das.dataArchiver.FileExecutors;
import com.virtualcode.agent.das.dataArchiver.JobStarter;
import com.virtualcode.agent.das.fileSystems.FileServiceInterface;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.threadsHandler.TPEList;
import com.virtualcode.agent.das.utils.Utility;
import java.util.ArrayList;
import java.util.LinkedList;
import org.apache.log4j.Logger;

/**
 *
 * @author se7
 */
public class CIFSService implements FileServiceInterface {
    
    Logger logger = null;
    Logger loginfo	=	null;
    private String execID   =   null;
    private String indexID	=	null;
    
    public CIFSService(String execID, String indexID) {
        this.execID =   execID;
        this.indexID	=	indexID;
        logger  =   LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL, this.execID);
        loginfo	=	LoggingManager.getLogger(LoggingManager.PUBLIC_MSGS, this.execID);
    }
    
    public void walkFileTree(String strPath, ArrayList<String> excPathList, boolean isRecursive) {
        
        try {
            
            //IF interupted by user than, Stop the Recursive Walk of Files
            if(JobStarter.isInterupted(this.execID)) {
                logger.info("File/Dir Walker is being stopped for "+this.execID);
                return;
            }
            
            CIFSFile cFile   =   new CIFSFile(strPath);
            //cFile.login("10.1.165.15", "mazhar", "newbuild");

            if(cFile.getFileObj().isDirectory() && isRecursive) {//Perform Recursion, if its a Directory
                logger.info("Visiting Dir: "+cFile.getFileObj().getName());

                /*if(cFile.getFileObj().getName().startsWith(".")) {//if its invalid directory
                    logger.warn("Skipped Dir..(bcoz of error in its Name)");
                } else*/ 
                
                if(Utility.containsIgnoreCase(excPathList,strPath)) {
                    logger.info("Skip the Excluded Dir: " + strPath);
                    
                } else {
                    LinkedList<String> cFileList =   cFile.getStrList();
                    for(int i=0; i<cFileList.size(); i++) {
                        this.walkFileTree(cFileList.get(i), excPathList, isRecursive);
                    }
                }
            } else {//if its file
                this.visitFile(cFile);
            }
            
        } catch (jcifs.smb.SmbAuthException sae){
        	loginfo.error("File reader is failed to read file "+((strPath!=null && strPath.contains("@"))?strPath.substring(strPath.lastIndexOf('@')):strPath));
            logger.error("Error: "+sae.getMessage());
            LoggingManager.printStackTrace(sae, logger);
            
        } catch (java.io.IOException ie) {
        	loginfo.error("File reader is failed to read file "+((strPath!=null && strPath.contains("@"))?strPath.substring(strPath.lastIndexOf('@')):strPath));
        	logger.error("Error: "+ie.getMessage());
            LoggingManager.printStackTrace(ie, logger);
            
        } catch (Exception e) {
        	loginfo.error("File reader is failed to read file "+((strPath!=null && strPath.contains("@"))?strPath.substring(strPath.lastIndexOf('@')):strPath));
        	logger.error("Error: "+e.getMessage());
            LoggingManager.printStackTrace(e, logger);
        }
    }
    
    private void visitFile(CIFSFile cFile) throws Exception {
        
        logger.info("Visiting File: "+cFile.getFileObj().getName());
        //IF interupted by user than, Stop the Recursive Walk of Files
        if(JobStarter.isInterupted(this.execID)) {
            logger.info("File Visitor is being stopped for "+this.execID);
            cFile.resetLastAccessTime();
            return;
        }

        //CIFSWalker  cw  =   new CIFSWalker(this.execID);
        //cw.visitFile(cFile);
        
        //FileExecutors.fileWalkedCount += 1;
        FileExecutors.incrementCounter(execID);//Update the Counter by 1, for current execID
        CIFSTask task = new CIFSTask(cFile, execID);
        logger.info("visiting CIFS ("+task.getExecID()+"): "+cFile.getFileObj().getPath());
        task.setIndexID(indexID);
        //JobStarter.fileEvaluator.execute(new CIFSEvaluater(task));
        //TPEList.getFileEvaluator(task.getExecID()).execute(new CIFSEvaluater(task));
        TPEList.startFileEvaluator(task.getExecID(), task);
    }
    
    /*
    public String createInternetShortcut(String name, String target) throws Exception {
        
        //Path p = FileSystems.getDefault().getPath(name);
        
        
        target = Utility.GetProp("DASUrl") + target;
                
        String icon = Utility.GetIcon(name);
        
        name = name.replace(".", "") + ".url";
        
        FileWriter fw = new FileWriter(name);
        fw.write("[InternetShortcut]\n");
        fw.write("URL=" + target + "\n");
        
        if (!icon.isEmpty()) {
            fw.write("IconFile=" + icon + "\n");
            fw.write("IconIndex=0" + "\n");
        }
        
        fw.flush();
        fw.close();
        
        if (Utility.GetProp("DeleteFiles").equals("1"))  {
            //Files.delete(p);
            //System.out.println("yawarz - Write Here to Del Smb File...");
        }
         
        fw = null;
        icon = name = null;

        return target;
    }
    */
}
