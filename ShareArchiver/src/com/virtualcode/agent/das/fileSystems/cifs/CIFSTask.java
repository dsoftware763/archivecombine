/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.fileSystems.cifs;

import com.virtualcode.agent.das.dataArchiver.TaskKPI;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.agent.das.utils.AuthorizationUtil;
//import com.virtualcode.agent.sa.Acl;
import com.virtualcode.cifs.Acl;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.UUID;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import jcifs.smb.ACE;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;
import org.apache.log4j.Logger;

/**
 *
 * @author Abbas
 */
public class CIFSTask implements FileTaskInterface {

    private final CIFSFile cFile;
    private String repoPath="";
    private UUID uid = null;
    private String encodedString = null;
    private String execID   =   null;
    private String indexID	=	"-1";//will only be used in CIFSIndexingService
    private TaskKPI taskKpi = new TaskKPI();
    private static Logger logger    =   Logger.getLogger(CIFSTask.class);
    
    public CIFSFile getCIFSFile() {
        return this.cFile;
    }
    
    public CIFSTask(CIFSFile cFile, String execID) throws IOException {
        if(uid == null)
            uid = UUID.randomUUID();
        this.cFile    =   cFile;
        this.execID   = execID;
    }

    public String getIndexID() {
		return indexID;
	}

	public void setIndexID(String indexID) {
		this.indexID = indexID;
	}

	public String getExecID() {
        return this.execID;
    }
    
    public String getDestPath() {
        return null;    //This is required in case of DataExport in RepoTask.class only
    }
    
    public String getDocName() {
        return this.getCIFSFile().getFileObj().getName();
    }
    
    public String getEncodedString() {
        return encodedString;
    }

    public void setEncodedString(String encodedString) {
        this.encodedString = encodedString;
    }

    public String getSecureRepoPath() {
        return repoPath;
    }

    public void setSecureRepoPath(String repoPath) {
        this.repoPath = repoPath;
    }

    public UUID getUid() {
        return uid;
    }

    public void setUid(UUID uid) {
        this.uid = uid;
    }
    
    public TaskKPI getTaskKpi() {
        return taskKpi;
    }

    public void setTaskKpi(TaskKPI taskKpi) {
        this.taskKpi = taskKpi;
    }
    
    public Date getLastAccessedDate() {
        return new Date(this.cFile.getFileObj().getLastAccess());
    }
    
    public Date getCreationDate() {
        Date   date =   null;
        try {
            date    =   new Date(this.cFile.getFileObj().createTime());
            
        } catch (SmbException se){
            logger.info("Error: " + se.getMessage());
            date    =   null;
        }
        return date;
    }
    
    public Date getLastModifiedDate() {
        return new Date(this.cFile.getFileObj().getLastModified());
    }
    
    public String getOwner() {
        String owner    =   "";
        /*
        try {
            SID sid   =   this.cFile.getFileObj().getOwnerUser(true);
            owner     =   sid.getAccountName();
        }catch(Exception e) {}
        
        if(owner==null || "".equals(owner.trim()) || owner.isEmpty()) {
        */
        owner   =   "unknown";
        //}
        
        return owner;
    }
    
    public String getURIPath() {
        return this.cFile.getFileObj().getPath();
    }
    
    public String getPathStr() {
        return this.cFile.getFileObj().getPath();
    }
    
    public String getNodeHeirarchy() {
        String nodeHeirarchy = this.getPathStr();
        
        //Remove the string before @ SIGN
        nodeHeirarchy       =   nodeHeirarchy.substring(nodeHeirarchy.indexOf("@")+1);
        nodeHeirarchy       =   nodeHeirarchy.replace(":", "").replace("\\\\", "").replace("\\", "/");
        
        return nodeHeirarchy;
    }
    
    /* Removed from BuiltIn Agent, to improve performance
    public DataHandler getDataHandler() throws IOException {
        
        DataSource dataSource  =   null;
        try {
            
            InputStream is  =   this.cFile.getFileObj().getInputStream();
            dataSource      =   new CIFSDataSource(is);
            
        } catch  (Exception se) {
            throw new IOException("Cant read SmbFileInputStream for " + this.getPathStr());
        }
        
        return new DataHandler(dataSource);
    }*/
        
    public Acl getACL(String documentPath) throws IOException {
        Acl acl =   null;
        
        //Get the Original Security ACL
        ACE[] aceList    =   this.getCIFSFile().getFileObj().getSecurity();
        
        String mapShareInfo =   Utility.GetProp("mapShareInfo");
        if ("1".equals(mapShareInfo)) {//if has to retrieve the ShareSecurity from file
            //String mapFilePath  =   Utility.GetProp("mapFilePath");
            String mapFilePath = "/mapingFile.properties";
            URL url 	= getClass().getResource(mapFilePath);
            if(url!=null)
            	mapFilePath	=	url.getPath();
            
            //System.out.println("Full doc path: "+documentPath);
            String entryFor     =   documentPath.substring(0, Utility.nthOccurrence(documentPath, '/', 1));//
            //System.out.println("Get entry from mapFilePath: "+entryFor);
            String mapingEntry  =   Utility.getMapingEntry(mapFilePath, entryFor);
            
            //if(mapingEntry!=null) {
                //Transform the original ACL into customized ACL
                acl =   AuthorizationUtil.transformACE(aceList, mapingEntry);
            //}
        } else {//if get original ShareSecurity, instead of maping file...
            //Get the Share Security ACL
            ACE[] aceShareList    =   this.getCIFSFile().getFileObj().getShareSecurity(true);
            
            //Transform the original ACL into customized ACL
            acl =   AuthorizationUtil.transformACE(aceList, aceShareList);
        }
        
        return acl;
    }
    
    public void closeCurrentTask(boolean syncLastAccessTime) {
    	try {
    		if(syncLastAccessTime && this.getCIFSFile()!=null)
    			this.getCIFSFile().resetLastAccessTime();
    	} catch (SmbException se) {
    		logger.warn(this.getExecID() +" Task NOT closed properly");
    	}
    }
}
