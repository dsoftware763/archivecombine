/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.fileSystems;

import com.virtualcode.agent.das.dataArchiver.TaskKPI;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSFile;
//import com.virtualcode.agent.sa.Acl;
import com.virtualcode.cifs.Acl;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;
import javax.activation.DataHandler;

/**
 *
 * @author Abbas
 */
public interface FileTaskInterface {
    
    public String getEncodedString();
    public void setEncodedString(String encodedString);
    public String getSecureRepoPath();
    public void setSecureRepoPath(String repoPath);
    public UUID getUid();
    public void setUid(UUID uid);
    public TaskKPI getTaskKpi();
    public String getExecID();
    public void setTaskKpi(TaskKPI TaskKpi);
    public Acl getACL(String documentPath) throws IOException;
    public String getDocName();
    
    //public String createInternetShortcut(String shortCutAt, String shortCutTo)throws Exception;
    public CIFSFile getCIFSFile();
    //public DataHandler getDataHandler() throws IOException;
    public String getPathStr();
    public String getNodeHeirarchy();
    public String getDestPath();
    public String getOwner();
    public String getURIPath();
    public Date getLastModifiedDate();
    public Date getCreationDate();
    public Date getLastAccessedDate();
    public void closeCurrentTask(boolean syncLastAccessTime);
}
