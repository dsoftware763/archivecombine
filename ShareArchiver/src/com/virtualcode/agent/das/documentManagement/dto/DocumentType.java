/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.documentManagement.dto;

/**
 *
 * @author faisal nawaz
 */
public class DocumentType implements java.io.Serializable {

	public DocumentType() {
    }
    private static final long serialVersionUID = 1051L;

    private int id;
    private String name;
    private String value;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
    

}
