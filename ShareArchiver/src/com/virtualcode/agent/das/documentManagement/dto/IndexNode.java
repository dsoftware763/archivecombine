package com.virtualcode.agent.das.documentManagement.dto;

public class IndexNode {

	private String filePath;
	private String fileName;
	private long dateCreation;
	private long dateModification;
	private long dateLastAccess;
	private long fileSize;
	private String isArchived;
	private String allowedSids;
	private String denySids;
	private String shareAllowedSids;
	private String shareDenySids;
	
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public long getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(long dateCreation) {
		this.dateCreation = dateCreation;
	}
	public long getDateModification() {
		return dateModification;
	}
	public void setDateModification(long dateModification) {
		this.dateModification = dateModification;
	}
	public long getDateLastAccess() {
		return dateLastAccess;
	}
	public void setDateLastAccess(long dateLastAccess) {
		this.dateLastAccess = dateLastAccess;
	}
	public long getFileSize() {
		return fileSize;
	}
	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}
	public String getIsArchived() {
		return isArchived;
	}
	public void setIsArchived(String isArchived) {
		this.isArchived = isArchived;
	}
	public String getAllowedSids() {
		return allowedSids;
	}
	public void setAllowedSids(String allowedSids) {
		this.allowedSids = allowedSids;
	}
	public String getDenySids() {
		return denySids;
	}
	public void setDenySids(String denySids) {
		this.denySids = denySids;
	}
	public String getShareAllowedSids() {
		return shareAllowedSids;
	}
	public void setShareAllowedSids(String shareAllowedSids) {
		this.shareAllowedSids = shareAllowedSids;
	}
	public String getShareDenySids() {
		return shareDenySids;
	}
	public void setShareDenySids(String shareDenySids) {
		this.shareDenySids = shareDenySids;
	}
	
	
}
