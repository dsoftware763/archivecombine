package com.virtualcode.agent.das.dataRestorer;

import com.virtualcode.agent.das.dataArchiver.EvalutorFlag;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSFile;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSTask;
import com.virtualcode.agent.das.utils.Utility;
//import com.virtualcode.agent.sa.export.Document;
import com.virtualcode.agent.das.utils.GlobMatch;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.documentManagement.dto.DocumentType;

import com.virtualcode.agent.das.threadsHandler.RestoreTPEList;
import com.virtualcode.agent.das.utils.MyHashMap;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicLong;

import jcifs.smb.SmbFile;

import org.apache.log4j.Logger;

/**
 * @author Abbas
 */
public class RestoreEvaluater implements Runnable {

    private Logger logger = null;
    private Logger loginfo	=	null;

    public RestoreEvaluater(CIFSTask task) {
        this.task = task;
        logger  =   LoggingManager.getLogger(LoggingManager.INDX_ACTIVITY_DETAIL, this.task.getExecID());
        loginfo	=   LoggingManager.getLogger(LoggingManager.PUBLIC_MSGS, this.task.getExecID());
    }
    public FileTaskInterface getFileTask() {
        return this.task;
    }
    
    //private final static AtomicLong executionCount = new AtomicLong(0);
    private static MyHashMap executionCtr=    new MyHashMap();
    
    private CIFSTask task = null;
    String extensions = "";
    private Long docAge = 0L;
    private Long docSize = 0L;
    private Long docLM = 0L;
    private Long docLA = 0L;
    private boolean ProcessHD = false;
    private boolean ProcessRO = false;

    private boolean Process() throws Exception {

        //Path file = task.getPath();
        //Path name = file.getFileName();
        CIFSFile    cFile   =   this.task.getCIFSFile();
        String cFileName    =   cFile.getFileObj().getName();
        String cFilePath    =   cFile.getFileObj().getPath();
        
        logger.info(task.getUid().toString() + " : Evaluating : " + cFilePath);
        GlobMatch matcher   =   new GlobMatch();
        String[] exts = extensions.split(",");
        Exception ex = null;

        if (exts.length == 0) {
            exts = "*.*,*.*".split(",");
        }

        for (String s : exts) {
            if (s == null || s.isEmpty()) {
                /////////   rp.skippedNullEmpty += 1;
                continue;
            }
            
            //matcher = FileSystems.getDefault().getPathMatcher("glob:" + s);
            if (cFileName != null && matcher.match(cFileName, s)) {
                logger.info(task.getUid().toString() + " : Matched : " + s);
                task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND;
                if (cFileName.isEmpty()
                        || !cFileName.endsWith(".url")//dont skip (only if its a stub)
                        ) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_TEMPORARY;
                    logger.info("Not a stub OR Temporary File OR Original File : " + cFileName);
                    return true;
                }

                //BasicFileAttributes attrs = task.getAttributes();
                SmbFile smbFile =   cFile.getFileObj();
                if (smbFile.isDirectory()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_ISDIR;
                    logger.info("Is a Directory " + cFilePath);
                    return true;
                }

                if (!ProcessHD && smbFile.isHidden()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_HIDDENLECTED;
                    logger.info("Is a Hidden File " + cFilePath);
                    return true;
                }

                if (!ProcessRO && !smbFile.canWrite()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_READ_ONLY;
                    logger.info("Is a Readonly File " + cFilePath);
                    return true;
                }

                /*if (docSize > 0 && smbFile.length() < docSize) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_SMALL_SIZE;
                    logger.info("File Size : ( " + smbFile.length() + " ) " + " Required Size : ( " + docSize + " ) ");
                    return true;
                } else*/ if (docAge > 0 && smbFile.createTime() > docAge) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_AGE;
                    logger.info("File Creation Time : ( " + smbFile.createTime() + " ) " + " Required Time : ( " + docAge + " ) ");
                    return true;
                } else if (docLA > 0 && smbFile.getLastAccess() > docLA) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_LASTACCESSTIME;
                    logger.info("File Last Access Time : ( " + smbFile.getLastAccess() + " ) " + " Required Time : ( " + docLA + " ) ");
                    return true;
                } else if (docLM > 0 && smbFile.getLastModified() > docLM) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_LASTMODIFIEDTIME;
                    logger.info("File Last Modified Time : ( " + smbFile.getLastModified() + " ) " + " Required Size : ( " + docLM + " ) ");
                    return true;
                } else {
                    return true;
                }
            }
        }
        return false;
    }

    public static void resetExecutionCtr(String execID) {
        executionCtr.put(execID, new AtomicLong(0));
    }
    public static String getExecutionCount(String execID) {
        return executionCtr.get(execID);
    }

    @Override
    public void run() {
        try {
            task.getTaskKpi().evalutionStartTime = System.currentTimeMillis();

            Job aJobDetail = RestoreExecutors.currentJobs.get(task.getExecID());
            Iterator newIterator = aJobDetail.getPolicy().getDocumentTypeSet().iterator();
            while (newIterator.hasNext()) {
                DocumentType documentType = (DocumentType) newIterator.next();
                extensions = extensions + "*" + documentType.getValue() + ",";
            }
            if (extensions.isEmpty()) {
                extensions = "*";
            }

            String dAge = aJobDetail.getPolicy().getDocumentAge();
            String laDate = aJobDetail.getPolicy().getLastAccessedDate();
            String lmDate = aJobDetail.getPolicy().getLastModifiedDate();
            String sz = aJobDetail.getPolicy().getSizeLargerThan();
            docAge = Utility.calculateMilliSec(Utility.parseYear(dAge), Utility.parseMonth(dAge));
            docLM = Utility.calculateMilliSec(Utility.parseYear(lmDate), Utility.parseMonth(lmDate));
            docLA = Utility.calculateMilliSec(Utility.parseYear(laDate), Utility.parseMonth(laDate));
            docSize = Utility.calculateSize(sz);
            ProcessRO = aJobDetail.isProcessReadOnly();
            ProcessHD = aJobDetail.isProcessHidden();
            boolean p = Process();
            task.getTaskKpi().evalutionEndTime = System.currentTimeMillis();

            if (!p && task.getTaskKpi().evaluatorFlag != EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND) {
                task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_EXTENSION;
            }

            //If current file is ready to be Restored
            if (task.getTaskKpi().evaluatorFlag == EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND) {
                
            	// here it is hte size of STUB file.. so here we setting it for EvaluateRestore type job only
            	// in case of Restore execution it will be overwritten by FileRestorer.class
            	//task.getTaskKpi().setFileSize(this.task.getCIFSFile().getFileObj().length());
            }
            
            if (task.getTaskKpi().evaluatorFlag == EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND
                    && (aJobDetail.getActionType().toLowerCase().trim().equals("restore"))) {
            	
                //logger.info(task.getUid() + " Generating Restores ");
            	RestoreTPEList.startRestorer(task.getExecID(), task);
                
            } else {            	
                logger.info(task.getUid() + " Sending to Task Statistic ");
                RestoreTPEList.startStatCalculator(task.getExecID(), task);
            }
            
        } catch (Exception ex) {
        	loginfo.error("Restore Evaluater failed : " + task.getUid()+" : "+ex.getMessage()+" : "+((task.getPathStr()!=null && task.getPathStr().contains("@"))?task.getPathStr().substring(task.getPathStr().lastIndexOf('@')):task.getPathStr()));
        	
            task.getTaskKpi().errorDetails = ex;
            task.getTaskKpi().failEvaluation = true;
            RestoreTPEList.startStatCalculator(task.getExecID(), task);
            
        } finally {
            executionCtr.increment(task.getExecID());
        }
    }
}