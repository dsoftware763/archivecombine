package com.virtualcode.agent.das.dataRestorer;

import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.archivePolicy.dto.JobStatistics;
import com.virtualcode.agent.das.dataArchiver.TaskKPI;
import com.virtualcode.agent.das.dataExporter.ExportExecutors;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.utils.MyHashMap;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Formatter;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.log4j.Logger;

public class TaskStatisticsRestore implements Runnable {

    //private final static AtomicLong executionCount = new AtomicLong(0);
    //private final static AtomicLong failedCount = new AtomicLong(0);
    private static MyHashMap comulativeSize =   new MyHashMap();
    private static MyHashMap executionCtr =   new MyHashMap();
    private static MyHashMap failedCtr =   new MyHashMap();
    
    private FileTaskInterface task = null;
    Logger errorLog = null;
    Logger taskSummary = null;

    public static void resetExecutionCtr(String execID) {
        executionCtr.put(execID, new AtomicLong(0));
    }
    public static String getExecutionCount(String execID) {
        return executionCtr.get(execID);
    }

    public static void resetFailedCtr(String execID) {
        failedCtr.put(execID, new AtomicLong(0));
    }
    public static String getFailedCount(String execID) {
        return failedCtr.get(execID);
    }

    public static void resetComulativeSize(String execID) {
    	comulativeSize.put(execID, new AtomicLong(0));
    }
    public static String getComulativeSize(String execID) {
        return comulativeSize.get(execID);
    }

    public TaskStatisticsRestore(FileTaskInterface task) {
        this.task = task;
        errorLog = LoggingManager.getLogger(LoggingManager.INDX_ACTIVITY_ERROR, this.task.getExecID());
        taskSummary = LoggingManager.getLogger(LoggingManager.INDX_TASK_SUMMARY, this.task.getExecID());
    }

    @Override
    public void run() {
        TaskKPI taskKPI =   task.getTaskKpi();
        
        AtomicLong	tempSize	=	new AtomicLong(Long.parseLong(comulativeSize.get(task.getExecID())));
        if(task.getTaskKpi().isExported)//if Restoreed
        	tempSize.addAndGet(task.getTaskKpi().getFileSize());
        comulativeSize.put(task.getExecID(), tempSize);//Store it into Static var, to be used in JobSummary
        
        Job j   			=   RestoreExecutors.currentJobs.get(task.getExecID());
        JobStatistics   s	=   j.getJobStatistics();
        s.setArchivedVolume(tempSize.longValue());
        s.update(taskKPI);

        StringBuilder sb = new StringBuilder();
        Formatter formatter = new Formatter(sb, Locale.US);

        formatter.format("{ID:%s},{IndxEval: %d secs},{FSize: %d},{Indxd: %d secs}",
                task.getUid().toString(),
                (task.getTaskKpi().evalutionEndTime - task.getTaskKpi().evalutionStartTime) / 1000,
                (task.getTaskKpi().getFileSize()),
                (task.getTaskKpi().exportEndTime - task.getTaskKpi().exportStartTime) / 1000);

        taskSummary.info(sb.toString());
                
        if (task.getTaskKpi().errorDetails != null) {
            failedCtr.increment(task.getExecID());
            Writer writer = new StringWriter();
            PrintWriter printWriter = new PrintWriter(writer);
            task.getTaskKpi().errorDetails.printStackTrace(printWriter);
            
            errorLog.info("F : " + task.getUid().toString() + " : " + task.getTaskKpi().errorDetails.getMessage() + "\n" + writer.toString());
            writer = null;
            printWriter = null;
            
        }
        executionCtr.increment(task.getExecID());
        task.closeCurrentTask(false);
        task = null;
    }
}
