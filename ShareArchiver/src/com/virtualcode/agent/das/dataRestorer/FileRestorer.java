package com.virtualcode.agent.das.dataRestorer;

import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Logger;

import com.virtualcode.agent.das.fileSystems.cifs.CIFSTask;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.threadsHandler.RestoreTPEList;
import com.virtualcode.agent.das.utils.CommonJobUtils;
import com.virtualcode.agent.das.utils.MyHashMap;
import com.virtualcode.agent.das.utils.Utility;

/**
 *
 * @author Abbas
 */
public class FileRestorer implements Runnable {

    public FileRestorer(CIFSTask task) {
        this.task = task;
        logger  =   LoggingManager.getLogger(LoggingManager.INDX_ACTIVITY_DETAIL, this.task.getExecID());
        loginfo	=	LoggingManager.getLogger(LoggingManager.INDX_PUBLIC_MSGS, this.task.getExecID());
    }

    private static MyHashMap executionCtr =   new MyHashMap();
    
    private CIFSTask task = null;
    private Logger logger = null;
    private Logger loginfo	=	null;
    
    private boolean Process() {
        boolean output  =   false;

    	String filePath	=	task.getCIFSFile().getFileObj().getPath();
    	filePath		=	(filePath.endsWith(".url"))? filePath.substring(0, filePath.lastIndexOf("/")) : filePath;
		String fileName	=	task.getCIFSFile().getFileObj().getName();
		
		CommonJobUtils	cju	=	new CommonJobUtils(logger, loginfo, task);
		String repoPath		=	cju.readDownloadPathFromStub(task.getCIFSFile().getFileObj());	
        output				=	cju.fetchContentsForStub(repoPath, 
        		filePath, 
        		null);//In restore case, the instance of Document VO is Null

        return output;
    }
    
    public static void resetExecutionCtr(String execID) {
        executionCtr.put(execID, new AtomicLong(0));
    }
    public static String getExecutionCount(String execID) {
        return executionCtr.get(execID);
    }

    @Override
    public void run() {

        try {
            task.getTaskKpi().exportStartTime = System.currentTimeMillis();
            String authString = "username=" + Utility.GetProp("AgentLogin") + "&password=" + Utility.GetProp("AgentPassword") + "";
            task.setEncodedString(new String(Utility.encodetoBase64(authString.getBytes())) );
             
            task.getTaskKpi().isExported    =   Process();
            task.getTaskKpi().exportEndTime = System.currentTimeMillis();

            if (task.getTaskKpi().isExported) {//if Restoreed
                RestoreTPEList.startStatCalculator(task.getExecID(), task);
                
            } else {
            	
                throw new Exception("Unable to Restore the file");
            }

        } catch (Exception ex) {
        	loginfo.error("Data analyzer couldnt read file "+((task.getCIFSFile().getFileObj().getPath()!=null &&task.getCIFSFile().getFileObj().getPath().contains("@"))?task.getCIFSFile().getFileObj().getPath().substring(task.getCIFSFile().getFileObj().getPath().lastIndexOf('@')):task.getCIFSFile().getFileObj().getPath()));
        	
            task.getTaskKpi().failedExporting = true;
            task.getTaskKpi().errorDetails = ex;
            RestoreTPEList.startStatCalculator(task.getExecID(), task);
            
        } finally {
            executionCtr.increment(task.getExecID());
        }
    }
}