package com.virtualcode.agent.das.dataRestorer;

import java.io.IOException;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.archivePolicy.dto.Job;

import org.apache.log4j.Logger;
/**
 *
 * @author YAwar
 */
public class RestoreThread implements Runnable {
    
    private String executionID  =   null;
    private Job j   =   null;
    public RestoreThread(Job j, String execID) {
        this.executionID =   execID;
        this.j= j;
    }
    
    @Override
    public void run() {
        Logger logger = LoggingManager.getLogger(LoggingManager.INDX_ACTIVITY_ERROR, this.executionID);

        try {
            if (j == null) {
                throw new Exception("Restore Job is null, Ops");
            }

            //execute the Job, in this newly created Thread for the Job
            RestoreExecutors ee = new RestoreExecutors();
            ee.startJob(j, this.executionID);
            ee = null;

        } catch (Exception ex) {

            logger.fatal("RestoreThread : " + ex);
            LoggingManager.printStackTrace(ex, logger);

        } finally {
        }
    }
}
