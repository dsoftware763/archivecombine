package com.virtualcode.agent.das.dataRestorer;

import com.virtualcode.agent.das.Executors.DataLayer;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.agent.das.archivePolicy.dto.Job;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexDeletionPolicy;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.KeepOnlyLastCommitDeletionPolicy;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
/**
 *
 * @author Abbas
 */
public class RestoreStarter implements Runnable {
    
    private String AgentName    =   null;
    private String actionType   =   null;
    private static Logger logger    =   Logger.getLogger(RestoreStarter.class);
    
    public static ArrayList<String> underProcessJobsList   =   new ArrayList<String>();
    private static List<Integer> interuptedJobIDsList  =   null;
    
    public RestoreStarter(String AgentName) {
        this.AgentName  =   AgentName;
        this.actionType =   "RESTORE";
    }
    
    @Override
    public void run() {
        try {
            
            Thread.currentThread().sleep(12 * 1000);// 12 sec
            logger.info(AgentName + " waiting for Restore Job...");
            
            while (true) {
                try {
                    
                    List<Job> jobsList  =   DataLayer.JobsbyAgent(AgentName, actionType);
                    if(jobsList!=null && !jobsList.isEmpty() && jobsList.size()>0) {
                        
                        logger.info(jobsList.size() + " new Restore Jobs Found.");
                        
                        interuptedJobIDsList  =   DataLayer.interuptedJobs();
                        logger.info("Interupted number of Jobs: "+interuptedJobIDsList.size());
                        
                        for (Job j : jobsList) {
                            int jID = j.getId();
                            
                            logger.info("Restore Job ID : " + jID);
                            if(underProcessJobsList.contains(jID+"")) {
                                logger.info("Already processing: "+jID);
                                
                            } else if(interuptedJobIDsList.contains(jID+"")) {
                                logger.warn("Job is Canceled / Interupted: "+jID);
                                
                            } else {
                                underProcessJobsList.add(jID+"");
                                String executionID = DataLayer.executionStarted(jID);
                                logger.info("Restore Execution ID : " + executionID);

                                LoggingManager.configureLogger(jID, executionID,LoggingManager.INDX_PUBLIC_MSGS);
                                LoggingManager.configureLogger(jID, executionID,LoggingManager.INDX_ACTIVITY_DETAIL);
                                LoggingManager.configureLogger(jID, executionID,LoggingManager.INDX_ACTIVITY_ERROR);
                                LoggingManager.configureLogger(jID, executionID,LoggingManager.INDX_JOB_SUMMARY);
                                LoggingManager.configureLogger(jID, executionID,LoggingManager.INDX_TASK_SUMMARY);

                                RestoreThread et =   new RestoreThread(j, executionID);
                                Thread exp  =   new Thread(et);
                                exp.start();
                            }
                            logger.info("Seeking next Restore Job...");
                        }

                    } else {
                        logger.info("New Restore Job Not Found");
                    }
                } catch(Exception ex) {//if WS is down at Server side, then dont' Terminate...
                    ex.getMessage();
                    ex.printStackTrace();
                    LoggingManager.printStackTrace(ex, logger);
                    
                } finally {
                    /*
                    fileEvaluator = null;
                    fileExporter = null;
                    statisticsCalculator = null;
                     */
                }
                Thread.currentThread().sleep(1000 * 40);//wait for 40sec
            }
            
        } catch (InterruptedException ex) {//if Thread.sleep is interupted
            ex.getMessage();
            ex.printStackTrace();
            LoggingManager.printStackTrace(ex, logger);
        }
    }
    
    public static boolean isInterupted(String execID) {
        
        String curJobID =   RestoreExecutors.currentJobs.get(execID).getId()+"";
        boolean isInterupted =   false;
        
        synchronized(RestoreStarter.class) {
            isInterupted =   RestoreStarter.interuptedJobIDsList.contains(curJobID);
        }
        if(isInterupted)
            logger.warn("jobID("+curJobID+") is Interupted by User while executing: "+execID);
        
        return isInterupted;
    }
}