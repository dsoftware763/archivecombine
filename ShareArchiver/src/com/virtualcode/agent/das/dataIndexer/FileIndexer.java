package com.virtualcode.agent.das.dataIndexer;

import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.documentManagement.dto.IndexNode;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSTask;
import com.virtualcode.agent.das.threadsHandler.IndexingTPEList;
import com.virtualcode.agent.das.utils.MyHashMap;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.cifs.Acl;
import com.virtualcode.cifs.CifsUtil;
import com.virtualcode.common.CustomProperty;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.log4j.Logger;
import org.apache.lucene.index.IndexWriter;

/**
 *
 * @author Abbas
 */
public class FileIndexer implements Runnable {

    public FileIndexer(CIFSTask task) {
        this.task = task;
        logger  =   LoggingManager.getLogger(LoggingManager.INDX_ACTIVITY_DETAIL, this.task.getExecID());
        loginfo	=	LoggingManager.getLogger(LoggingManager.INDX_PUBLIC_MSGS, this.task.getExecID());
    }

    private static MyHashMap executionCtr =   new MyHashMap();
    
    private CIFSTask task = null;
    private Logger logger = null;
    private Logger loginfo	=	null;

    private boolean Process() {
        boolean output  =   false;
        
        //get the writer, from initiated Pool
        IndexWriter writer	=	IndexerManager.getIndexWriter(task.getIndexID());
		//int originalNumDocs = writer.numDocs();
        
        //get the ShareAnalysisStats
        StatsAnalyzer saStats	=	IndexerManager.getShareAnalysisStats(task.getIndexID());
        
		try {
			IndexNode iNode	=	new IndexNode();

			String filePath	=	task.getCIFSFile().getFileObj().getPath();
			String fileName	=	task.getCIFSFile().getFileObj().getName();
			boolean isArchived	=	false;
			if(fileName.endsWith(".url")) {//also index the stubs
				isArchived	=	true;
			}
			
			//logger.info(this.task.getUid().toString() +" filePath: " + filePath);
			iNode.setFilePath(filePath);
			
			//logger.info(this.task.getUid().toString() +" fileName: " + fileName);
			iNode.setFileName(fileName);
			
			//logger.info(this.task.getUid().toString() +" dateCreation: " + task.getCreationDate().getTime());
			iNode.setDateCreation(task.getCreationDate().getTime());
			
			//logger.info(this.task.getUid().toString() +" dateModification: " +task.getLastModifiedDate().getTime());
			iNode.setDateModification(task.getLastModifiedDate().getTime());
			
			//logger.info(this.task.getUid().toString() +" dateLastAccess: " + task.getLastAccessedDate().getTime());
			iNode.setDateLastAccess(task.getLastAccessedDate().getTime());
			
			//logger.info(this.task.getUid().toString() +" fileSize: " +  task.getCIFSFile().getFileObj().length());
			iNode.setFileSize(task.getCIFSFile().getFileObj().length());
			
			//logger.info(this.task.getUid().toString() +" isArchived: " +  isArchived);
			iNode.setIsArchived(isArchived+"");
			
			CifsUtil	cifsUtil	=	new CifsUtil();
			Acl	acl	=	cifsUtil.getACL(task.getCIFSFile().getFileObj());
			if(acl!=null) {
				HashMap<String, String> aclAttribs	=	CifsUtil.getAclAttribs(acl);
				
				//logger.info(this.task.getUid().toString() +" VC_ALLOWED_SIDS: " +  aclAttribs.get(CustomProperty.VC_ALLOWED_SIDS));
				iNode.setAllowedSids(aclAttribs.get(CustomProperty.VC_ALLOWED_SIDS));
				iNode.setDenySids(aclAttribs.get(CustomProperty.VC_DENIED_SIDS));
				iNode.setShareAllowedSids(aclAttribs.get(CustomProperty.VC_S_ALLOWED_SIDS));
				iNode.setShareDenySids(aclAttribs.get(CustomProperty.VC_S_DENIED_SIDS));
			}
			//finally add it into IndexWriter
			IndexerManager.addDocument(writer, iNode);
			
			// also update the Stats - for summary/reporting
			if(saStats!=null)
				saStats.updateStats(iNode);
			else
				logger.info(this.task.getUid().toString() +" saStats not exist for Indexing Counts...");
			
			output	=	true;
			logger.info(this.task.getUid().toString() +" Added: " + fileName);
			
		} catch (Exception e) {
			loginfo.error("Data analyzer couldnt read file "+((task.getCIFSFile().getFileObj().getPath()!=null && task.getCIFSFile().getFileObj().getPath().contains("@"))?task.getCIFSFile().getFileObj().getPath().substring(task.getCIFSFile().getFileObj().getPath().lastIndexOf('@')):task.getCIFSFile().getFileObj().getPath()));
			
			logger.error(this.task.getUid().toString() +" : "+ e.getMessage());
            e.printStackTrace();
            output  =   false;
		}
		//int newNumDocs = writer.numDocs();
		
        return output;
    }
    
    public static void resetExecutionCtr(String execID) {
        executionCtr.put(execID, new AtomicLong(0));
    }
    public static String getExecutionCount(String execID) {
        return executionCtr.get(execID);
    }

    @Override
    public void run() {

        try {
            task.getTaskKpi().exportStartTime = System.currentTimeMillis();
            String authString = "username=" + Utility.GetProp("AgentLogin") + "&password=" + Utility.GetProp("AgentPassword") + "";
            task.setEncodedString(new String(Utility.encodetoBase64(authString.getBytes())) );
             
            task.getTaskKpi().isExported    =   Process();
            task.getTaskKpi().exportEndTime = System.currentTimeMillis();

            if (task.getTaskKpi().isExported) {//if Indexed
                IndexingTPEList.startStatCalculator(task.getExecID(), task);
                
            } else {
            	
                throw new Exception("Unable to index the file");
            }

        } catch (Exception ex) {
        	loginfo.error("Data analyzer couldnt read file "+((task.getCIFSFile().getFileObj().getPath()!=null &&task.getCIFSFile().getFileObj().getPath().contains("@"))?task.getCIFSFile().getFileObj().getPath().substring(task.getCIFSFile().getFileObj().getPath().lastIndexOf('@')):task.getCIFSFile().getFileObj().getPath()));
        	
            task.getTaskKpi().failedExporting = true;
            task.getTaskKpi().errorDetails = ex;
            IndexingTPEList.startStatCalculator(task.getExecID(), task);
            
        } finally {
            executionCtr.increment(task.getExecID());
        }
    }
}