package com.virtualcode.agent.das.dataIndexer;

import com.virtualcode.agent.das.Executors.DataLayer;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.agent.das.fileSystems.FileServiceInterface;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.archivePolicy.dto.SPDocLib;
import com.virtualcode.agent.das.archivePolicy.dto.ExcludedPath;
import com.virtualcode.agent.das.documentManagement.dto.DocumentType;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSIndexingService;
import com.virtualcode.agent.das.threadsHandler.IndexingTPEList;
import com.virtualcode.agent.das.utils.MyHashMap;
import com.virtualcode.services.DriveLettersService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.services.ShareAnalysisStatsService;
import com.virtualcode.util.SpringApplicationContext;
import com.virtualcode.vo.DriveLetters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.jsf.FacesContextUtils;

/**
 *
 * @author Abbas
 */
public class IndexingExecutors {

    public static HashMap<String, Job> currentJobs = new HashMap<String, Job>();
    
    private static MyHashMap fileWalkedCtr    =   new MyHashMap();
    
    void startJob(Job job, String executionID) throws InterruptedException, Exception {
    	
        Logger logger = LoggingManager.getLogger(LoggingManager.INDX_JOB_SUMMARY, executionID);
        Logger loginfo	=	LoggingManager.getLogger(LoggingManager.INDX_PUBLIC_MSGS, executionID);
        
        int errorCodeID = 0;
        int jID = -1;
        
        //initialize the counters with 0
        fileWalkedCtr.put(executionID, new AtomicLong(0));
        IndexEvaluater.resetExecutionCtr(executionID);
        FileIndexer.resetExecutionCtr(executionID);
        TaskStatisticsIndex.resetExecutionCtr(executionID);
        TaskStatisticsIndex.resetFailedCtr(executionID);
        TaskStatisticsIndex.resetComulativeSize(executionID);

        Job currentJob  =   null;
        try {          
            jID = (int) job.getId();

            currentJobs.put(executionID, job);
            currentJob  =   currentJobs.get(executionID);
            currentJob.getJobStatistics().setJobStartTime(new Date());
            
            String authString   = "username=" + Utility.GetProp("AgentLogin") + "&password=" + Utility.GetProp("AgentPassword") + "";
            String secureInfo   =   new String(Utility.encodetoBase64(authString.getBytes()));
            logger.info("Encoded Secure Info: "+secureInfo);
            
            if (executionID == null) {
                throw new Exception("Execution ID is null");
            }
                        
            // make/initiate the ThreadPools for current Execution of an Indexing Job
            IndexingTPEList threadsPoolSet  =   new IndexingTPEList(executionID, logger);
            
            logger.info("Indexing Job Info");
            logger.info("=======");
            logger.info("Execution ID " + executionID);
            logger.info("Job Name " + currentJob.getJobName());
            logger.info("Job Type " + currentJob.getActionType());
            Iterator iterator = job.getSpDocLibSet().iterator();
            while (iterator.hasNext()) {
                SPDocLib spDocLib = (SPDocLib) iterator.next();
                logger.info("\t Path to be Indexed : " + spDocLib.getLibraryName());
                //logger.info("\t Location for Indexes: " + spDocLib.getDestinationPath());
                logger.info("\t isRecursive: " + spDocLib.isRecursive());
            }

            logger.info("job -> policy name : " + job.getPolicy().getPolicyName());
            logger.info("job -> policy -> document-types: ");
            Iterator newIterator = job.getPolicy().getDocumentTypeSet().iterator();
            while (newIterator.hasNext()) {
                DocumentType documentType = (DocumentType) newIterator.next();
                logger.info("\t documentType name: " + documentType.getValue());
            }

            for (SPDocLib p : job.getSpDocLibSet()) {
                FileServiceInterface    fs  =   null;
                if("indexing".equals(currentJob.getActionType().toLowerCase()) ) {//if this is Indexing Type Job
                	fs  =   new CIFSIndexingService(executionID, p.getId()+"");
                    
                    //Initiate IndexWriter, and load in Pool
            	    DriveLetters driveLtr	=	IndexingExecutors.getDriveLetter(p);//driveLtr can be NULL
                    IndexerManager.initIndexWriter(driveLtr, p.getId()+"", true);
                
                } else {
                    logger.error("Indexing module dont support the Job of type "+currentJob.getActionType());
                }
                
                //Prepare the list of Excluded sub-paths of current Path
                ArrayList<String> excPathList   =   new ArrayList<String>();
                for(ExcludedPath ep : p.getExcludedPathSet()) {
                    excPathList.add(ep.getPath());
                }
                fs.walkFileTree(p.getLibraryName(), excPathList, p.isRecursive());
            }
                        
            //Wait for shutting down the threads of current Execution
            threadsPoolSet.stopCurThreadPools();
            ////proceed further AFTER STOPPING THE THREAD POOL OF CUR JOB-EXEC            
            
            //Now close the IndexWriter's of all Shares...after completing the Indexes
            for (SPDocLib p : job.getSpDocLibSet()) {
	        	try {
	        		IndexerManager.closeIndexWriter(p.getId()+"");
	        	} catch(IOException ioe) {
	        		logger.error("Indexer not closed for ["+p.getId()+"] "+p.getLibraryName());
	        		LoggingManager.printStackTrace(ioe, logger);
	        	}
            }
                        
            logger.info("Files Walked : " + fileWalkedCtr.get(executionID));
            fileWalkedCtr.put(executionID, new AtomicLong(0));//reset value to 0

            logger.info("");
            logger.info("Indexer's Report");
            logger.info("=================");
            logger.info("Nodes Evaluted : " + IndexEvaluater.getExecutionCount(executionID));
            logger.info("Indexed : " + FileIndexer.getExecutionCount(executionID));
            logger.info("Failed : " + TaskStatisticsIndex.getFailedCount(executionID));
            logger.info("Summary : " + TaskStatisticsIndex.getExecutionCount(executionID));
            logger.info("Total Size : " + TaskStatisticsIndex.getComulativeSize(executionID) + " bytes");

            currentJob.getJobStatistics().setJobEndTime(new Date());
            logger.info("Calling WS for executionCompleted with ID : " + executionID);

            boolean success = DataLayer.indexExecutionCompleted(jID, executionID, errorCodeID, currentJob.getJobStatistics());
            if (success) {
                logger.info("Successfull returned from execution Completed WS : " + jID);
            } else {
                throw new Exception("Oops.Error returned from execution Completed WS : " + jID);
            }

        } catch (Exception ex) {
            errorCodeID = 1;
            if (executionID != null) {

                if (currentJob == null) {
                	loginfo.error("CODE["+errorCodeID+"]: Fatal Error in Analyzing data!");
                	
                    logger.info("Current Job is Null");
                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.indexExecutionCompleted(jID, executionID, errorCodeID, null);

                } else if (currentJob.getJobStatistics() == null) {
                	loginfo.error("CODE["+errorCodeID+"]: Fatal Error in Analyzing data!");
                	
                    logger.info("currentJob.getJobStatistics() is Null");
                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.indexExecutionCompleted(jID, executionID, errorCodeID, null);
                    
                } else {
                    currentJob.getJobStatistics().setJobEndTime(new Date());
                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.indexExecutionCompleted(jID, executionID, errorCodeID, currentJob.getJobStatistics());
                }
            }
            
            loginfo.error("CODE["+errorCodeID+"]: Fatal Error in Analyzing data! "+executionID);
            logger.fatal("ExecutionID: " + executionID);
            logger.fatal("JobID: " + jID);
            logger.fatal("ErrorID: " + errorCodeID);
            logger.fatal(ex.getMessage());
            LoggingManager.printStackTrace(ex, logger);
            LoggingManager.getLogger(LoggingManager.INDX_ACTIVITY_ERROR, executionID).error("Fatal : " + "\n" + ex.getMessage());

        } finally {
            currentJob = null;
            currentJobs.remove(executionID);
            IndexStarter.underProcessIndexJobsList.remove(jID+"");
        }
    }
    
    public static void increment(String execID) {
        fileWalkedCtr.increment(execID);
    }
    
    public static DriveLetters getDriveLetter(SPDocLib p) {
    	
    	ServiceManager serviceManager = (ServiceManager)SpringApplicationContext.getBean("serviceManager");
		DriveLettersService dlService = serviceManager.getDriveLettersService();
		
		String sharePath	=	p.getLibraryName();
//		sharePath	=	sharePath.replaceAll("[/]", "\\");
		sharePath	=	sharePath.replaceAll("/", "\\\\");
		sharePath	=	"\\\\"+sharePath.substring(sharePath.lastIndexOf("@")+1);
		
		List<DriveLetters> dlList	=	dlService.getDriveLettersBySharePath(sharePath);
		
		DriveLetters	driveLetter =	null;
		if(dlList!=null && dlList.size()>0) {
			driveLetter	=	dlList.get(0);
		}    	
    	
    	return driveLetter;
    }
}