package com.virtualcode.agent.das.dataIndexer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.virtualcode.vo.DriveLetters;

public class StatsHolderFactory {
	
	private StatsHolderFactory(){
		
	}
	
	private static List<ShareAnalysisStateHolder> statsHoldersList=Collections.synchronizedList(new ArrayList<ShareAnalysisStateHolder>());
	
	private static StatsHolderFactory factory;
	
	public static StatsHolderFactory getFactory(){
		if(factory==null){
			factory=new StatsHolderFactory();
		}
		return factory;
	}
	
	public static ShareAnalysisStateHolder getInstance(DriveLetters drvLtr){
		boolean exists=false;
		ShareAnalysisStateHolder _instance=null;
		for(int i=0;i<statsHoldersList.size();i++){
			if(statsHoldersList.get(i).getDriveLetter().getId().intValue()==drvLtr.getId().intValue()){
				_instance=statsHoldersList.get(i);
				exists=true;break;
			}			
		}
		if(!exists){
			_instance= new ShareAnalysisStateHolder(drvLtr);
			statsHoldersList.add(_instance);
		}		
		return _instance;		
	}
	
	public static  boolean disposeInstane(ShareAnalysisStateHolder statsHolder){
		return statsHoldersList.remove(statsHolder);		
	}
	
	public static List<ShareAnalysisStateHolder> getAllStateHolders(){
		return statsHoldersList;
	}
	
		
}
