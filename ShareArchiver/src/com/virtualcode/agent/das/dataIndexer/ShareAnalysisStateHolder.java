package com.virtualcode.agent.das.dataIndexer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.virtualcode.services.DocumentClassifyService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.util.SpringApplicationContext;
import com.virtualcode.vo.DocumentCategory;
import com.virtualcode.vo.DriveLetters;
import com.virtualcode.vo.ShareAnalysisStats;

public class ShareAnalysisStateHolder {
	
		
	private List<ShareAnalysisStats> statObjList;
	private List<DocumentCategory> docCatList=Collections.synchronizedList(new ArrayList<DocumentCategory>());
	private DriveLetters driveLetter;
	
	public ShareAnalysisStateHolder(DriveLetters drvLtr){
		statObjList=Collections.synchronizedList(new ArrayList<ShareAnalysisStats>());
		driveLetter=drvLtr;
		createShareCategoryInstances();
	}
	
			
	private void createShareCategoryInstances(){
		ServiceManager serviceManager=(ServiceManager)SpringApplicationContext.getBean("serviceManager");
		docCatList=Collections.synchronizedList(serviceManager.getDocumentClassifyService().getAllDocumentCategories());
		DocumentClassifyService docClassifyService=serviceManager.getDocumentClassifyService();
		
		statObjList.clear();
		List<String> docExtList;
		for(int i=0;i<docCatList.size();i++){
			docExtList=docClassifyService.getAllDocCategoryExts(docCatList.get(i).getId());
			docCatList.get(i).setDocExtList(docExtList);
			statObjList.add(new ShareAnalysisStats(driveLetter, docCatList.get(i).getName()));			
		}		
	}
		
	
	public ShareAnalysisStats getByCategory(String categoryName){
		for(int i=0;i<statObjList.size();i++){
			if(statObjList.get(i).getDocumentCatName().equals(categoryName)){
				return statObjList.get(i);
			}
		}
		return null;
	}

	public List<ShareAnalysisStats> getStatObjList() {
		return statObjList;
	}
	
	public List<DocumentCategory> getDocCatList() {
		return docCatList;
	}

	public DriveLetters getDriveLetter() {
		return driveLetter;
	}

	public void setDriveLetter(DriveLetters driveLetter) {
		this.driveLetter = driveLetter;
	}
	

}
