package com.virtualcode.agent.das.dataIndexer;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.NumericField;
import org.apache.lucene.index.IndexDeletionPolicy;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.KeepOnlyLastCommitDeletionPolicy;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import com.virtualcode.agent.das.documentManagement.dto.IndexNode;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.vo.DriveLetters;
import com.virtualcode.vo.ShareAnalysisStats;

public class IndexerManager {

	private static Logger logger    								=   Logger.getLogger(IndexerManager.class);
	private static HashMap<String, IndexWriter> iwPool				=	new HashMap<String, IndexWriter>();
	private static HashMap<String, StatsAnalyzer> statsPool	=	new HashMap<String, StatsAnalyzer>();

	public static class Constants {
		public static String FILE_PATH	=	"filePath";
		public static String FILE_NAME	=	"fileName";
		public static String DATE_CREATE	=	"dateCreation";
		public static String DATE_MODIFY	=	"dateModification";
		public static String DATE_ACCESS	=	"dateLastAccess";
		public static String FILE_SIZE		=	"fileSize";
		public static String IS_ARCHIVED	=	"isArchived";
		public static String ALLOWED_SIDS	=	"allowedSids";
		public static String DENY_SIDS		=	"denySids";
		public static String SHARE_ALLOWED_SIDS	=	"shareAllowedSids";
		public static String SHARE_DENY_SIDS	=	"shareDenySids";
	}
	
    public static void initIndexWriter(DriveLetters driveLetter, String indexID, boolean createNew) throws IOException {
    	
    	//load and set the Base Path to save indexes
    	String indexLocation	=	Utility.GetProp("indexesBasePath");
    	if(!indexLocation.endsWith("/"))
    		indexLocation	=	indexLocation+"/";
    	indexLocation	=	indexLocation + indexID;
    	logger.info("initializing indexWriter at: "+indexLocation+" for "+indexID);
    	
		// Create instance of Directory where index files will be stored
		Directory fsDirectory =  FSDirectory.open(new File(indexLocation));
		// Create instance of analyzer, which will be used to tokenize the input data
		Analyzer standardAnalyzer = new StandardAnalyzer(Version.LUCENE_30);

		// Create the instance of deletion policy
		IndexDeletionPolicy deletionPolicy = new KeepOnlyLastCommitDeletionPolicy(); 
		IndexWriter writer =	new IndexWriter(fsDirectory, standardAnalyzer, createNew, deletionPolicy, IndexWriter.MaxFieldLength.UNLIMITED);
		
		if(driveLetter!=null) {//if the Analysis/Indexing is for some Drive Letter entry
			StatsAnalyzer statsAnalyzer	=	new StatsAnalyzer(driveLetter);
			statsPool.put(indexID, statsAnalyzer);
		}
		
    	iwPool.put(indexID, writer);
    }
    
    public static IndexWriter getIndexWriter(String indexID) {
    	return iwPool.get(indexID);
    }
    
    public static StatsAnalyzer getShareAnalysisStats(String indexID) {
    	return statsPool.get(indexID);
    }
    
    public static void addDocument(IndexWriter writer, IndexNode iNode) throws IOException {
		Document doc = new Document();
				
		doc.add(new Field(IndexerManager.Constants.FILE_PATH, iNode.getFilePath(), 
				Field.Store.YES, Field.Index.NOT_ANALYZED));
		
		doc.add(new Field(IndexerManager.Constants.FILE_NAME, iNode.getFileName(), 
				Field.Store.YES, Field.Index.NOT_ANALYZED));
		
		NumericField	dateCreateNF	=	new NumericField(IndexerManager.Constants.DATE_CREATE, Field.Store.YES, true);
		dateCreateNF.setLongValue(iNode.getDateCreation());
		doc.add(dateCreateNF);
		//doc.add(new Field(IndexerManager.Constants.DATE_CREATE, DateTools.timeToString(iNode.getDateCreation(), DateTools.Resolution.MINUTE),
		//        Field.Store.NO, Field.Index.NOT_ANALYZED));
		
		NumericField	dateModifyNF	=	new NumericField(IndexerManager.Constants.DATE_MODIFY, Field.Store.YES, true);
		dateModifyNF.setLongValue(iNode.getDateModification());
		doc.add(dateModifyNF);
		//doc.add(new Field(IndexerManager.Constants.DATE_MODIFY, DateTools.timeToString(iNode.getDateModification(), DateTools.Resolution.MINUTE),
		//        Field.Store.NO, Field.Index.NOT_ANALYZED));
		
		NumericField	dateAccessNF	=	new NumericField(IndexerManager.Constants.DATE_ACCESS, Field.Store.YES, true);
		dateAccessNF.setLongValue(iNode.getDateLastAccess());
		doc.add(dateAccessNF);
		//doc.add(new Field(IndexerManager.Constants.DATE_ACCESS,	DateTools.timeToString(iNode.getDateLastAccess(), DateTools.Resolution.MINUTE),
		//        Field.Store.NO, Field.Index.NOT_ANALYZED));
		
		//size is to be searched as Numeric
		NumericField	nf	=	new NumericField(IndexerManager.Constants.FILE_SIZE, Field.Store.YES, true);
		nf.setLongValue(iNode.getFileSize());
		doc.add(nf);
		
		doc.add(new Field(IndexerManager.Constants.IS_ARCHIVED, iNode.getIsArchived()+"", 
				Field.Store.YES, Field.Index.NOT_ANALYZED));

		doc.add(new Field(IndexerManager.Constants.ALLOWED_SIDS, iNode.getAllowedSids(), 
				Field.Store.YES, Field.Index.NOT_ANALYZED));
		
		doc.add(new Field(IndexerManager.Constants.DENY_SIDS, iNode.getDenySids(), 
				Field.Store.YES, Field.Index.NOT_ANALYZED));

		doc.add(new Field(IndexerManager.Constants.SHARE_ALLOWED_SIDS, iNode.getShareAllowedSids(), 
				Field.Store.YES, Field.Index.NOT_ANALYZED));
		
		doc.add(new Field(IndexerManager.Constants.SHARE_DENY_SIDS, iNode.getShareDenySids(), 
				Field.Store.YES, Field.Index.NOT_ANALYZED));
		
		writer.addDocument(doc);
    }
    
    public static void closeIndexWriter(String indexID) throws IOException {
    	
    	StatsAnalyzer	statsAnalyzer	=	statsPool.get(indexID);
    	if(statsAnalyzer!=null) {
    		if(statsAnalyzer.commitInDB())
    			logger.info("StatsAnalyzer committed in DB for indexID="+indexID);
    		else
    			logger.warn("StatsAnalyzer FAILED to be committed in DB for indexID="+indexID);
    		
    		statsPool.remove(indexID);
    	} else {
    		logger.info("StatsAnalyzer was not configured for Non-DriveLetter shares, so dont keeping stats in DB");
    	}
    	
    	if(iwPool.get(indexID)!=null) {
    		iwPool.get(indexID).close();//if not null etc
    		iwPool.remove(indexID);
    	}
    }
}
