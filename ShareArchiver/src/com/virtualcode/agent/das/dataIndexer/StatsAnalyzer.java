package com.virtualcode.agent.das.dataIndexer;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.virtualcode.agent.das.documentManagement.dto.IndexNode;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.services.ShareAnalysisStatsService;
import com.virtualcode.util.SpringApplicationContext;
import com.virtualcode.vo.DocumentCategory;
import com.virtualcode.vo.DriveLetters;
import com.virtualcode.vo.ShareAnalysisStats;

public class StatsAnalyzer {

	
	private static Logger logger			=   Logger.getLogger(StatsAnalyzer.class);
	//private List<DocumentCategory> docCatList;
	private DriveLetters driveLetter=null;
	
	ShareAnalysisStateHolder stateHolder;
	public StatsAnalyzer(DriveLetters dLtr) {
		initializeServices();
		this.driveLetter=dLtr;
		stateHolder=StatsHolderFactory.getInstance(driveLetter);		
	}
	
	public void updateStats(IndexNode iNode) {
		List<String> docExtList;
		String fileExt=iNode.getFileName().toLowerCase();		
		if(fileExt.indexOf(".")!=-1){
		   fileExt=fileExt.substring(fileExt.lastIndexOf("."));
		}else{
			return;
		}
		List<DocumentCategory> docCatList=stateHolder.getDocCatList();
		boolean catExists=false;
		for(int i=0;i<docCatList.size();i++){
			docExtList=docCatList.get(i).getDocExtList();
			if(docExtList.contains(fileExt)){
				this.updateDateWise(stateHolder.getByCategory(docCatList.get(i).getName()),iNode);
				catExists=true;
				break;
			}
		}
		if(!catExists){
			System.out.println("UnRecognized category: "+fileExt);
			DocumentCategory unknownCat=new DocumentCategory();
			unknownCat.setName(fileExt);
			unknownCat.getDocExtList().add((fileExt));
			docCatList.add(unknownCat);
					  
			ShareAnalysisStats saStat=new ShareAnalysisStats(driveLetter, fileExt);			
			List<ShareAnalysisStats> statObjList=stateHolder.getStatObjList();
			
			synchronized(statObjList){
				statObjList.add(saStat);
			}
			
		}
	}	

	private void updateDateWise(ShareAnalysisStats tempStats, IndexNode iNode) {
		
		if(tempStats==null || iNode==null){
			return;
		}
		
		long dateDiffLM	=	System.currentTimeMillis() - iNode.getDateModification();
		long monthMM	=	(long)30*24*60*60*1000;
		
		logger.debug("modifDate: " + System.currentTimeMillis() + ", " + dateDiffLM + ", "+monthMM);
		double size=iNode.getFileSize();
		long fileSizeMB=Math.round(size/(1024.0*1024.0));//easy conversion without f.p
		if( dateDiffLM < monthMM *6) {
			logger.debug("[0-6] months ");
			tempStats.setLm3(tempStats.getLm3()+fileSizeMB);
			
		} else if( dateDiffLM > monthMM*6  && dateDiffLM <= monthMM*12) {
			logger.debug("Greater than 6 months");
			tempStats.setLm6(tempStats.getLm6() + fileSizeMB);
		
		} else if( dateDiffLM > monthMM*12 && dateDiffLM <= monthMM*24) {
			logger.debug("Greater than 12 months");
			tempStats.setLm12(tempStats.getLm12() + fileSizeMB);
			
		} else if( dateDiffLM > monthMM*24 && dateDiffLM<=monthMM*36) {
			logger.debug("Greater than 24 months");
			tempStats.setLm24(tempStats.getLm24() + fileSizeMB);
			
		} else if( dateDiffLM > monthMM*36) {
			logger.debug("Greater than 36 months");
			tempStats.setLm36(tempStats.getLm36() + fileSizeMB);
			
		}

		//Now for Last Accessed
		long dateDiffLA	=	System.currentTimeMillis() - iNode.getDateLastAccess();
		if( dateDiffLA < monthMM *6) {
			logger.debug("[0-6] months ");
			tempStats.setLa3(tempStats.getLa3()+fileSizeMB);
			
		} else if( dateDiffLA > monthMM*6  && dateDiffLA <= monthMM*12) {
			logger.debug("Greater than 6 months");
			tempStats.setLa6(tempStats.getLa6() + fileSizeMB);
		
		} else if( dateDiffLA > monthMM*12 && dateDiffLA <= monthMM*24) {
			logger.debug("Greater than 12 months");
			tempStats.setLa12(tempStats.getLa12() + fileSizeMB);
			
		} else if( dateDiffLA > monthMM*24 && dateDiffLA<=monthMM*36) {
			logger.debug("Greater than 24 months");
			tempStats.setLa24(tempStats.getLa24() + fileSizeMB);
			
		} else if( dateDiffLA > monthMM*36) {
			logger.debug("Greater than 36 months");
			tempStats.setLa36(tempStats.getLa36() + fileSizeMB);
			
		}

		//Now for Created Date
		long dateDiffCD	=	System.currentTimeMillis() - iNode.getDateCreation();
		if( dateDiffCD < monthMM *6) {
			logger.debug("[0-6] months ");
			tempStats.setCd3(tempStats.getCd3()+fileSizeMB);
			
		} else if( dateDiffCD > monthMM*6  && dateDiffCD <= monthMM*12) {
			logger.debug("Greater than 6 months");
			tempStats.setCd6(tempStats.getCd6() + fileSizeMB);
		
		} else if( dateDiffCD > monthMM*12 && dateDiffCD <= monthMM*24) {
			logger.debug("Greater than 12 months");
			tempStats.setCd12(tempStats.getCd12() + fileSizeMB);
			
		} else if( dateDiffCD > monthMM*24 && dateDiffCD<=monthMM*36) {
			logger.debug("Greater than 24 months");
			tempStats.setCd24(tempStats.getCd24() + fileSizeMB);
			
		} else if( dateDiffCD > monthMM*36) {
			logger.debug("Greater than 36 months");
			tempStats.setCd36(tempStats.getCd36() + fileSizeMB);
			
		}
		
		 tempStats.setTotalVolume(tempStats.getTotalVolume() + fileSizeMB);
		 logger.debug("Total Volume");
		
	}
	
	public synchronized boolean commitInDB() {
		
		 ShareAnalysisStateHolder statsHolder=StatsHolderFactory.getInstance(driveLetter);
		 List<ShareAnalysisStats> statObjList=statsHolder.getStatObjList();
		 saService.deleteByDriveId(driveLetter.getId());
		 for(int j=0;j<statObjList.size();j++){
			if(statObjList.get(j).getTotalVolume()>1)//at least 1mb size of category
			   saService.save(statObjList.get(j));
		 }	
		 StatsHolderFactory.disposeInstane(statsHolder);
		return true;
	}

	public static void main(String args[]) throws Exception {
		//Date modifDate	=	new Date();
		String mystring = "January 7, 2012";
		Date modifDate = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).parse(mystring);

		long dateDiff	=	System.currentTimeMillis() - modifDate.getTime();
		long monthMM	=	(long)30*24*60*60*1000;
		logger.debug("modifDate: " + System.currentTimeMillis() + ", " + dateDiff + ", "+monthMM);
		if( dateDiff < monthMM*3)
			System.out.println("less than 3 months");
		
		else if( dateDiff < monthMM*6)
			System.out.println("less than 6 months");
		
		else if( dateDiff < monthMM*9)
			System.out.println("less than 9 months");
		else
			System.out.println("more than 9 months");
	}
	
	private ServiceManager serviceManager=(ServiceManager)SpringApplicationContext.getBean("serviceManager");
	private ShareAnalysisStatsService saService;
	
	
	public void initializeServices(){		
		saService = serviceManager.getShareAnalysisStatsService();
		
	}
}
