/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.jobScheduler;

import com.virtualcode.agent.das.Executors.DataLayer;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.archivePolicy.dto.Job;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
/**
 *
 * @author Abbas
 */
public class JobScheduler implements Runnable {
    
    private String AgentName    =   null;
    private static Logger logger   =   Logger.getLogger(JobScheduler.class);
            
    public JobScheduler(String AgentName) {
        this.AgentName  =   AgentName;
    }
    
    @Override
    public void run() {
        try {            
            logger.info(AgentName + " launching Scheduling...");
            Thread.currentThread().sleep(14 * 1000);// 14 sec
            
            while (true) {
                try {
                    int counter	=   DataLayer.setStatusForScheduledJobs();
                    
                    if(counter>0) {
                        logger.info(counter + " Jobs at rest found");
                        
                    } else {
                        logger.info("Jobs at rest not found");
                    }
                    
                } catch(Exception ex) {//if WS is down at Server side, then dont' Terminate...
                    ex.getMessage();
                    ex.printStackTrace();
                    LoggingManager.printStackTrace(ex, logger);
                    
                }
                
                Thread.currentThread().sleep(1000 * 60);//wait for 1 min to get new Jobs List
            }
            
        } catch (InterruptedException ex) {//if Thread.sleep is interupted
            ex.getMessage();
            ex.printStackTrace();
            LoggingManager.printStackTrace(ex, logger);
        }
    }
}
