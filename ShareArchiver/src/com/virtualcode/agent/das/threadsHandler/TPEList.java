/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.threadsHandler;

import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.dataArchiver.CIFSEvaluater;
import com.virtualcode.agent.das.dataArchiver.FileArchiver;
import com.virtualcode.agent.das.dataArchiver.FileExecutors;
import com.virtualcode.agent.das.dataArchiver.FileStubber;
import com.virtualcode.agent.das.dataArchiver.JobStarter;
import com.virtualcode.agent.das.dataArchiver.TaskStatistic;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSTask;
import com.virtualcode.agent.das.utils.Utility;
import java.util.HashMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;

/**
 *
 * @author YAwar
 */
public class TPEList {
    
    final private static int maxEQSize = Utility.GetPoolProperty("EvalutorQueueSize") + 1;
    final private static int maxAQSize = Utility.GetPoolProperty("ArchiverQueueSize") + 1;
    final private static int maxSQSize = Utility.GetPoolProperty("StubberQueueSize") + 1;
    final private static int maxTQSize = Utility.GetPoolProperty("JobStatisticQueueSize") + 1;
    final private static ArrayBlockingQueue<Runnable> evalutorQueue = new ArrayBlockingQueue<Runnable>(maxEQSize);
    final private static ArrayBlockingQueue<Runnable> archiverQueue = new ArrayBlockingQueue<Runnable>(maxAQSize);
    final private static ArrayBlockingQueue<Runnable> stubberQueue = new ArrayBlockingQueue<Runnable>(maxSQSize);
    final private static ArrayBlockingQueue<Runnable> jobStatisticQueue = new ArrayBlockingQueue<Runnable>(maxTQSize);
    
    private static HashMap<String, ThreadPoolExecutor> fileEvaluatorList =   new HashMap<String, ThreadPoolExecutor>();
    private static HashMap<String, ThreadPoolExecutor> fileStubberList =   new HashMap<String, ThreadPoolExecutor>();
    private static HashMap<String, ThreadPoolExecutor> fileArchiverList =   new HashMap<String, ThreadPoolExecutor>();
    private static HashMap<String, ThreadPoolExecutor> statisticsCalculatorList =   new HashMap<String, ThreadPoolExecutor>();
    
    private Logger logger   =   null;
    
    //non-static values, for each instance of execution...
    private String curExecID   =   null;
    
    //Constructor will start anther new PoolSet, and puts its reference in already existing List
    public TPEList(String execID, Logger logger) {
    	this.logger	=	logger;
        
        this.curExecID =   execID;
        
        ThreadPoolExecutor fileEvaluator = null;
        ThreadPoolExecutor fileStubber = null;
        ThreadPoolExecutor fileArchiver = null;
        ThreadPoolExecutor statisticsCalculator = null;
        
        
        int ePoolSize = Utility.GetPoolSize("EvalutorpoolSize");
        int eMaxPoolSize = Utility.GetPoolProperty("EvalutormaxPoolSize");
        int eTimeout = (Utility.GetPoolProperty("EvalutorkeepAliveTime"));
        String eTimeoutUnitStr = Utility.GetPoolPropertyAsString("EvalutorTimeAliveUnit");
        TimeUnit eTimeoutUnit = TimeUnit.valueOf(eTimeoutUnitStr);
        ThreadPoolExecutor.CallerRunsPolicy policy = new ThreadPoolExecutor.CallerRunsPolicy();
        fileEvaluator = new ThreadPoolExecutor(
                ePoolSize,
                eMaxPoolSize,
                eTimeout,
                eTimeoutUnit,
                evalutorQueue);

        fileStubber = new ThreadPoolExecutor(
                Utility.GetPoolSize("StubberpoolSize"),
                Utility.GetPoolProperty("StubbermaxPoolSize"),
                (Utility.GetPoolProperty("StubberkeepAliveTime")),
                TimeUnit.valueOf(Utility.GetPoolPropertyAsString("StubberTimeAliveUnit")),
                stubberQueue);

        fileArchiver = new ThreadPoolExecutor(
                Utility.GetPoolSize("ArchiverpoolSize"),
                Utility.GetPoolProperty("ArchivermaxPoolSize"),
                (Utility.GetPoolProperty("ArchiverkeepAliveTime")),
                TimeUnit.valueOf(Utility.GetPoolPropertyAsString("ArchiverTimeAliveUnit")),
                archiverQueue);
        statisticsCalculator = new ThreadPoolExecutor(
                Utility.GetPoolSize("JobStatisticpoolSize"),
                Utility.GetPoolProperty("JobStatisticmaxPoolSize"),
                (Utility.GetPoolProperty("JobStatistickeepAliveTime")),
                TimeUnit.valueOf(Utility.GetPoolPropertyAsString("JobStatisticTimeAliveUnit")),
                jobStatisticQueue);

        fileEvaluator.setRejectedExecutionHandler(policy);
        fileArchiver.setRejectedExecutionHandler(policy);
        fileStubber.setRejectedExecutionHandler(policy);
        statisticsCalculator.setRejectedExecutionHandler(policy);
        
        
        fileEvaluatorList.put(execID, fileEvaluator);
        fileStubberList.put(execID, fileStubber);
        fileArchiverList.put(execID, fileArchiver);
        statisticsCalculatorList.put(execID, statisticsCalculator);
    }
    
    //Following method will Stop the Pool Set of only current instance's Execution ID
    public void stopCurThreadPools() {
        try {
        	logger.info("FileEvaluator Shutting Down ... ");
            fileEvaluatorList.get(this.curExecID).shutdown();
            long timeout = 10;
            int counter = 0;
            while (!fileEvaluatorList.get(this.curExecID).awaitTermination(timeout, TimeUnit.MINUTES)) {

            	logger.info(" FileEvaluator is waiting to sutting down for : " + timeout * counter + " mins");

            }

            logger.info("FileArchiver Shutting Down ... ");
            fileArchiverList.get(this.curExecID).shutdown();
            counter = 0;
            while (!fileArchiverList.get(this.curExecID).awaitTermination(20, TimeUnit.SECONDS)) {
            	logger.info("FileArchiver ThreadPool is waiting to sutting down for : " + timeout * counter + " mins");
            }

            logger.info("FileStubber Shutting Down ... ");
            fileStubberList.get(this.curExecID).shutdown();
            counter = 0;
            while (!fileStubberList.get(this.curExecID).awaitTermination(20, TimeUnit.SECONDS)) {
            	logger.info("FileStubber ThreadPool is waiting to sutting down for : " + timeout * counter + " mins");
            }

            logger.info("TaskSummary Shutting Down ... ");
            statisticsCalculatorList.get(this.curExecID).shutdown();
            counter = 0;
            while (!statisticsCalculatorList.get(this.curExecID).awaitTermination(20, TimeUnit.SECONDS)) {
            	logger.info("TaskSummary ThreadPool is waiting to sutting down for : " + timeout * counter + " mins");
            }
            
        } catch (Exception ie) {
            LoggingManager.printStackTrace(ie, logger);
            
        } finally {
            fileEvaluatorList.put(this.curExecID, null);
            fileStubberList.put(this.curExecID, null);
            fileArchiverList.put(this.curExecID, null);
            statisticsCalculatorList.put(this.curExecID, null);
            
            fileEvaluatorList.remove(this.curExecID);
            fileStubberList.remove(this.curExecID);
            fileArchiverList.remove(this.curExecID);
            statisticsCalculatorList.remove(this.curExecID);
        }
    }
        
    public static void startFileEvaluator(String execID, CIFSTask task) {
        
        if(!JobStarter.isInterupted(execID)) {
            fileEvaluatorList.get(execID).execute(new CIFSEvaluater(task));
            
        } else {//if interupted, than go to keep Stats for current TASK
            startStatCalculator(execID, task);
        }
    }
    
    public static void startFileStubber(String execID, FileTaskInterface task) {
        
        if(!JobStarter.isInterupted(execID)) {
            fileStubberList.get(execID).execute(new FileStubber(task));
            
        } else {//if interupted, than go to keep Stats for current TASK
            startStatCalculator(execID, task);
        }
    }
    
    public static void startFileArchiver(String execID, FileTaskInterface task) {
        
        if(!JobStarter.isInterupted(execID)) {
            fileArchiverList.get(execID).execute(new FileArchiver(task));
            
        } else {//if interupted, than go to keep Stats for current TASK
            startStatCalculator(execID, task);
        }
    }
    
    public static void startStatCalculator(String execID, FileTaskInterface task) {
        statisticsCalculatorList.get(execID).execute(new TaskStatistic(task));
    }
}
