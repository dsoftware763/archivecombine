package com.virtualcode.agent.das.threadsHandler;

import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.dataRestorer.FileRestorer;
import com.virtualcode.agent.das.dataRestorer.RestoreEvaluater;
import com.virtualcode.agent.das.dataRestorer.RestoreStarter;
import com.virtualcode.agent.das.dataRestorer.TaskStatisticsRestore;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSTask;
import com.virtualcode.agent.das.utils.Utility;
import java.util.HashMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;

/**
 *
 * @author YAwar
 */
public class RestoreTPEList {
    
    final private static int maxEQSize = Utility.GetPoolProperty("EvalutorQueueSize") + 1;
    final private static int maxAQSize = Utility.GetPoolProperty("ArchiverQueueSize") + 1;
    final private static int maxTQSize = Utility.GetPoolProperty("JobStatisticQueueSize") + 1;
    final private static ArrayBlockingQueue<Runnable> evalutorQueue = new ArrayBlockingQueue<Runnable>(maxEQSize);
    final private static ArrayBlockingQueue<Runnable> restorerQueue = new ArrayBlockingQueue<Runnable>(maxAQSize);
    final private static ArrayBlockingQueue<Runnable> jobStatisticQueue = new ArrayBlockingQueue<Runnable>(maxTQSize);
    
    private static HashMap<String, ThreadPoolExecutor> fileEvaluatorList =   new HashMap<String, ThreadPoolExecutor>();
    private static HashMap<String, ThreadPoolExecutor> fileRestorerList =   new HashMap<String, ThreadPoolExecutor>();
    private static HashMap<String, ThreadPoolExecutor> statisticsCalculatorList =   new HashMap<String, ThreadPoolExecutor>();
    
    private Logger logger   =   null;
    
    //non-static values, for each instance of execution...
    private String curExecID   =   null;
    
    //Constructor will start anther new PoolSet, and puts its reference in already existing List
    public RestoreTPEList(String execID, Logger logger) {
        this.logger =   logger;
        
        this.curExecID =   execID;
        
        ThreadPoolExecutor fileEvaluator = null;
        ThreadPoolExecutor fileRestorer = null;
        ThreadPoolExecutor statisticsCalculator = null;
                
        int ePoolSize = Utility.GetPoolSize("EvalutorpoolSize");
        int eMaxPoolSize = Utility.GetPoolProperty("EvalutormaxPoolSize");
        int eTimeout = (Utility.GetPoolProperty("EvalutorkeepAliveTime"));
        String eTimeoutUnitStr = Utility.GetPoolPropertyAsString("EvalutorTimeAliveUnit");
        TimeUnit eTimeoutUnit = TimeUnit.valueOf(eTimeoutUnitStr);
        ThreadPoolExecutor.CallerRunsPolicy policy = new ThreadPoolExecutor.CallerRunsPolicy();
        fileEvaluator = new ThreadPoolExecutor(
                ePoolSize,
                eMaxPoolSize,
                eTimeout,
                eTimeoutUnit,
                evalutorQueue);
        fileRestorer = new ThreadPoolExecutor(
                Utility.GetPoolSize("ArchiverpoolSize"),
                Utility.GetPoolProperty("ArchivermaxPoolSize"),
                (Utility.GetPoolProperty("ArchiverkeepAliveTime")),
                TimeUnit.valueOf(Utility.GetPoolPropertyAsString("ArchiverTimeAliveUnit")),
                restorerQueue);
        statisticsCalculator = new ThreadPoolExecutor(
                Utility.GetPoolSize("JobStatisticpoolSize"),
                Utility.GetPoolProperty("JobStatisticmaxPoolSize"),
                (Utility.GetPoolProperty("JobStatistickeepAliveTime")),
                TimeUnit.valueOf(Utility.GetPoolPropertyAsString("JobStatisticTimeAliveUnit")),
                jobStatisticQueue);

        fileEvaluator.setRejectedExecutionHandler(policy);
        fileRestorer.setRejectedExecutionHandler(policy);
        statisticsCalculator.setRejectedExecutionHandler(policy);
                
        fileEvaluatorList.put(execID, fileEvaluator);
        fileRestorerList.put(execID, fileRestorer);
        statisticsCalculatorList.put(execID, statisticsCalculator);
    }
    
    //Following method will Stop the Pool Set of only current instance's Execution ID
    public void stopCurThreadPools() {
        try {
        	logger.info("RestoreEvaluator Shutting Down ... ");
            fileEvaluatorList.get(this.curExecID).shutdown();
            long timeout = 10;
            int counter = 0;
            while (!fileEvaluatorList.get(this.curExecID).awaitTermination(timeout, TimeUnit.MINUTES)) {

            	logger.info(" RestoreEvaluator is waiting to sutting down for : " + timeout * counter + " mins");

            }

            logger.info("FileRestorer Shutting Down ... ");
            fileRestorerList.get(this.curExecID).shutdown();
            counter = 0;
            while (!fileRestorerList.get(this.curExecID).awaitTermination(20, TimeUnit.SECONDS)) {
            	logger.info("FileRestorer ThreadPool is waiting to sutting down for : " + timeout * counter + " mins");
            }

            logger.info("TaskSummary Shutting Down ... ");
            statisticsCalculatorList.get(this.curExecID).shutdown();
            counter = 0;
            while (!statisticsCalculatorList.get(this.curExecID).awaitTermination(20, TimeUnit.SECONDS)) {
            	logger.info("TaskSummary ThreadPool is waiting to sutting down for : " + timeout * counter + " mins");
            }
            
        } catch (Exception ie) {
            LoggingManager.printStackTrace(ie, logger);
            
        } finally {
            fileEvaluatorList.put(this.curExecID, null);
            fileRestorerList.put(this.curExecID, null);
            statisticsCalculatorList.put(this.curExecID, null);
            
            fileEvaluatorList.remove(this.curExecID);
            fileRestorerList.remove(this.curExecID);
            statisticsCalculatorList.remove(this.curExecID);
        }
    }
        
    public static void startEvaluator(String execID, CIFSTask task) {
        
        if(!RestoreStarter.isInterupted(execID)) {
            fileEvaluatorList.get(execID).execute(new RestoreEvaluater(task));
            
        } else {//if interupted, than go to keep Stats for current TASK
            startStatCalculator(execID, task);
        }
    }
    
    public static void startRestorer(String execID, CIFSTask task) {
        
        if(!RestoreStarter.isInterupted(execID)) {
        	fileRestorerList.get(execID).execute(new FileRestorer(task));
            
        } else {//if interupted, than go to keep Stats for current TASK
            startStatCalculator(execID, task);
        }
    }
    
    public static void startStatCalculator(String execID, FileTaskInterface task) {
        statisticsCalculatorList.get(execID).execute(new TaskStatisticsRestore(task));
    }
}
