/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.threadsHandler;

import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.dataIndexer.FileIndexer;
import com.virtualcode.agent.das.dataIndexer.IndexEvaluater;
import com.virtualcode.agent.das.dataIndexer.IndexStarter;
import com.virtualcode.agent.das.dataIndexer.TaskStatisticsIndex;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSTask;
import com.virtualcode.agent.das.utils.Utility;
import java.util.HashMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;

/**
 *
 * @author YAwar
 */
public class IndexingTPEList {
    
    final private static int maxEQSize = Utility.GetPoolProperty("EvalutorQueueSize") + 1;
    final private static int maxAQSize = Utility.GetPoolProperty("ArchiverQueueSize") + 1;
    final private static int maxTQSize = Utility.GetPoolProperty("JobStatisticQueueSize") + 1;
    final private static ArrayBlockingQueue<Runnable> evalutorQueue = new ArrayBlockingQueue<Runnable>(maxEQSize);
    final private static ArrayBlockingQueue<Runnable> indexerQueue = new ArrayBlockingQueue<Runnable>(maxAQSize);
    final private static ArrayBlockingQueue<Runnable> jobStatisticQueue = new ArrayBlockingQueue<Runnable>(maxTQSize);
    
    private static HashMap<String, ThreadPoolExecutor> fileEvaluatorList =   new HashMap<String, ThreadPoolExecutor>();
    private static HashMap<String, ThreadPoolExecutor> fileIndexerList =   new HashMap<String, ThreadPoolExecutor>();
    private static HashMap<String, ThreadPoolExecutor> statisticsCalculatorList =   new HashMap<String, ThreadPoolExecutor>();
    
    private Logger logger   =   null;
    
    //non-static values, for each instance of execution...
    private String curExecID   =   null;
    
    //Constructor will start anther new PoolSet, and puts its reference in already existing List
    public IndexingTPEList(String execID, Logger logger) {
        this.logger =   logger;
        
        this.curExecID =   execID;
        
        ThreadPoolExecutor fileEvaluator = null;
        ThreadPoolExecutor fileIndexer = null;
        ThreadPoolExecutor statisticsCalculator = null;
                
        int ePoolSize = Utility.GetPoolSize("EvalutorpoolSize");
        int eMaxPoolSize = Utility.GetPoolProperty("EvalutormaxPoolSize");
        int eTimeout = (Utility.GetPoolProperty("EvalutorkeepAliveTime"));
        String eTimeoutUnitStr = Utility.GetPoolPropertyAsString("EvalutorTimeAliveUnit");
        TimeUnit eTimeoutUnit = TimeUnit.valueOf(eTimeoutUnitStr);
        ThreadPoolExecutor.CallerRunsPolicy policy = new ThreadPoolExecutor.CallerRunsPolicy();
        fileEvaluator = new ThreadPoolExecutor(
                ePoolSize,
                eMaxPoolSize,
                eTimeout,
                eTimeoutUnit,
                evalutorQueue);
        fileIndexer = new ThreadPoolExecutor(
                Utility.GetPoolSize("ArchiverpoolSize"),
                Utility.GetPoolProperty("ArchivermaxPoolSize"),
                (Utility.GetPoolProperty("ArchiverkeepAliveTime")),
                TimeUnit.valueOf(Utility.GetPoolPropertyAsString("ArchiverTimeAliveUnit")),
                indexerQueue);
        statisticsCalculator = new ThreadPoolExecutor(
                Utility.GetPoolSize("JobStatisticpoolSize"),
                Utility.GetPoolProperty("JobStatisticmaxPoolSize"),
                (Utility.GetPoolProperty("JobStatistickeepAliveTime")),
                TimeUnit.valueOf(Utility.GetPoolPropertyAsString("JobStatisticTimeAliveUnit")),
                jobStatisticQueue);

        fileEvaluator.setRejectedExecutionHandler(policy);
        fileIndexer.setRejectedExecutionHandler(policy);
        statisticsCalculator.setRejectedExecutionHandler(policy);
                
        fileEvaluatorList.put(execID, fileEvaluator);
        fileIndexerList.put(execID, fileIndexer);
        statisticsCalculatorList.put(execID, statisticsCalculator);
    }
    
    //Following method will Stop the Pool Set of only current instance's Execution ID
    public void stopCurThreadPools() {
        try {
        	logger.info("IndexEvaluator Shutting Down ... ");
            fileEvaluatorList.get(this.curExecID).shutdown();
            long timeout = 10;
            int counter = 0;
            while (!fileEvaluatorList.get(this.curExecID).awaitTermination(timeout, TimeUnit.MINUTES)) {

            	logger.info(" IndexEvaluator is waiting to sutting down for : " + timeout * counter + " mins");

            }

            logger.info("FileIndexer Shutting Down ... ");
            fileIndexerList.get(this.curExecID).shutdown();
            counter = 0;
            while (!fileIndexerList.get(this.curExecID).awaitTermination(20, TimeUnit.SECONDS)) {
            	logger.info("FileIndexer ThreadPool is waiting to sutting down for : " + timeout * counter + " mins");
            }

            logger.info("TaskSummary Shutting Down ... ");
            statisticsCalculatorList.get(this.curExecID).shutdown();
            counter = 0;
            while (!statisticsCalculatorList.get(this.curExecID).awaitTermination(20, TimeUnit.SECONDS)) {
            	logger.info("TaskSummary ThreadPool is waiting to sutting down for : " + timeout * counter + " mins");
            }
            
        } catch (Exception ie) {
            LoggingManager.printStackTrace(ie, logger);
            
        } finally {
            fileEvaluatorList.put(this.curExecID, null);
            fileIndexerList.put(this.curExecID, null);
            statisticsCalculatorList.put(this.curExecID, null);
            
            fileEvaluatorList.remove(this.curExecID);
            fileIndexerList.remove(this.curExecID);
            statisticsCalculatorList.remove(this.curExecID);
        }
    }
        
    public static void startEvaluator(String execID, CIFSTask task) {
        
        if(!IndexStarter.isInterupted(execID)) {
            fileEvaluatorList.get(execID).execute(new IndexEvaluater(task));
            
        } else {//if interupted, than go to keep Stats for current TASK
            startStatCalculator(execID, task);
        }
    }
    
    public static void startIndexer(String execID, CIFSTask task) {
        
        if(!IndexStarter.isInterupted(execID)) {
            fileIndexerList.get(execID).execute(new FileIndexer(task));
            
        } else {//if interupted, than go to keep Stats for current TASK
            startStatCalculator(execID, task);
        }
    }
    
    public static void startStatCalculator(String execID, FileTaskInterface task) {
        statisticsCalculatorList.get(execID).execute(new TaskStatisticsIndex(task));
    }
}
