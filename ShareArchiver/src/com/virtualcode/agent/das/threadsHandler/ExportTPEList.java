/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.threadsHandler;

import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.agent.das.dataArchiver.JobStarter;
import com.virtualcode.agent.das.dataExporter.ExportEvaluater;
import com.virtualcode.agent.das.dataExporter.ExportExecutors;
import com.virtualcode.agent.das.dataExporter.ExportStarter;
import com.virtualcode.agent.das.dataExporter.FileExporter;
import com.virtualcode.agent.das.dataExporter.TaskStatisticsExport;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.fileSystems.repo.RepoTask;
import java.util.HashMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
/**
 *
 * @author YAwar
 */
public class ExportTPEList {
    
    private final static int maxEQSize = Utility.GetPoolProperty("ExportEvalutorQueueSize") + 1;
    private final static int maxAQSize = Utility.GetPoolProperty("ExporterQueueSize") + 1;
    private final static int maxTQSize = Utility.GetPoolProperty("ExportJobStatisticQueueSize") + 1;
    private final static ArrayBlockingQueue<Runnable> evalutorQueue = new ArrayBlockingQueue<Runnable>(maxEQSize);
    private final static ArrayBlockingQueue<Runnable> exporterQueue = new ArrayBlockingQueue<Runnable>(maxAQSize);
    private final static ArrayBlockingQueue<Runnable> jobStatisticQueue = new ArrayBlockingQueue<Runnable>(maxTQSize);
        
    private static HashMap<String, ThreadPoolExecutor> fileEvaluatorList =   new HashMap<String, ThreadPoolExecutor>();
    private static HashMap<String, ThreadPoolExecutor> fileExporterList =   new HashMap<String, ThreadPoolExecutor>();
    private static HashMap<String, ThreadPoolExecutor> statisticsCalculatorList =   new HashMap<String, ThreadPoolExecutor>();
    
    private Logger logger   =   null;
    //non-static values, for each instance of execution...
    private String curExecID   =   null;
    
    public ExportTPEList(String execID, Logger logger) {
        this.logger =   logger;
        this.curExecID  =   execID;
        
        ThreadPoolExecutor fileEvaluator = null;
        ThreadPoolExecutor fileExporter = null;
        ThreadPoolExecutor statisticsCalculator = null;
    
        int ePoolSize = Utility.GetPoolSize("ExportEvalutorpoolSize");
        int eMaxPoolSize = Utility.GetPoolProperty("ExportEvalutormaxPoolSize");
        int eTimeout = (Utility.GetPoolProperty("ExportEvalutorkeepAliveTime"));
        String eTimeoutUnitStr = Utility.GetPoolPropertyAsString("ExportEvalutorTimeAliveUnit");
        TimeUnit eTimeoutUnit = TimeUnit.valueOf(eTimeoutUnitStr);
        ThreadPoolExecutor.CallerRunsPolicy policy = new ThreadPoolExecutor.CallerRunsPolicy();
        fileEvaluator = new ThreadPoolExecutor(
                ePoolSize,
                eMaxPoolSize,
                eTimeout,
                eTimeoutUnit,
                evalutorQueue);
        fileExporter = new ThreadPoolExecutor(
                Utility.GetPoolSize("ExporterpoolSize"),
                Utility.GetPoolProperty("ExportermaxPoolSize"),
                (Utility.GetPoolProperty("ExporterkeepAliveTime")),
                TimeUnit.valueOf(Utility.GetPoolPropertyAsString("ExporterTimeAliveUnit")),
                exporterQueue);
        statisticsCalculator = new ThreadPoolExecutor(
                Utility.GetPoolSize("ExportJobStatisticpoolSize"),
                Utility.GetPoolProperty("ExportJobStatisticmaxPoolSize"),
                (Utility.GetPoolProperty("ExportJobStatistickeepAliveTime")),
                TimeUnit.valueOf(Utility.GetPoolPropertyAsString("ExportJobStatisticTimeAliveUnit")),
                jobStatisticQueue);

        fileEvaluator.setRejectedExecutionHandler(policy);
        statisticsCalculator.setRejectedExecutionHandler(policy);
        fileExporter.setRejectedExecutionHandler(policy);
        
        
        fileEvaluatorList.put(execID, fileEvaluator);
        fileExporterList.put(execID, fileExporter);
        statisticsCalculatorList.put(execID, statisticsCalculator);
    }
    
    public void stopCurThreadPools() {
        try {
            
            logger.info("FileEvaluator Shutting Down ... ");
            fileEvaluatorList.get(this.curExecID).shutdown();
            long timeout = 10;
            int counter = 0;
            while (!fileEvaluatorList.get(this.curExecID).awaitTermination(timeout, TimeUnit.MINUTES)) {
                logger.info(" FileEvaluator is waiting to sutting down for : " + timeout * counter + " mins");
            }

            logger.info("FileExporter Shutting Down ... ");
            fileExporterList.get(this.curExecID).shutdown();
            counter = 0;
            while (!fileExporterList.get(this.curExecID).awaitTermination(20, TimeUnit.SECONDS)) {
                logger.info("FileExporter ThreadPool is waiting to sutting down for : " + timeout * counter + " mins");
            }

            logger.info("TaskSummary Shutting Down ... ");
            statisticsCalculatorList.get(this.curExecID).shutdown();
            counter = 0;
            while (!statisticsCalculatorList.get(this.curExecID).awaitTermination(20, TimeUnit.SECONDS)) {
                logger.info("TaskSummary ThreadPool is waiting to sutting down for : " + timeout * counter + " mins");
                        }
        } catch (Exception ie) {
            LoggingManager.printStackTrace(ie, logger);
            
        } finally {
            fileEvaluatorList.put(this.curExecID, null);
            fileExporterList.put(this.curExecID, null);
            statisticsCalculatorList.put(this.curExecID, null);
            
            fileEvaluatorList.remove(this.curExecID);
            fileExporterList.remove(this.curExecID);
            statisticsCalculatorList.remove(this.curExecID);
        }
    }
    
    public static void startFileEvaluator(String execID, RepoTask task) {
        if(!ExportStarter.isInterupted(execID)) {
            fileEvaluatorList.get(execID).execute(new ExportEvaluater(task));
            
        } else {//if interupted, than go to keep Stats for current TASK
            startStatCalculator(execID, task);
        }
    }
    
    public static void startFileExporter(String execID, FileTaskInterface task) {
        if(!ExportStarter.isInterupted(execID)) {
            fileExporterList.get(execID).execute(new FileExporter(task));
            
        } else {//if interupted, than go to keep Stats for current TASK
            startStatCalculator(execID, task);
        }
    }
    
    public static void startStatCalculator(String execID, FileTaskInterface task) {
        statisticsCalculatorList.get(execID).execute(new TaskStatisticsExport(task));
    }
}
