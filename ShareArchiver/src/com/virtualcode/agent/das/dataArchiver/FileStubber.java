/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataArchiver;

import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.threadsHandler.TPEList;
import com.virtualcode.agent.das.utils.CommonJobUtils;
import com.virtualcode.agent.das.utils.MyHashMap;
import com.virtualcode.agent.das.utils.Utility;

import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.log4j.Logger;

/**
 *
 * @author Saim, Abbas
 */
public class FileStubber implements Runnable {

    public FileStubber(FileTaskInterface task) {
        this.task = task;
        logger  =   LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL, this.task.getExecID());
        loginfo	=	LoggingManager.getLogger(LoggingManager.PUBLIC_MSGS, this.task.getExecID());
        
        if(MAX_NO_OF_TRIES==-1) {//set from *.properties file, if its the v first object of FileExporter
            String temp =   Utility.GetProp("MAX_NO_OF_TRIES");
            MAX_NO_OF_TRIES =   Integer.parseInt(temp);
        }
    }
    private static int MAX_NO_OF_TRIES =   -1;
    //private final static AtomicLong executionCount = new AtomicLong(0);
    private static MyHashMap executionCtr =   new MyHashMap();
    private FileTaskInterface task = null;
    //ThreadPoolExecutor nextExecutor = JobStarter.statisticsCalculator;
    private Logger logger = null;
    private Logger loginfo	=	null;

    private boolean Process() throws Exception {
        //String path = Utility.createInternetShortcut(task.getPathStr(), task.getRepoPath());
    	boolean output	=	false;
    	if(FileExecutors.currentExecs.get(task.getExecID()).getActionType().toLowerCase().trim().equals("withoutstub")) {
    		//simply delete the original file
    		logger.info(task.getUid() + " Deleting orignal file... ");
    		task.getCIFSFile().getFileObj().delete();
    		output	=	true;
    		
    	} else {//if have to create Stub...
	    	CommonJobUtils cju	=	new CommonJobUtils(logger, loginfo, task);
	        String path = cju.createInternetShortcut(task.getPathStr(), task.getSecureRepoPath(), task.getCIFSFile().getFileObj());
	        logger.info(task.getUid() + "Stubber : URL : " + path);
	        if (path == null || path.equals("")) {
	        	loginfo.error("Stub not created for "+((path!=null && path.contains("@"))?path.substring(path.lastIndexOf('@')):path));
	            output	=	false;
	            
	        }
	        output	=	true;
    	}
        return output;
    }

    public static void resetExecutionCtr(String execID) {
        executionCtr.put(execID, new AtomicLong(0));
    }
    public static String getExecutionCount(String execID) {
        return executionCtr.get(execID);
    }

    @Override
    public void run() {

        try {
        	//Terminate the current Thread, if CANCEL is requested by User
        	if(JobStarter.isInterupted(task.getExecID())) {
        		return;
        	}

            task.getTaskKpi().stubberStartTime = System.currentTimeMillis();
            
            boolean successStatus =   false;
            int ctr =   1;
            while (!successStatus && ctr<=MAX_NO_OF_TRIES) {
                logger.info("No of Try to Del / Stub: "+ctr);
                successStatus = Process();
                ctr++;
            }
            task.getTaskKpi().isStubbed	=   successStatus;
            
            task.getTaskKpi().stubberEndTime = System.currentTimeMillis();


            if (task.getTaskKpi().isStubbed) {
//                int taskInQueue = nextExecutor.getQueue().size();
//                while (taskInQueue >= (FileExecutors.maxTQSize - 1)) {
//                    try {
//                        LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL).debug("Stubber->Stats is sleeping now : Queue Size " + taskInQueue);
//                        Thread.sleep(5000);
//                    } catch (InterruptedException ex1) {
//                        LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL).debug(ex1);
//                    }
//                }
                //nextExecutor.execute(new TaskStatistic(task));
                //TPEList.getStatCalculator(task.getExecID()).execute(new TaskStatistic(task));
                TPEList.startStatCalculator(task.getExecID(), task);
                
            }
        } catch (Exception ex) {
            logger.info("Stubber Failed : " + task.getUid());
            loginfo.error("Stubber failed : " + task.getUid()+" : "+ex.getMessage()+" : "+((task.getPathStr()!=null && task.getPathStr().contains("@"))?task.getPathStr().substring(task.getPathStr().lastIndexOf('@')):task.getPathStr()));

            task.getTaskKpi().failStubbing = true;
            task.getTaskKpi().errorDetails = ex;
//            int taskInQueue = FileExecutors.statisticsCalculator.getQueue().size();
//                while (taskInQueue >= (FileExecutors.maxTQSize - 1)) {
//                    try {
//                        LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL).debug("Stubber->Stats is sleeping now : Queue Size " + taskInQueue);
//                        Thread.sleep(5000);
//                    } catch (InterruptedException ex1) {
//                        LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL).debug(ex1);
//                    }
//                }
            //JobStarter.statisticsCalculator.execute(new TaskStatistic(task));
            //TPEList.getStatCalculator(task.getExecID()).execute(new TaskStatistic(task));
            TPEList.startStatCalculator(task.getExecID(), task);
            
        } finally {
            //executionCount.addAndGet(1);
            executionCtr.increment(task.getExecID());
        }
    }
}
