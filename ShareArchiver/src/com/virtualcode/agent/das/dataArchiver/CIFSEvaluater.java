/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataArchiver;

import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSTask;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSFile;
import com.virtualcode.agent.das.utils.GlobMatch;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.dataIndexer.IndexerManager;
import com.virtualcode.agent.das.dataIndexer.StatsAnalyzer;
import com.virtualcode.agent.das.documentManagement.dto.DocumentType;
import com.virtualcode.agent.das.documentManagement.dto.IndexNode;

import com.virtualcode.agent.das.threadsHandler.TPEList;
import com.virtualcode.agent.das.utils.MyHashMap;
import com.virtualcode.cifs.Acl;
import com.virtualcode.cifs.CifsUtil;
import com.virtualcode.common.CustomProperty;

import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicLong;
import jcifs.smb.SmbFile;
import org.apache.log4j.Logger;
import org.apache.lucene.index.IndexWriter;

/**
 * @author Abbas
 */
public class CIFSEvaluater implements Runnable {

    private Logger logger = null;
    private Logger loginfo	=	null;

    public CIFSEvaluater(CIFSTask task) {
        this.task = task;
        logger  =   LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL, this.task.getExecID());
        loginfo	=	LoggingManager.getLogger(LoggingManager.PUBLIC_MSGS, this.task.getExecID());
        
        //nextExecutor    =   TPEList.getFileArchiver(this.task.getExecID());
    }
    
    public FileTaskInterface getFileTask() {
        return this.task;
    }
    
    //private final static AtomicLong executionCount = new AtomicLong(0);
    private static MyHashMap executionCtr  =   new MyHashMap();
    private CIFSTask task = null;
    String extensions = "";
    private Long docAge = 0L;
    private Long docSize = 0L;
    private Long docLM = 0L;
    private Long docLA = 0L;
    private boolean ProcessHD = false;
    private boolean ProcessRO = false;
    //private ThreadPoolExecutor nextExecutor = null;//JobStarter.fileArchiver;

    private boolean Process() throws Exception {

        //Path file = task.getPath();
        //Path name = file.getFileName();
        CIFSFile    cFile   =   this.task.getCIFSFile();
        String cFileName    =   cFile.getFileObj().getName();
        String cFilePath    =   cFile.getFileObj().getPath();
        
        logger.info(task.getUid().toString() + " : Evaluating : " + cFilePath);
        GlobMatch matcher   =   new GlobMatch();
        String[] exts = extensions.split(",");
        Exception ex = null;

        if (exts.length == 0) {
            exts = "*.*,*.*".split(",");
        }

        for (String s : exts) {
            if (s == null || s.isEmpty()) {
                /////////   rp.skippedNullEmpty += 1;
                continue;
            }
            
            //matcher = FileSystems.getDefault().getPathMatcher("glob:" + s);
            if (cFileName != null && matcher.match(cFileName, s)) {
                logger.info(task.getUid().toString() + " : Matched : " + s);
                task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND;
                if (cFileName.isEmpty()
                        || cFileName.startsWith("~")
                        || cFileName.startsWith("$")
                        //|| cFileName.startsWith(".")
                        || cFileName.endsWith(".lnk")
                        || cFileName.endsWith(".tmp")
                        || cFileName.endsWith(".url")) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_TEMPORARY;
                    logger.info("Link File OR Temporary File OR Office temporary File : " + cFileName);
                    return true;
                }

                //BasicFileAttributes attrs = task.getAttributes();
                SmbFile smbFile =   cFile.getFileObj();
                if (smbFile.isDirectory()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_ISDIR;
                    logger.info("Is a Directory " + cFilePath);
                    return true;
                }

                /*
                if (attrs.isSymbolicLink()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_SYMBOLLINK;
                    logger.info("Is a Symbolic Link " + file);
                    return true;

                }

                if (attrs.isOther()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_SYMBOLLINK;
                    logger.info("Is a Other File, no idea what does it mean, may be not regular " + file);
                    return true;
                }*/

                if (!ProcessHD && smbFile.isHidden()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_HIDDENLECTED;
                    logger.info("Is a Hidden File " + cFilePath);
                    return true;
                }

                if (!ProcessRO && !smbFile.canWrite()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_READ_ONLY;
                    logger.info("Is a Readonly File " + cFilePath);
                    return true;
                }

                if (smbFile.length()<=0) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_TEMPORARY;
                    logger.info("Link File OR ADF enabled Windows stub file : " + cFileName);
                    return true;
                }
                if (docSize > 0 && smbFile.length() < docSize) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_SMALL_SIZE;
                    logger.info("File Size : ( " + smbFile.length() + " ) " + " Required Size : ( " + docSize + " ) ");
                    return true;
                } else if (docAge > 0 && smbFile.createTime() > docAge) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_AGE;
                    logger.info("File Creation Time : ( " + smbFile.createTime()  + " ) " + " Required Time : ( " + docAge + " ) ");
                    return true;
                } else if (docLA > 0 && smbFile.getLastAccess() > docLA) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_LASTACCESSTIME;
                    logger.info("File Last Access Time : ( " + smbFile.getLastAccess() + " ) " + " Required Time : ( " + docLA + " ) ");
                    return true;
                } else if (docLM > 0 && smbFile.getLastModified() > docLM) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_LASTMODIFIEDTIME;
                    logger.info("File Last Modified Time : ( " + smbFile.getLastModified() + " ) " + " Required Size : ( " + docLM + " ) ");
                    return true;
                } else {
                    return true;
                }
            }
        }
        return false;
    }

    public static void resetExecutionCtr(String execID) {
        executionCtr.put(execID, new AtomicLong(0));
    }
    public static String getExecutionCount(String execID) {
        return executionCtr.get(execID);
    }

    @Override
    public void run() {
        try {

        	//Terminate the current Thread, if CANCEL is requested by User
        	if(JobStarter.isInterupted(task.getExecID())) {
        		return;
        	}
        	
            task.getTaskKpi().evalutionStartTime = System.currentTimeMillis();

            Job aJobDetail = FileExecutors.currentExecs.get(task.getExecID());
            Iterator newIterator = aJobDetail.getPolicy().getDocumentTypeSet().iterator();
            while (newIterator.hasNext()) {
                DocumentType documentType = (DocumentType) newIterator.next();
                extensions = extensions + "*" + documentType.getValue() + ",";
            }
            if (extensions.isEmpty()) {
                extensions = "*";
            }

            String dAge = aJobDetail.getPolicy().getDocumentAge();
            String laDate = aJobDetail.getPolicy().getLastAccessedDate();
            String lmDate = aJobDetail.getPolicy().getLastModifiedDate();
            String sz = aJobDetail.getPolicy().getSizeLargerThan();
            docAge = Utility.calculateMilliSec(Utility.parseYear(dAge), Utility.parseMonth(dAge));
            docLM = Utility.calculateMilliSec(Utility.parseYear(lmDate), Utility.parseMonth(lmDate));
            docLA = Utility.calculateMilliSec(Utility.parseYear(laDate), Utility.parseMonth(laDate));
            docSize = Utility.calculateSize(sz);
            ProcessRO = aJobDetail.isProcessReadOnly();
            ProcessHD = aJobDetail.isProcessHidden();

            boolean p = Process();//by default, only Evaluation Process
            task.getTaskKpi().evalutionEndTime = System.currentTimeMillis();

            //////////////////////
            ///////if(p && (its a EvaluatedOnly & Index Job)) {            
            	buildLuceneIndexes();
            ///////}
            //////////////
            
            if (!p && task.getTaskKpi().evaluatorFlag != EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND) {
                task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_EXTENSION;
            }

            //Exception shouldn't be thrown, bcoz we have to invoke Stats Calculator
            //if (task.getTaskKpi().evaluatorFlag != EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND) {
            //    throw new Exception("Task : " + this.task.getPathStr() + " Reason: " + task.getTaskKpi().evaluatorFlag.toString());
            //}

            //If current file is ready to archive / Stub / Evaluated
            if (task.getTaskKpi().evaluatorFlag == EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND) {
                
                //Get the file-size, to updated Evaluation vars
                task.getTaskKpi().setFileSize(this.task.getCIFSFile().getFileObj().length());
            }
            
            if (task.getTaskKpi().evaluatorFlag == EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND
                    && (aJobDetail.getActionType().toLowerCase().trim().equals("archive")
                    || aJobDetail.getActionType().toLowerCase().trim().equals("stub")
                    || aJobDetail.getActionType().toLowerCase().trim().equals("withoutstub"))) {
                logger.info(task.getUid() + " Sending to Archiving ");
                //TPEList.getFileArchiver(task.getExecID()).execute(new FileArchiver(task));
                TPEList.startFileArchiver(task.getExecID(), task);
                
              //  FileExecutors.statisticsCalculator.execute(new TaskStatistic(task));
            } else {

                logger.info(task.getUid() + " Sending to Task Statistic ");
                //JobStarter.statisticsCalculator.execute(new TaskStatistic(task));
                //TPEList.getStatCalculator(task.getExecID()).execute(new TaskStatistic(task));
                TPEList.startStatCalculator(task.getExecID(), task);
            }
        } catch (Exception ex) {
            task.getTaskKpi().errorDetails = ex;
            task.getTaskKpi().failEvaluation = true;
            loginfo.error("Evaluation failed: "+ex.getMessage());
            
            //JobStarter.statisticsCalculator.execute(new TaskStatistic(task));
            //TPEList.getStatCalculator(task.getExecID()).execute(new TaskStatistic(task));
            TPEList.startStatCalculator(task.getExecID(), task);
            
        } finally {
            //executionCount.addAndGet(1);
            executionCtr.increment(this.task.getExecID());//increment by 1 for current execID
        }
    }
    
    private void buildLuceneIndexes() throws Exception {
        
        //get the writer, from initiated Pool
        IndexWriter writer	=	IndexerManager.getIndexWriter(task.getIndexID());
		//int originalNumDocs = writer.numDocs();
        
        //get the ShareAnalysisStats
        StatsAnalyzer saStats	=	IndexerManager.getShareAnalysisStats(task.getIndexID());
        
		IndexNode iNode	=	new IndexNode();

		String filePath	=	task.getCIFSFile().getFileObj().getPath();
		String fileName	=	task.getCIFSFile().getFileObj().getName();
		boolean isArchived	=	false;
		if(fileName.endsWith(".url")) {//also index the stubs
			isArchived	=	true;
		}
		
		//logger.info(this.task.getUid().toString() +" filePath: " + filePath);
		iNode.setFilePath(filePath);
		
		//logger.info(this.task.getUid().toString() +" fileName: " + fileName);
		iNode.setFileName(fileName);
		
		//logger.info(this.task.getUid().toString() +" dateCreation: " + task.getCreationDate().getTime());
		iNode.setDateCreation(task.getCreationDate().getTime());
		
		//logger.info(this.task.getUid().toString() +" dateModification: " +task.getLastModifiedDate().getTime());
		iNode.setDateModification(task.getLastModifiedDate().getTime());
		
		//logger.info(this.task.getUid().toString() +" dateLastAccess: " + task.getLastAccessedDate().getTime());
		iNode.setDateLastAccess(task.getLastAccessedDate().getTime());
		
		//logger.info(this.task.getUid().toString() +" fileSize: " +  task.getCIFSFile().getFileObj().length());
		iNode.setFileSize(task.getCIFSFile().getFileObj().length());
		
		//logger.info(this.task.getUid().toString() +" isArchived: " +  isArchived);
		iNode.setIsArchived(isArchived+"");

		CifsUtil	cifsUtil	=	new CifsUtil();
		Acl	acl	=	cifsUtil.getACL(task.getCIFSFile().getFileObj());
		if(acl!=null) {
			HashMap<String, String> aclAttribs	=	CifsUtil.getAclAttribs(acl);
			
			//logger.info(this.task.getUid().toString() +" VC_ALLOWED_SIDS: " +  aclAttribs.get(CustomProperty.VC_ALLOWED_SIDS));
			iNode.setAllowedSids(aclAttribs.get(CustomProperty.VC_ALLOWED_SIDS));
			iNode.setDenySids(aclAttribs.get(CustomProperty.VC_DENIED_SIDS));
			iNode.setShareAllowedSids(aclAttribs.get(CustomProperty.VC_S_ALLOWED_SIDS));
			iNode.setShareDenySids(aclAttribs.get(CustomProperty.VC_S_DENIED_SIDS));
		}
		
		//finally add it into IndexWriter
		IndexerManager.addDocument(writer, iNode);
		
		// also update the Stats - for summary/reporting
		if(saStats!=null)
			saStats.updateStats(iNode);
		else
			logger.info(this.task.getUid().toString() +" saStats not exist for CIFSEvaluater Counts...");
		
		logger.info(this.task.getUid().toString() +" Added: " + fileName);
    }
    
    /*
    private void buildLuceneIndexes() throws Exception {
                
        //get the writer, from initiated Pool
        IndexWriter writer	=	IndexerManager.getIndexWriter(task.getIndexID());
		//int originalNumDocs = writer.numDocs();
        
		IndexNode iNode	=	new IndexNode();

		String filePath	=	task.getCIFSFile().getFileObj().getPath();
		String fileName	=	task.getCIFSFile().getFileObj().getName();
		boolean isArchived	=	false;
		if(fileName.endsWith(".url")) {//also index the stubs
			isArchived	=	true;
		}
		
		logger.info(this.task.getUid().toString() +" filePath: " + filePath);
		iNode.setFilePath(filePath);
		
		logger.info(this.task.getUid().toString() +" fileName: " + fileName);
		iNode.setFileName(fileName);
		
		logger.info(this.task.getUid().toString() +" dateCreation: " + task.getCreationDate().getTime());
		iNode.setDateCreation(task.getCreationDate().getTime());
		
		logger.info(this.task.getUid().toString() +" dateModification: " +task.getLastModifiedDate().getTime());
		iNode.setDateModification(task.getLastModifiedDate().getTime());
		
		logger.info(this.task.getUid().toString() +" dateLastAccess: " + task.getLastAccessedDate().getTime());
		iNode.setDateLastAccess(task.getLastAccessedDate().getTime());
		
		logger.info(this.task.getUid().toString() +" fileSize: " +  task.getCIFSFile().getFileObj().length());
		iNode.setFileSize(task.getCIFSFile().getFileObj().length());
		
		logger.info(this.task.getUid().toString() +" isArchived: " +  isArchived);
		iNode.setIsArchived(isArchived+"");
		
		//finally add it into IndexWriter
		IndexerManager.addDocument(writer, iNode);
		
		logger.info(this.task.getUid().toString() +" Added: " + fileName);
		
		//int newNumDocs = writer.numDocs();
    }
    */
}