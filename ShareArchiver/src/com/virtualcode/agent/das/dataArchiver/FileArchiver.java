/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataArchiver;

import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.utils.Utility;

import com.virtualcode.controller.DocumentManagementModule;
//import com.virtualcode.agent.sa.Acl;
import com.virtualcode.agent.das.threadsHandler.TPEList;
import com.virtualcode.agent.das.utils.MyHashMap;
import com.virtualcode.cifs.Acl;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.log4j.Logger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
//import java.util.concurrent.ThreadPoolExecutor;
import javax.activation.DataHandler;

/**
 *
 * @author Saim, Abbas
 */
public class FileArchiver implements Runnable {

    public FileArchiver(FileTaskInterface task) {
        this.task = task;
        logger  =   LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL , task.getExecID());
        loginfo	=	LoggingManager.getLogger(LoggingManager.PUBLIC_MSGS, task.getExecID());
    }
    
    //private final static AtomicLong executionCount = new AtomicLong(0);
    private static MyHashMap executionCtr =   new MyHashMap();
    private static MyHashMap alreadyExecCtr	=	new MyHashMap();
    
    private int tryCount	=	0;
    private FileTaskInterface task = null;
    //ThreadPoolExecutor nextExecutor = JobStarter.fileStubber;
    private Logger logger = null;
    private Logger loginfo	=	null;

    private String Process() throws Exception {
        String documentPath = task.getNodeHeirarchy();
        String layers = "";

        Date modifiedDate = task.getLastModifiedDate();
        Date createdDate = task.getCreationDate();
        Date accessedDate = task.getLastAccessedDate();

        String tagString	=	FileExecutors.currentExecs.get(task.getExecID()).getTags();
        
        //We can use H for 24 hr formatting and h for 12hr formating
        //e.g. yyyy.MM.dd G 'at' HH:mm:ss z
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        String owner = task.getOwner();
        layers += "ModifiedOn=" + df.format(modifiedDate) + "|";
        layers += "CreatedOn=" + df.format(createdDate) + "|";
        layers += "ModifiedBy=" + owner + "|";
        layers += "CreatedBy=" + owner + "|";
        layers += "AccessedOn=" + df.format(accessedDate) + "|";
        layers += "SourceAgent=FS|";
        layers += "AgentType=FS|";
        if(tagString!=null && !tagString.isEmpty())//dont sent empty value to Server, in tag attribute
        	layers += "tags=" + tagString + "|";
        layers += "SPURL=" + documentPath + "|";
        layers += ("secureInfo=" + task.getEncodedString());//.encodedString);
        layers = layers.trim();

        logger.info(task.getUid() + " Layers : " + layers);


        String path64           = new String(Utility.encodetoBase64(documentPath.getBytes("UTF-8")));
        //DataHandler dataHandler =   task.getDataHandler();
        
        Acl acl =   task.getACL(documentPath);//Get ACE list

        String DASUrl = null;
        if (Utility.GetProp("CM").equals("1")) {
            //DASUrl = Utility.uploadDocument(path64, dataHandler, layers, acl);
        	DASUrl = Utility.uploadDocument(path64, task, layers, acl, task.getUid().toString());
        	
        } else {
            //DASUrl = uploadDocument(path64, dataHandler, layers, acl);
        	DASUrl = uploadDocument(path64, task, layers, acl, task.getUid().toString());
        }

        //dataHandler = null;
        path64 = null;
        layers = null;
        df = null;
        createdDate = null;
        modifiedDate = null;
        accessedDate = null;
        owner = null;
        logger.info(task.getUid() + " Archive URL  : " + DASUrl);
        if (DASUrl != null && !DASUrl.isEmpty()) {
        	
        	String transactionFlag	=	task.getSecureRepoPath();
        	if("AE".equals(transactionFlag)) {//uploadDocumentByBuitInAgent, sets a temporary value in it...
        		logger.info(task.getUid() + " File already archived: ");//+((documentPath!=null && documentPath.contains("@"))?documentPath.substring(documentPath.lastIndexOf('@')+1):documentPath));
        		//loginfo.info("Already exist: "+((documentPath!=null && documentPath.contains("@"))?documentPath.substring(documentPath.lastIndexOf('@')+1):documentPath));
        		
        	} else if("LN".equals(transactionFlag)) {//uploadDocumentByBuitInAgent, sets a temporary value in it...
        		logger.info(task.getUid() + " Same content already exist (so linked it): ");//+((documentPath!=null && documentPath.contains("@"))?documentPath.substring(documentPath.lastIndexOf('@')+1):documentPath));
        		//loginfo.info("Duplicate content: "+((documentPath!=null && documentPath.contains("@"))?documentPath.substring(documentPath.lastIndexOf('@')+1):documentPath));
        		
        	} else {
        		//loginfo.info("Archived: "+((documentPath!=null && documentPath.contains("@"))?documentPath.substring(documentPath.lastIndexOf('@')+1):documentPath));
        	}
            task.setSecureRepoPath(DASUrl);   //Now overwrite the actual value of DASUrl
            DASUrl = null;
            return transactionFlag;
        }
        
        loginfo.error("FileArchiver failed to read (or access) file at location: "+((documentPath!=null && documentPath.contains("@"))?documentPath.substring(documentPath.lastIndexOf('@')+1):documentPath));
        DASUrl = null;
        documentPath = null;
        return "NO";//if not archived successfuly - than hardcoded No...
    }

    public static void resetExecutionCtr(String execID) {
        executionCtr.put(execID, new AtomicLong(0));
        alreadyExecCtr.put(execID, new AtomicLong(0));
    }
    public static String getExecutionCount(String execID) {
        return executionCtr.get(execID);
    }
    public static String getAlreadyArchviedCount(String execID) {
    	return alreadyExecCtr.get(execID);
    }

    @Override
    public void run() {

        try {
        	
        	//Launch the Runner..   ;)
            runner();

        } catch (Exception ex) {

        	loginfo.error("Archiver failed : " + task.getUid()+" : "+ex.getMessage()+" : "+((task.getPathStr()!=null && task.getPathStr().contains("@"))?task.getPathStr().substring(task.getPathStr().lastIndexOf('@')):task.getPathStr()));        	
            task.getTaskKpi().failedArchiving = true;
            task.getTaskKpi().errorDetails = ex;
            //JobStarter.statisticsCalculator.execute(new TaskStatistic(task));
            //TPEList.getStatCalculator(task.getExecID()).execute(new TaskStatistic(task));
            TPEList.startStatCalculator(task.getExecID(), task);
            
        } finally {
            //executionCount.addAndGet(1);
            executionCtr.increment(this.task.getExecID());//inc by 1
            //logger.info("File Archiver for ("+this.task.getExecID()+") becomes "+ executionCtr.get(this.task.getExecID()));
        }
    }

    private static String uploadDocument(java.lang.String fileContextPath, FileTaskInterface cFile, java.lang.String layers, Acl acl, String transID) throws Exception{
        DocumentManagementModule port   =   new DocumentManagementModule();
        return port.uploadDocument(fileContextPath, cFile, layers, acl, transID);
    }
    
    private void runner() throws Exception {
    	
    	//Terminate the current Thread, if CANCEL is requested by User
    	if(JobStarter.isInterupted(task.getExecID())) {
    		return;
    	}
        tryCount++;//increment the Try count by ONE

        task.getTaskKpi().archiveStartTime = System.currentTimeMillis();
        String authString = "username=" + Utility.GetProp("AgentLogin") + "&password=" + Utility.GetProp("AgentPassword") + "";
        task.setEncodedString(new String(Utility.encodetoBase64(authString.getBytes())) );
        task.getTaskKpi().isArchived = Process();
        task.getTaskKpi().archiveEndTime = System.currentTimeMillis();
        
    	if("AE".equals(task.getTaskKpi().isArchived)) {//if alredy archived
        	alreadyExecCtr.increment(this.task.getExecID());//inc by 15
        }
    	
        if (!"NO".equals(task.getTaskKpi().isArchived) && //if archived
        		(FileExecutors.currentExecs.get(task.getExecID()).getActionType().toLowerCase().trim().equals("stub") ||
        				FileExecutors.currentExecs.get(task.getExecID()).getActionType().toLowerCase().trim().equals("withoutstub"))) {
            //nextExecutor.execute(new FileStubber(task));
            //TPEList.getFileStubber(task.getExecID()).execute(new FileStubber(task));
            TPEList.startFileStubber(task.getExecID(), task);
            
        } else if (!"NO".equals(task.getTaskKpi().isArchived)) {//if archived
            //JobStarter.statisticsCalculator.execute(new TaskStatistic(task));
            //TPEList.getStatCalculator(task.getExecID()).execute(new TaskStatistic(task));
            TPEList.startStatCalculator(task.getExecID(), task);
            
        } else {//if errored....   "NO".equals(task.getTaskKpi().isArchived)
        	//loginfo.warn("Archive Attemp ["+retryCount +"] failed for "+ ((task.getPathStr()!=null && task.getPathStr().contains("@"))?task.getPathStr().substring(task.getPathStr().lastIndexOf('@')):task.getPathStr()));
        	logger.warn(task.getUid() + " Archive Attemp ["+tryCount +"] failed as "+task.getTaskKpi().isArchived);
        	int maxArchiveTries	=	(Utility.GetProp("maxArchiveTries")!=null)?Integer.parseInt(Utility.GetProp("maxArchiveTries")):2;
        	if(tryCount < maxArchiveTries) {
        		logger.info(task.getUid() + " Retrying for Archiver Process");
        		runner();
        		
        	} else {
        		throw new Exception("Unable to archive the file");
        	}
        }
    }
}
