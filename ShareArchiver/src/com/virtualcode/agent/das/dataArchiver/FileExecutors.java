/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataArchiver;

import com.virtualcode.agent.das.Executors.DataLayer;
import com.virtualcode.agent.das.archivePolicy.dto.ExcludedPath;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSService;
import com.virtualcode.agent.das.fileSystems.FileServiceInterface;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.archivePolicy.dto.SPDocLib;
import com.virtualcode.agent.das.dataIndexer.IndexerManager;
import com.virtualcode.agent.das.dataIndexer.IndexingExecutors;
import com.virtualcode.agent.das.documentManagement.dto.DocumentType;
import com.virtualcode.agent.das.threadsHandler.TPEList;
import com.virtualcode.agent.das.utils.MyHashMap;
import com.virtualcode.vo.DriveLetters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.log4j.Logger;

/**
 *
 * @author Saim, Abbas
 */
public class FileExecutors {

    //save the job instance of current executions, to be used in Stats etc
    public static HashMap<String, Job> currentExecs = new HashMap<String, Job>();
    
    private static MyHashMap fileWalkedCtr  =   new MyHashMap();
    
    void startJob(Job job, String executionID) throws InterruptedException, Exception {
        Logger logger = LoggingManager.getLogger(LoggingManager.JOB_SUMMARY, executionID);
        Logger loginfo	=	LoggingManager.getLogger(LoggingManager.PUBLIC_MSGS, executionID);
        
        int errorCodeID = 0;
        int jID = -1;
        
        //Initialize the Counters for current Execution of cur Job
        fileWalkedCtr.put(executionID, new AtomicLong(0));
        CIFSEvaluater.resetExecutionCtr(executionID);//initialize the counter for CIFS evaluater
        FileArchiver.resetExecutionCtr(executionID);
        TaskStatistic.resetExecutionCtr(executionID);
        TaskStatistic.resetFailedCtr(executionID);
        TaskStatistic.resetComulativeSize(executionID);
        FileStubber.resetExecutionCtr(executionID);
        
        Job currentJob  =   null;
        try {
            //Print the version of Agent, along with each Job
            //logger.info("Commercial Version ID = 1, Major Version ID = 1, Minor Version = 2");
            //logger.info("Commercial Version V 1.2.0, RD 16 Dec 2011");
            
            jID = (int) job.getId();

            currentExecs.put(executionID, job);
            currentJob = currentExecs.get(executionID);
            currentJob.getJobStatistics().setJobStartTime(new Date());
            
            if (executionID == null) {
                throw new Exception("Execution ID is null");
            }
                        
            // make/initiate the ThreadPools for current Execution of an archive Job
            TPEList threadsPoolSet  =   new TPEList(executionID, logger);
            
            logger.info("Job Info");
            logger.info("=======");
            logger.info("Execution ID " + executionID);
            logger.info("Job Name " + currentJob.getJobName());
            logger.info("Job Type " + currentJob.getActionType());
            loginfo.info(currentJob.getJobName() + " started with ID="+executionID);
            
            Iterator iterator = job.getSpDocLibSet().iterator();
            while (iterator.hasNext()) {
                SPDocLib spDocLib = (SPDocLib) iterator.next();
                logger.info("\t Path : " + spDocLib.getLibraryName());
                
                logger.info("job -> Paths -> excluded paths:");
                Iterator excludePathIterator = spDocLib.getExcludedPathSet().iterator();
                while (excludePathIterator.hasNext()) {
                    ExcludedPath excludePath = (ExcludedPath) excludePathIterator.next();
                    logger.info("\t \t" + excludePath.getPath());
                }
            }

            logger.info("job -> policy name : " + job.getPolicy().getPolicyName());
            logger.info("job -> policy -> document-types: ");
            Iterator newIterator = job.getPolicy().getDocumentTypeSet().iterator();
            while (newIterator.hasNext()) {
                DocumentType documentType = (DocumentType) newIterator.next();
                logger.info("\t documentType name: " + documentType.getValue());
            }

            for (SPDocLib p : job.getSpDocLibSet()) {
                
                FileServiceInterface    fs  =   null;
                if(p.getLibraryName().toLowerCase().contains("smb://")) {//if the path is type CIFS
                    fs  =   new CIFSService(executionID, ""+p.getId());

                    //Initiate IndexWriter, and load in Pool
                    DriveLetters driveLtr	=	IndexingExecutors.getDriveLetter(p);//driveLtr can be NULL
                    IndexerManager.initIndexWriter(driveLtr, p.getId()+"", true);
                    
                } else {//if the path is type NTFS
                	loginfo.error("CODE[FE001]: Invalid path format, to run this type of job!");
                	
                    logger.warn("Only CIFS is supported in this version of Agent");
                    throw new Exception("Invalid FileSystem Type");
                }
                
                //Prepare the list of Excuded sub-paths of current Path
                ArrayList<String> excPathList   =   new ArrayList();
                for(ExcludedPath ep : p.getExcludedPathSet()) {
                    excPathList.add(ep.getPath());
                }
                fs.walkFileTree(p.getLibraryName(), excPathList, true);
            }

            
            //Wait for shutting down the threads of current Execution
            threadsPoolSet.stopCurThreadPools();
            ////PROCEED FURTHER AFTER STOPPING THE THREAD POOL OF CUR JOB-EXEC

            //Now close the IndexWriter's of all Shares...after completing the Indexes
            for (SPDocLib p : job.getSpDocLibSet()) {
	        	try {
	        		IndexerManager.closeIndexWriter(p.getId()+"");
	        	} catch(IOException ioe) {
	        		logger.error("Indexer not closed for ["+p.getId()+"] "+p.getLibraryName());
	        		LoggingManager.printStackTrace(ioe, logger);
	        	}
            }
            
            logger.info("Memory ... " + Runtime.getRuntime().totalMemory());
            logger.info("File Walked : " + fileWalkedCtr.get(executionID));
            fileWalkedCtr.put(executionID, new AtomicLong(0));//reset value to 0
                        
            logger.info("");
            logger.info("Executors Report");
            logger.info("=================");
            logger.info("CIFS Evaluted : " + CIFSEvaluater.getExecutionCount(executionID));
//            logger.info("DeDup Checked : " + FileDeDupChecker.getExecutionCount());
            logger.info("Archived : " + FileArchiver.getExecutionCount(executionID));
            logger.info("Stubbed : " + FileStubber.getExecutionCount(executionID));
            logger.info("Failed : " + TaskStatistic.getFailedCount(executionID));
            logger.info("Summary : " + TaskStatistic.getExecutionCount(executionID));
            logger.info("Total Size : " + TaskStatistic.getComulativeSize(executionID) + " bytes");

            currentJob.getJobStatistics().setJobEndTime(new Date());

            logger.info("Calling WS for executionCompleted with ID : " + executionID);

            boolean success = DataLayer.executionCompleted(jID, executionID, errorCodeID, currentJob.getJobStatistics());
            if (success) {
                logger.info("Successfull returned from execution Completed WS : " + jID);
            } else {
                throw new Exception("Oops.Error returned from execution Completed WS : " + jID);
            }

        } catch (Exception ex) {
            errorCodeID = 1;
            if (executionID != null) {

                if (currentJob == null) {
                	loginfo.error("CODE["+errorCodeID+"]: Fatal Error!");
                	
                    logger.info("Current Job is Null");
                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.executionCompleted(jID, executionID, errorCodeID, null);

                } else if (currentJob.getJobStatistics() == null) {
                	loginfo.error("CODE["+errorCodeID+"]: Fatal Error!");
                	
                    logger.info("currentJob.getJobStatistics() is Null");
                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.executionCompleted(jID, executionID, errorCodeID, null);
                } else {
                    currentJob.getJobStatistics().setJobEndTime(new Date());
                    loginfo.error("CODE["+errorCodeID+"]: Fatal Error!");

                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.executionCompleted(jID, executionID, errorCodeID, currentJob.getJobStatistics());
                }
            }
            loginfo.error("CODE["+errorCodeID+"]: Fatal Error!");
            loginfo.error(ex.getMessage());
            
            logger.fatal("ExecutionID: " + executionID);
            logger.fatal("JobID: " + jID);
            logger.fatal("ErrorID: " + errorCodeID);
            
            logger.fatal(ex.getMessage());
            LoggingManager.printStackTrace(ex, logger);
            LoggingManager.getLogger(LoggingManager.ACTIVITY_ERROR, executionID).error("Fatal : " + "\n" + ex.getMessage());
            LoggingManager.getLogger(LoggingManager.PUBLIC_MSGS, executionID).error("Fatal : " + "\n" + ex.getMessage());

        } finally {
            currentJob = null;
            currentExecs.remove(executionID);
            JobStarter.underProcessJobsList.remove(jID+"");//remove from the volatile list at Agent side...
        }
    }
    
    public static void incrementCounter(String execID) {
        fileWalkedCtr.increment(execID);
    }
}
