/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataArchiver;

import com.virtualcode.agent.das.Executors.DataLayer;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.archivePolicy.dto.Job;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
/**
 *
 * @author se7
 */
public class JobStarter implements Runnable {
    
    private String AgentName    =   null;
    private String actionType   =   null;
    private static Logger logger   =   Logger.getLogger(JobStarter.class);
    
    public static ArrayList<String> underProcessJobsList   =   new ArrayList<String>();
    private static List<Integer> interuptedJobIDsList  =   null;
            
    public JobStarter(String AgentName) {
        this.AgentName  =   AgentName;
        this.actionType  =   "IMPORT";
    }
    
    @Override
    public void run() {
        try {            
            logger.info(AgentName + " waiting for Job...");
            Thread.currentThread().sleep(12 * 1000);// 12 sec
            
            while (true) {
                try {
                    //Get the Jobs list from Sharearchiver WS
                    List<Job> jobsList  =   DataLayer.JobsbyAgent(AgentName, actionType);
                    if(jobsList!=null && !jobsList.isEmpty() && jobsList.size()>0) {//if atleast One Job Exist
                        
                        logger.info(jobsList.size() + " new Jobs Found.");
                        
                        interuptedJobIDsList  =   DataLayer.interuptedJobs();
                        logger.info("Interupted number of Jobs: "+interuptedJobIDsList.size());
                        
                        //Traverse all the Jobs
                        for (Job j : jobsList) {
                            
                            int jID = j.getId();
                            logger.info("Job ID : " + jID);
                            if(underProcessJobsList.contains(jID+"")) {
                                logger.info("Already processing: "+jID);
                                
                            } else if(interuptedJobIDsList.contains(jID+"")) {
                                logger.warn("Job is Canceled / Interupted: "+jID);
                                
                            } else {
                                underProcessJobsList.add(jID+"");
                                String executionID = DataLayer.executionStarted(jID);
                                logger.info("Execution ID : " + executionID);

                                //Configure loggers for current executions
                                LoggingManager.configureLogger(jID, executionID, LoggingManager.PUBLIC_MSGS);
                                LoggingManager.configureLogger(jID, executionID, LoggingManager.ACTIVITY_DETAIL);
                                LoggingManager.configureLogger(jID, executionID, LoggingManager.ACTIVITY_ERROR);
                                LoggingManager.configureLogger(jID, executionID, LoggingManager.TASK_SUMMARY);
                                LoggingManager.configureLogger(jID, executionID, LoggingManager.JOB_SUMMARY);

                                JobThread jt    =   new JobThread(j, executionID);
                                Thread job      =   new Thread(jt);
                                job.start();
                            }
                            
                            logger.info("Goto next Job...");
                        }
                        
                    } else {
                        logger.info("New Jobs not found");
                    }
                } catch(Exception ex) {//if WS is down at Server side, then dont' Terminate...
                    ex.getMessage();
                    ex.printStackTrace();
                    LoggingManager.printStackTrace(ex, logger);
                    
                }
                
                Thread.currentThread().sleep(1000 * 30);//wait for 30sec to get new Jobs List
            }
            
        } catch (InterruptedException ex) {//if Thread.sleep is interupted
            ex.getMessage();
            ex.printStackTrace();
            LoggingManager.printStackTrace(ex, logger);
        }
    }
    
    public static boolean isInterupted(String execID) {
        
        Integer curJobID =   FileExecutors.currentExecs.get(execID).getId();
        boolean isInterupted =   false;
        
        synchronized(JobStarter.class) {
            isInterupted =   JobStarter.interuptedJobIDsList.contains(curJobID);
        }
        if(isInterupted)
            logger.warn("jobID("+curJobID+") is Interupted by User while executing: "+execID);
        
        return isInterupted;
    }
}
