/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataArchiver;

import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.archivePolicy.dto.Job;

import org.apache.log4j.Logger;

/**
 *
 * @author YAwar
 */
public class JobThread implements Runnable {
    
    private Job j;
    private String executionID;
    
    public JobThread(Job j, String executionID) {
        this.j  =   j;
        this.executionID    =   executionID;
    }
    
    @Override
    public void run() {
        Logger logger = LoggingManager.getLogger(LoggingManager.ACTIVITY_ERROR, this.executionID);

        try {
            if (j == null) {
                throw new Exception("Job is null, Ops");
            }

            FileExecutors fe = new FileExecutors();
            fe.startJob(j, this.executionID);
            fe = null;

        } catch (Exception ex) {

            logger.fatal("JobStarter : " + ex);
            LoggingManager.printStackTrace(ex, logger);

        } finally {
            //Utility.resetUtilityResourceBundles();
            ////TEST COMMENT, to see affects...  
            ////LoggingManager.shutdownLogging(executionID);
            //System.gc();
        }
    }
}
