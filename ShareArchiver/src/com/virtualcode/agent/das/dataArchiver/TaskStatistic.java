/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataArchiver;

import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.archivePolicy.dto.JobStatistics;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.utils.MyHashMap;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Formatter;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.log4j.Logger;

/**
 *
 * @author Abbas
 */
public class TaskStatistic implements Runnable {

    //private final static AtomicLong executionCount = new AtomicLong(0);
    //private final static AtomicLong failedCount = new AtomicLong(0);
    private static MyHashMap comulativeSize =   new MyHashMap();
    private static MyHashMap executionCtr =   new MyHashMap();
    private static MyHashMap failedCtr =   new MyHashMap();
    
    private FileTaskInterface task = null;
    Logger errorLog = null;
    Logger taskSummary = null;

    public static void resetExecutionCtr(String execID) {
        executionCtr.put(execID, new AtomicLong(0));
    }
    public static String getExecutionCount(String execID) {
        return executionCtr.get(execID);
    }

    public static void resetFailedCtr(String execID) {
        failedCtr.put(execID, new AtomicLong(0));
    }
    public static String getFailedCount(String execID) {
        return failedCtr.get(execID);
    }
    
    public static void resetComulativeSize(String execID) {
    	comulativeSize.put(execID, new AtomicLong(0));
    }
    public static String getComulativeSize(String execID) {
        return comulativeSize.get(execID);
    }

    public TaskStatistic(FileTaskInterface task) {
        this.task = task;
        
        errorLog = LoggingManager.getLogger(LoggingManager.ACTIVITY_ERROR, this.task.getExecID());
        taskSummary = LoggingManager.getLogger(LoggingManager.TASK_SUMMARY, this.task.getExecID());
    }

    @Override
    public void run() {

        TaskKPI taskKPI =   task.getTaskKpi();
        String eID  =   task.getExecID();
        Job j   =   FileExecutors.currentExecs.get(eID);
        
        AtomicLong	tempSize	=	new AtomicLong(Long.parseLong(comulativeSize.get(eID)));
        if((!"NO".equals(task.getTaskKpi().isArchived) && !"AE".equals(task.getTaskKpi().isArchived)) ||	//if freshly archived successfuly
        		
        		(task.getTaskKpi().evaluatorFlag == EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND
                && j.getActionType().toLowerCase().trim().equals("evaluate")) //OR if Evaluate_Only Job
                ) {
        	
        	tempSize.addAndGet(task.getTaskKpi().getFileSize());
        }
        comulativeSize.put(eID, tempSize);//Store it into Static var, to be used in JobSummary
        
        JobStatistics   s  =   j.getJobStatistics();
        s.setArchivedVolume(tempSize.longValue());
        s.update(taskKPI);

        StringBuilder sb = new StringBuilder();
        Formatter formatter = new Formatter(sb, Locale.US);

        formatter.format("{ID:%s},{Eval: %d secs},{FSize: %d},{DeDup: %d secs},{Arch: %d secs},{Stub: %d secs}",
                task.getUid().toString(),
                (task.getTaskKpi().evalutionEndTime - task.getTaskKpi().evalutionStartTime) / 1000,
                (task.getTaskKpi().getFileSize()),
                (task.getTaskKpi().deDupCheckerEndTime - task.getTaskKpi().deDupCheckerStartTime) / 1000,
                (task.getTaskKpi().archiveEndTime - task.getTaskKpi().archiveStartTime) / 1000,
                (task.getTaskKpi().stubberEndTime - task.getTaskKpi().stubberStartTime) / 1000);

        taskSummary.info(sb.toString());
        
        //taskSummary.debug(sb.toString());
        
        /*
        //This is a useless code - safe to delete
        taskSummary.info(" Ev-P : " + NTFSEvaluater.getExecutionCount()
                + //(" DeDupd: " + FileDeDupChecker.getExecutionCount())+
                " Ev-D : " + FileExecutors.currentJob.getJobStatistics().getTotalEvaluated()
                + " Ar-P : " + FileArchiver.getExecutionCount()
                + " Ar-D : " + FileExecutors.currentJob.getJobStatistics().getTotalArchived()
                + (" St-P : " + FileStubber.getExecutionCount())
                + " St-D : " + FileExecutors.currentJob.getJobStatistics().getTotalStubbed()
                + (" Ta-Pf : " + TaskStatistic.getFailedCount())
                + (" Ta-P : " + TaskStatistic.getExecutionCount()));
         */

        if (task.getTaskKpi().errorDetails != null) {
            //failedCount.addAndGet(1);
            failedCtr.increment(task.getExecID());
            Writer writer = new StringWriter();
            PrintWriter printWriter = new PrintWriter(writer);
            task.getTaskKpi().errorDetails.printStackTrace(printWriter);
            
            errorLog.info("F : " + task.getUid().toString() + " : " + task.getTaskKpi().errorDetails.getMessage() + "\n" + writer.toString());
            writer = null;
            printWriter = null;
            
        }
        executionCtr.increment(task.getExecID());
        task.closeCurrentTask(true);
        //taskSummary.info("Task Statistics for ("+this.task.getExecID()+") becomes "+executionCtr.get(task.getExecID()));
        //task = null;
        //executionCount.addAndGet(1);
    }
}
