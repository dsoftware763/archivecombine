/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.Executors;

import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.archivePolicy.dto.JobStatistics;

import com.virtualcode.agent.das.archivePolicy.dto.SPDocLib;
import com.virtualcode.agent.das.documentManagement.dto.DocumentType;
import com.virtualcode.services.JobService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.services.impl.JobServiceImpl;
import com.virtualcode.util.SpringApplicationContext;
//import com.ws.db.agent.AgentManagementService;
//import com.ws.db.agent.AgentManagementServiceService;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javax.xml.ws.BindingProvider;
import org.apache.log4j.Logger;

public class DataLayer {

    private static Logger logger    =   Logger.getLogger(DataLayer.class);
    
    public static int setStatusForScheduledJobs() {
//    	return DataLayer.agentManagementService.setStatusForScheduledJobs(agentName);
    	return DataLayer.agentManagementService.setStatusForScheduledJobs();
    }
    
    public static List<Job> JobsbyAgent(String AgentName, String actionType) throws Exception {
        //Todo Code to get Jobs from DB
        List<Job> jobs = new ArrayList<Job>();

        try {
        	System.out.println("DataLayer Searching jobs for agent: " + AgentName);
            byte[] jobListByteArray = getJobListForProcessing(AgentName, actionType);
            //byte[] jobListByteArray = DataLayer.agentManagementService.getJobListForProcessing(AgentName, actionType);

            try {
                ByteArrayInputStream bis = new ByteArrayInputStream(jobListByteArray);
                ObjectInputStream ois = new ObjectInputStream(bis);
                jobs = (ArrayList<Job>) ois.readObject();
                ois.close();
                bis.close();
                bis = null;
                ois = null;
            } catch (IOException ex) {
                ex.printStackTrace();
            } catch (ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        } catch (NullPointerException e) {
            logger.info("no job found");
        }
        return jobs;
    }
    
    public static List<Integer> interuptedJobs() throws Exception {
    	
    	List<Integer> jobIDs	=	null;
    	try {
	    	//JobService jServ		=	new JobServiceImpl();
	    	jobIDs					=	jobService.getCancelJobs();
	    	if(jobIDs!=null)
	    		logger.info("Size of Interupted Jobs List: "+jobIDs.toString() + " - "+jobIDs.size());
	    	else
	    		logger.info("Size of Interupted Jobs List: NULL");
	    	
    	} catch(Exception ex) {
        	logger.info("no interupted job id found");
        	
        } finally {
        	if(jobIDs==null) {
	        	jobIDs	=	new ArrayList<Integer>();
	        }
        }
        
        return jobIDs;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            testGetJobListForProcessing();
            //testExecutionStarted();
            //testExecutionCompleted();
        } catch (Exception ex) {
        }
    }
    /*
     * *************************************************************************
     * web service methods
     * *************************************************************************
     */

    public static byte[] getJobListForProcessing(java.lang.String AgentName, java.lang.String actionType) throws Exception {
        /*AgentManagementServiceService service = new AgentManagementServiceService();
        AgentManagementServiceImpl port = service.getAgentManagementServicePort();
        Map ctxt = ((BindingProvider) port).getRequestContext();
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, Utility.GetProp("AgentWS") + "/AgentManagementService?wsdl");*/

        //return port.getJobListForProcessing(arg0, actionType);
    	System.out.println("service:  "+DataLayer.agentManagementService);
        return DataLayer.agentManagementService.getJobListForProcessing(AgentName, actionType);
        
    }

    public static String executionStarted(int jobID) throws Exception {
        /*AgentManagementServiceService service = new AgentManagementServiceService();
        AgentManagementServiceImpl port = service.getAgentManagementServicePort();
        Map ctxt = ((BindingProvider) port).getRequestContext();
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,Utility.GetProp("AgentWS") + "/AgentManagementService?wsdl");
        return port.executionStarted(arg0);*/
    	return DataLayer.agentManagementService.executionStarted(jobID);
        
    }

    
    public static boolean exportExecutionCompleted(int JobID, String ExecutionID, int ErrorCodeID, JobStatistics jobKPI) throws Exception {
      /*  AgentManagementServiceService service = new AgentManagementServiceService();
        AgentManagementServiceImpl port = service.getAgentManagementServicePort();
        Map ctxt = ((BindingProvider) port).getRequestContext();
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, Utility.GetProp("AgentWS") + "/AgentManagementService?wsdl");
        */
        Logger logger = LoggingManager.getLogger(LoggingManager.EXP_JOB_SUMMARY, ExecutionID);

        logger.info("KPIs are"+(jobKPI!=null? " not ":"")+"null  " + ", Export JobID="+JobID);
    
        if (jobKPI != null) {
            com.virtualcode.vo.JobStatistics jobStatistics =  new com.virtualcode.vo.JobStatistics();
            
            jobKPI.setExecutionID(ExecutionID);
            jobKPI.setTotalFailedEvaluating(0l);
            
            jobStatistics.setExecutionId(ExecutionID);
            //jobStatistics.setJobStartTime(Utility.convertDate(jobKPI.getJobStartTime()));
            jobStatistics.setJobStartTime(Utility.convertDateToTimeStamp(jobKPI.getJobStartTime()));
            //jobStatistics.setJobEndTime(Utility.convertDate(jobKPI.getJobEndTime()));
            jobStatistics.setJobEndTime(Utility.convertDateToTimeStamp(jobKPI.getJobEndTime()));
            jobStatistics.setSkippedProcessfileSkippedHiddenlected(jobKPI.getSkippedProcessFileSkippedHiddenLected());
            jobStatistics.setSkippedProcessfileSkippedIsdir(jobKPI.getSkippedProcessFileSkippedIsDir());
            jobStatistics.setSkippedProcessfileSkippedMismatchAge(jobKPI.getSkippedProcessFileSkippedMismatchAge());
            jobStatistics.setSkippedProcessfileSkippedMismatchExtension(jobKPI.getSkippedProcessFileSkippedMismatchExtension());
            jobStatistics.setSkippedProcessfileSkippedMismatchLastaccesstime(jobKPI.getSkippedProcessFileSkippedMismatchLastAccessTime());
            jobStatistics.setSkippedProcessfileSkippedMismatchLastmodifiedtime(jobKPI.getSkippedProcessFileSkippedMismatchLastModifiedTime());
            jobStatistics.setSkippedProcessfileSkippedReadOnly(jobKPI.getSkippedProcessFileSkippedReadOnly());
            jobStatistics.setSkippedProcessfileSkippedSymbollink(jobKPI.getSkippedProcessFileSkippedSymbolLink());
            jobStatistics.setSkippedProcessfileSkippedTemporary(jobKPI.getSkippedProcessFileSkippedTemporary());
            jobStatistics.setSkippedProcessfileSkippedTooLarge(jobKPI.getSkippedProcessFileSkippedTooLarge());
            jobStatistics.setTotalFreshArchived(jobKPI.getTotalFreshArchived());
            jobStatistics.setAlreadyArchived(jobKPI.getAlreadyArchived());
            jobStatistics.setTotalDuplicated(jobKPI.getTotalDuplicated());
            jobStatistics.setTotalEvaluated(jobKPI.getTotalEvaluated());
            jobStatistics.setTotalFailedArchiving(jobKPI.getTotalFailedArchiving());
            jobStatistics.setTotalFailedDeduplication(jobKPI.getTotalFailedDeduplication());
            jobStatistics.setTotalFailedEvaluating(0l);
            jobStatistics.setTotalFailedStubbing(jobKPI.getTotalFailedStubbing());
            jobStatistics.setTotalStubbed(jobKPI.getTotalStubbed());
            jobStatistics.setArchivedVolume(jobKPI.getArchivedVolume());
    
            logger.info("Execution Statistics for Job  " + ", JobID="+JobID);
            logger.info("=======================================");
            logger.info("Summary Statistics");
            logger.info("Total Evaluated: : " + jobStatistics.getTotalEvaluated());
            long failed = jobStatistics.getSkippedProcessfileSkippedReadOnly()
                    + jobStatistics.getSkippedProcessfileSkippedHiddenlected()
                    + jobStatistics.getSkippedProcessfileSkippedTooLarge()
                    + jobStatistics.getSkippedProcessfileSkippedTemporary()
                    + jobStatistics.getSkippedProcessfileSkippedSymbollink()
                    + jobStatistics.getSkippedProcessfileSkippedIsdir()
                    + jobStatistics.getSkippedProcessfileSkippedMismatchAge()
                    + jobStatistics.getSkippedProcessfileSkippedMismatchLastaccesstime()
                    + jobStatistics.getSkippedProcessfileSkippedMismatchLastmodifiedtime()
                    + jobStatistics.getSkippedProcessfileSkippedMismatchExtension();
            
            logger.info("Total Skipped: " + failed);
            //logger.info("Total Duplicated: " + jobStatistics.getTotalDuplicated());
            logger.info("Total Exported: " + jobStatistics.getTotalFreshArchived());
            logger.info("Already Exported: " + jobStatistics.getAlreadyArchived());
            logger.info("Total Exported Vol: " + jobStatistics.getArchivedVolume());
            logger.info("Total Failed"
                    + (jobStatistics.getTotalFailedDeduplication()
                    + jobStatistics.getTotalFailedEvaluating()
                    + jobStatistics.getTotalFailedStubbing()
                    + jobStatistics.getTotalFailedArchiving()));
            logger.info("");
            logger.info("Failure Statistics");
            logger.info("Total Failed Exported: " + jobStatistics.getTotalFailedArchiving());
            logger.info("Total Failed Evaluating: " + jobStatistics.getTotalFailedEvaluating());
            //logger.info("Total Failed Stubbing: " + jobStatistics.getTotalFailedStubbing());
            //logger.info("Total Failed Deduplication: " + jobStatistics.getTotalFailedDeduplication());
            logger.info("");
            logger.info("Skipped Detail");
            logger.info("Skipped ReadOnly: " + jobStatistics.getSkippedProcessfileSkippedReadOnly());
            logger.info("Skipped Hidden: " + jobStatistics.getSkippedProcessfileSkippedHiddenlected());
            logger.info("Skipped Too Large: " + jobStatistics.getSkippedProcessfileSkippedTooLarge());
            logger.info("Skipped Temporary: " + jobStatistics.getSkippedProcessfileSkippedTemporary());
            logger.info("Skipped Symbol Link: " + jobStatistics.getSkippedProcessfileSkippedSymbollink());
            logger.info("Skipped Directory: " + jobStatistics.getSkippedProcessfileSkippedIsdir());
            logger.info("Skipped Age Mismatch: " + jobStatistics.getSkippedProcessfileSkippedMismatchAge());
            logger.info("Skipped Mismatch Last AccessTime: " + jobStatistics.getSkippedProcessfileSkippedMismatchLastaccesstime());
            logger.info("Skipped Mismatch Last ModifiedTime: " + jobStatistics.getSkippedProcessfileSkippedMismatchLastmodifiedtime());
            logger.info("Skipped Mismatch Extension: " + jobStatistics.getSkippedProcessfileSkippedMismatchExtension());

            logger.info("Job Start Time :" + jobStatistics.getJobStartTime().toString());
            logger.info("Job End Time :" + jobStatistics.getJobEndTime().toString());
            

            //return port.executionCompleted(JobID, ExecutionID, ErrorCodeID, jobStatistics);
            return DataLayer.agentManagementService.executionCompleted(JobID, ExecutionID, ErrorCodeID, jobKPI);
        }
        //return port.executionCompleted(JobID, ExecutionID, ErrorCodeID, null);
        return DataLayer.agentManagementService.executionCompleted(JobID, ExecutionID, ErrorCodeID, null);
    }
    
    public static boolean indexExecutionCompleted(int JobID, String ExecutionID, int ErrorCodeID, JobStatistics jobKPI) throws Exception {
    	Logger logger = LoggingManager.getLogger(LoggingManager.INDX_JOB_SUMMARY, ExecutionID);

          logger.info("KPIs are"+(jobKPI!=null? " not ":"")+"null  " + ", Indexing JobID="+JobID);
      
          if (jobKPI != null) {
              com.virtualcode.vo.JobStatistics jobStatistics =  new com.virtualcode.vo.JobStatistics();
              
              jobKPI.setExecutionID(ExecutionID);
              jobKPI.setTotalFailedEvaluating(0l);
              
              jobStatistics.setExecutionId(ExecutionID);
              //jobStatistics.setJobStartTime(Utility.convertDate(jobKPI.getJobStartTime()));
              jobStatistics.setJobStartTime(Utility.convertDateToTimeStamp(jobKPI.getJobStartTime()));
              //jobStatistics.setJobEndTime(Utility.convertDate(jobKPI.getJobEndTime()));
              jobStatistics.setJobEndTime(Utility.convertDateToTimeStamp(jobKPI.getJobEndTime()));
              jobStatistics.setSkippedProcessfileSkippedHiddenlected(jobKPI.getSkippedProcessFileSkippedHiddenLected());
              jobStatistics.setSkippedProcessfileSkippedIsdir(jobKPI.getSkippedProcessFileSkippedIsDir());
              jobStatistics.setSkippedProcessfileSkippedMismatchAge(jobKPI.getSkippedProcessFileSkippedMismatchAge());
              jobStatistics.setSkippedProcessfileSkippedMismatchExtension(jobKPI.getSkippedProcessFileSkippedMismatchExtension());
              jobStatistics.setSkippedProcessfileSkippedMismatchLastaccesstime(jobKPI.getSkippedProcessFileSkippedMismatchLastAccessTime());
              jobStatistics.setSkippedProcessfileSkippedMismatchLastmodifiedtime(jobKPI.getSkippedProcessFileSkippedMismatchLastModifiedTime());
              jobStatistics.setSkippedProcessfileSkippedReadOnly(jobKPI.getSkippedProcessFileSkippedReadOnly());
              jobStatistics.setSkippedProcessfileSkippedSymbollink(jobKPI.getSkippedProcessFileSkippedSymbolLink());
              jobStatistics.setSkippedProcessfileSkippedTemporary(jobKPI.getSkippedProcessFileSkippedTemporary());
              jobStatistics.setSkippedProcessfileSkippedTooLarge(jobKPI.getSkippedProcessFileSkippedTooLarge());
              jobStatistics.setTotalFreshArchived(jobKPI.getTotalFreshArchived());
              jobStatistics.setAlreadyArchived(jobKPI.getAlreadyArchived());
              jobStatistics.setTotalDuplicated(jobKPI.getTotalDuplicated());
              jobStatistics.setTotalEvaluated(jobKPI.getTotalEvaluated());
              jobStatistics.setTotalFailedArchiving(jobKPI.getTotalFailedArchiving());
              jobStatistics.setTotalFailedDeduplication(jobKPI.getTotalFailedDeduplication());
              jobStatistics.setTotalFailedEvaluating(0l);
              jobStatistics.setTotalFailedStubbing(jobKPI.getTotalFailedStubbing());
              jobStatistics.setTotalStubbed(jobKPI.getTotalStubbed());
              jobStatistics.setArchivedVolume(jobKPI.getArchivedVolume());
      
              logger.info("Indexing Statistics for Job  " + ", JobID="+JobID);
              logger.info("=======================================");
              logger.info("Summary Statistics");
              logger.info("Total Evaluated: : " + jobStatistics.getTotalEvaluated());
              long failed = jobStatistics.getSkippedProcessfileSkippedReadOnly()
                      + jobStatistics.getSkippedProcessfileSkippedHiddenlected()
                      + jobStatistics.getSkippedProcessfileSkippedTooLarge()
                      + jobStatistics.getSkippedProcessfileSkippedTemporary()
                      + jobStatistics.getSkippedProcessfileSkippedSymbollink()
                      + jobStatistics.getSkippedProcessfileSkippedIsdir()
                      + jobStatistics.getSkippedProcessfileSkippedMismatchAge()
                      + jobStatistics.getSkippedProcessfileSkippedMismatchLastaccesstime()
                      + jobStatistics.getSkippedProcessfileSkippedMismatchLastmodifiedtime()
                      + jobStatistics.getSkippedProcessfileSkippedMismatchExtension();
              
              logger.info("Total Skipped: " + failed);
              logger.info("Total Indexed: " + jobStatistics.getTotalFreshArchived());
              logger.info("Total Indexed Vol: " + jobStatistics.getArchivedVolume());
              logger.info("Total Failed"
                      + (jobStatistics.getTotalFailedDeduplication()
                      + jobStatistics.getTotalFailedEvaluating()
                      + jobStatistics.getTotalFailedStubbing()
                      + jobStatistics.getTotalFailedArchiving()));
              logger.info("");
              logger.info("Failure Statistics");
              logger.info("Total Failed Indexed: " + jobStatistics.getTotalFailedArchiving());
              logger.info("Total Failed Evaluating: " + jobStatistics.getTotalFailedEvaluating());
              //logger.info("Total Failed Stubbing: " + jobStatistics.getTotalFailedStubbing());
              logger.info("");
              logger.info("Skipped Detail");
              logger.info("Skipped ReadOnly: " + jobStatistics.getSkippedProcessfileSkippedReadOnly());
              logger.info("Skipped Hidden: " + jobStatistics.getSkippedProcessfileSkippedHiddenlected());
              logger.info("Skipped Too Large: " + jobStatistics.getSkippedProcessfileSkippedTooLarge());
              logger.info("Skipped Temporary: " + jobStatistics.getSkippedProcessfileSkippedTemporary());
              logger.info("Skipped Symbol Link: " + jobStatistics.getSkippedProcessfileSkippedSymbollink());
              logger.info("Skipped Directory: " + jobStatistics.getSkippedProcessfileSkippedIsdir());
              logger.info("Skipped Age Mismatch: " + jobStatistics.getSkippedProcessfileSkippedMismatchAge());
              logger.info("Skipped Mismatch Last AccessTime: " + jobStatistics.getSkippedProcessfileSkippedMismatchLastaccesstime());
              logger.info("Skipped Mismatch Last ModifiedTime: " + jobStatistics.getSkippedProcessfileSkippedMismatchLastmodifiedtime());
              logger.info("Skipped Mismatch Extension: " + jobStatistics.getSkippedProcessfileSkippedMismatchExtension());

              logger.info("Job Start Time :" + jobStatistics.getJobStartTime().toString());
              logger.info("Job End Time :" + jobStatistics.getJobEndTime().toString());
              

              //return port.executionCompleted(JobID, ExecutionID, ErrorCodeID, jobStatistics);
              return DataLayer.agentManagementService.executionCompleted(JobID, ExecutionID, ErrorCodeID, jobKPI);
          }
          //return port.executionCompleted(JobID, ExecutionID, ErrorCodeID, null);
          return DataLayer.agentManagementService.executionCompleted(JobID, ExecutionID, ErrorCodeID, null);
      }
    

    public static boolean restoreExecutionCompleted(int JobID, String ExecutionID, int ErrorCodeID, JobStatistics jobKPI) throws Exception {
    	Logger logger = LoggingManager.getLogger(LoggingManager.INDX_JOB_SUMMARY, ExecutionID);

          logger.info("KPIs are"+(jobKPI!=null? " not ":"")+"null  " + ", Restore JobID="+JobID);
      
          if (jobKPI != null) {
              com.virtualcode.vo.JobStatistics jobStatistics =  new com.virtualcode.vo.JobStatistics();
              
              jobKPI.setExecutionID(ExecutionID);
              jobKPI.setTotalFailedEvaluating(0l);
              
              jobStatistics.setExecutionId(ExecutionID);
              //jobStatistics.setJobStartTime(Utility.convertDate(jobKPI.getJobStartTime()));
              jobStatistics.setJobStartTime(Utility.convertDateToTimeStamp(jobKPI.getJobStartTime()));
              //jobStatistics.setJobEndTime(Utility.convertDate(jobKPI.getJobEndTime()));
              jobStatistics.setJobEndTime(Utility.convertDateToTimeStamp(jobKPI.getJobEndTime()));
              jobStatistics.setSkippedProcessfileSkippedHiddenlected(jobKPI.getSkippedProcessFileSkippedHiddenLected());
              jobStatistics.setSkippedProcessfileSkippedIsdir(jobKPI.getSkippedProcessFileSkippedIsDir());
              jobStatistics.setSkippedProcessfileSkippedMismatchAge(jobKPI.getSkippedProcessFileSkippedMismatchAge());
              jobStatistics.setSkippedProcessfileSkippedMismatchExtension(jobKPI.getSkippedProcessFileSkippedMismatchExtension());
              jobStatistics.setSkippedProcessfileSkippedMismatchLastaccesstime(jobKPI.getSkippedProcessFileSkippedMismatchLastAccessTime());
              jobStatistics.setSkippedProcessfileSkippedMismatchLastmodifiedtime(jobKPI.getSkippedProcessFileSkippedMismatchLastModifiedTime());
              jobStatistics.setSkippedProcessfileSkippedReadOnly(jobKPI.getSkippedProcessFileSkippedReadOnly());
              jobStatistics.setSkippedProcessfileSkippedSymbollink(jobKPI.getSkippedProcessFileSkippedSymbolLink());
              jobStatistics.setSkippedProcessfileSkippedTemporary(jobKPI.getSkippedProcessFileSkippedTemporary());
              jobStatistics.setSkippedProcessfileSkippedTooLarge(jobKPI.getSkippedProcessFileSkippedTooLarge());
              jobStatistics.setTotalFreshArchived(jobKPI.getTotalFreshArchived());
              jobStatistics.setAlreadyArchived(jobKPI.getAlreadyArchived());
              jobStatistics.setTotalDuplicated(jobKPI.getTotalDuplicated());
              jobStatistics.setTotalEvaluated(jobKPI.getTotalEvaluated());
              jobStatistics.setTotalFailedArchiving(jobKPI.getTotalFailedArchiving());
              jobStatistics.setTotalFailedDeduplication(jobKPI.getTotalFailedDeduplication());
              jobStatistics.setTotalFailedEvaluating(0l);
              jobStatistics.setTotalFailedStubbing(jobKPI.getTotalFailedStubbing());
              jobStatistics.setTotalStubbed(jobKPI.getTotalStubbed());
              jobStatistics.setArchivedVolume(jobKPI.getArchivedVolume());
      
              logger.info("Restore Statistics for Job  " + ", JobID="+JobID);
              logger.info("=======================================");
              logger.info("Summary Statistics");
              logger.info("Total Evaluated: : " + jobStatistics.getTotalEvaluated());
              long failed = jobStatistics.getSkippedProcessfileSkippedReadOnly()
                      + jobStatistics.getSkippedProcessfileSkippedHiddenlected()
                      + jobStatistics.getSkippedProcessfileSkippedTooLarge()
                      + jobStatistics.getSkippedProcessfileSkippedTemporary()
                      + jobStatistics.getSkippedProcessfileSkippedSymbollink()
                      + jobStatistics.getSkippedProcessfileSkippedIsdir()
                      + jobStatistics.getSkippedProcessfileSkippedMismatchAge()
                      + jobStatistics.getSkippedProcessfileSkippedMismatchLastaccesstime()
                      + jobStatistics.getSkippedProcessfileSkippedMismatchLastmodifiedtime()
                      + jobStatistics.getSkippedProcessfileSkippedMismatchExtension();
              
              logger.info("Total Skipped: " + failed);
              logger.info("Total Restored: " + jobStatistics.getTotalFreshArchived());
              logger.info("Total Restored Vol: " + jobStatistics.getArchivedVolume());
              logger.info("Total Failed"
                      + (jobStatistics.getTotalFailedDeduplication()
                      + jobStatistics.getTotalFailedEvaluating()
                      + jobStatistics.getTotalFailedStubbing()
                      + jobStatistics.getTotalFailedArchiving()));
              logger.info("");
              logger.info("Failure Statistics");
              logger.info("Total Failed Restored: " + jobStatistics.getTotalFailedArchiving());
              logger.info("Total Failed Evaluating: " + jobStatistics.getTotalFailedEvaluating());
              //logger.info("Total Failed Stubbing: " + jobStatistics.getTotalFailedStubbing());
              logger.info("");
              logger.info("Skipped Detail");
              logger.info("Skipped ReadOnly: " + jobStatistics.getSkippedProcessfileSkippedReadOnly());
              logger.info("Skipped Hidden: " + jobStatistics.getSkippedProcessfileSkippedHiddenlected());
              logger.info("Skipped Too Large: " + jobStatistics.getSkippedProcessfileSkippedTooLarge());
              logger.info("Skipped Temporary: " + jobStatistics.getSkippedProcessfileSkippedTemporary());
              logger.info("Skipped Non-Stubs: " + jobStatistics.getSkippedProcessfileSkippedSymbollink());
              logger.info("Skipped Directory: " + jobStatistics.getSkippedProcessfileSkippedIsdir());
              logger.info("Skipped Age Mismatch: " + jobStatistics.getSkippedProcessfileSkippedMismatchAge());
              logger.info("Skipped Mismatch Last AccessTime: " + jobStatistics.getSkippedProcessfileSkippedMismatchLastaccesstime());
              logger.info("Skipped Mismatch Last ModifiedTime: " + jobStatistics.getSkippedProcessfileSkippedMismatchLastmodifiedtime());
              logger.info("Skipped Mismatch Extension: " + jobStatistics.getSkippedProcessfileSkippedMismatchExtension());

              logger.info("Job Start Time :" + jobStatistics.getJobStartTime().toString());
              logger.info("Job End Time :" + jobStatistics.getJobEndTime().toString());
              

              //return port.executionCompleted(JobID, ExecutionID, ErrorCodeID, jobStatistics);
              return DataLayer.agentManagementService.executionCompleted(JobID, ExecutionID, ErrorCodeID, jobKPI);
          }
          //return port.executionCompleted(JobID, ExecutionID, ErrorCodeID, null);
          return DataLayer.agentManagementService.executionCompleted(JobID, ExecutionID, ErrorCodeID, null);
      }
    
    public static boolean executionCompleted(int JobID, String ExecutionID, int ErrorCodeID, JobStatistics jobKPI) throws Exception {
        /*AgentManagementServiceService service = new AgentManagementServiceService();
        AgentManagementServiceImpl port = service.getAgentManagementServicePort();
        Map ctxt = ((BindingProvider) port).getRequestContext();
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, Utility.GetProp("AgentWS") + "/AgentManagementService?wsdl");*/
        Logger logger = LoggingManager.getLogger(LoggingManager.JOB_SUMMARY, ExecutionID);

        logger.info("KPIs are"+(jobKPI!=null? " not ":"")+"null  " + ", JobID="+JobID);
    
        if (jobKPI != null) {
            com.virtualcode.vo.JobStatistics jobStatistics = new com.virtualcode.vo.JobStatistics();
            
            jobKPI.setExecutionID(ExecutionID);
            jobKPI.setTotalFailedEvaluating(0l);
            
            jobStatistics.setExecutionId(ExecutionID);
            jobStatistics.setJobStartTime(Utility.convertDateToTimeStamp(jobKPI.getJobStartTime()));
            jobStatistics.setJobEndTime(Utility.convertDateToTimeStamp(jobKPI.getJobEndTime()));
            jobStatistics.setSkippedProcessfileSkippedHiddenlected((jobKPI.getSkippedProcessFileSkippedHiddenLected()));
            jobStatistics.setSkippedProcessfileSkippedIsdir(jobKPI.getSkippedProcessFileSkippedIsDir());
            jobStatistics.setSkippedProcessfileSkippedMismatchAge(jobKPI.getSkippedProcessFileSkippedMismatchAge());
            jobStatistics.setSkippedProcessfileSkippedMismatchExtension(jobKPI.getSkippedProcessFileSkippedMismatchExtension());
            jobStatistics.setSkippedProcessfileSkippedMismatchLastaccesstime(jobKPI.getSkippedProcessFileSkippedMismatchLastAccessTime());
            jobStatistics.setSkippedProcessfileSkippedMismatchLastmodifiedtime(jobKPI.getSkippedProcessFileSkippedMismatchLastModifiedTime());
            jobStatistics.setSkippedProcessfileSkippedReadOnly(jobKPI.getSkippedProcessFileSkippedReadOnly());
            jobStatistics.setSkippedProcessfileSkippedSymbollink(jobKPI.getSkippedProcessFileSkippedSymbolLink());
            jobStatistics.setSkippedProcessfileSkippedTemporary(jobKPI.getSkippedProcessFileSkippedTemporary());
            jobStatistics.setSkippedProcessfileSkippedTooLarge(jobKPI.getSkippedProcessFileSkippedTooLarge());
            jobStatistics.setTotalFreshArchived(jobKPI.getTotalFreshArchived());
            jobStatistics.setAlreadyArchived(jobKPI.getAlreadyArchived());
            jobStatistics.setTotalDuplicated(jobKPI.getTotalDuplicated());
            jobStatistics.setTotalEvaluated(jobKPI.getTotalEvaluated());
            jobStatistics.setTotalFailedArchiving(jobKPI.getTotalFailedArchiving());
            jobStatistics.setTotalFailedDeduplication(jobKPI.getTotalFailedDeduplication());
            jobStatistics.setTotalFailedEvaluating(0l);
            jobStatistics.setTotalFailedStubbing(jobKPI.getTotalFailedStubbing());
            jobStatistics.setTotalStubbed(jobKPI.getTotalStubbed());
            jobStatistics.setArchivedVolume(jobKPI.getArchivedVolume());
    
            logger.info("Execution Statistics for Job  " + ", JobID="+JobID);
            logger.info("=======================================");
            logger.info("Summary Statistics");
            logger.info("Total Evaluated: : " + jobStatistics.getTotalEvaluated());
            long failed = jobStatistics.getSkippedProcessfileSkippedReadOnly()
                    + jobStatistics.getSkippedProcessfileSkippedHiddenlected()
                    + jobStatistics.getSkippedProcessfileSkippedTooLarge()
                    + jobStatistics.getSkippedProcessfileSkippedTemporary()
                    + jobStatistics.getSkippedProcessfileSkippedSymbollink()
                    + jobStatistics.getSkippedProcessfileSkippedIsdir()
                    + jobStatistics.getSkippedProcessfileSkippedMismatchAge()
                    + jobStatistics.getSkippedProcessfileSkippedMismatchLastaccesstime()
                    + jobStatistics.getSkippedProcessfileSkippedMismatchLastmodifiedtime()
                    + jobStatistics.getSkippedProcessfileSkippedMismatchExtension();
            
            logger.info("Total Skipped: " + failed);
            logger.info("Total Duplicated: " + jobStatistics.getTotalDuplicated());
            logger.info("Fresh Archived: " + jobStatistics.getTotalFreshArchived());
            logger.info("Already Archived: " + jobStatistics.getAlreadyArchived());
            logger.info("Total Archived Vol: " + jobStatistics.getArchivedVolume());
            logger.info("Total Stubbed: " + jobStatistics.getTotalStubbed());
            logger.info("Total Failed: "
                    + (jobStatistics.getTotalFailedDeduplication()
                    + jobStatistics.getTotalFailedEvaluating()
                    + jobStatistics.getTotalFailedStubbing()
                    + jobStatistics.getTotalFailedArchiving()));
            logger.info("");
            logger.info("Failure Statistics");
            logger.info("Total Failed Archiving: " + jobStatistics.getTotalFailedArchiving());
            logger.info("Total Failed Evaluating: " + jobStatistics.getTotalFailedEvaluating());
            logger.info("Total Failed Stubbing: " + jobStatistics.getTotalFailedStubbing());
            logger.info("Total Failed Deduplication: " + jobStatistics.getTotalFailedDeduplication());
            logger.info("");
            logger.info("Skipped Detail");
            logger.info("Skipped ReadOnly: " + jobStatistics.getSkippedProcessfileSkippedReadOnly());
            logger.info("Skipped Hidden: " + jobStatistics.getSkippedProcessfileSkippedHiddenlected());
            logger.info("Skipped Too Large: " + jobStatistics.getSkippedProcessfileSkippedTooLarge());
            logger.info("Skipped Temporary: " + jobStatistics.getSkippedProcessfileSkippedTemporary());
            logger.info("Skipped Symbol Link: " + jobStatistics.getSkippedProcessfileSkippedSymbollink());
            logger.info("Skipped Directory: " + jobStatistics.getSkippedProcessfileSkippedIsdir());
            logger.info("Skipped Age Mismatch: " + jobStatistics.getSkippedProcessfileSkippedMismatchAge());
            logger.info("Skipped Mismatch Last AccessTime: " + jobStatistics.getSkippedProcessfileSkippedMismatchLastaccesstime());
            logger.info("Skipped Mismatch Last ModifiedTime: " + jobStatistics.getSkippedProcessfileSkippedMismatchLastmodifiedtime());
            logger.info("Skipped Mismatch Extension: " + jobStatistics.getSkippedProcessfileSkippedMismatchExtension());

            logger.info("Job Start Time :" + jobStatistics.getJobStartTime().toString());
            logger.info("Job End Time :" + jobStatistics.getJobEndTime().toString());
            

            //return port.executionCompleted(JobID, ExecutionID, ErrorCodeID, jobStatistics);
            return DataLayer.agentManagementService.executionCompleted(JobID, ExecutionID, ErrorCodeID, jobKPI);
        }
        //return port.executionCompleted(JobID, ExecutionID, ErrorCodeID, null);
        return DataLayer.agentManagementService.executionCompleted(JobID, ExecutionID, ErrorCodeID, null);
    }

    /*
     * *************************************************************************
     * test methods for web service
     * *************************************************************************
     */
    public static void testGetJobListForProcessing() throws Exception {
        String agentName = "Sharearchiver";   //file-system agent
        String actionType   =   "IMPORT";//"EXPORT";
        //String agentName = "aSPAgent";  // sharepoint agent
        List<Job> jobList = new ArrayList<Job>();

        logger.info("getting job list...");

        try {
            byte[] jobListByteArray = getJobListForProcessing(agentName, actionType);

            try {
                ByteArrayInputStream bis = new ByteArrayInputStream(jobListByteArray);
                ObjectInputStream ois = new ObjectInputStream(bis);
                jobList = (ArrayList<Job>) ois.readObject();
            } catch (IOException ex) {
                ex.printStackTrace();
            } catch (ClassNotFoundException ex) {
                ex.printStackTrace();
            }


            //display results
            if (jobList.size() == 0) {
                logger.info("no job found");
            } else {
                logger.info("job count : " + jobList.size());
                for (Job job : jobList) {
                    logger.info("----------------------------------------------");
                    logger.info("job name :" + job.getJobName());
                    logger.info("job type:" + job.getActionType());
                    logger.info("job -> spDocLibs :");

                    Iterator iterator = job.getSpDocLibSet().iterator();
                    while (iterator.hasNext()) {
                        SPDocLib spDocLib = (SPDocLib) iterator.next();
                        logger.info("\t name: " + spDocLib.getLibraryName());
                    }

                    logger.info("job -> policy name : " + job.getPolicy().getPolicyName());
                    logger.info("job -> policy -> document-types: ");

                    Iterator newIterator = job.getPolicy().getDocumentTypeSet().iterator();
                    while (newIterator.hasNext()) {
                        DocumentType documentType = (DocumentType) newIterator.next();
                        logger.info("\t documentType name: " + documentType.getName());
                        logger.info("\t documentType name: " + documentType.getValue());
                    }
                }
            }
        } catch (NullPointerException e) {
            logger.info("no job found");
        }

    }

    public static void testExecutionStarted() throws Exception {
        int jobID = 1;
        String executionID = executionStarted(jobID);
    }

    public static void testExecutionCompleted() throws Exception {
        int newJobID = 1;
        String newExecutionID = generateExecutionID(newJobID);
        int errorCodeID = 0;    //errorCode.errorCodeID: 0 means success

//        JobStatistics jobStatistics = new JobStatistics();
//        jobStatistics.setExecutionID(newExecutionID);
//        jobStatistics.setJobStartTime(Utility.getCurrentDate());
//        jobStatistics.setJobEndTime(Utility.getCurrentDate());
//        jobStatistics.setSkippedProcessFileSkippedHiddenLected(100);
//        jobStatistics.setSkippedProcessFileSkippedIsDir(100);
//        jobStatistics.setSkippedProcessFileSkippedMismatchAge(100);
//        jobStatistics.setSkippedProcessFileSkippedMismatchExtension(100);
//        jobStatistics.setSkippedProcessFileSkippedMismatchLastAccessTime(100);
//        jobStatistics.setSkippedProcessFileSkippedMismatchLastModifiedTime(100);
//        jobStatistics.setSkippedProcessFileSkippedReadOnly(100);
//        jobStatistics.setSkippedProcessFileSkippedSymbolLink(100);
//        jobStatistics.setSkippedProcessFileSkippedTemporary(100);
//        jobStatistics.setSkippedProcessFileSkippedTooLarge(100);
//        jobStatistics.setTotalArchived(100);
//        jobStatistics.setTotalDuplicated(100);
//        jobStatistics.setTotalEvaluated(100);
//        jobStatistics.setTotalFailedArchiving(100);
//        jobStatistics.setTotalFailedDeduplication(100);
//        jobStatistics.setTotalFailedEvaluating(100);
//        jobStatistics.setTotalFailedStubbing(100);
//        jobStatistics.setTotalStubbed(100);
//
//
//        boolean saved = executionCompleted(newJobID, newExecutionID, errorCodeID, jobStatistics);

//        if (saved == true) {
//            logger.info("success");
//        } else {
//            logger.info("failed");
//        }


    }

    private static String generateExecutionID(int jobID) {

        int NO_OF_LAST_DIGITS_OF_JOB_ID = 4;
        int NO_OF_LAST_DIGITS = 2;  //same for day,month,hour and minute
        int NO_OF_LAST_DIGHTS_FOR_YEAR = 4;
        String executionID;                         //format: JJJJDDMMYYYYHHMM
        String lastFourDigitsOfJobID = getLastDigitsOfIntAsString(jobID, NO_OF_LAST_DIGITS_OF_JOB_ID);

        Calendar calendar = new GregorianCalendar();

        //converting one digit to two digit format e.g. converting "4" to "04"
        //for generating proper executionID with 16 digits
        String day = getLastDigitsOfIntAsString(calendar.get(Calendar.DATE), NO_OF_LAST_DIGITS);
        String month = getLastDigitsOfIntAsString(calendar.get(Calendar.MONTH), NO_OF_LAST_DIGITS);
        String year = getLastDigitsOfIntAsString(calendar.get(Calendar.YEAR), NO_OF_LAST_DIGHTS_FOR_YEAR);
        String hour = getLastDigitsOfIntAsString(calendar.get(Calendar.HOUR), NO_OF_LAST_DIGITS);
        String minute = getLastDigitsOfIntAsString(calendar.get(Calendar.MINUTE), NO_OF_LAST_DIGITS);

        executionID = lastFourDigitsOfJobID + day + month + year + hour + minute;

        logger.info("executionID: " + executionID);
        return executionID;
    }

    private static String getLastDigitsOfIntAsString(int jobID, int noOfLastDigits) {
        String str = "0000" + jobID;
        String strLastDigits = str.substring(str.length() - noOfLastDigits, str.length());
        return strLastDigits;
    }
    
    public static com.virtualcode.services.AgentManagementService agentManagementService;
    public static com.virtualcode.services.JobService jobService;

	public static ServiceManager serviceManager;
	public void setServiceManager(ServiceManager serviceManager) {
		this.serviceManager = serviceManager;
		this.jobService = DataLayer.serviceManager.getJobService();
		this.agentManagementService = DataLayer.serviceManager.getAgentManagementService();
	}

//	public static com.virtualcode.services.AgentManagementService getAgentManagementService() {
//		agentManagementService = (com.virtualcode.services.AgentManagementService)SpringApplicationContext.getBean("agentManagementService");
//		return agentManagementService;
//	}
	
	
	
	
	

}
