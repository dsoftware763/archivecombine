/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.Executors;

import org.apache.log4j.Logger;

import com.virtualcode.agent.das.dataArchiver.JobStarter;
import com.virtualcode.agent.das.dataExporter.ExportStarter;
import com.virtualcode.agent.das.dataIndexer.IndexStarter;
import com.virtualcode.agent.das.dataRestorer.RestoreStarter;
import com.virtualcode.agent.das.dataRetainer.RetentionHandler;
import com.virtualcode.agent.das.jobScheduler.JobScheduler;
import com.virtualcode.agent.das.utils.Utility;

/**
 *
 * @author YAwar
 */
public class StartAgentJobs {
    
    private static Logger log=Logger.getLogger(StartAgentJobs.class);
    private static Thread importer  =   null;
    private static Thread indexer	=	null;
    private static Thread exporter  =   null;
    private static Thread restorer	=	null;
    private static Thread scheduler	=	null;
    private static Thread retension	=	null;
    
    public StartAgentJobs(){
    	//Ignite the Agent threads, to get/process the Jobs of current Agent
    	startAgent();
    }
    
    private void scheduleJobs() {
    	
    }
    
    private void stopAgent() {

        log.debug("Stopping StartAgentJobs context successfully");
        
        if(importer!=null && importer.isAlive()) {
            importer.interrupt();
            importer    =   null;
        }
        if(indexer!=null && indexer.isAlive()) {
        	indexer.interrupt();
        	indexer    =   null;
        }
        if(exporter!=null && exporter.isAlive()) {
            exporter.interrupt();
            exporter    =   null;
        }
        if(restorer!=null && restorer.isAlive()) {
        	restorer.interrupt();
        	restorer    =   null;
        }
        if(scheduler!=null && scheduler.isAlive()) {
        	scheduler.interrupt();
        	scheduler    =   null;
        }
        if(retension!=null && retension.isAlive()) {
        	retension.interrupt();
        	retension    =   null;
        }
    }

    private void startAgent() {
    	
        log.debug("Initializing StartAgentJobs successfully");
        
         String AgentName = Utility.GetProp("AgentName");
         if(AgentName!=null && !AgentName.isEmpty()) {
        	 
             JobStarter js   =   new JobStarter(AgentName);
             importer   =   new Thread(js);
             importer.start();

             IndexStarter ijs    =   new IndexStarter(AgentName);
             indexer     =   new Thread(ijs);
             indexer.start(); //Start the Indexing Job Executor
             
             ExportStarter es    =   new ExportStarter(AgentName);
             exporter     =   new Thread(es);
             exporter.start(); //Start the Export Job Executor

             RestoreStarter rs    =   new RestoreStarter(AgentName);
             restorer	=   new Thread(rs);
             restorer.start(); //Start the Restore Job Executor
             
             JobScheduler	jsc	=	new JobScheduler(AgentName);
             scheduler	=	new Thread(jsc);
             scheduler.start();
             
             RetentionHandler srH	=	new RetentionHandler(AgentName);
             retension	=	new Thread(srH);
             retension.start();
         }
     }
}