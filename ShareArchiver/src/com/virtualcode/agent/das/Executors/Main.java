/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.Executors;

import com.virtualcode.agent.das.dataArchiver.JobStarter;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.agent.das.dataExporter.ExportStarter;
import com.virtualcode.common.VCSConstants;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import jcifs.smb.SmbFile;


/**
 *
 * @author se7
 */
public class Main {
/* Not to be used in archive.war 
 * Atiq
    
    public static void main(String[] args) {
        //String anyPath=args.length > 0 ? args[0] : "C:\\Users\\Seven\\Downloads\\CV";
        try {
            String AgentName = Utility.GetProp("AgentName");
            if(AgentName!=null && !AgentName.isEmpty()) {
                //System.out.println(Utility.GetProp("versionInfo"));
                //System.out.println(Utility.GetProp("versionDescription"));

                JobStarter js   =   new JobStarter(AgentName);
                Thread importer   =   new Thread(js);
                importer.start(); //js.run();//Start the Job Executor

                Thread.currentThread().sleep(1500);//1.5sec

                ExportStarter es    =   new ExportStarter(AgentName);
                Thread exporter     =   new Thread(es);
                exporter.start(); //es.run();//Start the Export Job Executor
            }
        } catch (InterruptedException ie) {
            ie.getMessage();
            ie.printStackTrace();
        }
    }*/
	public static void main(String[] args) {
        //String anyPath=args.length > 0 ? args[0] : "C:\\Users\\Seven\\Downloads\\CV";
        try {        
        	SimpleDateFormat srcDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    		Date mD	=	srcDateFormat.parse("2013-08-23T20:51:18.625+0500");
    		System.out.println("from str: "+mD.getTime());
    		
    		SmbFile file    =   new SmbFile("smb://NETWORK;se9:s9*@vc9/Files/Yawar//PUBLIC_MSGS2.log");
    		System.out.println("actual: "+file.lastModified());
        } catch (Exception e) {
        	e.printStackTrace();
        }
	}
}
