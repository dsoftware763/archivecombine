package com.virtualcode.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.net.URLDecoder;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.virtualcode.common.FacesUtil;
import com.virtualcode.common.SecureURL;
import com.virtualcode.controller.StubAccessController;
import com.virtualcode.repository.FileExplorerService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.vo.CustomTreeNode;

public class SecureGet2 extends HttpServlet {

	
	  private static final Logger log = Logger.getLogger(SecureGet2.class);
	   
	   private  static final String UNAUTHORIZE_REDIRECT = "/unauthorize.jsp";
	   private static final String DOC_PATH_BASE_REMOVE = "/repository/default/";
	   SecureURL secureURL = SecureURL.getInstance();
	
	public SecureGet2() {
		super();
	}

	
	public void destroy() {
		super.destroy(); 
		
	}

	 protected void processRequest(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        try {

	            log.debug("SECURE GET Started Processing Request ....................");

	            String pathInfo = request.getPathInfo();
	            String decryptedURL = null;

	            if (pathInfo != null && pathInfo.length() > 0) {
	                pathInfo = pathInfo.substring(1);
	                log.debug("path info: " + pathInfo);
	                try {
	                    decryptedURL = secureURL.getDecryptedURL(pathInfo);
	                    log.debug("Decrypted URL: " + decryptedURL);
	                } catch (Exception ex) {
	                    log.error(ex);
	                }
	            }


	            String servletContextPath = request.getContextPath();
	            String redirectURL = servletContextPath + decryptedURL;
	            String fileName = "";
	            String docRepoPath = null;
	            String secureInfo = "";
	            if(decryptedURL.lastIndexOf("?")!=-1){
	                //extracting secureinfo
	                secureInfo = decryptedURL.substring(decryptedURL.lastIndexOf("?")+1, decryptedURL.length());
	                log.debug("secure ---"+secureInfo);
	            }

	            try {
	                fileName = redirectURL.substring(redirectURL.lastIndexOf("/") + 1, redirectURL.lastIndexOf("?"));
	                log.debug("Filename Before decode : " + fileName);
	                fileName = URLDecoder.decode(fileName, "UTF-8");
	            } catch (Exception ex) {
	                log.error("Unable to get filename from URL, err: " + ex.toString());
	            }


	            try {
	                docRepoPath = decryptedURL.substring(0, decryptedURL.lastIndexOf("?"));
	                docRepoPath = docRepoPath.replaceAll(DOC_PATH_BASE_REMOVE, "");

	            } catch (Exception ex) {
	                log.error("Unable to get document repository path from URL, err: " + ex.toString());
	            }

	            log.debug("fileName: " + fileName);
	            log.debug("redirectURL: " + redirectURL);
	            log.debug("docRepoPath: " + docRepoPath);
	            docRepoPath=URLDecoder.decode(docRepoPath, "utf-8");
	            String userName = request.getRemoteUser();
	            log.debug("getRemoteUser in SECUREGET: " + userName);

//	            String openlink = "http://" + request.getServerName() + ":" + "8080"+servletContextPath+"/viewfile.jsp?nodeID="+docRepoPath+"&url="+redirectURL;
	            //String openlink = "http://" + request.getServerName() + ":" + request.getServerPort() + servletContextPath + "/viewfile.jsp?nodeID=" +com.virtualcode.util.StringUtils.encodeUrl(docRepoPath) + "&url=" + redirectURL
	            //      + "&pathinfo=" + pathInfo+ "&"+secureInfo;
	            downloadFile(request, response, docRepoPath);
	            //log.debug("Redirected URL: " + openlink);
//
//	            HttpSession httpsession = request.getSession();
//	            if (httpsession != null) {
//	                httpsession.setAttribute("remote_user", userName);
	                //String secureInfo=decryptedURL.substring(decryptedURL.lastIndexOf("secureInfo=")+11);

	           // }

	           // response.sendRedirect(openlink);


	        } catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        } finally {
	        	
	        }
	        log.debug("SECUREGET Finished Processing Request ....................");
	 }
	 
	 
	 private void downloadFile(HttpServletRequest request,HttpServletResponse response,String nodeID)throws Exception{
		 
			 nodeID = nodeID.substring(1);
			 String fileName=nodeID;
			 InputStream is=null;
			 int contentLength=0;
			 try{
	             
	             FileExplorerService explorerService=getFileExplorerService();
	             CustomTreeNode node=explorerService.getNodeByUUID(nodeID);
	             javax.jcr.Binary binaryStream =explorerService.downloadFile(node.getPath());
	             
	              if(binaryStream!=null){
		               is=binaryStream.getStream();
		               contentLength=Math.round(binaryStream.getSize());
		            }
	        }catch(Exception ex){
	           ex.printStackTrace();
	        }
	     
	    
	       try {
	              ServletOutputStream outs = response.getOutputStream();
	              String contentType ;//= uCon.getContentType();
	              
	               //------------------------------------------------------------------
	              
	              
	              FacesContext facesContext = FacesUtil.getFacesContext(request, response);
	              //StubAccessController bean = (StubAccessController) facesContext.getApplication().evaluateExpressionGet(facesContext, "#{stubAccessController}", StubAccessController.class);
	               
	              System.out.println("facesContext: " + facesContext); // shows that facesContext is null  
	             
	              
                 // System.out.println("****test**** "+bean.displayFileContents(nodeID));
	              
	              
	               //-----------------------------------------------------------------
	               String mimeType = URLConnection.guessContentTypeFromName(fileName);
	               if (mimeType == null) {
	                   if (fileName.endsWith(".doc")) {
	                       mimeType = "application/msword";
	                   } else if (fileName.endsWith(".xls")) {
	                       mimeType = "application/vnd.ms-excel";
	                   } else if (fileName.endsWith(".ppt")) {
	                       mimeType = "application/mspowerpoint";
	                   } else {
	                       mimeType = "application/octet-stream";
	                   }
	               }
	               //byte[] bytes = IOUtils.toByteArray(is);
	
	               response.setContentLength(contentLength);
	               response.setContentType((mimeType != null) ? mimeType : "application/octet-stream");
	               response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
	
	               log.debug("contentLength: " + contentLength);
	               log.debug("contentType: " + mimeType);
	
	               int bit;
	               try {
	                   while( (bit=is.read())!=-1 ) {
	                         outs.write(bit);
	                   }
	
	               } catch (IOException ioe) {
	                   log.debug("io excpetion 1 " + ioe.getMessage(),ioe);
	               }
	
	               try {
	                   is.close();
	                   outs.flush();
	                   log.debug("5555");
	                   outs.close();
	               } catch (IOException e) {
	                   log.debug("io exception 2 " + e.getMessage(),e);
	               }
	
	
	       } catch (MalformedURLException e) {
	           // TODO Auto-generated catch block
	           log.debug("malformed url exception " + e.getMessage(),e);
	           throw new Exception();
	       } catch (IOException e) {
	           // TODO Auto-generated catch block
	           log.debug("io expection 3 " + e.getMessage(),e);
	           throw new Exception();
	       }

		 
		
		 
		 
	 }
	 
	 
	 private FileExplorerService getFileExplorerService(){
		 ServletContext sc=getServletContext();
		 WebApplicationContext webAppContext=WebApplicationContextUtils.getWebApplicationContext(sc);
		 
		 ServiceManager serviceManager=(ServiceManager)webAppContext.getBean("serviceManager");
		 
		 return serviceManager.getFileExplorerService();
	 }
	
	
	
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		processRequest(request, response);
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		processRequest(request, response);
	}


	public void init() throws ServletException {
		// Put your code here
	}

}
