package com.virtualcode.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.util.Date;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.virtualcode.common.FacesUtil;
import com.virtualcode.common.SecureURL;
import com.virtualcode.common.VCSConstants;
import com.virtualcode.controller.StubAccessController;
import com.virtualcode.repository.FileExplorerService;
import com.virtualcode.schedular.PLVStatus;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.StringUtils;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.CustomTreeNode;

public class SecureGet extends HttpServlet {

	
	  private static final Logger log = Logger.getLogger(SecureGet.class);
	   
	   private  static final String UNAUTHORIZE_REDIRECT = "/unauthorize.jsp";
	   private static final String DOC_PATH_BASE_REMOVE = "/repository/default/";
	   SecureURL secureURL = SecureURL.getInstance();
	
	public SecureGet() {
		super();
	}

	
	public void destroy() {
		super.destroy(); 
		
	}

	 protected void processRequest(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        try {

	            log.debug("SECURE GET Started Processing Request ....................");
	            System.out.println("SECURE GET Started Processing Request ....................");

	            String pathInfo = request.getPathInfo();
	            String decryptedURL = null;
	            String spMode = request.getParameter("mode");            
	            	
	            System.out.println("mode = "+request.getParameter("mode"));
	            

	            if (pathInfo != null && pathInfo.length() > 0) {
	                pathInfo = pathInfo.substring(1);
	                log.debug("path info: " + pathInfo);
	                System.out.println("path info: " + pathInfo);
	                try {
	                    decryptedURL = secureURL.getDecryptedURL(pathInfo);
	                    log.debug("Decrypted URL: " + decryptedURL);
	                    System.out.println("Decrypted URL: " + decryptedURL);
	                } catch (Exception ex) {
	                    log.error(ex);
	                }
	            }


	            String servletContextPath = request.getContextPath();
	            String redirectURL = servletContextPath + decryptedURL;
	            String fileName = "";
	            String docRepoPath = null;
	            String secureInfo = "";
	            if(decryptedURL.lastIndexOf("?")!=-1){
	                //extracting secureinfo
	                secureInfo = decryptedURL.substring(decryptedURL.lastIndexOf("?")+1, decryptedURL.length());
	                log.debug("secure ---"+secureInfo);
	                System.out.println("secure info: "+ secureInfo);
	            }

	            try {
	                fileName = redirectURL.substring(redirectURL.lastIndexOf("/") + 1, redirectURL.lastIndexOf("?"));
	                log.debug("Filename Before decode : " + fileName);
	                fileName = URLDecoder.decode(fileName, "UTF-8");
	            } catch (Exception ex) {
	                log.error("Unable to get filename from URL, err: " + ex.toString());
	            }


	            try {
	                docRepoPath = decryptedURL.substring(0, decryptedURL.lastIndexOf("?"));
	                docRepoPath = docRepoPath.replaceAll(DOC_PATH_BASE_REMOVE, "");

	            } catch (Exception ex) {
	                log.error("Unable to get document repository path from URL, err: " + ex.toString());
	            }

	            log.debug("fileName: " + fileName);
	            log.debug("redirectURL: " + redirectURL);
	            log.debug("docRepoPath: " + docRepoPath);
	            docRepoPath=URLDecoder.decode(docRepoPath, "utf-8");
	            String userName = request.getRemoteUser();
	            log.debug("getRemoteUser in SECUREGET: " + userName);

                request.setAttribute(VCSConstants.STUB_UUID, fileName);
	            //add user to session
	        	//userName = httpRequest.getRemoteUser();
                if(userName!=null && userName.trim().length()>0)
                	userName = userName.substring(userName.indexOf("\\")+1);
                else
                	userName = "guest";
	        	System.out.println("username after removing domain: " + userName);
				request.getSession().setAttribute(VCSConstants.SESSION_USER_ROLE_NAME,VCSConstants.ROLE_PRIVILEGED);
				request.getSession().setAttribute(VCSConstants.SESSION_USER_AUTH_TYPE, "ad");
				request.getSession().setAttribute(VCSConstants.SESSION_USER_NAME, userName);
	            //remove the first '/'
				if(docRepoPath.indexOf("/")==0){
					docRepoPath = docRepoPath.substring(1);
				}
				String openlink = "";
				com.virtualcode.util.ResourcesUtil ru = com.virtualcode.util.ResourcesUtil.getResourcesUtil();
		        String mode = ru.getCodeValue("stub_access_mode", "General");// 1 =  view, 2 =  download and 3 = restore
                //String openlink = "http://" + request.getServerName() + ":" + request.getServerPort() + servletContextPath + "/StubAccess.faces?nodeID=" +fileName; 
				
		        PLVStatus plvStatus=PLVStatus.getInstance();
				if(!plvStatus.isValid()){//if license has expired,then send it to view page
				
					String strToday=VCSUtil.getDateAsString(new Date());
					Map<String, Integer> stubAccessMap=plvStatus.getStubAccessMap();
					
					if(!stubAccessMap.containsKey(strToday)){
						stubAccessMap.put(strToday, 1);
					}else if (stubAccessMap.containsKey(strToday) && stubAccessMap.get(strToday)<6){
						Integer todayAccessed=stubAccessMap.get(strToday)+1;
						stubAccessMap.put(strToday, todayAccessed);
					}else{
						mode="1";
					}					
									
				}
				
		        if((spMode!=null && spMode.equalsIgnoreCase("download")) || (mode!=null && mode.equals("2")))
					openlink = "http://" + request.getServerName() + ":" + request.getServerPort() + servletContextPath + "/downloadfile.jsp?nodeID="+secureURL.getEncryptedURL(docRepoPath)+"&"+secureInfo+"&mode=download";
				else if(spMode!=null && spMode.equalsIgnoreCase("downloadFS"))//case to be used for RestoreJob
					openlink = "http://" + request.getServerName() + ":" + request.getServerPort() + servletContextPath + "/DownloadFile?nodeID="+secureURL.getEncryptedURL(docRepoPath)+"&"+secureInfo+"&mode=download";
				else if(mode!=null && mode.equals("3"))
					//restore
					openlink = "http://" + request.getServerName() + ":" + request.getServerPort() + servletContextPath + "/RestoreStub.faces?nodeID=" +StringUtils.encodeUrl(docRepoPath);
				else
					openlink = "http://" + request.getServerName() + ":" + request.getServerPort() + servletContextPath + "/StubAccess.faces?nodeID=" +StringUtils.encodeUrl(docRepoPath);
				
               response.sendRedirect(openlink);

	        } catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        } finally {
	        	
	        }
	        log.debug("SECUREGET Finished Processing Request ....................");
	 }
	 
	 
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		processRequest(request, response);
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		processRequest(request, response);
	}


	public void init() throws ServletException {
		// Put your code here
	}

}
