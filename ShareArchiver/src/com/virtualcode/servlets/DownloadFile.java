package com.virtualcode.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URLConnection;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.virtualcode.repository.FileExplorerService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.vo.SystemCodeType;

public class DownloadFile extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public DownloadFile() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}
	
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException{
		 org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DownloadFile.class);
         String nodeID = request.getParameter("nodeID");
         String version = request.getParameter("version");
         log.info("before decode nodeID -->"+nodeID);
        

         String fileName = "";
         boolean isArchive =  true;

         try {
        	 if(nodeID.startsWith("na-")){
         		nodeID=com.virtualcode.util.StringUtils.decodeUrlAdvanced(nodeID.substring(3));
         		System.out.println(nodeID);
         		nodeID = nodeID.substring(2).replaceAll("\\\\", "/");
         		isArchive =  false;
         	}
         	else{
              	nodeID=com.virtualcode.util.StringUtils.decodeUrl(nodeID);
              	isArchive = true;
             }
             //nodeID  =   "archive/FS/vc6.network.vcsl/Everyone/try.esp";
             log.info("after decode nodeID -->"+nodeID);
             fileName = nodeID.substring(nodeID.lastIndexOf("/") + 1);
             log.debug("Filename Before decode : " + fileName);
             fileName =  new String(fileName.getBytes("UTF-8"), "ISO-8859-1");
             if(version!= null && version.equals("1")){
//            	String tempName = fileName;
     			if(fileName.indexOf("-")!=-1){
     				fileName = fileName.substring(0, fileName.lastIndexOf("-")).trim() + fileName.substring(fileName.lastIndexOf("-")+4).trim();
     			}
             }
             log.debug("Filename after decode : " + fileName);
         } catch (Exception ex) {
             log.error("Unable to get filename from URL, err: " + ex.toString());
             throw new ServletException();
         }
         
       
        InputStream is = null;
        int contentLength =0;
        com.virtualcode.util.ResourcesUtil ru = com.virtualcode.util.ResourcesUtil.getResourcesUtil();
        
         try{
               WebApplicationContext webAppContext=WebApplicationContextUtils.getWebApplicationContext(getServletContext());

               ServiceManager serviceManager=(ServiceManager)webAppContext.getBean("serviceManager");
               FileExplorerService explorerService=serviceManager.getFileExplorerService();
               //check if its a direct download file request from SecureGet
               String spMode = request.getParameter("mode");
               String mode = ru.getCodeValue("stub_access_mode", "General");// 1 =  view, 2 =  download and 3 = restore
               if((spMode!=null && spMode.equalsIgnoreCase("download")) || (mode!=null && mode.equals("2"))){
            	   com.virtualcode.vo.CustomTreeNode node = explorerService.getNodeByUUID(nodeID);
            	   if(node==null)
            		   node = explorerService.getNodeByPath(nodeID);
            	   
            	   if(node!=null){
            		   nodeID = node.getPath();
            		   fileName = nodeID.substring(nodeID.lastIndexOf("/") + 1);
            	   }
            	   
            	   
            	   
               }
               javax.jcr.Binary binaryStream =explorerService.downloadFile(nodeID, false);
               
                if(binaryStream!=null){
		               is=binaryStream.getStream();
		               contentLength=Math.round(binaryStream.getSize());
		            }
          }catch(Exception ex){
             ex.printStackTrace();
          }
       
      
         try {
                ServletOutputStream outs = response.getOutputStream();
                String contentType ;//= uCon.getContentType();

                 String mimeType = URLConnection.guessContentTypeFromName(fileName);
                 if (mimeType == null) {
                     if (fileName.endsWith(".doc")) {
                         mimeType = "application/msword";
                     } else if (fileName.endsWith(".xls")) {
                         mimeType = "application/vnd.ms-excel";
                     } else if (fileName.endsWith(".ppt")) {
                         mimeType = "application/mspowerpoint";
                     } else {
                         mimeType = "application/octet-stream";
                     }
                 }
                 //byte[] bytes = IOUtils.toByteArray(is);

                 response.setContentLength(contentLength);
                 response.setContentType((mimeType != null) ? mimeType : "application/octet-stream");
                 response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

                 log.debug("contentLength: " + contentLength);
                 log.debug("contentType: " + mimeType);

                 int bit = 256;
//                 int a;
                 
                 String tempSize	=	ru.getCodeValue("BUFF_SIZE_W", SystemCodeType.GENERAL);
 	            int buffSize	=	8192;//defaultBufferSize
 	            if(tempSize!=null && !tempSize.isEmpty()) {
 	            	try {
 	            		buffSize	=	Integer.parseInt(tempSize);
 	            	}catch(Exception ex) {
 	            		log.error(" Error to parse BUFF_SIZE_W: "+tempSize);
 	            	}
 	            }
 	            log.debug(" write_buffered is: "+buffSize);
 	            
                 byte[] buffer = new byte[buffSize];
//                 int bit;
                 try {
                    
                         while ((bit = is.read(buffer, 0, buffSize)) != -1) {
//                             bit = is.read();
                             outs.write(buffer, 0, bit);
//                             counter++;
                         
//                     while( (bit=is.read())!=-1 ) {
//                           outs.write(bit);
                     }

                 } catch (IOException ioe) {
                     log.debug("io excpetion 1 " + ioe.getMessage(),ioe);
                 }

                 try {
                     is.close();
                     outs.flush();
                     log.debug("5555");
                     outs.close();
                 } catch (IOException e) {
                     log.debug("io exception 2 " + e.getMessage(),e);
                 }


         } catch (MalformedURLException e) {
             // TODO Auto-generated catch block
             log.debug("malformed url exception " + e.getMessage(),e);
             throw new ServletException();
         } catch (IOException e) {
             // TODO Auto-generated catch block
             log.debug("io expection 3 " + e.getMessage(),e);
             throw new ServletException();
         }
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
