package com.virtualcode.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.virtualcode.common.SecureURL;
import com.virtualcode.common.VCSConstants;

public class SPRestore extends HttpServlet {
	
	
	   private static final Logger log = Logger.getLogger(SecureGet.class);
	   private static final String DOC_PATH_BASE_REMOVE = "/repository/default/";
	   SecureURL secureURL = SecureURL.getInstance();

	/**
	 * Constructor of the object.
	 */
	public SPRestore() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doService(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doService(request, response);
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}
	
	public void doService(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("  <HEAD><TITLE>SP Restore</TITLE></HEAD>");
		out.println("  <BODY>");
		out.print("    This is ");
		out.print(this.getClass());
		out.println(", using the POST method");

		org.vcsl.das.schedular.service.SchedularService schedularService = new org.vcsl.das.schedular.service.SchedularServiceImpl();
		org.vcsl.das.schedular.client.SchedularClient schedularClient = new org.vcsl.das.schedular.client.SchedularClientImpl();
		com.virtualcode.common.SecureURL secureUrl = com.virtualcode.common.SecureURL.getInstance();
		
        String webServiceURL = "";//"http://moss32:5544/DASService.svc";//"http://spserver01.vcsl31.local:6827/WCF32/DASService.svc";
        String targetNamespace = "http://tempuri.org/";
        String serviceName = "DASService";
        String portName = "BasicHttpBinding_IDASService";
        String portType = "IDASService";
        
//        schedularService.initParams(webServiceURL,
//                targetNamespace,
//                serviceName,
//                portType,
//                portName);
//        
//        schedularClient.initFarmStructure(webServiceURL,
//                targetNamespace,
//                serviceName,
//                portType,
//                portName);
        
        try {

            log.debug("SP Restore Started Processing Request ....................");
            System.out.println("SP Restore Started Processing Request ....................");

            String nodeID = request.getParameter("nodeID");
            String uid = request.getParameter("uid");
            String agentName = request.getParameter("agentName");
            String username = request.getRemoteUser();
            if(username==null || username.trim().equals("")){
            	throw new Exception("user name not found");
            	
            }
            if(username!=null && username.trim().length()>0)
            	username = username.substring(username.indexOf("\\")+1);
          //  String pathInfo = request.getPathInfo();
            String decryptedURL = null;

            if (nodeID != null && nodeID.length() > 0) {
                //nodeID = nodeID.substring(1);
                log.debug("path info: " + nodeID);
                System.out.println("path info: " + nodeID);
                try {
                    decryptedURL = secureURL.getDecryptedURL(nodeID);
                    decryptedURL = decryptedURL.substring(0, decryptedURL.lastIndexOf("?"));
                    log.debug("Decrypted URL: " + decryptedURL);
                    System.out.println("Decrypted URL: " + decryptedURL);
                } catch (Exception ex) {
                    log.error(ex);
                }
            }
            String secureInfo = "";
            if(decryptedURL.lastIndexOf("?")!=-1){
                //extracting secureinfo
                secureInfo = decryptedURL.substring(decryptedURL.lastIndexOf("?")+1, decryptedURL.length());
                log.debug("secure ---"+secureInfo);
                System.out.println("secure info: "+ secureInfo);
            }
            //get path for the node
            //nodeID = "9ac228b9-3ed8-4b43-a85f-794ad8ca2199";
            String nodePath = null;
            WebApplicationContext webAppContext=WebApplicationContextUtils.getWebApplicationContext(getServletContext());				  
            com.virtualcode.services.ServiceManager serviceManager=(com.virtualcode.services.ServiceManager)webAppContext.getBean("serviceManager");
            com.virtualcode.services.AgentService agentService = serviceManager.getAgentService();
            java.util.List agentList = agentService.getAgentByName(agentName);
            com.virtualcode.vo.Agent agent = null;
            if(agentList!=null && agentList.size()>0){
            	agent = (com.virtualcode.vo.Agent)agentList.get(0);            	
            }
            if(agent!=null)
            	webServiceURL = agent.getAllowedIps();
            else
            	out.println("agent '"+ agentName+"' not found");
            
            schedularService.initParams(webServiceURL,
                    targetNamespace,
                    serviceName,
                    portType,
                    portName);
            
            schedularClient.initFarmStructure(webServiceURL,
                    targetNamespace,
                    serviceName,
                    portType,
                    portName);
            
            com.virtualcode.repository.FileExplorerService fileExplorerService=serviceManager.getFileExplorerService();
            com.virtualcode.vo.CustomTreeNode selectedNode = null;
            //decryptedURL = decryptedURL.substring(0, decryptedURL.lastIndexOf("?"));
    		selectedNode=fileExplorerService.getNodeByUUID(decryptedURL);
    		
    		if(selectedNode == null){
    			System.out.println("getNodeByUUID not found, going to search by path");
    			selectedNode = fileExplorerService.getNodeByPath(decryptedURL);
    		}
    		if(selectedNode!=null){
               //authenticate 
              	  boolean authenticate = false;
      
                  authenticate = fileExplorerService.authorizeStub(selectedNode.getUuid(), username);
      
                    //authenticate = false;
                    if(!authenticate){
                    	 throw new Exception("user not authorized:[" + username +"]");
                    }
               
    			nodePath = selectedNode.getPath();
    			out.println("<br>");
    			out.println("  node found: " +selectedNode.getName());
    			nodePath =   "/DownloadFile?nodeID="+secureURL.getEncryptedURL(nodePath)+"&"+secureInfo;
				String restore = schedularClient.restoreItem(nodePath, uid);
				
				out.println("<br>");
				out.println("  response: " + restore);
				if(!(restore.startsWith("Error:"))){
					response.sendRedirect(restore);
					out.println("<br>");
				}
				
    		}else{
    			out.println("<br>");
    			out.println("node not found: "+ nodeID);
    		}
    		out.println("  </BODY>");
    		out.println("</HTML>");

            //end



        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            out.println(e.getMessage());
        } finally {
    		out.flush();
    		out.close();
        }
        log.debug("SP Restore Finished Processing Request ....................");
	}

}
