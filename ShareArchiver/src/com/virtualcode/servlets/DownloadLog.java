package com.virtualcode.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URLConnection;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.repository.FileExplorerService;
import com.virtualcode.services.ServiceManager;

public class DownloadLog extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public DownloadLog() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}
	
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException{
		
		 org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DownloadLog.class);
         String jobID = request.getParameter("jobid");
         String execID	=	request.getParameter("execid");
                
         File file	=	null;
        InputStream is = null;
        
         try{
        	 String basePath	=	Utility.GetProp("logBasePath");
        	 if(basePath==null || basePath.isEmpty())
         		basePath     =   "/jobsLog";
         	else {
         		
         		basePath	=	basePath + ((basePath.endsWith("/"))?"":"/") +"jobsLog";
         	}
        	 
        	 String filePath	=	basePath + "/" + jobID + "/" + execID + "/"+LoggingManager.PUBLIC_MSGS+".log";
        	 log.info("Loading log from: "+filePath);

        	 file	=	new File(filePath);
        	 if(!file.exists()) {
        		 log.warn("Log file not exist: "+filePath);
        		 throw new ServletException();
        	 }
        	 is=new FileInputStream(file);
				
          }catch(Exception ex){
             ex.printStackTrace();
          }
       
      
         try {
                ServletOutputStream outs = response.getOutputStream();
                String contentType ;//= uCon.getContentType();

                 String mimeType = "text/plain";
                 //byte[] bytes = IOUtils.toByteArray(is);

                 int contentLength	=	(int)file.length();
                 response.setContentLength(contentLength);
                 response.setContentType((mimeType != null) ? mimeType : "application/octet-stream");
                 //response.setHeader("Content-Disposition", "attachment; filename=\"" + execID + ".log\"");

                 log.debug("contentLength: " + contentLength);
                 log.debug("contentType: " + mimeType);

                 int bit = 256;
//                 int a;
                 byte[] buffer = new byte[4096];
//                 int bit;
                 try {
                    
                         while ((bit = is.read(buffer, 0, 4096)) != -1) {
//                             bit = is.read();
                             outs.write(buffer, 0, bit);
//                             counter++;
                         
//                     while( (bit=is.read())!=-1 ) {
//                           outs.write(bit);
                     }

                 } catch (IOException ioe) {
                     log.debug("io excpetion 1 " + ioe.getMessage(),ioe);
                 }

                 try {
                     is.close();
                     outs.flush();
                     log.debug("5555");
                     outs.close();
                 } catch (IOException e) {
                     log.debug("io exception 2 " + e.getMessage(),e);
                 }


         } catch (MalformedURLException e) {
             // TODO Auto-generated catch block
             log.debug("malformed url exception " + e.getMessage(),e);
             throw new ServletException();
         } catch (IOException e) {
             // TODO Auto-generated catch block
             log.debug("io expection 3 " + e.getMessage(),e);
             throw new ServletException();
         }
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
