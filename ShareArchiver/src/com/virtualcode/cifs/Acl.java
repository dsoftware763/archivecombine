/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.cifs;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author se7
 */
public class Acl implements Serializable {
    private ArrayList<Ace> allowAcl;
    private ArrayList<Ace> denyAcl;
    private ArrayList<Ace> shareAllowAcl;
    private ArrayList<Ace> shareDenyAcl;

    public Acl() {
        super();
        allowAcl         =   new ArrayList<Ace>();
        denyAcl         =   new ArrayList<Ace>();
        shareAllowAcl   =   new ArrayList<Ace>();
        shareDenyAcl    =   new ArrayList<Ace>();
    }

    public ArrayList<Ace> getAllowAcl() {
        return allowAcl;
    }

    public void setAllowAcl(ArrayList<Ace> allowAcl) {
        this.allowAcl = allowAcl;
    }

    public ArrayList<Ace> getDenyAcl() {
        return denyAcl;
    }

    public void setDenyAcl(ArrayList<Ace> denyAcl) {
        this.denyAcl = denyAcl;
    }

    public ArrayList<Ace> getShareAllowAcl() {
        return shareAllowAcl;
    }

    public void setShareAllowAcl(ArrayList<Ace> shareAllowAcl) {
        this.shareAllowAcl = shareAllowAcl;
    }

    public ArrayList<Ace> getShareDenyAcl() {
        return shareDenyAcl;
    }

    public void setShareDenyAcl(ArrayList<Ace> shareDenyAcl) {
        this.shareDenyAcl = shareDenyAcl;
    }
}
