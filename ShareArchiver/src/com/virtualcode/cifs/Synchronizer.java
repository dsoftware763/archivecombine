package com.virtualcode.cifs;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import javax.jcr.Session;

import org.apache.log4j.Logger;

import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.vo.DriveLetters;
import com.virtualcode.vo.SystemCodeType;

import jcifs.smb.ACE;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;

/**
 * @author SE6
 * Permission synchronizer for real time folder permissions to repo
 */
public class Synchronizer {
	
	static final Logger log = Logger.getLogger(Synchronizer.class);
	
    private String restoreDomainName = null;
    private String restoreUserName = null;
    private String restorePassword = null;    
    
    
    public Synchronizer(String domainName, String username, String password){        
        this.restoreDomainName=domainName;
        this.restoreUserName=username;
        this.restorePassword=password;
    }
    	
    
	
	public String synchronizePermissions(){
		
		return null;
	}
	
	//return comma separated sids list
	public ArrayList<String> getAllPermissions(String folderPath){
		
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		String domainName = "";
		String username = "";
		String password = "";
		String secAllowSids = "";
		String secDenySids = "";
		String shareAllowSids = "";
		String shareDenySids = "";
		
		ArrayList<String> sidList = null;
//        String domainName = "";
//        String username = "";
//        String password = "";
//        //if(driveLetter!=null){
//        	domainName = this.restoreDomainName;
//        	username = this.restoreUserName;
//        	password = this.restorePassword;
//        //}
//        if(domainName == null || domainName.trim().length()<1){
//        	domainName = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);//bundle.getString("domain.name");
//        	username = null;
//        	password = null;
//        }
//        String authStr="";
//        if(domainName!=null && ! domainName.trim().equals("")){
//        	if(username == null || username.trim().length()<1){
//        		username = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
//        		password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
//        	}	
		 
     	domainName = this.restoreDomainName;
     	username = this.restoreUserName;
     	password = this.restorePassword;
         if(domainName == null || domainName.trim().length()<1){
         	domainName = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);//bundle.getString("domain.name");
         	username = null;
         	password = null;
         }
         String authStr="";
         if(domainName!=null && ! domainName.trim().equals("")){
         	if(username == null || username.trim().length()<1){
         		username = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
         		password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
         	}
         	authStr = domainName+ ";" + username+ ":" + password;
         
         }
        if(folderPath.endsWith("/"))
        	folderPath = folderPath.substring(0, folderPath.length()-1);
		String smbPath = "smb://"+authStr+"@"+folderPath;
//		System.out.println(smbPath);
		log.debug("final path for getting permissions: " + smbPath);
		try {
			SmbFile sfile = new SmbFile(smbPath);
			if(sfile.exists()){
				System.out.println("is folder: " + sfile.isDirectory());
				
				System.out.println("Name:"+sfile.getName());				
				System.out.println("Share:"+sfile.getShare());
				System.out.println("Server:"+sfile.getServer());
				
				log.debug("Name:"+sfile.getName());
				log.debug("Share:"+sfile.getShare());
				log.debug("Server:"+sfile.getServer());
				
				ACE[] aces = sfile.getSecurity();
				int i = 0;
				for(ACE ace:aces){
//					System.out.println("Security Permission: " + ace.getSID());
					if(ace.isAllow())
						secAllowSids+=ace.getSID()+",";
					else
						secDenySids+=ace.getSID()+",";
				}
				System.out.println();
				aces = sfile.getShareSecurity(true);
				i = 0;
				for(ACE ace:aces){
//					System.out.println("Share Permission: " + ace.getSID());					
					if(ace.isAllow())
						shareAllowSids+=ace.getSID()+",";
					else
						shareDenySids+=ace.getSID()+",";
				}
				//remove the last (,)
				secAllowSids = secAllowSids.trim().length()>0?secAllowSids.substring(0, secAllowSids.length()-1):secAllowSids;
				secDenySids = secDenySids.trim().length()>0?secDenySids.substring(0, secDenySids.length()-1):secDenySids;
				shareAllowSids = shareAllowSids.trim().length()>0?shareAllowSids.substring(0, shareAllowSids.length()-1):shareAllowSids;
				shareDenySids = shareDenySids.trim().length()>0?shareDenySids.substring(0, shareDenySids.length()-1):shareDenySids;
				
				System.out.println("Security allow Sids: "+secAllowSids);
				System.out.println("Security deny Sids: "+secDenySids);
				System.out.println("Share allow Sids: "+shareAllowSids);
				System.out.println("Share deny Sids: "+shareDenySids);
				
				log.debug("Security allow Sids: "+secAllowSids);
				log.debug("Security deny Sids: "+secDenySids);
				log.debug("Share allow Sids: "+shareAllowSids);
				log.debug("Share deny Sids: "+shareDenySids);
				sidList =  new ArrayList<String>();
				sidList.add(secAllowSids);
				sidList.add(secDenySids);
				sidList.add(shareAllowSids);
				sidList.add(shareDenySids);
//				System.out.println("Security permissions: " + sfile.getSecurity().toString());
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.error("Error reading permissions" + e.getMessage());
		} catch (SmbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.error("Error reading permissions" + e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.error("Error reading permissions" + e.getMessage());
		}

		return sidList;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Synchronizer synchronizer = new Synchronizer("NETWORK", "se6", "jwdmuz1");
		synchronizer.getAllPermissions("192.168.30.24/test/test_share");

	}

}
