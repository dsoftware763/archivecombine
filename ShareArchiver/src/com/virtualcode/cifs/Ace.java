package com.virtualcode.cifs;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 *
 * @author Akhnukh Bashir Khan
 */
public class Ace implements Serializable  {

    private static final long serialVersionUID = 7526471155622776147L;
    private String sid;
    private String sidName;
    private boolean isAllow;
    private boolean isInherited;
    private int accessMask;

    public Ace() {
        super(); 
    }
    
    /*
    public Ace(
            String sid, String sidName, boolean isAllow, boolean isInherited, int accessMask) {
        super();
        setSid(sid);
        setSidName(sidName);
        setIsAllow(isAllow);
        setIsInherited(isInherited);
        setAccessMask(accessMask);
    }
     */

    /**
     * @return the sid
     */
    public String getSid() {
        return sid;
    }

    /**
     * @param sid the sid to set
     */
    public void setSid(String sid) {
        this.sid = sid;
    }
    
    /**
     * @return the sidName
     */
    public String getSidName() {
        return sidName;
    }

    /**
     * @param sidName the sidName to set
     */
    public void setSidName(String sidName) {
        this.sidName = sidName;
    }

    /**
     * @return the isAllow
     */
    public boolean isIsAllow() {
        return isAllow;
    }

    /**
     * @param isAllow the isAllow to set
     */
    public void setIsAllow(boolean isAllow) {
        this.isAllow = isAllow;
    }

    /**
     * @return the isInherited
     */
    public boolean isIsInherited() {
        return isInherited;
    }

    /**
     * @param isInherited the isInherited to set
     */
    public void setIsInherited(boolean isInherited) {
        this.isInherited = isInherited;
    }

    /**
     * @return the accessMask
     */
    public int getAccessMask() {
        return accessMask;
    }

    /**
     * @param accessMask the accessMask to set
     */
    public void setAccessMask(int accessMask) {
        this.accessMask = accessMask;
    }

    @Override
    public String toString() {

        return "SID: " + this.getSid() + "\nName: " + this.getSidName() + "\nAllow/Deny: " + (this.isIsAllow() ? "Allow" : "Deny") + "\nInherited/Direct: " + (this.isIsInherited() ? "Inherited" : "Direct") + "\nAccessMask: " + this.getAccessMask();
    }

    /**
     * Always treat de-serialization as a full-blown constructor, by
     * validating the final state of the de-serialized object.
     */
    private void readObject(
            ObjectInputStream aInputStream) throws ClassNotFoundException, IOException {
        //always perform the default de-serialization first
        aInputStream.defaultReadObject();
    }

    private void writeObject(
            ObjectOutputStream aOutputStream) throws IOException {
        //perform the default serialization for all non-transient, non-static fields
        aOutputStream.defaultWriteObject();
    }

    
}
