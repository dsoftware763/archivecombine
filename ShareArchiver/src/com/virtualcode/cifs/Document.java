/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.cifs;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author SE6
 */
@XmlRootElement(name = "document")
public class Document {

    //JCR Porperties
    private String id;
    private String title;
    private String dataStream;
    private String modifiedBy;
    private String modifiedOn;
    private String createdOn;
    private String mimeType;
    private String url;
    private String accessedOn;
    private Long size;
    private String author;
    private boolean linkedNodeYN;
    private boolean folderYN;
    private boolean fileYN;
    private String secureURL;
    private String stubURL;
    private String hashValue;

    private byte[] data;

    public Document() {
        super();
    }

    public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getHashValue() {
		return hashValue;
	}

	public void setHashValue(String hashValue) {
		this.hashValue = hashValue;
	}

	public String getAccessedOn() {
        return accessedOn;
    }

    public void setAccessedOn(String accessedOn) {
        this.accessedOn = accessedOn;
    }

    public String getDataStream() {
        return dataStream;
    }

    public void setDataStream(String dataStream) {
        this.dataStream = dataStream;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public boolean isFolderYN() {
        return folderYN;
    }

    public void setFolderYN(boolean folderYN) {
        this.folderYN = folderYN;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public boolean isFileYN() {
        return fileYN;
    }

    public void setFileYN(boolean fileYN) {
        this.fileYN = fileYN;
    }

    public boolean isLinkedNodeYN() {
        return linkedNodeYN;
    }

    public void setLinkedNodeYN(boolean linkedNodeYN) {
        this.linkedNodeYN = linkedNodeYN;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
        public String getSecureURL() {
        return secureURL;
    }

    public void setSecureURL(String secureURL) {
        this.secureURL = secureURL;
    }

	public String getStubURL() {
		return stubURL;
	}

	public void setStubURL(String stubURL) {
		this.stubURL = stubURL;
	}

}
