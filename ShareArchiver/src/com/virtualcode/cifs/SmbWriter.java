/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.cifs;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.jcr.Binary;
import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.Session;
import javax.jcr.UnsupportedRepositoryOperationException;
import javax.jcr.nodetype.InvalidNodeTypeDefinitionException;
import javax.jcr.nodetype.NodeTypeExistsException;

import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;

import org.apache.log4j.Logger;

import com.virtualcode.common.CustomProperty;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.vo.CustomTreeNode;
import com.virtualcode.vo.SystemCodeType;

/**
 *
 * @author SE6
 */
public class SmbWriter extends Thread {

    static final Logger log = Logger.getLogger(SmbWriter.class);
    private Session session;    
    private static Map<String, Integer> tasksInExecution=new HashMap<String,Integer>();
    private static Map<String, String> jobsInExecution=new HashMap<String,String>();
    private static HashMap<String, String> currentRestores=new HashMap<String, String>();
    
    private ResourcesUtil   ru  =   ResourcesUtil.getResourcesUtil();
    
    private String repoPath=null;
    private String destPath=null;
        
    private String restoreDomainName = null;
    private String restoreUserName = null;
    private String restorePassword = null;
    
    private boolean isVersion = false;
    private boolean winStubRestore	=	false;
    
    public SmbWriter(Session jcrSession) {    	
        this.session=jcrSession;
    }
        
    @Override
    public void run(){
    	System.out.println("******************** Starting a Thread for restore....");
    	restore();
    }
    
    
    public SmbWriter(String repoPath, String destPath, String restoreDomainName,String restoreUserName,String restorePassword ,Session jcrSession) {    	
             this.session=jcrSession;
             this.repoPath=repoPath;
             this.destPath=destPath;
             this.restoreDomainName=restoreDomainName;
             this.restoreUserName=restoreUserName;
             this.restorePassword=restorePassword;
    }
    public SmbWriter(String repoPath, String destPath, String restoreDomainName,String restoreUserName,String restorePassword ,Session jcrSession, boolean version) {    	
        this.session=jcrSession;
        this.repoPath=repoPath;
        this.destPath=destPath;
        this.restoreDomainName=restoreDomainName;
        this.restoreUserName=restoreUserName;
        this.restorePassword=restorePassword;
        this.isVersion = version;
    }
    public SmbWriter(String repoPath, String destPath, String restoreDomainName,String restoreUserName,String restorePassword ,Session jcrSession, boolean version, boolean winStubRestore) {    	
        this.session=jcrSession;
        this.repoPath=repoPath;
        this.destPath=destPath;
        this.restoreDomainName=restoreDomainName;
        this.restoreUserName=restoreUserName;
        this.restorePassword=restorePassword;
        this.isVersion = version;
        this.winStubRestore=winStubRestore;
}    
    

    public static Integer getCompletedPercentage(String filePath) {
//	         synchronized (tasksInExecution) {    	
	        	 return tasksInExecution.get(filePath);
//			}			
	}
    
    public static void removeJob(String filePath) {		
		synchronized(tasksInExecution){
    	   tasksInExecution.remove(filePath);
		}
		synchronized (jobsInExecution) {
			jobsInExecution.remove(filePath);	
		}
				
   }
    
    public static String getJobStatusMessage(String filePath) {
    	synchronized(jobsInExecution){	
		   return jobsInExecution.get(filePath);
    	}
    }
    
    public static boolean isJobRunning(String filePath) {
//        synchronized (tasksInExecution) {    	
       	 return tasksInExecution.containsKey(filePath);
//		}			
}
    
    public static boolean inCurRestoreList(String key) {
    	return currentRestores.containsKey(key);
    }

    public String restore() {

        
        boolean success = false;
        String message = "";
        String FILE_NAME = "";
        String outPath = "";
        boolean useDriveCreds = false;
//        System.out.println("Root " + root.getName());
        try {
            
        	//set the flag to restore started...
        	currentRestores.put(repoPath, "INITIATED");
            
        	Node root = session.getRootNode();
            Node file = null;
            Node content = null;
            Property data = null;

//            now retrieve the file
            String PATH = repoPath.substring(0, repoPath.lastIndexOf("/"));// "TestCompany/FS/192.168.30.24/C/tmp";
            FILE_NAME = repoPath.substring(repoPath.lastIndexOf("/") + 1, repoPath.length());//+"~sharearchiver.log";
            //String DEST = "smb://NETWORK;user:password@192.168.30.24/TestSmb/restore";
            String[] pathElements = com.virtualcode.util.StringUtils.getPathElements(PATH);

            String destPath = "smb://";//"smb://NETWORK;se6:jwdm222@";
            String domainName = "";
            String username = "";
            String password = "";
            //if(driveLetter!=null){
            	domainName = this.restoreDomainName;
            	username = this.restoreUserName;
            	password = this.restorePassword;
            //}
            	
            if(domainName!=null)	//driveletter based creds are being used
            	useDriveCreds = true;
            
            if(domainName == null || domainName.trim().length()<1){
            	domainName = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);//bundle.getString("domain.name");
            	username = null;
            	password = null;
            }
            String authStr="";
            if(domainName!=null && ! domainName.trim().equals("")){
            	if(username == null || username.trim().length()<1){
            		username = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
            		password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
            	}
//            	 authStr = domainName+ ";" + ru.getCodeValue("restore.username", SystemCodeType.ADAUTH_PROP) + ":" + ru.getCodeValue("restore.password", SystemCodeType.ADAUTH_PROP);
            	authStr = domainName+ ";" + username+ ":" + password;
            	 destPath += authStr;
                 destPath += "@";
            }else{
               //authStr = ru.getCodeValue("restore.username", SystemCodeType.ADAUTH_PROP) + ":" + ru.getCodeValue("restore.password", SystemCodeType.ADAUTH_PROP);
            }
                      
            System.out.println("Before path attaching: "+destPath);
            if(this.destPath==null || this.destPath.trim().length()<1){	// if the location of the file hasn't changed
	            for (int i = 0; i < pathElements.length; i++) {
	                System.out.println("elements: "+pathElements[i]);
	                if (i > 1) {
	                    destPath += pathElements[i] + "/";
	                }
	            }
            }else{
            	destPath+=this.destPath;
            }
            System.out.println("final destination path : " + destPath);
           

            file = root.getNode(PATH + "/" + FILE_NAME);

            content = file.getNode(Property.JCR_CONTENT);
            // download the latest version
            if(!isVersion && content.hasProperty(CustomProperty.VC_NEXT_VERSIONS)) {
	           	Property prop = content.getProperty(CustomProperty.VC_NEXT_VERSIONS);
	            String nextVerIDs	=	prop.getString();
	            log.debug("NextVersionIDs " + nextVerIDs);
	            
                if(nextVerIDs!=null && !nextVerIDs.isEmpty()) {
                    String[] temp	=	nextVerIDs.split(",");
                    
                    for(int i=0; i<temp.length; i++) {
                    	try {
                        	Node	tmpNode	=	session.getNodeByIdentifier(temp[temp.length-1].trim());
                        	if(tmpNode!=null) {
                        		tmpNode	=	tmpNode.getParent();
                        		content = tmpNode.getNode(Property.JCR_CONTENT);
                        	}
                    	} catch(Exception e) {
                    		log.debug("Version ID is problamatic: "+temp[i]);
                    		e.printStackTrace();
                    	}
                    }
                }
            }
            
            
            if(content.hasProperty(CustomProperty.VC_LINKED_TO)) {//if Custom Linked Node
               Property prop    =   content.getProperty(CustomProperty.VC_LINKED_TO);
               String refNodeID =   prop.getString();

               content  =   session.getNodeByIdentifier(refNodeID);
               file     =   content.getParent();
               //nodeID   =   nodeID.substring(1);//Remove the first '/'
               System.out.println("Getting content of: "+file.getPath());
               
            } else {
                System.out.println("Its an Original Node to be restored ");
            }
            
            if(content.hasProperty(CustomProperty.VC_VERSION_OF)){
            	Property prop = content.getProperty(CustomProperty.VC_VERSION_OF);
                String versionOf = prop.getString();
                log.debug("VersionOf " + versionOf);
                Node parentNode = session.getNodeByIdentifier(versionOf);
                if(parentNode!=null){
                	FILE_NAME = parentNode.getParent().getName();
                }
           }
            
            
            
            
            data = content.getProperty(Property.JCR_DATA);

            long dateModified = 0;
            long dateCreated = 0;


            //getting node properties
            
            if (content.hasProperty(Property.JCR_LAST_MODIFIED)) {
                Property prop = content.getProperty(Property.JCR_LAST_MODIFIED);
                Calendar c = prop.getDate();
                dateModified = c.getTimeInMillis();
                //System.out.println(dateModified);
                System.out.println("dateModified  " + dateModified);
            }

            if (content.hasProperty(CustomProperty.VC_CREATION_DATE)) {
                Property prop = content.getProperty(CustomProperty.VC_CREATION_DATE);
                Calendar c = prop.getDate();            
                dateCreated = c.getTimeInMillis();
                //System.out.println(dateCreated);
                System.out.println("dateCreation  " + dateCreated);
            }else{
                Calendar c = Calendar.getInstance();
                c.setTime(new Date());
                dateCreated = c.getTimeInMillis();
//                System.out.println(dateCreated);
                System.out.println("dateCreation  " + dateCreated);
            }


            Binary bin = data.getBinary();
            float fileSize=bin.getSize();
            long copied=0;
            float percentageCopied=0;
            InputStream sfis = bin.getStream();
            outPath = createDirectoryStructure(destPath, PATH);// + "/" + FILE_NAME;
            
            if(outPath.contains("Exception:") && useDriveCreds){
            	//rebuild url from default settings if restore is failed
            	destPath = "smb://";
            	domainName = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);//bundle.getString("domain.name");          
            	username = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
            	password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
            	authStr = domainName+ ";" + username+ ":" + password;
            	 destPath += authStr;
                 destPath += "@";
                 
                 if(this.destPath==null || this.destPath.trim().length()<1){	// if the location of the file hasn't changed
     	            for (int i = 0; i < pathElements.length; i++) {
     	                System.out.println("elements: "+pathElements[i]);
     	                if (i > 1) {
     	                    destPath += pathElements[i] + "/";
     	                }
     	            }
                 }else{
                 	destPath+=this.destPath;
                 }
                 outPath = createDirectoryStructure(destPath, PATH);// + "/" + FILE_NAME;
                 
//            	return "File restore failed ["+outPath.substring(10, outPath.lastIndexOf(".")).trim()+"].";            	
            }
            
            if(outPath.contains("Exception:")){
            	message	=	"File restore failed ["+outPath.substring(10, outPath.lastIndexOf(".")).trim()+"].";
            	throw new IOException(message);
            }
            
            if(outPath.trim().endsWith("/"))
                outPath = outPath + FILE_NAME;
            else
            	 outPath = outPath + "/" + FILE_NAME;
            
            String tempOutPath	=	outPath + ".temp";
            String stubUrl = outPath + ".url";

            SmbFile tempOutFile = new SmbFile(tempOutPath);
            if (tempOutFile.exists()) {
            	tempOutFile.delete();
            	tempOutFile	=	null;
            	
            	tempOutFile	=	new SmbFile(tempOutPath);
            }

            String tempSize	=	ru.getCodeValue("BUFF_SIZE_W", SystemCodeType.GENERAL);
            int buffSize	=	8192;//defaultBufferSize
            if(tempSize!=null && !tempSize.isEmpty()) {
            	try {
            		buffSize	=	Integer.parseInt(tempSize);
            	}catch(Exception ex) {
            		log.error(" Error to parse BUFF_SIZE_W: "+tempSize);
            	}
            }
            log.debug(" write_buffered size is: "+buffSize);
            
            SmbFileOutputStream sfos = new SmbFileOutputStream(tempOutFile);
            System.out.println("Restoring file: " + FILE_NAME);            
            
//            System.out.println("Starting restore..");
            Long before = System.currentTimeMillis();
            int a = 256;
            
            byte[] buffer = new byte[buffSize];
            System.out.println("*************Before writing..."+new Date());
            while ((a = sfis.read(buffer, 0, buffSize)) != -1) {
                 sfos.write(buffer, 0, a);
                
                copied=copied+a;               
                percentageCopied=(copied/fileSize)*100;                
                synchronized(tasksInExecution){
                 tasksInExecution.put(repoPath, (int)percentageCopied);
                }
               
            			
            }
            System.out.println("*************After writing..."+new Date());
            sfos.flush();
            sfis.close();
            sfos.close();
            
            SmbFile outFile = new SmbFile(outPath);//it may be a Window stub
            if(outFile.exists()) {
            	
            	if(winStubRestore && outFile.length()==0l) {//If the already exisiting file is a stub than delete it
	            	outFile.delete();//how can i identify that the already existign is a stub
	            	outFile	=	null;
	            	
            	} else {//dont delete, but create the New file with other name
            		
            		//Code to modify name
                    System.out.println("File already exists: " + FILE_NAME);
                    System.out.println("Creating new file");
                    Date date = new Date();
                    SimpleDateFormat smf = new SimpleDateFormat("yyyyMMddHHmmss");
                    FILE_NAME = FILE_NAME.substring(0, FILE_NAME.lastIndexOf("."))+"-" + smf.format(date) + FILE_NAME.substring(FILE_NAME.lastIndexOf("."), FILE_NAME.length());
                    outPath = createDirectoryStructure(destPath, PATH);
                    if(outPath.endsWith("/")){
                    	outPath+= FILE_NAME;
                    }else{
                    	outPath+= "/" + FILE_NAME;
                    }
                    //end code to change the name (if already exist)
            	}
            	
            	outFile	=	new SmbFile(outPath);
            }
            tempOutFile.setCreateTime(dateCreated);
            tempOutFile.setLastModified(dateModified);
            tempOutFile.renameTo(outFile);
            //outFile.setCreateTime(dateCreated);
            //outFile.setLastModified(dateModified);
            
            Long after = System.currentTimeMillis();
            
            deleteStub(stubUrl);
            System.out.println("Restore complete in: " + (after - before));
            
            success = true;
            
        } catch (InvalidNodeTypeDefinitionException ex) {
            ex.printStackTrace();
            System.out.println("Error accessing file: " + ex.getMessage());
        } catch (NodeTypeExistsException ex) {
            ex.printStackTrace();
            System.out.println("Error accessing file: " + ex.getMessage());
        } catch (UnsupportedRepositoryOperationException ex) {
            ex.printStackTrace();
            System.out.println("Error accessing file: " + ex.getMessage());
        }catch(jcifs.smb.SmbAuthException ex){
        	ex.printStackTrace();
        	System.out.println("Error creating directory structure: " + ex.getMessage());
        	System.err.println("Error: " + ex.getMessage());
        	message="File restore failed [ "+ex.getMessage()+"].";
        	synchronized (jobsInExecution) {
        		jobsInExecution.put(repoPath, message);
			}
        	
        }catch (SmbException ex) {			
			ex.printStackTrace();
			message= "File restore failed [ "+ex.getMessage()+"].";
			synchronized(jobsInExecution){
			   jobsInExecution.put(repoPath, message);
			}
			
		}catch (IOException ex) {			
			ex.printStackTrace();
			message= "File restore failed [ "+ex.getMessage()+"].";
			synchronized(jobsInExecution){
			  jobsInExecution.put(repoPath, message);
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
			message= "File restore failed [ "+ex.getMessage()+"].";
			synchronized(jobsInExecution){
			  jobsInExecution.put(repoPath, message);
			}
		}finally {
			currentRestores.put(repoPath, null);
			currentRestores.remove(repoPath);
		}
//            System.out.println("Error accessing file: " + ex.getMessage());
//            ex.printStackTrace();
//        } 
        if (success) {
            message = "File restored successfully '\\\\" + outPath.substring(outPath.indexOf("@")+1).replaceAll("/", "\\\\") + "'";
        } else if(!success && (message==null || message.isEmpty())){
            message = "File restore failed";
        }
        synchronized(jobsInExecution){
            jobsInExecution.put(repoPath, message);
        }
        
       return message;
    }

    private String createDirectoryStructure(String destPath, String repoPath) {

        try {

            StringTokenizer st = new StringTokenizer(repoPath, "/");
            int i = 0;
            repoPath = "";
            while (st.hasMoreTokens()) {

                if (i < 4) {
                    st.nextToken();
                } else {
                    repoPath += "/" + st.nextToken();
                   
                }
                i++;
            }
            //logger.debug(repoPath);
            System.out.println("path = " + destPath);
            //destPath = destPath + repoPath.replaceAll("[.]", "_");            

            if (destPath.toLowerCase().contains("smb://")) {//if Dest Path is CIFS

                synchronized (this.getClass()) {//Only one instance of following block should execute at a time
                    SmbFile file = new SmbFile(destPath);
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                }
                /*
                synchronized (this) {
                StringTokenizer st = new StringTokenizer(repoPath, "/");
                while(st.hasMoreTokens()) {
                System.out.println("mkdirs : " +destPath);

                String curDir   =   st.nextToken();
                destPath        =   destPath + "/" + curDir;
                SmbFile file    =   new SmbFile(destPath);
                if(!file.exists())
                file.mkdir();
                }
                }
                 */
            } else {
                File file = new File(destPath);
                if (!file.exists()) {
                    file.mkdirs();
                }
            }
        }catch(jcifs.smb.SmbAuthException sae){
        	sae.printStackTrace();
        	System.out.println("Error creating directory structure: " + sae.getMessage());
        	System.err.println("Error: " + sae.getMessage());
        	return "Exception: "+sae.getMessage();
        }catch(jcifs.smb.SmbException sae){
        	sae.printStackTrace();
        	System.out.println("Error creating directory structure: " + sae.getMessage());
        	System.err.println("Error: " + sae.getMessage());
        	return "Exception: "+sae.getMessage();
        }catch (Exception mue) {
            mue.printStackTrace();
            System.out.println("Error creating directory structure: " + mue.getMessage());
            System.err.println("Error: " + mue.getMessage());
            return "Exception: "+mue.getMessage();
        }
        //System.out.println("dest path = " + destPath);
        return destPath;
    }

    public void deleteStub(String stubUrl) {
        System.out.println("Deleting stub: " + stubUrl);
        try {
            SmbFile stub = new SmbFile(stubUrl);

            stub.delete();
            stub = null;
        } catch (Exception ie) {
            //ie.printStackTrace();
            log.error("Deletion Failed:" + ie.getMessage());
        }
    }
}
