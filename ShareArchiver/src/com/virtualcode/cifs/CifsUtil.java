/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.cifs;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import jcifs.smb.ACE;
import jcifs.smb.SmbFile;

import org.apache.log4j.Logger;

import com.virtualcode.agent.das.utils.AuthorizationUtil;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.common.CustomProperty;

/**
 *
 * @author se7
 */
public class CifsUtil {
    
    static final Logger log = Logger.getLogger(CifsUtil.class);    
    
    public static HashMap<String, String> getAclAttribs(Acl acl) {
        HashMap<String, String> aclAttribs    =   new HashMap<String, String>();
        
        if(acl!=null) {
            String allowedSids      =   getSids(acl.getAllowAcl());
            String deniedSids       =   getSids(acl.getDenyAcl());
            String shareAllowedSids =   getSids(acl.getShareAllowAcl());
            String shareDeniedSids  =   getSids(acl.getShareDenyAcl());

            aclAttribs.put(CustomProperty.VC_ALLOWED_SIDS,      allowedSids);
            aclAttribs.put(CustomProperty.VC_DENIED_SIDS,       deniedSids);
            aclAttribs.put(CustomProperty.VC_S_ALLOWED_SIDS,    shareAllowedSids);
            aclAttribs.put(CustomProperty.VC_S_DENIED_SIDS,     shareDeniedSids);
        }
        return aclAttribs;
    }
    
    private static String getSids(ArrayList<Ace> aceList) {        
        String sids  =   "";
        for(int i=0; aceList!=null && i<aceList.size(); i++) {
            Ace ace =   aceList.get(i);
            sids    =   sids + "," + ace.getSid();
            log.debug(ace.toString());
        }
        if(!sids.isEmpty())
            sids =   sids.substring(1);//Trim the first COMMA
        
        return sids;
    }
    
	 public Acl getACL(SmbFile smbFile) throws IOException {
	        Acl acl =   null;
	        
	        //Get the Original Security ACL
	        ACE[] aceList    =   smbFile.getSecurity();
	        String documentPath = smbFile.getPath();
	        
	        String mapShareInfo =   Utility.GetProp("mapShareInfo");
	        if ("1".equals(mapShareInfo)) {//if has to retrieve the ShareSecurity from file
	            //String mapFilePath  =   Utility.GetProp("mapFilePath");
	            String mapFilePath = "/mapingFile.properties";
	            URL url 	= getClass().getResource(mapFilePath);
	            if(url!=null)
	            	mapFilePath	=	url.getPath();
	            
	            //System.out.println("Full doc path: "+documentPath);
	            String entryFor     =   documentPath.substring(0, Utility.nthOccurrence(documentPath, '/', 1));//
	            //System.out.println("Get entry from mapFilePath: "+entryFor);
	            String mapingEntry  =   Utility.getMapingEntry(mapFilePath, entryFor);
	            
	            //if(mapingEntry!=null) {
	                //Transform the original ACL into customized ACL
	                acl =   AuthorizationUtil.transformACE(aceList, mapingEntry);
	            //}
	        } else {//if get original ShareSecurity, instead of maping file...
	            //Get the Share Security ACL
	            ACE[] aceShareList    =   smbFile.getShareSecurity(true);
	            
	            //Transform the original ACL into customized ACL
	            acl =   AuthorizationUtil.transformACE(aceList, aceShareList);
	        }
	        
	        return acl;
	    }
}
