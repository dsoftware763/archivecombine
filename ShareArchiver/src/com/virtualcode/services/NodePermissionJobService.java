package com.virtualcode.services;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.NodePermissionJob;


public interface NodePermissionJobService {
	
	public void saveNodePermissionJob(NodePermissionJob transientInstance);

	public void deleteNodePermissionJob(NodePermissionJob persistentInstance);

	public NodePermissionJob getNodePermissionJobById(java.lang.Integer id) ;

	public List<NodePermissionJob> getNodePermissionJobByExample(NodePermissionJob instance) ;

	public List<NodePermissionJob> getNodePermissionJobByProperty(String propertyName, Object value);

	public List<NodePermissionJob> getNodePermissionJobByName(Object name) ;

	public List<NodePermissionJob> getNodePermissionJobByCommand(Object command);
	
	public List<NodePermissionJob> getNodePermissionJobByStatus(Object status);

	public List<NodePermissionJob> getNodePermissionJobByRegisterdate(Object registerdate) ;

	public List<NodePermissionJob> getNodePermissionJobByStartdate(Object startdate) ;

	public List<NodePermissionJob> getNodePermissionJobByFinisheddate(Object finisheddate) ;

	public List<NodePermissionJob> getNodePermissionJobBySecureinfo(Object secureinfo) ;

	public List<NodePermissionJob> getNodePermissionJobBySids(Object sids) ;

	public List<NodePermissionJob> getNodePermissionJobByDenysids(Object denysids);

	public List<NodePermissionJob> getNodePermissionJobBySharesids(Object sharesids);

	public List<NodePermissionJob> getNodePermissionJobByDenysharesids(Object denysharesids);

	public List<NodePermissionJob> getNodePermissionJobByIsrecursive(Object isrecursive) ;

	public List<NodePermissionJob> getAll();

}