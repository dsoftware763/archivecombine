package com.virtualcode.services;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.UserPermissions;
import com.virtualcode.vo.UserPermissionsId;


public interface UserPermissionsService  {
	        
    public void save(UserPermissions transientInstance);
    
	public void delete(UserPermissions persistentInstance);
    
    public UserPermissions getById( com.virtualcode.vo.UserPermissionsId id);    
       
	public List getAll() ;	
  
}