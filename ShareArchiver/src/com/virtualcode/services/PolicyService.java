package com.virtualcode.services;

import java.util.List;

import com.virtualcode.vo.Policy;

public interface PolicyService {
	
   public void savePolicy(Policy Policy);
	
	public List<Policy> getAllPolicies();
	
	public Policy getPolicyById(Integer policyId);
	
	public List getPolicyByName(Object name);
	
	public void updatePolicy(Policy policy);
	
	public void deletePolicy(Policy policy);
	

}
