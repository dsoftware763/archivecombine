package com.virtualcode.services;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.JobStatus;


public interface JobStatusService {

	public void saveJobStatus(JobStatus transientInstance) ;

	public void deleteJobStatus(JobStatus persistentInstance) ;

	public JobStatus getJobStatusById(java.lang.Integer id) ;

	public List<JobStatus> getJobStatusByExample(JobStatus instance);

	public List<JobStatus> getJobStatusByProperty(String propertyName, Object value);

	public List<JobStatus> getJobStatusByDescription(Object description);
	
	public List<JobStatus> getJobStatusByIsCurrent(Object isCurrent);

	public List<JobStatus> getJobStatusByPreviousJobStatusId(Object previousJobStatusId) ;

	public List<JobStatus> getJobStatusByStatus(Object status) ;

	public List<JobStatus> getJobStatusByExecutionId(Object executionId);

	public List<JobStatus> getAll();
	
	public List<JobStatus> getJobStatusByJob(Object jobId);


}