package com.virtualcode.services;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.JobStatistics;

/**
 * A data access object (DAO) providing persistence and search support for
 * JobStatistics entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.vo.JobStatistics
 * @author MyEclipse Persistence Tools
 */

public interface JobStatisticsService {
	

	public void saveJobStatistics(JobStatistics transientInstance);
	
	public void updateJobStatistics(JobStatistics transientInstance);

	public void deleteJobStatistics(JobStatistics persistentInstance);

	public JobStatistics getJobStatisticsById(java.lang.Integer id);

	public List<JobStatistics> getJobStatisticsByExample(JobStatistics instance);

	public List<JobStatistics> getJobStatisticsByProperty(String propertyName, Object value);

	public List<JobStatistics> getJobStatisticsByExecutionId(Object executionId);

	public List<JobStatistics> getJobStatisticsBySkippedProcessfileSkippedHiddenlected(
			Object skippedProcessfileSkippedHiddenlected);

	public List<JobStatistics> getJobStatisticsBySkippedProcessfileSkippedIsdir(
			Object skippedProcessfileSkippedIsdir);

	public List<JobStatistics> getJobStatisticsBySkippedProcessfileSkippedMismatchAge(
			Object skippedProcessfileSkippedMismatchAge);

	public List<JobStatistics> getJobStatisticsBySkippedProcessfileSkippedMismatchExtension(
			Object skippedProcessfileSkippedMismatchExtension);

	public List<JobStatistics> getJobStatisticsBySkippedProcessfileSkippedMismatchLastaccesstime(
			Object skippedProcessfileSkippedMismatchLastaccesstime);

	public List<JobStatistics> getJobStatisticsBySkippedProcessfileSkippedMismatchLastmodifiedtime(
			Object skippedProcessfileSkippedMismatchLastmodifiedtime);

	public List<JobStatistics> getJobStatisticsBySkippedProcessfileSkippedReadOnly(
			Object skippedProcessfileSkippedReadOnly);

	public List<JobStatistics> getJobStatisticsBySkippedProcessfileSkippedSymbollink(
			Object skippedProcessfileSkippedSymbollink);

	public List<JobStatistics> getJobStatisticsBySkippedProcessfileSkippedTemporary(
			Object skippedProcessfileSkippedTemporary);

	public List<JobStatistics> getJobStatisticsBySkippedProcessfileSkippedTooLarge(
			Object skippedProcessfileSkippedTooLarge);

	public List<JobStatistics> getJobStatisticsByTotalArchived(Object totalArchived);

	public List<JobStatistics> getJobStatisticsByTotalDuplicated(Object totalDuplicated);

	public List<JobStatistics> getJobStatisticsByTotalEvaluated(Object totalEvaluated);

	public List<JobStatistics> getJobStatisticsByTotalFailedArchiving(Object totalFailedArchiving);

	public List<JobStatistics> getJobStatisticsByTotalFailedDeduplication(Object totalFailedDeduplication);

	public List<JobStatistics> getJobStatisticsByTotalFailedEvaluating(Object totalFailedEvaluating);

	public List<JobStatistics> getJobStatisticsByTotalFailedStubbing(Object totalFailedStubbing);

	public List<JobStatistics> getJobStatisticsByTotalStubbed(Object totalStubbed);

	public List<JobStatistics> getAll();
	
	public List<JobStatistics> getJobStatisticsByJob(Object jobId);
	
	


}