package com.virtualcode.services;

import java.util.List;

import com.virtualcode.vo.RoleFeature;


public interface RoleFeatureService  {
	
	public void save(RoleFeature transientInstance);

	public RoleFeature findById(java.lang.Integer id) ;

	public List<RoleFeature> getByProperty(String propertyName, Object value) ;

	public List<RoleFeature> getByName(Object name);

	public List<RoleFeature> getByAdmin(Object admin);

	public List<RoleFeature> getByPriv(Object priv) ;

	public List<RoleFeature> getByLdap(Object ldap) ;
	public List<RoleFeature> getAll() ;

	public RoleFeature merge(RoleFeature detachedInstance) ;

	public void attachDirty(RoleFeature instance) ;
	public void attachClean(RoleFeature instance) ;

	
}