package com.virtualcode.services;

import java.util.List;

public interface UserPreferencesService {
	public List<String> getGroupsForUser(String username);
}
