package com.virtualcode.services;

import java.util.List;
import com.virtualcode.vo.SystemAlerts;


public interface SystemAlertsService {
	
	public void saveSystemAlert(SystemAlerts systemAlert);

	public void deleteSystemAlert(SystemAlerts systemAlert);

	public SystemAlerts getSystemAlertsById(java.lang.Integer id);

	public List<SystemAlerts> getSystemAlertsByExample(SystemAlerts instance);

	public List<SystemAlerts> getSystemAlertsByProperty(String propertyName, Object value);

	public List<SystemAlerts> getSystemAlertsByUsername(Object username);

	public List<SystemAlerts> getSystemAlertsByAlertType(Object alertType);

	public List<SystemAlerts> getSystemAlertsByAlertMessage(Object alertMessage);

	public List<SystemAlerts> getAll();

}