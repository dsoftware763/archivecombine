package com.virtualcode.services;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.LinkAccessDocuments;

/**
 * A data access object (DAO) providing persistence and search support for
 * LinkAccessDocuments entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.vo.LinkAccessDocuments
 * @author MyEclipse Persistence Tools
 */

public interface LinkAccessDocumentsService{

	public void saveLinkAccessDocuments(LinkAccessDocuments transientInstance);

	public void deleteLinkAccessDocuments(LinkAccessDocuments persistentInstance);

	public LinkAccessDocuments getLinkAccessDocumentsById(java.lang.Integer id);

	public List<LinkAccessDocuments> getLinkAccessDocumentsByExample(LinkAccessDocuments instance);

	public List<LinkAccessDocuments> getLinkAccessDocumentsByProperty(String propertyName, Object value);
	
	public LinkAccessDocuments getLinkAccessDocumentsByUuidAndUser(String uuid, String user);

	public LinkAccessDocuments getLinkAccessDocumentsByUuidDocuments(String uuidDocuments);

	public List<LinkAccessDocuments> getAll();
}