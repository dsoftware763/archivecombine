package com.virtualcode.services;

import java.util.List;
import com.virtualcode.vo.ActiveArchiveReport;


public interface ActiveArchiveReportService{
	
	public void save(ActiveArchiveReport transientInstance);

	public void delete(ActiveArchiveReport persistentInstance);

	public ActiveArchiveReport findById(java.lang.Integer id);

	public List<ActiveArchiveReport> getByExample(ActiveArchiveReport instance);

	public List<ActiveArchiveReport> getByProperty(String propertyName, Object value);

	public List<ActiveArchiveReport> getByDailyCount(Object dailyCount);

	public List<ActiveArchiveReport> getByTotalCount(Object totalCount);

	public List<ActiveArchiveReport> getByTotalJobs(Object totalJobs);

	public List<ActiveArchiveReport> getAll();	

}