package com.virtualcode.services;

import java.util.List;

import com.virtualcode.vo.ShareAnalysisStats;


public interface ShareAnalysisStatsService {
	
	public void save(ShareAnalysisStats transientInstance) ;

	public void delete(ShareAnalysisStats persistentInstance);
	
	public void deleteByDriveId(Integer driveLetterId);

	public ShareAnalysisStats getById(java.lang.Long id) ;
	
	public List<ShareAnalysisStats> getByShare(Integer shareId);

	public List<ShareAnalysisStats> getByExample(ShareAnalysisStats instance);

	public List<ShareAnalysisStats> getByProperty(String propertyName, Object value) ;

	public List<ShareAnalysisStats> getByDocumentCatName(Object documentCatName) ;
			
	public List<ShareAnalysisStats> findAll() ;


}