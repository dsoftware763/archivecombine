package com.virtualcode.services;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.SystemCode;


public interface SystemCodeService {
	

	public void saveSystemCode(SystemCode transientInstance);

	public void update(SystemCode systemCode);
	
	public void deleteSystemCode(SystemCode persistentInstance);

	public SystemCode getSystemCodeById(java.lang.Integer id);

	public List<SystemCode> getSystemCodeByExample(SystemCode instance);

	public List<SystemCode> getSystemCodeByProperty(String propertyName, Object value);
	
	public List<SystemCode> getSystemCodeByCodename(Object codename);
	
	public SystemCode getSystemCodeByName(Object codename);

	public List<SystemCode> getSystemCodeByCodetype(Object codetype);

	public List<SystemCode> getSystemCodeByCodevalue(Object codevalue);

	public List<SystemCode> getSystemCodeByEditable(Object editable);

	public List<SystemCode> getSystemCodeByViewable(Object viewable);

	public List<SystemCode> getAll();
	
	
	public List<SystemCode> getAllGeneralCodes();	
	
	public List<SystemCode> getAllLDAPCodes();
	
	public List<SystemCode> getAllJespCodes();
	
	public List<SystemCode> getAllAgentCodes();
	
	public List<SystemCode> getAllCompanyCodes();
	
	public List<SystemCode> getAllAccountPolicyCodes();
	
	
}