package com.virtualcode.services;

import java.util.List;

import com.virtualcode.vo.DriveLetters;

public interface DriveLettersService {
	
	public void saveDriveLetter(DriveLetters transientInstance) ;
	
	public void deleteDriveLetter(DriveLetters persistentInstance) ;

	public DriveLetters getDriveLetterById(java.lang.Integer id);

	
	public List<DriveLetters> getDriveLettersByDriveLetter(Object driveLetter);

	public List<DriveLetters> getDriveLettersBySharePath(Object sharePath);

	public List<DriveLetters> getDriveLettersByStatus(Object status);

	public List<DriveLetters> getAll() ;
	
}