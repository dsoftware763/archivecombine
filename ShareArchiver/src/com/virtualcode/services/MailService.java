package com.virtualcode.services;

import java.io.File;


public interface MailService {

	public boolean sendMail( String subject, String msg);
	
	
	public boolean sendMail(String toEmail,String subject, String msgbody,String fileName,File attachment);
	
	public boolean sendMail(String fromEmail, String toEmail,String subject, String msgbody,String fileName,File attachment);
	
	
}
