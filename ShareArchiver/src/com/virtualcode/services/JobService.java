package com.virtualcode.services;

import java.util.List;

import com.virtualcode.vo.DriveLetters;
import com.virtualcode.vo.Job;
import com.virtualcode.vo.JobSpDocLib;

public interface JobService {	
		
	   public void saveJob(Job Job);

	   public void saveRetentionJob(Job Job);
		
	   public void saveExportJob(Job Job);
	   
	   public List<DriveLetters> getSpDocLibByDriveLetters(List<DriveLetters> tempList);
	   
	   public void saveExportJobPriv(Job Job, List<String> exportPathList, String remoteUNC);
	   
	   public List getLibsByNameLike(String value);
	   
		public List<Job> getAllJobs();
		
		public List getAllFoStatus(); 
		
		public List getAllForStatus(String type) ;
		
		public Job getJobById(Integer jobId);
		
		public Job getJobByName(String name);
		
		public void updateJob(Job Job);
		
		public void deleteJob(Job job);	
		
		public void deleteJobDependencies(Job job);
		
		public List<Job> getAgentImportJobs(String agentName);
		
		public List<Job> getAgentIndexingJobs(String agentName);
		
		public List<Job> getAgentExportJobs(String agentName);
		
		public List<Job> getJobByProperty(String propertyName, Object value);
		
		public boolean updateJobExecutionStatus(final Integer jobStatusId,final String status);
		
		public List<Integer> getCancelJobs();

		List<Job> getAgentScheduledJobs(String agentName);
		
		public List<Job> getAllScheduledJobs();
		
		public List<Job> getAgentProactiveJobs(String agentName);
		
		public List<Job> getAgentActiveArchivingJobs(String agentName);
		
		public List<Job> getAllActiveArchivingJobs();
		
		public List<Job> getAgentPrintArchivingJobs(String agentName);
		
		public List<Job> getAgentRetensionJobs(String agentName);
		
		public List<Job> getAgentRestoreJobs(String agentName);
		
		public List<JobSpDocLib> getShareThreshholdJobs(String sharePath);
		
		public List<JobSpDocLib> getShareAllThreshholdJobs(String sharePath);
		
		public void deleteSpDocLib(String name);
		
		public void mergeJobTransient(Job job);
		
		public void saveSpDocLib(JobSpDocLib lib);
		
		public List<Job> getAllActiveImportJobs();
		
		public List<Job> getAllActiveJobs();
}
