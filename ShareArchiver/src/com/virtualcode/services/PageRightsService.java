package com.virtualcode.services;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.PageRights;

/**
 * A data access object (DAO) providing persistence and search support for
 * PageRights entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.virtualcode.vo.PageRights
 * @author MyEclipse Persistence Tools
 */

public interface PageRightsService {

	public void savePageRights(PageRights transientInstance);

	public void deletePageRights(PageRights persistentInstance);

	public PageRights getPageRightsById(java.lang.Integer id);

	public ArrayList<PageRights> getPageRightsByExample(PageRights instance);

	public ArrayList<PageRights> getPageRightsByProperty(String propertyName, Object value);

	public ArrayList<PageRights> getPageRightsByPageUri(Object pageUri);

	public ArrayList<PageRights> getAll();

}