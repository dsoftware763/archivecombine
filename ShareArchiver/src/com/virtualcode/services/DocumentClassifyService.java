package com.virtualcode.services;

import java.util.List;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.DocumentTypeDAO;
import com.virtualcode.vo.*;

public interface DocumentClassifyService {
	
	public List<DocumentCategory> getAllDocumentCategories();
	
	public List<DocumentType> getAllDocumentTypes();
	
	public DocumentCategory getDocumentCategory(Integer categoryId);
		
	public List<DocumentType> getDocumentTypes(Integer categoryId);
	
	public List<String> getAllDocCategoryExts(Integer categoryId);
	
	public DocumentType getDocumentType(Integer documentTypeId);
	
	public boolean removeDocumentType(Integer documentTypeId);
	
	public boolean removeDocumentCategory(Integer documentCategoryId);
	
	
	public void saveDocumentCategory(DocumentCategory documentCategory);
	
	public void saveDocumentType(DocumentType documentType);	
		
		
	public void updateDocumentType(DocumentType documentType);
	
    public boolean isDocCategoryInUse(Integer docCategoryId);
	
	public boolean isDocTypeInUse(Integer docCategoryId);

	
	
	
}
	
