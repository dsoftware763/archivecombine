package com.virtualcode.services;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.RolePermissions;
import com.virtualcode.vo.RolePermissionsId;


public interface RolePermissionsService {
	        
    public void save(RolePermissions transientInstance) ;
    
	public void delete(RolePermissions persistentInstance);
    
    public RolePermissions getById( com.virtualcode.vo.RolePermissionsId id) ;
    
	public List getAll() ;
	
	
}