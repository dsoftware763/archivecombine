package com.virtualcode.services;

import java.util.List;
import java.util.Set;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.Role;

public interface RoleService  {
	       
    public void save(Role transientInstance);
    
	public void delete(Role persistentInstance);
    
    public Role getById( java.lang.Long id) ;    
   
	public List getAll();	
 	
}