package com.virtualcode.services;

import java.util.List;

import com.virtualcode.vo.DelJob;

public interface DelJobService {
    
    public void save(DelJob transientInstance);
    
	public void delete(DelJob persistentInstance);
    
    public DelJob getById( java.lang.Integer id);
    
    public List getByExample(DelJob instance) ;
    public List getByProperty(String propertyName, Object value) ;
	public List getByActionType(Object actionType
	) ;
	
	public List getByName(Object name);
	public List getAll();
    public DelJob merge(DelJob detachedInstance) ;

    public void attachDirty(DelJob instance);
    public void attachClean(DelJob instance);

}
