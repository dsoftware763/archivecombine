package com.virtualcode.services.impl;

import java.util.List;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.ErrorCodeDAO;
import com.virtualcode.services.ErrorCodeService;
import com.virtualcode.vo.ErrorCode;


public class ErrorCodeServiceImpl implements ErrorCodeService {

	@Override
	public void save(ErrorCode transientInstance) {
		errorCodeDAO.save(transientInstance);
		
	}

	@Override
	public void delete(ErrorCode persistentInstance) {
		errorCodeDAO.delete(persistentInstance);
		
	}

	@Override
	public ErrorCode getById(Integer id) {
		return errorCodeDAO.findById(id);
	}

	@Override
	public List getByExample(ErrorCode instance) {
		return errorCodeDAO.findByExample(instance);
	}


	@Override
	public List getAll() {
		return errorCodeDAO.findAll();
	}

	
	private ErrorCodeDAO errorCodeDAO;
	private DAOManager daoManager;    
	
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.errorCodeDAO=daoManager.getErrorCodeDAO();
	}
	
	
	

	
}