package com.virtualcode.services.impl;

import com.virtualcode.dao.AgentDAO;
import com.virtualcode.dao.DAOManager;
import com.virtualcode.services.AgentService;
import com.virtualcode.vo.Agent;

import java.io.Serializable;
import java.util.*;

/**
 * @author AtiqurRehman
 *
 */
public class AgentServiceImpl implements AgentService,Serializable {
	
	public List<Agent> getAllAgents(){		
		return agentDao.findAll();
	}	

	
	@Override
	public Agent getAgentById(Integer agentId) {
		 return agentDao.findById(agentId);
	}
	
	public List getAgentByName(String name) {
		List agentList = agentDao.findByName(name);
		if(agentList!=null && agentList.size()>0){
			return agentList;
		}
		return null;
	}
	
	public List getAgentByLogin(String login) {
		List agentList = agentDao.findByLogin(login);
		if(agentList!=null && agentList.size()>0){
			return agentList;
		}
		return null;
	}

	@Override
	public boolean updateAgent(Agent agent) {
		try{     
		agentDao.merge(agent);
		}catch(Exception ex){
			return false;
		}
		    
		return true;		
	}

	@Override
	public boolean deleteAgent(Integer agentId) {
		try{     
			agentDao.delete(agentDao.findById(agentId));
			}catch(Exception ex){
				return false;
			}
			    
			return true;
	}

	@Override
	public void saveAgent(Agent agent) {
		Agent agentA;
		try{     
			agentDao.save(agent);
		}catch(Exception ex){
			
		}    
			
	}
	
	
    private AgentDAO agentDao;	
	private DAOManager daoManager;
	
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.agentDao=daoManager.getAgentDAO();
	}


	@Override
	public List getAgentByType(String type) {
		// TODO Auto-generated method stub
		return agentDao.findByType(type);
	}

	
	
}
