package com.virtualcode.services.impl;

import java.util.List;

import com.sun.star.comp.servicemanager.ServiceManager;
import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.LinkAccessDocumentsDAO;
import com.virtualcode.services.LinkAccessDocumentsService;
import com.virtualcode.vo.LinkAccessDocuments;
import com.virtualcode.vo.LinkAccessUsers;

public class LinkAccessDocumentsServiceImpl implements
		LinkAccessDocumentsService {

	@Override
	public void saveLinkAccessDocuments(LinkAccessDocuments transientInstance) {
		// TODO Auto-generated method stub
		linkAccessDocumentsDAO.save(transientInstance);

	}

	@Override
	public void deleteLinkAccessDocuments(LinkAccessDocuments persistentInstance) {
		// TODO Auto-generated method stub

	}

	@Override
	public LinkAccessDocuments getLinkAccessDocumentsById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<LinkAccessDocuments> getLinkAccessDocumentsByExample(
			LinkAccessDocuments instance) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<LinkAccessDocuments> getLinkAccessDocumentsByProperty(
			String propertyName, Object value) {
		// TODO Auto-generated method stub
		return linkAccessDocumentsDAO.findByProperty(propertyName, value);
	}

	@Override
	public LinkAccessDocuments getLinkAccessDocumentsByUuidDocuments(
			String uuidDocuments) {
		// TODO Auto-generated method stub
		
		List<LinkAccessDocuments> list = linkAccessDocumentsDAO.findByProperty("uuidDocuments", uuidDocuments);
		if(list!=null && list.size()>0){
			return list.get(0);
		}
		return null;
	}
	@Override
	public LinkAccessDocuments getLinkAccessDocumentsByUuidAndUser(String uuid, String user){
		List<LinkAccessDocuments> list = linkAccessDocumentsDAO.findByUuidAndUser(uuid, user);
		if(list!=null && list.size()>0){
			return list.get(list.size()-1);
		}		
		return null;
	}

	@Override
	public List<LinkAccessDocuments> getAll() {
		// TODO Auto-generated method stub
		return null;
	}
	
	private LinkAccessDocumentsDAO linkAccessDocumentsDAO;
	private DAOManager daoManager;
	
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.linkAccessDocumentsDAO = this.daoManager.getLinkAccessDocumentsDAO();
	}
	
	
	
	

}
