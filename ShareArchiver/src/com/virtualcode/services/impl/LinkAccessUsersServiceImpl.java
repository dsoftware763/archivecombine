package com.virtualcode.services.impl;

import java.util.List;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.LinkAccessUsersDAO;
import com.virtualcode.services.LinkAccessUsersService;
import com.virtualcode.vo.LinkAccessUsers;

public class LinkAccessUsersServiceImpl implements LinkAccessUsersService {

	@Override
	public void saveLinkAccessUsers(LinkAccessUsers transientInstance) {
		// TODO Auto-generated method stub
		linkAccessUsersDAO.save(transientInstance);
	}

	@Override
	public void deleteLinkAccessUsers(LinkAccessUsers persistentInstance) {
		// TODO Auto-generated method stub

	}

	@Override
	public LinkAccessUsers getLinkAccessUsersById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<LinkAccessUsers> getLinkAccessUsersByExample(
			LinkAccessUsers instance) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<LinkAccessUsers> getLinkAccessUsersByProperty(
			String propertyName, Object value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LinkAccessUsers getLinkAccessUsersByRecipient(String recipient) {
		// TODO Auto-generated method stub
		List<LinkAccessUsers> list = linkAccessUsersDAO.findByRecipient(recipient);		
		if(list!=null && list.size()>0){
			return list.get(list.size()-1);
		}			
		
		return null;
	}

	@Override
	public List<LinkAccessUsers> getLinkAccessUsersByPassword(Object password) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<LinkAccessUsers> getLinkAccessUsersBySender(Object sender) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<LinkAccessUsers> getAll() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean recipientExists(String recipient) {
		// TODO Auto-generated method stub
//		List<LinkAccessUsers> list = getLinkAccessUsersByRecipient(recipient);
//		if(list!=null && list.size()>0)
//			return true;
		
		return false;
	}
	
	@Override
	public boolean login(String username, String password){
		List<LinkAccessUsers> list = linkAccessUsersDAO.findByUsernamePassword(username, password);
		if(list!=null && list.size()>0){
			return true;			
		}
		return false;
	}
	
	private LinkAccessUsersDAO linkAccessUsersDAO;
	private DAOManager daoManager;
	
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.linkAccessUsersDAO = this.daoManager.getLinkAccessUsersDAO();
	}


	
	

}
