package com.virtualcode.services.impl;

import java.util.List;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.DelJobFiledetailsDAO;
import com.virtualcode.services.DelJobFiledetailsService;
import com.virtualcode.vo.DelJobFiledetails;
 
public class DelJobFiledetailsServiceImpl implements DelJobFiledetailsService {

    private DelJobFiledetailsDAO delJobFiledetailsDAO;
    private DAOManager daoManager;	
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.delJobFiledetailsDAO=daoManager.getDelJobFiledetailsDAO();
	}
	
	@Override
	public void save(DelJobFiledetails transientInstance) {
		// TODO Auto-generated method stub
		delJobFiledetailsDAO.save(transientInstance);
	}

	@Override
	public void delete(DelJobFiledetails persistentInstance) {
		// TODO Auto-generated method stub
		delJobFiledetailsDAO.delete(persistentInstance);
	}

	@Override
	public DelJobFiledetails getById(Integer id) {
		// TODO Auto-generated method stub
		return delJobFiledetailsDAO.findById(id);
	}

	@Override
	public List getByExample(DelJobFiledetails instance) {
		// TODO Auto-generated method stub
		return delJobFiledetailsDAO.findByExample(instance);
	}

	@Override
	public List getByProperty(String propertyName, Object value) {
		// TODO Auto-generated method stub
		return delJobFiledetailsDAO.findByProperty(propertyName, value);
	}

	@Override
	public List getByUuid(Object uuid) {
		// TODO Auto-generated method stub
		return delJobFiledetailsDAO.findByUuid(uuid);
	}

	@Override
	public List getByStatus(Object status) {
		// TODO Auto-generated method stub
		return delJobFiledetailsDAO.findByStatus(status);
	}

	@Override
	public List getByDescription(Object description) {
		// TODO Auto-generated method stub
		return delJobFiledetailsDAO.findByDescription(description);
	}

	@Override
	public List getAll() {
		// TODO Auto-generated method stub
		return delJobFiledetailsDAO.findAll();
	}

	@Override
	public DelJobFiledetails merge(DelJobFiledetails detachedInstance) {
		// TODO Auto-generated method stub
		return delJobFiledetailsDAO.merge(detachedInstance);
	}

	@Override
	public void attachDirty(DelJobFiledetails instance) {
		// TODO Auto-generated method stub
		delJobFiledetailsDAO.attachDirty(instance);
	}

	@Override
	public void attachClean(DelJobFiledetails instance) {
		// TODO Auto-generated method stub
		delJobFiledetailsDAO.attachClean(instance);
	}

}
