package com.virtualcode.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.DocumentCategoryDAO;
import com.virtualcode.dao.DocumentTypeDAO;
import com.virtualcode.dao.PolicyDAO;
import com.virtualcode.services.DocumentClassifyService;
import com.virtualcode.vo.*;

public class DocumentClassifyServiceImpl implements DocumentClassifyService {
	
	@Override
	public List<DocumentCategory> getAllDocumentCategories() {
		List<DocumentCategory> docCategoryList=documentCategoryDAO.findAll();
		return docCategoryList;
	}
	
	@Override
	public List<DocumentType> getAllDocumentTypes() {
		List<DocumentType> docTypeList=documentTypeDAO.findAll();
		return docTypeList;
	}
	
	@Override
	public DocumentCategory getDocumentCategory(Integer categoryId) {
		DocumentCategory documentCategory=documentCategoryDAO.findById(categoryId);
		return documentCategory;
	}

	
	@Override
	public List<DocumentType> getDocumentTypes(Integer categoryId) {
		return documentTypeDAO.findByCategoryId(categoryId);
	}
	
	public List<String> getAllDocCategoryExts(Integer categoryId){
		List<DocumentType> docTypeList=documentTypeDAO.findByCategoryId(categoryId);
		List<String> docTypeExtList=new ArrayList<String>();
		for(int i=0;i<docTypeList.size();i++){
			docTypeExtList.add(docTypeList.get(i).getValue().toLowerCase());
		}
		return docTypeExtList;
		
	}
	
	@Override
	public DocumentType getDocumentType(Integer documentTypeId) {
		return documentTypeDAO.findById(documentTypeId);
	}
	
	
//	public List<DocumentType> getAllDocumentTypes(){
//		List<DocumentType> documentTypesList= documentTypeDAO.findAll();
//		for(int i=0;i<documentTypesList.size();i++){
//			DocumentType documentType=documentTypesList.get(i);//set full objects in lazily loaded list
//			documentType.setDocumentCategory(documentCategoryDAO.findById(documentType.getDocumentCategory().getId()));
//		}
//		
//		return documentTypesList;
//		
//	}
	
	@Override
	public boolean removeDocumentType(Integer documentTypeId) {
		try{
			documentTypeDAO.delete(documentTypeId);
			
		}catch(Exception ex){
			return false;
		}
		return true;
	}

	@Override
	public boolean removeDocumentCategory(Integer documentCategoryId) {
		try{
						
			documentTypeDAO.deleteByCategory(documentCategoryId);
			/*DocumentCategory documentCategory=new DocumentCategory();			
			documentCategory.setId(documentCategoryId);
			documentCategoryDAO.delete(documentCategory);*/
			
			//above code was throwing exception"-null property references a null or transient value: com.virtualcode.vo.DocumentCategory.name"
			DocumentCategory documentCategory = documentCategoryDAO.findById(documentCategoryId);
			
			documentCategoryDAO.delete(documentCategory);
			
		}catch(Exception ex){
			return false;
		}
		return true;
	}
	
	public boolean isDocCategoryInUse(Integer docCategoryId){
		  
	  return policyDAO.isDocCategoryInUse(docCategoryId);
	}
	
	public boolean isDocTypeInUse(Integer docTypeId){
		
		return policyDAO.isDocTypeInUse(docTypeId);
	}

	
	@Override
	public void saveDocumentType(DocumentType documentType) {
		
		documentTypeDAO.save(documentType);
	}
	
	@Override
	public void updateDocumentType(DocumentType documentType) {
		
		documentTypeDAO.update(documentType);
	}
	
	
	public void saveDocumentCategory(DocumentCategory documentCategory){
		DocumentType documentType;
		
		//end temp
		if(documentCategory.getId()!=null && documentCategory.getId().intValue()>0){
			documentCategory=documentCategoryDAO.update(documentCategory);
			
			
		 }else{
			documentCategory=documentCategoryDAO.save(documentCategory);
		 }
			
		
	}
	
	
	
	
	
	private DocumentTypeDAO documentTypeDAO;
	private DocumentCategoryDAO documentCategoryDAO;
	
	private DAOManager daoManager;
	private PolicyDAO policyDAO;

	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.documentTypeDAO=daoManager.getDocumentTypeDAO();
		this.documentCategoryDAO=daoManager.getDocumentCategoryDAO();
		this.policyDAO=daoManager.getPolicyDAO();
	}

	
	
}
	
