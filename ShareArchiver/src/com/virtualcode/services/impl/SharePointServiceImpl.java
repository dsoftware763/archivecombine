package com.virtualcode.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.virtualcode.dao.AgentDAO;
import com.virtualcode.dao.DAOManager;
import com.virtualcode.services.SharePointService;
import com.virtualcode.util.VirtualcodeUtil;
import com.virtualcode.vo.Agent;
import com.virtualcode.vo.SPTreeNode;

public class SharePointServiceImpl implements SharePointService {

	private org.vcsl.das.schedular.client.SchedularClient schedularClient;
	private ResourceBundle bundle = VirtualcodeUtil.getResourceBundle();//ResourceBundle.getBundle("config");
	
    //private String webServiceURL;
    private String targetNamespace;
    private String serviceName;
    private String portName;
    private String portType;
	
	@Override
	public List getSiteList() {
		// TODO Auto-generated method stub
		System.out.println("siteList from : getSiteList()");
		List<Agent> agentList = agentDAO.findByType("SP");
		List<String> siteList = null;
		if(agentList!=null && agentList.size()>0)
		{
			String webServiceURL = agentList.get(agentList.size()-1).getAllowedIps();
			if(webServiceURL==null || webServiceURL.isEmpty() || webServiceURL.trim().equals("")){
				return null;
			}else{
				try{
			//	schedularClient = new org.vcsl.das.schedular.client.SchedularClientImpl();
	                schedularClient.initFarmStructure(webServiceURL,targetNamespace,serviceName,portType,portName);
	                siteList=schedularClient.getSiteUrl();
	                System.out.println("*************siteList*****************");
	                for(String site: siteList){
	                	System.out.println(site);
	                }
	                System.out.println("*************siteList end**************");
				}catch(Exception ex){
					System.out.println("Exception in service: " + ex.getMessage());
					ex.printStackTrace();
					return null;
				}
			}
		}
		return siteList;
	}

	@Override
	public List getSiteListByURL(String webServiceURL) {
		// TODO Auto-generated method stub
		System.out.println("siteList from : getSiteList()");
		List<String> siteList = null;
			if(webServiceURL==null || webServiceURL.isEmpty() || webServiceURL.trim().equals("")){
				return null;
			}else{
				try{
                schedularClient.initFarmStructure(webServiceURL,targetNamespace,serviceName,portType,portName);
                siteList=schedularClient.getSiteUrl();
                System.out.println("*************siteList*****************");
                for(String site: siteList){
                	System.out.println(site);
                }
                System.out.println("*************siteList end**************");
				}catch(Exception ex){
					System.out.println("Exception in service: " + ex.getMessage());
					ex.printStackTrace();
					return null;
				}
			}
		
		return siteList;
	}
	
	@Override
	public List getSiteNodesList() {
		// TODO Auto-generated method stub
		System.out.println("siteList from : getSiteList()");
		List<Agent> agentList = agentDAO.findByType("SP");
		List<String> siteList = null;
		if(agentList!=null && agentList.size()>0)
		{
			String webServiceURL = agentList.get(agentList.size()-1).getAllowedIps();
			if(webServiceURL==null || webServiceURL.isEmpty() || webServiceURL.trim().equals("")){
				return null;
			}else{
				try{
			//	schedularClient = new org.vcsl.das.schedular.client.SchedularClientImpl();
	                schedularClient.initFarmStructure(webServiceURL,targetNamespace,serviceName,portType,portName);
	                siteList=schedularClient.getSiteUrl();
	                System.out.println("*************siteList*****************");
	                for(String site: siteList){
	                	System.out.println(site);
	                }
	                System.out.println("*************siteList end**************");
				}catch(Exception ex){
					System.out.println("Exception in service: " + ex.getMessage());
					ex.printStackTrace();
					return null;
				}
			}
		}
		return siteList;
	}
	
	@Override
	public List getSiteContentTypes(String webServiceURL, String siteUrl) {
		// TODO Auto-generated method stub
		System.out.println("contentType from : getSiteContentTypes()");		
		List<String> contentList = null;
		if(webServiceURL==null || webServiceURL.isEmpty() || webServiceURL.trim().equals("")){
			return null;
		}else{
			try{
		//	schedularClient = new org.vcsl.das.schedular.client.SchedularClientImpl();
                schedularClient.initFarmStructure(webServiceURL,targetNamespace,serviceName,portType,portName);
                contentList=schedularClient.getSiteContentType(siteUrl);
                System.out.println("*************contentList*****************");
                for(String type: contentList){
                	System.out.println(type.replaceAll("\\\\", ""));
                }
                System.out.println("*************contentList end**************");
			}catch(Exception ex){
				System.out.println("Exception in service: " + ex.getMessage());
				ex.printStackTrace();
				return null;
			}
		}

		return contentList;
	}

	@Override
	public List getSiteNodesListByURL(String webServiceURL) {
		// TODO Auto-generated method stub
		System.out.println("siteList from : getSiteList()");
		List<String> siteList = null;
		List<SPTreeNode> siteNodesList = null;
			if(webServiceURL==null || webServiceURL.isEmpty() || webServiceURL.trim().equals("")){
				return null;
			}else{
				try{
                schedularClient.initFarmStructure(webServiceURL,targetNamespace,serviceName,portType,portName);
                siteList=schedularClient.getSiteUrl();
                siteNodesList = new ArrayList<SPTreeNode>();
                System.out.println("*************siteList*****************");
                for(String site: siteList){
                	SPTreeNode spTreeNode = new SPTreeNode();
                	spTreeNode.setName(site);
                	spTreeNode.setType("site");
                	spTreeNode.setPath(site);
                	spTreeNode.setGuid(getSiteId(site, webServiceURL));
                	spTreeNode.setSelected(false);
                	siteNodesList.add(spTreeNode);
                	System.out.println(site);
                }
                System.out.println("*************siteList end**************");
				}catch(Exception ex){
					System.out.println("Exception in service: " + ex.getMessage());
					ex.printStackTrace();
					return null;
				}
			}
		
		return siteNodesList;
	}

	@Override
	public List getLibraryList(String sitePathOrSiteURL, String webServiceURL) {
		// TODO Auto-generated method stub
		System.out.println("siteList from : getLibraryList()");
		List<String> libraryList = null;
		if(webServiceURL==null || webServiceURL.isEmpty() || webServiceURL.trim().equals("")){
			return null;
		}else{
            schedularClient.initFarmStructure(webServiceURL,targetNamespace,serviceName,portType,portName);
            libraryList=schedularClient.getLibraryList(sitePathOrSiteURL);
            System.out.println("*************libraryList*****************");
            for(String library: libraryList){
            	System.out.println(library);
            	System.out.println(getLibraryId(sitePathOrSiteURL, library, webServiceURL));
            }
            System.out.println("*************libraryList end**************");
		}
		return libraryList;
	}

	@Override
	public List getSubLibraryList(String guid, String sitePathOrSiteURL,
			String webServiceURL) {
		// TODO Auto-generated method stub
		System.out.println("siteList from : getLSubibraryList()");
		List<String> libraryList = null;
		if(webServiceURL==null || webServiceURL.isEmpty() || webServiceURL.trim().equals("")){
			return null;
		}else{
            schedularClient.initFarmStructure(webServiceURL,targetNamespace,serviceName,portType,portName);
            libraryList=schedularClient.getLibraryList(guid, sitePathOrSiteURL);
            System.out.println("*************libraryList*****************");
            for(String library: libraryList){
            	System.out.println(library);
            	
            }
            System.out.println("*************libraryList end**************");
		}
		return libraryList;
	}
	
	@Override
	public List getLibraryNodesList(String sitePathOrSiteURL, String webServiceURL) {
		// TODO Auto-generated method stub
		System.out.println("siteList from : getLibraryNodesList()");
		List<String> libraryList = null;
		List<SPTreeNode> libraryNodesList = new ArrayList<SPTreeNode>();
		if(webServiceURL==null || webServiceURL.isEmpty() || webServiceURL.trim().equals("")){
			return null;
		}else{
            schedularClient.initFarmStructure(webServiceURL,targetNamespace,serviceName,portType,portName);
            libraryList=schedularClient.getLibraryList(sitePathOrSiteURL);
            System.out.println("*************libraryList*****************");
            for(String library: libraryList){
            	SPTreeNode spTreeNode = new SPTreeNode();
            	spTreeNode.setName(library);
            	spTreeNode.setType("library");
            	spTreeNode.setGuid(getLibraryId(sitePathOrSiteURL, library, webServiceURL));
            	if(!(library.startsWith("http")))
            		spTreeNode.setPath(sitePathOrSiteURL+"/"+library);
            	else
            		spTreeNode.setPath(library);
            	spTreeNode.setSitePath(getLibrarySiteUrl(sitePathOrSiteURL, library, webServiceURL));
            	spTreeNode.setParentPath(sitePathOrSiteURL);
            	spTreeNode.setSelected(false);
            	spTreeNode.setSubLibrary(false);
            	System.out.println(library);
            	libraryNodesList.add(spTreeNode);
//            	System.out.println(getLibraryId(sitePathOrSiteURL, library, webServiceURL));
            }
            System.out.println("*************libraryList end**************");
		}
		return libraryNodesList;
	}

	@Override
	public List getSubLibraryNodesList(String guid, String sitePathOrSiteURL,
			String webServiceURL) {
		// TODO Auto-generated method stub
		System.out.println("siteList from : getLSubibraryNodesList()");
		List<String> libraryList = null;
		List<SPTreeNode> libraryNodesList = new ArrayList<SPTreeNode>();
		if(webServiceURL==null || webServiceURL.isEmpty() || webServiceURL.trim().equals("")){
			return null;
		}else{
            schedularClient.initFarmStructure(webServiceURL,targetNamespace,serviceName,portType,portName);
            libraryList=schedularClient.getLibraryList(guid, sitePathOrSiteURL);
            System.out.println("*************libraryList*****************");
            for(String library: libraryList){
            	System.out.println(library);
            	SPTreeNode spTreeNode = new SPTreeNode();
            	spTreeNode.setName(library);
            	spTreeNode.setType("library");
            	spTreeNode.setGuid(getSubLibraryId(guid,sitePathOrSiteURL, library, webServiceURL));
            	spTreeNode.setParentPath(sitePathOrSiteURL);
            	spTreeNode.setPath(sitePathOrSiteURL+"/"+library);
            	spTreeNode.setSitePath(getSubLibrarySiteUrl(guid,sitePathOrSiteURL, library, webServiceURL));
            	spTreeNode.setSelected(false);
            	spTreeNode.setSubLibrary(true);
            	System.out.println(library);
            	libraryNodesList.add(spTreeNode);
            	
            }
            System.out.println("*************libraryList end**************");
		}
		return libraryNodesList;
	}

	@Override
	public List getLibraryNodesList(String sitePathOrSiteURL, List<String> paths, String webServiceURL) {
		// TODO Auto-generated method stub
		System.out.println("siteList from : getLibraryNodesList()");
		List<String> libraryList = null;
		List<SPTreeNode> libraryNodesList = new ArrayList<SPTreeNode>();
		if(webServiceURL==null || webServiceURL.isEmpty() || webServiceURL.trim().equals("")){
			return null;
		}else{
            schedularClient.initFarmStructure(webServiceURL,targetNamespace,serviceName,portType,portName);
            libraryList=schedularClient.getLibraryList(sitePathOrSiteURL);
            System.out.println("*************libraryList*****************");
            for(String library: libraryList){
            	SPTreeNode spTreeNode = new SPTreeNode();
            	spTreeNode.setName(library);
            	spTreeNode.setType("library");
            	spTreeNode.setGuid(getLibraryId(sitePathOrSiteURL, library, webServiceURL));
            	String libPath = sitePathOrSiteURL+"/"+library;
            	spTreeNode.setPath(libPath);
            	for(String path:paths){
	            	if(libPath.toLowerCase().equalsIgnoreCase(path)){
	            		spTreeNode.setSelected(true);
	            		break;
	            	}else{
	            		spTreeNode.setSelected(false);
	            	}
            	}
            	spTreeNode.setSubLibrary(false);
            	System.out.println(library);
            	libraryNodesList.add(spTreeNode);
//            	System.out.println(getLibraryId(sitePathOrSiteURL, library, webServiceURL));
            }
            System.out.println("*************libraryList end**************");
		}
		return libraryNodesList;
	}

	@Override
	public List getSubLibraryNodesList(String guid, String sitePathOrSiteURL, List<String> paths,
			String webServiceURL) {
		// TODO Auto-generated method stub
		System.out.println("siteList from : getLSubibraryNodesList()");
		List<String> libraryList = null;
		List<SPTreeNode> libraryNodesList = new ArrayList<SPTreeNode>();
		if(webServiceURL==null || webServiceURL.isEmpty() || webServiceURL.trim().equals("")){
			return null;
		}else{
            schedularClient.initFarmStructure(webServiceURL,targetNamespace,serviceName,portType,portName);
            libraryList=schedularClient.getLibraryList(guid, sitePathOrSiteURL);
            System.out.println("*************libraryList*****************");
            for(String library: libraryList){
            	System.out.println(library);
            	SPTreeNode spTreeNode = new SPTreeNode();
            	spTreeNode.setName(library);
            	spTreeNode.setType("library");
            	spTreeNode.setGuid(getSubLibraryId(guid,sitePathOrSiteURL, library, webServiceURL));
            	spTreeNode.setParentPath(sitePathOrSiteURL);
            	String libPath = sitePathOrSiteURL+"/"+library;
            	spTreeNode.setPath(libPath);
            	for(String path:paths){
	            	if(libPath.toLowerCase().equalsIgnoreCase(path)){
	            		spTreeNode.setSelected(true);
	            		break;
	            	}else{
	            		spTreeNode.setSelected(false);
	            	}
            	}
            	spTreeNode.setSubLibrary(true);
            	System.out.println(library);
            	libraryNodesList.add(spTreeNode);
            	
            }
            System.out.println("*************libraryList end**************");
		}
		return libraryNodesList;
	}	
	@Override
	public String getSiteId(String sitePathOrSiteURL, String webServiceURL) {
		// TODO Auto-generated method stub
		String siteId = null;
		if(webServiceURL==null || webServiceURL.isEmpty() || webServiceURL.trim().equals("")){
			return null;
		}else{
            schedularClient.initFarmStructure(webServiceURL,targetNamespace,serviceName,portType,portName);
            siteId=schedularClient.getSiteIdByUrl(sitePathOrSiteURL);
            System.out.println("*************siteId"+siteId);            
		}
		return siteId;
	}

	@Override
	public String getLibraryId(String sitePathOrSiteURL, String libraryName,
			String webServiceURL) {
		// TODO Auto-generated method stub
		String libraryId = null;
		if(webServiceURL==null || webServiceURL.isEmpty() || webServiceURL.trim().equals("")){
			return null;
		}else{
            schedularClient.initFarmStructure(webServiceURL,targetNamespace,serviceName,portType,portName);
            libraryId=schedularClient.getLibraryIdByName(sitePathOrSiteURL,libraryName);
            System.out.println("*************libraryId"+libraryId);            
		}
		return libraryId;		

	}

	@Override
	public String getSubLibraryId(String guid, String sitePathOrSiteURL,
			String libraryName, String webServiceURL) {
		// TODO Auto-generated method stub
		String libraryId = null;
		if(webServiceURL==null || webServiceURL.isEmpty() || webServiceURL.trim().equals("")){
			return null;
		}else{
            schedularClient.initFarmStructure(webServiceURL,targetNamespace,serviceName,portType,portName);
            libraryId=schedularClient.getLibraryIdByName(guid,sitePathOrSiteURL,libraryName);
            System.out.println("*************libraryId "+libraryId);            
		}
		return libraryId;		
	}
	
	@Override
	public String getLibrarySiteUrl(String sitePathOrSiteURL, String libraryName,
			String webServiceURL) {
		// TODO Auto-generated method stub
		String libraryUrl = null;
		if(webServiceURL==null || webServiceURL.isEmpty() || webServiceURL.trim().equals("")){
			return null;
		}else{
            schedularClient.initFarmStructure(webServiceURL,targetNamespace,serviceName,portType,portName);
            libraryUrl=schedularClient.getLibrarySiteUrlByName(sitePathOrSiteURL,libraryName);
            System.out.println("*************libraryUrl"+libraryUrl);            
		}
		return libraryUrl;		

	}

	@Override
	public String getSubLibrarySiteUrl(String guid, String sitePathOrSiteURL,
			String libraryName, String webServiceURL) {
		// TODO Auto-generated method stub
		String libraryUrl = null;
		if(webServiceURL==null || webServiceURL.isEmpty() || webServiceURL.trim().equals("")){
			return null;
		}else{
            schedularClient.initFarmStructure(webServiceURL,targetNamespace,serviceName,portType,portName);
            libraryUrl=schedularClient.getLibrarySiteUrlByName(guid,sitePathOrSiteURL,libraryName);
            System.out.println("*************libraryUrl "+libraryUrl);            
		}
		return libraryUrl;		
	}	
	
	public void setServiceProperties(){
		schedularClient = new org.vcsl.das.schedular.client.SchedularClientImpl();
		targetNamespace = bundle.getString("targetNamespace");
        serviceName = bundle.getString("serviceName");
        portName = bundle.getString("portName");
        portType = bundle.getString("portType");
        
	}
	
	private AgentDAO agentDAO;
	private DAOManager daoManager;

	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		agentDAO = this.daoManager.getAgentDAO();
		//initialize Web service properties
		setServiceProperties();
	}

	@Override
	public boolean testSPAgentService(String webServiceUrl) {
		// TODO Auto-generated method stub
		try{
			 schedularClient.initFarmStructure(webServiceUrl,targetNamespace,serviceName,portType,portName);
			String str = schedularClient.getAgentInfo();
			if(str!=null && str.length()>0)
				return true;
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return false;
	}
	
	
	

}
