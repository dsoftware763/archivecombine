package com.virtualcode.services.impl;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockMode;
import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.JobStatusDAO;
import com.virtualcode.repository.impl.UploadDocumentServiceImpl;
import com.virtualcode.services.JobStatusService;
import com.virtualcode.vo.JobStatus;

/**
 * A data access object (DAO) providing persistence and search support for
 * JobStatus entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.virtualcode.vo.JobStatus
 * @author MyEclipse Persistence Tools
 */

public class JobStatusServiceImpl implements JobStatusService{
	Logger log=Logger.getLogger(JobStatisticsServiceImpl.class);
	@Override
	public void saveJobStatus(JobStatus jobStatus) {
		// TODO Auto-generated method stub		
		jobStatusDAO.save(jobStatus);
	}

	@Override
	public void deleteJobStatus(JobStatus jobStatus) {
		// TODO Auto-generated method stub
		jobStatusDAO.delete(jobStatus);
	}

	@Override
	public JobStatus getJobStatusById(Integer id) {
		// TODO Auto-generated method stub
		return jobStatusDAO.findById(id);
	}

	@Override
	public List<JobStatus> getJobStatusByExample(JobStatus instance) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<JobStatus> getJobStatusByProperty(String propertyName,
			Object value) {
		// TODO Auto-generated method stub
		return jobStatusDAO.findByProperty(propertyName, value);
	}

	@Override
	public List<JobStatus> getJobStatusByDescription(Object description) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<JobStatus> getJobStatusByIsCurrent(Object isCurrent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<JobStatus> getJobStatusByPreviousJobStatusId(
			Object previousJobStatusId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<JobStatus> getJobStatusByStatus(Object status) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<JobStatus> getJobStatusByExecutionId(Object executionId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<JobStatus> getAll() {
		// TODO Auto-generated method stub
		return jobStatusDAO.findAll();
	}

	@Override
	public List<JobStatus> getJobStatusByJob(Object jobId) {
		// TODO Auto-generated method stub
//		System.out.println("Finding statuses for job id:" + jobId);
		log.info("Finding statuses for job id:" + jobId);
		
		return jobStatusDAO.findByJob(jobId);
	}
	
	private JobStatusDAO jobStatusDAO;
	private DAOManager daoManager;
	
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.jobStatusDAO = this.daoManager.getJobStatusDAO();
	}
	
	

	
}