package com.virtualcode.services.impl;

import java.util.List;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.SystemCodeDAO;
import com.virtualcode.services.SystemCodeService;
import com.virtualcode.vo.SystemCode;

public class SystemCodeServiceImpl implements SystemCodeService{

	@Override
	public void saveSystemCode(SystemCode systemCode) {
		systemCodeDAO.save(systemCode);
		
	}
	
	@Override
	public void update(SystemCode systemCode){
		
		systemCodeDAO.update(systemCode);		
	}

	@Override
	public void deleteSystemCode(SystemCode systemCode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public SystemCode getSystemCodeById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SystemCode> getSystemCodeByExample(SystemCode systemCode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SystemCode> getSystemCodeByProperty(String propertyName,
			Object value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SystemCode> getSystemCodeByCodename(Object codename) {
		// TODO Auto-generated method stub
		List<SystemCode> systemCodeList = systemCodeDAO.findByCodename(codename);
		if(systemCodeList!=null && systemCodeList.size()>0){
			return systemCodeList;
		}
		return null;
	}

	@Override
	public List<SystemCode> getSystemCodeByCodetype(Object codetype) {
		// TODO Auto-generated method stub
		List<SystemCode> systemCodeList = systemCodeDAO.findByCodetype(codetype);
		if(systemCodeList!=null && systemCodeList.size()>0){
			return systemCodeList;
		}
		return null;
	}

	@Override
	public List<SystemCode> getSystemCodeByCodevalue(Object codevalue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SystemCode> getSystemCodeByEditable(Object editable) {
		// TODO Auto-generated method stub
		return systemCodeDAO.findByEditable(editable);
	}

	@Override
	public List<SystemCode> getSystemCodeByViewable(Object viewable) {
		// TODO Auto-generated method stub
		return systemCodeDAO.findByViewable(viewable);
	}

	@Override
	public List<SystemCode> getAll() {
		// TODO Auto-generated method stub
		return systemCodeDAO.findAll();
	}
	
	
	private SystemCodeDAO systemCodeDAO;
	private DAOManager daoManager;
	
	public void setDaoManager(DAOManager daoManager) {
		System.out.println("------------------------------in setter");
		this.daoManager = daoManager;
		this.systemCodeDAO = daoManager.getSystemCodeDAO();
	}

	@Override
	public SystemCode getSystemCodeByName(Object codename) {
		// TODO Auto-generated method stub
		List<SystemCode> systemCodeList = systemCodeDAO.findByCodename(codename);
		if(systemCodeList!=null && systemCodeList.size()>0){
			return systemCodeList.get(0);
		}
		return null;
	}
	
	
   public List<SystemCode> getAllGeneralCodes(){
	   return systemCodeDAO.findAllGeneralCodes();
   }
	
	public List<SystemCode> getAllLDAPCodes(){
		
		return systemCodeDAO.findAllLDAPCodes();
	}
	
	public List<SystemCode> getAllJespCodes(){
		
		return systemCodeDAO.findAllJespCodes();
	}
	
	public List<SystemCode> getAllAgentCodes(){
		
		return systemCodeDAO.findAllAgentCodes();
	}
	
	public List<SystemCode> getAllCompanyCodes(){
		   return systemCodeDAO.findAllCompanyCodes();
	}
	
	public List<SystemCode> getAllAccountPolicyCodes(){
		   return systemCodeDAO.findAllAccountPolicyCodes();
	}
	
}
