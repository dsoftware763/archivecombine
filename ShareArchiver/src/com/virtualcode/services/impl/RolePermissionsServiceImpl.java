package com.virtualcode.services.impl;

import java.util.List;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.RolePermissionsDAO;
import com.virtualcode.services.RolePermissionsService;
import com.virtualcode.vo.RolePermissions;
import com.virtualcode.vo.RolePermissionsId;



public class RolePermissionsServiceImpl implements RolePermissionsService {

	@Override
	public void save(RolePermissions transientInstance) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(RolePermissions persistentInstance) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public RolePermissions getById(RolePermissionsId id) {
		return rolePermissionsDAO.findById(id);
	}

	@Override
	public List getAll() {
		// TODO Auto-generated method stub
		return null;
	}
	
	private RolePermissionsDAO rolePermissionsDAO;
	private DAOManager daoManager;
    
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.rolePermissionsDAO=daoManager.getRolePermissionsDAO();
	}
	
	
}