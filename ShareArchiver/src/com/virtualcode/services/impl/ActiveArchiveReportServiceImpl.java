package com.virtualcode.services.impl;

import java.util.List;

import com.virtualcode.dao.ActiveArchiveReportDAO;
import com.virtualcode.dao.DAOManager;
import com.virtualcode.services.ActiveArchiveReportService;
import com.virtualcode.vo.ActiveArchiveReport;

public class ActiveArchiveReportServiceImpl implements ActiveArchiveReportService{

	@Override
	public void save(ActiveArchiveReport transientInstance) {
		// TODO Auto-generated method stub
		activeArchiveReportDAO.save(transientInstance);
	}

	@Override
	public void delete(ActiveArchiveReport persistentInstance) {
		// TODO Auto-generated method stub
		activeArchiveReportDAO.delete(persistentInstance);
	}

	@Override
	public ActiveArchiveReport findById(Integer id) {
		// TODO Auto-generated method stub
		return activeArchiveReportDAO.findById(id);
	}

	@Override
	public List<ActiveArchiveReport> getByExample(ActiveArchiveReport instance) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ActiveArchiveReport> getByProperty(String propertyName,
			Object value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ActiveArchiveReport> getByDailyCount(Object dailyCount) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ActiveArchiveReport> getByTotalCount(Object totalCount) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ActiveArchiveReport> getByTotalJobs(Object totalJobs) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ActiveArchiveReport> getAll() {
		// TODO Auto-generated method stub
		return activeArchiveReportDAO.findAll();
	}
	
	private DAOManager daoManager;
	private ActiveArchiveReportDAO activeArchiveReportDAO;
	
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.activeArchiveReportDAO = this.daoManager.getActiveArchiveReportDAO();
	}
	
	

}
