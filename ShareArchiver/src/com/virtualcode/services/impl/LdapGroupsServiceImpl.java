package com.virtualcode.services.impl;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.LdapGroupsDAO;
import com.virtualcode.services.LdapGroupsService;
import com.virtualcode.vo.LdapGroups;

/**
 * A data access object (DAO) providing persistence and search support for
 * LdapGroups entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.virtualcode.vo.LdapGroups
 * @author MyEclipse Persistence Tools
 */

public class LdapGroupsServiceImpl implements LdapGroupsService{

	@Override
	public void saveLdapGroup(LdapGroups transientInstance) {
		// TODO Auto-generated method stub
		ldapGroupsDAO.save(transientInstance);
	}

	@Override
	public void deleteLdapGroup(LdapGroups persistentInstance) {
		// TODO Auto-generated method stub
		ldapGroupsDAO.delete(persistentInstance);
	}

	@Override
	public LdapGroups getLdapGroupById(Integer id) {
		// TODO Auto-generated method stub
		return ldapGroupsDAO.findById(id);
	}

	@Override
	public List<LdapGroups> getLdapGroupByExample(LdapGroups instance) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<LdapGroups> getLdapGroupByProperty(String propertyName,
			Object value) {
		// TODO Auto-generated method stub
		return ldapGroupsDAO.findByProperty(propertyName, value);
	}

	@Override
	public List<LdapGroups> getLdapGroupByGroupName(Object groupName) {
		// TODO Auto-generated method stub
		return ldapGroupsDAO.findByGroupName(groupName);
	}

	@Override
	public List<LdapGroups> getLdapGroupByRestrictGroup(Object restrictGroup) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<LdapGroups> getLdapGroupByEnableTranscript(
			Object enableTranscript) {
		// TODO Auto-generated method stub
		return ldapGroupsDAO.findByEnableTranscript(enableTranscript);
	}

	@Override
	public List<LdapGroups> getAll() {
		// TODO Auto-generated method stub
		return ldapGroupsDAO.findAll();
	}

	private DAOManager daoManager;
	private LdapGroupsDAO ldapGroupsDAO;
	
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.ldapGroupsDAO = this.daoManager.getLdapGroupsDAO();
	}
	
}