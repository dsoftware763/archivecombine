package com.virtualcode.services.impl;

import java.util.List;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.ShareAnalysisStatsDAO;
import com.virtualcode.services.ShareAnalysisStatsService;
import com.virtualcode.vo.ShareAnalysisStats;

public class ShareAnalysisStatsServiceImpl implements ShareAnalysisStatsService {

	@Override
	public void save(ShareAnalysisStats transientInstance) {
		shareAStatsDAO.save(transientInstance);
		
	}

	@Override
	public void delete(ShareAnalysisStats persistentInstance) {
		shareAStatsDAO.delete(persistentInstance);
		
	}
	
	@Override
	public void deleteByDriveId(Integer driveLetterId){
		shareAStatsDAO.deleteByDriveId(driveLetterId);
	}

	@Override
	public ShareAnalysisStats getById(Long id) {
		return shareAStatsDAO.findById(id);
	}

	@Override
	public List<ShareAnalysisStats> getByExample(ShareAnalysisStats instance) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ShareAnalysisStats> getByProperty(String propertyName, Object value) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public List<ShareAnalysisStats> getByShare(Integer shareId) {
		return shareAStatsDAO.findByShare(shareId);
	}


	@Override
	public List<ShareAnalysisStats> getByDocumentCatName(Object documentCatName) {
		return shareAStatsDAO.findByDocumentCatName(documentCatName);
	}

	
	@Override
	public List<ShareAnalysisStats> findAll() {
		return shareAStatsDAO.findAll();
	}

	
	private ShareAnalysisStatsDAO shareAStatsDAO;
	
	private DAOManager daoManager;
  
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.shareAStatsDAO=daoManager.getShareAnalysisStatsDAO();
	}
	
	
	
	

}
