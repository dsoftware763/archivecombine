package com.virtualcode.services.impl;

import java.util.List;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.SystemAlertsDAO;
import com.virtualcode.services.SystemAlertsService;
import com.virtualcode.vo.SystemAlerts;

public class SystemAlertsServiceImpl implements SystemAlertsService {

	@Override
	public void saveSystemAlert(SystemAlerts systemAlert) {
		// TODO Auto-generated method stub
		systemAlertsDAO.save(systemAlert);
	}

	@Override
	public void deleteSystemAlert(SystemAlerts systemAlert) {
		// TODO Auto-generated method stub
		systemAlertsDAO.delete(systemAlert);
	}

	@Override
	public SystemAlerts getSystemAlertsById(Integer id) {
		// TODO Auto-generated method stub
		return systemAlertsDAO.findById(id);
	}

	@Override
	public List<SystemAlerts> getSystemAlertsByExample(SystemAlerts instance) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SystemAlerts> getSystemAlertsByProperty(String propertyName,
			Object value) {
		// TODO Auto-generated method stub
		return systemAlertsDAO.findByProperty(propertyName, value);
	}

	@Override
	public List<SystemAlerts> getSystemAlertsByUsername(Object username) {
		// TODO Auto-generated method stub
		return systemAlertsDAO.findByUsername(username);
	}

	@Override
	public List<SystemAlerts> getSystemAlertsByAlertType(Object alertType) {
		// TODO Auto-generated method stub
		return systemAlertsDAO.findByAlertType(alertType);
	}

	@Override
	public List<SystemAlerts> getSystemAlertsByAlertMessage(Object alertMessage) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SystemAlerts> getAll() {
		// TODO Auto-generated method stub
		return systemAlertsDAO.findAll();
	}
	
	private DAOManager daoManager;
	private SystemAlertsDAO systemAlertsDAO;
	
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.systemAlertsDAO = this.daoManager.getSystemAlertsDAO();
	}
	
	

}
