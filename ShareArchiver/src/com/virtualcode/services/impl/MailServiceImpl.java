package com.virtualcode.services.impl;

import java.io.File;
import java.util.List;

import javax.mail.internet.MimeMessage;


import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.sun.xml.internal.ws.api.message.Attachment;
import com.virtualcode.services.MailService;
import com.virtualcode.services.SystemCodeService;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.SystemCode;

/**
 * @author se8
 *
 */
public class MailServiceImpl implements MailService{

	
	
	public boolean sendMail(String subject, String msg) {
		 
		MimeMessage message=mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);
				
		try{
			
			List<SystemCode> sysCodeList=systemCodeService.getSystemCodeByCodename("mail_from");
			if(sysCodeList!=null && sysCodeList.size()>0){			
				helper.setFrom(sysCodeList.get(0).getCodevalue());
			}
			
			sysCodeList=systemCodeService.getSystemCodeByCodename("mail_to");
			if(sysCodeList!=null && sysCodeList.size()>0){			
				helper.setTo(sysCodeList.get(0).getCodevalue());
			}
						
			helper.setSubject(subject);
			helper.setText(msg,true);
		    mailSender.send(message);	
		   return true;
		}catch(Exception ex){
			ex.printStackTrace();
			return false;
		}
	}
	
	public boolean sendTestMail(String host,int port,String emailSecProtocol,String username,String password,String from,String to) {
		 
		MimeMessage message=mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);
		try{
			
			if(emailSecProtocol.equalsIgnoreCase("basic")){
				  this.mailSender.setProtocol("smtp");
				  this.mailSender.getJavaMailProperties().setProperty("mail.smtp.ssl.enable", "false");
				  this.mailSender.getJavaMailProperties().setProperty("mail.smtp.starttls.enable", "false");
			}
			else if(emailSecProtocol.equalsIgnoreCase("ssl")){
				  this.mailSender.setProtocol("smtps");
				  this.mailSender.getJavaMailProperties().setProperty("mail.smtp.ssl.enable", "true");
				  this.mailSender.getJavaMailProperties().setProperty("mail.smtp.starttls.enable", "false");
			  }else{
				  this.mailSender.setProtocol("smtp");
				  this.mailSender.getJavaMailProperties().setProperty("mail.smtp.ssl.enable", "false");
				  this.mailSender.getJavaMailProperties().setProperty("mail.smtp.starttls.enable", "true");
			  }
			 this.mailSender.getJavaMailProperties().setProperty("mail.smtp.ssl.trust", host);
			
			mailSender.setHost(host);
			mailSender.setPort(port);
			mailSender.setUsername(username);
			mailSender.setPassword(password);
			helper.setValidateAddresses(true);
			helper.setFrom(from);
			helper.setTo(to);					
			helper.setSubject("Test configuration email");
			helper.setText("Test configuration email",true);
		    mailSender.send(message);	
		   return true;
		}catch(Exception ex){
			ex.printStackTrace();
			return false;
		}
	}
	
	public boolean sendMail(String toEmail,String subject, String msgbody,String fileName,File attachment) {
			
		try{
			
			MimeMessage message=mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message,true);
			
			List<SystemCode> sysCodeList=systemCodeService.getSystemCodeByCodename("mail_from");
			if(sysCodeList!=null && sysCodeList.size()>0){			
				helper.setFrom(sysCodeList.get(0).getCodevalue());
			}
			
			helper.setTo(toEmail);
									
			helper.setSubject(subject);
			helper.setText(msgbody,true);
		    if(attachment!=null && attachment.exists()){
		    	helper.addAttachment(fileName, attachment);
		    }
			mailSender.send(message);	
		   return true;
		}catch(Exception ex){
			ex.printStackTrace();
			return false;
		}
	}
	
	public boolean sendMail(String fromEmail, String toEmail,String subject, String msgbody,String fileName,File attachment) {
		
		try{
			
			MimeMessage message=mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message,true);
			
//			List<SystemCode> sysCodeList=systemCodeService.getSystemCodeByCodename("mail_from");
//			if(sysCodeList!=null && sysCodeList.size()>0){			
				helper.setFrom(fromEmail);
//			}
			
			helper.setTo(toEmail);
									
			helper.setSubject(subject);
			helper.setText(msgbody,true);
		    if(attachment!=null && attachment.exists()){
		    	helper.addAttachment(fileName, attachment);
		    }
			mailSender.send(message);	
		   return true;
		}catch(Exception ex){
			ex.printStackTrace();
			return false;
		}
	}
		
	private JavaMailSenderImpl mailSender;//=(JavaMailSender)SpringApplicationContext.getBean("mailSender");
		
	public void setMailSender(JavaMailSenderImpl mailSender) {
		this.mailSender = mailSender;
		List<SystemCode> sysCodeList=systemCodeService.getSystemCodeByCodename("mail_username");
		if(sysCodeList!=null && sysCodeList.size()>0){			
		    this.mailSender.setUsername(sysCodeList.get(0).getCodevalue());
		}
		sysCodeList=systemCodeService.getSystemCodeByCodename("mail_password");
		if(sysCodeList!=null && sysCodeList.size()>0){			
		    this.mailSender.setPassword(sysCodeList.get(0).getCodevalue());
		}
		
		sysCodeList=systemCodeService.getSystemCodeByCodename("smpt_host");
		if(sysCodeList!=null && sysCodeList.size()>0){			
		    this.mailSender.setHost(sysCodeList.get(0).getCodevalue());
		}
		sysCodeList=systemCodeService.getSystemCodeByCodename("smpt_port");
		if(sysCodeList!=null && sysCodeList.size()>0){			
		    this.mailSender.setPort(VCSUtil.getIntValue(sysCodeList.get(0).getCodevalue()));
		}
		
		sysCodeList=systemCodeService.getSystemCodeByCodename("mail_sec_protocol");
		if(sysCodeList!=null && sysCodeList.size()>0){			
			String emailSecProtocol=sysCodeList.get(0).getCodevalue();  
			
			if(emailSecProtocol.equalsIgnoreCase("basic")){
				  this.mailSender.setProtocol("smtp");
				  this.mailSender.getJavaMailProperties().setProperty("mail.smtp.ssl.enable", "false");
				  this.mailSender.getJavaMailProperties().setProperty("mail.smtp.starttls.enable", "false");
			}
			else if(emailSecProtocol.equalsIgnoreCase("ssl")){
				  this.mailSender.setProtocol("smtps");
				  this.mailSender.getJavaMailProperties().setProperty("mail.smtp.ssl.enable", "true");
				  this.mailSender.getJavaMailProperties().setProperty("mail.smtp.starttls.enable", "false");
			  }else{
				  this.mailSender.setProtocol("smtp");
				  this.mailSender.getJavaMailProperties().setProperty("mail.smtp.ssl.enable", "false");
				  this.mailSender.getJavaMailProperties().setProperty("mail.smtp.starttls.enable", "true");
			  }		
		}
	
		
	}
	
	
	//This method is to forecefully re-initialize
	public void reInitilize(){ 
		setMailSender(mailSender);
	}
	
	public void reInitilize(String host,int port,String userName,String password,String fromEmail,String emailSecurityProtocol){ 
		  this.mailSender.setHost(host);
		  this.mailSender.setPort(port);
		  this.mailSender.setUsername(userName);
		  this.mailSender.setPassword(password);
		 
		  if(emailSecurityProtocol.equalsIgnoreCase("basic")){
			  this.mailSender.setProtocol("smtp");
			  this.mailSender.getJavaMailProperties().setProperty("mail.smtp.ssl.enable", "false");
			  this.mailSender.getJavaMailProperties().setProperty("mail.smtp.starttls.enable", "false");
		  }
		  else if(emailSecurityProtocol.equalsIgnoreCase("ssl")){
			  this.mailSender.setProtocol("smtps");
			  this.mailSender.getJavaMailProperties().setProperty("mail.smtp.ssl.enable", "true");
			  this.mailSender.getJavaMailProperties().setProperty("mail.smtp.starttls.enable", "false");
		  }else{
			  this.mailSender.setProtocol("smtp");
			  this.mailSender.getJavaMailProperties().setProperty("mail.smtp.ssl.enable", "false");
			  this.mailSender.getJavaMailProperties().setProperty("mail.smtp.starttls.enable", "true");
		  }
		  this.mailSender.getJavaMailProperties().setProperty("mail.smtp.ssl.trust", host);
		  
	}
	
	private SystemCodeService systemCodeService;

	public void setSystemCodeService(SystemCodeService systemCodeService) {
		this.systemCodeService = systemCodeService;
	}
	
	
	
	
	
	
}
