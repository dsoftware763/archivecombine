package com.virtualcode.services.impl;

import java.util.ArrayList;
import java.util.List;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.PageRightsDAO;
import com.virtualcode.services.PageRightsService;
import com.virtualcode.vo.PageRights;

public class PageRightsServiceImpl implements PageRightsService{


	@Override
	public void savePageRights(PageRights transientInstance) {
		// TODO Auto-generated method stub
		pageRightsDAO.save(transientInstance);
	}

	@Override
	public void deletePageRights(PageRights persistentInstance) {
		// TODO Auto-generated method stub
		pageRightsDAO.delete(persistentInstance);
	}

	@Override
	public PageRights getPageRightsById(Integer id) {
		// TODO Auto-generated method stub
		return pageRightsDAO.findById(id);
	}

	@Override
	public ArrayList<PageRights> getPageRightsByExample(PageRights instance) {
		// TODO Auto-generated method stub
		return (ArrayList<PageRights>)pageRightsDAO.findByExample(instance);
	}

	@Override
	public ArrayList<PageRights> getPageRightsByProperty(String propertyName,
			Object value) {
		// TODO Auto-generated method stub
		return (ArrayList<PageRights>)pageRightsDAO.findByProperty(propertyName, value);
	}

	@Override
	public ArrayList<PageRights> getPageRightsByPageUri(Object pageUri) {
		// TODO Auto-generated method stub
		return (ArrayList<PageRights>)pageRightsDAO.findByPageUri(pageUri);
	}

	@Override
	public ArrayList<PageRights> getAll() {
		// TODO Auto-generated method stub
		return (ArrayList<PageRights>)pageRightsDAO.findAll();
	}
	
	private PageRightsDAO pageRightsDAO;
	private DAOManager daoManager;
	
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		pageRightsDAO = this.daoManager.getPageRightsDAO(); 
	}
}
