package com.virtualcode.services.impl;

import javax.jcr.Session;

import org.springmodules.jcr.JcrTemplate;

import com.virtualcode.repository.monitor.RepositoryMonitor;
import com.virtualcode.services.RepositoryStatusService;
import com.virtualcode.vo.RepositoryStatus;

public class RepositoryStatusServiceImpl implements RepositoryStatusService{
	
	
	private JcrTemplate jcrTemplate;
	private Session session;
	
	public RepositoryStatusServiceImpl(JcrTemplate jcrTemplate){
		this.jcrTemplate=jcrTemplate;		
		try{
			session=jcrTemplate.getSessionFactory().getSession();
		}catch(Exception ex){
			ex.printStackTrace();
		}		
	
	}
	
	public RepositoryStatus getRepositoryStatus(){
		
		 RepositoryMonitor repositoryMonitor = RepositoryMonitor.getInstance();
		 RepositoryStatus repositoryStatus = repositoryMonitor.getRepositoryStatus(session.getRepository());
		 
		 return repositoryStatus;
		 
		
	}
	
	
	

}
 