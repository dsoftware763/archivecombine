package com.virtualcode.services.impl;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.JobStatisticsDAO;
import com.virtualcode.services.JobStatisticsService;
import com.virtualcode.vo.JobStatistics;

/**
 * A data access object (DAO) providing persistence and search support for
 * JobStatistics entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.vo.JobStatistics
 * @author MyEclipse Persistence Tools
 */

public class JobStatisticsServiceImpl implements JobStatisticsService{

	@Override
	public void saveJobStatistics(JobStatistics transientInstance) {
		// TODO Auto-generated method stub
		jobStatisticsDAO.save(transientInstance);
	}
	
	@Override
	public void updateJobStatistics(JobStatistics transientInstance) {
		// TODO Auto-generated method stub
		jobStatisticsDAO.update(transientInstance);
	}
	
	
	@Override
	public void deleteJobStatistics(JobStatistics persistentInstance) {
		// TODO Auto-generated method stub
		jobStatisticsDAO.delete(persistentInstance);
	}
	@Override
	public JobStatistics getJobStatisticsById(Integer id) {
		// TODO Auto-generated method stub
		return jobStatisticsDAO.findById(id);
	}
	@Override
	public List<JobStatistics> getJobStatisticsByExample(JobStatistics instance) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<JobStatistics> getJobStatisticsByProperty(String propertyName,
			Object value) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<JobStatistics> getJobStatisticsByExecutionId(Object executionId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<JobStatistics> getJobStatisticsBySkippedProcessfileSkippedHiddenlected(
			Object skippedProcessfileSkippedHiddenlected) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<JobStatistics> getJobStatisticsBySkippedProcessfileSkippedIsdir(
			Object skippedProcessfileSkippedIsdir) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<JobStatistics> getJobStatisticsBySkippedProcessfileSkippedMismatchAge(
			Object skippedProcessfileSkippedMismatchAge) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<JobStatistics> getJobStatisticsBySkippedProcessfileSkippedMismatchExtension(
			Object skippedProcessfileSkippedMismatchExtension) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<JobStatistics> getJobStatisticsBySkippedProcessfileSkippedMismatchLastaccesstime(
			Object skippedProcessfileSkippedMismatchLastaccesstime) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<JobStatistics> getJobStatisticsBySkippedProcessfileSkippedMismatchLastmodifiedtime(
			Object skippedProcessfileSkippedMismatchLastmodifiedtime) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<JobStatistics> getJobStatisticsBySkippedProcessfileSkippedReadOnly(
			Object skippedProcessfileSkippedReadOnly) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<JobStatistics> getJobStatisticsBySkippedProcessfileSkippedSymbollink(
			Object skippedProcessfileSkippedSymbollink) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<JobStatistics> getJobStatisticsBySkippedProcessfileSkippedTemporary(
			Object skippedProcessfileSkippedTemporary) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<JobStatistics> getJobStatisticsBySkippedProcessfileSkippedTooLarge(
			Object skippedProcessfileSkippedTooLarge) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<JobStatistics> getJobStatisticsByTotalArchived(
			Object totalArchived) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<JobStatistics> getJobStatisticsByTotalDuplicated(
			Object totalDuplicated) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<JobStatistics> getJobStatisticsByTotalEvaluated(
			Object totalEvaluated) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<JobStatistics> getJobStatisticsByTotalFailedArchiving(
			Object totalFailedArchiving) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<JobStatistics> getJobStatisticsByTotalFailedDeduplication(
			Object totalFailedDeduplication) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<JobStatistics> getJobStatisticsByTotalFailedEvaluating(
			Object totalFailedEvaluating) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<JobStatistics> getJobStatisticsByTotalFailedStubbing(
			Object totalFailedStubbing) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<JobStatistics> getJobStatisticsByTotalStubbed(
			Object totalStubbed) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<JobStatistics> getAll() {
		// TODO Auto-generated method stub
		return (List<JobStatistics>)jobStatisticsDAO.findAll();
	}
	@Override
	public List<JobStatistics> getJobStatisticsByJob(Object jobId) {
		// TODO Auto-generated method stub
		return (List<JobStatistics>)jobStatisticsDAO.findByJob(jobId);
	}
	
	private JobStatisticsDAO jobStatisticsDAO;
	private DAOManager daoManager;
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.jobStatisticsDAO = this.daoManager.getJobStatisticsDAO();
	}

}