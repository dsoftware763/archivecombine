package com.virtualcode.services.impl;

import java.io.IOException;
import java.util.List;

import jespa.security.SecurityProviderException;

import com.virtualcode.services.UserPreferencesService;
import com.virtualcode.util.JespaUtil;

public class UserPreferencesServiceImpl implements UserPreferencesService {

	@Override
	public List<String> getGroupsForUser(String username) {
		// TODO Auto-generated method stub
		JespaUtil jespaUtil = JespaUtil.getInstance();
		try {
			List<String> userGroups = jespaUtil.getGroupsForUser(username);
			return userGroups;
		} catch (SecurityProviderException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
