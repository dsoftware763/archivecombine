package com.virtualcode.services.impl;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.RepoStatsDAO;
import com.virtualcode.services.RepoStatsService;
import com.virtualcode.vo.RepoStats;

/**
 * A data access object (DAO) providing persistence and search support for
 * RepoStats entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.virtualcode.vo.RepoStats
 * @author MyEclipse Persistence Tools
 */

public class RepoStatsServiceImpl implements RepoStatsService{

	@Override
	public void saveRepoStats(RepoStats repoStatus) {
		// TODO Auto-generated method stub
		repoStatsDAO.save(repoStatus);
	}

	@Override
	public void deleteRepoStats(RepoStats repoStatus) {
		// TODO Auto-generated method stub
		repoStatsDAO.delete(repoStatus);
	}

	@Override
	public RepoStats getRepoStatusById(Integer id) {
		// TODO Auto-generated method stub
		return repoStatsDAO.findById(id);
	}

	@Override
	public ArrayList<RepoStats> getRepoStatusByExample(RepoStats repoStatus) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<RepoStats> getRepoStatusByProperty(String propertyName,
			Object value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<RepoStats> getRepoStatusByRepoPath(Object repoPath) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<RepoStats> getRepoStatusByTotalFolders(Object totalFolders) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<RepoStats> getRepoStatusByTotalFiles(Object totalFiles) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<RepoStats> getRepoStatusByFsDocs(Object fsDocs) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<RepoStats> getRepoStatusBySpDocs(Object spDocs) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<RepoStats> getRepoStatusByUsedSpace(Object usedSpace) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<RepoStats> getRepoStatusByDuplicateDataSize(
			Object duplicateDataSize) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<RepoStats> getAll() {
		// TODO Auto-generated method stub
		List list = repoStatsDAO.findAll();
		return (ArrayList<RepoStats>)repoStatsDAO.findAll();
	}
	
	@Override
	public RepoStats getLatestStats() {
		// TODO Auto-generated method stub
		ArrayList<RepoStats> list = getAll();
		if(list !=null && list.size()>0){
			return list.get(list.size()-1);
		}
		return null;
	}
	
	private RepoStatsDAO repoStatsDAO;
	private DAOManager daoManager;
	
	
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.repoStatsDAO = this.daoManager.getRepoStatsDAO();
	}

}