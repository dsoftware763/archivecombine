package com.virtualcode.services.impl;

import java.util.List;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.DelPolicyDAO;
import com.virtualcode.services.DelPolicyService;
import com.virtualcode.vo.DelPolicy;

public class DelPolicyServiceImpl implements DelPolicyService {


    private DelPolicyDAO delPolicyDAO;
    private DAOManager daoManager;	
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.delPolicyDAO=daoManager.getDelPolicyDAO();
	}
	
	@Override
	public void save(DelPolicy transientInstance) {
		// TODO Auto-generated  method stub
		delPolicyDAO.save(transientInstance);
	}

	@Override
	public void delete(DelPolicy persistentInstance) {
		// TODO Auto-generated method stub
		delPolicyDAO.delete(persistentInstance);
	}

	@Override
	public DelPolicy getById(Integer id) {
		// TODO Auto-generated method stub
		return delPolicyDAO.findById(id);
	}

	@Override
	public List getByExample(DelPolicy instance) {
		// TODO Auto-generated method stub
		return delPolicyDAO.findByExample(instance);
	}

	@Override
	public List getByProperty(String propertyName, Object value) {
		// TODO Auto-generated method stub
		return delPolicyDAO.findByProperty(propertyName, value);
	}

	@Override
	public List getByName(Object name) {
		// TODO Auto-generated method stub
		return delPolicyDAO.findByName(name);
	}

	@Override
	public List getByModifiedDate(Object modifiedDate) {
		// TODO Auto-generated method stub
		return delPolicyDAO.findByModifiedDate(modifiedDate);
	}

	@Override
	public List getByAccessDate(Object accessDate) {
		// TODO Auto-generated method stub
		return delPolicyDAO.findByAccessDate(accessDate);
	}

	@Override
	public List getByArchivalDate(Object archivalDate) {
		// TODO Auto-generated method stub
		return delPolicyDAO.findByArchivalDate(archivalDate);
	}

	@Override
	public List getByFileType(Object fileType) {
		// TODO Auto-generated method stub
		return delPolicyDAO.findByFileType(fileType);
	}

	@Override
	public List getByFileSize(Object fileSize) {
		// TODO Auto-generated method stub
		return delPolicyDAO.findByFileSize(fileSize);
	}

	@Override
	public List getAll() {
		// TODO Auto-generated method stub
		return delPolicyDAO.findAll();
	}

	@Override
	public DelPolicy merge(DelPolicy detachedInstance) {
		// TODO Auto-generated method stub
		return delPolicyDAO.merge(detachedInstance);
	}

	@Override
	public void attachDirty(DelPolicy instance) {
		// TODO Auto-generated method stub
		delPolicyDAO.attachDirty(instance);
	}

	@Override
	public void attachClean(DelPolicy instance) {
		// TODO Auto-generated method stub
		delPolicyDAO.attachClean(instance);
	}

}
