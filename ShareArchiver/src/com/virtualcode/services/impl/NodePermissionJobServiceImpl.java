package com.virtualcode.services.impl;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.NodePermissionJobDAO;
import com.virtualcode.services.NodePermissionJobService;
import com.virtualcode.vo.NodePermissionJob;


public class NodePermissionJobServiceImpl implements NodePermissionJobService{

	@Override
	public void saveNodePermissionJob(NodePermissionJob transientInstance) {
		// TODO Auto-generated method stub
		nodePermissionJobDAO.save(transientInstance);
	}

	@Override
	public void deleteNodePermissionJob(NodePermissionJob persistentInstance) {
		// TODO Auto-generated method stub
		nodePermissionJobDAO.delete(persistentInstance);
	}

	@Override
	public NodePermissionJob getNodePermissionJobById(Integer id) {
		// TODO Auto-generated method stub
		return nodePermissionJobDAO.findById(id);
	}

	@Override
	public List<NodePermissionJob> getNodePermissionJobByExample(
			NodePermissionJob instance) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<NodePermissionJob> getNodePermissionJobByProperty(
			String propertyName, Object value) {
		// TODO Auto-generated method stub
		return (List<NodePermissionJob>)nodePermissionJobDAO.findByProperty(propertyName, value);
	}

	@Override
	public List<NodePermissionJob> getNodePermissionJobByName(Object name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<NodePermissionJob> getNodePermissionJobByCommand(Object command) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<NodePermissionJob> getNodePermissionJobByStatus(Object status) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<NodePermissionJob> getNodePermissionJobByRegisterdate(
			Object registerdate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<NodePermissionJob> getNodePermissionJobByStartdate(
			Object startdate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<NodePermissionJob> getNodePermissionJobByFinisheddate(
			Object finisheddate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<NodePermissionJob> getNodePermissionJobBySecureinfo(
			Object secureinfo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<NodePermissionJob> getNodePermissionJobBySids(Object sids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<NodePermissionJob> getNodePermissionJobByDenysids(
			Object denysids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<NodePermissionJob> getNodePermissionJobBySharesids(
			Object sharesids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<NodePermissionJob> getNodePermissionJobByDenysharesids(
			Object denysharesids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<NodePermissionJob> getNodePermissionJobByIsrecursive(
			Object isrecursive) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<NodePermissionJob> getAll() {
		// TODO Auto-generated method stub
		return (List<NodePermissionJob>)nodePermissionJobDAO.findAll();
	}
	
	private NodePermissionJobDAO nodePermissionJobDAO;
	private DAOManager daoManager;
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.nodePermissionJobDAO = this.daoManager.getNodePermissionJobDAO();
	}
	
	
	

}