package com.virtualcode.services.impl;

import java.util.List;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.RoleFeatureDAO;
import com.virtualcode.services.RoleFeatureService;
import com.virtualcode.vo.RoleFeature;

public class RoleFeatureServiceImpl implements RoleFeatureService{

	@Override
	public void save(RoleFeature transientInstance) {
		roleFeatureDAO.save(transientInstance);
	}

	@Override
	public RoleFeature findById(Integer id) {
		return roleFeatureDAO.findById(id);
	}
	
	@Override
	public  List<RoleFeature> getByProperty(String propertyName, Object value) {
		return roleFeatureDAO.findByProperty(propertyName, value);
	}

	@Override
	public  List<RoleFeature> getByName(Object name) {
		return roleFeatureDAO.findByName(name);
	}

	@Override
	public  List<RoleFeature> getByAdmin(Object admin) {
		return roleFeatureDAO.findByAdmin(admin);
	}

	@Override
	public  List<RoleFeature> getByPriv(Object priv) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public  List<RoleFeature> getByLdap(Object ldap) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public  List<RoleFeature> getAll() {
		return roleFeatureDAO.findAll();
	}

	@Override
	public RoleFeature merge(RoleFeature detachedInstance) {
		return roleFeatureDAO.merge(detachedInstance);
	}

	@Override
	public void attachDirty(RoleFeature instance) {
		roleFeatureDAO.attachDirty(instance);
		
	}

	@Override
	public void attachClean(RoleFeature instance) {
		roleFeatureDAO.attachClean(instance);
		
	}
	
	private RoleFeatureDAO roleFeatureDAO;
	private DAOManager daoManager;
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		roleFeatureDAO=daoManager.getRoleFeatureDAO();
	}
	
    
}
