package com.virtualcode.services.impl;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.FilesProcessedSizeDAO;
import com.virtualcode.services.FilesProcessedSizeService;
import com.virtualcode.vo.FilesProcessedSize;

/**
 * A data access object (DAO) providing persistence and search support for
 * FilesProcessedSize entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.vo.FilesProcessedSize
 * @author MyEclipse Persistence Tools
 */

public class FilesProcessedSizeServiceImpl implements FilesProcessedSizeService{

	@Override
	public void saveFileProcessedSize(FilesProcessedSize transientInstance) {
		// TODO Auto-generated method stub
		filesProcessedSizeDAO.save(transientInstance);
	}

	@Override
	public void deleteFileProcessedSize(FilesProcessedSize persistentInstance) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public FilesProcessedSize getById(Integer id) {
		// TODO Auto-generated method stub
		return filesProcessedSizeDAO.findById(id);
	}

	@Override
	public List<FilesProcessedSize> getByProperty(String propertyName, Object value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<FilesProcessedSize> getByTotalProcessedFiles(Object totalProcessedFiles) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<FilesProcessedSize> getAll() {
		// TODO Auto-generated method stub
		return (List<FilesProcessedSize>)filesProcessedSizeDAO.findAll();
	}

	private FilesProcessedSizeDAO filesProcessedSizeDAO;
	private DAOManager daoManager;
	
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		filesProcessedSizeDAO = this.daoManager.getFilesProcessedSizeDAO();
	}
	
	

}