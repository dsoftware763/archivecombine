package com.virtualcode.services.impl;


import java.util.List;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.RoleDAO;
import com.virtualcode.services.RoleService;
import com.virtualcode.vo.Role;

public class RoleServiceImpl  implements RoleService {

	@Override
	public void save(Role transientInstance) {
		roleDAO.save(transientInstance);
		
	}

	@Override
	public void delete(Role persistentInstance) {
		roleDAO.delete(persistentInstance);
		
	}

	@Override
	public Role getById(Long id) {
		return roleDAO.findById(id);
	}

	@Override
	public List getAll() {
		return roleDAO.findAll();
	}
	
	
	
	
	       
	private RoleDAO  roleDAO;
	private DAOManager daoManager;
    public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.roleDAO=daoManager.getRoleDAO();
	}
	
	
}