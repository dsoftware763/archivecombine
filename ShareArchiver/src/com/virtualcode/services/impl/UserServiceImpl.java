package com.virtualcode.services.impl;
// default package

import java.util.List;
import java.util.Set;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.UserDAO;
import com.virtualcode.services.UserService;
import com.virtualcode.vo.User;


public class UserServiceImpl implements UserService  {

	@Override
	public void save(User transientInstance) {
		userDAO.save(transientInstance);
		
	}

	@Override
	public void delete(User persistentInstance) {
		userDAO.delete(persistentInstance);
		
	}

	@Override
	public User getById(Long id) {
		return userDAO.findById(id);
	}

	@Override
	public List getByEmail(Object email) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List getByFirstName(Object firstName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List getByLastName(Object lastName) {
		// TODO Auto-generated method stub
		
		return null;
	}

	@Override
	public List getByLockStatus(Object lockStatus) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List getByUsername(Object username) {
		// TODO Auto-generated method stub
		List userList = userDAO.findByUsername(username);
		if(userList!=null && userList.size()>0){
			return userList;
		}
		
		return null;
	}
	
	public User login(String username,String password){
		return userDAO.findUser(username,password);
		
	}

	@Override
	public List<User> getAll() {
		return userDAO.findAll();
	}
	
	private UserDAO userDAO;
	
	private DAOManager daoManager;

	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.userDAO=daoManager.getUserDAO();
	}
	
	   
  
	
}