package com.virtualcode.services.impl;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.DataGuardianDAO;
import com.virtualcode.services.DataGuardianService;
import com.virtualcode.vo.DataGuardian;

/**
 * A data access object (DAO) providing persistence and search support for
 * DataGuardian entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.vo.DataGuardian
 * @author MyEclipse Persistence Tools
 */

public class DataGuardianServiceImpl implements DataGuardianService{

	@Override
	public void saveDataGuardian(DataGuardian transientInstance) {
		// TODO Auto-generated method stub
		dataGuardianDAO.save(transientInstance);
	}

	@Override
	public void deleteDataGuardian(DataGuardian persistentInstance) {
		// TODO Auto-generated method stub
		dataGuardianDAO.delete(persistentInstance);
	}

	@Override
	public DataGuardian getDataGuardianById(Integer id) {
		// TODO Auto-generated method stub
		return dataGuardianDAO.findById(id);
	}

	@Override
	public List<DataGuardian> getDataGuardianByExample(DataGuardian instance) {
		// TODO Auto-generated method stub
		return dataGuardianDAO.findByExample(instance);
	}

	@Override
	public List<DataGuardian> getDataGuardianByProperty(String propertyName,
			Object value) {
		// TODO Auto-generated method stub
		return dataGuardianDAO.findByProperty(propertyName, value);
	}

	@Override
	public List<DataGuardian> getDataGuardianByDataGuardianEmail(
			Object dataGuardianEmail) {
		// TODO Auto-generated method stub
		return dataGuardianDAO.findByDataGuardianEmail(dataGuardianEmail);
	}

	@Override
	public List<DataGuardian> getAll() {
		// TODO Auto-generated method stub
		return dataGuardianDAO.findAll();
	}
	
	private DAOManager daoManager;
	private DataGuardianDAO dataGuardianDAO;
	
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.dataGuardianDAO = this.daoManager.getDataGuardianDAO();
	}
	
	
	
		
}