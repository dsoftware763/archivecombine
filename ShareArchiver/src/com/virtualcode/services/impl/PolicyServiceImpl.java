package com.virtualcode.services.impl;

import java.util.List;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.JobDAO;
import com.virtualcode.dao.PolicyDAO;
import com.virtualcode.services.PolicyService;
import com.virtualcode.vo.Policy;

public class PolicyServiceImpl implements PolicyService {

	@Override
	public void savePolicy(Policy policy) {
		policyDAO.save(policy);
		
	}

	@Override
	public List<Policy> getAllPolicies() {
		return policyDAO.findAll();
	}

	@Override
	public Policy getPolicyById(Integer policyId) {
		return policyDAO.findById(policyId);
	}
	
	public List getPolicyByName(Object name){
		List policyList = policyDAO.findByName(name);
		if(policyList!=null && policyList.size()>0){
			return policyList;
		}
		
		return null;
	}

	@Override
	public void updatePolicy(Policy policy) {
		policyDAO.save(policy);
	}

	@Override
	public void deletePolicy(Policy policy) {
		System.out.println("--finding policy -- by job id ");
		System.out.println(jobDAO.findByProperty("policy.id", policy.getId()));
		policyDAO.delete(policy);
	}
	
    private PolicyDAO policyDAO;
    private JobDAO jobDAO;
	
    private DAOManager daoManager;	
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.policyDAO=daoManager.getPolicyDAO();
		this.jobDAO=daoManager.getJobDAO();
	}
	

}
