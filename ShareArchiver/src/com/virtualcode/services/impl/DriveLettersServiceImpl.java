package com.virtualcode.services.impl;

import java.util.List;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.DriveLettersDAO;
import com.virtualcode.services.DriveLettersService;
import com.virtualcode.vo.DriveLetters;

public class DriveLettersServiceImpl implements DriveLettersService{

	@Override
	public void saveDriveLetter(DriveLetters transientInstance) {
		driveLettersDAO.save(transientInstance);
		
	}

	@Override
	public void deleteDriveLetter(DriveLetters persistentInstance) {
		driveLettersDAO.delete(persistentInstance);
		
	}

	@Override
	public DriveLetters getDriveLetterById(Integer id) {
		return driveLettersDAO.findById(id);
	}

	@Override
	public List<DriveLetters> getDriveLettersByDriveLetter(Object driveLetter) {
		return driveLettersDAO.findByDriveLetter(driveLetter);
	}

	@Override
	public List<DriveLetters> getDriveLettersBySharePath(Object sharePath) {
		return driveLettersDAO.findBySharePath(sharePath);
	}

	@Override
	public List<DriveLetters> getDriveLettersByStatus(Object status) {
		return driveLettersDAO.findByStatus(status);
	}

	@Override
	public List<DriveLetters> getAll() {
		return driveLettersDAO.findAll();
	}
	
	private DriveLettersDAO driveLettersDAO;
	private DAOManager daoManager;
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.driveLettersDAO=daoManager.getDriveLettersDAO();
	}
	

}
