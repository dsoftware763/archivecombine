package com.virtualcode.services.impl;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.common.SecureURL;
import com.virtualcode.repository.FileExplorerService;
import com.virtualcode.repository.monitor.RepositoryMonitor;
import com.virtualcode.schedular.PLVStatus;
import com.virtualcode.services.AgentManagementService;
import com.virtualcode.services.ErrorCodeService;
import com.virtualcode.services.JobService;
import com.virtualcode.services.JobStatisticsService;
import com.virtualcode.services.JobStatusService;
import com.virtualcode.services.MailService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.services.SystemCodeService;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.SpringApplicationContext;
import com.virtualcode.vo.Agent;
import com.virtualcode.vo.DocumentCategory;
import com.virtualcode.vo.DocumentType;
import com.virtualcode.vo.ErrorCode;
import com.virtualcode.vo.HotStatistics;
import com.virtualcode.vo.Job;
import com.virtualcode.vo.JobSpDocLib;
import com.virtualcode.vo.JobSpDocLibExcludedPath;
import com.virtualcode.vo.JobStatistics;
import com.virtualcode.vo.JobStatus;
import com.virtualcode.vo.Policy;
import com.virtualcode.vo.RepositoryStatus;
import com.virtualcode.vo.SystemCode;
import com.virtualcode.vo.SystemCodeType;
                                             

public class AgentManagementServiceImpl implements AgentManagementService,Serializable{

       private static boolean transactional = true;
       
       private boolean enableStatusEmail=false;

	    // actionType values on GSP pages
	    private String  EVALUATING  =   "EVALUATING";
	    private String  RUNNING     =   "RUNNING";
	    private String  COMPLETED   =   "COMPLETED";

	    // actionType values in DB
	    private String  ARCHIVE_DB     =   "ARCHIVE";
	    private String  INDEXING_DB     =   "INDEXING";
		private String  ARCHIVE_AND_STUB_DB  =   "STUB";
		private String  ARCHIVE_WITHOUT_STUB_DB  =   "WITHOUTSTUB";
		private String  EVALUATE_ONLY_DB    =   "EVALUATE";
		private String  EXPORT_DB       =   "EXPORT";
		
		//private WebServiceContext context;
		
		public AgentManagementServiceImpl() {
			// TODO Auto-generated constructor stub
			//serviceManager = (com.virtualcode.services.ServiceManager)SpringApplicationContext.getBean("serviceManager");
			jobService = (com.virtualcode.services.JobService)SpringApplicationContext.getBean("jobService");
			errorCodeService = (com.virtualcode.services.ErrorCodeService)SpringApplicationContext.getBean("errorCodeService");
			jobStatisticsService = (com.virtualcode.services.JobStatisticsService)SpringApplicationContext.getBean("jobStatisticsService");
			jobStatusService = (com.virtualcode.services.JobStatusService)SpringApplicationContext.getBean("jobStatusService");
			mailService=(com.virtualcode.services.MailService)SpringApplicationContext.getBean("mailService");
			systemCodeService = (com.virtualcode.services.SystemCodeService)SpringApplicationContext.getBean("systemCodeService");
			
			
			//proactiveLibService = (com.virtualcode.services.ProactiveLibService)SpringApplicationContext.getBean("proactiveLibService");
			
		}
		
	    public  byte[] getJobListForProcessing(String agentName,String actionType) {
	    	
	    	 //ServletContext sc = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
	         // jobService=getJobService(sc);
	    	if(Agent.agentLastCallMap==null)
	    		Agent.agentLastCallMap = new HashMap<String, Long>();
	    	
	    	Agent.agentLastCallMap.put(agentName, System.currentTimeMillis());
	    	
	    	 PLVStatus plvStatus=PLVStatus.getInstance();
	    	 if(!plvStatus.isValid()){//if no valid license,don't pick jobs.
	    		 return null;
	    	 }
	    	 
	    	 RepositoryMonitor repositoryMonitor = RepositoryMonitor.getInstance();
			 RepositoryStatus repositoryStatus = repositoryMonitor.getRepositoryStatus(null);
			 
			 ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
			 
			 Long criticalLimit = 95l;
			 
			 if(ru.getCodeValue("server_critical_limit", SystemCodeType.GENERAL)!=null){
				 criticalLimit = new Long(ru.getCodeValue("server_critical_limit", SystemCodeType.GENERAL));
			 }
			 
			 Long warningLimit = 90l;
			 
			 if(ru.getCodeValue("server_warning_limit", SystemCodeType.GENERAL)!=null){
				 warningLimit = new Long(ru.getCodeValue("server_warning_limit", SystemCodeType.GENERAL));
			 }
			 
			 if(repositoryStatus.getTotalFreeSpaceOnDisk()>=criticalLimit && (!actionType.equalsIgnoreCase(EXPORT_DB) || !actionType.equalsIgnoreCase("RESTORE"))){
				 return null;
			 }
			 
			 if((repositoryStatus.getTotalFreeSpaceOnDisk()>=warningLimit)
					 && (actionType.equalsIgnoreCase(ARCHIVE_DB) || actionType.equalsIgnoreCase(ARCHIVE_AND_STUB_DB) || actionType.equalsIgnoreCase(ARCHIVE_WITHOUT_STUB_DB))){
				 return null;
			 }
			 
	    	
	    	 List<Job> jobsList;
	    	 System.out.println("getting jobs for agent: " + agentName + "and actionType: "+actionType);
	    	 
	    	 if(actionType.equalsIgnoreCase("EXPORT")){
	    	     jobsList=jobService.getAgentExportJobs(agentName);
	    	     
	    	 } else if(actionType.equalsIgnoreCase("INDEXING")) {
	    		 jobsList=jobService.getAgentIndexingJobs(agentName);
	    		 
	    	 } else if(actionType.equalsIgnoreCase("ACTIVEARCH")){
	    		 jobsList = jobService.getAgentActiveArchivingJobs(agentName);	 
	    		 
	    	 } else if(actionType.equalsIgnoreCase("PRINTARCH")){
	    		 jobsList = jobService.getAgentPrintArchivingJobs(agentName);
	    		 
	    	 } else if(actionType.equalsIgnoreCase("RESTORE")){
	    		 jobsList = jobService.getAgentRestoreJobs(agentName);
	    		 
	    	 } else if(actionType.equalsIgnoreCase("RETENSION")){
	    		 jobsList = jobService.getAgentRetensionJobs(agentName);
	    		 
	    	 } else {
	    		 jobsList=jobService.getAgentImportJobs(agentName); 
	    	 }
	    	
	    	if(jobsList==null || jobsList.size()==0){
	    		return null;
	    	}
	    	List<com.virtualcode.agent.das.archivePolicy.dto.Job>                jobDTOList              =   new ArrayList<com.virtualcode.agent.das.archivePolicy.dto.Job>();
	   
	    	com.virtualcode.agent.das.archivePolicy.dto.Job                      jobDTO                  =   new com.virtualcode.agent.das.archivePolicy.dto.Job();
	    	com.virtualcode.agent.das.archivePolicy.dto.Policy                   policyDTO               =   new com.virtualcode.agent.das.archivePolicy.dto.Policy();
	    	com.virtualcode.agent.das.archivePolicy.dto.SPDocLib                 spDocLibDTO             =   new com.virtualcode.agent.das.archivePolicy.dto.SPDocLib();
	        com.virtualcode.agent.das.archivePolicy.dto.ExcludedPath             excludedPathDTO         =   new com.virtualcode.agent.das.archivePolicy.dto.ExcludedPath();
	        com.virtualcode.agent.das.archivePolicy.dto.JobStatistics            jobStatisticsDTO        =   new com.virtualcode.agent.das.archivePolicy.dto.JobStatistics();
	        com.virtualcode.agent.das.documentManagement.dto.DocumentType        documentTypeDTO         =   new  com.virtualcode.agent.das.documentManagement.dto.DocumentType();
            
            Job job;
            Agent agent;
            Policy policy;
           
            List<Job> resultsList=new ArrayList<Job>();
            for(int i=0;i<jobsList.size();i++){
            	
            	job=jobsList.get(i);            	
            	policy=job.getPolicy();
            	if(policy!=null){
            		System.out.println("Policy name "+policy.getName());
            		Iterator<DocumentType> ite=policy.getPolicyDocumentTypes().iterator();
            		
	   				 DocumentType selectedDocumentType;	   					   				
	   				 System.out.println("Setting selected document types");
	   				 Set<DocumentType> docTypesSet=new HashSet<DocumentType>();
	   				 
	   				 while(ite!=null && ite.hasNext()){
	   					selectedDocumentType=(DocumentType)ite.next();
	   					DocumentCategory docCategory=selectedDocumentType.getDocumentCategory();
	   					selectedDocumentType.setDocumentCategory(docCategory);
	   					docTypesSet.add(selectedDocumentType);	
	   				 }
	   					
	   				 job.setJobDocuments(docTypesSet);
	   				
            	}
            	
            	agent=job.getAgent();
            	System.out.println("Agent name "+agent.getName());            	          	
            	
            	Iterator<JobSpDocLib> docLibsIterator=job.getJobSpDocLibs().iterator();
        		Iterator<JobSpDocLibExcludedPath> exPathIterator;
        		
        		Set<JobSpDocLib> jobSpDocLibSet=new HashSet<JobSpDocLib>();
        		
        		while(docLibsIterator!=null && docLibsIterator.hasNext()){
        			JobSpDocLib jobSpDocLib=(JobSpDocLib)docLibsIterator.next();
        			exPathIterator=jobSpDocLib.getJobSpDocLibExcludedPaths().iterator();
        			Set<JobSpDocLibExcludedPath> exPathSet=new HashSet<JobSpDocLibExcludedPath>();
        			while(exPathIterator!=null && exPathIterator.hasNext()){				
        				JobSpDocLibExcludedPath exPath=(JobSpDocLibExcludedPath)exPathIterator.next();
        				exPathSet.add(exPath);
        			}
        			jobSpDocLib.setJobSpDocLibExcludedPaths(exPathSet);
        			jobSpDocLibSet.add(jobSpDocLib);
        		}
        		
        		job.setJobSpDocLibs(jobSpDocLibSet);        		
            	
            	resultsList.add(job);
            }
            
            for(Job jobInstance:resultsList){
            	//init
                Set<com.virtualcode.agent.das.archivePolicy.dto.SPDocLib>            spDocLibDTOSet          =   new HashSet<com.virtualcode.agent.das.archivePolicy.dto.SPDocLib>(0);
                Set<com.virtualcode.agent.das.archivePolicy.dto.JobStatistics>       jobStatisticsDTOSet     =   new HashSet<com.virtualcode.agent.das.archivePolicy.dto.JobStatistics>(0);
                Set<com.virtualcode.agent.das.documentManagement.dto.DocumentType>   documentTypeDTOSet      =   new HashSet<com.virtualcode.agent.das.documentManagement.dto.DocumentType>(0);
                jobDTO = getMappedJobDTO(jobInstance);
                policyDTO = getMappedPolicyDTO(jobInstance.getPolicy());
                
                //set documentTypes for policy
                for(DocumentType documentTypeInstance : (Set<DocumentType>)jobInstance.getPolicy().getPolicyDocumentTypes()){
                    documentTypeDTO =   getMappedDocumentTypeDTO(documentTypeInstance);
                    documentTypeDTOSet.add(documentTypeDTO);
                }
                policyDTO.setDocumentTypeSet(documentTypeDTOSet);

                //set policy for job
                jobDTO.setPolicy(policyDTO);
                

                //set spDocLibs for job
                for(JobSpDocLib spDocLibInstance : (Set<JobSpDocLib>)jobInstance.getJobSpDocLibs()){
                    Set<com.virtualcode.agent.das.archivePolicy.dto.ExcludedPath>        excludedPathDTOSet      =   new HashSet<com.virtualcode.agent.das.archivePolicy.dto.ExcludedPath>(0);
                    //System.out.println( "lib name: " + spDocLibInstance.getName());
                    spDocLibDTO =   getMappedSPDocLibDTO(spDocLibInstance);

                    //set excludedPaths for spDocLib
                    for(JobSpDocLibExcludedPath excludedPathInstance : (Set<JobSpDocLibExcludedPath>)spDocLibInstance.getJobSpDocLibExcludedPaths()){
                        excludedPathDTO = getMappedExcludedPathDTO(excludedPathInstance);
                        excludedPathDTOSet.add(excludedPathDTO);
                    }
                    spDocLibDTO.setExcludedPathSet(excludedPathDTOSet);
                    spDocLibDTOSet.add(spDocLibDTO);
                    //System.out.println("----------------");
                }

                jobDTO.setSpDocLibSet(spDocLibDTOSet);
                jobDTOList.add(jobDTO);
            }
            
            byte[] jobDTOBytesArray = null;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            try {
                ObjectOutputStream oos = new ObjectOutputStream(bos);
                //oos.writeObject(resultsList);
                oos.writeObject(jobDTOList);
                oos.flush();
                oos.close();
                bos.close();
                jobDTOBytesArray = bos.toByteArray ();
                //            println "conversion completed"
            }catch(Exception e){
                System.out.println( "********************** EXCEPTION **************************");
                System.out.println("Exception while converting jobList to byte[]");
                System.out.println("***********************************************************");
                return null;
            }
            //        println "process successfully completed"
            return jobDTOBytesArray; 
         	    	
	    }
	    

//	    public  int setStatusForScheduledJobs(String agentName) {
	   	public  int setStatusForScheduledJobs() {
	    	
	    	int counter	=	0;
//	    	System.out.println("set ready_to_Exec for scheduled jobs of Agent: " + agentName);
	    	System.out.println("set ready_to_Exec for scheduled jobs");
	    	 RepositoryMonitor repositoryMonitor = RepositoryMonitor.getInstance();
			 RepositoryStatus repositoryStatus = repositoryMonitor.getRepositoryStatus(null);
			 ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
			 
			 Long criticalLimit = 95l;
			 Long warningLimit = 90l;

			 if(ru.getCodeValue("server_warning_limit", SystemCodeType.GENERAL)!=null){
				 warningLimit = new Long(ru.getCodeValue("server_warning_limit", SystemCodeType.GENERAL));
			 }
			 
			 if(ru.getCodeValue("server_critical_limit", SystemCodeType.GENERAL)!=null){
				 criticalLimit = new Long(ru.getCodeValue("server_critical_limit", SystemCodeType.GENERAL));
			 }
			 
			 if(repositoryStatus.getTotalFreeSpaceOnDisk()>=warningLimit){
				 return 0;	// stop further scheduling of jobs
			 }

//	    	List<Job> jobsList	=	jobService.getAgentScheduledJobs(agentName);
	    	List<Job> jobsList	=	jobService.getAllScheduledJobs();
			if(jobsList!=null) {				
				Job job;
				for(int i=0;i<jobsList.size();i++){
					job=jobsList.get(i);
					
					if(job.getIsScheduled()) {
						
						long initTime	=	(job.getExecTimeStart()!=null)? job.getExecTimeStart().getTime() : System.currentTimeMillis();
						long curTime	=	System.currentTimeMillis();
						
						if(curTime >= initTime) {//if time reaches to start the job...
							
							long reRunDelay	=	(job.getExecutionInterval()!=null)?job.getExecutionInterval().longValue() : 1 ;//get and convert into long
							initTime	=	initTime + (reRunDelay*24*60*60*1000);//also convert the days into Millis
							
							//reset the next startup time of current Job
							job.setExecTimeStart(new Timestamp(initTime));
							
							//Set the flag to run-now
							job.setReadyToExecute(true);
							
							System.out.println("seting job schedule");
							
							if(job.getExecutionInterval()==0)
								job.setIsScheduled(false);
							
							System.out.println("value: " + job.getIsScheduled());
							
							//save the modified values in DB for this job
							jobService.saveJob(job);
							System.out.println("ScheduledJobID:  " + job.getId() + " is ready to execute now");
							counter++;
						}
					}
				}
			} else {
				System.out.println("scheduled jobs not found");
			}
			return counter;    	
	    }
	    
	    public  List<com.virtualcode.agent.das.archivePolicy.dto.Job> getJobListObjects(String agentName, String actionType) {
	    	
	    	 //ServletContext sc = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
	         
	    	// jobService=getJobService(sc);
	    	
	    	if(Agent.agentLastCallMap==null)
	    		Agent.agentLastCallMap = new HashMap<String, Long>();
	    	
	    	Agent.agentLastCallMap.put(agentName, System.currentTimeMillis());
	    	
	    	 PLVStatus plvStatus=PLVStatus.getInstance();
	    	 if(!plvStatus.isValid()){//if no valid license,don't pick jobs.
	    		 return null;
	    	 }
	    	 
	    	 RepositoryMonitor repositoryMonitor = RepositoryMonitor.getInstance();
			 RepositoryStatus repositoryStatus = repositoryMonitor.getRepositoryStatus(null);
			 
			 ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
			 
			 Long criticalLimit = 95l;
			 
			 if(ru.getCodeValue("server_critical_limit", SystemCodeType.GENERAL)!=null){
				 criticalLimit = new Long(ru.getCodeValue("server_critical_limit", SystemCodeType.GENERAL));
			 }
			 if(repositoryStatus.getTotalFreeSpaceOnDisk()>=criticalLimit){
				 return null;
			 }
	    	
	    	 List<Job> jobsList;
	    	 System.out.println("getting jobs for agent: " + agentName + "and actionType: "+actionType);
	    	 
	    	 if(actionType.equalsIgnoreCase("EXPORT")){
	    	     jobsList=jobService.getAgentExportJobs(agentName);
	    	 }else if(actionType.equalsIgnoreCase("DYNARCH")){
	    		 jobsList=jobService.getAgentProactiveJobs(agentName);
	    	 }else if (actionType.equalsIgnoreCase("ACTIVEARCH")){ 
	    		 jobsList = jobService.getAgentActiveArchivingJobs(agentName);
	    	 }else{
	    		 jobsList=jobService.getAgentImportJobs(agentName);
	    	 }
	    	
	    	if(jobsList==null || jobsList.size()==0){
	    		return null;
	    	}
	    	List<com.virtualcode.agent.das.archivePolicy.dto.Job>                jobDTOList              =   new ArrayList<com.virtualcode.agent.das.archivePolicy.dto.Job>();
	   
	    	com.virtualcode.agent.das.archivePolicy.dto.Job                      jobDTO                  =   new com.virtualcode.agent.das.archivePolicy.dto.Job();
	    	com.virtualcode.agent.das.archivePolicy.dto.Policy                   policyDTO               =   new com.virtualcode.agent.das.archivePolicy.dto.Policy();
	    	com.virtualcode.agent.das.archivePolicy.dto.SPDocLib                 spDocLibDTO             =   new com.virtualcode.agent.das.archivePolicy.dto.SPDocLib();
	        com.virtualcode.agent.das.archivePolicy.dto.ExcludedPath             excludedPathDTO         =   new com.virtualcode.agent.das.archivePolicy.dto.ExcludedPath();
	        com.virtualcode.agent.das.archivePolicy.dto.JobStatistics            jobStatisticsDTO        =   new com.virtualcode.agent.das.archivePolicy.dto.JobStatistics();
	        com.virtualcode.agent.das.documentManagement.dto.DocumentType        documentTypeDTO         =   new  com.virtualcode.agent.das.documentManagement.dto.DocumentType();
           
           Job job;
           Agent agent;
           Policy policy;
          
           List<Job> resultsList=new ArrayList<Job>();
           for(int i=0;i<jobsList.size();i++){
           	
           	job=jobsList.get(i);            	
           	policy=job.getPolicy();
           	if(policy!=null){
           		System.out.println("Policy name "+policy.getName());
           		Iterator<DocumentType> ite=policy.getPolicyDocumentTypes().iterator();
           		
	   				 DocumentType selectedDocumentType;	   					   				
	   				 System.out.println("Setting selected document types");
	   				 Set<DocumentType> docTypesSet=new HashSet<DocumentType>();
	   				 
	   				 while(ite!=null && ite.hasNext()){
	   					selectedDocumentType=(DocumentType)ite.next();
	   					DocumentCategory docCategory=selectedDocumentType.getDocumentCategory();
	   					selectedDocumentType.setDocumentCategory(docCategory);
	   					docTypesSet.add(selectedDocumentType);	
	   				 }
	   					
	   				 job.setJobDocuments(docTypesSet);
	   				
           	}
           	
           	agent=job.getAgent();
           	System.out.println("Agent name "+agent.getName());            	          	
           	
           	Iterator<JobSpDocLib> docLibsIterator=job.getJobSpDocLibs().iterator();
       		Iterator<JobSpDocLibExcludedPath> exPathIterator;
       		
       		Set<JobSpDocLib> jobSpDocLibSet=new HashSet<JobSpDocLib>();
       		
       		while(docLibsIterator!=null && docLibsIterator.hasNext()){
       			JobSpDocLib jobSpDocLib=(JobSpDocLib)docLibsIterator.next();
       			exPathIterator=jobSpDocLib.getJobSpDocLibExcludedPaths().iterator();
       			Set<JobSpDocLibExcludedPath> exPathSet=new HashSet<JobSpDocLibExcludedPath>();
       			while(exPathIterator!=null && exPathIterator.hasNext()){				
       				JobSpDocLibExcludedPath exPath=(JobSpDocLibExcludedPath)exPathIterator.next();
       				exPathSet.add(exPath);
       			}
       			jobSpDocLib.setJobSpDocLibExcludedPaths(exPathSet);
       			jobSpDocLibSet.add(jobSpDocLib);
       		}
       		
       		job.setJobSpDocLibs(jobSpDocLibSet);        		
           	
           	resultsList.add(job);
           }
           
           for(Job jobInstance:resultsList){
           	//init
               Set<com.virtualcode.agent.das.archivePolicy.dto.SPDocLib>            spDocLibDTOSet          =   new HashSet<com.virtualcode.agent.das.archivePolicy.dto.SPDocLib>(0);
               Set<com.virtualcode.agent.das.archivePolicy.dto.JobStatistics>       jobStatisticsDTOSet     =   new HashSet<com.virtualcode.agent.das.archivePolicy.dto.JobStatistics>(0);
               Set<com.virtualcode.agent.das.documentManagement.dto.DocumentType>   documentTypeDTOSet      =   new HashSet<com.virtualcode.agent.das.documentManagement.dto.DocumentType>(0);
               jobDTO = getMappedJobDTO(jobInstance);
               policyDTO = getMappedPolicyDTO(jobInstance.getPolicy());
               
               //set documentTypes for policy
               for(DocumentType documentTypeInstance : (Set<DocumentType>)jobInstance.getPolicy().getPolicyDocumentTypes()){
                   documentTypeDTO =   getMappedDocumentTypeDTO(documentTypeInstance);
                   documentTypeDTOSet.add(documentTypeDTO);
               }
               policyDTO.setDocumentTypeSet(documentTypeDTOSet);

               //set policy for job
               jobDTO.setPolicy(policyDTO);
               

               //set spDocLibs for job
               for(JobSpDocLib spDocLibInstance : (Set<JobSpDocLib>)jobInstance.getJobSpDocLibs()){
                   Set<com.virtualcode.agent.das.archivePolicy.dto.ExcludedPath>        excludedPathDTOSet      =   new HashSet<com.virtualcode.agent.das.archivePolicy.dto.ExcludedPath>(0);
                   //System.out.println( "lib name: " + spDocLibInstance.getName());
                   spDocLibDTO =   getMappedSPDocLibDTO(spDocLibInstance);

                   //set excludedPaths for spDocLib
                   for(JobSpDocLibExcludedPath excludedPathInstance : (Set<JobSpDocLibExcludedPath>)spDocLibInstance.getJobSpDocLibExcludedPaths()){
                       excludedPathDTO = getMappedExcludedPathDTO(excludedPathInstance);
                       excludedPathDTOSet.add(excludedPathDTO);
                   }
                   spDocLibDTO.setExcludedPathSet(excludedPathDTOSet);
                   spDocLibDTOSet.add(spDocLibDTO);
                   //System.out.println("----------------");
               }

               jobDTO.setSpDocLibSet(spDocLibDTOSet);
               jobDTOList.add(jobDTO);
           }
           
          
           //        println "process successfully completed"
           return jobDTOList; 
        	    	
	    }
	    
	    public  List<com.virtualcode.vo.Job> getProactiveArchivingList(String agentName) {
	    	
	    	return jobService.getAgentProactiveJobs(agentName);
	    	
	    }
	    
	    public boolean incrProActiveArchCount(Integer jobId,String executionId,String actionType,Integer documentsCount){
	    	boolean status=true;
	    	try{
	    		
	    		List<JobStatistics> jobStatsList=  jobStatisticsService.getJobStatisticsByJob(jobId);
	    		JobStatistics jobStatistics=null;
	    		if(jobStatsList!=null && jobStatsList.size()>0){
	    			jobStatistics=jobStatsList.get(0);
	    			if(actionType!=null && actionType.equalsIgnoreCase("archive")){
	    			   jobStatistics.setTotalFreshArchived(jobStatistics.getTotalFreshArchived()+1);
	    			   jobStatistics.setAlreadyArchived(jobStatistics.getAlreadyArchived()+1);
	    			}
	    			if(actionType!=null && actionType.equalsIgnoreCase("stub")){
		    			   jobStatistics.setTotalStubbed(jobStatistics.getTotalStubbed()+1);
		    		}	
	    			if(actionType!=null && actionType.equalsIgnoreCase("evaluate") && documentsCount!=null && documentsCount.intValue()>0){
	    				
		    			   jobStatistics.setTotalEvaluated(jobStatistics.getTotalEvaluated()+documentsCount.intValue());
		    		}	  
	    			
	    			jobStatisticsService.updateJobStatistics(jobStatistics);
	    			
	    		}else{
	    			jobStatistics=new JobStatistics();
	    			
	    			jobStatistics.setExecutionId(executionId);
	    			jobStatistics.setJob(jobService.getJobById(jobId));
	    			
	    			jobStatistics.setJobStartTime(getCurrentDateTime());	    			
			        
	    			if(actionType!=null && actionType.equalsIgnoreCase("archive")){
		    			   jobStatistics.setTotalFreshArchived(1l);
		    			   jobStatistics.setAlreadyArchived(1l);
		    		}
		    		
	    			if(actionType!=null && actionType.equalsIgnoreCase("stub")){
			    			   jobStatistics.setTotalStubbed(1l);
			    	}	
	    			
	    			if(actionType!=null && actionType.equalsIgnoreCase("evaluate") && documentsCount!=null && documentsCount.intValue()>0){
	    				
		    			   jobStatistics.setTotalEvaluated(jobStatistics.getTotalEvaluated());
		    		}
	    			
		    		jobStatisticsService.saveJobStatistics(jobStatistics);
	    			
	    		}
	    		
	    	}catch(Exception ex){
	    		ex.printStackTrace();
	    		status=false;
	    	}
	    	
	    	return status;
	    }
	    
		
	    public String executionStarted(int jobID){
			Job jobInstance;
			ErrorCode errorCode=null;
			String executionID=null;
			JobStatus previousJobStatusInstance=null;
			Integer previousJobStatusID=null;
			  try{
				  System.out.println("--------------execution started--------------");
		            jobInstance =   jobService.getJobById(jobID);
		       //     System.out.println("--------------execution started 2--------------");
		            errorCode  =   errorCodeService.getById(0);
		            //System.out.println("--------------execution started 3--------------");

		        }catch(Exception e){ 
		        	e.printStackTrace();
		            System.out.println("********************** EXCEPTION **************************");
		            e.printStackTrace();
		            System.out.println( "Exception while initializing instances in executionStarted() ... " + e);
		            System.out.println("***********************************************************");
		            return null;
		        }

		        //  initialize fields
		        try{
		            executionID                 =   generateExecutionID(jobID);
		            System.out.println("Execution ID in agentManagementService: "+ executionID);
		            previousJobStatusInstance   =   getPreviousJobStatusInstance(jobInstance);
		            previousJobStatusID         =   previousJobStatusInstance.getId();
		            System.out.println("Previous Job Status ID: "+previousJobStatusID);
		        }catch(Exception e){
		        	
		        	System.out.println( "********************** EXCEPTION **************************");
		        	System.out.println( "Exception while initializing fields in executionStarted() ... "+e);
		        	System.out.println( "***********************************************************");

		            previousJobStatusID =   -1;
		        }

                String status;
		        //  calculate JobStatus.status
		        if (jobInstance.getActionType().equals(ARCHIVE_DB) || jobInstance.getActionType().equals(INDEXING_DB) || jobInstance.getActionType().equals(ARCHIVE_WITHOUT_STUB_DB)){
		            status  =   "RUNNING";
		        }else if(jobInstance.getActionType().equals(ARCHIVE_AND_STUB_DB)){
		            status  =   "RUNNING";
		        }else if (jobInstance.getActionType().equals(EVALUATE_ONLY_DB)){
		            status  =   "EVALUATING";
//		        }else if (jobInstance.getActionType().equals(EXPORT_DB)){
		        }else if (jobInstance.getActionType().contains("EXPORT")){
		            status  =   "RUNNING";
		        
		        }else if (jobInstance.getActionType().contains("RESTORE")){
		            status  =   "RUNNING";
		        
		        }else if (jobInstance.getActionType().contains("RETENSION")){
		            status  =   "RUNNING";
		        
		        }else if(jobInstance.getActionType().contains("ACTIVEARCH") || jobInstance.getActionType().contains("PRINTARCH")){
//		        	status  = 	"RUNNING";
		        	jobInstance.setCurrentStatus("RUN"); 
		        	jobService.saveRetentionJob(jobInstance);// only save job nothing else
		        	return executionID;
		        	
		        }else{
		            return null;
		        }

		        
		        JobStatus jobStatus=new JobStatus();
		        
		        jobStatus.setExecutionId(executionID);
		        jobStatus.setExecStartTime(getCurrentDateTime());
		        jobStatus.setErrorCode(errorCode);
                jobStatus.setJob(jobInstance);
                jobStatus.setStatus(status);
                jobStatus.setDescription("running ... > job.actionType: " + jobInstance.getActionType());
                jobStatus.setCurrent(1);
                jobStatus.setPreviousJobStatusId(previousJobStatusID);
                jobStatusService.saveJobStatus(jobStatus);
                
                updateIsCurrent(previousJobStatusID);
                System.out.println("Execution ID in agentManagementService: at end "+ executionID);
                
                
                List<SystemCode> sysCodesList=systemCodeService.getSystemCodeByCodename("enable_job_status_email");
    			if(sysCodesList!=null  && sysCodesList.size()>0){
    				enableStatusEmail=(sysCodesList.get(0).getCodevalue().equalsIgnoreCase("yes")?true:false);
    			}
    			if(enableStatusEmail){
                   sendJobStartMail(jobInstance,executionID,status);//start job mail..
    			}
                
                
                return executionID;

		}
	    
	    public String updateStatus(int jobID, String status){
	    	Job jobInstance;
			ErrorCode errorCode=null;
			String executionID=null;
			JobStatus previousJobStatusInstance=null;
			Integer previousJobStatusID=null;
			status = status.toUpperCase();// convert all to upper case
			  try{
				  System.out.println("--------------update status started--------------");
		            jobInstance =   jobService.getJobById(jobID);
		       //     System.out.println("--------------execution started 2--------------");
		            errorCode  =   errorCodeService.getById(0);
		            jobInstance.setCurrentStatus(status);
		            
		            if(status.equalsIgnoreCase("CANCELLED") || status.equalsIgnoreCase("PAUSED")){
		            	jobInstance.setReadyToExecute(false);	// do not pick for next run
		            }else{
		            	jobInstance.setReadyToExecute(true);
		            }
		            jobService.saveJob(jobInstance);
		            //System.out.println("--------------execution started 3--------------");
//		            return "success";
		        }catch(Exception e){ 
		        	e.printStackTrace();
		            System.out.println("********************** EXCEPTION **************************");
		            e.printStackTrace();
		            System.out.println( "Exception while initializing instances in executionStarted() ... " + e);
		            System.out.println("***********************************************************");
		            return "server side exception:" + e.getMessage();
		        }
			  
		        //  initialize fields
		        try{
		            executionID                 =   generateExecutionID(jobID);
		            System.out.println("Execution ID in agentManagementService: "+ executionID);
		            previousJobStatusInstance   =   getPreviousJobStatusInstance(jobInstance);
		            previousJobStatusID         =   previousJobStatusInstance.getId();
		            System.out.println("Previous Job Status ID: "+previousJobStatusID);
		        }catch(Exception e){
		        	
		        	System.out.println( "********************** EXCEPTION **************************");
		        	System.out.println( "Exception while initializing fields in executionStarted() ... "+e);
		        	System.out.println( "***********************************************************");

		            previousJobStatusID =   -1;
		        }

		        //	String status;
		        //  calculate JobStatus.status
		      

		        
		      JobStatus jobStatus=new JobStatus();
		        
		      jobStatus.setExecutionId(executionID);
		      jobStatus.setExecStartTime(getCurrentDateTime());
		      jobStatus.setErrorCode(errorCode);
              jobStatus.setJob(jobInstance);
              jobStatus.setStatus(status);
              jobStatus.setDescription(status +" ... > job.actionType: " + jobInstance.getActionType());
              jobStatus.setCurrent(1);
              jobStatus.setPreviousJobStatusId(previousJobStatusID);
              jobStatusService.saveJobStatus(jobStatus);
              
              updateIsCurrent(previousJobStatusID);

	    	return "success";
	    }
		
	    private void sendJobStartMail(Job jobInstance,String executionID,String status){
	    	
	    	  try{
	    		  
	    	  String mailText="Job Name: \""+jobInstance.getName()+"\" started. Job details are as follows.<br/><br/>";
              mailText+="Job ID: "+jobInstance.getId()+"<br/>";
              mailText+="Execution ID: "+executionID+"<br/>";
              mailText+="Job Name: "+jobInstance.getName()+"<br/>";
              if(jobInstance.getActionType()!=null && jobInstance.getActionType().equalsIgnoreCase("INDEXING")){
            	  mailText+="Action: ANALYSIS<br/>";
              }else{
                 mailText+="Action: "+jobInstance.getActionType()+"<br/>";
              }
              mailText+="Job Start Time:"+new Date().toString()+"<br/>";
              mailText+="Job Status: "+status+"<br/>";             
              Iterator<JobSpDocLib> itte=jobInstance.getJobSpDocLibs().iterator();
              String incPathsStr="";
              String  excPathsStr="";
              String temp;
              while(itte!=null && itte.hasNext()){
              	temp=itte.next().getName();
              	if(jobInstance.getActionType().equals("EXPORT")){
              		incPathsStr+=temp.substring(temp.lastIndexOf("@")+1)+", ";
              	}else{
              		 temp=temp.replaceAll("/", "\\\\");
              		 incPathsStr+="\\\\"+temp.substring(temp.lastIndexOf("@")+1)+", ";
              	}
              }
              mailText+="Job Paths: "+incPathsStr+"<br/>";
              mailText+="-----------------";
              mailService.sendMail("Alert type : Info - Job \""+jobInstance.getName()+"\" started.", mailText);
	    	  }catch(Exception ex){
	    		  ex.printStackTrace();
	    		  System.out.println("Error in Sending mail...");
	    	  }
                            
	    }
	    
	    	    
		
		 private JobStatus getPreviousJobStatusInstance(Job jobInstance) {
				      
			      JobStatus jobStatusInstance   =   null;
                  String jobId=jobInstance.getId()+"";
                  System.out.println("Getting prev job status for job id-->"+jobId);
			      List<JobStatus> jobStatusList=jobStatusService.getJobStatusByJob(Integer.parseInt(jobId));

			        if(jobStatusList!=null && jobStatusList.size() >   0){
			            jobStatusInstance   =   (JobStatus) jobStatusList.get(0);
			            System.out.println("Prev Job Status Id:--:--: "+jobStatusInstance.getId()); 
			            return jobStatusInstance;
			        
			        }
			  return null;
	     }
		 
		private boolean updateIsCurrent(int jobStatusID){
		      
			    System.out.println("updateIsCurrent--jobStatusID--->"+jobStatusID);
		        JobStatus jobStatusInstance = jobStatusService.getJobStatusById(jobStatusID);
		        
		        try{
		        	
		        	jobStatusInstance.setCurrent(1);
		            jobStatusService.saveJobStatus(jobStatusInstance);
		            return true;
		        }catch(Exception e){
		        	e.printStackTrace();
		            System.out.println("********************** EXCEPTION **************************");
		            System.out.println("Exception while updateIsCurrent() ...");		          
		            System.out.println( "***********************************************************");
		            return false;
		        }
		   
		}

		
		
		private String generateExecutionID(int jobID){
	      System.out.println("Generating Execution ID for job ID:"+ jobID);
	        int NO_OF_LAST_DIGITS_OF_JOB_ID=4;
	        int NO_OF_LAST_DIGITS=2;//same for day,month,hour and minute
	        int NO_OF_LAST_DIGHTS_FOR_YEAR=4;
	        String executionID;//format: JJJJDDMMYYYYHHMM
	        String lastFourDigitsOfJobID=getLastDigitsOfIntAsString(jobID,NO_OF_LAST_DIGITS_OF_JOB_ID);

	        Calendar calendar = new GregorianCalendar();

	        //converting one digit to two digit format e.g. converting "4" to "04"
	        //for generating proper executionID with 16 digits
	        String day = getLastDigitsOfIntAsString(calendar.get(Calendar.DATE),NO_OF_LAST_DIGITS);
	        String month = getLastDigitsOfIntAsString(calendar.get(Calendar.MONTH),NO_OF_LAST_DIGITS);
	        String  year = getLastDigitsOfIntAsString(calendar.get(Calendar.YEAR),NO_OF_LAST_DIGHTS_FOR_YEAR);
	        String  hour = getLastDigitsOfIntAsString(calendar.get(Calendar.HOUR),NO_OF_LAST_DIGITS);
	        String  minute = getLastDigitsOfIntAsString(calendar.get(Calendar.MINUTE),NO_OF_LAST_DIGITS);

	        executionID = lastFourDigitsOfJobID + day + month + year + hour + minute;
	   	        
	        System.out.println("Generated ExecutionID: " + executionID);

	        return executionID;
	    }
		
		private String getLastDigitsOfIntAsString(int jobID, int noOfLastDigits){
	       
	        String str= "0000"+jobID;
	        String strLastDigits= str.substring(str.length()-noOfLastDigits,str.length());
	        return strLastDigits;
	    }
		
		 
		 public boolean executionCompleted(int jobID, String executionID, int errorCodeID, com.virtualcode.agent.das.archivePolicy.dto.JobStatistics jobStatistics){
		
			    //System.out.println("executionCompleted: jobId-> "+jobID+",execId-> "+executionID+", errorCodeId-->"+errorCodeID+", jobstats-->"+jobStatistics.toString());
		        Job jobInstance=null;
		        try{
		           jobInstance=jobService.getJobById(jobID);
		           jobInstance.setReadyToExecute(false);
		           jobService.saveJob(jobInstance);
		            //            println "job.readyToExecute set to false"
		        }catch(Exception e){
		            System.out.println("********************** EXCEPTION **************************");
		            System.out.println("Exception while saving Job instance ... ");
		            
		            System.out.println("***********************************************************");
		            return false;
		        }

		        //  define instances
		        ErrorCode errorCodeInstance           =   null;
		        JobStatistics jobStatisticsInstance       =   null;
		        JobStatus previousJobStatusInstance   =   null;

		        //  initialize fields
		        String status                   =   "COMPLETED";
		        int previousJobStatusID         =   -1;

		        //  initialize instances
		        try{
		            errorCodeInstance           =   errorCodeService.getById(errorCodeID);
		            jobStatisticsInstance       =   mapJobStatisticsInstance(jobStatistics);
		            previousJobStatusInstance   =   getPreviousJobStatusInstance(jobInstance);
		            
		            previousJobStatusID =   previousJobStatusInstance.getId();

		        }catch(Exception e){
		        	e.printStackTrace();
		            errorCodeID             =   1;
		            errorCodeInstance       =   errorCodeService.getById(errorCodeID);

		            jobStatisticsInstance   =   generateDummyJobStatisticsInstance(jobInstance.getId());
		            jobStatisticsInstance.setJob(jobInstance);

		            jobStatisticsService.saveJobStatistics(jobStatisticsInstance);		                   
		        }

		        //create JobStatus instance
	            if(previousJobStatusInstance!=null && previousJobStatusInstance.getStatus()!=null && previousJobStatusInstance.getStatus().contains("CANCEL")){
	            	previousJobStatusInstance.setStatus("CANCELED");
	            	jobStatusService.saveJobStatus(previousJobStatusInstance);
	            }else{
			        JobStatus jobStatusInstance  =   new JobStatus();		        
			          jobStatusInstance.setJob(jobInstance);
			          jobStatusInstance.setErrorCode(errorCodeInstance);
			          jobStatusInstance.setExecStartTime(getCurrentDateTime());
			          jobStatusInstance.setStatus(status);
			          jobStatusInstance.setDescription("completed ...");
			          jobStatusInstance.setCurrent(1);
			          jobStatusInstance.setPreviousJobStatusId(previousJobStatusID);
			          jobStatusInstance.setExecutionId(executionID);		          
			           
	
			        //save new JobStatus instance
			        try{
			            jobStatusService.saveJobStatus(jobStatusInstance);
			          
			        }catch(Exception e){
			            System.out.println("********************** EXCEPTION **************************");
			            System.out.println("Exception while saving JobStatus instance ... ");
			            e.printStackTrace();
			            System.out.println("***********************************************************");
			            return false;
			        }
	            }

		        //update last jobStatus.isCurrent to 0
		        updateIsCurrent(previousJobStatusID);

		        //create JobStatistics instance
		        jobStatisticsInstance.setJob(jobInstance);

		        //save new JobStatistics instance
		        try{
		            jobStatisticsService.saveJobStatistics(jobStatisticsInstance);
		            
		            List<SystemCode> sysCodesList=systemCodeService.getSystemCodeByCodename("enable_job_status_email");
	    			if(sysCodesList!=null  && sysCodesList.size()>0){
	    				enableStatusEmail=(sysCodesList.get(0).getCodevalue().equalsIgnoreCase("yes")?true:false);
	    			}
	    			if(enableStatusEmail){
		                sendJobCompleteMail(jobInstance,jobStatisticsInstance,executionID);
	    			}
		            
		            //            println "new JobStatistics instance saved"
		        }catch(NullPointerException e){
		        	System.out.println( "********************** EXCEPTION **************************");
		        	System.out.println( "Exception while saving JobStatistics instance ... ");
		            e.printStackTrace();
		            System.out.println( "***********************************************************");
		            return false;
		        }
		        
		        return true;
		        
	    }
		 
		 private void sendJobCompleteMail(Job jobInstance,JobStatistics jobStatInstance,String executionId){
		    	
	    	  try{
	    		 
	    	   String mailText="Job Name  \""+jobInstance.getName()+"\"  has completed. Job execution details are as follows.<br/><br/>";
	           
	    	   mailText+="Job ID: "+jobInstance.getId()+"<br/>";
	    	   mailText+="Execution ID: "+executionId+"<br/>";
	           mailText+="Job Name: "+jobInstance.getName()+"<br/>";
	           if(jobInstance.getActionType().equalsIgnoreCase("INDEXING")){
	        	  mailText+="Action: ANALYSIS<br/>";
	           }else{
	              mailText+="Action: "+jobInstance.getActionType()+"<br/>";
	           }
	           
	           mailText+="Job Start Time: "+jobStatInstance.getJobStartTime()+"<br/>";
	           
	           mailText+="Job End Time: "+jobStatInstance.getJobEndTime()+"<br/>";
	          
	           mailText+="Total Documents: "+jobStatInstance.getTotalEvaluated()+"<br/>";
	           	           
	           mailText+="Total Visited: "+jobStatInstance.getTotalEvaluated()+"<br/>";
	           
	           if(jobInstance.getActionType().equalsIgnoreCase("EXPORT") || jobInstance.getActionType().equalsIgnoreCase("RESTORE") ){
	               mailText+="Total Exported: "+jobStatInstance.getTotalFreshArchived()+"<br/>";
	             
	           }else if(jobInstance.getActionType().equalsIgnoreCase("INDEXING")){
	               mailText+="Total Analyzed: "+jobStatInstance.getTotalFreshArchived()+"<br/>"; 
               }else if(jobInstance.getActionType().equalsIgnoreCase("EVALUATE")){
	              //nothing to show
            	   mailText+="Total Qualified: " + (jobStatInstance.getTotalEvaluated()-jobStatInstance.getTotalSkipped()) + "<br/>";
               } else{	           
	        	   mailText+="Already Archived: "+jobStatInstance.getAlreadyArchived()+"<br/>";
	        	   mailText+="Total Archived: "+jobStatInstance.getTotalFreshArchived()+"<br/>";
	        	   mailText+="Total Stubbed: "+jobStatInstance.getTotalStubbed()+"<br/>"; 
	           }
	           
	           //mailText+="Total Errored: "+jobStatInstance.getTotalFailed()+"<br/>";
	           
	           if(jobStatInstance.getArchivedVolume()>0){
	        	   if(jobInstance.getActionType().equalsIgnoreCase("INDEXING") || jobInstance.getActionType().equalsIgnoreCase("EVALUATE")){
	        		   mailText+="Total Processed Volume: "+jobStatInstance.getArchivedVolume()+" MB<br/>";  
	        	   }else if(jobInstance.getActionType().equalsIgnoreCase("EXPORT") || jobInstance.getActionType().equalsIgnoreCase("RESTORE")){
	        		   mailText+="Total Exported Volume: "+jobStatInstance.getArchivedVolume()+" MB<br/>";  
	        	   }	        	   
	        	   else{
	        	       mailText+="Total Archived Volume: "+jobStatInstance.getArchivedVolume()+" MB<br/>";
	        	   }
	           }
	        	   
	           long totalSkipped = jobStatInstance.getSkippedProcessfileSkippedHiddenlected()+
	        		     jobStatInstance.getSkippedProcessfileSkippedIsdir()+
						 jobStatInstance.getSkippedProcessfileSkippedMismatchAge()+
						 jobStatInstance.getSkippedProcessfileSkippedMismatchExtension()+
						 jobStatInstance.getSkippedProcessfileSkippedMismatchLastaccesstime()+
						 jobStatInstance.getSkippedProcessfileSkippedMismatchLastmodifiedtime()+
						 jobStatInstance.getSkippedProcessfileSkippedReadOnly()+
						 jobStatInstance.getSkippedProcessfileSkippedSymbollink()+
						 jobStatInstance.getSkippedProcessfileSkippedTemporary()+
						 jobStatInstance.getSkippedProcessfileSkippedTooLarge();
	           
	           mailText+="Total Skipped: "+totalSkipped+"<br/>"; 
	         
	          
	           Iterator<JobSpDocLib> itte=jobInstance.getJobSpDocLibs().iterator();
	              String incPathsStr="";
	              String  excPathsStr="";
	              String temp;
	              while(itte!=null && itte.hasNext()){
	              	temp=itte.next().getName();
	              	if(jobInstance.getActionType().equals("EXPORT")){
	              		incPathsStr+=temp.substring(temp.lastIndexOf("@")+1)+", ";
	              	}else{
	              		 temp=temp.replaceAll("/", "\\\\");
	              		 incPathsStr+="\\\\"+temp.substring(temp.lastIndexOf("@")+1)+", ";
	              	}
	              }
	           
	           mailText+="Job Paths: "+incPathsStr+"<br/>";	           	                     
	           
	           mailText+="-----------------";
	           mailService.sendMail("Alert type : Info - Job \""+jobInstance.getName()+"\" has completed.", mailText);
		    	  }catch(Exception ex){
		    		  ex.printStackTrace();
		    		  System.out.println("Error in Sending mail...");
		    	  }
                         
	    }
	    
		 //this method is being used from WS(remote agents) to syn hotStats
		 public boolean updateHotStatistics(int jobID, String executionID, int errorCodeID,com.virtualcode.agent.das.archivePolicy.dto.JobStatistics jobStatistics){
			 try{
				 HotStatistics hotStatistics = HotStatistics.getInstance();
				 hotStatistics.setEvaluatedCtr(executionID, jobStatistics.getTotalEvaluated());
				 hotStatistics.setArchivedCtr(executionID, jobStatistics.getTotalFreshArchived());
				 hotStatistics.setAlreadyArchivedCtr(executionID, jobStatistics.getAlreadyArchived());
				 hotStatistics.setStubbedCtr(executionID, jobStatistics.getTotalStubbed());
				 hotStatistics.setExportedCtr(executionID, jobStatistics.getTotalFreshArchived());	//total archived = total export in case of export job
				 Long totalFailed = jobStatistics.getTotalFailedArchiving() + jobStatistics.getTotalFailedDeduplication() + jobStatistics.getTotalFailedEvaluating()
						 + jobStatistics.getTotalFailedStubbing();
				 hotStatistics.setFailedCtr(executionID, totalFailed);
	//			 hotStatistics.setHotStatisticsAvailable(executionID, 1l);
			 return true;
			 }catch (Exception e) {
				// TODO: handle exception
				 e.printStackTrace();
				 return false;
			}
//			 hotStatistics.setCompletedCtr(executionID, jobStatistics.gett)
//			 hotStatistics.
//			 hotStatistics.
			 
		 }
		public List<Integer> getCancelJobs(){
				return jobService.getCancelJobs();
		}		 
		 
		 public JobStatistics mapJobStatisticsInstance(com.virtualcode.agent.das.archivePolicy.dto.JobStatistics jobStatistics){
		        //        println "---------------------------------------------------------------"
		        //        println "converting java JobStatistics instance to grails..."

			    System.out.println("mapJobStatisticsInstance method.....");
			    
			    System.out.println("jobstatistics -- exec id--> "+jobStatistics.getExecutionID());
			    System.out.println("jobstatistics -- JobStartTime --> "+jobStatistics.getJobStartTime());
			    System.out.println("jobstatistics -- jobEnd time --> "+jobStatistics.getJobEndTime());
			    
		        JobStatistics jobStatisticsInstance   =   new JobStatistics();
		        jobStatisticsInstance.setExecutionId(jobStatistics.getExecutionID());
		        if(jobStatistics.getJobStartTime()==null){
		        	jobStatistics.setJobStartTime(new Date());
		        }
		        if(jobStatistics.getJobEndTime()==null){
		        	jobStatistics.setJobEndTime(new Date());
		        }
		        jobStatisticsInstance.setJobStartTime(Utility.convertDateToTimeStamp(jobStatistics.getJobStartTime()));
		        jobStatisticsInstance.setJobEndTime(Utility.convertDateToTimeStamp(jobStatistics.getJobEndTime()));
		        jobStatisticsInstance.setTotalEvaluated(jobStatistics.getTotalEvaluated());
		        jobStatisticsInstance.setTotalDuplicated(jobStatistics.getTotalDuplicated());
		        jobStatisticsInstance.setTotalFreshArchived(jobStatistics.getTotalFreshArchived());
		        jobStatisticsInstance.setAlreadyArchived(jobStatistics.getAlreadyArchived());
		        jobStatisticsInstance.setTotalStubbed(jobStatistics.getTotalStubbed());
		        jobStatisticsInstance.setTotalFailedArchiving(jobStatistics.getTotalFailedArchiving());
		        jobStatisticsInstance.setTotalFailedEvaluating(jobStatistics.getTotalFailedEvaluating());
		        jobStatisticsInstance.setTotalFailedStubbing(jobStatistics.getTotalFailedStubbing());
		        jobStatisticsInstance.setTotalFailedDeduplication(jobStatistics.getTotalFailedDeduplication());
		        jobStatisticsInstance.setSkippedProcessfileSkippedReadOnly(jobStatistics.getSkippedProcessFileSkippedReadOnly());
		        jobStatisticsInstance.setSkippedProcessfileSkippedHiddenlected(jobStatistics.getSkippedProcessFileSkippedHiddenLected());
		        jobStatisticsInstance.setSkippedProcessfileSkippedTooLarge(jobStatistics.getSkippedProcessFileSkippedTooLarge());
		        jobStatisticsInstance.setSkippedProcessfileSkippedTemporary(jobStatistics.getSkippedProcessFileSkippedTemporary());
		        jobStatisticsInstance.setSkippedProcessfileSkippedSymbollink( jobStatistics.getSkippedProcessFileSkippedSymbolLink());
		        jobStatisticsInstance.setSkippedProcessfileSkippedIsdir(jobStatistics.getSkippedProcessFileSkippedIsDir());
		        jobStatisticsInstance.setSkippedProcessfileSkippedMismatchAge(jobStatistics.getSkippedProcessFileSkippedMismatchAge());
		        jobStatisticsInstance.setSkippedProcessfileSkippedMismatchLastaccesstime(jobStatistics.getSkippedProcessFileSkippedMismatchLastAccessTime());
		        jobStatisticsInstance.setSkippedProcessfileSkippedMismatchLastmodifiedtime(jobStatistics.getSkippedProcessFileSkippedMismatchLastModifiedTime());
		        jobStatisticsInstance.setSkippedProcessfileSkippedMismatchExtension(jobStatistics.getSkippedProcessFileSkippedMismatchExtension());
		        jobStatisticsInstance.setArchivedVolume(jobStatistics.getArchivedVolume());
		        
		        
		        return jobStatisticsInstance;

		    }
		 
		 private JobStatistics generateDummyJobStatisticsInstance(int jobID){
		        //        println "---------------------------------------------------------------"
		        //        println "generating dummy JobStatistics instance"

			    JobStatistics jobStatistics=new JobStatistics();
			    jobStatistics.setExecutionId(generateExecutionID(jobID));
			    jobStatistics.setExecutionId(generateExecutionID(jobID));
			    jobStatistics.setJobStartTime(getCurrentDateTime());
			    jobStatistics.setJobEndTime(getCurrentDateTime());
			    jobStatistics.setTotalEvaluated(0l);
			    jobStatistics.setTotalDuplicated(0l);
			    jobStatistics.setTotalFreshArchived(0l);
			    jobStatistics.setAlreadyArchived(0l);
			    jobStatistics.setTotalStubbed(0l);
			    jobStatistics.setTotalFailedArchiving(0l);
			    
			    jobStatistics.setTotalFailedEvaluating(0l);
			    jobStatistics.setTotalFailedStubbing(0l);
			    jobStatistics.setTotalFailedDeduplication(0l);
			    
			    jobStatistics.setSkippedProcessfileSkippedReadOnly(0l);
			    jobStatistics.setSkippedProcessfileSkippedHiddenlected(0l);
			    jobStatistics.setSkippedProcessfileSkippedTooLarge(0l);
			    
			    jobStatistics.setSkippedProcessfileSkippedTemporary(0l);
			    jobStatistics.setSkippedProcessfileSkippedSymbollink(0l);
			    jobStatistics.setSkippedProcessfileSkippedIsdir(0l);
			    
			    jobStatistics.setSkippedProcessfileSkippedMismatchAge(0l);
			    jobStatistics.setSkippedProcessfileSkippedMismatchLastaccesstime(0l);
			    jobStatistics.setSkippedProcessfileSkippedMismatchLastmodifiedtime(0l);
			    jobStatistics.setSkippedProcessfileSkippedMismatchExtension(0l);
			    
		       

		        return jobStatistics;
		    }
		 
		 private Timestamp getCurrentDateTime() {
		       
		        Calendar cal = new GregorianCalendar();
		        java.sql.Timestamp timeStampDate=null;
		        SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");

		        SimpleDateFormat dateFormat = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
		        Date convertedDate=null;
				try {
					convertedDate = dateFormat.parse(sdf.format(cal.getTime()));
					timeStampDate = new 	Timestamp(convertedDate.getTime());
				} catch (ParseException e) {				
					e.printStackTrace();
				}
		        return timeStampDate;
		    }
		 
		 /**
	     *
	     * *************************************************************************
	     * Utility Methods for getJobListForProcessing()
	     * *************************************************************************
	     *
	     * */
	    public com.virtualcode.agent.das.archivePolicy.dto.Job getMappedJobDTO(Job jobInstance){
	        //        println "---------------------------------------------------------------"
	        //        println "mapping job DTO ... "

	    	com.virtualcode.agent.das.archivePolicy.dto.Job job   =   new com.virtualcode.agent.das.archivePolicy.dto.Job();

	        job.setId(jobInstance.getId());
	        job.setActive(jobInstance.getActive());
	        job.setJobName(jobInstance.getName());
	        job.setDescription(jobInstance.getDescription());
	        job.setExecTimeStart(jobInstance.getExecTimeStart());
	        job.setActionType(jobInstance.getActionType());
	        job.setSiteId(jobInstance.getSpSiteId());
	        job.setSitePath(jobInstance.getSpPath());
	        job.setProcessCurrentPublishedVersion(jobInstance.getProcessCurrentPublishedVersion());
	        job.setProcessLegacyPublishedVersion(jobInstance.getProcessLegacyPublishedVersion());
	        job.setProcessLegacyDraftVersion(jobInstance.getProcessLegacyDraftVersion());
	        job.setProcessReadOnly(jobInstance.getProcessReadOnly());
	        job.setProcessArchive(jobInstance.getProcessArchive());
	        job.setProcessHidden(jobInstance.getProcessHidden());
	        job.setReadyToExecute(jobInstance.getReadyToExecute());
	        job.setActiveActionType(jobInstance.getActiveActionType());
	        job.setTags(jobInstance.getTags());
	        job.setCurrentStatus(jobInstance.getCurrentStatus());

	        return job;
	    }
	    public com.virtualcode.agent.das.archivePolicy.dto.Policy getMappedPolicyDTO(Policy policyInstance){
	        //        println "---------------------------------------------------------------"
	        //        println "mapping policy DTO ... "

	    	com.virtualcode.agent.das.archivePolicy.dto.Policy policy=new com.virtualcode.agent.das.archivePolicy.dto.Policy();

	        policy.setId(policyInstance.getId());
	        policy.setPolicyName(policyInstance.getName());
	        policy.setDescription(policyInstance.getDescription());
	        policy.setDocumentAge(policyInstance.getDocumentAge());
	        policy.setLastModifiedDate(policyInstance.getLastModifiedDate());
	        policy.setLastAccessedDate(policyInstance.getLastAccessedDate());
	        policy.setSizeLargerThan(policyInstance.getSizeLargerThan());
	        policy.setActive(policyInstance.getActive());

	        return policy;
	    }
	    
	    public com.virtualcode.agent.das.documentManagement.dto.DocumentType getMappedDocumentTypeDTO(DocumentType documentTypeInstance){
	        //        println "---------------------------------------------------------------"
	        //        println "mapping documentType DTO ... "

	    	com.virtualcode.agent.das.documentManagement.dto.DocumentType documentType    =   new  com.virtualcode.agent.das.documentManagement.dto.DocumentType();

	        documentType.setId(documentTypeInstance.getId());
	        documentType.setName(documentTypeInstance.getName());
	        documentType.setValue(documentTypeInstance.getValue());

	        return documentType;
	    }
	   public com.virtualcode.agent.das.archivePolicy.dto.SPDocLib getMappedSPDocLibDTO(JobSpDocLib spDocLibInstance){
	        //        println "---------------------------------------------------------------"
	        //        println "mapping spDocLib DTO ... with spDocLib id: "+spDocLibInstance.id

		   com.virtualcode.agent.das.archivePolicy.dto.SPDocLib spDocLib    =   new com.virtualcode.agent.das.archivePolicy.dto.SPDocLib();
	        
	        spDocLib.setId(spDocLibInstance.getId());
	        spDocLib.setLibraryName(spDocLibInstance.getName());
	        spDocLib.setLibraryGUID(spDocLibInstance.getGuid());
	        spDocLib.setLibraryType(spDocLibInstance.getType());
	        spDocLib.setDestinationPath(spDocLibInstance.getDestinationPath());
	        spDocLib.setRecursive(spDocLibInstance.getRecursive());
	        spDocLib.setSitePath(spDocLibInstance.getSite());
	        spDocLib.setDrivePath(spDocLibInstance.getDrivePath());
	        spDocLib.setContentTypes(spDocLibInstance.getContentTypes());
	        spDocLib.setTags(spDocLibInstance.getTags());

	        return spDocLib;
	    }
	    public com.virtualcode.agent.das.archivePolicy.dto.ExcludedPath getMappedExcludedPathDTO(JobSpDocLibExcludedPath excludedPathInstance){
	        //        println "---------------------------------------------------------------"
	        //        println "mapping spDocLib DTO ... with spDocLib id: "+spDocLibInstance.id

	    	com.virtualcode.agent.das.archivePolicy.dto.ExcludedPath excludedPath    =   new com.virtualcode.agent.das.archivePolicy.dto.ExcludedPath();

	        excludedPath.setId(excludedPathInstance.getId());
	        excludedPath.setPath(excludedPathInstance.getPath());

	        return excludedPath;
	    }
	    
	    public com.virtualcode.agent.das.archivePolicy.dto.JobStatistics getMappedJobStatisticsDTO(JobStatistics jobStatisticsInstance){
	        //        println "---------------------------------------------------------------"
	        //        println "mapping JobStatistics DTO ... with jobStatistics id: "+jobStatisticsInstance.id

	    	com.virtualcode.agent.das.archivePolicy.dto.JobStatistics jobStatistics    =   new com.virtualcode.agent.das.archivePolicy.dto.JobStatistics();

	        jobStatistics.setExecutionID(jobStatisticsInstance.getExecutionId());
	        jobStatistics.setJobEndTime(jobStatisticsInstance.getJobEndTime());
	        jobStatistics.setJobStartTime(jobStatisticsInstance.getJobStartTime());
	        jobStatistics.setSkippedProcessFileSkippedHiddenLected(jobStatisticsInstance.getSkippedProcessfileSkippedHiddenlected());
	        jobStatistics.setSkippedProcessFileSkippedIsDir(jobStatisticsInstance.getSkippedProcessfileSkippedIsdir());
	        jobStatistics.setSkippedProcessFileSkippedMismatchAge(jobStatisticsInstance.getSkippedProcessfileSkippedMismatchAge());
	        jobStatistics.setSkippedProcessFileSkippedMismatchExtension(jobStatisticsInstance.getSkippedProcessfileSkippedMismatchExtension());
	        jobStatistics.setSkippedProcessFileSkippedMismatchLastAccessTime(jobStatisticsInstance.getSkippedProcessfileSkippedMismatchLastaccesstime());
	        jobStatistics.setSkippedProcessFileSkippedMismatchLastModifiedTime(jobStatisticsInstance.getSkippedProcessfileSkippedMismatchLastmodifiedtime());
	        jobStatistics.setSkippedProcessFileSkippedReadOnly(jobStatisticsInstance.getSkippedProcessfileSkippedReadOnly());
	        jobStatistics.setSkippedProcessFileSkippedSymbolLink(jobStatisticsInstance.getSkippedProcessfileSkippedSymbollink());
	        jobStatistics.setSkippedProcessFileSkippedTemporary(jobStatisticsInstance.getSkippedProcessfileSkippedTemporary());
	        jobStatistics.setSkippedProcessFileSkippedTooLarge(jobStatisticsInstance.getSkippedProcessfileSkippedTooLarge());
	        jobStatistics.setTotalFreshArchived(jobStatisticsInstance.getTotalFreshArchived());
	        jobStatistics.setAlreadyArchived(jobStatisticsInstance.getAlreadyArchived());
	        jobStatistics.setTotalDuplicated(jobStatisticsInstance.getTotalDuplicated());
	        jobStatistics.setTotalEvaluated(jobStatisticsInstance.getTotalEvaluated());
	        jobStatistics.setTotalFailedArchiving(jobStatisticsInstance.getTotalFailedArchiving());
	        jobStatistics.setTotalFailedDeduplication(jobStatisticsInstance.getTotalFailedDeduplication());
	        jobStatistics.setTotalFailedEvaluating(jobStatisticsInstance.getTotalFailedEvaluating());
	        jobStatistics.setTotalFailedStubbing(jobStatisticsInstance.getTotalFailedStubbing());
	        jobStatistics.setTotalStubbed(jobStatisticsInstance.getTotalStubbed());

	        return jobStatistics;
	    }
		 
	    
	    private ServiceManager serviceManager;
	    private SystemCodeService systemCodeService;
	    private MailService mailService;
	    private JobService jobService;
	    private ErrorCodeService errorCodeService;
	    private JobStatusService jobStatusService;
	    private JobStatisticsService jobStatisticsService;	 
	    
//	    private ProactiveLibService proactiveLibService;
	    
//	    private JobService getJobService(ServletContext sc){
//	    	this.serviceManager=(ServiceManager)getWebAppContext(sc).getBean("serviceManager");
//	    	this.jobService=serviceManager.getJobService();
//	    	this.errorCodeService=serviceManager.getErrorCodeService();
//	    	this.jobStatusService=serviceManager.getJobStatusService();
//	    	this.jobStatisticsService=serviceManager.getJobStatisticsService();
//	        return jobService;
//	    }

//		public void setServiceManager(ServiceManager serviceManager) {
//			this.serviceManager = serviceManager;
//	    	this.jobService=serviceManager.getJobService();
//	    	this.errorCodeService=serviceManager.getErrorCodeService();
//	    	this.jobStatusService=serviceManager.getJobStatusService();
//	    	this.jobStatisticsService=serviceManager.getJobStatisticsService();
//		}	    
	    
	
}
