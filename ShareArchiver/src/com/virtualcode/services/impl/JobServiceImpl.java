package com.virtualcode.services.impl;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.JobDAO;
import com.virtualcode.dao.JobSpDocLibDAO;
import com.virtualcode.dao.JobSpDocLibExcludedPathDAO;
import com.virtualcode.services.JobService;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.StringUtils;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.DriveLetters;
import com.virtualcode.vo.Job;
import com.virtualcode.vo.JobSpDocLib;
import com.virtualcode.vo.JobSpDocLibExcludedPath;
import com.virtualcode.vo.SystemCodeType;


public class JobServiceImpl implements JobService{

	@Override
	public void saveJob(Job job) {
		jobDAO.save(job);
		if(!job.getActionType().equals("INDEXING")){	//handelled in the main function
			Iterator<JobSpDocLib> docLibsIterator=job.getJobSpDocLibs().iterator();
			Iterator<JobSpDocLibExcludedPath> exPathIterator;
			while(docLibsIterator!=null && docLibsIterator.hasNext()){
				JobSpDocLib jobSpDocLib=(JobSpDocLib)docLibsIterator.next();
				exPathIterator=jobSpDocLib.getJobSpDocLibExcludedPaths().iterator();
				
				jobSpDocLibDAO.save(jobSpDocLib);
				while(exPathIterator!=null && exPathIterator.hasNext()){				
					JobSpDocLibExcludedPath exPath=(JobSpDocLibExcludedPath)exPathIterator.next();
					jobSpDocLibExcludedPathDAO.save(exPath);
				}			
			}
		}
		
	}
	
	@Override
	public void saveRetentionJob(Job job) {
		jobDAO.save(job);		
	}
	
	@Override
	public void saveExportJob(Job job) {
		jobDAO.save(job);
		Iterator<JobSpDocLib> docLibsIterator=job.getJobSpDocLibs().iterator();
		Iterator<JobSpDocLibExcludedPath> exPathIterator;
		while(docLibsIterator!=null && docLibsIterator.hasNext()){
			JobSpDocLib jobSpDocLib=(JobSpDocLib)docLibsIterator.next();
			//exPathIterator=jobSpDocLib.getJobSpDocLibExcludedPaths().iterator();
			jobSpDocLibDAO.save(jobSpDocLib);
			
		}
		
	}

	@Override
	public void saveExportJobPriv(Job job, List<String> exportPathList, String remoteUNC) {
		ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
		Date date = VCSUtil.getCurrentDateTime();
		Timestamp timestamp = new Timestamp(date.getTime());
		job.setExecTimeStart(timestamp);
        
        job.setProcessCurrentPublishedVersion(false);
        job.setProcessLegacyPublishedVersion(false);
        job.setProcessLegacyDraftVersion(false);
        job.setProcessReadOnly(false);
        job.setProcessArchive(false);
        job.setProcessHidden(false);
        
        job.setReadyToExecute(true);
        job.setActive(1);
        job.setExecutionInterval(1d);
        job.setActionType("EXPORTSEARCHED");
        Set<JobSpDocLib> setJobSpDocLibs = new HashSet<JobSpDocLib>();
//		searchResults.toString();
//		System.out.println(selectAll);

		long fileCount = 0l;

			for(String exportPath:exportPathList){
				JobSpDocLib jobSpDocLib = new JobSpDocLib();        	
//				String[] elements = StringUtils.getPathElements(exportPath);
//				String path ="";
//				for(int i = 2; i <elements.length;i++ ){
//					path = path + elements[i];
//					if(i < elements.length-1){
//						path+="/";
//					}
//				}
				//only upto maximum limit
//				if(limit>0 && fileCount>=limit){
//					break;
//				}
	    	
	        	String smbPath = "smb://"+ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL) +";"+ru.getCodeValue("restore.username", SystemCodeType.GENERAL)+":"+ru.getCodeValue("restore.password", SystemCodeType.GENERAL)+"@"
	        					   + remoteUNC.substring(2).replaceAll("\\\\", "/");
	        	if(job.isPreserveFilePaths()){
	        		if(!(smbPath.endsWith("/"))){
	        			smbPath+="/";
	        		}
	        	
	        		String[] elements = StringUtils.getPathElements(exportPath);
	        		
	        		for(int i = 4; i <elements.length-1; i++){	        			
	        			smbPath+=elements[i]+"/";
	        		}
	        		
	        	}
				System.out.println("-------"+smbPath+"-----");
				jobSpDocLib.setRecursive(false);
				jobSpDocLib.setName(exportPath);
				jobSpDocLib.setGuid(null);
	//			if(job.getAgent().getType().trim().equals("SP")){
	//				jobSpDocLib.setType("SP");
	//				jobSpDocLib.setDestinationPath(path);
	//			}else{
					jobSpDocLib.setType("CIFS");
					jobSpDocLib.setDestinationPath(smbPath);
	//			}
				
				jobSpDocLib.setJobSpDocLibExcludedPaths(null);
				setJobSpDocLibs.add(jobSpDocLib);
				jobSpDocLib.setJob(job);
				fileCount = fileCount +1;
			}
		
		
        job.setJobSpDocLibs(new HashSet());
        job.getJobSpDocLibs().addAll(setJobSpDocLibs);        
        saveExportJob(job);
		
	}
	
	@Override
	public List getLibsByNameLike(String value){
		return jobSpDocLibDAO.findByNameLike(value);
	}

	@Override
	public List<Job> getAllJobs() {
		return jobDAO.findAll();
	}

	@Override
	public Job getJobById(Integer jobId) {
		return jobDAO.findById(jobId);
	}

	@Override
	public void updateJob(Job job) {
		 jobDAO.save(job);
	}

	@Override
	public void deleteJob(Job job) {			
		jobDAO.delete(job);
	}
	
	public void deleteJobDependencies(Job job){
		
		jobDAO.deleteJobExcludedPaths(job);
		jobDAO.deleteJobSpDocLib(job);
	}
	
	public void deleteSpDocLib(String name){
		List<JobSpDocLib> libList = jobSpDocLibDAO.findByNameLikeForArchive(name);
		if(libList!=null && libList.size()>0){
			for(JobSpDocLib lib: libList){
				System.out.println("lib id: "+lib.getName());
				jobSpDocLibDAO.delete(lib);
			}
		}
		
	}
	
	public void mergeJobTransient(Job job) {
		jobDAO.merge(job);
	}
	
	public void saveSpDocLib(JobSpDocLib lib){
		jobSpDocLibDAO.merge(lib);
	}
	
	@Override
	public List<Job> getAgentIndexingJobs(String agentName){
		return jobDAO.findAgentIndexingJobs(agentName);
	}
	
	@Override
	public List<Job> getAgentRetensionJobs(String agentName) {
		return jobDAO.findAgentRetensionJobs(agentName);
	}
	
	@Override
	public List<Job> getAgentRestoreJobs(String agentName) {
		return jobDAO.findAgentRestoreJobs(agentName);
	}
	
	@Override
	public List<Job> getAgentImportJobs(String agentName){
		return jobDAO.findAgentImportJobs(agentName);
	}
	
	@Override
	public List<Job> getAgentScheduledJobs(String agentName){
		return jobDAO.findAgentScheduledJobs(agentName);
	}	
	
	@Override
	public List<Job> getAllScheduledJobs(){
		return jobDAO.findAllScheduledJobs();
	}

	public List<Job> getAgentExportJobs(String agentName){
		return jobDAO.findAgentExportJobs(agentName);
	}
		
    private JobDAO jobDAO;	
	private JobSpDocLibDAO jobSpDocLibDAO;
	private JobSpDocLibExcludedPathDAO jobSpDocLibExcludedPathDAO;
    private DAOManager daoManager;	
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.jobDAO=daoManager.getJobDAO();
		this.jobSpDocLibDAO=daoManager.getJobSpDocLibDAO();
		this.jobSpDocLibExcludedPathDAO=daoManager.getJobSpDocLibExcludedPathDAO();
	}

	@Override
	public Job getJobByName(String name) {
		// TODO Auto-generated method stub
		List jobList = jobDAO.findByName(name);
		if(jobList!=null && jobList.size()>0)
		{
			return (Job)jobList.get(0);
		}
		return null;
	}
	
	@Override
	public List<DriveLetters> getSpDocLibByDriveLetters(List<DriveLetters> tempList) {
		
		List<DriveLetters> driveLettersList	=	null;
		if(tempList!=null) {
			driveLettersList		=	new ArrayList<DriveLetters>();
			for(int i=0; i<tempList.size(); i++) {
				DriveLetters dLtr	=	tempList.get(i);
				String sharePath	=	dLtr.getSharePath();
				
				if(sharePath!=null && sharePath.startsWith("\\\\") && dLtr.getDriveLetter()!=null) {
					//smb://NETWORK;se6:jwdmuz1@vc6/sp_export/Archiving/
					String fullPath	=	"smb://"+dLtr.getRestoreDomain()+";"+dLtr.getRestoreUsername()+":"+dLtr.getRestorePassword()+"@" 
										+ sharePath.substring(2).replace("\\", "/");
					
					List<JobSpDocLib> matchedSpDocs	=	jobSpDocLibDAO.findByProperty("name", fullPath);
					if(matchedSpDocs!=null && matchedSpDocs.size()>0) {
						dLtr.setSpDocLibId(matchedSpDocs.get(0).getId());
						System.out.println("FullPath : "+ fullPath + " is mapped on " + matchedSpDocs.get(0).getId());
						driveLettersList.add(dLtr);
					} else {
						System.out.println("Not Analyzed, so skipped : "+ fullPath);
					}
					
				} else {
					System.out.println("Not a valid Path so skipped in DriveLetters list : "+ sharePath);
				}
			}
		} 

		return driveLettersList;
	}
	
	
	public boolean updateJobExecutionStatus(final Integer jobStatusId,final String status){
		
		return jobDAO.updateJobExecutionStatus(jobStatusId,status);
	}
	
	public List<Integer> getCancelJobs(){
		return jobDAO.findCancelJobs();
	}
	

	@Override
	public List<Job> getJobByProperty(String propertyName, Object value) {
		// TODO Auto-generated method stub		
		return jobDAO.findByProperty(propertyName, value);
	}

	@Override
	public List getAllFoStatus() {
		// TODO Auto-generated method stub
		return jobDAO.findAllFoStatus();
	}
	
	public List getAllForStatus(String type){
		return jobDAO.findAllForStatus(type);
		
	}

	@Override
	public List<Job> getAgentProactiveJobs(String agentName) {
		// TODO Auto-generated method stub
		return jobDAO.findAgentProactiveJobs(agentName);
	}

	@Override
	public List<Job> getAgentActiveArchivingJobs(String agentName) {
		// TODO Auto-generated method stub
		return jobDAO.findAgentActiveArchivingJobs(agentName);
	}

	@Override
	public List<Job> getAgentPrintArchivingJobs(String agentName) {
		// TODO Auto-generated method stub
		return jobDAO.findAgentActiveArchivingJobs(agentName);
	}
	
	
	public List<JobSpDocLib> getShareThreshholdJobs(String sharePath){
		
		return jobDAO.findShareThreshholdJobs(sharePath);
	}
	
	public List<JobSpDocLib> getShareAllThreshholdJobs(String sharePath){
		
		return jobDAO.findShareAllThreshholdJobs(sharePath);
	}
	
	public List<Job> getAllActiveImportJobs(){		
		return jobDAO.findAllActiveImportJobs();
	}
	
	public List<Job> getAllActiveJobs(){		
		return jobDAO.findAllActiveJobs();
	}
	
	public List<Job> getAllActiveArchivingJobs(){
		return jobDAO.findAllActiveArchivingJobs();
	}

}
