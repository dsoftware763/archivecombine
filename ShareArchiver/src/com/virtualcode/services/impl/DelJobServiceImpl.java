package com.virtualcode.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.virtualcode.dao.DAOManager;
import com.virtualcode.dao.DelJobDAO;
import com.virtualcode.services.DelJobService;
import com.virtualcode.vo.DelJob;

public class DelJobServiceImpl implements DelJobService  {
    private static final Logger log = LoggerFactory.getLogger(DelJobServiceImpl.class);
 	
    private DelJobDAO delJobDAO;
    private DAOManager daoManager;	
	public void setDaoManager(DAOManager daoManager) {
		this.daoManager = daoManager;
		this.delJobDAO=daoManager.getDelJobDAO();
	}

	@Override
	public DelJob getById(Integer id) {
		// TODO Auto-generated method stub 
		return delJobDAO.findById(id);
	}

	@Override
	public List getByExample(DelJob instance) {
		// TODO Auto-generated method stub
		return delJobDAO.findByExample(instance);
	}

	@Override
	public List getByProperty(String propertyName, Object value) {
		// TODO Auto-generated method stub
		return delJobDAO.findByProperty(propertyName, value);
	}

	@Override
	public List getByActionType(Object actionType) {
		// TODO Auto-generated method stub
		return delJobDAO.findByActionType(actionType);
	}

	@Override
	public List getByName(Object name) {
		// TODO Auto-generated method stub
		return delJobDAO.findByName(name);
	}

	@Override
	public List getAll() {
		// TODO Auto-generated method stub
		return delJobDAO.findAll();
	}

	@Override
	public void save(DelJob transientInstance) {
		// TODO Auto-generated method stub
		delJobDAO.save(transientInstance);
	}

	@Override
	public void delete(DelJob persistentInstance) {
		// TODO Auto-generated method stub
		delJobDAO.delete(persistentInstance);
	}

	@Override
	public DelJob merge(DelJob detachedInstance) {
		// TODO Auto-generated method stub
		return delJobDAO.merge(detachedInstance);
	}

	@Override
	public void attachDirty(DelJob instance) {
		// TODO Auto-generated method stub
		delJobDAO.attachDirty(instance);
	}

	@Override
	public void attachClean(DelJob instance) {
		// TODO Auto-generated method stub
		delJobDAO.attachClean(instance);
	}
}
