package com.virtualcode.services;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.FilesProcessedSize;

/**
 * A data access object (DAO) providing persistence and search support for
 * FilesProcessedSize entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.vo.FilesProcessedSize
 * @author MyEclipse Persistence Tools
 */

public interface FilesProcessedSizeService {

	
	public void saveFileProcessedSize(FilesProcessedSize transientInstance);

	public void deleteFileProcessedSize(FilesProcessedSize persistentInstance);

	public FilesProcessedSize getById(java.lang.Integer id);

//	public List findByExample(FilesProcessedSize instance);

	public List<FilesProcessedSize> getByProperty(String propertyName, Object value);

	public List<FilesProcessedSize> getByTotalProcessedFiles(Object totalProcessedFiles);

	public List<FilesProcessedSize> getAll();

	}