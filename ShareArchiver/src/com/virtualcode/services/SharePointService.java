package com.virtualcode.services;

import java.util.List;

public interface SharePointService {
	
	public List getSiteList();
	
	public List getSiteListByURL(String webServiceURL);
	
	public List getSiteContentTypes(String webServiceURL, String siteUrl);
	
	public List getLibraryList(String sitePathOrSiteURL, String webServiceURL);
	
	public List getSubLibraryList(String guid, String sitePathOrSiteURL, String webServiceURL);
	
	public String getSiteId(String sitePathOrSiteURL, String webServiceURL);
	
	public String getLibraryId(String sitePathOrSiteURL,String libraryName, String webServiceURL);
	
	public String getSubLibraryId(String guid, String sitePathOrSiteURL,String libraryName, String webServiceURL);
	
	public List getLibraryNodesList(String sitePathOrSiteURL, String webServiceURL);
	
	public List getSubLibraryNodesList(String guid, String sitePathOrSiteURL, String webServiceURL);
	
	public List getLibraryNodesList(String sitePathOrSiteURL, List<String> paths, String webServiceURL);
	
	public List getSubLibraryNodesList(String guid, String sitePathOrSiteURL, List<String> paths, String webServiceURL);

	public List getSiteNodesList();
	
	public List getSiteNodesListByURL(String webServiceURL);

	String getLibrarySiteUrl(String sitePathOrSiteURL, String libraryName, String webServiceURL);

	String getSubLibrarySiteUrl(String guid, String sitePathOrSiteURL,	String libraryName, String webServiceURL);
	
	public boolean testSPAgentService(String webServiceUrl);
	
}
