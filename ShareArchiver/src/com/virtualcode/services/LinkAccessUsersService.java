package com.virtualcode.services;

import java.util.List;
import java.util.Set;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.LinkAccessUsers;

/**
 * A data access object (DAO) providing persistence and search support for
 * LinkAccessUsers entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.vo.LinkAccessUsers
 * @author MyEclipse Persistence Tools
 */

public interface LinkAccessUsersService {


	public void saveLinkAccessUsers(LinkAccessUsers transientInstance);

	public void deleteLinkAccessUsers(LinkAccessUsers persistentInstance);

	public LinkAccessUsers getLinkAccessUsersById(java.lang.Integer id);

	public List<LinkAccessUsers> getLinkAccessUsersByExample(LinkAccessUsers instance);

	public List<LinkAccessUsers> getLinkAccessUsersByProperty(String propertyName, Object value);

	public LinkAccessUsers getLinkAccessUsersByRecipient(String recipient);

	public List<LinkAccessUsers> getLinkAccessUsersByPassword(Object password);

	public List<LinkAccessUsers> getLinkAccessUsersBySender(Object sender);

	public List<LinkAccessUsers> getAll();
	
	public boolean recipientExists(String recipient);
	
	public boolean login(String username, String password);
	

}