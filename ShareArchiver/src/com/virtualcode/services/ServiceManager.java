package com.virtualcode.services;

import com.virtualcode.repository.AnalysisService;
import com.virtualcode.repository.ArchiveDateRunnerService;
import com.virtualcode.repository.FileExplorerService;
import com.virtualcode.repository.FileSizeRunnerService;
import com.virtualcode.repository.SearchService;
import com.virtualcode.repository.UploadDocumentService;
import com.virtualcode.repository.UserActivityService;


public class ServiceManager {
	
	private AgentService agentService;

	private DocumentClassifyService documentClassifyService;
	
	
    private UserService userService;
	
	private RoleService roleService;
	
	private UserPermissionsService userPermissionsService;
	
	private RolePermissionsService rolePermissionsService;
	
	private JobService jobService;
	
	private PolicyService policyService;
	
	
	private SearchService searchService;
	
	private AnalysisService analysisService;
	
	private DriveLettersService driveLettersService;
	
	private FileExplorerService fileExplorerService;
	
	private UploadDocumentService uploadDocumentService;
	
    private SystemCodeService systemCodeService;
	
	private JobStatisticsService jobStatisticsService;
	
	private JobStatusService jobStatusService;
	
	private NodePermissionJobService nodePermissionJobService;
	
	private ErrorCodeService errorCodeService;
	
	private AgentManagementService agentManagementService;
	
	private SharePointService sharePointService;
	
	private RepoStatsService repoStatsService;
	
	private UserActivityService userActivityService;
	
	private SystemAlertsService systemAlertsService;
	
	private ShareAnalysisStatsService shareAnalysisStatsService;
	
	private PageRightsService pageRightsService;
	
	private MailService mailService;
	
	private RoleFeatureService roleFeatureService;
	
	private UserPreferencesService userPreferencesService;
	
	private DelJobService delJobService;
	private DelJobFiledetailsService delJobFiledetailsService;
	private DelPolicyService delPolicyService;

	private FilesProcessedSizeService filesProcessedSizeService;
	
	private FileSizeRunnerService fileSizeRunnerService;
	
	private LinkAccessUsersService linkAccessUsersService;
	
	private LinkAccessDocumentsService linkAccessDocumentsService;
	

	
	public DelJobService getDelJobService() {
		return delJobService;
	}

	public void setDelJobService(DelJobService delJobService) {
		this.delJobService = delJobService;
	}

	public DelJobFiledetailsService getDelJobFiledetailsService() {
		return delJobFiledetailsService;
	}

	public void setDelJobFiledetailsService(
			DelJobFiledetailsService delJobFiledetailsService) {
		this.delJobFiledetailsService = delJobFiledetailsService;
	}

	public DelPolicyService getDelPolicyService() {
		return delPolicyService;
	}

	public void setDelPolicyService(DelPolicyService delPolicyService) {
		this.delPolicyService = delPolicyService;
	}

//	private GroupGuardianService groupGuardianService;
	
	private DataGuardianService dataGuardianService;
	
	private LdapGroupsService ldapGroupsService;
	
	private ArchiveDateRunnerService archiveDateRunnerService;
	
	private ActiveArchiveReportService activeArchiveReportService;
	
	public RepoStatsService getRepoStatsService() {
		return repoStatsService;
	}

	public void setRepoStatsService(RepoStatsService repoStatsService) {
		this.repoStatsService = repoStatsService;
	}

	public AgentService getAgentService() {
		return agentService;
	}

	public void setAgentService(AgentService agentService) {
		this.agentService = agentService;
	}

	public DocumentClassifyService getDocumentClassifyService() {
		return documentClassifyService;
	}

	public void setDocumentClassifyService(DocumentClassifyService documentClassifyService) {
		this.documentClassifyService = documentClassifyService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public RoleService getRoleService() {
		return roleService;
	}

	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}

	public UserPermissionsService getUserPermissionsService() {
		return userPermissionsService;
	}

	public void setUserPermissionsService(UserPermissionsService userPermissionsService) {
		this.userPermissionsService = userPermissionsService;
	}

	public RolePermissionsService getRolePermissionsService() {
		return rolePermissionsService;
	}

	public void setRolePermissionsService(	RolePermissionsService rolePermissionsService) {
		this.rolePermissionsService = rolePermissionsService;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	public PolicyService getPolicyService() {
		return policyService;
	}

	public void setPolicyService(PolicyService policyService) {
		this.policyService = policyService;
	}

	public SearchService getSearchService() {
		return searchService;
	}

	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}

	public AnalysisService getAnalysisService() {
		return analysisService;
	}

	public void setAnalysisService(AnalysisService analysisService) {
		this.analysisService = analysisService;
	}

	public DriveLettersService getDriveLettersService() {
		return driveLettersService;
	}

	public void setDriveLettersService(DriveLettersService driveLettersService) {
		this.driveLettersService = driveLettersService;
	}

	public FileExplorerService getFileExplorerService() {
		return fileExplorerService;
	}

	public void setFileExplorerService(FileExplorerService fileExplorerService) {
		this.fileExplorerService = fileExplorerService;
	}

	public UploadDocumentService getUploadDocumentService() {
		return uploadDocumentService;
	}

	public void setUploadDocumentService(UploadDocumentService uploadDocumentService) {
		this.uploadDocumentService = uploadDocumentService;
	}
	
	
	public JobStatisticsService getJobStatisticsService() {
		return jobStatisticsService;
	}

	public void setJobStatisticsService(JobStatisticsService jobStatisticsService) {
		this.jobStatisticsService = jobStatisticsService;
	}

	public JobStatusService getJobStatusService() {
		return jobStatusService;
	}

	public void setJobStatusService(JobStatusService jobStatusService) {
		this.jobStatusService = jobStatusService;
	}

	public NodePermissionJobService getNodePermissionJobService() {
		return nodePermissionJobService;
	}

	public void setNodePermissionJobService(
			NodePermissionJobService nodePermissionJobService) {
		this.nodePermissionJobService = nodePermissionJobService;
	}

	public SystemCodeService getSystemCodeService() {
		return systemCodeService;
	}

	public void setSystemCodeService(SystemCodeService systemCodeService) {
		this.systemCodeService = systemCodeService;
	}

	public ErrorCodeService getErrorCodeService() {
		return errorCodeService;
	}

	public void setErrorCodeService(ErrorCodeService errorCodeService) {
		this.errorCodeService = errorCodeService;
	}

	public AgentManagementService getAgentManagementService() {
		return agentManagementService;
	}

	public void setAgentManagementService(
			AgentManagementService agentManagementService) {
		this.agentManagementService = agentManagementService;
	}

	public SharePointService getSharePointService() {
		return sharePointService;
	}

	public void setSharePointService(SharePointService sharePointService) {
		this.sharePointService = sharePointService;
	}

	public UserActivityService getUserActivityService() {
		return userActivityService;
	}

	public void setUserActivityService(UserActivityService userActivityService) {
		this.userActivityService = userActivityService;
	}

	public SystemAlertsService getSystemAlertsService() {
		return systemAlertsService;
	}

	public void setSystemAlertsService(SystemAlertsService systemAlertsService) {
		this.systemAlertsService = systemAlertsService;
	}

	public ShareAnalysisStatsService getShareAnalysisStatsService() {
		return shareAnalysisStatsService;
	}

	public void setShareAnalysisStatsService(ShareAnalysisStatsService shareAnalysisStatsService) {
		this.shareAnalysisStatsService = shareAnalysisStatsService;
	}

	public PageRightsService getPageRightsService() {
		return pageRightsService;
	}

	public void setPageRightsService(PageRightsService pageRightsService) {
		this.pageRightsService = pageRightsService;
	}

	public MailService getMailService() {
		return mailService;
	}

	public void setMailService(MailService mailService) {
		this.mailService = mailService;
	}

	public UserPreferencesService getUserPreferenceService() {
		return userPreferencesService;
	}

	public void setUserPreferenceService(UserPreferencesService userPreferenceService) {
		this.userPreferencesService = userPreferenceService;
	}

	public void setRoleFeatureService(RoleFeatureService roleFeatureService) {
		this.roleFeatureService = roleFeatureService;
	}

	public RoleFeatureService getRoleFeatureService() {
		return roleFeatureService;
	}

	public FilesProcessedSizeService getFilesProcessedSizeService() {
		return filesProcessedSizeService;
	}

	public void setFilesProcessedSizeService(
			FilesProcessedSizeService filesProcessedSizeService) {
		this.filesProcessedSizeService = filesProcessedSizeService;
	}

	public FileSizeRunnerService getFileSizeRunnerService() {
		return fileSizeRunnerService;
	}

	public void setFileSizeRunnerService(FileSizeRunnerService fileSizeRunnerService) {
		this.fileSizeRunnerService = fileSizeRunnerService;
	}

	public LinkAccessUsersService getLinkAccessUsersService() {
		return linkAccessUsersService;
	}

	public void setLinkAccessUsersService(
			LinkAccessUsersService linkAccessUsersService) {
		this.linkAccessUsersService = linkAccessUsersService;
	}

	public LinkAccessDocumentsService getLinkAccessDocumentsService() {
		return linkAccessDocumentsService;
	}

	public void setLinkAccessDocumentsService(
			LinkAccessDocumentsService linkAccessDocumentsService) {
		this.linkAccessDocumentsService = linkAccessDocumentsService;
	}

//	public GroupGuardianService getGroupGuardianService() {
//		return groupGuardianService;
//	}
//
//	public void setGroupGuardianService(GroupGuardianService groupGuardianService) {
//		this.groupGuardianService = groupGuardianService;
//	}

	public DataGuardianService getDataGuardianService() {
		return dataGuardianService;
	}

	public void setDataGuardianService(DataGuardianService dataGuardianService) {
		this.dataGuardianService = dataGuardianService;
	}

	public LdapGroupsService getLdapGroupsService() {
		return ldapGroupsService;
	}

	public void setLdapGroupsService(LdapGroupsService ldapGroupsService) {
		this.ldapGroupsService = ldapGroupsService;
	}

	public ArchiveDateRunnerService getArchiveDateRunnerService() {
		return archiveDateRunnerService;
	}

	public void setArchiveDateRunnerService(
			ArchiveDateRunnerService archiveDateRunnerService) {
		this.archiveDateRunnerService = archiveDateRunnerService;
	}

	public ActiveArchiveReportService getActiveArchiveReportService() {
		return activeArchiveReportService;
	}

	public void setActiveArchiveReportService(
			ActiveArchiveReportService activeArchiveReportService) {
		this.activeArchiveReportService = activeArchiveReportService;
	}
	
}
