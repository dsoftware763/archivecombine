package com.virtualcode.services;

import java.util.List;

import com.virtualcode.vo.ErrorCode;


public interface ErrorCodeService {
	
	public void save(ErrorCode transientInstance);

	public void delete(ErrorCode persistentInstance);

	public ErrorCode getById(java.lang.Integer id);

	public List getByExample(ErrorCode instance);

	public List getAll() ;
		
}