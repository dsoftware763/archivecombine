package com.virtualcode.services;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.LdapGroups;

/**
 * A data access object (DAO) providing persistence and search support for
 * LdapGroups entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.virtualcode.vo.LdapGroups
 * @author MyEclipse Persistence Tools
 */

public interface LdapGroupsService{


	public void saveLdapGroup(LdapGroups transientInstance);

	public void deleteLdapGroup(LdapGroups persistentInstance);

	public LdapGroups getLdapGroupById(java.lang.Integer id);

	public List<LdapGroups> getLdapGroupByExample(LdapGroups instance);

	public List<LdapGroups> getLdapGroupByProperty(String propertyName, Object value);

	public List<LdapGroups> getLdapGroupByGroupName(Object groupName);

	public List<LdapGroups> getLdapGroupByRestrictGroup(Object restrictGroup);

	public List<LdapGroups> getLdapGroupByEnableTranscript(Object enableTranscript);

	public List<LdapGroups> getAll();
	
}