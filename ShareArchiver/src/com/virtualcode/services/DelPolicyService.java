package com.virtualcode.services;

import java.util.List;

import com.virtualcode.vo.DelPolicy;

public interface DelPolicyService {
	public void save(DelPolicy transientInstance);
	
	public void delete(DelPolicy persistentInstance);
	
	public DelPolicy getById( java.lang.Integer id);
	
	public List getByExample(DelPolicy instance);
	
	public List getByProperty(String propertyName, Object value);
	
	public List getByName(Object name);
	
	public List getByModifiedDate(Object modifiedDate);
	
	public List getByAccessDate(Object accessDate);
	
	public List getByArchivalDate(Object archivalDate);
	
	public List getByFileType(Object fileType);
	
	public List getByFileSize(Object fileSize) ;
	
	
	public List getAll() ;
	
	public DelPolicy merge(DelPolicy detachedInstance);
	
	public void attachDirty(DelPolicy instance);
	
	public void attachClean(DelPolicy instance);

}
