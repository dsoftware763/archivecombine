package com.virtualcode.services;
// default package

import java.util.List;
import java.util.Set;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.User;


public interface UserService  {
	   
    
    public void save(User transientInstance) ;
    
	public void delete(User persistentInstance);
    
    public User getById( java.lang.Long id);
        
 
	public List getByEmail(Object email);
	
	public List getByFirstName(Object firstName);
	
	public List getByLastName(Object lastName);
	
	public List getByLockStatus(Object lockStatus);
	
	
	public List getByUsername(Object username);

	public List<User> getAll() ;
	
	public User login(String username,String password);
 	
}