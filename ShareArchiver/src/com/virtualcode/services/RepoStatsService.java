package com.virtualcode.services;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.RepoStats;

/**
 * A data access object (DAO) providing persistence and search support for
 * RepoStats entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.virtualcode.vo.RepoStats
 * @author MyEclipse Persistence Tools
 */

public interface RepoStatsService {

	public void saveRepoStats(RepoStats transientInstance);

	public void deleteRepoStats(RepoStats repoStatus);

	public RepoStats getRepoStatusById(java.lang.Integer id);

	public ArrayList<RepoStats> getRepoStatusByExample(RepoStats repoStatus);

	public ArrayList<RepoStats> getRepoStatusByProperty(String propertyName, Object value);

	public ArrayList<RepoStats> getRepoStatusByRepoPath(Object repoPath);

	public ArrayList<RepoStats> getRepoStatusByTotalFolders(Object totalFolders);

	public ArrayList<RepoStats> getRepoStatusByTotalFiles(Object totalFiles);

	public ArrayList<RepoStats> getRepoStatusByFsDocs(Object fsDocs);

	public ArrayList<RepoStats> getRepoStatusBySpDocs(Object spDocs);

	public ArrayList<RepoStats> getRepoStatusByUsedSpace(Object usedSpace);

	public ArrayList<RepoStats> getRepoStatusByDuplicateDataSize(Object duplicateDataSize);

	public ArrayList<RepoStats> getAll();
	
	public RepoStats getLatestStats();

}