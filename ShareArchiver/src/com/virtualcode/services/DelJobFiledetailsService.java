package com.virtualcode.services;

import java.util.List;

import com.virtualcode.vo.DelJobFiledetails;

public interface DelJobFiledetailsService {
	public void save(DelJobFiledetails transientInstance);
	public void delete(DelJobFiledetails persistentInstance);
	public DelJobFiledetails getById( java.lang.Integer id);
	public List getByExample(DelJobFiledetails instance);
	
	public List getByProperty(String propertyName, Object value);
	public List getByUuid(Object uuid);
	public List getByStatus(Object status);
	public List getByDescription(Object description);
	public List getAll();
	
	public DelJobFiledetails merge(DelJobFiledetails detachedInstance);
	public void attachDirty(DelJobFiledetails instance);
	public void attachClean(DelJobFiledetails instance);
}
