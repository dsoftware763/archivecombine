package com.virtualcode.services;

import java.util.List;

import com.virtualcode.vo.Agent;

public interface AgentService {
	
	public void saveAgent(Agent agent);
	
	public List<Agent> getAllAgents();
	
	public Agent getAgentById(Integer agentId);
	
	public List getAgentByName(String name);
	
	public List getAgentByLogin(String login);
	
	public List getAgentByType(String type);
	
	public boolean updateAgent(Agent agent);
	
	public boolean deleteAgent(Integer agentId);
	
}
