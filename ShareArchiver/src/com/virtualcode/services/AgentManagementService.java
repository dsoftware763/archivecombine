package com.virtualcode.services;

import java.util.List;


import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.vo.JobStatistics;

public interface AgentManagementService {
	    			
	    public  byte[] getJobListForProcessing(String agentName, String actionType);
	    
	    public  List<Job> getJobListObjects(String agentName, String actionType);
		
		public String executionStarted( int jobID);
		 
		public boolean executionCompleted( int jobID, String executionID, int errorCodeID,com.virtualcode.agent.das.archivePolicy.dto.JobStatistics jobStatistics);

//		public int setStatusForScheduledJobs(String agentName);
		public int setStatusForScheduledJobs();
		
		public  List<com.virtualcode.vo.Job> getProactiveArchivingList(String agentName);
		
		public boolean incrProActiveArchCount(Integer jobId,String executionId,String actionType,Integer documentsCount);
				
		public boolean updateHotStatistics(int jobID, String executionID, int errorCodeID,com.virtualcode.agent.das.archivePolicy.dto.JobStatistics jobStatistics);
		
		public List<Integer> getCancelJobs();
		
		public String updateStatus(int jobID, String status);

}
