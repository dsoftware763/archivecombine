package com.virtualcode.filters;


import java.io.IOException;
import java.util.Map;
import java.util.HashMap;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.virtualcode.common.VCSConstants;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.vo.SystemCodeType;

import jespa.security.SecurityProviderException;

public class JespaFilter extends jespa.http.HttpSecurityService implements Filter
{

    public void init(FilterConfig config) throws ServletException
    {
        Map properties = new HashMap();
        Enumeration e = config.getInitParameterNames();
        while (e.hasMoreElements()) {
            String name = (String)e.nextElement();
            properties.put(name, config.getInitParameter(name));
        }
        try {
            super.init(config.getFilterName(), config.getServletContext(), properties);
        } catch (SecurityProviderException spe) {
            throw new ServletException(spe);
        }
    }
    
    
    @Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		HttpServletRequest httpRequest=(HttpServletRequest) request;
		HttpServletResponse httpResponse=(HttpServletResponse) response;
		System.out.println(httpRequest.getRequestURI());
		if(httpRequest.getRequestURI().contains("login.faces")){
			chain.doFilter(request, response);
			return;
		}
		if(httpRequest.getRequestURI().contains("ViewFile.faces")){
			chain.doFilter(request, response);
			return;
		}
		String username = (String)httpRequest.getSession().getAttribute(VCSConstants.SESSION_USER_NAME);
		String authType = (String)httpRequest.getSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE);
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		String authorizeStub = ru.getCodeValue("enable.stub.authorization", SystemCodeType.GENERAL);
		if((username == null  || username.trim().equals("")) && httpRequest.getRequestURI().contains("/downloadfile.jsp")){
				String nodeID = request.getParameter("nodeID");
//				if(nodeID.startsWith("na-")){
					String enableSecurityString = ru.getCodeValue("enable_file_sharing_security", SystemCodeType.GENERAL);
					boolean enableSecurity = false;
					if(enableSecurityString!=null)
						enableSecurity = enableSecurityString.equals("yes")?true:false;
					if(enableSecurity){
						httpResponse.sendRedirect("EmailUserLogin.faces");
						return;
					}
//				}				
		}
		
		//if stub authorization is disabled then dont ask for creds
		if(!(authorizeStub.equalsIgnoreCase("yes")) && (httpRequest.getRequestURI().contains("/SecureGet/") || httpRequest.getRequestURI().contains("/downloadfile.jsp"))){
			chain.doFilter(request, response);
			return;
		}
//		String username = (String)httpRequest.getSession().getAttribute(VCSConstants.SESSION_USER_NAME);
//		String authType = (String)httpRequest.getSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE);
		if(httpRequest.getRequestURI().contains("/SecureGet/") || username == null  || username.trim().equals(""))
			super.doFilter(request, response, chain);	//send request to jespa
		else
			chain.doFilter(request, response);	//if user is in the session then do not send to jespa
		
		return;
	}


	public void destroy()
    {
    }
}
