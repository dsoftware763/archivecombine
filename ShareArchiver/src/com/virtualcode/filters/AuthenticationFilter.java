package com.virtualcode.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;


import com.virtualcode.common.VCSConstants;
import com.virtualcode.repository.UserActivityService;
import com.virtualcode.security.PageAuthorizator;
import com.virtualcode.security.impl.PageAuthorizatorImpl;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.services.SystemCodeService;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.SpringApplicationContext;
import com.virtualcode.vo.SystemCodeType;

public class AuthenticationFilter implements Filter{

	 private FilterConfig filterConfig = null;
	 
	@Override
	public void destroy() {		
		this.filterConfig=null;
  }

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain) throws IOException, ServletException {
		
		 if (filterConfig == null){
	         return;
		 }
		 
		HttpServletRequest httpRequest=(HttpServletRequest) request;
		HttpServletResponse httpResponse=(HttpServletResponse) response;
		
		try{		
			request.setCharacterEncoding("UTF-8");
			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/html;charset=utf-8");
			httpResponse.setHeader("X-UA-Compatible", "IE=EmulateIE8");
			
			HttpSession httpSession=httpRequest.getSession();
			String requestedPage=getRequestedPage(httpRequest);
			
			//System.out.println("-----Requested page: " + requestedPage);
				
			 if (requestedPage==null || requestedPage.equals("")) {
		           httpResponse.sendRedirect("login.faces"); 
		           return; 
		      }
		 
			  if(requestedPage.equalsIgnoreCase("login.faces") || requestedPage.equalsIgnoreCase("logout.faces")
					  || requestedPage.equalsIgnoreCase("EmailUserLogin.faces") || requestedPage.equalsIgnoreCase("fileupload.faces")){
					chain.doFilter(httpRequest, httpResponse);
					return;
				}
			  
			    if(requestedPage.startsWith("rfRes/")){	//to handle styles when not signed in
			    	chain.doFilter(httpRequest, httpResponse);
			    	return;
			    }
			    if(requestedPage.startsWith("javax.faces.resource/")){
			    	chain.doFilter(httpRequest, httpResponse);
			    	return;
			    }
			    if(requestedPage.endsWith("skinning.ecss.faces")){
			    	chain.doFilter(httpRequest, httpResponse);
			    	return;
			    }
				if(httpRequest.getRequestURI().contains("ViewFile.faces")){
					chain.doFilter(request, response);
					return;
				}
				if(httpRequest.getRequestURI().contains("ErrorLink.faces")){
					chain.doFilter(request, response);
					return;
				}
				if(httpRequest.getRequestURI().contains("EmailUserLogin.faces")){
					chain.doFilter(request, response);
					return;
				}
			    
		        HttpSession session = httpRequest.getSession(); 
		        
		        String userName=(String)httpSession.getAttribute(VCSConstants.SESSION_USER_NAME);
		        String userRole=(String)httpSession.getAttribute(VCSConstants.SESSION_USER_ROLE_NAME);
		        //modified to redirect to jespa SSO filter if SSO is enabled in DB
		        String ssoEnabled = "no";
		        String authenticationMode = "1";	// set default to DB only
		        ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
//		        com.virtualcode.services.SystemCodeService systemCodeService = (SystemCodeService) SpringApplicationContext.getBean("systemCodeService");
//		        java.util.List<com.virtualcode.vo.SystemCode> systemCodeList = systemCodeService.getSystemCodeByCodename("authentication.enable.sso");
		        ssoEnabled = ru.getCodeValue("authentication.enable.sso", SystemCodeType.GENERAL);
		        authenticationMode = ru.getCodeValue("authentication.mode", SystemCodeType.GENERAL);
		        
//		        if(systemCodeList!=null && systemCodeList.size()>0){
//		        	ssoEnabled = systemCodeList.get(0).getCodevalue();
//		        }
		        //if sso is enabled, authentication mode is NOT DB-only and the username is null, check if there is a remote user from SSO
		        //matched value changed from 'true' to 'yes'		        
		        if(!(authenticationMode.equals("1")) && ssoEnabled.equals("yes") && (userName == null || userName.trim().equals(""))){
		        	System.out.println("remote user: " + httpRequest.getRemoteUser());
		        	if(httpRequest.getRemoteUser()!=null){
			        	userName = httpRequest.getRemoteUser();
			        	userName = userName.substring(userName.indexOf("\\")+1);
			        	System.out.println("username after removing domain: " + userName);
			        	//treat all AD users as priv users
						httpSession.setAttribute(VCSConstants.SESSION_USER_ROLE_NAME, VCSConstants.ROLE_PRIVILEGED);
						httpSession.setAttribute(VCSConstants.SESSION_USER_AUTH_TYPE, "ad");
			        	httpSession.setAttribute(VCSConstants.SESSION_USER_NAME, userName);
			        	userRole = VCSConstants.ROLE_PRIVILEGED;
		        	}
		        }
		        
		        if(requestedPage.contains("SecureGet")){
		        	System.out.println("remote user for stub access: " + httpRequest.getRemoteUser());
		        	if(httpRequest.getRemoteUser()!=null){
			        	userName = httpRequest.getRemoteUser();
			        	userName = userName.substring(userName.indexOf("\\")+1);
			        	System.out.println("username after removing domain: " + userName);
			        	//treat all AD users as priv users
						httpSession.setAttribute(VCSConstants.SESSION_USER_ROLE_NAME,VCSConstants.ROLE_PRIVILEGED);
						httpSession.setAttribute(VCSConstants.SESSION_USER_AUTH_TYPE, "ad");
			        	httpSession.setAttribute(VCSConstants.SESSION_USER_NAME, userName);
			        	userRole = VCSConstants.ROLE_PRIVILEGED;
		        	
		        	

			   		   WebApplicationContext webAppContext=WebApplicationContextUtils.getWebApplicationContext(httpRequest.getSession().getServletContext());

			           ServiceManager serviceManager=(ServiceManager)webAppContext.getBean("serviceManager");
			           UserActivityService userActivityService=serviceManager.getUserActivityService();
			           userActivityService.userLogout(userName,httpRequest.getRemoteAddr(),httpRequest.getSession().getId(),httpRequest.getSession().getCreationTime());
			   	
		        	
		        	}
		        }
		        //end
		        
//		        if(userName!=null && requestedPage!=null && userName.equalsIgnoreCase("PRIVILEGED") && ! ( requestedPage.trim().equals("") || requestedPage.contains("Search.faces") || requestedPage.contains("downloadfile.jsp") || requestedPage.contains("StubAccess.faces")  ) ){
//		        	
//		        	httpResponse.sendRedirect("login.faces");
//		        }
				
				if(userName==null || userName.trim().equals("")){
					if(!(authenticationMode.equals("1")) && ssoEnabled.equalsIgnoreCase("yes")){
						httpResponse.sendRedirect("/ShareArchiver/SecureLogin/Browse.faces");
						return;
					}
					
					if ("partial/ajax".equals(httpRequest.getHeader("Faces-Request"))) {
					    response.setContentType("text/xml");
					    response.getWriter()
					        .append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
					        .printf("<partial-response><redirect url=\"%s\"></redirect></partial-response>", "login.faces");
					} else {
						httpResponse.sendRedirect("login.faces");
					}					
					return;
				}else if ((userName!=null && !(userName.trim().equals(""))) && requestedPage.equalsIgnoreCase("SecureLogin/Browse.faces")){
					httpResponse.sendRedirect("/ShareArchiver/Browse.faces");
					return;
					//
				}else{
					PageAuthorizator authorizator = new PageAuthorizatorImpl();
					if(authorizator.isAuthorizeRole(requestedPage, userRole)){
						chain.doFilter(httpRequest, httpResponse);
						return;
					}else{
						System.out.println("user: "+ userName +" not authorized for: " + requestedPage);
						
						if ("partial/ajax".equals(httpRequest.getHeader("Faces-Request"))) {
						    response.setContentType("text/xml");
						    response.getWriter()
						        .append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
						        .printf("<partial-response><redirect url=\"%s\"></redirect></partial-response>", "login.faces");
						} else {
							httpResponse.sendRedirect("login.faces");
						}					
						return;
						
					}
				}		        
		       
						
		 }catch(Exception ex){
			ex.printStackTrace();
		 }
		
	}
	
	  private String getRequestedPage(HttpServletRequest httpRequest) { 
	        String url = httpRequest.getRequestURI(); //javax.faces.resource/
	        System.out.println("(orignal)url: "+url);
	        int firstSlash = url.indexOf("/",1); 
	        String requestedPage = null; 
	        if (firstSlash != -1) requestedPage = 
	            url.substring(firstSlash + 1, url.length()); 
	        System.out.println("(page)url: "+requestedPage);
	        return requestedPage; 
	    } 
	 

	@Override
	public void init(FilterConfig config) throws ServletException {
		this.filterConfig=config;
	}

}
