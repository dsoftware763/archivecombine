package com.virtualcode.util;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.PropertyType;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.apache.log4j.Logger;

import com.virtualcode.common.VCSConstants;

public class VCSUtil {
	
	private static final Logger log = Logger.getLogger(VCSUtil.class);
	private static ResourceBundle bundle = VirtualcodeUtil.getResourceBundle();;//ResourceBundle.getBundle("config"); 	    
	public static int getIntValue(String param){
		try{
			return Integer.parseInt(param);
		}catch(Exception ex){
			
		}
		return -1;
	}
	
	public static long getLongValue(String param){
		try{
			return Long.parseLong(param);
		}catch(Exception ex){
			
		}
		return -1;
	}
	
	
	public static float getFloatValue(String param){
		try{
			return Float.parseFloat(param);
		}catch(Exception ex){
			
		}
		return -1;
	}
	
	public static int getAbsolute(int number){
	     if(number >=0){
	        return number;
	     } else{
	        return (~number)+1;
	     }
	}
	
	public static String getUNCPath(String path){
		String unc=null;
		if(path!=null){
			if(path.indexOf("/FS/")>0){
			  unc=path.substring(path.indexOf("/FS/")+4, path.length());
			}else if(path.indexOf("/SP/")>0){
				 unc=path.substring(path.indexOf("/SP/")+4, path.length());
		    }
		}
		if(unc!=null){
//			System.out.println("unc before replacing: "+unc);
			unc="\\\\"+unc.replaceAll("/", "\\\\");
		}
		return unc;
	}
	
	public static boolean pathExists(String parentPath,String childPath){
		 System.out.println(parentPath + ", " + childPath);
		 if(parentPath==null || childPath==null)
			 return false;
		 //just match the strings and return
		 if(childPath.toLowerCase().contains(parentPath.toLowerCase()))
			 return true;		 
		 
		 
		 File possibleParent= new File(parentPath);
		 File maybeChild= new File(childPath);
		 
		 System.out.println("possibleParent: " + possibleParent);
		 System.out.println("maybeChild: " + maybeChild);
		 System.out.println("parent: " + maybeChild.getAbsoluteFile().getParentFile());
		 File parent = maybeChild.getParentFile();
		 while ( parent != null ) {
			 System.out.println("parent: " + parent);
		   if ( parent.equals( possibleParent ))
		     return true;
		   parent = parent.getParentFile();
		 }
		 return false;	 

	 }
	

    public static String getSizeToDisplay(String size) {
    	String sizeToDisplay	=	null;
    	
    	if (size!=null && !size.isEmpty()) {
            double length = Double.parseDouble(size);
            size = String.valueOf(Math.round(Math.ceil(length / 1000d))) ;
            DecimalFormat df = new DecimalFormat("#.##");
            
            length = length / 1000d;
            sizeToDisplay = df.format(length)+" KB";
            //convert to MBs
            if(length>1024d){
           	 length = length / 1024d;
           	 sizeToDisplay = df.format(length)+" MB";
            }
          //convert to GBs
            if(length>1024d){
           	 length = length / 1024d;
           	 sizeToDisplay = df.format(length)+" GB";
            }
        }
    	
    	return sizeToDisplay;
    }
    
	public static String getDomainName(String sambaPath){
		//smb://vcsl30;se8:****!(*#@192.168.30.249/Changes/twst/
		String domainName=null;
		if(sambaPath==null)
			return null;
		try{
			String[] pathsArr=sambaPath.split(";");
			if(pathsArr!=null && pathsArr.length>1 ){
				domainName=pathsArr[0]; 
				domainName= domainName.substring(6);
			}
		}catch(Exception ex){ex.printStackTrace();}
			
		return domainName;		
	}
	
	public static String getUserName(String sambaPath){
		//smb://vcsl30;se8:****!(*#@192.168.30.249/Changes/twst/		
		String userName=null;
		if(sambaPath==null)
			return null;
		try{
			int start=sambaPath.indexOf(";")+1;
			int end=sambaPath.lastIndexOf(":");
	        userName= sambaPath.substring(start,end);		
		}catch(Exception ex){ex.printStackTrace(); }
		return userName;
	}
	
	public static String getUserPassword(String sambaPath){
		//smb://vcsl30;se8:***!(*#@192.168.30.249/Changes/twst/
		String password=null;
		if(sambaPath==null)
			return null;
		  try{
			  
			    String[] pathsArr=sambaPath.split(";");
				if(pathsArr!=null && pathsArr.length>1 ){
					password=pathsArr[1];
					int endIndex=password.length()-1;	
				    if(password.indexOf("@")!=-1){
				    	endIndex=password.indexOf("@");
				    }
					 password= password.substring(password.indexOf(":")+1,endIndex);	
					
				}
					
	       
		  }catch(Exception ex){ex.printStackTrace(); }
		  
	    return password;	  
	}
	
	public static String getIP(String sambaPath){
		//smb://vcsl30;se8:***!(*#@192.168.30.249/Changes/twst/
		
		String ip=null;
		if(sambaPath==null)
			return null;
		try{
			String[] pathsArr=sambaPath.split(";");
			if(pathsArr!=null && pathsArr.length>1 ){
				ip=pathsArr[0];
				ip= ip.substring(ip.indexOf("@")+1,ip.indexOf("/"));	
			}
		}catch(Exception ex){ex.printStackTrace();}
		
		return ip;
	}
	
	
	public static String getDirPathFromSambaPath(String sambaPath){
		//smb://vcsl30;se8:***!(*#@192.168.30.249/Changes/twst/
		
		String path=null;
		if(sambaPath==null)
			return null;
		try{
			String[] pathsArr=sambaPath.split(";");
			if(pathsArr!=null && pathsArr.length>1 ){
				path=pathsArr[1];
				path= path.substring(path.indexOf("@")+1);
				//added for edit job to show paths as UNC in the form \\server\sharename 
				path = "\\\\"+ path.replaceAll("/", "\\\\");
			}else{
				path=pathsArr[0];
				path= path.substring(path.indexOf("@")+1);
				//added for edit job to show paths as UNC in the form \\server\sharename 
				path = "\\\\"+ path.replaceAll("/", "\\\\");
			}
		}catch(Exception ex){ ex.printStackTrace();}
		return path;
	}
	
	
	public static String getSambaPath(String domainName,String userName,String password,String dirPath){
		try{
			dirPath=dirPath.replaceAll("\\\\", "/");
			dirPath=dirPath.replaceAll("//", "");
		}catch(Exception ex){ex.printStackTrace();}
		
//		if(dirPath!=null && ! dirPath.startsWith("/"))
//			dirPath=dirPath+"/";
		
		if(dirPath!=null && ! dirPath.endsWith("/"))
			dirPath=dirPath+"/";
		if(domainName!=null && domainName.trim().length()>0)
			return "smb://"+domainName+";"+userName+":"+password+"@"+dirPath;
		else
			return "smb://"+userName+":"+password+"@"+dirPath;
	}
	
	
	public static void dump(Node node) throws RepositoryException {
        log.debug("dump: Started");
        log.debug("Node Details:");
        log.debug("Path: " + node.getPath());
        log.debug("Name: " + node.getName());
        log.debug("Type: " + node.getPrimaryNodeType().getName());
        log.debug("Identifier: " + node.getIdentifier());
        log.debug("");


        PropertyIterator properties = node.getProperties();
        while (properties.hasNext()) {
            Property property = properties.nextProperty();
            log.debug(property.getPath() + "=");
            if (property.getDefinition().isMultiple()) {
                Value[] values = property.getValues();
                for (int i = 0; i < values.length; i++) {
                    if (i > 0) {
                        log.debug(",");
                    }
                    log.debug(values[i].getString());
                }
            } else {
                if (property.getType() == PropertyType.BINARY) {
                    log.debug("<binary>");
                } else {
                    log.debug(property.getString());
                }

            }
            log.debug("");
        }

        NodeIterator nodes = node.getNodes();
        while (nodes.hasNext()) {
            Node child = nodes.nextNode();
            dump(child);
        }
    }
	
	
	 public static void dumpProperty(Property property) {
	        log.debug("dumpProperty: Started");
	        try {
	            log.debug(property.getPath() + "=");
	            if (property.getDefinition().isMultiple()) {
	                Value[] values = property.getValues();
	                for (int i = 0; i < values.length; i++) {
	                    if (i > 0) {
	                        log.debug(",");
	                    }
	                    log.debug(values[i].getString());
	                }
	            } else {
	                if (property.getType() == PropertyType.BINARY) {
	                    log.debug("<binary>");
	                } else {
	                    log.debug(property.getString());
	                }
	            }
	            log.debug("");

	        } catch (RepositoryException ex) {
	            logError(ex);
	        }
	  }
	 
	 
	 public static String getDateAsString(Date inputDate){
		 
		 java.text.DateFormat formatter = new java.text.SimpleDateFormat(VCSConstants.SEACH_DATE_FORMAT);
		 
		 try{		    
	        
			 return  formatter.format(inputDate);
		 
		 }catch(Exception ex){
			 return null;
		 }
	        
	 }
	 
	 
    public static Date getDate(String dateStr,String format ){
		 
		 java.text.DateFormat formatter = new java.text.SimpleDateFormat(format);
		 
		 try{        
			 return  formatter.parse(dateStr);
		 
		 }catch(Exception ex){
			 return null;
		 }
	        
	 }
	 
	 
	 public static  void logError(Exception ex) {
	        log.error("logError: " + ex.getMessage());

	        StringWriter sw = new StringWriter();
	        PrintWriter pw = new PrintWriter(sw);
	        ex.printStackTrace(pw);
	        log.error(sw.toString()); // stack trace as a string
	  }
	
	 public static Date getCurrentDateTime() {

	        Calendar cal = new GregorianCalendar();
	        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yy-MM-dd HH:mm:ss");
	        
	        Date convertedDate = null;
			try {
				convertedDate = sdf.parse(sdf.format(cal.getTime()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	        // println "current date time: "+convertedDate
	        return convertedDate;
	    }
	 
	 public static String getProperty(String property){
		
		 return bundle.getString(property);
	 }
	 
	 public static boolean validateEmailAddress(String emailAddress) {
            try{
			    Pattern regexPattern;
			    Matcher regMatcher;
			    regexPattern = Pattern.compile("^[(a-zA-Z-0-9-\\_\\+\\.)]+@[(a-z-A-Z-0-9.\\-)]+\\.[(a-zA-Z._)]{2,10}");
			    regMatcher   = regexPattern.matcher(emailAddress);
			    if(regMatcher.matches()){
			       return true;
			    } else {
			       return false;
			    }
            }catch(Exception ex){
            	ex.printStackTrace();
            	return false;
            }
		}
	 
	 public static boolean isPdfViewable(String filename){
		 filename = filename.toLowerCase();
		 if(filename.endsWith(".doc") || filename.endsWith(".docx") || filename.endsWith(".xls") || filename.endsWith(".xlsx")
				|| filename.endsWith(".ppt") || filename.endsWith(".pptx") || filename.endsWith(".pps") || filename.endsWith(".ppsx")
				|| filename.endsWith(".txt") || filename.endsWith(".log")
				|| filename.endsWith(".pdf"))
			 return true;
		 return false;
	 }
	 
	 public static void main(String[] args) throws Exception{
		 SimpleDateFormat sdf=new SimpleDateFormat("MMM d, yyyy");
		 System.out.println("Date: "+sdf.parse("May 8, 2012"));
		 
		 System.out.println("atiq@sharearchiver.com: "+validateEmailAddress("atiq@com.com"));
		 
		
		 
	 }
		
	
}
