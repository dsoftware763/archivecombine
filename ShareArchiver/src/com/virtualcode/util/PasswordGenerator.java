package com.virtualcode.util;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import com.virtualcode.vo.SystemCodeType;


public class PasswordGenerator {

	public static final String ALPHA_NUMERIC_STRING = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789*|!@";
	public static final int CHAR_COUNT = 8;
	
//	"^.*(?=.{6,})(?=.*[a-z])(?=.*[A-Z])(?=.{2,}[\\d])(?=.{2,}[\\W]).*$"
	public static final String REGEX_ENFORCE_CAPS = "(?=.*[A-Z])";
	public static final String REGEX_ENFORCE_NUMS = "(?=.*[\\d])";
	public static final String REGEX_ENFORCE_SPCH = "(?=.*[\\W])";
	
	
	public static String randomAlphaNumeric() {
		int count = CHAR_COUNT;
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
		}
	
	
	public static String generateRegularExpression(){
		
		String regEx = "^.*";
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		if(ru.getCodeValue("password_minimum_length", SystemCodeType.ACCOUNT_POLICY)!=null)
			regEx += "(?=.{"+ru.getCodeValue("password_minimum_length", SystemCodeType.ACCOUNT_POLICY) + ",})";
		else
			regEx += "(?=.{8,})"; 
		
		if(ru.getCodeValue("enforce_caps_in_passwords", SystemCodeType.ACCOUNT_POLICY)!=null 
				&& ru.getCodeValue("enforce_caps_in_passwords", SystemCodeType.ACCOUNT_POLICY).equals("yes"))
			regEx += REGEX_ENFORCE_CAPS;
		
		if(ru.getCodeValue("enforce_numbers_in_passwords", SystemCodeType.ACCOUNT_POLICY)!=null
				&& ru.getCodeValue("enforce_numbers_in_passwords", SystemCodeType.ACCOUNT_POLICY).equals("yes"))
			regEx += REGEX_ENFORCE_NUMS;
		
		if(ru.getCodeValue("enforce_special_chars_in_passwords", SystemCodeType.ACCOUNT_POLICY)!=null
				&& ru.getCodeValue("enforce_special_chars_in_passwords", SystemCodeType.ACCOUNT_POLICY).equals("yes"))
			regEx += REGEX_ENFORCE_SPCH;
		
		regEx += ".*$";
		System.out.println("final RegEx: " + regEx);
		
		return regEx;
	}
	
	public static List<String> generateErrorMessages(){
		
		List<String> errorMessages = new ArrayList<String>();
		
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		if(ru.getCodeValue("password_minimum_length", SystemCodeType.ACCOUNT_POLICY)!=null)
			errorMessages.add("Minimum Password length is " + ru.getCodeValue("password_minimum_length", SystemCodeType.ACCOUNT_POLICY));
		else
			errorMessages.add("Minimum Password length is 8"); 
		
		if(ru.getCodeValue("enforce_caps_in_passwords", SystemCodeType.ACCOUNT_POLICY)!=null
			&& ru.getCodeValue("enforce_caps_in_passwords", SystemCodeType.ACCOUNT_POLICY).equals("yes"))
			errorMessages.add("Password must contain Capital Letter(s)");
		
		if(ru.getCodeValue("enforce_numbers_in_passwords", SystemCodeType.ACCOUNT_POLICY)!=null
				&& ru.getCodeValue("enforce_numbers_in_passwords", SystemCodeType.ACCOUNT_POLICY).equals("yes"))
			errorMessages.add("Password must contain Number(s)");
		
		if(ru.getCodeValue("enforce_special_chars_in_passwords", SystemCodeType.ACCOUNT_POLICY)!=null
				&& ru.getCodeValue("enforce_special_chars_in_passwords", SystemCodeType.ACCOUNT_POLICY).equals("yes"))
			errorMessages.add("Password must contain Special Character(s)");
		
		return errorMessages;
	}

//	public final class SessionIdentifierGenerator
//	{
//
//	  private SecureRandom random = new SecureRandom();
//
//	  public String nextSessionId()
//	  {
//	    return new BigInteger(130, random).toString(32);
//	  }
//
//	}
//
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
//		SecureRandom random = new SecureRandom();		 
//		String pwd = new BigInteger(43, random).toString(32);
//		System.out.println(pwd);
//		
//		int num = 40;
//		for(int i = num; i < 55 ;  i ++)
//		{
//			pwd = new BigInteger(i, random).toString(32).toUpperCase();
//			System.out.println(pwd +" for number " + i);
//		}
		
//		KeyGenerator keygenerator;
//		try {
//			keygenerator = KeyGenerator.getInstance("DES");
//			SecretKey myDesKey = keygenerator.generateKey();
//			System.out.println(myDesKey.getFormat());
//		} catch (NoSuchAlgorithmException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
//		System.out.println(randomAlphaNumeric());
//		System.out.println(randomAlphaNumeric());
//		System.out.println(randomAlphaNumeric());
//		System.out.println(randomAlphaNumeric());
//		System.out.println(randomAlphaNumeric());
//		System.out.println(randomAlphaNumeric());
		
		//match RE
		String pattern = "^.*(?=.{6,})(?=.*[a-z])(?=.*[A-Z])(?=.{2,}[\\d])(?=.{2,}[\\W]).*$";
		System.out.println("1sqqAq2!".matches(pattern));
		

	}

}
