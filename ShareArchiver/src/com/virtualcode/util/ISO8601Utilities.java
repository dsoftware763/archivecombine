/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.virtualcode.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;

public class ISO8601Utilities
{
    static final Logger log = Logger.getLogger(ISO8601Utilities.class);
    private static DateFormat m_ISO8601Local =
        new SimpleDateFormat ("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    public static String formatDateTime()
    {
        return formatDateTime (new Date());
    }

    public static String formatDateTime (Date date)
    {
        if (date == null) {
            return formatDateTime (new Date());
        }

        // format in (almost) ISO8601 format
        String dateStr = m_ISO8601Local.format (date);
        log.debug("Formatted as: " + dateStr);
        int i = dateStr.length()-2;
        // remap the timezone from 0000 to 00:00 (starts at char 22)
        return dateStr.substring (0, i)
            + ":" + dateStr.substring (i);
    }
}