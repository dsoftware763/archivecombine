/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.virtualcode.services.DriveLettersService;
import com.virtualcode.services.JobService;
import com.virtualcode.services.LinkAccessDocumentsService;
import com.virtualcode.services.SystemCodeService;
import com.virtualcode.vo.DriveLetters;
import com.virtualcode.vo.LinkAccessDocuments;
import com.virtualcode.vo.SystemCode;
import com.virtualcode.vo.SystemCodeType;
import com.virtualcode.common.UpdateNodePermissionJob;

/**
 *
 * @author AtiqurRehman
 */
public class VirtualcodeUtil {

    static final Logger log = Logger.getLogger(VirtualcodeUtil.class);
    static final String CONNECTION_URL = "dbConnectionUrl";
    static final String CONNECTION_DRIVER = "driverClassName";
    static final String SSO_PROPERTY_PATH= "WEB-INF/jespafilter.prp";
    
    static final String COMPLIANCE_MODE = "saComplianceMode";
    
    private String connectionUrl;
    private String connectionDriver;
    private String complianceMode;
    
    private static VirtualcodeUtil object;
    
    
    public synchronized static  VirtualcodeUtil getInstance(){
        if(object==null)
            object=new VirtualcodeUtil();
       
        return object;
    }
    
    private VirtualcodeUtil(){
    	
//        File file = new File("C:\\Apache Software Foundation\\apache-tomcat-6.0.32\\config");
//        ResourceBundle bundle = ResourceBundle.getBundle("config");
//        try{
//        URL[] urls = {file.toURI().toURL()};
//        ClassLoader loader = new URLClassLoader(urls);  
//        bundle = ResourceBundle.getBundle("config", Locale.getDefault(), loader);
//    	}catch(Exception ex){}
    
        ResourceBundle bundle = getResourceBundle();
        connectionUrl=bundle.getString(CONNECTION_URL);
        connectionUrl=connectionUrl.substring(0, connectionUrl.indexOf("?"));
        connectionUrl+="?user="+bundle.getString("dbUserName")+"&password="+bundle.getString("dbPassword");
        System.out.println("Connection Url-->"+connectionUrl);
        connectionDriver = bundle.getString(CONNECTION_DRIVER);
        
        //for compliance mode
        complianceMode = bundle.getString(COMPLIANCE_MODE);
    }

    public static Properties convertResourceBundleToProperties(ResourceBundle resource) {
        Properties properties = new Properties();

        try {

            Enumeration<String> keys = resource.getKeys();
            while (keys.hasMoreElements()) {
                String key = keys.nextElement();
                properties.put(key, resource.getString(key));
            }
        } catch (Exception ex) {
            log.error("Error occured while converting Resourcebundle to properties. Err: " + ex.toString());

        }

        return properties;
    }
    
    public void updateSSOProperties(String path){
        
        Properties ssoProperties=loadPropertiesFromDB(SystemCodeType.JESPA);
        FileOutputStream out;
        try {
            out = new FileOutputStream(path);
            ssoProperties.store(out, "Updated on "+new Date());
            out.close();
        }catch (IOException ex) {
            log.error("ProperFile DoesNot exsist : "+ ex);
        }
        
    }

    public Properties loadPropertiesFromDB(String codeType) {
        Properties properties = new Properties();
        log.debug("loadPropertiesFromDB : started");
      
        Connection con = null;
        Statement st = null;

        try {
            System.out.println(connectionUrl+"-------------------------------------------");
            Class.forName(connectionDriver);
            con = DriverManager.getConnection(connectionUrl);
            st = con.createStatement();
            String SQL_QUERY = "select syscode.id, syscode.codename, syscode.codevalue, syscode.codetype from system_code syscode where syscode.codetype= '" + codeType + "'";
            log.debug("Query : " + SQL_QUERY);
            ResultSet rs = st.executeQuery(SQL_QUERY);

            while (rs != null && rs.next()) {
                int id = (int) rs.getLong("id");
                String codename = (String) rs.getString("codename");
                String codevalue = (String) rs.getString("codevalue");
                String codetype = (String) rs.getString("codetype");
                String displayName = (String) rs.getString("display_name");
                SystemCode code = new SystemCode(id, codename, codevalue, codetype,displayName);
                properties.put(code.getCodename(), code.getCodevalue());
            }
            rs.close();
        } catch (SQLException e) {
            log.error("Error while execution: " + e);
        } catch (ClassNotFoundException cE) {
            log.error("Error while execution: " + cE);
        } finally {
            try {
                st.close();
                con.close();
            } catch (SQLException ex) {
                log.error("Error while closing st,con: " + ex);
            }

        }

        return properties;
    }
    
    
    
    /**
     * 
     * @param job
     * @return 
     */
    
    public String getQueryParam(String input){
        if(input!=null && input.length()>0)
            return input;
//              return "'"+input+"'";
        else
            return "NULL";
    }
    public int addUpdatePermissionJobToQueue(UpdateNodePermissionJob job) {
        
        log.debug("addUpdatePermissionJobToQueue : started");
        Connection con = null;
        Statement st = null;
        PreparedStatement pst = null;
        int update=0;

        try {
        	System.out.println(connectionUrl+"-------------------------------------------");
            Class.forName(connectionDriver);
            con = DriverManager.getConnection(connectionUrl);
            st = con.createStatement();
//            String SQL_QUERY = "insert into node_permission_job (name, command, status, registerdate,secureinfo,sids, denysids, sharesids, denysharesids, isrecursive) values "+
//             "('"+job.getName()+"','"+job.getCommand()+"',"+job.getStatus()+",'"+job.getRegisterDate()+"'"
//                    + ", '"+job.getSecureInfo()+"', "+getQueryParam(job.getSids())+", "+getQueryParam(job.getDenySids())+", "+getQueryParam(job.getShareSids())+","+getQueryParam(job.getDenyShareSids())+", "+job.isRecursive()+")";

            String SQL_QUERY = "insert into node_permission_job (name, command, status, registerdate,secureinfo,sids, denysids, sharesids, denysharesids, isrecursive) values "+
             "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            log.debug("Query : " + SQL_QUERY);
            pst = con.prepareStatement(SQL_QUERY);
            pst.setString(1, job.getName());                // name
            pst.setString(2, job.getCommand());             // command
            pst.setInt(3, job.getStatus());                 // status
            pst.setString(4, job.getRegisterDate());        //registerdate
            pst.setString(5, job.getSecureInfo());          //secureinfo
            pst.setString(6, getQueryParam(job.getSids())); //sids
            pst.setString(7, getQueryParam(job.getDenySids())); //deny sids
            pst.setString(8, getQueryParam(job.getShareSids())); //shared sids
            pst.setString(9, getQueryParam(job.getDenyShareSids())); //deny share sids
            pst.setInt(10, job.isRecursive()); //is recursive

            update =  pst.executeUpdate();//st.executeUpdate(SQL_QUERY);  

        } catch (SQLException e) {
            log.error("Error while execution: " + e);
        } catch (ClassNotFoundException cE) {
            log.error("Error while execution: " + cE);
        } finally {
            try {
                st.close();
                con.close();
            } catch (SQLException ex) {
                log.error("Error while closing st,con: " + ex);
            }

        }

        return update;
    }
    
    /**
     * 
     * @param jobStatus
     * @return 
     */
     public ArrayList getUpdatePermissionJobs(int jobStatus) {
        ArrayList<UpdateNodePermissionJob> jobs=new ArrayList<UpdateNodePermissionJob>();
        log.debug("getUpdatePermissionJobs : started");
       

        Connection con = null;
        Statement st = null;

        try {
            Class.forName(connectionDriver);
            con = DriverManager.getConnection(connectionUrl);
            st = con.createStatement();
            String SQL_QUERY = "select id,name, command, status, registerdate,secureinfo,sids, denysids, sharesids, denysharesids, isrecursive from node_permission_job where status= '" + jobStatus + "'";
            log.debug("Query : " + SQL_QUERY);
            ResultSet rs = st.executeQuery(SQL_QUERY);

            while (rs != null && rs.next()) {
//id, name, command, status, registerdate, starteddate, finisheddate                
                UpdateNodePermissionJob job=new UpdateNodePermissionJob();
                job.setId(rs.getLong("id"));
                job.setName(rs.getString("name"));
                job.setCommand(rs.getString("command"));
                job.setStatus(rs.getInt("status"));
                job.setRegisterDate(rs.getString("registerdate"));
                
                job.setSecureInfo(rs.getString("secureinfo"));
                job.setSids(rs.getString("sids"));
                job.setDenySids(rs.getString("denysids"));
                job.setShareSids(rs.getString("sharesids"));
                job.setDenyShareSids(rs.getString("denysharesids"));
                job.setRecursive(rs.getInt("isrecursive"));
                
                jobs.add(job);
            }
            rs.close();
        } catch (SQLException e) {
            log.error("Error while execution: " + e);
        } catch (ClassNotFoundException cE) {
            log.error("Error while execution: " + cE);
        } finally {
            try {
                st.close();
                con.close();
            } catch (SQLException ex) {
                log.error("Error while closing st,con: " + ex);
            }

        }

        return jobs;
    }
     
public int udpateUpdatePermissionJob(UpdateNodePermissionJob job) {
        
        log.debug("udpateUpdatePermissionJobToQueue : started");
        Connection con = null;
        Statement st = null;
        int update=0;

        try {
            Class.forName(connectionDriver);
            con = DriverManager.getConnection(connectionUrl);
            st = con.createStatement();
            String SQL_QUERY = "update node_permission_job set status="+job.getStatus()+", startdate='"+job.getStartedDate()+"', finisheddate='"+job.getFinishedDate()+"' where id="+job.getId()+"";

            log.debug("Query : " + SQL_QUERY);
            update = st.executeUpdate(SQL_QUERY);

        } catch (SQLException e) {
            log.error("Error while execution: " + e);
        } catch (ClassNotFoundException cE) {
            log.error("Error while execution: " + cE);
        } finally {
            try {
                st.close();
                con.close();
            } catch (SQLException ex) {
                log.error("Error while closing st,con: " + ex);
            }

        }

        return update;
    }

	public String getURL(String id){
		com.virtualcode.services.LinkAccessDocumentsService linkAccessDocumentsService = (LinkAccessDocumentsService) SpringApplicationContext.getBean("LinkAccessDocumentsService");
		List<LinkAccessDocuments> docList =  linkAccessDocumentsService.getLinkAccessDocumentsByProperty("identifier", new Long(id));
		if(docList!=null && docList.size()>0){			
			return docList.get(0).getUuidDocuments();
		}
		
		return null;
	}
	
	public String SHAsum(byte[] convertme) throws NoSuchAlgorithmException{
	    MessageDigest md = MessageDigest.getInstance("SHA-1"); 
	    return byteArray2Hex(md.digest(convertme));
	}

	private String byteArray2Hex(final byte[] hash) {
	    Formatter formatter = new Formatter();
	    for (byte b : hash) {
	        formatter.format("%02x", b);
	    }
	    return formatter.toString();
	}
	
	public DriveLetters getDriveLetterMappings(String remotePath){
		DriveLettersService driveLettersService = (DriveLettersService) SpringApplicationContext.getBean("driveLettersService");
		DriveLetters driveLetter = null;
	    List<DriveLetters> driveLetterList=driveLettersService.getAll();
		if(remotePath.startsWith("\\\\")){
			remotePath = remotePath.substring(2);
			if(remotePath.contains("\\"))
				remotePath = remotePath.substring(0, remotePath.indexOf("\\"));
		}
	      for(DriveLetters letter: driveLetterList){
				String sharePath = letter.getSharePath().toLowerCase();
				if(sharePath.startsWith("\\\\")){					
//					ResourcesUtil ru = ResourcesUtil.getResourcesUtil();				
//					sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase() + "/fs/" + sharePath.substring(2).replaceAll("\\\\", "/");
				}
				

//				System.out.println(sharePath);
				if(sharePath.startsWith("\\\\")){
					sharePath = sharePath.substring(2);
//					if(sharePath.endsWith("\\"))
//						sharePath = sharePath.substring(0, sharePath.indexOf("\\"));
				}

//				if(!remotePath.endsWith("\\"))
//					remotePath = remotePath+ "\\";
				System.out.println(sharePath + ", " + remotePath.toLowerCase());
	      	if(remotePath.toLowerCase().contains(sharePath) || sharePath.contains(remotePath.toLowerCase())){
	      		driveLetter = letter;
	      		break;
	      	}	      	
	      }
		return driveLetter;
	}
	
	public DriveLetters getDriveLetterMappingsForSearch(String remotePath){
		DriveLettersService driveLettersService = (DriveLettersService) SpringApplicationContext.getBean("driveLettersService");
		JobService jobService = (JobService) SpringApplicationContext.getBean("jobService");
		
		
		DriveLetters driveLetter = null;
		List<DriveLetters> tempList=driveLettersService.getAll();
	    List<DriveLetters> driveLetterList = null;//driveLettersService.getAll();
	    driveLetterList	=	jobService.getSpDocLibByDriveLetters(tempList);
	      
	      for(DriveLetters letter: driveLetterList){
				String sharePath = letter.getSharePath().toLowerCase();
				if(sharePath.startsWith("\\\\")){					
//					ResourcesUtil ru = ResourcesUtil.getResourcesUtil();				
//					sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase() + "/fs/" + sharePath.substring(2).replaceAll("\\\\", "/");
				}
				
				if(remotePath.startsWith("\\\\")){
//					remotePath = remotePath.substring(2);
					if(remotePath.endsWith("\\"))
						remotePath = remotePath.substring(0, remotePath.indexOf("\\"));
				}

//				if(!remotePath.endsWith("\\"))
//					remotePath = remotePath+ "\\";
				System.out.println("Matching: "+sharePath + ", " + remotePath.toLowerCase()+"---------------------");
	      	if(remotePath.toLowerCase().contains(sharePath) || sharePath.contains(remotePath.toLowerCase())){
	      		driveLetter = letter;
	      		break;
	      	}
	      }
		return driveLetter;
	}
	
	public boolean getComplianceMode(){
		return (complianceMode!=null && complianceMode.equalsIgnoreCase("on"))?true:false;
	}
	
	public static ResourceBundle getResourceBundle(){
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		String outputPath = "/opt/tomcat/configs/";
		if(ru.getCodeValue("path_for_configs", SystemCodeType.GENERAL)!=null)
			outputPath = ru.getCodeValue("path_for_configs", SystemCodeType.GENERAL);
		
		if(!outputPath.endsWith("/"))
			outputPath +="/";
		File file = new File(outputPath);
		
        ResourceBundle bundle = null;//ResourceBundle.getBundle("config");
        try{
        	bundle = ResourceBundle.getBundle("config");
        }catch (Exception e) {
			// TODO: handle exception
        	System.out.println("Handled exception:" + e.getMessage());
		}
        
        try{
        	URL[] urls = {file.toURI().toURL()};
        	ClassLoader loader = new URLClassLoader(urls);  
        	bundle = ResourceBundle.getBundle("config", Locale.getDefault(), loader);
    	}catch(Exception ex){
    		System.out.println("Handled exception - Error getting resource bundle .." + ex.getMessage());  
    		bundle = ResourceBundle.getBundle("config");
    	}
		return bundle;
	}
     
     
    
}
