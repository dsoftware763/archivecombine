/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.util;

import java.util.ArrayList;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.virtualcode.services.ServiceManager;
import com.virtualcode.services.SystemCodeService;

/**
 *
 * @author YAwar
 */
public class ResourcesUtil {
    
    private static ResourcesUtil ruObj    =   null;
    private static final Logger logger = Logger.getLogger(ResourcesUtil.class);
        
    private ArrayList<com.virtualcode.vo.SystemCode> scList    =   null;
    
    private SystemCodeService systemCodeService;
    private ServiceManager serviceManager;
    
    
    public void setServiceManager(ServiceManager serviceManager) {
		this.serviceManager = serviceManager;
		this.systemCodeService = serviceManager.getSystemCodeService();
	}

	private ResourcesUtil() {
    	
    	
        logger.info("Creating new instance of ResourcesUtil");
        
//        ResourceBundle bundle   =   ResourceBundle.getBundle("config");
//        String connectionUrl    =   bundle.getString("dbConnectionUrl");
//        String connectionDriver =   bundle.getString("driverClassName");
        
     // open/read the application context file
       
        //ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext.("applicationContext.xml");
        //(SystemCodeDAOImpl) ctx.getBean("SystemCodeDAO");
        //com.virtualcode.services.SystemCodeService systemCodeService = new com.virtualcode.services.impl.SystemCodeServiceImpl();
        //com.virtualcode.dao.SystemCodeDAO systemCodeDAO = (com.virtualcode.dao.impl.SystemCodeDAOImpl) ctx.getBean("SystemCodeDAO");
        com.virtualcode.services.SystemCodeService systemCodeService = (SystemCodeService) SpringApplicationContext.getBean("systemCodeService");
        scList = (ArrayList)systemCodeService.getAll();//(ArrayList)systemCodeDAO.findAll();
//        System.out.println("values loaded, size: " + scList.size());
        logger.info("values loaded, size: " + scList.size());
     /*   
        Connection con =null;
        Statement st =null;
        try {
        	
            //Class.forName(connectionDriver);
            //con = DriverManager.getConnection(connectionUrl);
            //st = con.createStatement();
            //String SQL_QUERY = "select * from system_code sc";
            
            logger.debug("Query : " + SQL_QUERY);
            ResultSet rs = st.executeQuery(SQL_QUERY);

            if(rs != null) {
                
                scList  =   new ArrayList<SystemCode>();
                while (rs.next()) {
                    SystemCode sc   =   new SystemCode();
                    
                    sc.setCodename(rs.getString("codename"));
                    sc.setCodevalue(rs.getString("codevalue"));
                    sc.setCodetype(rs.getString("codetype"));
                    sc.setId(rs.getInt("id"));
                    
                    //logger.debug(sc.toString());
                    
                    scList.add(sc);
                }
            }
            rs.close();
            
        } catch (SQLException e) {
            logger.error("Error while execution: " + e);
            
        } catch (ClassNotFoundException cE) {
            logger.error("Error while execution: " + cE);
            
        } finally {
            try {
                if(st!=null)
                    st.close();
                if(con!=null)
                    con.close();
            } catch (SQLException ex) {
                logger.error("Error while closing st,con: " + ex);
            }
        }
        */
        
    }
    
    public static ResourcesUtil getResourcesUtil() {
        if(ruObj==null)
            ruObj   =   new ResourcesUtil();
        
        return ruObj;
    }
    
    public static ResourcesUtil forceValueLoad(){
    	
        ruObj   =   new ResourcesUtil();        
        return ruObj;
    }
    
    public String getCodeValue(String codeName, String codeType) {
//        System.out.println("Resource util - getCodeValue: Finding value codeName:"+ codeName +", with codetype:"+ codeType);
        logger.info("Resource util - getCodeValue: Finding value codeName:"+ codeName +", with codetype:"+ codeType);
        String result   =   null;
        
        for(int i=0; scList!=null && i<scList.size(); i++) {
            String curName  =   scList.get(i).getCodename();
            String curType  =   scList.get(i).getCodetype();
            
            if(curName.equalsIgnoreCase(codeName) && curType.equalsIgnoreCase(codeType)) {
                result  =   scList.get(i).getCodevalue();
                logger.info("Found: "+result);
                break;
            }
        }
        
        if(result==null) {
            logger.error("Couldnt found entry for "+codeName+" ["+codeType+"], in DB");
        }
        
        return result;
    }
    
    public Properties convertResourceUtilToProperties(String codeType) {
        Properties properties = new Properties();

        try {
            
            for(int i=0; scList!=null && i<scList.size(); i++) {
                if(codeType!=null && codeType.equalsIgnoreCase(scList.get(i).getCodetype()))
                    properties.put(scList.get(i).getCodename(), scList.get(i).getCodevalue());
            }
        } catch (Exception ex) {
            logger.error("Error occured while converting Resourcebundle to properties. Err: " + ex.toString());

        }

        return properties;
    }
}
