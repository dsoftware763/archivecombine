package com.virtualcode.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import com.virtualcode.common.PLFormatException;
import com.virtualcode.repository.monitor.RepositoryMonitor;
import com.virtualcode.vo.RepositoryStatus;

public class PLVManager {
	
	private PLVManager(){
		
	}
	
	private static PLVManager  _instance;
	public static PLVManager getInstance(){
		
		if(_instance==null){
			_instance=new PLVManager();
		}
		return _instance;
	}
	
	
	public  String generateLicense(License license){				
		
		String licenseStr=license.getCompanyName()+":"+license.getLicenseType()+":"+license.getParameter()+":"+license.getValidUntil().getTime()+":"+license.getRepoLimitGb();
		licenseStr=StringUtils.encodeUrl(licenseStr);
		return licenseStr;
	}
	
	
	public  long getDaysRemaing(Date validDate){
	
		Calendar validUntil=Calendar.getInstance();
		if(validDate==null){
			return -1;
		}
		validUntil.setTime(validDate);
		
		Calendar todayCal=Calendar.getInstance();
		todayCal.setTime(new Date());
	
		long validms=validUntil.getTimeInMillis();
		long todayms=todayCal.getTimeInMillis();			
		long diff=validms-todayms;
	    diff=Math.round(diff/(1000*60*60*24));
	    diff++;
	   return diff;		 
	
	}
	
	
	public  double getRepositorySize(){
		 
		 RepositoryMonitor repositoryMonitor = RepositoryMonitor.getInstance();
		 RepositoryStatus repositoryStatus = repositoryMonitor.getRepositoryStatus(null);
		 
		 return (repositoryStatus.getTotalArchived()/1024.0);
	}
	
   
	public  boolean isValidLicense(License license){
	  		  
	   if(license==null){
		   return false;
	   }
	   long daysRemaining=getDaysRemaing(license.getValidUntil());
	   double repoSize=getRepositorySize();	 
	   
	   if(license.getParameter().equalsIgnoreCase("time")){	   
		  
		   return daysRemaining>=0;
	   }
	   if(license.getParameter().equalsIgnoreCase("volume")){
		   
		   return repoSize<license.getRepoLimitGb();
	   }
	   
	   if(license.getParameter().equalsIgnoreCase("both")){
	    
		   return (daysRemaining >= 0 && repoSize<license.getRepoLimitGb());
	  } 
	   
	  return false; 
   }
	
	
   public License readLicense(String licenseStr) throws PLFormatException{
		
	    licenseStr=StringUtils.decodeUrl(licenseStr);
	   
	    if(licenseStr==null){
	       throw new PLFormatException("Invalid PL formate");
	    }
		
		System.out.println("Decoded License String : "+licenseStr);
		String[] liceseStrArr=licenseStr.split(":");
		
		if(liceseStrArr!=null && liceseStrArr.length>3){
			License license=new License();
			license.setCompanyName(liceseStrArr[0]);
			license.setLicenseType(liceseStrArr[1]);
			license.setParameter(liceseStrArr[2]);
			if(license.getParameter().equalsIgnoreCase("time") ){
				if(liceseStrArr[3]!=null && ! liceseStrArr[3].equals("null")){
				   license.setValidUntil(new Date(VCSUtil.getLongValue(liceseStrArr[3])));
				}else{
					license.setValidUntil(null);
				}
				license.setRepoLimitGb(-1);
			}
			if(license.getParameter().equalsIgnoreCase("volume")){
				license.setValidUntil(null);
				license.setRepoLimitGb(VCSUtil.getIntValue(liceseStrArr[4]));
			}			
			if(license.getParameter().equalsIgnoreCase("both")){
				license.setValidUntil(new Date(VCSUtil.getLongValue(liceseStrArr[3])));
				license.setRepoLimitGb(VCSUtil.getIntValue(liceseStrArr[4]));
			}			
			return license;			
		 }else{
			 throw new PLFormatException("Invalid PL formate");
		 }
		
	}
   
    public License commenceTrial(License license){
    	
    	if(license.getLicenseType().equalsIgnoreCase("trial")){
    		Calendar validUntil=Calendar.getInstance();
    		validUntil.setTime(new Date());
    		validUntil.add(Calendar.DATE, 15);
    		license.setValidUntil(validUntil.getTime());
    	}
    	return license;
    }

	
	public static  void main(String[] args){
		
		License license=new License();
		license.setCompanyName("ShareArchiver");
		license.setLicenseType("TRIAL");
		license.setParameter("Time");
		Calendar validUntil=Calendar.getInstance();
		validUntil.setTime(new Date());
		validUntil.add(Calendar.YEAR, 1);
		license.setValidUntil(validUntil.getTime());
		license.setRepoLimitGb(1);
		
		PLVManager plvmgr=PLVManager.getInstance();
		String licenseStr=plvmgr.generateLicense(license);
		
		System.out.println("Generated License--->"+licenseStr);
		
		
		try{
		license=plvmgr.readLicense(licenseStr);
		}catch(Exception ex){
			System.out.println("Invalid Licensing format");
		}
		
		System.out.println("Is Valid License-->"+plvmgr.getDaysRemaing(license.getValidUntil()));
		
		
	}
	
   
}
