/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.log4j.Logger;

import com.virtualcode.common.SecureURL;

/**
 *
 * @author Akhnukh
 */
public class AdvSearchUtil {
    
    
    static final Logger log = Logger.getLogger(AdvSearchUtil.class);
    public final static String SECURE_GET_SERVLET_PATH = "/AdvSearchSecureGet/";
    private SecureURL secureURL = null;
    private static AdvSearchUtil _object=null;
    public static final String DD_ERROR = "ERROR";
    
    public AdvSearchUtil(){
        secureURL=SecureURL.getInstance();
    }
    
    public synchronized static AdvSearchUtil getAdvSearchUtil() {
        if (_object == null) {
            _object = new AdvSearchUtil();
        }
        return _object;
    }

    public String prepareURLOfNode(String url, String wspName, String secureInfo) {

        String output   =   null;     
        
        if (url != null && wspName != null) {        
            
            url = "/repository/" + wspName + url;//Skip the CODE Appending functionality.. untill the Agent is sync with it
            
            log.debug("url(actual): " + url);
            url = encodeURL(url);
            log.debug("url(encoded): " + url);

            url = url + "?secureInfo=" + secureInfo;
           // log.debug("Document URL after adding SecureInfo: " + url);

            //encrypt URL
            try {
                url = SECURE_GET_SERVLET_PATH + secureURL.getEncryptedURL(url);
                output = url;
            } catch(Exception e) {//if some error occured to Encrypt URL
                log.error(e);
                output  =   DD_ERROR;
            }
        }
        //log.debug("#####  Output  ##### :" + output);

        return output;
    }
    
    private String encodeURL(String url) {
        if (url == null || url.length() < 2) {
            return url;
        }

        String[] tokens = url.substring(1).split("/");
        String[] correctedTokens = new String[tokens.length];
        int i = 0;

        try {
            for (String t : tokens) {
                correctedTokens[i++] = URLEncoder.encode(t, "utf-8").replaceAll("[+]", "%20");
            }
        } catch (UnsupportedEncodingException ex) {
            log.error(ex);
        }
        StringBuilder stb = new StringBuilder();
        for (String t : correctedTokens) {
            stb.append('/').append(t);
        }
        return stb.toString();
    }
    
    public static void main(String [] ar){
        /**
         * For Testing Purpose only......
         */
        AdvSearchUtil util=new AdvSearchUtil();
        
        //String url="/archive/FS/snapserver.virtualcode.co.uk/test/archive/mydocuments/Sales/Documentation/Marketing/PartnerArea/back-up/images/secureCoding.jpg";
        String url="/archive/FS/10.1.165.217/FolderC/Archive-Test/test-file.txt"; //SecureGet/f9b1fe21d2467eff1c9f32b5e0542ac8ea5eeaece287d548e307a5c3f7106828f0c23415d04a34e2be9612fda7d9bf5d56a916bc5d5912b851909e6060b9295723448a73fc0f6f940567cf8342d9917293793c569b93e20336a3a5ea6c417780093a2d822e837d9126fcb178d5ae9c49beb85adeacfd87eb79b70813e50aa3144cf48f8b5f188336479d197507e87c1d7c3f1960bbb113f6
        
        url="/archive/FS/10.1.165.65/perftest/rbfiles/cryoserver/cryoserver_2/css/cryoserverzentext_old.css";
        url="/archive/FS/10.1.165.217/archive-app/flyer/Cryoserver%20Flierv5.ai";
        url="/archive/FS/10.1.165.65/perftest/performance_test/rbfiles/flyer/Oct2007-Front.pdf";
        //http://10.1.165.60:8080/archive/SecureGet/f9b1fe21d2467eff1c9f32b5e0542ac8ea5eeaece287d548e307a5c3f7106828f0c23415d04a34e28ccab5e0c1bcdb483e4867bfc9343d3a5b040e20a07c83b06f9dd55348003b313cafcd0015a107fb3fb44e2d8a2b2a6f8d60685ac914b1c25765930f0034455148c24f6e46dd846724bf1a15198b8e8abc6e89b547189c9ce70e3282abd3091aebc18e6fe12e8ddfc7bfdaba17d09925a81002d6dec15b17ee07dd4c82f16c4f5d2a0158174d7b27
        //http://10.1.165.60:8080/archive/repository/default/archive/FS/10.1.165.65/perftest/rbfiles/cryoserver/cryoserver_2/css/cryoserverzentext.css
        
        System.out.println("URL : -- "+util.prepareURLOfNode(url, "default", "dXNlcm5hbWU9c2hhcmVhcmNoaXZlciZwYXNzd29yZD1zaGFyZWFyY2hpdmVy"));
        // 1 GB HORCHARD_toFeb2004.PST  SecureGet/f9b1fe21d2467eff1c9f32b5e0542ac8ea5eeaece287d548f200d839a4dd6c00d6644cc0c5de303462f3038ee5e2189d8e976c27b32e3f76be01666af1729df8ae210a68496ab23f48db3fbf108f8eaa617072697c7121d2ec6c19b8d4cb9917b8f7886f5608609072f636aae88e04e46ddfef6dcf8614fbba8bcb15c13cd1ffd04632615b8f77e346f947d6e84388d5a90301d7dac8456b422e620e29452d287ebba1fd6de3c27b9785687decfe409d71837292afb9cff2a33e937ea8094f2f
        //http://10.1.165.60:8080/archive/SecureGet/f9b1fe21d2467eff1c9f32b5e0542ac8ea5eeaece287d548e307a5c3f7106828f0c23415d04a34e2281f5b7aee6a0e72400547a10e3f6b11553fe641a6faf7a2f30502e39d7ef70010c5a9f55e30bdd0eb3290f7a9b317b51ac56ef40c5696d125ca21c61a6a38dc89659fe38de96e5b5dd8792375f4c2f3332fa1a5754dfbd8f5aa0fc31e8c661b777fd20a94a1ccfc639124b4efb6ce7f84c7cec1d961ea48
    
        String userName="VSCL\\akhnukh";
        userName=userName.substring(userName.lastIndexOf("\\")+1);
        System.out.println("userName: "+userName);
        
        //
        String decryptedURL="/repository/default/archive/FS/10.1.165.65/perftest/performance_test/rbfiles/flyer/Oct2007-Front.pdf?secureInfo=dXNlcm5hbWU9c2hhcmVhcmNoaXZlciZwYXNzd29yZD1zaEByM2FyY2ghdjNy";
        String docRepoPath="";
        try{
                docRepoPath=decryptedURL.substring(0,decryptedURL.lastIndexOf("?"));
                docRepoPath=docRepoPath.replaceAll("/repository/default/","");
                
                System.out.println("docRepoPath: "+docRepoPath);
            }catch(Exception ex){
                log.error("Unable to get document repository path from URL, err: "+ex.toString());
            }
    }
    
}
