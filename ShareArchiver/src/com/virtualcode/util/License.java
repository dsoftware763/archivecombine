package com.virtualcode.util;

import java.util.Date;

public class License {

	private String companyName="ShareArchiver";//for all trial licenses
	
	private String licenseType="Trial";//TRIAL,SUBSCRIPTION,PERPETUAL
	
	private String parameter="Time";//Volume,Both
	
	private Date validUntil=null;//only applicable in case of subscription or perpetual
	
	private int repoLimitGb=0;//in gb's
	
	
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getLicenseType() {
		return licenseType;
	}

	public void setLicenseType(String licenseType) {
		this.licenseType = licenseType;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public Date getValidUntil() {
		return validUntil;
	}

	public void setValidUntil(Date validUntil) {
		this.validUntil = validUntil;
	}

	public int getRepoLimitGb() {
		return repoLimitGb;
	}

	public void setRepoLimitGb(int repoLimitGb) {
		this.repoLimitGb = repoLimitGb;
	}

	
}
