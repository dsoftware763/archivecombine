
package com.virtualcode.util;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.TimerTask;

import javax.jcr.LoginException;
import javax.jcr.RepositoryException;
import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.springmodules.jcr.JcrTemplate;

import com.virtualcode.common.UpdateNodePermissionJob;



/**
 *
 * @author Akhnukh
 */
public class SecurityPermissionUtilDaemon extends TimerTask {

	private JcrTemplate jcrTemplate;
    private static File f;
    private static FileChannel channel;
    private static FileLock lock;
    ServletContext sc=null;
    private final static Logger log=Logger.getLogger(SecurityPermissionUtilDaemon.class);
    
    public SecurityPermissionUtilDaemon(ServletContext sContext){
        sc=sContext;
    }
    
    public SecurityPermissionUtilDaemon(JcrTemplate jcrTemplate){
    	
        this.jcrTemplate =jcrTemplate;
    }
    
   public void run() {
	   //System.out.println(":: run  started");
       log.debug(":: run  started");
       start();
       log.debug(":: run  Finshed");
       //System.out.println(":: run  Finshed");
   }
   
    public void executeJobs(){
    	//System.out.println(":: executeJobs  started");
        //log.debug(":: executeJobs  started");
        DateFormat df = SimpleDateFormat.getDateInstance(SimpleDateFormat.LONG);
        VirtualcodeUtil vcUtil=VirtualcodeUtil.getInstance();
        
        ArrayList list=vcUtil.getUpdatePermissionJobs(UpdateNodePermissionJob.QUEUED_JOB);
        //System.out.println(":: getUpdatePermissionJobs List from DB is "+list);
        log.debug(":: getUpdatePermissionJobs List from DB is "+list);
        Iterator itr=list.iterator();
        while(itr.hasNext()){
            UpdateNodePermissionJob job=(UpdateNodePermissionJob)itr.next();
            try {
            	//System.out.println(":: got Job for process JOB "+job.getId());
                log.debug(":: got Job for process JOB "+job.getId());
                job.setStartedDate(DateFormat.getInstance().format(new Date()));
                log.debug(":: going to invoke Job Upodate process");
                //SecurityPermissionsUtil.updateNodeSecurityPermissions(job.getCommand(), sc,job.getSecureInfo(),job.getSids() ,job.getDenySids() ,job.getShareSids(),job.getDenyShareSids(),((job.isRecursive()==1)?true:false));
                SecurityPermissionsUtil.updateNodeSecurityPermissions(job.getCommand(), jcrTemplate,job.getSecureInfo(),job.getSids() ,job.getDenySids() ,job.getShareSids(),job.getDenyShareSids(),((job.isRecursive()==1)?true:false));
                log.debug(":: Job Upodate process finished");
                job.setFinishedDate(DateFormat.getInstance().format(new Date()));
                job.setStatus(UpdateNodePermissionJob.COMPELTED_JOB);
                log.debug(":: Updating Job in DB");
                vcUtil.udpateUpdatePermissionJob(job);
                
                
            } catch (LoginException ex) {
                log.error(ex);
            } catch (RepositoryException ex) {
                log.error(ex);
            } catch (Exception ex) {
                log.error(ex);
            }
        }
        log.debug(":: executeJobs  Finshed");
    }

    public void start() {
        
        log.debug(":: start  Started");
        try {
            f = new File("SecurityPermissionUtilDaemon.lock");
            // Check if the lock exist
            if (f.exists()) {
                // if exist try to delete it
                f.delete();
            }
            // Try to get the lock
            channel = new RandomAccessFile(f, "rw").getChannel();
            lock = channel.tryLock();
            if(lock == null)
            {
                // File is lock by other application
                channel.close();
                throw new RuntimeException("Only 1 instance of MyApp can run.");
            }
			

            try {
                executeJobs();
				unlockFile();
                
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        catch(IOException e)
        {
            throw new RuntimeException("Could not start process.", e);
        }
        log.debug(":: start  Finished");
    }

    public void unlockFile() {
        log.debug(":: unlockFile Started");
        // release and delete file lock
        try {
            if(lock != null) {
                lock.release();
                channel.close();
                f.delete();
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
        log.debug(":: unlockFile Finshed");
    }

}