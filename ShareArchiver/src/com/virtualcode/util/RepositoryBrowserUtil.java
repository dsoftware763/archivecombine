/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.virtualcode.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.virtualcode.common.FileTypeMapper;

/**
 *
 * @author Akhnukh
 */
public class RepositoryBrowserUtil {
    static final Logger log = Logger.getLogger(RepositoryBrowserUtil.class);
    private static RepositoryBrowserUtil repositoryBrowserUtil;
    //private final String MAPPING_FILE = "../webapps/archive/WEB-INF/filetypes.csv";
    private HashMap fileTypes;

    private RepositoryBrowserUtil(){
        init();
    }

    public static RepositoryBrowserUtil getInstance(){
        if(repositoryBrowserUtil==null)
            repositoryBrowserUtil=new RepositoryBrowserUtil();
        return repositoryBrowserUtil;
    }
    
    private void init(){
        fileTypes=new HashMap();
        try
        {
            //ResourceBundle bundle   =   ResourceBundle.getBundle("com.virtualcode.resources.sharearchiver");
            //String mappingFilePath  =   bundle.getString("mappingfile.path");
            ResourcesUtil   ru  =   ResourcesUtil.getResourcesUtil();
            String mappingFilePath  =   "/filetypes.csv";
            
            URL url = getClass().getResource(mappingFilePath);
            File mappingFile = new File(url.getPath().replaceAll("%20", " "));

            
            
            //File mappingFile=new File(mappingFilePath);
            BufferedReader br=new BufferedReader(new FileReader(mappingFile));
            String line="";
            while((br!=null) && (line=br.readLine())!=null){
                String values[]=line.split(",");
                FileTypeMapper fileTypeMapper=new FileTypeMapper(values[0],values[1],values[2]);
                fileTypes.put(fileTypeMapper.getExtention().toLowerCase(), fileTypeMapper);
            }
            br.close();

            FileTypeMapper unknownMapper=new FileTypeMapper("unknown","Unknown Document","unknown.gif");
            fileTypes.put(unknownMapper.getExtention(),unknownMapper);
        }catch(Exception ex){
            log.error("Error while initiating file types mapping, Error: "+ex);
        }

      }
    
   
    public FileTypeMapper getFileTypeMapperFromName(String fileName, boolean isFolderNode){
        FileTypeMapper result=null;
        try{
            if(isFolderNode)
                result= new FileTypeMapper("folder","File Folder","folder.gif");
            else {
                String ext="";
                log.debug("getFileTypeMapperFromName called with name: "+fileName);
                if(fileName!=null){
                    try{
                        ext=fileName.substring(fileName.lastIndexOf(".")+1).toLowerCase();
                    }catch(Exception ex){
                        log.error("Invalid file name unable to find extension EX: "+ex);
                        ext="unknown";
                    }
                    log.debug("Getting FileTypeMapper for Ext: "+ext);
                    result= (FileTypeMapper)fileTypes.get(ext);
                }else
                    result=null;
            }
            
            if(result!=null)
                return result;
            else
                return new FileTypeMapper();

        }catch(Exception ex){
            log.error("Error occoured while getting extention mapping, ex: "+ex);
            return new FileTypeMapper();
        }
    }
}
