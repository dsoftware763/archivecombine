package com.virtualcode.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import javax.naming.NamingException;
import javax.naming.ldap.Control;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.PagedResultsControl;
import javax.naming.ldap.PagedResultsResponseControl;

import jespa.ldap.LdapAccount;
import jespa.ldap.LdapEntry;
import jespa.ldap.LdapSecurityProvider;
import jespa.security.SecurityProviderException;

import org.apache.log4j.Logger;

import com.virtualcode.common.SID;
import com.virtualcode.vo.SystemCodeType;

/**
 *
 * @author AtiqurRehman
 */
public class JespaUtil {

    static final Logger log = Logger.getLogger(JespaUtil.class);
    private static JespaUtil _object;
    Map groupSipMap = Collections.synchronizedMap(new HashMap<String, String>());
    Map usersSidMap = Collections.synchronizedMap(new HashMap<String, String>()); // added to get all Users from LDAP
    Map usersEmailMap = Collections.synchronizedMap(new HashMap<String, String>()); // added to get all Users from LDAP
    //for adding default SIDs from CSV
//    private ResourceBundle bundle = ResourceBundle.getBundle("config");
    private String filePath = "/defaultSids.csv";

    public synchronized static JespaUtil getInstance() {
        if (_object == null) {
            _object = new JespaUtil();
        }
        return _object;
    }

    private JespaUtil() {
        ResourcesUtil   ru  =   ResourcesUtil.getResourcesUtil();
        
    	 Properties configProperties;
         configProperties = ru.convertResourceUtilToProperties(SystemCodeType.ACTIVE_DIR_AUTH);//VirtualcodeUtil.convertResourceBundleToProperties(bundle);
        //configProperties = VirtualcodeUtil.convertResourceBundleToProperties(bundle);
        log.debug("configProperties " + configProperties);
        try {
            groupSipMap = populateGroupsSids(configProperties);
            usersSidMap = populateUsersSids(configProperties);
        } catch (SecurityProviderException ex) {
            log.debug("Error Occured in JESPAUTIL... ");
            ex.printStackTrace();
            log.error(ex);
        } catch (IOException ex) {
            log.debug("Error Occured in JESPAUTIL... ");
            ex.printStackTrace();
            log.error(ex);
        } catch (Exception ex) {
            log.debug("Error Occured in JESPAUTIL... ");
            ex.printStackTrace();
        }
    }

    public String getGroupName(String sid) {
        log.debug("SID Value: " + sid);
        String name = "NO_NAME";
        try {
            name = (String) groupSipMap.get(sid);
        } catch (Exception ex) {
            log.error("Error Occured in JESPAUTIL... ");
            ex.printStackTrace();
        }
        if (name == null) {
            name = "NO_NAME";
//        	name = getUserName(sid);
        }
        log.debug("SID Value Name : " + name);
        return name;
    }
    
    public String getUserName(String sid) {
        log.debug("SID Value: " + sid);
        String name = "NO_NAME";
        try {
            name = (String) usersSidMap.get(sid);
        } catch (Exception ex) {
            log.error("Error Occured in JESPAUTIL... ");
            ex.printStackTrace();
        }
        if (name == null) {
            name = "NO_NAME";
        }
        log.debug("SID Value Name : " + name);
        return name;
    }

    public String getSIDsDescription(String sids) {
        if (sids != null && !(sids.trim().length() == 0)) {
            log.debug("SIDS Value : " + sids);

            String stringDesc = "";
            StringTokenizer tokenizer = new StringTokenizer(sids, ",");
            int i = 0;
            while (tokenizer.hasMoreTokens()) {
                if (i > 0) {
                    stringDesc = stringDesc + ",";
                }
//                stringDesc += getGroupName(tokenizer.nextToken());
                String sid = tokenizer.nextToken();
                String name = getGroupName(sid);
                if(name.equals("NO_NAME")){
                	name = getUserName(sid);
                }
                stringDesc+=name;
                i++;
            }
            return stringDesc;
        } else {
            return sids;
        }
    }

    public String getSIDsDescriptionFromList(ArrayList<SID> sids) {
        String stringDesc = "";
        if (sids != null && (sids.size() > 0)) {
            log.debug("SIDS Value to Get Description : " + sids);
            Iterator it = sids.iterator();
            int i = 0;
            while (it.hasNext()) {
                SID sid = (SID) it.next();
                if (i > 0) {
                    stringDesc = stringDesc + ",";
                }
                stringDesc += sid.getDescription();
                i++;
            }
            return stringDesc;
        } else {
            return stringDesc;
        }
    }
    
    public String getUserEmail(String name) {
        log.debug("user name : " + name);
        String mail = "NO_EMAIL";
        try {
            mail = (String) usersEmailMap.get(name);
        } catch (Exception ex) {
            log.error("Error Occured in JESPAUTIL... ");
            ex.printStackTrace();
        }
        if (mail == null) {
            mail = "NO_EMAIL";
        }
        log.debug("SID Value Name : " + mail);
        return mail;
    }

    public ArrayList<SID> getSIDsObjects(String sids) {
        ArrayList<SID> sidObjects = new ArrayList<SID>();
        if (sids != null && !(sids.trim().length() == 0)) {
            log.debug("String SID's Value : " + sids);
            StringTokenizer tokenizer = new StringTokenizer(sids, ",");
            while (tokenizer.hasMoreTokens()) {
                String sidString = tokenizer.nextToken();
                String sidDesc = getGroupName(sidString);
                if(sidDesc.equals("NO_NAME")){
                	sidDesc = getUserName(sidString);
                	if(!sidDesc.equals("NO_NAME")){
                		sidObjects.add(new SID(sidString, sidDesc, true));	//its a user sid
                	}else{
                		sidObjects.add(new SID(sidString, sidDesc));	//its a group or unknown sid
                	}
                }else{
                	sidObjects.add(new SID(sidString, sidDesc));	//its a group sid
                }
//                sidObjects.add(new SID(sidString, sidDesc));
            }
            return sidObjects;
        } else {
            return sidObjects;
        }
    }

    /**
     * 
     * @param props
     * @param groupName     any name or * for all
     * @param objectClass    Person/Group
     * @throws SecurityProviderException 
     */
    private HashMap populateGroupsSids(Properties props) throws SecurityProviderException, IOException {


        log.debug("PopulateGroupsSids STARTED>>>>. ");

        HashMap sidMap = new HashMap<String, String>();
        LdapSecurityProvider lsp = new LdapSecurityProvider(props);
        try {

            String filter = "(&(objectCategory=Group)(cn=*))";
//            String filter = "(cn=" + input + ")";
            log.debug("filter Value: " + filter);



            InitialLdapContext ctx = (InitialLdapContext) lsp.getProperty("ldap.context", null);
            int pageSize = (props.getProperty("page.size") != null) ? Integer.parseInt(props.getProperty("page.size")) : 5;
            Control[] ctls = new Control[]{new PagedResultsControl(pageSize, Control.CRITICAL)};
            ctx.setRequestControls(ctls);

            byte[] cookie = null;
            do {

                List results = lsp.search(LdapEntry.ALL_ATTRS, filter);
                Iterator eiter = results.iterator();
                String record = "";
                while (eiter.hasNext()) {

                    LdapEntry val = (LdapEntry) eiter.next();
//                log.debug("LdapEntry: " + val);


                    String name = "";
                    Object sid = "";

                    try {
                        name = (String) val.getProperty("name");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        name = "NO_NAME";
                    }
                    try {
                        sid = val.getProperty("objectSid");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        sid = "NO_SID";
                    }

                    record = "Name: " + name;
                    record += ",   SID: " + sid.toString();

                    try {
                        sidMap.put(sid.toString(), name);
                    } catch (Exception ex) {
                        log.debug("Unable to add recordd to map : " + record);
                    }
                    log.debug("record: " + record);
                }

                ctls = ctx.getResponseControls();
                if (ctls != null) {
                    for (int ci = 0; ci < ctls.length; ci++) {
                        if (ctls[ci] instanceof PagedResultsResponseControl) {
                            PagedResultsResponseControl prrc = (PagedResultsResponseControl) ctls[ci];
                            cookie = prrc.getCookie();
                            break;
                        }
                    }

                    ctls = new Control[]{new PagedResultsControl(pageSize, cookie, Control.CRITICAL)};
                    ctx.setRequestControls(ctls);
                }
            } while (cookie != null);


        } catch (NamingException ex) {
            log.error("Error Occured in JESPAUTIL... EX: " + ex);

        } catch (Exception ex) {
            log.error("Error Occured in JESPAUTIL... EX: " + ex);
        } finally {
            lsp.dispose();
        }
        //add all SIDs from default csv SID file
        log.debug("Adding default SIds from csv file");

        HashMap<String, String> defaultSids = getDefaultSids();
        if (defaultSids != null && defaultSids.size() > 0) {
            sidMap.putAll(defaultSids);
            log.debug("default SID size: " + defaultSids.size());
        } else {
            log.error("Default SIds from csv file is empty or null");
        }
        return sidMap;
    }

    private HashMap populateUsersSids(Properties props) throws SecurityProviderException, IOException {


        log.debug("PopulateGroupsSids STARTED>>>>. ");

        HashMap sidMap = new HashMap<String, String>();
        LdapSecurityProvider lsp = new LdapSecurityProvider(props);
        try {

            //(&(objectClass=user)(objectCategory=person))
            String filter = "(&(objectClass=user)(objectCategory=person)(cn=*))";
            
//            String filter = "(cn=" + input + ")";
            log.debug("filter Value: " + filter);



            InitialLdapContext ctx = (InitialLdapContext) lsp.getProperty("ldap.context", null);
            int pageSize = (props.getProperty("page.size") != null) ? Integer.parseInt(props.getProperty("page.size")) : 5;
            Control[] ctls = new Control[]{new PagedResultsControl(pageSize, Control.CRITICAL)};
            ctx.setRequestControls(ctls);

            byte[] cookie = null;
            do {

                List results = lsp.search(LdapEntry.ALL_ATTRS, filter);
                Iterator eiter = results.iterator();
                String record = "";
                while (eiter.hasNext()) {

                    LdapEntry val = (LdapEntry) eiter.next();
//                log.debug("LdapEntry: " + val);


                    String name = "";
                    Object sid = "";
                    String mail="";

                    try {
                        name = (String) val.getProperty("name");
                        System.out.println("for user name:" + name);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        name = "NO_NAME";
                    }
                    try {
                    	//available only for users who have mail boxes
                        mail = (String) val.getProperty("mail");
                        System.out.println("--------mail--------"+ mail);
                        
                    } catch (Exception ex) {
//                        ex.printStackTrace();                    	
                        System.out.println("Error getting email from 'mail'"+ex.getMessage());
                        log.error("Error getting email from 'mail'"+ex.getMessage());
                        mail = null;
                    }
                    if(mail==null || mail.length()<1){	// use as a fallback
	                    try {
	                    	//available only for all users
	                        List<String> mailList = (List<String>) val.getProperty("userprincipalname");
	                        mail = mailList.get(0);
	                        System.out.println("----------userprincipalname-----------"+ mail);
	                        
	                    } catch (Exception ex) {
	//                        ex.printStackTrace();
	                        System.out.println("Error getting email from 'userprincipalname'"+ex.getMessage());
	                        log.error("Error getting email from 'userprincipalname'"+ex.getMessage());
	                        mail = null;
	                    }
                    }
                    
                    if(mail!=null)
                    	usersEmailMap.put(name, mail);
                    
                    try {
                        sid = val.getProperty("objectSid");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        sid = "NO_SID";
                    }

                    record = "Name: " + name;
                    record += ",   SID: " + sid.toString();

                    try {
                        sidMap.put(sid.toString(), name);
                    } catch (Exception ex) {
                        log.debug("Unable to add recordd to map : " + record);
                    }
                    log.debug("record: " + record);
                }

                ctls = ctx.getResponseControls();
                if (ctls != null) {
                    for (int ci = 0; ci < ctls.length; ci++) {
                        if (ctls[ci] instanceof PagedResultsResponseControl) {
                            PagedResultsResponseControl prrc = (PagedResultsResponseControl) ctls[ci];
                            cookie = prrc.getCookie();
                            break;
                        }
                    }

                    ctls = new Control[]{new PagedResultsControl(pageSize, cookie, Control.CRITICAL)};
                    ctx.setRequestControls(ctls);
                }
            } while (cookie != null);


        } catch (NamingException ex) {
            log.error("Error Occured in JESPAUTIL... EX: " + ex);

        } catch (Exception ex) {
            log.error("Error Occured in JESPAUTIL... EX: " + ex);
        } finally {
            lsp.dispose();
        }
        return sidMap;
    }

    public List<String> getGroupsForUser(String username) throws SecurityProviderException, IOException {


        log.debug("PopulateGroupsSids STARTED>>>>. ");

        HashMap sidMap = new HashMap<String, String>();
        ResourcesUtil   ru  =   ResourcesUtil.getResourcesUtil();
        Properties props = ru.convertResourceUtilToProperties(SystemCodeType.ACTIVE_DIR_AUTH);//VirtualcodeUtil.convertResourceBundleToProperties(bundle);
        LdapSecurityProvider lsp = new LdapSecurityProvider(props);
        ArrayList<String> userGroup = new ArrayList<String>();

        try {

        	username += props.getProperty("domain.name");
            log.debug("UserID After appending domain name: " + username);

            String[] attrib = {"tokenGroups","objectSid"};
            lsp = new LdapSecurityProvider(props);
            LdapAccount acct = (LdapAccount) lsp.getAccount(username, attrib);
            log.debug("Account :" + acct);
            
          //adding the default everyone group
            userGroup.add("Everyone");
            
            List lval = (List) acct.getProperty("tokenGroups");
            Iterator liter = lval.iterator();
            while (liter.hasNext()) {
                String sid = ((jespa.util.SID) liter.next()).toString();
                userGroup.add(getGroupName(sid));
            }

            
          
          
          return userGroup;

        }  catch (Exception ex) {
        	ex.printStackTrace();
            log.error("Error Occured in JESPAUTIL user groups... EX: " + ex);
        } finally {
            lsp.dispose();
        }
        return null;
    }    
    
    public Map getGroups() {
        return groupSipMap;
    }
    
    public Map getUsers() {
        return usersSidMap;
    }

    public HashMap<String, String> getDefaultSids() {
        HashMap<String, String> defaultSids = new HashMap<String, String>();
        
        URL url = getClass().getResource(filePath);
           
        File sidFile = new File(url.getPath().replaceAll("%20", " "));
        
        try { 
            BufferedReader br = new BufferedReader(new FileReader(sidFile));
            String line = "";
            while (br != null && (line = br.readLine()) != null) {
                String values[] = line.split(",");
                if(values.length==2){
                   defaultSids.put(values[0], values[1]);
                }
            }
            br.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            log.error("error while loading default SID file" + ex.getMessage());
        } catch (IOException ex) {
            ex.printStackTrace();
            log.error("error while loading default SID file" + ex.getMessage());
        } catch (ArrayIndexOutOfBoundsException ex) {
            ex.printStackTrace();
            log.error("Error: one or more entries are in incorrect format in the csv ");
        }
        //if any error occured after in between adding records, return records added before the error
        if (defaultSids != null && !(defaultSids.isEmpty())) {
            return defaultSids;
        } else {
            return null;
        }
    }

    public static void main(String[] ar) {
        JespaUtil jespaUtil = JespaUtil.getInstance();
        System.out.println(jespaUtil.getGroupName("S-1-5-21-2118986085-1642494700-2308185068-1155"));
        System.out.println(jespaUtil.getGroupName("S-1-5-21-2118986085-1642494700-2308185068-1153"));
        System.out.println(jespaUtil.getGroupName("S-1-5-21-2118986085-1642494700-2308185068-1152"));
        System.out.println(jespaUtil.getGroupName("S-1-5-2-2118986085-1642494700-2308185068-1151"));
        System.out.println(jespaUtil.getGroupName("S-1-5-21-2118986085-1642494700-2308185068-1148"));
        System.out.println(jespaUtil.getSIDsDescription("S-1-5-21-2118986085-1642494700-2308185068-1148"));
        System.out.println(jespaUtil.getSIDsDescription("S-1-5-21-2397042273-2794206915-3762467723-513"));
        System.out.println(jespaUtil.getSIDsDescription(""));
    }
}

