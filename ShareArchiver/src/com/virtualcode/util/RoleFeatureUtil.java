package com.virtualcode.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.virtualcode.services.RoleFeatureService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.vo.RoleFeature;

/**
 *
 * @author AtiqurRehman
 */
public class RoleFeatureUtil {
    
    private static RoleFeatureUtil rfUtilObj    =   null;
    private static final Logger logger = Logger.getLogger(RoleFeatureUtil.class);
    private List<com.virtualcode.vo.RoleFeature> roleFeatureList    =   null;
    private Map<String,RoleFeature> featuresMap=new HashMap<String,RoleFeature>();
      
	private RoleFeatureUtil() {
    	
        logger.info("Creating new instance of RoleFeaturesUtil");

        com.virtualcode.services.ServiceManager serviceManager = (ServiceManager) SpringApplicationContext.getBean("serviceManager");
        RoleFeatureService roleFeatureService=serviceManager.getRoleFeatureService();
        
        roleFeatureList=roleFeatureService.getAll();
        
        for(int i=0;roleFeatureList!=null && i< roleFeatureList.size();i++){
        	featuresMap.put(roleFeatureList.get(i).getName(), roleFeatureList.get(i));
        }       
    }
    
    public static RoleFeatureUtil getRoleFeatureUtil() {
        if(rfUtilObj==null)
        	rfUtilObj   =   new RoleFeatureUtil();
        return rfUtilObj;
    }
    
    public static RoleFeatureUtil forceValueLoad(){
    	 rfUtilObj   =   new RoleFeatureUtil();        
         return rfUtilObj;
    }
    
    public RoleFeature getFeature(String featureName) {
    	return featuresMap.get(featureName);
    }
    
    
}
