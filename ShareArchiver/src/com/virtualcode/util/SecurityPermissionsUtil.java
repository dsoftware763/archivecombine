/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.util;

import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import javax.servlet.ServletContext;

//import org.apache.jackrabbit.j2ee.RepositoryAccessServlet;
import org.apache.log4j.Logger;
import org.springmodules.jcr.JcrTemplate;

import com.virtualcode.common.CustomNodeType;
import com.virtualcode.common.CustomProperty;

/**
 *
 * @author AtiqurRehman
 */
public class SecurityPermissionsUtil {

    static final Logger log = Logger.getLogger(SecurityPermissionsUtil.class);

    //public static void updateNodeSecurityPermissions(String nodeID, ServletContext sc,
    public static void updateNodeSecurityPermissions(String nodeID, JcrTemplate jcrTemplate,
            String secureInfo,
            String sidsString,
            String denySidsString,
            String shareSidsString,
            String denyShareSidsString, boolean isRecursive) throws LoginException, RepositoryException, Exception {
        Session jSession = null;

        boolean update = false;
        Repository rep;
        Node root = null;

        try {
            if (nodeID == null || secureInfo == null) {
                log.debug("nodeID/secureInfo is null " + nodeID);
                log.debug("nodeID : " + nodeID);
                log.debug("secureInfo : " + secureInfo);
            } else {

                //rep = RepositoryAccessServlet.getRepository(sc);
                /*SimpleCredentials creds = SecurityUtil.decodeSecureInfo(secureInfo);
                if (rep == null || creds == null) {
                    throw new LoginException("Repository object isn't initialized");
                }*/
                //jSession = rep.login(creds);
                jSession = jcrTemplate.getSessionFactory().getSession();
                root = jSession.getRootNode();

                // Removing / from begning of nodeID because getNode method generates Error if it has / in start
                if (nodeID.indexOf("/") == 0) {
                    nodeID = nodeID.substring(1);
                    log.debug("After Removing / from Node Path : " + nodeID);
                }

                if (root != null) {
                    root = root.getNode(nodeID);

                    String nodeName = root.getName();
                    String nodeType = root.getPrimaryNodeType().getName();
                    boolean isFolderNode = nodeType.equalsIgnoreCase("nt:folder");

                    log.debug("nodeName " + nodeName);
                    log.debug("nodeType " + nodeType);
                    log.debug("isFolderNode " + isFolderNode);
                    log.debug("isRecursive " + isRecursive);

                    if (isFolderNode) {
                        if (isRecursive) {
                            updateFolderPermissionsRecursively(root, sidsString, denySidsString, shareSidsString, denyShareSidsString);
                            update = true;
                        } else {
                            updateFolderPermissions(root, sidsString, denySidsString, shareSidsString, denyShareSidsString);
                            update = true;
                        }

                    } else {

                        update = updateFileNodePermissions(root, sidsString, denySidsString, shareSidsString, denyShareSidsString);
                    }//else


                } else {
                    log.debug("unable to get repository Root on given path.");
                }
            }//else

            if (!update) {
                throw new Exception("Unable to update");
            }

        } catch (RepositoryException ex) {
            log.error("RepositoryException occured. Ex: " + ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Message while updating Node " + ex);
            throw ex;
        } finally {

            if (jSession != null) {
                try {
                    jSession.save();
                } catch (Exception ex) {
                } finally {

                    jSession.logout();
                }
            }

        }
    }

    public static void updateFolderPermissionsRecursively(Node root,
            String sidsString,
            String denySidsString,
            String shareSidsString,
            String denyShareSidsString) throws LoginException, RepositoryException, Exception {

        if (root == null) {
            log.debug("Node DirId is null so loading empty rows");
        } else {

            log.debug("Updating Recursive for ---------> " + root.getName());
            NodeIterator it = null;
            Node node = null;
            String nodeName = null;
            String nodePath = null;
            String nodeType = null;

            nodeName = root.getName();
            nodeType = root.getPrimaryNodeType().getName();
            nodePath = root.getPath();

            log.debug("nodeName " + nodeName);
            log.debug("nodeType " + nodeType);
            log.debug("nodePath " + nodePath);
            boolean isFolderNode = nodeType.equalsIgnoreCase("nt:folder");
            boolean isFileNode = nodeType.equalsIgnoreCase("nt:file");
            if (isFolderNode) {
                 boolean update = updateFolderNodePermissions(root, sidsString, denySidsString, shareSidsString, denyShareSidsString);
                it = root.getNodes();
                for (; (it != null && it.hasNext());) {
                    try {
                        node = it.nextNode();
                        if (node.getPrimaryNodeType().getName().equalsIgnoreCase("nt:folder") || node.getPrimaryNodeType().getName().equalsIgnoreCase("nt:file")) {
                            updateFolderPermissionsRecursively(node,
                                    sidsString,
                                    denySidsString,
                                    shareSidsString,
                                    denyShareSidsString);
                        } 
                    } catch (Exception ex) {
                        log.error("Exception while iterating node Ex: " + ex);
                    }
                }//                for (; it.hasNext();) {
            } else if (isFileNode){
                boolean update = updateFileNodePermissions(root, sidsString, denySidsString, shareSidsString, denyShareSidsString);
            }
        }
    }
    
    public static void updateFolderPermissions(Node root,
            String sidsString,
            String denySidsString,
            String shareSidsString,
            String denyShareSidsString) throws LoginException, RepositoryException, Exception {

        if (root == null) {
            log.debug("Node DirId is null so loading empty rows");
        } else {

            log.debug("Updating Recursive for ---------> " + root.getName());
            NodeIterator it = null;
            Node node = null;
            String nodeName = null;
            String nodePath = null;
            String nodeType = null;

            nodeName = root.getName();
            nodeType = root.getPrimaryNodeType().getName();
            nodePath = root.getPath();

            log.debug("nodeName " + nodeName);
            log.debug("nodeType " + nodeType);
            log.debug("nodePath " + nodePath);
            boolean isFolderNode = nodeType.equalsIgnoreCase("nt:folder");
            
            if (isFolderNode) {
                 boolean update = updateFolderNodePermissions(root, sidsString, denySidsString, shareSidsString, denyShareSidsString);
                it = root.getNodes();
                for (; (it != null && it.hasNext());) {
                    try {
                        node = it.nextNode();
                        if (node.getPrimaryNodeType().getName().equalsIgnoreCase("nt:file")) {
                               update = updateFileNodePermissions(node, sidsString, denySidsString, shareSidsString, denyShareSidsString);
                        } 
                    } catch (Exception ex) {
                        log.error("Exception while iterating node Ex: " + ex);
                    }
                }//                for (; it.hasNext();) {
            }//if (isFolderNode) {
            
        }
    }

    public static boolean updateFolderNodePermissions(Node root, String sidsString, String denySidsString, String shareSidsString, String denyShareSidsString) throws RepositoryException {

        boolean update = false;
        if (root.hasNode(CustomNodeType.VC_RESOURCE_PERMISSIONS)) {// Property Exsist
            log.debug("Custome Node Exsist at folder So updating property on that.");
            root = root.getNode(CustomNodeType.VC_RESOURCE_PERMISSIONS);

        } else {
            log.debug("Custome Node doesn't Exsist at folder So updating property on that.");
            root = root.addNode(CustomNodeType.VC_RESOURCE_PERMISSIONS, CustomNodeType.VC_RESOURCE_PERMISSIONS);
        }
        update = updateNodePermissionProperties(root, sidsString, denySidsString, shareSidsString, denyShareSidsString);
        return update;

    }

    public static boolean updateFileNodePermissions(Node root, String sidsString, String denySidsString, String shareSidsString, String denyShareSidsString) throws RepositoryException {

        NodeIterator innerIt = null;
        Node innerNode = null;
        innerIt = root.getNodes("jcr:content");
        boolean update = false;

        while (innerIt != null && innerIt.hasNext()) {
            log.debug("node contains JCR:CONTENT Node");
            innerNode = innerIt.nextNode();
            update = updateNodePermissionProperties(innerNode, sidsString, denySidsString, shareSidsString, denyShareSidsString);
        }
        return update;
    }

    public static boolean updateNodePermissionProperties(Node node, String sidsString, String denySidsString, String shareSidsString, String denyShareSidsString) throws RepositoryException {

        if (sidsString != null) {
            Property prop = node.setProperty(CustomProperty.VC_ALLOWED_SIDS, sidsString.toString());
            log.debug("AllowedSIDs " + sidsString.toString());
        }
        if (denySidsString != null) {
            Property prop = node.setProperty(CustomProperty.VC_DENIED_SIDS, denySidsString.toString());
            log.debug("DeniedSIDs " + denySidsString.toString());
        }
        if (shareSidsString != null) {
            Property prop = node.setProperty(CustomProperty.VC_S_ALLOWED_SIDS, shareSidsString.toString());
            log.debug("AllowedShareSIDs " + shareSidsString.toString());
        }
        if (denyShareSidsString != null) {
            Property prop = node.setProperty(CustomProperty.VC_S_DENIED_SIDS, denyShareSidsString.toString());
            log.debug("DenyShareSIDs " + denyShareSidsString.toString());
        }
        return true;

    }
}
