package com.virtualcode.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;

import com.artofsolving.jodconverter.DocumentConverter;
import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.converter.OpenOfficeDocumentConverter;

public class DocConverter {
	
	private static DocConverter docConverter;
	private DocConverter(){
		
	}
	
	public static DocConverter getInstnce(){
		if(docConverter==null)
			docConverter = new DocConverter();
		return docConverter;
	}
	
	
	public String convertDocument(String documentPath, String outputPath){
		// TODO Auto-generated method stub
		System.out.println("Starting conversion");
		long startTime = System.currentTimeMillis();
//		String filename = "Lec14";
		File inputFile = new File(documentPath);
		File outputFile = new File(outputPath);
		
		String outputFilePath = null;
		// connect to an OpenOffice.org instance running on port 8100

		try {
//			InputStream in = new FileInputStream(inputFile);
//			OutputStream os = new FileOutputStream(outputFile);

//			if connection fails, follow these steps:
//			1. Install OpenOffice.org v2.0.3 or higher (has been tested on many versions, include 3.2).
//			2. Go to the folder where it was installed (for example, C:\Program Files\OpenOffice.org\program) and start OpenOffice service with the following command: 
//			soffice -headless -accept="socket,host=127.0.0.1,port=8100;urp;" -nofirststartwizard
			OpenOfficeConnection connection = new SocketOpenOfficeConnection(8100);//(8100);
			/*-<document-format>
			 * <name> Microsoft PowerPoint</name>
			 * <family>Presentation</family>
			 * <mime-type>application/vnd.ms-powerpoint</mime-type>
			 * <file-extension>ppt</file-extension>
			 * -<export-options>-<entry><family>Presentation</family>-
			 * <map>-<entry><string>FilterName</string><string>MS PowerPoint 97</string></entry></map></entry></export-options><import-options/></document-format>*/
			// close the connection			
			connection.connect();
//			DocumentFormat idf = new DocumentFormat("Microsoft PowerPoint", DocumentFamily.PRESENTATION, "application/vnd.ms-powerpoint", "ppt");
//			System.out.println(idf.getName());
//			idf.setExportFilter(DocumentFamily.PRESENTATION, "impress_pdf_Export");
//			DocumentFormat odf = new DocumentFormat("Portable Document Format", DocumentFamily.PRESENTATION, "application/pdf", "pdf");
//			System.out.println(odf.getName());
			DocumentConverter converter = new OpenOfficeDocumentConverter(connection);
//			converter.convert(in, idf , os, odf); //not working with error java.lang.IllegalArgumentException: unsupported conversion: from Microsoft PowerPoint to Portable Document Format
			converter.convert(inputFile, outputFile);
			connection.disconnect();
			outputFilePath = outputFile.getPath();
			System.out.println(outputFilePath);
			
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch(Exception ex){
			ex.printStackTrace();
		}
		System.out.println("conversion completed in: " + (System.currentTimeMillis() - startTime));
		
		return outputFilePath;
		
	}

	
}
