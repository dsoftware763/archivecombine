/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;

import com.virtualcode.common.AdvancedSecureURL;
import com.virtualcode.common.InvalidCharacterHandler;
import com.virtualcode.common.SID;
import com.virtualcode.common.SecureURL;

/**
 *
 * @author se7
 */
public class StringUtils {

    static final Logger log = Logger.getLogger(StringUtils.class);

    public static String encodeStr(String orig) {
        //This method Supports all known HTML 4.0 entities, including funky accents...
        String encoded = StringEscapeUtils.escapeHtml(orig) + "";
        encoded = encoded.replaceAll("'", "&apos;");//Note that the commonly used apostrophe escape character (&apos;) is not a legal entity and so is not supported
        // encoded         = encoded.replaceAll("[+]", "%2B");//Note that the commonly used apostrophe escape character (&apos;) is not a legal entity and so is not supported
        //org.apache.commons.lang.StringEscapeUtils.unescapeXml(orig);
        return encoded;
    } 

    private static String removeNonUtf8CompliantCharacters(String inString) {
        if (null == inString) {
            return null;
        }

        byte[] byteArr = inString.getBytes();
        for (int i = 0; i < byteArr.length; i++) {
            byte ch = byteArr[i];
            // remove any characters outside the valid UTF-8 range as well as all control characters
            // except tabs and new lines
            if (!((ch > 31 && ch < 253) || ch == '\t' || ch == '\n' || ch == '\r')) {
                byteArr[i] = ' ';
            }
        }

        return new String(byteArr);
    }

    public static String[] getReplacedInvalidCharacters(String[] tokens) {
        if (tokens == null || tokens.length == 0) {
            return tokens;
        }
        for (int i = 0; i < tokens.length; i++) {
            tokens[i] = InvalidCharacterHandler.replaceInvalidCharacters(tokens[i]);
        }
        return tokens;
    }

    private static String[] getTokens(String path) {
        String regExp = "/";
        String[] tokens = path.split(regExp);
        String[] contextList = null;
        if (tokens.length > 1) {
            contextList = Arrays.copyOfRange(tokens, 0, tokens.length);
        } else {
            contextList = tokens;
        }
        return contextList;
    }

    private static HashMap getLayerTokens(String layers, String secureInfoStr) {
        String regExp = "\\|";
        String[] tokens = layers.split(regExp);
        HashMap hLayers = new HashMap();
        String token = null;
        String[] attrib = null;

        for (int j = 0; j < tokens.length; j++) {
            log.debug("Token[" + j + "] - " + tokens[j]);
        }

        for (int i = 0; i < tokens.length; i++) {
            token = tokens[i];
            if (token.startsWith(secureInfoStr)) {
                continue;
            }
            log.debug("Token: " + token);
            attrib = token.split("=");
            hLayers.put(attrib[0], attrib[1]);
            log.debug("name: " + attrib[0] + " Value: " + attrib[1]);
        }
        log.debug("Hashtable: " + hLayers.toString());
        return hLayers;
    }

    public static String[] getPathElements(String fileContextPath) {
        log.debug("fileContextPath --->"+fileContextPath);
        String[] pathElements = getTokens(fileContextPath);
        log.debug("Path Elements before invalid character handling");
        for (String t : pathElements) {
            log.debug(t + ",");
        }
        pathElements = getReplacedInvalidCharacters(pathElements);
        log.debug("Path Elements after invalid character handling");
        for (String t : pathElements) {
            log.debug(t + ",");
        }

        return pathElements;
    }

    public static HashMap getLayerAttribs(String layersList, String secureInfoStr) {

        HashMap layerAttribs = null;

        // extract secureInfo
        int indexSIStart = 0;
        int indexSIEnd = 0;
        String secureInfo = "";

        indexSIStart = layersList.indexOf(secureInfoStr);
        log.debug("indexSIStart : " + indexSIStart);

        if (indexSIStart >= 0) {
            indexSIStart += secureInfoStr.length() + 1; // to get to start of the value of secureInfo
            indexSIEnd = layersList.indexOf('|', indexSIStart);
            if (indexSIEnd < 0) {
                indexSIEnd = layersList.length();
            }
            log.debug("indexSIEnd : " + indexSIStart);

            secureInfo = layersList.substring(indexSIStart, indexSIEnd);
        }

        log.debug("Secure Info inserted into Hashtable: " + secureInfo);

        if (layersList != null) {
            log.debug("Layers List: " + layersList);
            layerAttribs = getLayerTokens(layersList, secureInfoStr);
        }
        if (layerAttribs != null) {
            layerAttribs.put(secureInfoStr, secureInfo);
        }

        return layerAttribs;
    }

    public static String convertArrayToString(String[] sids) {
        StringBuilder sidsString = new StringBuilder();
        int i = 0;
        if (sids != null) {

            for (String s : sids) {
                if (i++ > 0) {
                    sidsString.append(",");
                }
                sidsString.append(s);
            }
        }
        return sidsString.toString();
    }
    //overloaded method to handle SID ArrayList
    public static String convertArrayToString(ArrayList<SID> sids) {
        StringBuilder sidsString = new StringBuilder();
        int i = 0;
        if (sids != null) {

            for (SID s : sids) {
            	String sid = s.getSid();
                if (i++ > 0) {
                    sidsString.append(",");
                }
                sidsString.append(sid);
            }
        }
        return sidsString.toString();
    }

    /*
     * Manually handling special characters
     * <white-space>,'&', '?', '+',',', '#'
     */
    public static String encodeNames(String path) {
//        String[] elements = getTokens(path);
        String encoded = "";
        try {
//            for (int e = 0; e < elements.length; e++) {

                encoded = path.replaceAll(" ", "%20")
                 //       .replaceAll("&", "%26")
                        //.replaceAll("[?]", "%3F")
                        .replaceAll("[+]", "%2B")
                        .replaceAll(",", "%2C")
                        .replaceAll("#", "%23")
                        .replaceAll("'","%27");// + java.net.URLEncoder.encode(elements[e], "UTF-8");

                //problem with &amp and &

                encoded = encoded.replaceAll("&amp;", "%26").replaceAll("&", "%26");


//                if(e != (elements.length-1)){
//                    encoded = encoded  + "/";
//                }
//            }
            //encoded = encoded.substring(encoded.length()-1);
                log.debug("encoded path: "+encoded);
        } catch (Exception ex) {
            log.debug("Exception while encoding: "+ex);
            ex.printStackTrace();
            encoded = "";
        }
        return encoded;
    }

        public static String decodeNames(String path) {
//        String[] elements = getTokens(path);
        String decoded = "";
        try {
//            for (int e = 0; e < elements.length; e++) {
                decoded =  path.replaceAll("%20", " ")
                        .replaceAll("%26", "&")
                     //   .replaceAll("%3F", "?")
                        .replaceAll( "%2B", "+")
                        .replaceAll("%2C", ",")
                        .replaceAll("%23","#")
                        .replaceAll("%27","'");//java.net.URLDecoder.decode(elements[e], "UTF-8");
//                if(e != (elements.length-1)){
//                    decoded = decoded  + "/";
//                }
//            }
                log.debug("decoded path: " + decoded);


        } catch (Exception ex) {
            log.debug("Exception while decoding: "+ex);
            ex.printStackTrace();
            decoded = "";
        }
        return decoded;
    }

     public static String escapeXmlSpecialChars(String input){
       if(input==null)
           return null;
       input = input.replaceAll("&", "&amp;");
       input = input.replaceAll("<", "&lt;");
       input = input.replaceAll(">", "&gt;");
       input = input.replaceAll("\"", "&quot;");
       input = input.replaceAll("'", "&apos;");
       return input;
   }

    public static String restoreXmlSpecialChars(String input){
       if(input==null)
           return null;
       input = input.replaceAll("&amp;","&");
       input = input.replaceAll("&lt;","<");
       input = input.replaceAll("&gt;",">");
       input = input.replaceAll("&quot;","\"");
       input = input.replaceAll("&apos;","'");
       return input;
    }

    public synchronized static String encodeUrl(String url){
        long start=System.currentTimeMillis();
        SecureURL secUrl=SecureURL.getInstance();
        try {
            String encryptedUrl= secUrl.getEncryptedURL(url);
            log.debug("Time elapsed in encodeUrl-->"+(System.currentTimeMillis()-start));
            return encryptedUrl;
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
        return null;


    }

    public synchronized static String decodeUrl(String url){
        long start=System.currentTimeMillis();
        SecureURL secUrl=SecureURL.getInstance();
        try {
           String decryptedUrl= secUrl.getDecryptedURL(url);
           log.debug("Time elapsed in decodeUrl-->"+(System.currentTimeMillis()-start));
           return decryptedUrl;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    public synchronized static String encodeUrlAdvanced(String url){
        long start=System.currentTimeMillis();
        AdvancedSecureURL  secUrl=AdvancedSecureURL.getInstance();
        try {
            String encryptedUrl= secUrl.getEncryptedURL(url);
            log.debug("Time elapsed in encodeUrl-->"+(System.currentTimeMillis()-start));
            return encryptedUrl;
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
        return null;


    }

    public synchronized static String decodeUrlAdvanced(String url){
        long start=System.currentTimeMillis();
        AdvancedSecureURL secUrl=AdvancedSecureURL.getInstance();
        try {
           String decryptedUrl= secUrl.getDecryptedURL(url);
           log.debug("Time elapsed in decodeUrl-->"+(System.currentTimeMillis()-start));
           return decryptedUrl;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }



       private static String encodeURLByTokens(String url) {
        if (url == null || url.length() < 2) {
            return url;
        }

        String[] tokens = url.substring(1).split("/");
        String[] correctedTokens = new String[tokens.length];
        int i = 0;

        try {
            for (String t : tokens) {
                correctedTokens[i++] = URLEncoder.encode(t, "utf-8").replaceAll("[+]", "%20");
            }
        } catch (UnsupportedEncodingException ex) {
           log.error(ex);
        }
        StringBuilder stb = new StringBuilder();
        for (String t : correctedTokens) {
            log.debug("token added --> "+t);
            stb.append('/').append(t);
        }
        return stb.toString();
    }
       
       public static String removeDuplicates(String value){
    	  // System.out.println("value before:" + value);
		    String[] array = value.split(",");
		    Set<String> set = new HashSet<String>(Arrays.asList(array));
		    value = set.toString();
//		    System.out.println(value.substring(1, value.indexOf("]")));
		    log.debug("set to string: " + value);
		    value = value.substring(1, value.indexOf("]")).replaceAll(" ", "");
		    log.debug("after removing [] and white spaces:" + value);
		    //System.out.println("after removing []:" + value);
		    
    	   return value;
    	   
       }



    public static void main(String[] args) {
        String path = "aaaoâ™«â”‚â•�jâ”‚â˜¼Â£Ã¬â••445";
        String str="";
        System.out.println("orignal String-->"+path);
        try{
            str=URLEncoder.encode(path, "utf-8");
        }catch(Exception ex){
            ex.printStackTrace();
        }
        try{
            System.out.println("decoded url -->"+URLDecoder.decode(str, "utf-8"));
        }catch(Exception ex){

        }
        str = "3, 2, 1, S-1-1-0, S-1-5-32-544, S-1-5-18, S-1-5-21-2397042273-2794206915-3762467723-1112, S-1-5-21-2397042273-2794206915-3762467723-1162,"+
        		"3, 2, 1, S-1-1-0, S-1-5-32-544, S-1-5-18, S-1-5-21-2397042273-2794206915-3762467723-1112, S-1-5-21-2397042273-2794206915-3762467723-1162";
        
        System.out.println(removeDuplicates(str));

    }
}
