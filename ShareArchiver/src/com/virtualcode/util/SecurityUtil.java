
package com.virtualcode.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.jcr.SimpleCredentials;

import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;

/**
 * Utility Class which will provide methods for Hashing and encoding authentication request.
 * @author AtiqurRehman
 */
public class SecurityUtil {

    static final Logger log = Logger.getLogger(SecurityUtil.class);

    /**
     * Method which will take Base64 String and decode it to user credentials
     * @param secureInfo
     * @return
     */
    public static SimpleCredentials decodeSecureInfo(String secureInfo) {

        log.debug("Decoding Secure Info Parameter: " + secureInfo);

        SimpleCredentials cred = null;
        try {
            String decodedString = new String(org.apache.commons.codec.binary.Base64.decodeBase64(secureInfo.getBytes()));
            String username = decodedString.substring("username=".length(), decodedString.indexOf("&"));
            String password = decodedString.substring(decodedString.indexOf("&") + "password=".length() + 1);
            cred = new SimpleCredentials(username, password.toCharArray());

            log.debug("username :" + username);
            log.debug("password :" + password);


        } catch (Exception e) {
            log.error(e.toString());

        }
        log.debug("Decoding Secure Info Parameter End");
        return cred;
    }

    /**
     * Method will take username and password as and input and encode it to Base64 encoding string which will be used by authentication filter
     * @param username
     * @param password
     * @return
     */
    public static String encodeSecureInfo(String username, String password) {

        log.debug("encodeSecureInfo started.");

        String secureInfo = "username=" + username + "&password=" + password;
        try {
            secureInfo = new String(org.apache.commons.codec.binary.Base64.encodeBase64(secureInfo.getBytes()));
        } catch (Exception e) {
            log.error(e.toString());
        }
        log.debug("encodeSecureInfo finished.");
        return secureInfo;
    }

    public static String getMD5(String input) {

        String result = null;
        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("MD5");

            messageDigest.reset();
            messageDigest.update(input.getBytes("UTF8"));
            final byte[] resultByte = messageDigest.digest();
            log.debug(resultByte);
            result = new String(Hex.encodeHex(resultByte));

        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }
}
