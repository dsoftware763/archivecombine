package com.virtualcode.schedular;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.jcr.query.Row;
import javax.jcr.query.RowIterator;

import org.apache.log4j.Logger;
import org.springmodules.jcr.JcrTemplate;

import com.virtualcode.common.CustomProperty;
import com.virtualcode.repository.monitor.ArchiveDateRunnerMonitor;

import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.vo.SystemCodeType;

public class ArchiveDateRunner implements Runnable{
	
	Logger log=Logger.getLogger("FILESIZERUNNER");
	private Session session;
		
	private JcrTemplate jcrTemplate;
	
	public ArchiveDateRunner(Session session){
		this.session = session;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
//		running = true;
//		log.debug("starting file size runner");
//        log.info(" ****************      Going to Execute Query: "+new Date()+"  **************************");
//        
//        RowIterator searchResult = null;
//        String INPUT_DATE_FORMAT = "dd-MM-yyyy";
//        SimpleDateFormat inputDF = new SimpleDateFormat(INPUT_DATE_FORMAT);
//
//        String strStratModifiedDate = "";
//        String strEndModifiedDate = "";
//        String strFileSizeStart = "";
//        String strFileSizeEnd = "";
//       // String queryLanguage = Query.JCR_SQL2;
//        //get all files which do not have vc:fileSize property
//        String  statement = "SELECT excerpt(.) FROM nt:resource WHERE vc:fileSize is NULL";
//        
//        log.info(" Query String: \n" + statement);
//        QueryManager qm = null;
//        
//        try {
//			qm = session.getWorkspace().getQueryManager();
//			Query q = qm.createQuery(statement, Query.SQL);
//			Long maxSearchRes=2000l;
//            
//			ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
//			 
//      		if(ru.getCodeValue("MAX_SEARCH_RESULTS", SystemCodeType.GENERAL)!=null){
//	      		try{
//	                  maxSearchRes=Long.parseLong(ru.getCodeValue("MAX_SEARCH_RESULTS", SystemCodeType.GENERAL));
//	            }catch(Exception ex){
//	                  maxSearchRes=2000l;
//	            }
//      		}
//          
//      		log.info("MAX_SEARCH_RESULTS:"+maxSearchRes);
//            q.setLimit(maxSearchRes);
//	        QueryResult r = q.execute();
//	        searchResult = r.getRows();
//	        log.info("result size: "+searchResult.getSize());
//	        log.debug("result size: "+searchResult.getSize());
//	        Long totalFilesProcessed = 0l; 
//	        int count = 0;
//	        
//	        FileSizeRunnerMonitor fileSizeRunnerMonitor = FileSizeRunnerMonitor.getInstance();
//	        totalFilesProcessed = fileSizeRunnerMonitor.getCurrentFileCount();
//	        
//	        if(totalFilesProcessed==null)
//	        	totalFilesProcessed = 0l;
//	        
////	        if (searchResult==null)return;
//	        
//	        javax.jcr.query.Row row;
//	        javax.jcr.Node node;
//	        javax.jcr.Node resource;
//	        
//	        
//	        while(searchResult.hasNext() && fileSizeRunnerMonitor.isRunning()){
//                row = searchResult.nextRow();
//                node = row.getNode().getParent();
//                resource = row.getNode();
//                String fileName = node.getName();
//                log.info("setting file size for :" + fileName);
//                if (resource.hasProperty("jcr:data")) {
////                     double length = resource.getProperty("jcr:data").getLength();
////                     resource.setProperty(CustomProperty.VC_FILE_SIZE, length);
//                 }
//                totalFilesProcessed = totalFilesProcessed + 1;
//                fileSizeRunnerMonitor.setCurrentFileCount(totalFilesProcessed);
//                count = count+1;
//                if(count>20)
//                	break;
//                
//	        }
//	        session.save();
//	        
//		} catch (RepositoryException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			log.error(e.toString());
//		}
//        
//        
//
//        log.info(" Query execution was successfull ");
		ArchiveDateRunnerMonitor archiveDateRunnerMonitor = ArchiveDateRunnerMonitor.getInstance();
		while(true){
			if(archiveDateRunnerMonitor.isStopped())
				break;
			
			if(process()) {
				try {
					log.debug("SLEEPING");
					Thread.sleep(10000);//sleep for 10secs only
				} catch(InterruptedException ie){}
				
			} else {
				archiveDateRunnerMonitor.setStopped(true);
				archiveDateRunnerMonitor.setRunning(false);
				log.debug("TERMINATING");
			}			
		}
		
		log.info("STOPPED ..............");
	}
	
	public boolean process(){
		
		boolean runAgain	=	false;
		ArchiveDateRunnerMonitor archiveDateRunnerMonitor = ArchiveDateRunnerMonitor.getInstance();
		
		if(archiveDateRunnerMonitor.isStopped())
			return runAgain;	//return without processing
		
		log.debug("starting file size runner "+new Date());
        
        RowIterator searchResult = null;
        //String INPUT_DATE_FORMAT = "dd-MM-yyyy";
        //SimpleDateFormat inputDF = new SimpleDateFormat(INPUT_DATE_FORMAT);

        //String strStratModifiedDate = "";
        //String strEndModifiedDate = "";
        //String strFileSizeStart = "";
        //String strFileSizeEnd = "";
        // String queryLanguage = Query.JCR_SQL2;
        //get all files which do not have vc:archiveDate property
        String  statement = "SELECT excerpt(.) FROM nt:resource WHERE vc:archiveDate is NULL";
        
        log.info(" Query String: \n" + statement);
        QueryManager qm = null;
        
        try {
			qm = session.getWorkspace().getQueryManager();
			Query q = qm.createQuery(statement, Query.SQL);
			Long maxBatchSize=2000l;
            
			ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
			 
      		if(ru.getCodeValue("MAX_BATCH_SIZE", SystemCodeType.GENERAL)!=null){
	      		try{
	                  maxBatchSize=Long.parseLong(ru.getCodeValue("MAX_BATCH_SIZE", SystemCodeType.GENERAL));
	            }catch(Exception ex){
	                  maxBatchSize=2000l;
	            }
      		}
      		log.info("MAX_BATCH_SIZE:"+maxBatchSize);
            q.setLimit(maxBatchSize);
	        QueryResult r = q.execute();
	        searchResult = r.getRows();

	        if(searchResult==null || searchResult.getSize()<=0) {
	        	log.info("Documents not exist, to be updated");	  
	        	runAgain	=	false;
	        	
	        } else {
	        	runAgain	=	true;
	        	
	        	Row row;
		        Node node;
		        Node resource;
		        
		        Long totalFilesProcessed = 0l;
		        totalFilesProcessed = archiveDateRunnerMonitor.getCurrentFileCount();
		        
		        if(totalFilesProcessed==null)
		        	totalFilesProcessed = 0l;
		        
		        log.debug("initial count: " + totalFilesProcessed);
		        log.debug("More Expected: "+searchResult.getSize());
//		        System.out.println("result size: " + searchResult.getSize());
		        while(searchResult.hasNext()){
		        	try{
		        		row = searchResult.nextRow();

	                System.out.println("curretn node: " + row.getPath());
	                node = row.getNode().getParent();
	                resource = row.getNode();
	                String fileName = node.getName();
	                String nodeType = node.getPrimaryNodeType().getName();
	                boolean isFileNode = "nt:file".equalsIgnoreCase(nodeType);
	                Long length = 0l;
	                //log.info("setting file size for :" + fileName);
//	                System.out.println("setting archive date for :" + fileName + " of type: " + nodeType);
	                log.debug("setting file size for :" + fileName);
	                if (node.hasProperty(Property.JCR_CREATED)) {
	                	
//	                	if(isFileNode && node.hasNode(Property.JCR_CONTENT) && node.getNode(Property.JCR_CONTENT).hasProperty(CustomProperty.VC_LINKED_TO)) {//if Custom Linked Node
//		                   Property prop    =   node.getNode(Property.JCR_CONTENT).getProperty(CustomProperty.VC_LINKED_TO);
//		                   String refNodeID =   prop.getString();
//		                   
//		                   //log.debug("linked node found");
//		                   Node refNode =  session.getNodeByIdentifier(refNodeID);		
//		                   log.info("node linked to: " + refNodeID);
//		                   
//	                        if (refNode.hasProperty("jcr:data")) {
//	                            log.debug(refNode.getName() + " , contains JCR:DATA Property So calculating Size & MimeType");
//	                            length = new Long(refNode.getProperty("jcr:data").getLength());
//	                        } else {
//	                        	log.error("Error to calculate size for Linked Node "+fileName+", RefNode: "+refNodeID);
//	                        }
//			                    //}
//	                	} else {
//	                		//log.info("Original node "+fileName);
//	                		length = new Long(resource.getProperty("jcr:data").getLength());
//	                	}
	                	
	                	Property prop = node.getProperty(Property.JCR_CREATED);
	    				Calendar c = prop.getDate();
//	    				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//	    				System.out.println(df.format(c.getTime()));
//	    				archiveDate = df.format(c.getTime());
		                
		                log.debug("length of file: " + length );
		                resource.setProperty(CustomProperty.VC_ARCHIVE_DATE, c);
	
		                session.save();
		    	        log.debug("session saved successfully-------");
		    	        
		                totalFilesProcessed = totalFilesProcessed + 1;
		                archiveDateRunnerMonitor.setCurrentFileCount(totalFilesProcessed);
		    	        
	                } else {
	                	log.error("jcr:created not found for "+fileName);
	                }
	                }catch(Exception ex){
	                	ex.printStackTrace();
	                }
	                
		        }
		        log.debug("final count: " + totalFilesProcessed);
	        }	        
	        
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.error(e.toString());
		}
        
        log.info("batch execution was successfull ");
        return runAgain;
	}
}