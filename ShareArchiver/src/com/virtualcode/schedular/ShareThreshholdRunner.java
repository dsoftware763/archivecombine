package com.virtualcode.schedular;

import java.util.List;
import java.util.TimerTask;

import org.apache.poi.ss.formula.functions.Days360;

import com.virtualcode.common.ShareSizeRunner;
import com.virtualcode.services.DriveLettersService;
import com.virtualcode.services.JobService;
import com.virtualcode.services.JobStatisticsService;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.DriveLetters;
import com.virtualcode.vo.Job;
import com.virtualcode.vo.JobSpDocLib;
import com.virtualcode.vo.JobStatistics;

public class ShareThreshholdRunner implements Runnable {

	
	public void run(){
		
		List<DriveLetters> driveLettersList=driveLettersService.getAll();
		for(int i=0;driveLettersList!=null && i<driveLettersList.size();i++){
			DriveLetters driveLetter=driveLettersList.get(i);
			
			ShareSizeRunner shareSizeRunner=new ShareSizeRunner();
			shareSizeRunner.setDriveLetter(driveLetter);
			Thread subThread=new Thread(shareSizeRunner);
			try{
				subThread.start();
			    subThread.join();
			}catch(InterruptedException intExcep){intExcep.printStackTrace();}
			
			System.out.println("Share \""+driveLetter.getSharePath()+"\" Free Space: "+driveLetter.getFreeSpace()+"  Used Space: "+driveLetter.getShareUsedPercentage());
			
			if(driveLetter.getShareUsedPercentage()<driveLetter.getCriticalLimit()){
				continue;//don't start job till critical limit.
			}
			
			String smbPath=VCSUtil.getSambaPath(driveLetter.getRestoreDomain(), driveLetter.getRestoreUsername(), driveLetter.getRestorePassword(), driveLetter.getSharePath());
			List<JobSpDocLib> jobsList=jobService.getShareThreshholdJobs(smbPath);
			
			if(jobsList!=null){
				System.out.println(jobsList.size()+" Jobs found against sharepath "+driveLetter.getSharePath());
				for(int k=0;k<jobsList.size();k++){
					Job job=jobService.getJobById(jobsList.get(k).getJob().getId());
					List<JobStatistics> jobStatsList=jobStatisticsService.getJobStatisticsByJob(job.getId());
					if(jobStatsList!=null && jobStatsList.size()>0){
						 
						int days = (int)( (jobStatsList.get(0).getJobEndTime().getTime() - new java.util.Date().getTime()) 
				                 / (1000 * 60 * 60 * 24) );
						if(days>=2){
							job.setReadyToExecute(true);
							jobService.saveJob(job);
						}
					}
					job.setReadyToExecute(true);
					jobService.saveJob(job);
					
				}
			}else{
				System.out.println("No Job found against sharepath "+driveLetter.getSharePath());
			}
			
		}		
		
	}
	
	private DriveLettersService driveLettersService;
	
	private JobService jobService;
	
	private JobStatisticsService jobStatisticsService;

	public void setDriveLettersService(DriveLettersService driveLettersService) {
		this.driveLettersService = driveLettersService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	public void setJobStatisticsService(JobStatisticsService jobStatisticsService) {
		this.jobStatisticsService = jobStatisticsService;
	}
	
	
	
}
