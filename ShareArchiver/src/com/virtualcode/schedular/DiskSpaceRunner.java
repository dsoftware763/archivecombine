package com.virtualcode.schedular;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.virtualcode.common.PLFormatException;
import com.virtualcode.common.VCSConstants;
import com.virtualcode.repository.monitor.RepositoryMonitor;
import com.virtualcode.services.JobService;
import com.virtualcode.services.JobStatusService;
import com.virtualcode.services.MailService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.services.SystemCodeService;
import com.virtualcode.util.License;
import com.virtualcode.util.PLVManager;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.Job;
import com.virtualcode.vo.JobStatus;
import com.virtualcode.vo.RepositoryStatus;
import com.virtualcode.vo.SystemCode;
import com.virtualcode.vo.SystemCodeType;

@Service
public class DiskSpaceRunner implements Runnable {

	Logger log=Logger.getLogger("DISKSPACERUNNER");
	
	public void run(){
		
		log.debug("DISKSPACERUNNER ... starting");
		 RepositoryMonitor repositoryMonitor = RepositoryMonitor.getInstance();
		 RepositoryStatus repositoryStatus = repositoryMonitor.getRepositoryStatus(null);
		 System.out.println(repositoryStatus.getTotalFreeSpaceOnDisk());
		 Long usedSpace = repositoryStatus.getTotalFreeSpaceOnDisk();
		 
		 String hostname = "unknown";
		 try {
			  InetAddress addr = InetAddress.getLocalHost();
			  byte[] ipAddr = addr.getAddress();
			  hostname = addr.getHostName();
			  System.out.println("hostname="+hostname);
		  } catch (UnknownHostException e) {
			  hostname = "unknown";
		  }
		 
		 Long warningLimit = 90l;
		 Long criticalLimit = 95l;
		 ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
		 if(ru.getCodeValue("server_warning_limit", SystemCodeType.GENERAL)!=null){
			 warningLimit = new Long(ru.getCodeValue("server_warning_limit", SystemCodeType.GENERAL));
		 }
		 
		 if(ru.getCodeValue("server_critical_limit", SystemCodeType.GENERAL)!=null){
			 criticalLimit = new Long(ru.getCodeValue("server_critical_limit", SystemCodeType.GENERAL));
		 }
		 log.debug("DISKSPACERUNNER ... used space on dirve: "+ usedSpace);
		 if(usedSpace>=criticalLimit){
			 
			 log.debug("DISKSPACERUNNER ... used space on dirve reached 95% - sending email notification");
			 boolean status = mailService.sendMail("Critical alert Disk capacity exceeded critical limit", "Disk space capacity has reached or exceeded the critical limit on Server : "+ hostname);
			 log.debug("DISKSPACERUNNER ... used space on dirve above 90% - sending email notification");
//			 mailService.sendMail("Disk Space Running Low Notification", "Your disk capacity has reached 90%.- ["+usedSpace+"%] Delete some data to free some space for archiving ");//+days+" days.<br/> Register your application to avoid the inconvenience.");
			 log.debug("DISKSPACERUNNER ... 95% - email status: " + status);
			 log.debug("DISKSPACERUNNER ... 95% - get all active jobs");
			 List<Job> activeJobList = jobService.getAllActiveJobs();
			 log.debug("DISKSPACERUNNER ... cancelling all active  jobs");
			 if(activeJobList!=null && activeJobList.size()>0)
				 log.debug("DISKSPACERUNNER ... total active jobs: " + activeJobList.size());
			 else
				 log.debug("DISKSPACERUNNER ... no active jobs found");
			 for(Job job: activeJobList){
				 System.out.println("Job name: " + job.getName());
				 
			   List<JobStatus> jobStatsList=jobStatusService.getJobStatusByJob(job.getId());
   
			   if(jobStatsList!=null && jobStatsList.size()>0){
				   JobStatus jobStatus=jobStatsList.get(0);
				   System.out.println("job status id: "+jobStatus.getId());
				   JobStatus _instance=new JobStatus();
				   _instance.setDescription("blocking...");
				   _instance.setExecStartTime(jobStatus.getExecStartTime());
				   _instance.setErrorCode(jobStatus.getErrorCode());
				   _instance.setExecutionId(jobStatus.getExecutionId());
				   _instance.setCurrent(1);
				   _instance.setJob(jobStatus.getJob());
				   _instance.setPreviousJobStatusId(jobStatus.getId());
				   _instance.setStatus(VCSConstants.JOB_STATUS_CANCEL);
				  
				   jobStatusService.saveJobStatus(_instance);
			   }
			 }
			 
		 }else if(usedSpace>=warningLimit){
			 log.debug("DISKSPACERUNNER ... used space on dirve reached 90% - sending email notification");
//			 boolean status = mailService.sendMail("Critical alert Disk capacity exceeded critical limit", "Your disk capacity has reached 90%. Current Status: ["+usedSpace+"%] Please more space for archiving ");//+days+" days.<br/> Register your application to avoid the inconvenience.");
			 boolean status = mailService.sendMail("Critical alert Disk capacity exceeded warning limit", "Disk space capacity has reached or exceeded the warning limit on Server : "+ hostname);

			 log.debug("DISKSPACERUNNER ... 90% - email status: " + status);
			 log.debug("DISKSPACERUNNER ... 90% - get all active import jobs");
			 List<Job> activeJobList = jobService.getAllActiveImportJobs();
			 log.debug("DISKSPACERUNNER ... cancelling all active import jobs");
			 if(activeJobList!=null && activeJobList.size()>0)
				 log.debug("DISKSPACERUNNER ... total active import jobs: " + activeJobList.size());
			 else
				 log.debug("DISKSPACERUNNER ... no active import jobs found");
			 for(Job job: activeJobList){
				 System.out.println("Job name: " + job.getName());
				 
			   List<JobStatus> jobStatsList=jobStatusService.getJobStatusByJob(job.getId());
//			   
			   if(jobStatsList!=null && jobStatsList.size()>0){
				   JobStatus jobStatus=jobStatsList.get(0);
				   System.out.println("job status id: "+jobStatus.getId());
				   JobStatus _instance=new JobStatus();
				   _instance.setDescription("blocking...");
				   _instance.setExecStartTime(jobStatus.getExecStartTime());
				   _instance.setErrorCode(jobStatus.getErrorCode());
				   _instance.setExecutionId(jobStatus.getExecutionId());
				   _instance.setCurrent(1);
				   _instance.setJob(jobStatus.getJob());
				   _instance.setPreviousJobStatusId(jobStatus.getId());
				   _instance.setStatus(VCSConstants.JOB_STATUS_CANCEL);
				  
				   jobStatusService.saveJobStatus(_instance);
			   }
			 }

		 }else{
			 log.debug("DISKSPACERUNNER ... used space on dirve: "+ usedSpace);
		 }
		 
		 
		 log.debug("DISKSPACERUNNER ... end");
		
	}
	

	
	private ServiceManager serviceManager;
	private JobService jobService;
	private JobStatusService jobStatusService;
	private MailService mailService;
	
	public ServiceManager getServiceManager() {
		return serviceManager;
	}

	public void setServiceManager(ServiceManager serviceManager) {
		this.serviceManager = serviceManager;
		this.jobService = this.serviceManager.getJobService();
		this.jobStatusService = this.serviceManager.getJobStatusService();
		this.mailService = this.serviceManager.getMailService();
	}
	
	
	
	
	
	
}
