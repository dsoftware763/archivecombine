package com.virtualcode.schedular;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class PLVStatus {

	private PLVStatus(){
		
	}
	
	private static PLVStatus _instance;
	
	public static PLVStatus getInstance(){
		
		if(_instance==null){
			_instance=new PLVStatus();
			_instance.stubAccessMap=Collections.synchronizedMap(new HashMap<String,Integer>());
		}
		return _instance;
	}
	
	private boolean valid=false;
	
	private long  dayCounter=0;
	
	private Map<String,Integer> stubAccessMap;

	public boolean isValid() {
		return valid;
	}

	protected void setValid(boolean valid) {
		this.valid = valid;
	}

	public long getDayCounter() {
		return dayCounter;
	}

	protected void setDayCounter(long dayCounter) {
		this.dayCounter = dayCounter;
	}

	public Map<String, Integer> getStubAccessMap() {
		return stubAccessMap;
	}

	
}
