package com.virtualcode.schedular;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.virtualcode.common.PLFormatException;
import com.virtualcode.services.MailService;
import com.virtualcode.services.SystemCodeService;
import com.virtualcode.util.License;
import com.virtualcode.util.PLVManager;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.SystemCode;

@Service
public class PLVRunner implements Runnable {

	
	public void run(){
		
		List<SystemCode> codesList=sysCodeService.getSystemCodeByCodename("sh_license"); 
		PLVStatus plvStatus=PLVStatus.getInstance();
		
		if(codesList!=null && codesList.size()>0){
			
			SystemCode systemCode=codesList.get(0);
			String licenseStr=systemCode.getCodevalue();
			
			PLVManager plvManager=PLVManager.getInstance();
			License license=null;
			try{
			    license=plvManager.readLicense(licenseStr);
			}catch(PLFormatException plFormatException){
				plFormatException.printStackTrace();
				plvStatus.setValid(false);
				plvStatus.setDayCounter(-1);
				return;
			}
			
			boolean isValid=plvManager.isValidLicense(license);			
			long days=plvManager.getDaysRemaing(license.getValidUntil());
						
			System.out.println("Is Valid License---->"+isValid+"--->"+days);
			plvStatus.setDayCounter(days);
			plvStatus.setValid(isValid);
			
			if(isValid && (days>=0 && days<6) ){
			    if(license.getParameter().equalsIgnoreCase("time") || license.getParameter().equalsIgnoreCase("both")){
				   mailService.sendMail("License Expiry Notification", "Your "+license.getLicenseType()+" is going to expire in "+days+" days.<br/> Register your application to avoid the inconvenience.");
			    }
			}
			
		}else{		
		
		   plvStatus.setValid(false);
		   plvStatus.setDayCounter(-1);
		   System.out.println("NO Valid License----->");
		}
		
		removeInvalidEntries(plvStatus);		
		
	}
	
	private void removeInvalidEntries(PLVStatus plvStatus){
		
		try{
			String strToday=VCSUtil.getDateAsString(new Date());
			Map<String, Integer> stubAccessMap=plvStatus.getStubAccessMap();
			
				if(stubAccessMap.containsKey(strToday)){
					int docs=stubAccessMap.get(strToday);
					stubAccessMap.clear();
					stubAccessMap.put(strToday, docs);
				}else{
					stubAccessMap.clear();
				}
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}
	
	
	private SystemCodeService sysCodeService;
   
	public void setSysCodeService(SystemCodeService sysCodeService) {
		this.sysCodeService = sysCodeService;
	}
	
	private MailService mailService;

	public void setMailService(MailService mailService) {
		this.mailService = mailService;
	}
	
	
	
	
	
	
}
