/*
 * Copyright 2006-2012 ICEsoft Technologies Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS
 * IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either * express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */
package com.virtualcode.pdfviewer.viewer.view;

//import com.icesoft.faces.component.inputfile.FileInfo;
//import com.icesoft.faces.component.inputfile.InputFile;
//import com.icesoft.faces.context.DisposableBean;
import com.virtualcode.controller.AbstractController;

import org.icepdf.core.pobjects.fonts.FontFactory;
import com.virtualcode.pdfviewer.viewer.util.FacesUtils;
import com.virtualcode.repository.FileExplorerService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.util.DocConverter;
import com.virtualcode.util.SpringApplicationContext;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.ValueChangeEvent;
import javax.jcr.RepositoryException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import java.util.EventObject;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.awt.*;

import jcifs.smb.SmbFile;

/**
 * DocumentManager is a session scpoed bean responsible for managing the
 * state of PDF documents.  When a new document is loaded it is added to the
 * document history list.  The document state is stored for all documents
 * opened during the user session but only one document is actually kept
 * open in memory.
 *
 * @since 3.0
 */
@ManagedBean(name="documentManager")
@ViewScoped
public class DocumentManager extends AbstractController implements java.io.Serializable {

    private static final Logger logger =
            Logger.getLogger(DocumentManager.class.toString());

    // state of current document, outline, annotations, page cursor, zoom, path
    // and image export type.
    private DocumentState currentDocumentState;

    // list of demo files if present
    private static final String DEMO_DIRECTORY = "/WEB-INF/demos/";
    private static ArrayList<DocumentState> demoFilePaths;
    private int fileUploadProgress;
    private boolean uploadDialogVisibility;    
    

    // enable/disable font engine state.
    private boolean isFontEngine = true;
    private static boolean isDemo;

    // list of document history, we only keep one document open at at time
    // but we can keep a list of previous document states encase the document
    // is opened again.
    private ArrayList<DocumentState> documentStateHistory =
            new ArrayList<DocumentState>(10);

    // document cache, intended to lower memory consumption for files that
    // are open more then one session such as the demo files are.  Other
    // files are opened in a users session, so it best to clean when we can.
    private DocumentCache documentDemoCache;
    
    //To make download available on viewer
    private String documentPath;
    
    private String documentName;
    
    private boolean documentReady = false;

    /**
     * Opens the PDF document specified by the request param "documentPath".
     *
     * @param event jsf action event.
     */
    public void openDocument() {
    	String url= "common/simple_document.jsp?doc=";
    	String filename = null;

        try {

        	ServiceManager serviceManager = (ServiceManager)SpringApplicationContext.getBean("serviceManager");
        	FileExplorerService fileExplorerService = serviceManager.getFileExplorerService();
        	InputStream is = null;
        	OutputStream os = null;
//        	getHttpResponse().sendRedirect(url);
            // opens a document from based on the path information passed
            // in as a request parameter.  Called from either the document history
            // or demo folder list.
//        	if(FacesContext.getCurrentInstance().isPostback())
//        		return;
        		String documentPath = this.documentPath;// FacesUtils.getRequestParameter("documentPath");
            try {
//            	if(documentPath!=null)
//            		this.documentPath = documentPath;
            	if(!documentPath.startsWith("smb://"))
            		documentPath=com.virtualcode.util.StringUtils.decodeUrl(documentPath);
               //nodeID  =   "archive/FS/vc6.network.vcsl/Everyone/try.esp";
            	filename = documentPath.substring(documentPath.lastIndexOf("/") + 1);
//            	filename = filename.replaceAll(" ", "_").toLowerCase();
	            MessageDigest md = MessageDigest.getInstance("MD5");
	            System.out.println(filename.substring(0, filename.indexOf(".")));
	    		md.update(filename.substring(0,filename.indexOf(".")).getBytes());
	    		byte[] digest = md.digest();
	    		StringBuffer sb = new StringBuffer();
	    		for (byte b : digest) {
	    			sb.append(Integer.toHexString((int) (b & 0xff)));
	    		}
	            filename = sb.toString() + filename.substring(filename.indexOf("."));//System.currentTimeMillis() + filename.substring(filename.indexOf(".")); //to handle foreign characters

               
           } catch (Exception ex) {
               ex.printStackTrace();
           }
        	com.virtualcode.pdfviewer.flexpaper.Config config = new com.virtualcode.pdfviewer.flexpaper.Config();
        	String pathpdf = config.getConfig("path.pdf", "");// + pdfdoc;
			File f_midir = new File(pathpdf);
            if(!f_midir.isDirectory() && !f_midir.mkdirs())
            	f_midir = new File("/");
            System.out.println(pathpdf);
            if(!(filename.toLowerCase().endsWith(".pdf"))){
            	DocConverter converter = DocConverter.getInstnce();
		        String sessionId=getHttpSession().getId();
	              
	            File outputDir=new File(getContextRealPath()+"/temp/"+sessionId);
	            if(!outputDir.exists()){
	           	  outputDir.mkdirs();
	            }
	            if(documentPath.startsWith("smb://")){
	            	SmbFile remoteFile = new SmbFile(documentPath);
	            	is =  remoteFile.getInputStream();	            	
	            }else{
	            	is = fileExplorerService.downloadFile(documentPath).getStream();
	            }
	            //download to temp location
//	            MessageDigest md = MessageDigest.getInstance("MD5");
//	            System.out.println(filename.substring(0, filename.indexOf(".")));
//	    		md.update(filename.substring(0,filename.indexOf(".")).getBytes());
//	    		byte[] digest = md.digest();
//	    		StringBuffer sb = new StringBuffer();
//	    		for (byte b : digest) {
//	    			sb.append(Integer.toHexString((int) (b & 0xff)));
//	    		}
//	            filename = sb.toString() + filename.substring(filename.indexOf("."));//System.currentTimeMillis() + filename.substring(filename.indexOf(".")); //to handle foreign characters
	             os = new FileOutputStream(new File(outputDir+"/"+filename.replace(".log", ".txt")));
	             int a = 256;
	             
	             byte[] buffer = new byte[4096];
	             System.out.println("*************Before writing..."+new Date());
	             while ((a = is.read(buffer, 0, 4096)) != -1) {
	                  os.write(buffer, 0, a);             			
	             }
	             System.out.println("*************After writing..."+new Date());
	             is.close();
	             os.flush();
	             os.close();
	             
	             
	             converter.convertDocument(outputDir+"/"+filename.replace(".log", ".txt"), pathpdf+"/"+filename.replaceAll(" ", "_").substring(0, filename.lastIndexOf("."))+".pdf");
	             filename = filename.substring(0, filename.lastIndexOf("."))+".pdf";
	            
            }else{

	            System.out.println(pathpdf);
	        	
	        	
	        	
	             try {
	            	 //	filename = filename.replaceAll(" ", "_");	//replace all <spaces> (bug fix)
		            	if(documentPath.startsWith("smb://")){
		 	            	SmbFile remoteFile = new SmbFile(documentPath);
		 	            	is =  remoteFile.getInputStream();	            	
		 	            }else{
	            	 		is = fileExplorerService.downloadFile(documentPath).getStream();
		 	            }
						os = new FileOutputStream(new File(pathpdf+"/"+filename));
						
					} catch (RepositoryException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	             
	             int a = 256;
	             
	             byte[] buffer = new byte[4096];
	             System.out.println("*************Before writing..."+new Date());
	             while ((a = is.read(buffer, 0, 4096)) != -1) {
	                  os.write(buffer, 0, a);             			
	             }
	             System.out.println("*************After writing..."+new Date());
	             is.close();
	             os.flush();
	             os.close();
            }
             url+=filename;
             documentName = filename;
             
//             getHttpResponse().sendRedirect(url);
            // try to load the document
//            loadFilePath(documentPath);

            // refresh current page state.
         //   refreshDocumentState();
        } catch (Throwable e) {
            logger.log(Level.WARNING, "Error opening document.", e);
            e.printStackTrace();
        }
    }

    /**
     * Loads the specified document
     *
     * @param demoFileName name of demo file to load
     */
    public void setDocumentDemoFile(String demoFileName) {
        if (demoFilePaths == null) {
            try {
                loadDemoFiles();
            } catch (Throwable e) {
                logger.log(Level.WARNING, "Error loading demo files.", e);
            }
        }

        // check for demo file in path as an id, we reuse the document state
        for (DocumentState documentSate : demoFilePaths) {
            if (documentSate.getDocumentName().equals(demoFileName)) {
                currentDocumentState = new DocumentState(documentSate);
            }
        }

        // see if we can open the document.
        try {
            currentDocumentState.openDocument(documentDemoCache);
        } catch (Throwable e) {
            logger.log(Level.WARNING, "Error loading file default file: ", e);
        }
    }

    /**
     * Gets the upload progress bar perscent complete.  Should only be
     * called be the progress bar component.
     *
     * @param event inputFile eventObject.
     */
//    public void fileUploadProgress(EventObject event) {
//        InputFile ifile = (InputFile) event.getSource();
//        if (ifile != null) {
//            fileUploadProgress = ifile.getFileInfo().getPercent();
//        }
//    }

    /**
     * File upload event, we only listen for document that have been saved and
     * are of type .pdf.
     *
     * @param event jsf action event.
     */
//    public void fileUploadMonitor(ActionEvent event) {
//        try {
//            InputFile inputFile = (InputFile) event.getSource();
//            FileInfo fileInfo = inputFile.getFileInfo();
//            if (fileInfo.getStatus() == FileInfo.SAVED) {
//                logger.info("File UPload Path " + fileInfo.getFileName());
//                if (fileInfo.getFileName().toLowerCase().endsWith(".pdf")) {
//                    loadFilePath(fileInfo.getPhysicalPath());
//                    // refresh current page state.
//                    refreshDocumentState();
//                    FacesUtils.addInfoMessage("Successfully upload PDF Document.");
//                }
//            }
//        } catch (Throwable e) {
//            FacesUtils.addInfoMessage("Error during upload of PDF Document.");
//            logger.log(Level.WARNING,
//                    "Error opening PDF document that was uploaded."
//                            + e.getMessage(), e);
//        }
//    }

    /**
     * Stats the upload process by showing an upload dialog.  Users can either
     * chose to open a document or close the dialog.
     *
     * @param event jsf action event.
     */
    public void uploadDocument(ActionEvent event) {

        // reset file progress.
        fileUploadProgress = 0;

        // show upload dialog
        uploadDialogVisibility = true;
    }

    /**
     * Utility method which loads the PDF document specified by the document
     * path.
     *
     * @param documentPath path to PDF document.
     */
    private void loadFilePath(String documentPath) {
        // check the state history to see if this session has opened the document
        // in question before.  If so re-use document state.
        DocumentState documentState = null;
        for (DocumentState documentHistoryState : documentStateHistory) {
            if (documentPath.equals(documentHistoryState.getDocumentPath())) {
                documentState = documentHistoryState;
                break;
            }
        }
        // setup of current references so that the servlet can show the state
        // in question.
        if (documentState == null) {
            documentState = new DocumentState(documentPath);
            documentStateHistory.add(0, documentState);
        } else {
            // if the document changes then we'll close the previous one.
            // but only if it is not shared session.
            if (currentDocumentState != null &&
                    !currentDocumentState.isSharedSession() &&
                    !documentState.getDocumentName().equals(
                            currentDocumentState.getDocumentName()) ) {
                currentDocumentState.closeDocument();
            }
            // update history queue
            documentStateHistory.remove(documentState);
            documentStateHistory.add(0, documentState);
        }
        // see if we can open the document.
        try {
            documentState.openDocument(documentDemoCache);
            // assign the newly open document state.
            currentDocumentState = documentState;

        } catch (Throwable e) {
            logger.log(Level.WARNING, "Error loading file at path: " + documentPath, e);
            System.out.println("Error Loading file " + e.getMessage());
            FacesUtils.addInfoMessage("Could not open the PDF file." +
                    documentState.getDocumentName());
            // clean up and reset the viewer state. 
            if (!documentState.isSharedSession()){
                documentState.closeDocument();
            }
        }
    }

    /**
     * Updates the current document state page cursor to point to the next
     * logical page in the document.  Nothing happens if there is now documents
     * loaded or if the page cursor is at the end of the document.
     *
     * @param event jsf action event.
     */
    public void nextPage(ActionEvent event) {
        try {
            // if their is is a currentDocument then go to the next page.
            if (currentDocumentState != null) {
                int totalPages = currentDocumentState.getDocumentLength();
                int currentPage = currentDocumentState.getPageCursor();
                currentPage++;
                if (currentPage > totalPages) {
                    currentPage = totalPages;
                }
                currentDocumentState.setPageCursor(currentPage);

                // refresh current page state.
                refreshDocumentState();
            }
        } catch (Throwable e) {
            logger.log(Level.WARNING, "Error paging document.", e);
        }
    }

    /**
     * Updates the current document state page cursor to point to the previous
     * logical page in the document.  Nothing happens if there is now documents
     * loaded or if the page cursor is at the begining  of the document.
     *
     * @param event jsf action event.
     */
    public void previousPage(ActionEvent event) {
        try {
            // if their is is a currentDocument then go to the next page.
            if (currentDocumentState != null) {
                int currentPage = currentDocumentState.getPageCursor();
                currentPage--;
                if (currentPage < 1) {
                    currentPage = 1;
                }
                currentDocumentState.setPageCursor(currentPage);

                // refresh current page state.
                refreshDocumentState();
            }
        } catch (Throwable e) {
            logger.log(Level.WARNING, "Error paging document.", e);
        }
    }

    /**
     * Rotate the current document state by 90 degrees.
     *
     * @param event jsf action event.
     */
    public void rotateDocumentRight(ActionEvent event) {
        try {
            if (currentDocumentState != null) {
                float viewRotation = currentDocumentState.getRotation();
                viewRotation -= DocumentState.ROTATION_FACTOR;
                if (viewRotation < 0) {
                    viewRotation += 360;
                }
                currentDocumentState.setRotation(viewRotation);

                // refresh current page state.
                refreshDocumentState();
            }
        } catch (Throwable e) {
            logger.log(Level.WARNING,"Error rotating document.", e);
        }
    }

    /**
     * Rotate the current document state by -90 degrees.
     *
     * @param event jsf action event.
     */
    public void rotateDocumentLeft(ActionEvent event) {
        try {
            if (currentDocumentState != null) {
                float viewRotation = currentDocumentState.getRotation();
                viewRotation += DocumentState.ROTATION_FACTOR;
                viewRotation %= 360;
                currentDocumentState.setRotation(viewRotation);

                // refresh current page state.
                refreshDocumentState();
            }
        } catch (Throwable e) {
            logger.log(Level.WARNING, "Error rotating document.", e);
        }
    }

    /**
     * Gets a list of demo files located in the demo folder.
     *
     * @return list of PDF document paths in demo folder.
     */
    public ArrayList<DocumentState> getDemoFilePaths() {
        if (demoFilePaths == null) {
            try {
                loadDemoFiles();
            } catch (Throwable e) {
                logger.log(Level.WARNING, "Error loading demo files.", e);
            }
        }
        return demoFilePaths;
    }

    /**
     * Go to the page number specified by the request param "pageNumber".
     *
     * @param event JSF action event.
     */
    public void goToDestination(ActionEvent event) {
        try {
            int pageNumber = Integer.parseInt(
                    FacesUtils.getRequestParameter("pageNumber"));
            currentDocumentState.setPageCursor(pageNumber + 1);

            // refresh current page state.
            refreshDocumentState();
        } catch (Throwable e) {
            logger.log(Level.WARNING, "Error goign to specified page number.");
        }
    }

    /**
     * Go to the page number specifed by the current document state.  If the
     * page number is not in the range of the documents pages it is altered to
     * the nearest bound.
     *
     * @param event jsf action event.
     */
    public void goToPage(ActionEvent event) {
        try {
            if (currentDocumentState != null) {

                int totalPages = currentDocumentState.getDocumentLength();
                int currentPage = currentDocumentState.getPageCursor();

                if (currentPage > totalPages) {
                    currentDocumentState.setPageCursor(totalPages);
                    currentPage = totalPages;
                }
                if (currentPage < 1) {
                    currentDocumentState.setPageCursor(1);
                    currentPage = 1;
                }
                // refresh current page state.
                refreshDocumentState();
            }
        } catch (Throwable e) {
            logger.log(Level.WARNING, "Error paging document.", e);
        }
    }

     /**
     * Gets the image associated with the current document state.
     *
     * @return image represented by the pageCursor, rotation and zoom.
     */
    public Image getCurrentPageImage() {
        if (currentDocumentState != null) {
            FontFactory.getInstance().setAwtFontSubstitution(!isFontEngine);
            // invalidate the content streams, so we are paint with as close
            // to as possible the correct awt font state.
            if(isDemo){
                currentDocumentState.invalidate();
            }
            return currentDocumentState.getPageImage();
        }
        return null;
    }


    /**
     * Toggle the font engine functionality and refresht he current page view
     *
     * @param event jsf action event.
     */
    public void toggleFontEngine(ActionEvent event) {
        try {
            if (currentDocumentState != null) {

                // toggle flag.
                isFontEngine = !isFontEngine;

                // refresh current page state.
                refreshDocumentState();
            }
        } catch (Throwable e) {
            logger.log(Level.WARNING, "Error enable/disabling document.", e);
        }
    }

    /**
     * Read all PDF document that can be found in the Servlet context path +
     * DEMO_DIRECTORY .
     *
     * @throws MalformedURLException error getting file path.
     * @throws URISyntaxException    error getting file path.
     */
    public static void loadDemoFiles() throws MalformedURLException, URISyntaxException {

        // Loading of the resource must be done the "JSF way" so that
        // it is agnostic about it's environment (portlet vs servlet).
        HttpSession session = FacesUtils.getHttpSession(false);
        ServletContext context = session.getServletContext();
        String demoFilesPath = context.getRealPath(DEMO_DIRECTORY);

        // get listing of pdf files that might be in this folder.
        File directory = new File(demoFilesPath);
        String[] fontPaths;
        demoFilePaths = new ArrayList<DocumentState>(5);
        if (directory.canRead()) {
            fontPaths = directory.list();
            for (String pdfFile : fontPaths) {
                if (pdfFile != null &&
                        pdfFile.endsWith(".pdf")) {
                    demoFilePaths.add(new DocumentState(
                            directory.getAbsolutePath() + File.separatorChar +
                                    pdfFile, true));
                }
            }
        }
    }

    private void refreshDocumentState() {
        if (currentDocumentState != null) {
            // setup page size
            currentDocumentState.calculatePageImageSize();
            // refresh current image.
            currentDocumentState.generateDocumentID();
        }
    }
    
    public void downloadFile(){
    	try{    		
    	   String url=	"DownloadFile?nodeID="+documentPath;
    	   getHttpResponse().sendRedirect(url);
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    }

    
    public int getFileUploadProgress() {
        return fileUploadProgress;
    }

    public void setFileUploadProgress(int fileUploadProgress) {
        this.fileUploadProgress = fileUploadProgress;
    }

    public void documentZoomLevelChange() {
//        if (event.getPhaseId() != PhaseId.INVOKE_APPLICATION) {
//            event.setPhaseId(PhaseId.INVOKE_APPLICATION);
//            event.queue();
//        } else {
            // refresh current page state.
    		System.out.println(currentDocumentState.getZoom());
            refreshDocumentState();
//        }
    }

    public void dispose() throws Exception {
        if (currentDocumentState != null &&
                !currentDocumentState.isSharedSession()){
            currentDocumentState.closeDocument();
        }
    }

    public ArrayList<DocumentState> getDocumentStateHistory() {
        return documentStateHistory;
    }

    public DocumentState getCurrentDocumentState() {
        return currentDocumentState;
    }

    public void toggleUploadDialogVisibility(ActionEvent event) {
        uploadDialogVisibility = !uploadDialogVisibility;
    }

    public boolean isUploadDialogVisibility() {
        return uploadDialogVisibility;
    }

    public void setUploadDialogVisibility(boolean uploadDialogVisibility) {
        this.uploadDialogVisibility = uploadDialogVisibility;
    }

    public boolean isFontEngine() {
        return isFontEngine;
    }

    public void setFontEngine(boolean fontEngine) {
        isFontEngine = fontEngine;
    }

    public boolean isDemo() {
        return isDemo;
    }

    public void setDemo(boolean demo) {
        isDemo = demo;
    }

    public void setDocumentDemoCache(DocumentCache documentDemoCache) {
        this.documentDemoCache = documentDemoCache;
    }

	public String getDocumentPath() {
		return documentPath;
	}

	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}

	public boolean isDocumentReady() {
		return documentReady;
	}

	public void setDocumentReady(boolean documentReady) {
		this.documentReady = documentReady;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
}
