package com.virtualcode.dao;

import java.util.List;
import java.util.Set;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.Role;

public interface RoleDAO  {
	       
    public void save(Role transientInstance);
    
	public void delete(Role persistentInstance);
    
    public Role findById( java.lang.Long id) ;
    
    public List findByExample(Role instance);
    
    public List findByProperty(String propertyName, Object value);

	public List findAll();
	
    public Role merge(Role detachedInstance);

    public void attachDirty(Role instance);
    
    public void attachClean(Role instance);
	
}