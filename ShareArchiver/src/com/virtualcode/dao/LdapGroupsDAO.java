package com.virtualcode.dao;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.LdapGroups;

/**
 * A data access object (DAO) providing persistence and search support for
 * LdapGroups entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.virtualcode.vo.LdapGroups
 * @author MyEclipse Persistence Tools
 */

public interface LdapGroupsDAO{


	public void save(LdapGroups transientInstance);

	public void delete(LdapGroups persistentInstance);

	public LdapGroups findById(java.lang.Integer id);

	public List findByExample(LdapGroups instance);

	public List findByProperty(String propertyName, Object value);

	public List findByGroupName(Object groupName);

	public List findByRestrictGroup(Object restrictGroup);

	public List findByEnableTranscript(Object enableTranscript);

	public List findAll();

	public LdapGroups merge(LdapGroups detachedInstance);

	public void attachDirty(LdapGroups instance);

	public void attachClean(LdapGroups instance);

}