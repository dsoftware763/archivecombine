package com.virtualcode.dao;

import java.util.List;
import java.util.Set;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.Agent;


public interface AgentDAO  {
	
	public void save(Agent transientInstance);;

	public void delete(Agent persistentInstance);

	public Agent findById(java.lang.Integer id);

	public List findByExample(Agent instance);

	public List findByProperty(String propertyName, Object value);

	public List findByActive(Object active);

	public List findByDescription(Object description);
	
	public List findByLogin(Object login) ;

	public List findByName(Object name);

	public List findByPassword(Object password);
	
	public List findByType(Object type);

	public List findByAllowedIps(Object allowedIps);
	
	public List findAll() ;

	public Agent merge(Agent detachedInstance) ;

	public void attachDirty(Agent instance);
	
	public void attachClean(Agent instance) ;
	
}