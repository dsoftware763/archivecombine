package com.virtualcode.dao;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.PageRights;

/**
 * A data access object (DAO) providing persistence and search support for
 * PageRights entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.virtualcode.vo.PageRights
 * @author MyEclipse Persistence Tools
 */

public interface PageRightsDAO {

	public void save(PageRights transientInstance);

	public void delete(PageRights persistentInstance);

	public PageRights findById(java.lang.Integer id);

	public List findByExample(PageRights instance);

	public List findByProperty(String propertyName, Object value);

	public List findByPageUri(Object pageUri);

	public List findAll();

	public PageRights merge(PageRights detachedInstance);

}