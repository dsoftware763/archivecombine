package com.virtualcode.dao.impl;

import java.util.List;

import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.DriveLettersDAO;
import com.virtualcode.vo.DriveLetters;

/**
 * A data access object (DAO) providing persistence and search support for
 * DriveLetters entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.dao.impl.DriveLetters
 * @author MyEclipse Persistence Tools
 */

public class DriveLettersDAOImpl extends HibernateDaoSupport implements DriveLettersDAO {
	private static final Logger log = LoggerFactory
			.getLogger(DriveLettersDAO.class);
	// property constants
	public static final String DRIVE_LETTER = "driveLetter";
	public static final String SHARE_PATH = "sharePath";
	public static final String STATUS = "status";

	protected void initDao() {
		// do nothing
	}

	public void save(DriveLetters transientInstance) {
		log.debug("saving DriveLetters instance");
		try {
			getHibernateTemplate().saveOrUpdate(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(DriveLetters persistentInstance) {
		log.debug("deleting DriveLetters instance");
		try {
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public DriveLetters findById(java.lang.Integer id) {
		log.debug("getting DriveLetters instance with id: " + id);
		try {
			DriveLetters instance = (DriveLetters) getHibernateTemplate().get(
					"com.virtualcode.vo.DriveLetters", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(DriveLetters instance) {
		log.debug("finding DriveLetters instance by example");
		try {
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding DriveLetters instance with property: "
				+ propertyName + ", value: " + value);
		try {
			String queryString = "from DriveLetters as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByDriveLetter(Object driveLetter) {
		return findByProperty(DRIVE_LETTER, driveLetter);
	}

	public List findBySharePath(Object sharePath) {
		return findByProperty(SHARE_PATH, sharePath);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all DriveLetters instances");
		try {
			String queryString = "from DriveLetters as d where d.isFileShare=1";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public DriveLetters merge(DriveLetters detachedInstance) {
		log.debug("merging DriveLetters instance");
		try {
			DriveLetters result = (DriveLetters) getHibernateTemplate().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(DriveLetters instance) {
		log.debug("attaching dirty DriveLetters instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(DriveLetters instance) {
		log.debug("attaching clean DriveLetters instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static DriveLettersDAO getFromApplicationContext(
			ApplicationContext ctx) {
		return (DriveLettersDAO) ctx.getBean("DriveLettersDAO");
	}
}