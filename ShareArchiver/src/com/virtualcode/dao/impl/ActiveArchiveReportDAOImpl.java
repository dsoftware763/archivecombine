package com.virtualcode.dao.impl;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.ActiveArchiveReportDAO;
import com.virtualcode.vo.ActiveArchiveReport;

/**
 * A data access object (DAO) providing persistence and search support for
 * ActiveArchiveReport entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.vo.ActiveArchiveReport
 * @author MyEclipse Persistence Tools
 */

public class ActiveArchiveReportDAOImpl extends HibernateDaoSupport implements ActiveArchiveReportDAO{
	private static final Logger log = LoggerFactory
			.getLogger(ActiveArchiveReportDAOImpl.class);
	// property constants
	public static final String DAILY_COUNT = "dailyCount";
	public static final String TOTAL_COUNT = "totalCount";
	public static final String TOTAL_JOBS = "totalJobs";

	protected void initDao() {
		// do nothing
	}

	public void save(ActiveArchiveReport transientInstance) {
		log.debug("saving ActiveArchiveReport instance");
		try {
			getHibernateTemplate().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(ActiveArchiveReport persistentInstance) {
		log.debug("deleting ActiveArchiveReport instance");
		try {
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public ActiveArchiveReport findById(java.lang.Integer id) {
		log.debug("getting ActiveArchiveReport instance with id: " + id);
		try {
			ActiveArchiveReport instance = (ActiveArchiveReport) getHibernateTemplate()
					.get("com.virtualcode.vo.ActiveArchiveReport", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(ActiveArchiveReport instance) {
		log.debug("finding ActiveArchiveReport instance by example");
		try {
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding ActiveArchiveReport instance with property: "
				+ propertyName + ", value: " + value);
		try {
			String queryString = "from ActiveArchiveReport as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByDailyCount(Object dailyCount) {
		return findByProperty(DAILY_COUNT, dailyCount);
	}

	public List findByTotalCount(Object totalCount) {
		return findByProperty(TOTAL_COUNT, totalCount);
	}

	public List findByTotalJobs(Object totalJobs) {
		return findByProperty(TOTAL_JOBS, totalJobs);
	}

	public List findAll() {
		log.debug("finding all ActiveArchiveReport instances");
		try {
			String queryString = "from ActiveArchiveReport";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public ActiveArchiveReport merge(ActiveArchiveReport detachedInstance) {
		log.debug("merging ActiveArchiveReport instance");
		try {
			ActiveArchiveReport result = (ActiveArchiveReport) getHibernateTemplate()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(ActiveArchiveReport instance) {
		log.debug("attaching dirty ActiveArchiveReport instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(ActiveArchiveReport instance) {
		log.debug("attaching clean ActiveArchiveReport instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static ActiveArchiveReportDAOImpl getFromApplicationContext(
			ApplicationContext ctx) {
		return (ActiveArchiveReportDAOImpl) ctx.getBean("ActiveArchiveReportDAO");
	}
}