package com.virtualcode.dao.impl;

import java.util.List;

import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.DelPolicyDAO;
import com.virtualcode.vo.DelPolicy;

/**
 	* A data access object(DAO) providing persistence and search support for DelPolicy entities.
 			* Transaction control of the save(), update() and delete() operations 
		can directly support Spring container-managed transactions or they can be augmented	to handle user-managed Spring transactions. 
		Each of these methods provides additional information for how to configure it for the desired type of transaction control. 	
	 * @see com.virtualcode.vo.DelPolicy 
  * @author MyEclipse Persistence Tools 
 */

public class DelPolicyDAOImpl extends HibernateDaoSupport implements DelPolicyDAO  {
	private static final Logger log = LoggerFactory
			.getLogger(DelPolicyDAO.class);
	// property constants
	public static final String NAME = "name";
	public static final String MODIFIED_DATE = "modifiedDate";
	public static final String ACCESS_DATE = "accessDate";
	public static final String ARCHIVAL_DATE = "archivalDate";
	public static final String FILE_TYPE = "fileType";
	public static final String FILE_SIZE = "fileSize";

	protected void initDao() {
		// do nothing
	}

	public void save(DelPolicy transientInstance) {
		log.debug("saving DelPolicy instance");
		try {
			getHibernateTemplate().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(DelPolicy persistentInstance) {
		log.debug("deleting DelPolicy instance");
		try {
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public DelPolicy findById(java.lang.Integer id) {
		log.debug("getting DelPolicy instance with id: " + id);
		try {
			DelPolicy instance = (DelPolicy) getHibernateTemplate().get(
					"com.virtualcode.vo.DelPolicy", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(DelPolicy instance) {
		log.debug("finding DelPolicy instance by example");
		try {
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding DelPolicy instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from DelPolicy as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByName(Object name) {
		return findByProperty(NAME, name);
	}

	public List findByModifiedDate(Object modifiedDate) {
		return findByProperty(MODIFIED_DATE, modifiedDate);
	}

	public List findByAccessDate(Object accessDate) {
		return findByProperty(ACCESS_DATE, accessDate);
	}

	public List findByArchivalDate(Object archivalDate) {
		return findByProperty(ARCHIVAL_DATE, archivalDate);
	}

	public List findByFileType(Object fileType) {
		return findByProperty(FILE_TYPE, fileType);
	}

	public List findByFileSize(Object fileSize) {
		return findByProperty(FILE_SIZE, fileSize);
	}

	public List findAll() {
		log.debug("finding all DelPolicy instances");
		try {
			String queryString = "from DelPolicy";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public DelPolicy merge(DelPolicy detachedInstance) {
		log.debug("merging DelPolicy instance");
		try {
			DelPolicy result = (DelPolicy) getHibernateTemplate().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(DelPolicy instance) {
		log.debug("attaching dirty DelPolicy instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(DelPolicy instance) {
		log.debug("attaching clean DelPolicy instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}