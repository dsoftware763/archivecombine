package com.virtualcode.dao.impl;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.RepoStatsDAO;
import com.virtualcode.vo.RepoStats;

/**
 * A data access object (DAO) providing persistence and search support for
 * RepoStats entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.virtualcode.vo.RepoStats
 * @author MyEclipse Persistence Tools
 */

public class RepoStatsDAOImpl extends HibernateDaoSupport implements RepoStatsDAO{
	private static final Logger log = LoggerFactory
			.getLogger(RepoStatsDAOImpl.class);
	// property constants
	public static final String REPO_PATH = "repoPath";
	public static final String TOTAL_FOLDERS = "totalFolders";
	public static final String TOTAL_FILES = "totalFiles";
	public static final String FS_DOCS = "fsDocs";
	public static final String SP_DOCS = "spDocs";
	public static final String USED_SPACE = "usedSpace";
	public static final String DUPLICATE_DATA_SIZE = "duplicateDataSize";

	protected void initDao() {
		// do nothing
	}

	public void save(RepoStats transientInstance) {
		log.debug("saving RepoStats instance");
		try {
			getHibernateTemplate().saveOrUpdate(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(RepoStats persistentInstance) {
		log.debug("deleting RepoStats instance");
		try {
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public RepoStats findById(java.lang.Integer id) {
		log.debug("getting RepoStats instance with id: " + id);
		try {
			RepoStats instance = (RepoStats) getHibernateTemplate().get(
					"com.virtualcode.vo.RepoStats", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(RepoStats instance) {
		log.debug("finding RepoStats instance by example");
		try {
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding RepoStats instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from RepoStats as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByRepoPath(Object repoPath) {
		return findByProperty(REPO_PATH, repoPath);
	}

	public List findByTotalFolders(Object totalFolders) {
		return findByProperty(TOTAL_FOLDERS, totalFolders);
	}

	public List findByTotalFiles(Object totalFiles) {
		return findByProperty(TOTAL_FILES, totalFiles);
	}

	public List findByFsDocs(Object fsDocs) {
		return findByProperty(FS_DOCS, fsDocs);
	}

	public List findBySpDocs(Object spDocs) {
		return findByProperty(SP_DOCS, spDocs);
	}

	public List findByUsedSpace(Object usedSpace) {
		return findByProperty(USED_SPACE, usedSpace);
	}

	public List findByDuplicateDataSize(Object duplicateDataSize) {
		return findByProperty(DUPLICATE_DATA_SIZE, duplicateDataSize);
	}

	public List findAll() {
		log.debug("finding all RepoStats instances");
		try {
			String queryString = "from com.virtualcode.vo.RepoStats";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public RepoStats merge(RepoStats detachedInstance) {
		log.debug("merging RepoStats instance");
		try {
			RepoStats result = (RepoStats) getHibernateTemplate().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(RepoStats instance) {
		log.debug("attaching dirty RepoStats instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(RepoStats instance) {
		log.debug("attaching clean RepoStats instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static RepoStatsDAOImpl getFromApplicationContext(ApplicationContext ctx) {
		return (RepoStatsDAOImpl) ctx.getBean("repoStatsDAO");
	}
}