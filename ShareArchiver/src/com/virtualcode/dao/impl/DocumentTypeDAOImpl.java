package com.virtualcode.dao.impl;


import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.DocumentTypeDAO;
import com.virtualcode.vo.DocumentType;

/**
 	* A data access object (DAO) providing persistence and search support for DocumentType entities.
 			* Transaction control of the save(), update() and delete() operations 
		can directly support Spring container-managed transactions or they can be augmented	to handle user-managed Spring transactions. 
		Each of these methods provides additional information for how to configure it for the desired type of transaction control. 	
	 * @see .DocumentType
  * @author MyEclipse Persistence Tools 
 */

public class DocumentTypeDAOImpl extends HibernateDaoSupport implements DocumentTypeDAO  {
	     private static final Logger log = LoggerFactory.getLogger(DocumentTypeDAOImpl.class);
		//property constants
	public static final String NAME = "name";
	public static final String VALUE = "value";



	protected void initDao() {
		//do nothing
	}
    
    public void save(DocumentType transientInstance) {
        log.debug("saving DocumentType instance");
        try {
            getHibernateTemplate().saveOrUpdate(transientInstance);
            log.debug("save successful");
        } catch (RuntimeException re) {
            log.error("save failed", re);
            throw re;
        }
    }
    
    public void update(DocumentType transientInstance) {
        log.debug("saving DocumentType instance");
        try {
        	
            getHibernateTemplate().saveOrUpdate(transientInstance);
            log.debug("update successful");
        } catch (RuntimeException re) {
            log.error("save failed", re);
            throw re;
        }
    }
    
    
	public void delete(Integer  documentTypeId) {
        log.debug("deleting DocumentType instance");
        try {
       	 final String query="delete from com.virtualcode.vo.DocumentType dt where dt.id="+documentTypeId;
            getHibernateTemplate().execute(new HibernateCallback<Boolean>() {

				@Override
				public Boolean doInHibernate(Session session) throws HibernateException,
						SQLException {
					   
					   Query hqlQuery=session.createQuery(query);
					    int status=hqlQuery.executeUpdate();
					    System.out.println("DELETE Document Type "+status);
					    
					   return true;
					 
				}
			});
            
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }
	
	public void deleteByCategory(Integer categoryId) {
        log.debug("deleting DocumentType instance");
        try {
        	 final String query="delete from com.virtualcode.vo.DocumentType dt where dt.documentCategory.id="+categoryId;
             getHibernateTemplate().execute(new HibernateCallback<Boolean>() {

				@Override
				public Boolean doInHibernate(Session session) throws HibernateException,
						SQLException {
					   
					   Query hqlQuery=session.createQuery(query);
					    int status=hqlQuery.executeUpdate();
					    System.out.println("DELETE CATEOGRY "+status);
					    
					   return true;
					 
				}
			});
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }
    
    public DocumentType findById( java.lang.Integer id) {
        log.debug("getting DocumentType instance with id: " + id);
        try {
            DocumentType instance = (DocumentType) getHibernateTemplate()
                    .get("com.virtualcode.vo.DocumentType", id);
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    
    public List findByExample(DocumentType instance) {
        log.debug("finding DocumentType instance by example");
        try {
            List results = getHibernateTemplate().findByExample(instance);
            log.debug("find by example successful, result size: " + results.size());
            return results;
        } catch (RuntimeException re) {
            log.error("find by example failed", re);
            throw re;
        }
    }    
    
    public List findByProperty(String propertyName, Object value) {
      log.debug("finding DocumentType instance with property: " + propertyName
            + ", value: " + value);
      try {
         String queryString = "from DocumentType as model where model." 
         						+ propertyName + "= ?";
		 return getHibernateTemplate().find(queryString, value);
      } catch (RuntimeException re) {
         log.error("find by property name failed", re);
         throw re;
      }
	}

	public List findByName(Object name) {
		return findByProperty(NAME, name);
	}
	
	public List findByValue(Object value) {
		return findByProperty(VALUE, value);
	}
	

	public List findAll() {
		log.debug("finding all DocumentType instances");
		try {
			String queryString = "from DocumentType";
		 	return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}
	
    public DocumentType merge(DocumentType detachedInstance) {
        log.debug("merging DocumentType instance");
        try {
            DocumentType result = (DocumentType) getHibernateTemplate()
                    .merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public void attachDirty(DocumentType instance) {
        log.debug("attaching dirty DocumentType instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }
    
    public void attachClean(DocumentType instance) {
        log.debug("attaching clean DocumentType instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }
    
    @Override
	public List<DocumentType> findByCategoryId(Integer categoryId) {
    	log.debug("finding DocumentTypes by Category ID");
		try {
			String queryString = "from DocumentType dt where dt.documentCategory.id=?";
		 	return getHibernateTemplate().find(queryString,categoryId);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public static DocumentTypeDAOImpl getFromApplicationContext(ApplicationContext ctx) {
    	return (DocumentTypeDAOImpl) ctx.getBean("DocumentTypeDAOImpl");
	}

	
}