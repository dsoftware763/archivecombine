package com.virtualcode.dao.impl;

import java.util.List;

import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.RoleFeatureDAO;
import com.virtualcode.vo.RoleFeature;

public class RoleFeatureDAOImpl extends HibernateDaoSupport implements RoleFeatureDAO{
	private static final Logger log = LoggerFactory
			.getLogger(RoleFeatureDAOImpl.class);
	// property constants
	public static final String NAME = "name";
	public static final String ADMIN_ALLOWED = "adminAllowed";
	public static final String PRIV_ALLOWED = "privAllowed";
	public static final String LDAP_ALLOWED = "ldapAllowed";

	protected void initDao() {
		// do nothing
	}

	public void save(RoleFeature transientInstance) {
		log.debug("saving RoleFeature instance");
		try {
			getHibernateTemplate().saveOrUpdate(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}


	public RoleFeature findById(java.lang.Integer id) {
		log.debug("getting RoleFeature instance with id: " + id);
		try {
			RoleFeature instance = (RoleFeature) getHibernateTemplate().get(
					"com.virtualcode.dao.impl.RoleFeature", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding RoleFeature instance with property: "
				+ propertyName + ", value: " + value);
		try {
			String queryString = "from RoleFeature as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByName(Object name) {
		return findByProperty(NAME, name);
	}

	public List findByAdmin(Object admin) {
		return findByProperty(ADMIN_ALLOWED, admin);
	}

	public List findByPriv(Object priv) {
		return findByProperty(PRIV_ALLOWED, priv);
	}

	public List findByLdap(Object ldap) {
		return findByProperty(LDAP_ALLOWED, ldap);
	}

	public List findAll() {
		log.debug("finding all RoleFeature instances");
		try {
			String queryString = "from RoleFeature";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public RoleFeature merge(RoleFeature detachedInstance) {
		log.debug("merging RoleFeature instance");
		try {
			RoleFeature result = (RoleFeature) getHibernateTemplate().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(RoleFeature instance) {
		log.debug("attaching dirty RoleFeature instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(RoleFeature instance) {
		log.debug("attaching clean RoleFeature instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static RoleFeatureDAOImpl getFromApplicationContext(
			ApplicationContext ctx) {
		return (RoleFeatureDAOImpl) ctx.getBean("roleFeatureDAOImpl");
	}
}