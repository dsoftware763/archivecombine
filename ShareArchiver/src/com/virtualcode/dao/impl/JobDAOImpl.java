package com.virtualcode.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.JobDAO;
import com.virtualcode.vo.Job;
import com.virtualcode.vo.JobSpDocLib;

/**
 * A data access object (DAO) providing persistence and search support for Job
 * entities. Transaction control of the save(), update() and delete() operations
 * can directly support Spring container-managed transactions or they can be
 * augmented to handle user-managed Spring transactions. Each of these methods
 * provides additional information for how to configure it for the desired type
 * of transaction control.
 * 
 * @see com.virtualcode.vo.Job
 * @author MyEclipse Persistence Tools
 */

public class JobDAOImpl extends HibernateDaoSupport implements JobDAO {
	private static final Logger log = LoggerFactory.getLogger(JobDAOImpl.class);
	// property constants
	public static final String ACTION_TYPE = "actionType";
	public static final String ACTIVE = "active";
	public static final String DESCRIPTION = "description";
	public static final String NAME = "name";
	public static final String SP_SITE_ID = "spSiteId";
	public static final String SP_PATH = "spPath";
	public static final String PROCESS_CURRENT_PUBLISHED_VERSION = "processCurrentPublishedVersion";
	public static final String PROCESS_LEGACY_DRAFT_VERSION = "processLegacyDraftVersion";
	public static final String PROCESS_LEGACY_PUBLISHED_VERSION = "processLegacyPublishedVersion";
	public static final String PROCESS_ARCHIVE = "processArchive";
	public static final String PROCESS_HIDDEN = "processHidden";
	public static final String PROCESS_READ_ONLY = "processReadOnly";
	public static final String READY_TO_EXECUTE = "readyToExecute";

	protected void initDao() {
		// do nothing
	}

	public void save(Job transientInstance) {
		log.debug("saving Job instance");
		try {
			getHibernateTemplate().saveOrUpdate(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);			
		}
	}
	
	
	//	public void delete(Job persistentInstance) {
//		log.debug("deleting Job instance");
//		try {
//			
//			getHibernateTemplate().delete(persistentInstance);
//			log.debug("delete successful");
//		} catch (RuntimeException re) {
//			log.error("delete failed", re);
//			throw re;
//		}
//	}
	
	
	public void delete(Job persistentInstance) {
		log.debug("deleting Job instance");
		try {
			deleteJobExcludedPaths(persistentInstance);
			deleteJobSpDocLib(persistentInstance);
			deleteJobStatus(persistentInstance);
			deleteJobStatistics(persistentInstance);
			deleteJob(persistentInstance);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	private void deleteJob(Job persistentInstance) {
		log.debug("deleting Job instance");
		try {
			Integer jobId=persistentInstance.getId();
			
			final String queryA="delete from com.virtualcode.vo.Job job where job.id="+jobId;
			 getHibernateTemplate().execute(new HibernateCallback<Boolean>() {

				@Override
				public Boolean doInHibernate(Session session) throws HibernateException,
						SQLException {
					  Transaction tx = session.beginTransaction();
					
					   Query hqlQueryA=session.createQuery(queryA);
					   int status=hqlQueryA.executeUpdate();
					 
					    System.out.println("DELETE JOb "+status);
					    tx.commit();
					   return true;
					 
				}
			});
            
			log.debug("delete job successful");
		} catch (RuntimeException re) {
			log.error("delete job failed", re);
			throw re;
		}
	}
	
	public void deleteJobSpDocLib(Job persistentInstance) {
		log.debug("deleting Job instance");
		try {
			Integer jobId=persistentInstance.getId();
						
			final String queryB="delete from com.virtualcode.vo.JobSpDocLib lib where lib.job.id="+jobId;
			
            getHibernateTemplate().execute(new HibernateCallback<Boolean>() {

				@Override
				public Boolean doInHibernate(Session session) throws HibernateException,
						SQLException {
					  Transaction tx = session.beginTransaction();
					
					   					   
					   Query hqlQueryB=session.createQuery(queryB);
					   int status=hqlQueryB.executeUpdate();	  
					 
					    System.out.println("DELETE Doc Lib Type "+status);
					    tx.commit();
					   return true;
					 
				}
			});
            
			log.debug("delete  doclib successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}
	
	
	
	public void deleteJobExcludedPaths(Job persistentInstance) {
		log.debug("deleting Job instance");
		try {
			Integer jobId=persistentInstance.getId();			
			final String queryC="delete from com.virtualcode.vo.JobSpDocLibExcludedPath exPath where exPath.jobSpDocLib.id in(select lib.id from JobSpDocLib lib where lib.job.id= "+jobId+")";
            getHibernateTemplate().execute(new HibernateCallback<Boolean>() {

				@Override
				public Boolean doInHibernate(Session session) throws HibernateException,
						SQLException {
					  Transaction tx = session.beginTransaction();
					
					    Query hqlQueryC=session.createQuery(queryC);
					   
					   int status=hqlQueryC.executeUpdate();				   
					    System.out.println("DELETE ExPath "+status);
					    tx.commit();
					   return true;
					 
				}
			});
            
			log.debug("delete expath.. successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}
		
	public void deleteJobStatus(Job persistentInstance) {
		log.debug("deleting Job status instances");
		try {
			Integer jobId=persistentInstance.getId();
						
			final String queryB="delete from com.virtualcode.vo.JobStatus status where status.job.id="+jobId;
			
            getHibernateTemplate().execute(new HibernateCallback<Boolean>() {

				@Override
				public Boolean doInHibernate(Session session) throws HibernateException,
						SQLException {
					  Transaction tx = session.beginTransaction();
					
					   					   
					   Query hqlQueryB=session.createQuery(queryB);
					   int status=hqlQueryB.executeUpdate();	  
					 
					    System.out.println("DELETE job status Type "+status);
					    tx.commit();
					   return true;
					 
				}
			});
            
			log.debug("delete  status successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}
	
	public void deleteJobStatistics(Job persistentInstance) {
		log.debug("deleting Job statistics instances");
		try {
			Integer jobId=persistentInstance.getId();
						
			final String queryB="delete from com.virtualcode.vo.JobStatistics statistics where statistics.job.id="+jobId;
			
            getHibernateTemplate().execute(new HibernateCallback<Boolean>() {

				@Override
				public Boolean doInHibernate(Session session) throws HibernateException,
						SQLException {
					  Transaction tx = session.beginTransaction();
					
					   					   
					   Query hqlQueryB=session.createQuery(queryB);
					   int status=hqlQueryB.executeUpdate();	  
					 
					    System.out.println("DELETE job statistics Type "+status);
					    tx.commit();
					   return true;
					 
				}
			});
            
			log.debug("delete  statistics successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}	

	public com.virtualcode.vo.Job findById(java.lang.Integer id) {
		log.debug("getting Job instance with id: " + id);
		try {
			String query="from com.virtualcode.vo.Job where id=?";
			List<Job> jobsList =  getHibernateTemplate().find(query,id);
			if(jobsList!=null){
				return jobsList.get(0);
			}
			return null;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Job instance) {
		log.debug("finding Job instance by example");
		try {
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Job instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from Job as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByActionType(Object actionType) {
		return findByProperty(ACTION_TYPE, actionType);
	}

	public List findByActive(Object active) {
		return findByProperty(ACTIVE, active);
	}

	public List findByDescription(Object description) {
		return findByProperty(DESCRIPTION, description);
	}

	public List findByName(Object name) {
		return findByProperty(NAME, name);
	}

	public List findBySpSiteId(Object spSiteId) {
		return findByProperty(SP_SITE_ID, spSiteId);
	}

	public List findBySpPath(Object spPath) {
		return findByProperty(SP_PATH, spPath);
	}

	public List findByProcessCurrentPublishedVersion(
			Object processCurrentPublishedVersion) {
		return findByProperty(PROCESS_CURRENT_PUBLISHED_VERSION,
				processCurrentPublishedVersion);
	}

	public List findByProcessLegacyDraftVersion(Object processLegacyDraftVersion) {
		return findByProperty(PROCESS_LEGACY_DRAFT_VERSION,
				processLegacyDraftVersion);
	}

	public List findByProcessLegacyPublishedVersion(
			Object processLegacyPublishedVersion) {
		return findByProperty(PROCESS_LEGACY_PUBLISHED_VERSION,
				processLegacyPublishedVersion);
	}

	public List findByProcessArchive(Object processArchive) {
		return findByProperty(PROCESS_ARCHIVE, processArchive);
	}

	public List findByProcessHidden(Object processHidden) {
		return findByProperty(PROCESS_HIDDEN, processHidden);
	}

	public List findByProcessReadOnly(Object processReadOnly) {
		return findByProperty(PROCESS_READ_ONLY, processReadOnly);
	}

	public List findByReadyToExecute(Object readyToExecute) {
		return findByProperty(READY_TO_EXECUTE, readyToExecute);
	}

	public List findAll() {
		log.debug("finding all Job instances");
		try {
			String queryString = "from com.virtualcode.vo.Job ORDER BY id DESC";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public List findAllFoStatus() {
		log.debug("finding all Job instances");
		try {
			String queryString = "SELECT job from com.virtualcode.vo.JobStatus as status RIGHT OUTER JOIN status.job as job GROUP BY job.id ORDER BY MAX(status.id) DESC";
//					"SELECT job.id, job_status.id FROM com.virtualcode.vo.Job as job "+ 
//					"LEFT OUTER JOIN com.virtualcode.vo.JobStatus as job_status "+ 
//					"ON job.id=job_status.job_id "+ 
//					"GROUP BY job.id "+
//					"ORDER BY MAX(job_status.id) DESC";
			
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}
	
	public List findAllForStatus(String type) {
		log.debug("finding all Job instances");
		try {
			String queryString = "from com.virtualcode.vo.JobStatus as status RIGHT OUTER JOIN status.job as job  ";
			
			
			if(type!=null && (type.equalsIgnoreCase("FS") || type.equals("SP") ) )
				queryString+=" where job.agent.type='"+type+"' ";
			
			if(type!=null && type.equalsIgnoreCase("PS"))
				queryString+=" where job.actionType='PRINTARCH' ";
			
			queryString+=" GROUP BY job.id ORDER BY MAX(status.id) DESC";
			
			List<Job> jobsList= getHibernateTemplate().find(queryString);
			return jobsList;
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}
	
	public Job merge(Job detachedInstance) {
		log.debug("merging Job instance");
		try {
			Job result = (Job) getHibernateTemplate().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Job instance) {
		log.debug("attaching dirty Job instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Job instance) {
		log.debug("attaching clean Job instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	@Override
	public List<Job> findAgentScheduledJobs(String agentName) {
		log.debug("JobDAO Getting all scheduled jobs for agent: " + agentName);
//		System.out.println("JobDAO Getting all scheduled jobs for agent: " + agentName);
		
		//readyToExecute=false means that we are not fetching jobs those are already Running or ready to Run
		//isScheduled=true means that its a Scheduled Job (means not a one time Run_Now type job)
		String query="from com.virtualcode.vo.Job j where j.readyToExecute=false and j.agent.name='"+agentName+"' and j.isScheduled=true";
		try {
			return getHibernateTemplate().find(query);			
		} catch (RuntimeException re) {
			System.out.println("Get Agent Jobs failed"+ re);
			log.error("Get Agent Jobs failed", re);			
		}
		return null;
	}
	
	@Override
	public List<Job> findAllScheduledJobs() {
		log.debug("JobDAO Getting all scheduled jobs ");
//		System.out.println("JobDAO Getting all scheduled jobs for agent: " + agentName);
		
		//readyToExecute=false means that we are not fetching jobs those are already Running or ready to Run
		//isScheduled=true means that its a Scheduled Job (means not a one time Run_Now type job)
		String query="from com.virtualcode.vo.Job j where j.readyToExecute=false and j.isScheduled=true";
		try {
			return getHibernateTemplate().find(query);			
		} catch (RuntimeException re) {
			System.out.println("Get Scheduled Jobs failed"+ re);
			log.error("Get Scheduled Jobs failed", re);			
		}
		return null;
	}

	@Override
	public List<Job> findAgentIndexingJobs(String agentName) {
		System.out.println("JobDAO Getting Indexing jobs for agent: " + agentName);
		String query="from com.virtualcode.vo.Job j where j.actionType in('INDEXING') and j.agent.name='"+agentName+"' and j.readyToExecute=true";
		try {
			return getHibernateTemplate().find(query);			
		} catch (RuntimeException re) {
			System.out.println("Get Agent Jobs failed"+ re);
			log.error("Get Agent Jobs failed", re);			
		}
		return null;
	}
	
	@Override
	public List<Job> findAgentImportJobs(String agentName) {
		log.debug("JobDAO Getting archive jobs for agent: " + agentName);
//		System.out.println("JobDAO Getting archive jobs for agent: " + agentName);
		String query="from com.virtualcode.vo.Job j where j.actionType in('ARCHIVE','STUB','WITHOUTSTUB','EVALUATE') and j.agent.name='"+agentName+"' and j.readyToExecute=true";
		try {
			return getHibernateTemplate().find(query);			
		} catch (RuntimeException re) {
			System.out.println("Get Agent Jobs failed"+ re);
			log.error("Get Agent Jobs failed", re);			
		}
		return null;
	}
	

	@Override
	public List<Job> findAgentRetensionJobs(String agentName) {
		log.debug("JobDAO Getting retension jobs for agent: " + agentName);
		String query="from com.virtualcode.vo.Job j where j.actionType in('RETENSION') and j.agent.name='"+agentName+"' and j.readyToExecute=true";
		try {
			return getHibernateTemplate().find(query);			
		} catch (RuntimeException re) {
			log.error("Get Agent Jobs failed", re);			
		}
		return null;
	}
	
	@Override
	public List<Job> findAgentRestoreJobs(String agentName) {
		log.debug("JobDAO Getting restore jobs for agent: " + agentName);
		String query="from com.virtualcode.vo.Job j where j.actionType in('RESTORE') and j.agent.name='"+agentName+"' and j.readyToExecute=true";
		try {
			return getHibernateTemplate().find(query);			
		} catch (RuntimeException re) {
			log.error("Get Agent Jobs failed", re);			
		}
		return null;
	}
	
	@Override
	public List<Job> findAgentExportJobs(String agentName) {
		log.debug("JobDAO Getting export jobs for agent: " + agentName);
		String query="from com.virtualcode.vo.Job j where j.actionType in('EXPORT','EXPORTSTUB','EXPORTSEARCHED') and j.agent.name='"+agentName+"' and j.readyToExecute=true";
		try {
			return getHibernateTemplate().find(query);			
		} catch (RuntimeException re) {
			log.error("Get Agent Jobs failed", re);			
		}
		return null;
	}

	@Override
	public List<Job> findAgentActiveArchivingJobs(String agentName) {
		log.debug("JobDAO Getting archive jobs for agent: " + agentName);
//		System.out.println("JobDAO Getting archive jobs for agent: " + agentName);
		String query="from com.virtualcode.vo.Job j where j.actionType in('ACTIVEARCH','PRINTARCH') and j.agent.name='"+agentName+"' and j.readyToExecute=true";
		try {
			return getHibernateTemplate().find(query);			
		} catch (RuntimeException re) {
			System.out.println("Get Agent Jobs failed"+ re);
			log.error("Get Agent Jobs failed", re);			
		}
		return null;
	}
	
	@Override
	public List<Job> findAgentPrintArchivingJobs(String agentName) {
		log.debug("JobDAO Getting export jobs for agent: " + agentName);
		String query="from com.virtualcode.vo.Job j where j.actionType in('PRINTARCH','PRINTARCH') and j.agent.name='"+agentName+"' and j.readyToExecute=true and j.currentStatus in ('RUN', 'PAUSE', 'CANCEL')";
		try {
			return getHibernateTemplate().find(query);			
		} catch (RuntimeException re) {
			log.error("Get Agent Jobs failed", re);			
		}
		return null;
	}
	
	public List<Job> findAllActiveArchivingJobs() {
		log.debug("JobDAO Getting all active jobs for agent: ");
		String query="from com.virtualcode.vo.Job j where j.actionType in('ACTIVEARCH')";
		try {
			return getHibernateTemplate().find(query);			
		} catch (RuntimeException re) {
			log.error("Get Agent Jobs failed", re);			
		}
		return null;
	}
	
	@Override
	public boolean updateJobExecutionStatus(final Integer jobStatusId,final String status) {
		log.debug("deleting Job instance");
		try {
									
			final String queryB="update com.virtualcode.vo.JobStatus status set status.status='"+status+"' where status.id='"+jobStatusId+"'";
			
            getHibernateTemplate().execute(new HibernateCallback<Boolean>() {

				@Override
				public Boolean doInHibernate(Session session) throws HibernateException,SQLException {
					   
					   Transaction tx = session.beginTransaction();
					   Query hqlQueryB=session.createQuery(queryB);
					   int status=hqlQueryB.executeUpdate();	  
					 
					    System.out.println("Blocking Job Status ID---> "+jobStatusId);
					    tx.commit();
					   return true;
					 
				}
			});
            
			log.debug("update job status successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
		return false;
	}
	

	
	
	@Override
	public List<Integer> findCancelJobs() {
		String query="select j.job.id from com.virtualcode.vo.JobStatus as j where j.status='CANCEL'";
		try {
			return getHibernateTemplate().find(query);			
		} catch (RuntimeException re) {
			log.error("Get Agent Jobs failed", re);			
		}
		return null;
	}
	
	public List<Job> findAgentProactiveJobs(String agentName) {
		log.debug("JobDAO Getting dynamic archiving jobs for agent: " + agentName);
//		System.out.println("JobDAO Getting dynamic archiving jobs for agent: " + agentName);
		String query="from com.virtualcode.vo.Job j where j.actionType in('DYNARCH') and j.agent.name='"+agentName+"' and j.readyToExecute=true";
		try {
			return getHibernateTemplate().find(query);			
		} catch (RuntimeException re) {
			System.out.println("Get Agent Jobs failed"+ re);
			log.error("Get Agent Jobs failed", re);			
		}
		return null;
	}
	
	
	public List<JobSpDocLib> findShareThreshholdJobs(String sharePath) {
		log.debug("JobDAO Getting processable threshhold jobs for sharepath: " + sharePath);
//		System.out.println("JobDAO Getting all scheduled jobs for agent: " + agentName);
		
		//readyToExecute=false means that we are not fetching jobs those are already Running or ready to Run
		//isScheduled=true means that its a Scheduled Job (means not a one time Run_Now type job)
		String query="from com.virtualcode.vo.JobSpDocLib as lib ";
		 query+= " where lib.job.readyToExecute=false and lib.job.threshholdJob=true  and lower(lib.name)='"+sharePath.toLowerCase()+"'";
		try {
			return getHibernateTemplate().find(query);			
		} catch (RuntimeException re) {
			 re.printStackTrace();
			 log.error("Get Threshhold Jobs failed", re);			
		}
		return null;
	}
	
	
	public List<JobSpDocLib> findShareAllThreshholdJobs(String sharePath) {
		log.debug("JobDAO Getting all threshhold jobs for sharepath: " + sharePath);
//		System.out.println("JobDAO Getting all scheduled jobs for agent: " + agentName);
		
		//readyToExecute=false means that we are not fetching jobs those are already Running or ready to Run
		//isScheduled=true means that its a Scheduled Job (means not a one time Run_Now type job)
		String query="from com.virtualcode.vo.JobSpDocLib as lib ";
		 query+= " where lib.job.threshholdJob=true  and lower(lib.name)='"+sharePath.toLowerCase()+"'";
		try {
			return getHibernateTemplate().find(query);			
		} catch (RuntimeException re) {
			 re.printStackTrace();
			 log.error("Get Threshhold Jobs failed", re);			
		}
		return null;
	}
	
	public List findAllActiveImportJobs(){
		
		log.debug("JobDAO Getting all archive import jobs for ");
//		System.out.println("JobDAO Getting archive jobs for agent: " + agentName);
		String query="from com.virtualcode.vo.Job j where j.actionType in('ARCHIVE','STUB','WITHOUTSTUB') and j.readyToExecute=true";
		try {
			return getHibernateTemplate().find(query);			
		} catch (RuntimeException re) {
			System.out.println("Get Agent Jobs failed"+ re);
			log.error("Get Agent Jobs failed", re);			
		}
		return null;

	}
	
	public List findAllActiveJobs(){
		log.debug("JobDAO Getting all archive jobs for ");
//		System.out.println("JobDAO Getting archive jobs for agent: " + agentName);
		String query="from com.virtualcode.vo.Job j where j.actionType not in('EXPORT') and j.readyToExecute=true";
		try {
			return getHibernateTemplate().find(query);			
		} catch (RuntimeException re) {
			System.out.println("Get Agent Jobs failed"+ re);
			log.error("Get Agent Jobs failed", re);			
		}
		return null;
	}
	
	
	
	
}