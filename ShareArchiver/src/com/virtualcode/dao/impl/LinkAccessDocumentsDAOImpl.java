package com.virtualcode.dao.impl;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.LinkAccessDocumentsDAO;
import com.virtualcode.vo.LinkAccessDocuments;

/**
 * A data access object (DAO) providing persistence and search support for
 * LinkAccessDocuments entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.vo.LinkAccessDocuments
 * @author MyEclipse Persistence Tools
 */

public class LinkAccessDocumentsDAOImpl extends HibernateDaoSupport implements com.virtualcode.dao.LinkAccessDocumentsDAO{
	private static final Logger log = LoggerFactory
			.getLogger(LinkAccessDocumentsDAOImpl.class);
	// property constants
	public static final String UUID_DOCUMENTS = "uuidDocuments";

	protected void initDao() {
		// do nothing
	}

	public void save(LinkAccessDocuments transientInstance) {
		log.debug("saving LinkAccessDocuments instance");
		try {
			getHibernateTemplate().saveOrUpdate(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(LinkAccessDocuments persistentInstance) {
		log.debug("deleting LinkAccessDocuments instance");
		try {
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public LinkAccessDocuments findById(java.lang.Integer id) {
		log.debug("getting LinkAccessDocuments instance with id: " + id);
		try {
			LinkAccessDocuments instance = (LinkAccessDocuments) getHibernateTemplate()
					.get("com.virtualcode.vo.LinkAccessDocuments", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(LinkAccessDocuments instance) {
		log.debug("finding LinkAccessDocuments instance by example");
		try {
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding LinkAccessDocuments instance with property: "
				+ propertyName + ", value: " + value);
		try {
			String queryString = "from com.virtualcode.vo.LinkAccessDocuments as model where model."
					+ propertyName + "= ? order by model.sharedOn desc";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}
	
	public List findByUuidAndUser(String user, String uuid) {
		log.debug("finding LinkAccessDocuments instance with property: "
				+ uuid + ", value: " + user);
		try {
			String queryString = "from com.virtualcode.vo.LinkAccessDocuments as model where model.linkAccessUsers.id= ? " +
					"and model.uuidDocuments=?";
			Integer userid = new Integer(user);
			return getHibernateTemplate().find(queryString, userid, uuid);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUuidDocuments(Object uuidDocuments) {
		return findByProperty(UUID_DOCUMENTS, uuidDocuments);
	}

	public List findAll() {
		log.debug("finding all LinkAccessDocuments instances");
		try {
			String queryString = "from com.virtualcode.vo.LinkAccessDocuments";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public LinkAccessDocuments merge(LinkAccessDocuments detachedInstance) {
		log.debug("merging LinkAccessDocuments instance");
		try {
			LinkAccessDocuments result = (LinkAccessDocuments) getHibernateTemplate()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(LinkAccessDocuments instance) {
		log.debug("attaching dirty LinkAccessDocuments instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(LinkAccessDocuments instance) {
		log.debug("attaching clean LinkAccessDocuments instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static LinkAccessDocumentsDAO getFromApplicationContext(
			ApplicationContext ctx) {
		return (LinkAccessDocumentsDAO) ctx.getBean("LinkAccessDocumentsDAO");
	}
}