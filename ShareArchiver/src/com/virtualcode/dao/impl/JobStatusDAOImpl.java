package com.virtualcode.dao.impl;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.JobStatusDAO;
import com.virtualcode.vo.JobStatus;

/**
 * A data access object (DAO) providing persistence and search support for
 * JobStatus entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.virtualcode.vo.JobStatus
 * @author MyEclipse Persistence Tools
 */

public class JobStatusDAOImpl extends HibernateDaoSupport implements JobStatusDAO{
	private static final Logger log = LoggerFactory
			.getLogger(JobStatusDAOImpl.class);
	// property constants
	public static final String DESCRIPTION = "description";
	public static final String IS_CURRENT = "isCurrent";
	public static final String PREVIOUS_JOB_STATUS_ID = "previousJobStatusId";
	public static final String STATUS = "status";
	public static final String EXECUTION_ID = "executionId";
	public static final String JOB = "job.id";

	protected void initDao() {
		// do nothing
	}

	public void save(JobStatus transientInstance) {
		log.debug("saving JobStatus instance");
		System.out.println("saving JobStatus instance");
		try {
			getHibernateTemplate().saveOrUpdate(transientInstance);
			//getHibernateTemplate().flush();
			log.debug("save successful");
			System.out.println("saving JobStatus instance success");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(JobStatus persistentInstance) {
		log.debug("deleting JobStatus instance");
		try {
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public JobStatus findById(java.lang.Integer id) {
		log.debug("getting JobStatus instance with id: " + id);
		try {
			JobStatus instance = (JobStatus) getHibernateTemplate().get(
					"com.virtualcode.vo.JobStatus", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(JobStatus instance) {
		log.debug("finding JobStatus instance by example");
		try {
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding JobStatus instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from JobStatus as model where model."
					+ propertyName + "= ? ORDER BY model.id DESC";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByDescription(Object description) {
		return findByProperty(DESCRIPTION, description);
	}

	public List findByIsCurrent(Object isCurrent) {
		return findByProperty(IS_CURRENT, isCurrent);
	}

	public List findByPreviousJobStatusId(Object previousJobStatusId) {
		return findByProperty(PREVIOUS_JOB_STATUS_ID, previousJobStatusId);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findByExecutionId(Object executionId) {
		return findByProperty(EXECUTION_ID, executionId);
	}

	public List findAll() {
		log.debug("finding all JobStatus instances");
		try {
			String queryString = "from JobStatus";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public JobStatus merge(JobStatus detachedInstance) {
		log.debug("merging JobStatus instance");
		try {
			JobStatus result = (JobStatus) getHibernateTemplate().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(JobStatus instance) {
		log.debug("attaching dirty JobStatus instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(JobStatus instance) {
		log.debug("attaching clean JobStatus instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static JobStatusDAOImpl getFromApplicationContext(ApplicationContext ctx) {
		return (JobStatusDAOImpl) ctx.getBean("JobStatusDAO");
	}

	@Override
	public List findByJob(Object jobId) {
		// TODO Auto-generated method stub
		//return findByProperty(JOB, jobId);
		log.debug("finding JobStatus by Job with jobID: " + jobId);
//		System.out.println("finding JobStatus by Job with jobID: " + jobId);
		try {
			String queryString = "from JobStatus as model where model.job.id = ? ORDER BY model.id DESC";
			List statusList = getHibernateTemplate().find(queryString, jobId);
//			System.out.println("size in status dao for jobID: "+jobId +" is " + statusList.size());
			return getHibernateTemplate().find(queryString, jobId);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}
}