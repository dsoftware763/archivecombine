package com.virtualcode.dao.impl;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.LdapGroupsDAO;
import com.virtualcode.vo.LdapGroups;

/**
 * A data access object (DAO) providing persistence and search support for
 * LdapGroups entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.virtualcode.vo.LdapGroups
 * @author MyEclipse Persistence Tools
 */

public class LdapGroupsDAOImpl extends HibernateDaoSupport implements LdapGroupsDAO{
	private static final Logger log = LoggerFactory
			.getLogger(LdapGroupsDAOImpl.class);
	// property constants
	public static final String GROUP_NAME = "groupName";
	public static final String RESTRICT_GROUP = "restrictGroup";
	public static final String ENABLE_TRANSCRIPT = "enableTranscript";

	protected void initDao() {
		// do nothing
	}

	public void save(LdapGroups transientInstance) {
		log.debug("saving LdapGroups instance");
		try {
			getHibernateTemplate().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(LdapGroups persistentInstance) {
		log.debug("deleting LdapGroups instance");
		try {
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public LdapGroups findById(java.lang.Integer id) {
		log.debug("getting LdapGroups instance with id: " + id);
		try {
			LdapGroups instance = (LdapGroups) getHibernateTemplate().get(
					"com.virtualcode.vo.LdapGroups", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(LdapGroups instance) {
		log.debug("finding LdapGroups instance by example");
		try {
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding LdapGroups instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from com.virtualcode.vo.LdapGroups as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByGroupName(Object groupName) {
		return findByProperty(GROUP_NAME, groupName);
	}

	public List findByRestrictGroup(Object restrictGroup) {
		return findByProperty(RESTRICT_GROUP, restrictGroup);
	}

	public List findByEnableTranscript(Object enableTranscript) {
		return findByProperty(ENABLE_TRANSCRIPT, enableTranscript);
	}

	public List findAll() {
		log.debug("finding all LdapGroups instances");
		try {
			String queryString = "from com.virtualcode.vo.LdapGroups";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public LdapGroups merge(LdapGroups detachedInstance) {
		log.debug("merging LdapGroups instance");
		try {
			LdapGroups result = (LdapGroups) getHibernateTemplate().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(LdapGroups instance) {
		log.debug("attaching dirty LdapGroups instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(LdapGroups instance) {
		log.debug("attaching clean LdapGroups instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static LdapGroupsDAOImpl getFromApplicationContext(ApplicationContext ctx) {
		return (LdapGroupsDAOImpl) ctx.getBean("ldapGroupsDAO");
	}
}