package com.virtualcode.dao.impl;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.SystemAlertsDAO;
import com.virtualcode.vo.SystemAlerts;

/**
 * A data access object (DAO) providing persistence and search support for
 * SystemAlerts entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.vo.SystemAlerts
 * @author MyEclipse Persistence Tools
 */

public class SystemAlertsDAOImpl extends HibernateDaoSupport implements SystemAlertsDAO{
	private static final Logger log = LoggerFactory
			.getLogger(SystemAlertsDAOImpl.class);
	// property constants
	public static final String USERNAME = "username";
	public static final String ALERT_TYPE = "alertType";
	public static final String ALERT_MESSAGE = "alertMessage";

	protected void initDao() {
		// do nothing
	}

	public void save(SystemAlerts transientInstance) {
		log.debug("saving SystemAlerts instance");
		try {
			getHibernateTemplate().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(SystemAlerts persistentInstance) {
		log.debug("deleting SystemAlerts instance");
		try {
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public SystemAlerts findById(java.lang.Integer id) {
		log.debug("getting SystemAlerts instance with id: " + id);
		try {
			SystemAlerts instance = (SystemAlerts) getHibernateTemplate().get(
					"com.virtualcode.vo.SystemAlerts", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(SystemAlerts instance) {
		log.debug("finding SystemAlerts instance by example");
		try {
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding SystemAlerts instance with property: "
				+ propertyName + ", value: " + value);
		try {
			String queryString = "from SystemAlerts as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUsername(Object username) {
		return findByProperty(USERNAME, username);
	}

	public List findByAlertType(Object alertType) {
		return findByProperty(ALERT_TYPE, alertType);
	}

	public List findByAlertMessage(Object alertMessage) {
		return findByProperty(ALERT_MESSAGE, alertMessage);
	}

	public List findAll() {
		log.debug("finding all SystemAlerts instances");
		try {
			String queryString = "from SystemAlerts";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public SystemAlerts merge(SystemAlerts detachedInstance) {
		log.debug("merging SystemAlerts instance");
		try {
			SystemAlerts result = (SystemAlerts) getHibernateTemplate().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(SystemAlerts instance) {
		log.debug("attaching dirty SystemAlerts instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(SystemAlerts instance) {
		log.debug("attaching clean SystemAlerts instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static SystemAlertsDAOImpl getFromApplicationContext(
			ApplicationContext ctx) {
		return (SystemAlertsDAOImpl) ctx.getBean("systemAlertsDAO");
	}
}