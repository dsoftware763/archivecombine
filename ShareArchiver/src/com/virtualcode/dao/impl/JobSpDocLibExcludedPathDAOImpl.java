package com.virtualcode.dao.impl;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.JobSpDocLibDAO;
import com.virtualcode.dao.JobSpDocLibExcludedPathDAO;
import com.virtualcode.vo.JobSpDocLibExcludedPath;

/**
 * A data access object (DAO) providing persistence and search support for
 * JobSpDocLibExcludedPath entities. Transaction control of the save(), update()
 * and delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.dao.impl.JobSpDocLibExcludedPath
 * @author MyEclipse Persistence Tools
 */

public class JobSpDocLibExcludedPathDAOImpl extends HibernateDaoSupport implements JobSpDocLibExcludedPathDAO {
	private static final Logger log = LoggerFactory
			.getLogger(JobSpDocLibExcludedPathDAOImpl.class);
	// property constants
	public static final String PATH = "path";

	protected void initDao() {
		// do nothing
	}

	public void save(JobSpDocLibExcludedPath transientInstance) {
		log.debug("saving JobSpDocLibExcludedPath instance");
		try {
			getHibernateTemplate().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(JobSpDocLibExcludedPath persistentInstance) {
		log.debug("deleting JobSpDocLibExcludedPath instance");
		try {
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public JobSpDocLibExcludedPath findById(java.lang.Integer id) {
		log.debug("getting JobSpDocLibExcludedPath instance with id: " + id);
		try {
			JobSpDocLibExcludedPath instance = (JobSpDocLibExcludedPath) getHibernateTemplate()
					.get("com.virtualcode.dao.impl.JobSpDocLibExcludedPath", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(JobSpDocLibExcludedPath instance) {
		log.debug("finding JobSpDocLibExcludedPath instance by example");
		try {
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding JobSpDocLibExcludedPath instance with property: "
				+ propertyName + ", value: " + value);
		try {
			String queryString = "from JobSpDocLibExcludedPath as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByPath(Object path) {
		return findByProperty(PATH, path);
	}

	public List findAll() {
		log.debug("finding all JobSpDocLibExcludedPath instances");
		try {
			String queryString = "from JobSpDocLibExcludedPath";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public JobSpDocLibExcludedPath merge(
			JobSpDocLibExcludedPath detachedInstance) {
		log.debug("merging JobSpDocLibExcludedPath instance");
		try {
			JobSpDocLibExcludedPath result = (JobSpDocLibExcludedPath) getHibernateTemplate()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(JobSpDocLibExcludedPath instance) {
		log.debug("attaching dirty JobSpDocLibExcludedPath instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(JobSpDocLibExcludedPath instance) {
		log.debug("attaching clean JobSpDocLibExcludedPath instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static JobSpDocLibExcludedPathDAOImpl getFromApplicationContext(
			ApplicationContext ctx) {
		return (JobSpDocLibExcludedPathDAOImpl) ctx
				.getBean("JobSpDocLibExcludedPathDAO");
	}
}