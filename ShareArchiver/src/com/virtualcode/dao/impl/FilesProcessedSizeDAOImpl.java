package com.virtualcode.dao.impl;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.FilesProcessedSizeDAO;
import com.virtualcode.vo.FilesProcessedSize;

/**
 * A data access object (DAO) providing persistence and search support for
 * FilesProcessedSize entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.vo.FilesProcessedSize
 * @author MyEclipse Persistence Tools
 */

public class FilesProcessedSizeDAOImpl  extends HibernateDaoSupport implements FilesProcessedSizeDAO{
	private static final Logger log = LoggerFactory
			.getLogger(FilesProcessedSizeDAO.class);
	// property constants
	public static final String TOTAL_PROCESSED_FILES = "totalProcessedFiles";

	protected void initDao() {
		// do nothing
	}

	public void save(FilesProcessedSize transientInstance) {
		log.debug("saving FilesProcessedSize instance");
		try {
			getHibernateTemplate().saveOrUpdate(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(FilesProcessedSize persistentInstance) {
		log.debug("deleting FilesProcessedSize instance");
		try {
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public FilesProcessedSize findById(java.lang.Integer id) {
		log.debug("getting FilesProcessedSize instance with id: " + id);
		try {
			FilesProcessedSize instance = (FilesProcessedSize) getHibernateTemplate()
					.get("com.virtualcode.vo.FilesProcessedSize", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(FilesProcessedSize instance) {
		log.debug("finding FilesProcessedSize instance by example");
		try {
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding FilesProcessedSize instance with property: "
				+ propertyName + ", value: " + value);
		try {
			String queryString = "from com.virtualcode.vo.FilesProcessedSize as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByTotalProcessedFiles(Object totalProcessedFiles) {
		return findByProperty(TOTAL_PROCESSED_FILES, totalProcessedFiles);
	}

	public List findAll() {
		log.debug("finding all FilesProcessedSize instances");
		try {
			String queryString = "from com.virtualcode.vo.FilesProcessedSize";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public FilesProcessedSize merge(FilesProcessedSize detachedInstance) {
		log.debug("merging FilesProcessedSize instance");
		try {
			FilesProcessedSize result = (FilesProcessedSize) getHibernateTemplate()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(FilesProcessedSize instance) {
		log.debug("attaching dirty FilesProcessedSize instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(FilesProcessedSize instance) {
		log.debug("attaching clean FilesProcessedSize instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static FilesProcessedSizeDAO getFromApplicationContext(
			ApplicationContext ctx) {
		return (FilesProcessedSizeDAO) ctx.getBean("FilesProcessedSizeDAO");
	}
}