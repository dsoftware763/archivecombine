package com.virtualcode.dao.impl;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.UserPermissionsDAO;
import com.virtualcode.vo.UserPermissions;
import com.virtualcode.vo.UserPermissionsId;

/**
 	* A data access object (DAO) providing persistence and search support for UserPermissions entities.
 			* Transaction control of the save(), update() and delete() operations 
		can directly support Spring container-managed transactions or they can be augmented	to handle user-managed Spring transactions. 
		Each of these methods provides additional information for how to configure it for the desired type of transaction control. 	
	 * @see com.virtualcode.vo.UserPermissions
  * @author MyEclipse Persistence Tools 
 */

public class UserPermissionsDAOImpl extends HibernateDaoSupport implements UserPermissionsDAO  {
	     private static final Logger log = LoggerFactory.getLogger(UserPermissionsDAOImpl.class);
		//property constants



	protected void initDao() {
		//do nothing
	}
    
    public void save(UserPermissions transientInstance) {
        log.debug("saving UserPermissions instance");
        try {
            getHibernateTemplate().save(transientInstance);
            log.debug("save successful");
        } catch (RuntimeException re) {
            log.error("save failed", re);
            throw re;
        }
    }
    
	public void delete(UserPermissions persistentInstance) {
        log.debug("deleting UserPermissions instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }
    
    public UserPermissions findById( com.virtualcode.vo.UserPermissionsId id) {
        log.debug("getting UserPermissions instance with id: " + id);
        try {
            UserPermissions instance = (UserPermissions) getHibernateTemplate()
                    .get("com.virtualcode.dao.UserPermissions", id);
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    
    public List findByExample(UserPermissions instance) {
        log.debug("finding UserPermissions instance by example");
        try {
            List results = getHibernateTemplate().findByExample(instance);
            log.debug("find by example successful, result size: " + results.size());
            return results;
        } catch (RuntimeException re) {
            log.error("find by example failed", re);
            throw re;
        }
    }    
    
    public List findByProperty(String propertyName, Object value) {
      log.debug("finding UserPermissions instance with property: " + propertyName
            + ", value: " + value);
      try {
         String queryString = "from UserPermissions as model where model." 
         						+ propertyName + "= ?";
		 return getHibernateTemplate().find(queryString, value);
      } catch (RuntimeException re) {
         log.error("find by property name failed", re);
         throw re;
      }
	}


	public List findAll() {
		log.debug("finding all UserPermissions instances");
		try {
			String queryString = "from UserPermissions";
		 	return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}
	
    public UserPermissions merge(UserPermissions detachedInstance) {
        log.debug("merging UserPermissions instance");
        try {
            UserPermissions result = (UserPermissions) getHibernateTemplate()
                    .merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public void attachDirty(UserPermissions instance) {
        log.debug("attaching dirty UserPermissions instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }
    
    public void attachClean(UserPermissions instance) {
        log.debug("attaching clean UserPermissions instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

	public static UserPermissionsDAOImpl getFromApplicationContext(ApplicationContext ctx) {
    	return (UserPermissionsDAOImpl) ctx.getBean("UserPermissionsDAO");
	}
}