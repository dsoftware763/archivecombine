package com.virtualcode.dao.impl;
// default package

import java.util.List;
import java.util.Set;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.UserDAO;
import com.virtualcode.vo.User;

/**
 	* A data access object (DAO) providing persistence and search support for User entities.
 			* Transaction control of the save(), update() and delete() operations 
		can directly support Spring container-managed transactions or they can be augmented	to handle user-managed Spring transactions. 
		Each of these methods provides additional information for how to configure it for the desired type of transaction control. 	
	 * @see .User
  * @author MyEclipse Persistence Tools 
 */

public class UserDAOImpl extends HibernateDaoSupport implements UserDAO {
	     private static final Logger log = LoggerFactory.getLogger(UserDAOImpl.class);
		//property constants
	public static final String EMAIL = "email";
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String LOCK_STATUS = "lockStatus";
	public static final String PASSWORD_HASH = "passwordHash";
	public static final String USERNAME = "username";



	protected void initDao() {
		//do nothing
	}
    
    public void save(User transientInstance) {
        log.debug("saving User instance");
        try {
            getHibernateTemplate().saveOrUpdate(transientInstance);
            log.debug("save successful");
        } catch (RuntimeException re) {
            log.error("save failed", re);
            throw re;
        }
    }
    
	public void delete(User persistentInstance) {
        log.debug("deleting User instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }
    
    public User findById( java.lang.Long id) {
        log.debug("getting User instance with id: " + id);
        try {
            User instance = (User) getHibernateTemplate()
                    .get("com.virtualcode.vo.User", id);
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    
    public List findByExample(User instance) {
        log.debug("finding User instance by example");
        try {
            List results = getHibernateTemplate().findByExample(instance);
            log.debug("find by example successful, result size: " + results.size());
            return results;
        } catch (RuntimeException re) {
            log.error("find by example failed", re);
            throw re;
        }
    }    
    
    public List findByProperty(String propertyName, Object value) {
      log.debug("finding User instance with property: " + propertyName
            + ", value: " + value);
      try {
         String queryString = "from User as model where model." 
         						+ propertyName + "= ?";
		 return getHibernateTemplate().find(queryString, value);
      } catch (RuntimeException re) {
         log.error("find by property name failed", re);
         throw re;
      }
	}

	public List findByEmail(Object email) {
		return findByProperty(EMAIL, email);
	}
	
	public List findByFirstName(Object firstName) {
		return findByProperty(FIRST_NAME, firstName	);
	}
	
	public List findByLastName(Object lastName) {
		return findByProperty(LAST_NAME, lastName );
	}
	
	public List findByLockStatus(Object lockStatus ) {
		return findByProperty(LOCK_STATUS, lockStatus
		);
	}
	
	public List findByPasswordHash(Object passwordHash) {
		return findByProperty(PASSWORD_HASH, passwordHash);
	}
	
	public List findByUsername(Object username) {
		return findByProperty(USERNAME, username);
	}
	
	public User findUser(String username,String password) {
		log.debug("finding user :"+username);
		try{
		List users = getHibernateTemplate().findByNamedParam("select u from User as u where lower(u.username)=:username and u.passwordHash=:password", new String[] { "username", "password" }, new Object[] { username, password });
		if (users!=null && users.size() == 1){
		   return (User) users.get(0);
		}
		
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return null;		
	}
	

	public List<User> findAll() {
		log.debug("finding all User instances");
		try {
			String queryString = "from User";
		 	return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}
	
    public User merge(User detachedInstance) {
        log.debug("merging User instance");
        try {
            User result = (User) getHibernateTemplate()
                    .merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public void attachDirty(User instance) {
        log.debug("attaching dirty User instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }
    
    public void attachClean(User instance) {
        log.debug("attaching clean User instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

	public static UserDAOImpl getFromApplicationContext(ApplicationContext ctx) {
    	return (UserDAOImpl) ctx.getBean("UserDAO");
	}
}