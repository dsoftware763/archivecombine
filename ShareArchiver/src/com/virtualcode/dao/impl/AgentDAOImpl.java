package com.virtualcode.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.AgentDAO;
import com.virtualcode.vo.Agent;

/**
 * A data access object (DAO) providing persistence and search support for Agent
 * entities. Transaction control of the save(), update() and delete() operations
 * can directly support Spring container-managed transactions or they can be
 * augmented to handle user-managed Spring transactions. Each of these methods
 * provides additional information for how to configure it for the desired type
 * of transaction control.
 * 
 * @see com.virtualcode.vo.Agent
 * @author MyEclipse Persistence Tools
 */

public class AgentDAOImpl extends HibernateDaoSupport implements AgentDAO,Serializable {
	
	private static final Logger log = LoggerFactory.getLogger(AgentDAOImpl.class);
	// property constants
	public static final String ACTIVE = "active";
	public static final String DESCRIPTION = "description";
	public static final String LOGIN = "login";
	public static final String NAME = "name";
	public static final String PASSWORD = "password";
	public static final String TYPE = "type";
	public static final String ALLOWED_IPS = "allowedIps";

	protected void initDao() {
		// do nothing
		
	}

	public void save(Agent transientInstance) {
		log.debug("saving Agent instance");
		
		try {
			 getHibernateTemplate().saveOrUpdate(transientInstance);
			
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}		
		
	}

	public void delete(Agent persistentInstance) {
		log.debug("deleting Agent instance");
		try {
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Agent findById(java.lang.Integer id) {
		log.debug("getting Agent instance with id: " + id);
		try {
			Agent instance = (Agent) getHibernateTemplate().get(
					"com.virtualcode.vo.Agent", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Agent instance) {
		log.debug("finding Agent instance by example");
		try {
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Agent instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from Agent as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByActive(Object active) {
		return findByProperty(ACTIVE, active);
	}

	public List findByDescription(Object description) {
		return findByProperty(DESCRIPTION, description);
	}

	public List findByLogin(Object login) {
		return findByProperty(LOGIN, login);
	}

	public List findByName(Object name) {
		return findByProperty(NAME, name);
	}

	public List findByPassword(Object password) {
		return findByProperty(PASSWORD, password);
	}

	public List findByType(Object type) {
		return findByProperty(TYPE, type);
	}

	public List findByAllowedIps(Object allowedIps) {
		return findByProperty(ALLOWED_IPS, allowedIps);
	}

	public List findAll() {
		log.debug("finding all Agent instances");
		try {
			String queryString = "from Agent agent where agent.login<>'bitarch' and agent.login<>'admin'";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Agent merge(Agent detachedInstance) {
		log.debug("merging Agent instance");
		try {
			Agent result = (Agent) getHibernateTemplate().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Agent instance) {
		log.debug("attaching dirty Agent instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Agent instance) {
		log.debug("attaching clean Agent instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static AgentDAOImpl getFromApplicationContext(ApplicationContext ctx) {
		return (AgentDAOImpl) ctx.getBean("AgentDAO");
	}
}