package com.virtualcode.dao.impl;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.NodePermissionJobDAO;
import com.virtualcode.vo.NodePermissionJob;

/**
 * A data access object (DAO) providing persistence and search support for
 * NodePermissionJob entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.vo.NodePermissionJob
 * @author MyEclipse Persistence Tools
 */

public class NodePermissionJobDAOImpl extends HibernateDaoSupport implements NodePermissionJobDAO{
	private static final Logger log = LoggerFactory
			.getLogger(NodePermissionJobDAOImpl.class);
	// property constants
	public static final String NAME = "name";
	public static final String COMMAND = "command";
	public static final String STATUS = "status";
	public static final String REGISTERDATE = "registerdate";
	public static final String STARTDATE = "startdate";
	public static final String FINISHEDDATE = "finisheddate";
	public static final String SECUREINFO = "secureinfo";
	public static final String SIDS = "sids";
	public static final String DENYSIDS = "denysids";
	public static final String SHARESIDS = "sharesids";
	public static final String DENYSHARESIDS = "denysharesids";
	public static final String ISRECURSIVE = "isrecursive";

	protected void initDao() {
		// do nothing
	}

	public void save(NodePermissionJob transientInstance) {
		log.debug("saving NodePermissionJob instance");
		try {
			getHibernateTemplate().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(NodePermissionJob persistentInstance) {
		log.debug("deleting NodePermissionJob instance");
		try {
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public NodePermissionJob findById(java.lang.Integer id) {
		log.debug("getting NodePermissionJob instance with id: " + id);
		try {
			NodePermissionJob instance = (NodePermissionJob) getHibernateTemplate()
					.get("com.virtualcode.vo.NodePermissionJob", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(NodePermissionJob instance) {
		log.debug("finding NodePermissionJob instance by example");
		try {
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding NodePermissionJob instance with property: "
				+ propertyName + ", value: " + value);
		try {
			String queryString = "from NodePermissionJob as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByName(Object name) {
		return findByProperty(NAME, name);
	}

	public List findByCommand(Object command) {
		return findByProperty(COMMAND, command);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findByRegisterdate(Object registerdate) {
		return findByProperty(REGISTERDATE, registerdate);
	}

	public List findByStartdate(Object startdate) {
		return findByProperty(STARTDATE, startdate);
	}

	public List findByFinisheddate(Object finisheddate) {
		return findByProperty(FINISHEDDATE, finisheddate);
	}

	public List findBySecureinfo(Object secureinfo) {
		return findByProperty(SECUREINFO, secureinfo);
	}

	public List findBySids(Object sids) {
		return findByProperty(SIDS, sids);
	}

	public List findByDenysids(Object denysids) {
		return findByProperty(DENYSIDS, denysids);
	}

	public List findBySharesids(Object sharesids) {
		return findByProperty(SHARESIDS, sharesids);
	}

	public List findByDenysharesids(Object denysharesids) {
		return findByProperty(DENYSHARESIDS, denysharesids);
	}

	public List findByIsrecursive(Object isrecursive) {
		return findByProperty(ISRECURSIVE, isrecursive);
	}

	public List findAll() {
		log.debug("finding all NodePermissionJob instances");
		try {
			String queryString = "from NodePermissionJob";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public NodePermissionJob merge(NodePermissionJob detachedInstance) {
		log.debug("merging NodePermissionJob instance");
		try {
			NodePermissionJob result = (NodePermissionJob) getHibernateTemplate()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(NodePermissionJob instance) {
		log.debug("attaching dirty NodePermissionJob instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(NodePermissionJob instance) {
		log.debug("attaching clean NodePermissionJob instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static NodePermissionJobDAOImpl getFromApplicationContext(
			ApplicationContext ctx) {
		return (NodePermissionJobDAOImpl) ctx.getBean("NodePermissionJobDAO");
	}
}