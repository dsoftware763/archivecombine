package com.virtualcode.dao.impl;


import java.util.List;

import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.DocumentCategoryDAO;
import com.virtualcode.vo.DocumentCategory;

/**
 	* A data access object (DAO) providing persistence and search support for DocumentCategory entities.
 			* Transaction control of the save(), update() and delete() operations 
		can directly support Spring container-managed transactions or they can be augmented	to handle user-managed Spring transactions. 
		Each of these methods provides additional information for how to configure it for the desired type of transaction control. 	
	 * @see .DocumentCategory
  * @author MyEclipse Persistence Tools 
 */

public class DocumentCategoryDAOImpl extends HibernateDaoSupport implements DocumentCategoryDAO  {
	     private static final Logger log = LoggerFactory.getLogger(DocumentCategoryDAOImpl.class);
		//property constants
	public static final String DESCRIPTION = "description";
	public static final String NAME = "name";



	protected void initDao() {
		//do nothing
	}
    
    public DocumentCategory save(DocumentCategory transientInstance) {
        log.debug("saving DocumentCategory instance");
        try {
            Integer id=(Integer)getHibernateTemplate().save(transientInstance);
            transientInstance.setId(id);
            getHibernateTemplate().flush();
            log.debug("save successful");
        } catch (RuntimeException re) {
            log.error("save failed", re);
            throw re;
        }
        return transientInstance;
    }
    
    public DocumentCategory update(DocumentCategory transientInstance) {
        log.debug("saving DocumentCategory instance");
        try {
            getHibernateTemplate().saveOrUpdate(transientInstance);   
            getHibernateTemplate().flush();
            log.debug("save successful");
        } catch (RuntimeException re) {
            log.error("save failed", re);
            throw re;
        }
        return transientInstance;
    }
    
	public void delete(DocumentCategory persistentInstance) {
        log.debug("deleting DocumentCategory instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            getHibernateTemplate().flush();
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }
    
    public DocumentCategory findById( java.lang.Integer id) {
        log.debug("getting DocumentCategory instance with id: " + id);
        try {
            DocumentCategory instance = (DocumentCategory) getHibernateTemplate()
                    .get("com.virtualcode.vo.DocumentCategory", id);
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    
    public List findByExample(DocumentCategory instance) {
        log.debug("finding DocumentCategory instance by example");
        try {
            List results = getHibernateTemplate().findByExample(instance);
            log.debug("find by example successful, result size: " + results.size());
            return results;
        } catch (RuntimeException re) {
            log.error("find by example failed", re);
            throw re;
        }
    }    
    
    public List findByProperty(String propertyName, Object value) {
      log.debug("finding DocumentCategory instance with property: " + propertyName
            + ", value: " + value);
      try {
         String queryString = "from DocumentCategory as model where model." 
         						+ propertyName + "= ?";
		 return getHibernateTemplate().find(queryString, value);
      } catch (RuntimeException re) {
         log.error("find by property name failed", re);
         throw re;
      }
	}

	public List findByDescription(Object description
	) {
		return findByProperty(DESCRIPTION, description
		);
	}
	
	public List findByName(Object name
	) {
		return findByProperty(NAME, name
		);
	}
	

	public List findAll() {
		log.debug("finding all DocumentCategory instances");
		try {
			
			String queryString = "from com.virtualcode.vo.DocumentCategory dc";
		 	return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}
	
    public DocumentCategory merge(DocumentCategory detachedInstance) {
        log.debug("merging DocumentCategory instance");
        try {
            DocumentCategory result = (DocumentCategory) getHibernateTemplate()
                    .merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public void attachDirty(DocumentCategory instance) {
        log.debug("attaching dirty DocumentCategory instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }
    
    public void attachClean(DocumentCategory instance) {
        log.debug("attaching clean DocumentCategory instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

	public static DocumentCategoryDAOImpl getFromApplicationContext(ApplicationContext ctx) {
    	return (DocumentCategoryDAOImpl) ctx.getBean("DocumentCategoryDAO");
	}
}