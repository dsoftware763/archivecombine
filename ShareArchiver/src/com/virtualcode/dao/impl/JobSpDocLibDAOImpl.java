package com.virtualcode.dao.impl;

import java.util.List;

import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.JobSpDocLibDAO;
import com.virtualcode.vo.JobSpDocLib;

/**
 * A data access object (DAO) providing persistence and search support for
 * JobSpDocLib entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.dao.impl.JobSpDocLib
 * @author MyEclipse Persistence Tools
 */

public class JobSpDocLibDAOImpl extends HibernateDaoSupport implements JobSpDocLibDAO {
	private static final Logger log = LoggerFactory
			.getLogger(JobSpDocLibDAOImpl.class);
	// property constants
	public static final String GUID = "guid";
	public static final String NAME = "name";
	public static final String TYPE = "type";
	public static final String DESTINATION_PATH = "destinationPath";
	public static final String RECURSIVE = "recursive";

	protected void initDao() {
		// do nothing
	}

	public void save(JobSpDocLib transientInstance) {
		log.debug("saving JobSpDocLib instance");
		try {
			getHibernateTemplate().saveOrUpdate(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(JobSpDocLib persistentInstance) {
		log.debug("deleting JobSpDocLib instance");
		try {
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public JobSpDocLib findById(java.lang.Integer id) {
		log.debug("getting JobSpDocLib instance with id: " + id);
		try {
			JobSpDocLib instance = (JobSpDocLib) getHibernateTemplate().get(
					"com.virtualcode.dao.impl.JobSpDocLib", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(JobSpDocLib instance) {
		log.debug("finding JobSpDocLib instance by example");
		try {
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding JobSpDocLib instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from JobSpDocLib as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}
	
	public List findByNameLike(String value) {
		log.debug("finding JobSpDocLib instance with property: name" 
				+ ", value: " + value);
		try {
//			from Cat as cat where cat.mate.name like '%s%'
			String queryString = "from JobSpDocLib as model where lower(model.name) like '%" + value + "' and model.job.actionType='INDEXING' and model.job.isShareAnalysis=1";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}
	
	public List findByNameLikeForArchive(String value) {
		log.debug("finding JobSpDocLib instance with property: name" 
				+ ", value: " + value);
		try {
//			from Cat as cat where cat.mate.name like '%s%'
			String queryString = "from JobSpDocLib as model where lower(model.name) like '%" + value + "'";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByGuid(Object guid) {
		return findByProperty(GUID, guid);
	}

	public List findByName(Object name) {
		return findByProperty(NAME, name);
	}

	public List findByType(Object type) {
		return findByProperty(TYPE, type);
	}

	public List findByDestinationPath(Object destinationPath) {
		return findByProperty(DESTINATION_PATH, destinationPath);
	}

	public List findByRecursive(Object recursive) {
		return findByProperty(RECURSIVE, recursive);
	}

	public List findAll() {
		log.debug("finding all JobSpDocLib instances");
		try {
			String queryString = "from JobSpDocLib";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public JobSpDocLib merge(JobSpDocLib detachedInstance) {
		log.debug("merging JobSpDocLib instance");
		try {
			JobSpDocLib result = (JobSpDocLib) getHibernateTemplate().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(JobSpDocLib instance) {
		log.debug("attaching dirty JobSpDocLib instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(JobSpDocLib instance) {
		log.debug("attaching clean JobSpDocLib instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static JobSpDocLibDAOImpl getFromApplicationContext(
			ApplicationContext ctx) {
		return (JobSpDocLibDAOImpl) ctx.getBean("JobSpDocLibDAO");
	}
}