package com.virtualcode.dao.impl;

import java.sql.SQLException;
import java.util.List;

import javax.jcr.Session;
import javax.persistence.Query;

import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.ShareAnalysisStatsDAO;
import com.virtualcode.vo.DriveLetters;
import com.virtualcode.vo.ShareAnalysisStats;

/**
 * A data access object (DAO) providing persistence and search support for
 * ShareAnalysisStats entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.vo.ShareAnalysisStats
 * @author MyEclipse Persistence Tools
 */

public class ShareAnalysisStatsDAOImpl extends HibernateDaoSupport implements ShareAnalysisStatsDAO {
	private static final Logger log = LoggerFactory
			.getLogger(ShareAnalysisStatsDAOImpl.class);
	// property constants
	public static final String SHARE_ID = "share_id";
	public static final String DOCUMENT_CAT_NAME = "documentCatName";
	public static final String TOTAL_VOLUME = "totalVolume";
	public static final String THREE_MONTHS_OLDER_VOL = "threeMonthsOlderVol";
	public static final String SIX_MONTHS_OLDER_VOL = "sixMonthsOlderVol";
	public static final String ONE_YEAR_OLDER_VOL = "oneYearOlderVol";
	public static final String TWO_YEAR_OLDER_VOL = "twoYearOlderVol";
	public static final String THREE_YEAR_OLDER_VOL = "threeYearOlderVol";

	protected void initDao() {
		// do nothing
	}

	public void save(ShareAnalysisStats transientInstance) {
		log.debug("saving ShareAnalysisStats instance");
		try {
			getHibernateTemplate().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(ShareAnalysisStats persistentInstance) {
		log.debug("deleting ShareAnalysisStats instance");
		try {
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}
	
	public void deleteByDriveId(Integer driveLetterId){
		
		log.debug("deleting ShareAnalysisStats By drive letter Id "+driveLetterId);
		try {
			final String query="delete from  ShareAnalysisStats sa where sa.driveLetters.id="+driveLetterId;
			   getHibernateTemplate().execute(new HibernateCallback<ShareAnalysisStats>() {

				@Override
				public ShareAnalysisStats doInHibernate(org.hibernate.Session session)
						throws HibernateException, SQLException {
					    session.createQuery(query).executeUpdate();
					return null;
				}
			});
					
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("deleting ShareAnalysisStats failed.", re);
			throw re;
		}
	}

	public ShareAnalysisStats findById(java.lang.Long id) {
		log.debug("getting ShareAnalysisStats instance with id: " + id);
		try {
			ShareAnalysisStats instance = (ShareAnalysisStats) getHibernateTemplate()
					.get("com.virtualcode.vo.ShareAnalysisStats", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(ShareAnalysisStats instance) {
		log.debug("finding ShareAnalysisStats instance by example");
		try {
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding ShareAnalysisStats instance with property: "
				+ propertyName + ", value: " + value);
		try {
			String queryString = "from ShareAnalysisStats as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List<ShareAnalysisStats> findByShare(Integer shareId){
		log.debug("finding ShareAnalysisStats instance with shareId: "+ shareId);
		try {
		   getHibernateTemplate().setMaxResults(10);
			String queryString = "from com.virtualcode.vo.ShareAnalysisStats as model where model.driveLetters.id=? order by model.totalVolume desc";
					
			List list= getHibernateTemplate().find(queryString, shareId);
			getHibernateTemplate().setMaxResults(0);
			return list;
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}
	
	public List findByDocumentCatName(Object documentCatName) {
		return findByProperty(DOCUMENT_CAT_NAME, documentCatName);
	}

	public List findByTotalVolume(Object totalVolume) {
		return findByProperty(TOTAL_VOLUME, totalVolume);
	}

	public List findByThreeMonthsOlderVol(Object threeMonthsOlderVol) {
		return findByProperty(THREE_MONTHS_OLDER_VOL, threeMonthsOlderVol);
	}

	public List findBySixMonthsOlderVol(Object sixMonthsOlderVol) {
		return findByProperty(SIX_MONTHS_OLDER_VOL, sixMonthsOlderVol);
	}

	public List findByOneYearOlderVol(Object oneYearOlderVol) {
		return findByProperty(ONE_YEAR_OLDER_VOL, oneYearOlderVol);
	}

	public List findByTwoYearOlderVol(Object twoYearOlderVol) {
		return findByProperty(TWO_YEAR_OLDER_VOL, twoYearOlderVol);
	}

	public List findByThreeYearOlderVol(Object threeYearOlderVol) {
		return findByProperty(THREE_YEAR_OLDER_VOL, threeYearOlderVol);
	}

	public List findAll() {
		log.debug("finding all ShareAnalysisStats instances");
		try {
			String queryString = "from com.virtualcode.vo.ShareAnalysisStats";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public ShareAnalysisStats merge(ShareAnalysisStats detachedInstance) {
		log.debug("merging ShareAnalysisStats instance");
		try {
			ShareAnalysisStats result = (ShareAnalysisStats) getHibernateTemplate()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(ShareAnalysisStats instance) {
		log.debug("attaching dirty ShareAnalysisStats instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(ShareAnalysisStats instance) {
		log.debug("attaching clean ShareAnalysisStats instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	
}