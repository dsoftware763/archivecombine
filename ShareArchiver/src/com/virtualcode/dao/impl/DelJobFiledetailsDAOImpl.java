package com.virtualcode.dao.impl;

import java.util.List;

import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.DelJobFiledetailsDAO;
import com.virtualcode.vo.DelJobFiledetails;

/**
 	* A data access object (DAO) providing persistence and search support for DelJobFiledetails entities.
 			* Transaction control of the save(), update() and delete() operations 
		can directly support  Spring container-managed transactions or they can be augmented	to handle user-managed Spring transactions. 
		Each of these methods provides additional information for how to configure it for the desired type of transaction control. 	
	 * @see com.virtualcode.vo.DelJobFiledetails
  * @author MyEclipse Persistence Tools 
 */

public class DelJobFiledetailsDAOImpl extends HibernateDaoSupport implements DelJobFiledetailsDAO {

	private static final Logger log = LoggerFactory
			.getLogger(DelJobFiledetailsDAO.class);
	// property constants
	public static final String UUID = "uuid";
	public static final String STATUS = "status";
	public static final String DESCRIPTION = "description";

	protected void initDao() {
		// do nothing
	}

	public void save(DelJobFiledetails transientInstance) {
		log.debug("saving DelJobFiledetails instance");
		try {
			getHibernateTemplate().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(DelJobFiledetails persistentInstance) {
		log.debug("deleting DelJobFiledetails instance");
		try {
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public DelJobFiledetails findById(java.lang.Integer id) {
		log.debug("getting DelJobFiledetails instance with id: " + id);
		try {
			DelJobFiledetails instance = (DelJobFiledetails) getHibernateTemplate()
					.get("com.virtualcode.vo.DelJobFiledetails", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(DelJobFiledetails instance) {
		log.debug("finding DelJobFiledetails instance by example");
		try {
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding DelJobFiledetails instance with property: "
				+ propertyName + ", value: " + value);
		try {
			String queryString = "from DelJobFiledetails as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUuid(Object uuid) {
		return findByProperty(UUID, uuid);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findByDescription(Object description) {
		return findByProperty(DESCRIPTION, description);
	}

	public List findAll() {
		log.debug("finding all DelJobFiledetails instances");
		try {
			String queryString = "from DelJobFiledetails";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public DelJobFiledetails merge(DelJobFiledetails detachedInstance) {
		log.debug("merging DelJobFiledetails instance");
		try {
			DelJobFiledetails result = (DelJobFiledetails) getHibernateTemplate()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(DelJobFiledetails instance) {
		log.debug("attaching dirty DelJobFiledetails instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(DelJobFiledetails instance) {
		log.debug("attaching clean DelJobFiledetails instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}