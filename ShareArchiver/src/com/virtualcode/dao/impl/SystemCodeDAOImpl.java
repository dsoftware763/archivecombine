package com.virtualcode.dao.impl;

import java.util.List;

import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.SystemCodeDAO;
import com.virtualcode.vo.SystemCode;

/**
 * A data access object (DAO) providing persistence and search support for
 * SystemCode entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.virtualcode.vo.SystemCode
 * @author MyEclipse Persistence Tools
 */

public class SystemCodeDAOImpl extends HibernateDaoSupport implements SystemCodeDAO{
	private static final Logger log = LoggerFactory
			.getLogger(SystemCodeDAOImpl.class);
	// property constants
	public static final String CODENAME = "codename";
	public static final String CODETYPE = "codetype";
	public static final String CODEVALUE = "codevalue";
	public static final String EDITABLE = "editable";
	public static final String VIEWABLE = "viewable";

	protected void initDao() {
		// do nothing
	}

	public void save(SystemCode systemCode) {
		log.debug("saving SystemCode instance");
		try {
			getHibernateTemplate().save(systemCode);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(SystemCode systemCode) {
		log.debug("deleting SystemCode instance");
		try {
			getHibernateTemplate().delete(systemCode);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public SystemCode findById(java.lang.Integer id) {
		log.debug("getting SystemCode instance with id: " + id);
		try {
			SystemCode instance = (SystemCode) getHibernateTemplate().get(
					"com.virtualcode.vo.SystemCode", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(SystemCode instance) {
		log.debug("finding SystemCode instance by example");
		try {
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding SystemCode instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from SystemCode as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByCodename(Object codename) {
		return findByProperty(CODENAME, codename);
	}

	public List findByCodetype(Object codetype) {
		return findByProperty(CODETYPE, codetype);
	}

	public List findByCodevalue(Object codevalue) {
		return findByProperty(CODEVALUE, codevalue);
	}

	public List findByEditable(Object editable) {
		return findByProperty(EDITABLE, editable);
	}

	public List findByViewable(Object viewable) {
		return findByProperty(VIEWABLE, viewable);
	}

	public List findAll() {
		log.debug("finding all SystemCode instances");
		System.out.println("-------------------------findAll systemcode DAO---------------------");
		try {
			String queryString = "from com.virtualcode.vo.SystemCode";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			re.printStackTrace();
			throw re;
		}
	}
	
	public List<SystemCode> findAllGeneralCodes() {
		log.debug("finding all General system codes...");
		
		try {
			String queryString = "from com.virtualcode.vo.SystemCode as systemCode where systemCode.codetype='"+com.virtualcode.vo.SystemCodeType.GENERAL+"' and systemCode.editable=1";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			re.printStackTrace();
			throw re;
		}
	}
	
	public List<SystemCode> findAllLDAPCodes() {
		log.debug("finding all ldap system codes...");
		
		try {
			String queryString = "from com.virtualcode.vo.SystemCode as systemCode where systemCode.codetype='"+com.virtualcode.vo.SystemCodeType.ACTIVE_DIR_AUTH+"' and systemCode.editable=1";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all ldap failed", re);
			re.printStackTrace();
			throw re;
		}
	}
	
	
	public List<SystemCode> findAllJespCodes() {
		log.debug("finding all jesp system codes...");
		
		try {
			String queryString = "from com.virtualcode.vo.SystemCode as systemCode where systemCode.codetype='"+com.virtualcode.vo.SystemCodeType.JESPA+"' and systemCode.editable=1";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all jespa failed", re);
			re.printStackTrace();
			throw re;
		}
	}
	
	public List<SystemCode> findAllAgentCodes() {
		log.debug("finding all ldap system codes...");
		
		try {
			String queryString = "from com.virtualcode.vo.SystemCode as systemCode where systemCode.codetype='"+com.virtualcode.vo.SystemCodeType.ZUES_AGENT+"' and systemCode.editable=1";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			re.printStackTrace();
			throw re;
		}
	}
	
	
	public List<SystemCode> findAllCompanyCodes() {
		log.debug("finding all company system codes...");
		
		try {
			String queryString = "from com.virtualcode.vo.SystemCode as systemCode where systemCode.codetype='"+com.virtualcode.vo.SystemCodeType.COMPANY+"' or systemCode.codename='sh_license' and systemCode.editable=1";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all ldap failed", re);
			re.printStackTrace();
			throw re;
		}
	}
	
	public List<SystemCode> findAllAccountPolicyCodes() {
		log.debug("finding all company system codes...");
		
		try {
			String queryString = "from com.virtualcode.vo.SystemCode as systemCode where systemCode.codetype='"+com.virtualcode.vo.SystemCodeType.ACCOUNT_POLICY+"' and systemCode.editable=1";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all ldap failed", re);
			re.printStackTrace();
			throw re;
		}
	}
	
	public SystemCode merge(SystemCode detachedInstance) {
		log.debug("merging SystemCode instance");
		try {
			SystemCode result = (SystemCode) getHibernateTemplate().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(SystemCode instance) {
		log.debug("attaching dirty SystemCode instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
	
	public void update(SystemCode instance) {
		log.debug("update SystemCode instance");
		try {
			getHibernateTemplate().update(instance);
			log.debug("update successful");
		} catch (RuntimeException re) {
			log.error("update failed", re);
			throw re;
		}
	}

	public void attachClean(SystemCode instance) {
		log.debug("attaching clean SystemCode instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static SystemCodeDAOImpl getFromApplicationContext(ApplicationContext ctx) {
		return (SystemCodeDAOImpl) ctx.getBean("SystemCodeDAO");
	}
}