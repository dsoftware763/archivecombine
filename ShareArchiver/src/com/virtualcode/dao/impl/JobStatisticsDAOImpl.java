package com.virtualcode.dao.impl;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.JobStatisticsDAO;
import com.virtualcode.vo.JobStatistics;

/**
 * A data access object (DAO) providing persistence and search support for
 * JobStatistics entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.vo.JobStatistics
 * @author MyEclipse Persistence Tools
 */

public class JobStatisticsDAOImpl extends HibernateDaoSupport implements JobStatisticsDAO{
	private static final Logger log = LoggerFactory
			.getLogger(JobStatisticsDAOImpl.class);
	// property constants
	public static final String EXECUTION_ID = "executionId";
	public static final String SKIPPED_PROCESSFILE_SKIPPED_HIDDENLECTED = "skippedProcessfileSkippedHiddenlected";
	public static final String SKIPPED_PROCESSFILE_SKIPPED_ISDIR = "skippedProcessfileSkippedIsdir";
	public static final String SKIPPED_PROCESSFILE_SKIPPED_MISMATCH_AGE = "skippedProcessfileSkippedMismatchAge";
	public static final String SKIPPED_PROCESSFILE_SKIPPED_MISMATCH_EXTENSION = "skippedProcessfileSkippedMismatchExtension";
	public static final String SKIPPED_PROCESSFILE_SKIPPED_MISMATCH_LASTACCESSTIME = "skippedProcessfileSkippedMismatchLastaccesstime";
	public static final String SKIPPED_PROCESSFILE_SKIPPED_MISMATCH_LASTMODIFIEDTIME = "skippedProcessfileSkippedMismatchLastmodifiedtime";
	public static final String SKIPPED_PROCESSFILE_SKIPPED_READ_ONLY = "skippedProcessfileSkippedReadOnly";
	public static final String SKIPPED_PROCESSFILE_SKIPPED_SYMBOLLINK = "skippedProcessfileSkippedSymbollink";
	public static final String SKIPPED_PROCESSFILE_SKIPPED_TEMPORARY = "skippedProcessfileSkippedTemporary";
	public static final String SKIPPED_PROCESSFILE_SKIPPED_TOO_LARGE = "skippedProcessfileSkippedTooLarge";
	public static final String TOTAL_ARCHIVED = "totalArchived";
	public static final String TOTAL_DUPLICATED = "totalDuplicated";
	public static final String TOTAL_EVALUATED = "totalEvaluated";
	public static final String TOTAL_FAILED_ARCHIVING = "totalFailedArchiving";
	public static final String TOTAL_FAILED_DEDUPLICATION = "totalFailedDeduplication";
	public static final String TOTAL_FAILED_EVALUATING = "totalFailedEvaluating";
	public static final String TOTAL_FAILED_STUBBING = "totalFailedStubbing";
	public static final String TOTAL_STUBBED = "totalStubbed";
	public static final String JOB = "job.id";

	protected void initDao() {
		// do nothing
	}

	public void save(JobStatistics transientInstance) {
		log.debug("saving JobStatistics instance");
		try {
			getHibernateTemplate().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}
	
	public void update(JobStatistics transientInstance) {
		log.debug("Update JobStatistics instance");
		try {
			getHibernateTemplate().update(transientInstance);
			log.debug("update successful");
		} catch (RuntimeException re) {
			log.error("update failed", re);
			throw re;
		}
	}

	public void delete(JobStatistics persistentInstance) {
		log.debug("deleting JobStatistics instance");
		try {
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public JobStatistics findById(java.lang.Integer id) {
		log.debug("getting JobStatistics instance with id: " + id);
		try {
			JobStatistics instance = (JobStatistics) getHibernateTemplate()
					.get("com.virtualcode.vo.JobStatistics", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(JobStatistics instance) {
		log.debug("finding JobStatistics instance by example");
		try {
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding JobStatistics instance with property: "
				+ propertyName + ", value: " + value);
		try {
			String queryString = "from JobStatistics as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByExecutionId(Object executionId) {
		return findByProperty(EXECUTION_ID, executionId);
	}

	public List findBySkippedProcessfileSkippedHiddenlected(
			Object skippedProcessfileSkippedHiddenlected) {
		return findByProperty(SKIPPED_PROCESSFILE_SKIPPED_HIDDENLECTED,
				skippedProcessfileSkippedHiddenlected);
	}

	public List findBySkippedProcessfileSkippedIsdir(
			Object skippedProcessfileSkippedIsdir) {
		return findByProperty(SKIPPED_PROCESSFILE_SKIPPED_ISDIR,
				skippedProcessfileSkippedIsdir);
	}

	public List findBySkippedProcessfileSkippedMismatchAge(
			Object skippedProcessfileSkippedMismatchAge) {
		return findByProperty(SKIPPED_PROCESSFILE_SKIPPED_MISMATCH_AGE,
				skippedProcessfileSkippedMismatchAge);
	}

	public List findBySkippedProcessfileSkippedMismatchExtension(
			Object skippedProcessfileSkippedMismatchExtension) {
		return findByProperty(SKIPPED_PROCESSFILE_SKIPPED_MISMATCH_EXTENSION,
				skippedProcessfileSkippedMismatchExtension);
	}

	public List findBySkippedProcessfileSkippedMismatchLastaccesstime(
			Object skippedProcessfileSkippedMismatchLastaccesstime) {
		return findByProperty(
				SKIPPED_PROCESSFILE_SKIPPED_MISMATCH_LASTACCESSTIME,
				skippedProcessfileSkippedMismatchLastaccesstime);
	}

	public List findBySkippedProcessfileSkippedMismatchLastmodifiedtime(
			Object skippedProcessfileSkippedMismatchLastmodifiedtime) {
		return findByProperty(
				SKIPPED_PROCESSFILE_SKIPPED_MISMATCH_LASTMODIFIEDTIME,
				skippedProcessfileSkippedMismatchLastmodifiedtime);
	}

	public List findBySkippedProcessfileSkippedReadOnly(
			Object skippedProcessfileSkippedReadOnly) {
		return findByProperty(SKIPPED_PROCESSFILE_SKIPPED_READ_ONLY,
				skippedProcessfileSkippedReadOnly);
	}

	public List findBySkippedProcessfileSkippedSymbollink(
			Object skippedProcessfileSkippedSymbollink) {
		return findByProperty(SKIPPED_PROCESSFILE_SKIPPED_SYMBOLLINK,
				skippedProcessfileSkippedSymbollink);
	}

	public List findBySkippedProcessfileSkippedTemporary(
			Object skippedProcessfileSkippedTemporary) {
		return findByProperty(SKIPPED_PROCESSFILE_SKIPPED_TEMPORARY,
				skippedProcessfileSkippedTemporary);
	}

	public List findBySkippedProcessfileSkippedTooLarge(
			Object skippedProcessfileSkippedTooLarge) {
		return findByProperty(SKIPPED_PROCESSFILE_SKIPPED_TOO_LARGE,
				skippedProcessfileSkippedTooLarge);
	}

	public List findByTotalArchived(Object totalArchived) {
		return findByProperty(TOTAL_ARCHIVED, totalArchived);
	}

	public List findByTotalDuplicated(Object totalDuplicated) {
		return findByProperty(TOTAL_DUPLICATED, totalDuplicated);
	}

	public List findByTotalEvaluated(Object totalEvaluated) {
		return findByProperty(TOTAL_EVALUATED, totalEvaluated);
	}

	public List findByTotalFailedArchiving(Object totalFailedArchiving) {
		return findByProperty(TOTAL_FAILED_ARCHIVING, totalFailedArchiving);
	}

	public List findByTotalFailedDeduplication(Object totalFailedDeduplication) {
		return findByProperty(TOTAL_FAILED_DEDUPLICATION,
				totalFailedDeduplication);
	}

	public List findByTotalFailedEvaluating(Object totalFailedEvaluating) {
		return findByProperty(TOTAL_FAILED_EVALUATING, totalFailedEvaluating);
	}

	public List findByTotalFailedStubbing(Object totalFailedStubbing) {
		return findByProperty(TOTAL_FAILED_STUBBING, totalFailedStubbing);
	}

	public List findByTotalStubbed(Object totalStubbed) {
		return findByProperty(TOTAL_STUBBED, totalStubbed);
	}

	public List findAll() {
		log.debug("finding all JobStatistics instances");
		try {
			String queryString = "from JobStatistics";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public JobStatistics merge(JobStatistics detachedInstance) {
		log.debug("merging JobStatistics instance");
		try {
			JobStatistics result = (JobStatistics) getHibernateTemplate()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(JobStatistics instance) {
		log.debug("attaching dirty JobStatistics instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(JobStatistics instance) {
		log.debug("attaching clean JobStatistics instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static JobStatisticsDAOImpl getFromApplicationContext(
			ApplicationContext ctx) {
		return (JobStatisticsDAOImpl) ctx.getBean("JobStatisticsDAO");
	}

	@Override
	public List findByJob(Object jobId) {
		// TODO Auto-generated method stub
		log.debug("finding JobStatistics by Job with jobID: " + jobId);
		System.out.println("finding JobStatistics by Job with jobID: " + jobId);
		
		try {
			String queryString = "from JobStatistics as model where model.job.id= ? ORDER BY model.id DESC";
			getHibernateTemplate().setMaxResults(5);
			return getHibernateTemplate().find(queryString, jobId);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}
}