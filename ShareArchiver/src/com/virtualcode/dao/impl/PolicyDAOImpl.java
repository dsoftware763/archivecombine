package com.virtualcode.dao.impl;

import java.util.List;
import java.util.Set;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.PolicyDAO;
import com.virtualcode.vo.Policy;

/**
 * A data access object (DAO) providing persistence and search support for
 * Policy entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.virtualcode.vo.Policy
 * @author MyEclipse Persistence Tools
 */

public class PolicyDAOImpl extends HibernateDaoSupport implements PolicyDAO {
	private static final Logger log = LoggerFactory.getLogger(PolicyDAOImpl.class);
	// property constants
	public static final String ACTIVE = "active";
	public static final String DESCRIPTION = "description";
	public static final String DOCUMENT_AGE = "documentAge";
	public static final String LAST_ACCESSED_DATE = "lastAccessedDate";
	public static final String LAST_MODIFIED_DATE = "lastModifiedDate";
	public static final String NAME = "name";
	public static final String SIZE_LARGER_THAN = "sizeLargerThan";
	public static final String JOB = "job.id";

	protected void initDao() {
		// do nothing
	}

	public void save(Policy transientInstance) {
		log.debug("saving Policy instance");
		try {
			getHibernateTemplate().saveOrUpdate(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Policy persistentInstance) {
		log.debug("deleting Policy instance");
		try {
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Policy findById(java.lang.Integer id) {
		log.debug("getting Policy instance with id: " + id);
		try {
			Policy instance = (Policy) getHibernateTemplate().get(
					"com.virtualcode.vo.Policy", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Policy instance) {
		log.debug("finding Policy instance by example");
		try {
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Policy instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from Policy as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}
	
	public boolean isDocCategoryInUse(Integer docCategoryId) {
		log.debug("document category id: "+docCategoryId);
		try {
			Session session=getHibernateTemplate().getSessionFactory().openSession();
			Query query = session.createSQLQuery("SELECT * FROM policy_document_type p WHERE p.document_type_id IN (SELECT d.id FROM document_type d WHERE d.document_category_id="+docCategoryId+")");
					
			List list = query.list();
			session.close();
			if(list!=null && list.size()>0){
				return true;
			}
			return false;
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}
	
	public boolean isDocTypeInUse(Integer docTypeId) {
		log.debug("document category id: "+docTypeId);
		try {
			Session session=getHibernateTemplate().getSessionFactory().openSession();
			Query query = session.createSQLQuery("SELECT * FROM policy_document_type p WHERE p.document_type_id="+docTypeId);
					
			List list = query.list();
			session.close();
			
			if(list!=null && list.size()>0){
				return true;
			}
			return false;
			
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByActive(Object active) {
		return findByProperty(ACTIVE, active);
	}

	public List findByDescription(Object description) {
		return findByProperty(DESCRIPTION, description);
	}

	public List findByDocumentAge(Object documentAge) {
		return findByProperty(DOCUMENT_AGE, documentAge);
	}

	public List findByLastAccessedDate(Object lastAccessedDate) {
		return findByProperty(LAST_ACCESSED_DATE, lastAccessedDate);
	}

	public List findByLastModifiedDate(Object lastModifiedDate) {
		return findByProperty(LAST_MODIFIED_DATE, lastModifiedDate);
	}

	public List findByName(Object name) {
		return findByProperty(NAME, name);
	}

	public List findBySizeLargerThan(Object sizeLargerThan) {
		return findByProperty(SIZE_LARGER_THAN, sizeLargerThan);
	}

	public List findAll() {
		log.debug("finding all Policy instances");
		try {
			String queryString = "from Policy p where p.active=1";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Policy merge(Policy detachedInstance) {
		log.debug("merging Policy instance");
		try {
			Policy result = (Policy) getHibernateTemplate().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Policy instance) {
		log.debug("attaching dirty Policy instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Policy instance) {
		log.debug("attaching clean Policy instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static PolicyDAOImpl getFromApplicationContext(ApplicationContext ctx) {
		return (PolicyDAOImpl) ctx.getBean("PolicyDAOImpl");
	}
}