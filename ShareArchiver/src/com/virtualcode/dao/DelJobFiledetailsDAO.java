package com.virtualcode.dao;

import java.util.List;

import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.dao.impl.DelJobFiledetailsDAOImpl;
import com.virtualcode.vo.DelJobFiledetails;


public interface DelJobFiledetailsDAO {
	public void save(DelJobFiledetails transientInstance);
	public void delete(DelJobFiledetails persistentInstance);
	public DelJobFiledetails findById( java.lang.Integer id);
	public List findByExample(DelJobFiledetails instance);
	
	public List findByProperty(String propertyName, Object value);
	public List findByUuid(Object uuid);
	public List findByStatus(Object status);
	public List findByDescription(Object description);
	public List findAll();
	
	public DelJobFiledetails merge(DelJobFiledetails detachedInstance);
	public void attachDirty(DelJobFiledetails instance);
	public void attachClean(DelJobFiledetails instance);
} 