package com.virtualcode.dao;

import java.util.List;

import com.virtualcode.vo.Policy;

public interface PolicyDAO {
	
	public void save(Policy transientInstance);

	public void delete(Policy persistentInstance);

	public Policy findById(java.lang.Integer id);

	public List findByExample(Policy instance) ;

	public List findByProperty(String propertyName, Object value);

	public List findByActive(Object active);
	
	public List findByName(Object name);
	
	public List findAll();

	public Policy merge(Policy detachedInstance) ;

	public void attachDirty(Policy instance);

	public void attachClean(Policy instance);
	
	public boolean isDocCategoryInUse(Integer docCategoryId);
	
	public boolean isDocTypeInUse(Integer docTypeId);

	
}