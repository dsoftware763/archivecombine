package com.virtualcode.dao;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.JobStatus;

/**
 * A data access object (DAO) providing persistence and search support for
 * JobStatus entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.virtualcode.vo.JobStatus
 * @author MyEclipse Persistence Tools
 */

public interface JobStatusDAO {

	public void save(JobStatus transientInstance) ;

	public void delete(JobStatus persistentInstance) ;

	public JobStatus findById(java.lang.Integer id) ;

	public List findByExample(JobStatus instance);

	public List findByProperty(String propertyName, Object value);

	public List findByDescription(Object description);
	
	public List findByIsCurrent(Object isCurrent);

	public List findByPreviousJobStatusId(Object previousJobStatusId) ;

	public List findByStatus(Object status) ;

	public List findByExecutionId(Object executionId);

	public List findAll() ;
	
	public List findByJob(Object jobId);

	public JobStatus merge(JobStatus detachedInstance) ;

	public void attachDirty(JobStatus instance);

	public void attachClean(JobStatus instance);

}