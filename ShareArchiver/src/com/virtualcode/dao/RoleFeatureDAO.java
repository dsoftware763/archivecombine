package com.virtualcode.dao;

import java.util.List;

import com.virtualcode.vo.RoleFeature;


public interface RoleFeatureDAO  {
	
	public void save(RoleFeature transientInstance);

	public RoleFeature findById(java.lang.Integer id) ;
	
	public List findByProperty(String propertyName, Object value) ;

	public List findByName(Object name);

	public List findByAdmin(Object admin);

	public List findByPriv(Object priv) ;

	public List findByLdap(Object ldap) ;
	public List findAll() ;

	public RoleFeature merge(RoleFeature detachedInstance) ;

	public void attachDirty(RoleFeature instance) ;
	public void attachClean(RoleFeature instance) ;

	
}