package com.virtualcode.dao;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.RolePermissions;
import com.virtualcode.vo.RolePermissionsId;


public interface RolePermissionsDAO {
	        
    public void save(RolePermissions transientInstance) ;
    
	public void delete(RolePermissions persistentInstance);
    
    public RolePermissions findById( com.virtualcode.vo.RolePermissionsId id) ;
        
    public List findByExample(RolePermissions instance) ;
    
    public List findByProperty(String propertyName, Object value) ;

	public List findAll() ;
	
    public RolePermissions merge(RolePermissions detachedInstance);

    public void attachDirty(RolePermissions instance);
    
    public void attachClean(RolePermissions instance) ;

	
}