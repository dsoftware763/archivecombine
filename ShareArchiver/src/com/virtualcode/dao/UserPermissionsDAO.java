package com.virtualcode.dao;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.UserPermissions;
import com.virtualcode.vo.UserPermissionsId;


public interface UserPermissionsDAO  {
	        
    public void save(UserPermissions transientInstance);
    
	public void delete(UserPermissions persistentInstance);
    
    public UserPermissions findById( com.virtualcode.vo.UserPermissionsId id);    
    
    public List findByExample(UserPermissions instance);
    
    public List findByProperty(String propertyName, Object value) ;

	public List findAll() ;
	
    public UserPermissions merge(UserPermissions detachedInstance);

    public void attachDirty(UserPermissions instance) ;
    
    public void attachClean(UserPermissions instance);

	
}