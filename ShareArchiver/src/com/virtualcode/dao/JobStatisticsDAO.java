package com.virtualcode.dao;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.JobStatistics;

/**
 * A data access object (DAO) providing persistence and search support for
 * JobStatistics entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.vo.JobStatistics
 * @author MyEclipse Persistence Tools
 */

public interface JobStatisticsDAO {
	

	public void save(JobStatistics transientInstance);
	
	public void update(JobStatistics transientInstance);

	public void delete(JobStatistics persistentInstance);

	public JobStatistics findById(java.lang.Integer id);

	public List findByExample(JobStatistics instance);

	public List findByProperty(String propertyName, Object value);

	public List findByExecutionId(Object executionId);

	public List findBySkippedProcessfileSkippedHiddenlected(
			Object skippedProcessfileSkippedHiddenlected);

	public List findBySkippedProcessfileSkippedIsdir(
			Object skippedProcessfileSkippedIsdir);

	public List findBySkippedProcessfileSkippedMismatchAge(
			Object skippedProcessfileSkippedMismatchAge);

	public List findBySkippedProcessfileSkippedMismatchExtension(
			Object skippedProcessfileSkippedMismatchExtension);

	public List findBySkippedProcessfileSkippedMismatchLastaccesstime(
			Object skippedProcessfileSkippedMismatchLastaccesstime);

	public List findBySkippedProcessfileSkippedMismatchLastmodifiedtime(
			Object skippedProcessfileSkippedMismatchLastmodifiedtime);

	public List findBySkippedProcessfileSkippedReadOnly(
			Object skippedProcessfileSkippedReadOnly);

	public List findBySkippedProcessfileSkippedSymbollink(
			Object skippedProcessfileSkippedSymbollink);

	public List findBySkippedProcessfileSkippedTemporary(
			Object skippedProcessfileSkippedTemporary);

	public List findBySkippedProcessfileSkippedTooLarge(
			Object skippedProcessfileSkippedTooLarge);

	public List findByTotalArchived(Object totalArchived);

	public List findByTotalDuplicated(Object totalDuplicated);

	public List findByTotalEvaluated(Object totalEvaluated);

	public List findByTotalFailedArchiving(Object totalFailedArchiving);

	public List findByTotalFailedDeduplication(Object totalFailedDeduplication);

	public List findByTotalFailedEvaluating(Object totalFailedEvaluating);

	public List findByTotalFailedStubbing(Object totalFailedStubbing);

	public List findByTotalStubbed(Object totalStubbed);

	public List findAll();
	
	public List findByJob(Object jobId);

	public JobStatistics merge(JobStatistics detachedInstance);

	public void attachDirty(JobStatistics instance);

	public void attachClean(JobStatistics instance);

}