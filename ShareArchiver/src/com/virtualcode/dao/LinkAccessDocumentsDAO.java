package com.virtualcode.dao;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.LinkAccessDocuments;

/**
 * A data access object (DAO) providing persistence and search support for
 * LinkAccessDocuments entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.vo.LinkAccessDocuments
 * @author MyEclipse Persistence Tools
 */

public interface LinkAccessDocumentsDAO{

	public void save(LinkAccessDocuments transientInstance);

	public void delete(LinkAccessDocuments persistentInstance);

	public LinkAccessDocuments findById(java.lang.Integer id);

	public List findByExample(LinkAccessDocuments instance);

	public List findByProperty(String propertyName, Object value);
	
	public List findByUuidAndUser(String uuid, String user);

	public List findByUuidDocuments(Object uuidDocuments);

	public List findAll();

	public LinkAccessDocuments merge(LinkAccessDocuments detachedInstance);

	public void attachDirty(LinkAccessDocuments instance);

	public void attachClean(LinkAccessDocuments instance);

}