package com.virtualcode.dao;


import java.util.List;

import com.virtualcode.vo.DocumentType;

/**
 	
  * @author AtiqurRehman 
 */

public interface DocumentTypeDAO   {
	
    
    public void save(DocumentType transientInstance) ;
    
    public void update(DocumentType transientInstance) ;
    
	public void delete(Integer documentTypeId) ;
    
    public DocumentType findById( java.lang.Integer id);
      
    public List findByExample(DocumentType instance);
    
    public List<DocumentType> findByCategoryId(Integer categoryId);

	public List findAll() ;
	
    public DocumentType merge(DocumentType detachedInstance);
    
    public void deleteByCategory(Integer categoryId) ;
   
}