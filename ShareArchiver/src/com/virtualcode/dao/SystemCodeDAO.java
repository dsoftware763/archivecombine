package com.virtualcode.dao;

import java.util.List;

import com.virtualcode.vo.SystemCode;

/**
 * A data access object (DAO) providing persistence and search support for
 * SystemCode entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.virtualcode.vo.SystemCode
 * @author MyEclipse Persistence Tools
 */

public interface SystemCodeDAO {
	

	public void save(SystemCode transientInstance);
	
	public void update(SystemCode transientInstance);

	public void delete(SystemCode persistentInstance);

	public SystemCode findById(java.lang.Integer id);

	public List findByExample(SystemCode instance);

	public List findByProperty(String propertyName, Object value);

	public List findByCodename(Object codename);

	public List findByCodetype(Object codetype);

	public List findByCodevalue(Object codevalue);

	public List findByEditable(Object editable);

	public List findByViewable(Object viewable);

	public List findAll();
	
	public List<SystemCode> findAllGeneralCodes();
	
	public List<SystemCode> findAllLDAPCodes();
	
	public List<SystemCode> findAllJespCodes();
	
	public List<SystemCode> findAllAgentCodes();
	
	public List<SystemCode> findAllCompanyCodes();
	
	public List<SystemCode> findAllAccountPolicyCodes();

	public SystemCode merge(SystemCode detachedInstance);

	public void attachDirty(SystemCode instance);

	public void attachClean(SystemCode instance);

}