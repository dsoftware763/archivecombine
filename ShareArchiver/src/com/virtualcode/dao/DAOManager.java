package com.virtualcode.dao;

public class DAOManager {

	private AgentDAO agentDAO;
	
	private DocumentCategoryDAO documentCategoryDAO;
	
	private DocumentTypeDAO documentTypeDAO;

	private UserDAO userDAO;
	
	private RoleDAO roleDAO;
	
	private UserPermissionsDAO userPermissionsDAO;
	
	private RolePermissionsDAO rolePermissionsDAO;
	
	private DelJobDAO delJobDAO;
	private DelPolicyDAO delPolicyDAO;
	private DelJobFiledetailsDAO delJobFiledetailsDAO;
	
	
	private JobDAO jobDAO;
	
	private PolicyDAO policyDAO;
	
	private JobSpDocLibDAO jobSpDocLibDAO;
	
	private JobSpDocLibExcludedPathDAO jobSpDocLibExcludedPathDAO;
	
	private DriveLettersDAO driveLettersDAO;
	
	private SystemCodeDAO systemCodeDAO;
	
	private JobStatusDAO jobStatusDAO;
	
	private JobStatisticsDAO jobStatisticsDAO;
	
	private NodePermissionJobDAO nodePermissionJobDAO;
		
	private ErrorCodeDAO errorCodeDAO;
	
	private RepoStatsDAO repoStatsDAO;
	
	private SystemAlertsDAO systemAlertsDAO;
	
	private ShareAnalysisStatsDAO shareAnalysisStatsDAO;
	
	private PageRightsDAO pageRightsDAO;
	
	private RoleFeatureDAO roleFeatureDAO;
	
	private FilesProcessedSizeDAO filesProcessedSizeDAO;
	
	private LinkAccessUsersDAO linkAccessUsersDAO;
	
	private LinkAccessDocumentsDAO linkAccessDocumentsDAO;
	
//	private GroupGuardianDAO groupGuardianDAO;
	
	private DataGuardianDAO dataGuardianDAO;
	
	private LdapGroupsDAO ldapGroupsDAO;
	
	private ActiveArchiveReportDAO activeArchiveReportDAO;
	
	
	public NodePermissionJobDAO getNodePermissionJobDAO() {
		return nodePermissionJobDAO;
	}

	public void setNodePermissionJobDAO(NodePermissionJobDAO nodePermissionJobDAO) {
		this.nodePermissionJobDAO = nodePermissionJobDAO;
	}

	public JobStatusDAO getJobStatusDAO() {
		return jobStatusDAO;
	}

	public void setJobStatusDAO(JobStatusDAO jobStatusDAO) {
		this.jobStatusDAO = jobStatusDAO;
	}

	public JobStatisticsDAO getJobStatisticsDAO() {
		return jobStatisticsDAO;
	}

	public void setJobStatisticsDAO(JobStatisticsDAO jobStatisticsDAO) {
		this.jobStatisticsDAO = jobStatisticsDAO;
	}

	public SystemCodeDAO getSystemCodeDAO() {
		return systemCodeDAO;
	}

	public void setSystemCodeDAO(SystemCodeDAO systemCodeDAO) {
		this.systemCodeDAO = systemCodeDAO;
	}

	public AgentDAO getAgentDAO() {
		return agentDAO;
	}

	public void setAgentDAO(AgentDAO agentDAO) {
		this.agentDAO = agentDAO;
	}

	public DocumentCategoryDAO getDocumentCategoryDAO() {
		return documentCategoryDAO;
	}

	public void setDocumentCategoryDAO(DocumentCategoryDAO documentCategoryDAO) {
		this.documentCategoryDAO = documentCategoryDAO;
	}

	public DocumentTypeDAO getDocumentTypeDAO() {
		return documentTypeDAO;
	}

	public void setDocumentTypeDAO(DocumentTypeDAO documentTypeDAO) {
		this.documentTypeDAO = documentTypeDAO;
	}

	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	public RoleDAO getRoleDAO() {
		return roleDAO;
	}

	public void setRoleDAO(RoleDAO roleDAO) {
		this.roleDAO = roleDAO;
	}

	public UserPermissionsDAO getUserPermissionsDAO() {
		return userPermissionsDAO;
	}

	public void setUserPermissionsDAO(UserPermissionsDAO userPermissionsDAO) {
		this.userPermissionsDAO = userPermissionsDAO;
	}

	public RolePermissionsDAO getRolePermissionsDAO() {
		return rolePermissionsDAO;
	}

	public void setRolePermissionsDAO(RolePermissionsDAO rolePermissionsDAO) {
		this.rolePermissionsDAO = rolePermissionsDAO;
	}

	public JobDAO getJobDAO() {
		return jobDAO;
	}

	public void setJobDAO(JobDAO jobDAO) {
		this.jobDAO = jobDAO;
	}

	public PolicyDAO getPolicyDAO() {
		return policyDAO;
	}

	public void setPolicyDAO(PolicyDAO policyDAO) {
		this.policyDAO = policyDAO;
	}

	public JobSpDocLibDAO getJobSpDocLibDAO() {
		return jobSpDocLibDAO;
	}

	public void setJobSpDocLibDAO(JobSpDocLibDAO jobSpDocLibDAO) {
		this.jobSpDocLibDAO = jobSpDocLibDAO;
	}

	public JobSpDocLibExcludedPathDAO getJobSpDocLibExcludedPathDAO() {
		return jobSpDocLibExcludedPathDAO;
	}

	public void setJobSpDocLibExcludedPathDAO(
			JobSpDocLibExcludedPathDAO jobSpDocLibExcludedPathDAO) {
		this.jobSpDocLibExcludedPathDAO = jobSpDocLibExcludedPathDAO;
	}

	public DriveLettersDAO getDriveLettersDAO() {
		return driveLettersDAO;
	}

	public void setDriveLettersDAO(DriveLettersDAO driveLettersDAO) {
		this.driveLettersDAO = driveLettersDAO;
	}

	public ErrorCodeDAO getErrorCodeDAO() {
		return errorCodeDAO;
	}

	public void setErrorCodeDAO(ErrorCodeDAO errorCodeDAO) {
		this.errorCodeDAO = errorCodeDAO;
	}

	public RepoStatsDAO getRepoStatsDAO() {
		return repoStatsDAO;
	}

	public void setRepoStatsDAO(RepoStatsDAO repoStatsDAO) {
		this.repoStatsDAO = repoStatsDAO;
	}
	
	public SystemAlertsDAO getSystemAlertsDAO() {
		return systemAlertsDAO;
	}

	public void setSystemAlertsDAO(SystemAlertsDAO systemAlertsDAO) {
		this.systemAlertsDAO = systemAlertsDAO;
	}

	public ShareAnalysisStatsDAO getShareAnalysisStatsDAO() {
		return shareAnalysisStatsDAO;
	}

	public void setShareAnalysisStatsDAO(ShareAnalysisStatsDAO shareAnalysisStatsDAO) {
		this.shareAnalysisStatsDAO = shareAnalysisStatsDAO;
	}

	public PageRightsDAO getPageRightsDAO() {
		return pageRightsDAO;
	}

	public void setPageRightsDAO(PageRightsDAO pageRightsDAO) {
		this.pageRightsDAO = pageRightsDAO;
	}

	public RoleFeatureDAO getRoleFeatureDAO() {
		return roleFeatureDAO;
	}

	public void setRoleFeatureDAO(RoleFeatureDAO roleFeatureDAO) {
		this.roleFeatureDAO = roleFeatureDAO;
	}

	public DelJobDAO getDelJobDAO() {
		return delJobDAO;
	}

	public void setDelJobDAO(DelJobDAO delJobDAO) {
		this.delJobDAO = delJobDAO;
	}

	public DelPolicyDAO getDelPolicyDAO() {
		return delPolicyDAO;
	}

	public void setDelPolicyDAO(DelPolicyDAO delPolicyDAO) {
		this.delPolicyDAO = delPolicyDAO;
	}

	public DelJobFiledetailsDAO getDelJobFiledetailsDAO() {
		return delJobFiledetailsDAO;
	}

	public void setDelJobFiledetailsDAO(DelJobFiledetailsDAO delJobFiledetailsDAO) {
		this.delJobFiledetailsDAO = delJobFiledetailsDAO;
	}

	public FilesProcessedSizeDAO getFilesProcessedSizeDAO() {
		return filesProcessedSizeDAO;
	}

	public void setFilesProcessedSizeDAO(FilesProcessedSizeDAO filesProcessedSizeDAO) {
		this.filesProcessedSizeDAO = filesProcessedSizeDAO;
	}

	public LinkAccessUsersDAO getLinkAccessUsersDAO() {
		return linkAccessUsersDAO;
	}

	public void setLinkAccessUsersDAO(LinkAccessUsersDAO linkAccessUsersDAO) {
		this.linkAccessUsersDAO = linkAccessUsersDAO;
	}

	public LinkAccessDocumentsDAO getLinkAccessDocumentsDAO() {
		return linkAccessDocumentsDAO;
	}

	public void setLinkAccessDocumentsDAO(
			LinkAccessDocumentsDAO linkAccessDocumentsDAO) {
		this.linkAccessDocumentsDAO = linkAccessDocumentsDAO;
	}

//	public GroupGuardianDAO getGroupGuardianDAO() {
//		return groupGuardianDAO;
//	}
//
//	public void setGroupGuardianDAO(GroupGuardianDAO groupGuardianDAO) {
//		this.groupGuardianDAO = groupGuardianDAO;
//	}

	public DataGuardianDAO getDataGuardianDAO() {
		return dataGuardianDAO;
	}

	public void setDataGuardianDAO(DataGuardianDAO dataGuardianDAO) {
		this.dataGuardianDAO = dataGuardianDAO;
	}

	public LdapGroupsDAO getLdapGroupsDAO() {
		return ldapGroupsDAO;
	}

	public void setLdapGroupsDAO(LdapGroupsDAO ldapGroupsDAO) {
		this.ldapGroupsDAO = ldapGroupsDAO;
	}

	public ActiveArchiveReportDAO getActiveArchiveReportDAO() {
		return activeArchiveReportDAO;
	}

	public void setActiveArchiveReportDAO(
			ActiveArchiveReportDAO activeArchiveReportDAO) {
		this.activeArchiveReportDAO = activeArchiveReportDAO;
	}
			
}
