package com.virtualcode.dao;
// default package

import java.util.List;
import java.util.Set;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.User;


public interface UserDAO  {
	   
    
    public void save(User transientInstance) ;
    
	public void delete(User persistentInstance);
    
    public User findById( java.lang.Long id);
        
    public List findByExample(User instance) ;
    
    public List findByProperty(String propertyName, Object value);

	public List findByEmail(Object email);
	
	public List findByFirstName(Object firstName);
	
	public List findByLastName(Object lastName);
	
	public List findByLockStatus(Object lockStatus);
	
	public List findByPasswordHash(Object passwordHash) ;
	
	public List findByUsername(Object username);
	
	public User findUser(String username,String password);

	public List<User> findAll() ;
	
    public User merge(User detachedInstance) ;

    public void attachDirty(User instance) ;
    
    public void attachClean(User instance);

	
}