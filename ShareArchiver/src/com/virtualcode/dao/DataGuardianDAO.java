package com.virtualcode.dao;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.DataGuardian;

/**
 * A data access object (DAO) providing persistence and search support for
 * DataGuardian entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.vo.DataGuardian
 * @author MyEclipse Persistence Tools
 */

public interface DataGuardianDAO{
	
	public void save(DataGuardian transientInstance);

	public void delete(DataGuardian persistentInstance);

	public DataGuardian findById(java.lang.Integer id);

	public List findByExample(DataGuardian instance);

	public List findByProperty(String propertyName, Object value);

	public List findByDataGuardianEmail(Object dataGuardianEmail);

	public List findAll();

	public DataGuardian merge(DataGuardian detachedInstance);

	public void attachDirty(DataGuardian instance);

	public void attachClean(DataGuardian instance);

}