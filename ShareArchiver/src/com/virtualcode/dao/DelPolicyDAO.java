package com.virtualcode.dao;

import java.util.List;

import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.virtualcode.dao.impl.DelPolicyDAOImpl;
import com.virtualcode.vo.DelPolicy;

public interface DelPolicyDAO {
	
	public void save(DelPolicy transientInstance);
	
	public void delete(DelPolicy persistentInstance);
	
	public DelPolicy findById( java.lang.Integer id);
	
	
	public List findByExample(DelPolicy instance);
	
	public List findByProperty(String propertyName, Object value);
	
	public List findByName(Object name);
	
	public List findByModifiedDate(Object modifiedDate);
	
	public List findByAccessDate(Object accessDate);
	
	public List findByArchivalDate(Object archivalDate);
	
	public List findByFileType(Object fileType);
	
	public List findByFileSize(Object fileSize) ;
	
	public List findAll() ;
	
	public DelPolicy merge(DelPolicy detachedInstance);
	
	public void attachDirty(DelPolicy instance);
	
	public void attachClean(DelPolicy instance);
}
