package com.virtualcode.dao;

import java.util.List;

import com.virtualcode.vo.JobSpDocLib;

public interface JobSpDocLibDAO {
	
	public void save(JobSpDocLib transientInstance);

	public void delete(JobSpDocLib persistentInstance);

	public JobSpDocLib findById(java.lang.Integer id);

	public List findByExample(JobSpDocLib instance);

	public List findByProperty(String propertyName, Object value);
	
	public List findByNameLike(String value);
	
	public List findByNameLikeForArchive(String value);
		
	public List findAll();

	public JobSpDocLib merge(JobSpDocLib detachedInstance);

	public void attachDirty(JobSpDocLib instance);

	public void attachClean(JobSpDocLib instance) ;


}