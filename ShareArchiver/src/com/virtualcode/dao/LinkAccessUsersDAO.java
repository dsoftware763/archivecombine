package com.virtualcode.dao;

import java.util.List;
import java.util.Set;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.LinkAccessUsers;

/**
 * A data access object (DAO) providing persistence and search support for
 * LinkAccessUsers entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.vo.LinkAccessUsers
 * @author MyEclipse Persistence Tools
 */

public interface LinkAccessUsersDAO {


	public void save(LinkAccessUsers transientInstance);

	public void delete(LinkAccessUsers persistentInstance);

	public LinkAccessUsers findById(java.lang.Integer id);

	public List findByExample(LinkAccessUsers instance);

	public List findByProperty(String propertyName, Object value);

	public List findByRecipient(Object recipient);

	public List findByPassword(Object password);

	public List findBySender(Object sender);

	public List findAll();
	
	public List findByUsernamePassword(String username, String password);

	public LinkAccessUsers merge(LinkAccessUsers detachedInstance);

	public void attachDirty(LinkAccessUsers instance);

	public void attachClean(LinkAccessUsers instance);

}