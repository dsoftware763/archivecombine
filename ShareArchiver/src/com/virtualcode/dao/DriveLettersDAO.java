package com.virtualcode.dao;

import java.util.List;

import com.virtualcode.vo.DriveLetters;

public interface DriveLettersDAO {
	
	public void save(DriveLetters transientInstance) ;
	
	public void delete(DriveLetters persistentInstance) ;

	public DriveLetters findById(java.lang.Integer id);

	public List findByExample(DriveLetters instance) ;

	public List findByProperty(String propertyName, Object value) ;
	
	public List findByDriveLetter(Object driveLetter);

	public List findBySharePath(Object sharePath);

	public List findByStatus(Object status);

	public List findAll() ;

	public DriveLetters merge(DriveLetters detachedInstance);

	public void attachDirty(DriveLetters instance) ;
	
	public void attachClean(DriveLetters instance);

	
}