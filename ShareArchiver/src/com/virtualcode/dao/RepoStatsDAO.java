package com.virtualcode.dao;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.RepoStats;

/**
 * A data access object (DAO) providing persistence and search support for
 * RepoStats entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.virtualcode.vo.RepoStats
 * @author MyEclipse Persistence Tools
 */

public interface RepoStatsDAO {

	public void save(RepoStats transientInstance);

	public void delete(RepoStats persistentInstance);

	public RepoStats findById(java.lang.Integer id);

	public List findByExample(RepoStats instance);

	public List findByProperty(String propertyName, Object value);

	public List findByRepoPath(Object repoPath);

	public List findByTotalFolders(Object totalFolders);

	public List findByTotalFiles(Object totalFiles);

	public List findByFsDocs(Object fsDocs);

	public List findBySpDocs(Object spDocs);

	public List findByUsedSpace(Object usedSpace);

	public List findByDuplicateDataSize(Object duplicateDataSize);

	public List findAll();
	
	public RepoStats merge(RepoStats detachedInstance);

	public void attachDirty(RepoStats instance);

	public void attachClean(RepoStats instance);
	
}