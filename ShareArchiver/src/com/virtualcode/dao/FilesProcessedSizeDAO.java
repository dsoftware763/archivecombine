package com.virtualcode.dao;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.FilesProcessedSize;

/**
 * A data access object (DAO) providing persistence and search support for
 * FilesProcessedSize entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.vo.FilesProcessedSize
 * @author MyEclipse Persistence Tools
 */

public interface FilesProcessedSizeDAO {


	public static final String TOTAL_PROCESSED_FILES = "totalProcessedFiles";

//	void initDao();

	public void save(FilesProcessedSize transientInstance);

	public void delete(FilesProcessedSize persistentInstance);

	public FilesProcessedSize findById(java.lang.Integer id);

	public List findByExample(FilesProcessedSize instance);

	public List findByProperty(String propertyName, Object value);

	public List findByTotalProcessedFiles(Object totalProcessedFiles);

	public List findAll();

	public FilesProcessedSize merge(FilesProcessedSize detachedInstance);

	public void attachDirty(FilesProcessedSize instance);

	public void attachClean(FilesProcessedSize instance);

}