package com.virtualcode.dao;

import java.util.List;

import com.virtualcode.vo.ShareAnalysisStats;


public interface ShareAnalysisStatsDAO {
	
	public void save(ShareAnalysisStats transientInstance) ;

	public void delete(ShareAnalysisStats persistentInstance);
	
	public void deleteByDriveId(Integer driveLetterId);

	public ShareAnalysisStats findById(java.lang.Long id) ;

	public List findByExample(ShareAnalysisStats instance);

	public List findByProperty(String propertyName, Object value) ;
	
	
	public List<ShareAnalysisStats> findByShare(Integer shareId);

	public List findByDocumentCatName(Object documentCatName) ;
	
	public List findByTotalVolume(Object totalVolume) ;
	
	public List findByThreeMonthsOlderVol(Object threeMonthsOlderVol);

	public List findBySixMonthsOlderVol(Object sixMonthsOlderVol);

	public List findByOneYearOlderVol(Object oneYearOlderVol);
	
	public List findByTwoYearOlderVol(Object twoYearOlderVol) ;

	public List findByThreeYearOlderVol(Object threeYearOlderVol) ;
	
	public List findAll() ;

	public ShareAnalysisStats merge(ShareAnalysisStats detachedInstance);

	public void attachDirty(ShareAnalysisStats instance) ;
	
	public void attachClean(ShareAnalysisStats instance);

}