package com.virtualcode.dao;

import java.util.List;

import com.virtualcode.vo.JobSpDocLibExcludedPath;

public interface JobSpDocLibExcludedPathDAO {
	
	public void save(JobSpDocLibExcludedPath transientInstance);
	
	public void delete(JobSpDocLibExcludedPath persistentInstance);

	public JobSpDocLibExcludedPath findById(java.lang.Integer id);

	public List findByExample(JobSpDocLibExcludedPath instance);

	public List findByProperty(String propertyName, Object value);

	public List findByPath(Object path);

	public List findAll() ;

	public JobSpDocLibExcludedPath merge(JobSpDocLibExcludedPath detachedInstance);

	public void attachDirty(JobSpDocLibExcludedPath instance);

	public void attachClean(JobSpDocLibExcludedPath instance) ;
	
}