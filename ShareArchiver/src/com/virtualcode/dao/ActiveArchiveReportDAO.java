package com.virtualcode.dao;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.ActiveArchiveReport;

/**
 * A data access object (DAO) providing persistence and search support for
 * ActiveArchiveReport entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.vo.ActiveArchiveReport
 * @author MyEclipse Persistence Tools
 */

public interface ActiveArchiveReportDAO{
	
	public void save(ActiveArchiveReport transientInstance);

	public void delete(ActiveArchiveReport persistentInstance);

	public ActiveArchiveReport findById(java.lang.Integer id);

	public List findByExample(ActiveArchiveReport instance);

	public List findByProperty(String propertyName, Object value);

	public List findByDailyCount(Object dailyCount);

	public List findByTotalCount(Object totalCount);

	public List findByTotalJobs(Object totalJobs);

	public List findAll();

	public ActiveArchiveReport merge(ActiveArchiveReport detachedInstance);

	public void attachDirty(ActiveArchiveReport instance);

	public void attachClean(ActiveArchiveReport instance);

}