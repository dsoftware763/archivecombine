package com.virtualcode.dao;

import java.util.List;

import com.virtualcode.vo.ErrorCode;


public interface ErrorCodeDAO {
	
	public void save(ErrorCode transientInstance);

	public void delete(ErrorCode persistentInstance);

	public ErrorCode findById(java.lang.Integer id);

	public List findByExample(ErrorCode instance);

	public List findByProperty(String propertyName, Object value) ;

	public List findByDescription(Object description);

	public List findAll() ;
	
	public ErrorCode merge(ErrorCode detachedInstance) ;

	public void attachDirty(ErrorCode instance) ;

	public void attachClean(ErrorCode instance) ;

	
}