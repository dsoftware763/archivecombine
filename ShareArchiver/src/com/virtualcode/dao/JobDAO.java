package com.virtualcode.dao;

import java.util.List;

import com.virtualcode.vo.Job;
import com.virtualcode.vo.JobSpDocLib;


public interface JobDAO  {
	
	public void save(Job transientInstance);

	public void delete(Job persistentInstance);
	
	public void deleteJobSpDocLib(Job persistentInstance);
	
	public void deleteJobExcludedPaths(Job persistentInstance);
	
	
	public Job findById(java.lang.Integer id);

	public List findByExample(Job instance) ;

	public List findByProperty(String propertyName, Object value) ;

	public List findByActionType(Object actionType) ;

	public List findByActive(Object active);

	
	public List findByName(Object name);
	
	
	public List findAll() ;
	
	public List findAllFoStatus();

	public Job merge(Job detachedInstance);

	public void attachDirty(Job instance);

	public void attachClean(Job instance);
	
	public List<Job> findAgentImportJobs(String agentName);
	
	public List<Job> findAgentIndexingJobs(String agentName);
	
	public List<Job> findAgentExportJobs(String agentName);
	
		
	public boolean updateJobExecutionStatus(final Integer jobStatusId,final String status);
	
	public List<Integer> findCancelJobs();

	public List<Job> findAgentScheduledJobs(String agentName);
	
	public List<Job> findAllScheduledJobs();
	
	public List<Job> findAgentProactiveJobs(String agentName);

	public List<Job> findAgentActiveArchivingJobs(String agentName);
	
	public List<Job> findAllActiveArchivingJobs();

	public List<Job> findAgentPrintArchivingJobs(String agentName);

	public List<Job> findAgentRetensionJobs(String agentName);
	
	public List<Job> findAgentRestoreJobs(String agentName);
	
	public List<JobSpDocLib> findShareThreshholdJobs(String sharePath);
	
	public List<JobSpDocLib> findShareAllThreshholdJobs(String sharePath);
	
	public List findAllForStatus(String type) ;
	
	public List findAllActiveImportJobs();
	
	public List findAllActiveJobs();
}