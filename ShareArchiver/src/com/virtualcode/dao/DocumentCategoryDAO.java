package com.virtualcode.dao;

// default package

import java.util.List;

import com.virtualcode.vo.DocumentCategory;

/**
 	
  * @author AtiqurRehman
 */

public interface DocumentCategoryDAO   {
	   

    public DocumentCategory save(DocumentCategory transientInstance) ;
    
    public DocumentCategory update(DocumentCategory transientInstance);
    
	public void delete(DocumentCategory persistentInstance);
    
    public DocumentCategory findById( java.lang.Integer id);
    
    
    public List findByExample(DocumentCategory instance);
    
    public List findByProperty(String propertyName, Object value);

	public List findAll();
	
    public DocumentCategory merge(DocumentCategory detachedInstance);
  
  
}