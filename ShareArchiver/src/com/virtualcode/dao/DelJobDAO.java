package com.virtualcode.dao;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.DelJob;

/*
 	* A data access object (DAO) providing persistence and search support for DelJob entities.
 			* Transaction control of the save(), update() and delete() operations 
		can directly support Spring container-managed transactions or they can be augmented	to handle user-managed Spring transactions. 
		Each of these methods provides additional information for how to configure it for the desired type of transaction control. 	
	 * @see com.virtualcode.vo.DelJob
  * @author MyEclipse Persistence Tools 
 */

public interface DelJobDAO  {
    
    public void save(DelJob transientInstance);
    
	public void delete(DelJob persistentInstance);
    
    public DelJob findById( java.lang.Integer id);
    
    
    public List findByExample(DelJob instance) ;
    public List findByProperty(String propertyName, Object value) ;
	public List findByActionType(Object actionType
	) ;
	
	public List findByName(Object name);
	public List findAll();
    public DelJob merge(DelJob detachedInstance) ;

    public void attachDirty(DelJob instance);
    public void attachClean(DelJob instance);

}