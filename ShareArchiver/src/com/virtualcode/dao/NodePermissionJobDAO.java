package com.virtualcode.dao;

import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.NodePermissionJob;

/**
 * A data access object (DAO) providing persistence and search support for
 * NodePermissionJob entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.vo.NodePermissionJob
 * @author MyEclipse Persistence Tools
 */

public interface NodePermissionJobDAO {
	
	public void save(NodePermissionJob transientInstance);

	public void delete(NodePermissionJob persistentInstance);

	public NodePermissionJob findById(java.lang.Integer id) ;

	public List findByExample(NodePermissionJob instance) ;

	public List findByProperty(String propertyName, Object value);

	public List findByName(Object name) ;

	public List findByCommand(Object command);
	
	public List findByStatus(Object status);

	public List findByRegisterdate(Object registerdate) ;

	public List findByStartdate(Object startdate) ;

	public List findByFinisheddate(Object finisheddate) ;

	public List findBySecureinfo(Object secureinfo) ;

	public List findBySids(Object sids) ;

	public List findByDenysids(Object denysids);

	public List findBySharesids(Object sharesids);

	public List findByDenysharesids(Object denysharesids);

	public List findByIsrecursive(Object isrecursive) ;

	public List findAll();
	
	public NodePermissionJob merge(NodePermissionJob detachedInstance);

	public void attachDirty(NodePermissionJob instance);

	public void attachClean(NodePermissionJob instance) ;

}