package com.virtualcode.dao;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.virtualcode.vo.SystemAlerts;

/**
 * A data access object (DAO) providing persistence and search support for
 * SystemAlerts entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.virtualcode.vo.SystemAlerts
 * @author MyEclipse Persistence Tools
 */

public interface SystemAlertsDAO {
	
	public void save(SystemAlerts transientInstance);

	public void delete(SystemAlerts persistentInstance);

	public SystemAlerts findById(java.lang.Integer id);

	public List findByExample(SystemAlerts instance);

	public List findByProperty(String propertyName, Object value);

	public List findByUsername(Object username);

	public List findByAlertType(Object alertType);

	public List findByAlertMessage(Object alertMessage);

	public List findAll();

	public SystemAlerts merge(SystemAlerts detachedInstance);

	public void attachDirty(SystemAlerts instance);

	public void attachClean(SystemAlerts instance);
}