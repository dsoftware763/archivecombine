/*
 * Repository servlet that starts the repository and registers it to JNDI ans RMI.
 * If you already have the repository registered in this appservers JNDI context,
 * or if its accessible via RMI, you do not need to use this servlet.
 */

package com.virtualcode.base.web;

/**
 *
 * @author Akhnukh
 */
public class RepositoryStartupServlet extends org.apache.jackrabbit.j2ee.RepositoryStartupServlet {
   
}
