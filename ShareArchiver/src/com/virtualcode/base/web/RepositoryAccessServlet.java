/*
 * This servlet provides other servlets and jsps a common way to access
 * the repository. The repository can be accessed via JNDI, RMI or Webdav.
 */

package com.virtualcode.base.web;

/**
 *
 * @author Akhnukh
 */
public class RepositoryAccessServlet extends org.apache.jackrabbit.j2ee.RepositoryAccessServlet {
   
}
