/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.virtualcode.base.core.db;

import java.io.File;
import java.util.logging.Level;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.apache.log4j.Logger;

/**
 *
 * @author Akhnukh
 */
public class DatabaseManager {

    static final Logger log = Logger.getLogger(DatabaseManager.class);
    private static final String HIBERNATE_CFG_FILE = "../webapps/archive/WEB-INF/hibernate.cfg.xml";
    private static DatabaseManager dbManager=null;
    private static SessionFactory sessions=null;

    private DatabaseManager(){
    }

    public static DatabaseManager getInstance() {
        log.debug("DatabaseManager  getInstance");
        if (dbManager==null && sessions ==null) {
            dbManager = new DatabaseManager();
            try {
                File cfgFile=new File(HIBERNATE_CFG_FILE);
                sessions = new Configuration().configure(cfgFile).buildSessionFactory();
            } catch (Exception ex) {
                log.error("Unable to init Hibernate ex: "+ex.toString());
                ex.printStackTrace();

            }
        }
        return dbManager;
    }

    public SessionFactory getSessionFactory(){
        return sessions;
    }
    

}
