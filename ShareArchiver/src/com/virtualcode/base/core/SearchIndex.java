
package com.virtualcode.base.core;

/**
 *
 * @author Atiqur Rehman
 */
public class SearchIndex extends org.apache.jackrabbit.core.query.lucene.SearchIndex {

	public SearchIndex(){
		int processors = Runtime.getRuntime().availableProcessors();
		System.out.println("Processors Count **** "+processors);
		setExtractorPoolSize(processors*10);
		setMinMergeDocs(50);	
		setResultFetchSize(10000);
		setRespectDocumentOrder(false);
		
	}
	
}
