package com.virtualcode.security;

public interface PageAuthorizator {
	
	public boolean isAuthorizeRole(String view, String role);
	
}
