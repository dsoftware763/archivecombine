/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.virtualcode.security;

import javax.jcr.Credentials;

/**
 *
 * @author Akhnukh
 */
public interface VirtualCodeAuthenticator {

    public boolean authenticate(Credentials userCredentials) throws Exception;

}
