/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.security.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.jcr.Credentials;
import javax.jcr.SimpleCredentials;

import org.apache.log4j.Logger;

import com.virtualcode.security.VirtualCodeAuthenticator;

/**
 * Class will handle user authentication from file. User name and password will be loaded first time when authenticate class will be called.
 *
 * @author Akhnukh
 */
public class FileBasedAuthenticator implements VirtualCodeAuthenticator {

    static final Logger log = Logger.getLogger(FileBasedAuthenticator.class);
    public static String USER_FILE = "/users.properties";
    //private final String REPOSITORY_BOOTSTRAP_PATH = "../webapps/archive/WEB-INF/bootstrap.properties";
    private final String REPOSITORY_HOME = "repository.home";
    private static FileBasedAuthenticator fileBaseAuthenticator = null;
    private Properties users;

    private final static String INVALID_USERNAME="User name doesen't exist, Please try again with valid user.";
    private final static String INVALID_PASSWORD="Invalid Password, Please try again with valid password.";
    private final static String INIT_ERROR="Internal system error, Please try again or contact administrator.";

    /**
     *
     * @throws Exception
     */
    public FileBasedAuthenticator() throws AuthenticatorException {
        init();
    }

    
    public synchronized static FileBasedAuthenticator getInstance() throws Exception {
        if (fileBaseAuthenticator == null) {
            fileBaseAuthenticator = new FileBasedAuthenticator();
        }
        return fileBaseAuthenticator;
    }

    public void init() throws AuthenticatorException {
        users = new Properties();

        try {
            /****  Bootstrap is moved to com.virtualcode.resources.bootstrap
             * 
            Properties properties = new Properties();
            properties.load(new FileInputStream(REPOSITORY_BOOTSTRAP_PATH));
            String repositoryPath = (String) properties.getProperty(REPOSITORY_HOME);
             *
             */
            ResourceBundle bundle   =   ResourceBundle.getBundle("com.virtualcode.resources.bootstrap");
            String repositoryPath   =   bundle.getString(REPOSITORY_HOME);
            
            log.debug("Respository path property loaded : "+repositoryPath);
            users.load(new FileInputStream(repositoryPath+USER_FILE));
        } catch (IOException e) {
            log.error("init: "+e);
            throw new AuthenticatorException("Error while loading user repositry, Please check users file on server.");
        }
    }

    /**
     * Method will authenticate and return success/failure based on input credential matching.
     *
     * @param userCredentials
     * @return
     */
    @Override
    public boolean authenticate(Credentials userCredentials) throws AuthenticatorException {

            log.info("authenticate: started");
            if (users != null) {
                String username = ((SimpleCredentials) userCredentials).getUserID();
                String password = new String(((SimpleCredentials) userCredentials).getPassword());
                log.debug("UserID: " + username);
                log.debug("Password: " + password);

                if (username != null && username.length() > 0) {
                    if (users.getProperty(username) != null && users.getProperty(username).equalsIgnoreCase(password)) {
                        log.debug("Authenticated");
                        return true;
                    } else {
                        log.debug("Invalid User Name or Password");
                        throw new AuthenticatorException(INVALID_PASSWORD);
                    }

                } else {
                 throw new AuthenticatorException(INVALID_USERNAME);
                }

            } else {
                throw new AuthenticatorException(INIT_ERROR);
            }

    }
}
