package com.virtualcode.security.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;

import jespa.ldap.LdapAccount;
import jespa.ldap.LdapSecurityProvider;
import jespa.util.SID;

import org.apache.log4j.Logger;

import com.virtualcode.vo.SystemCodeType;
import com.virtualcode.security.VirtualCodeAuthorization;
import com.virtualcode.util.ResourcesUtil;

/**
 *
 * @author Akhnukh
 */
public class VirtualCodeAuthorizatorImpl implements VirtualCodeAuthorization {

    private static VirtualCodeAuthorizatorImpl vcAuthorizator;
    Properties configProperties;
    LdapSecurityProvider lsp;
    static final Logger log = Logger.getLogger(VirtualCodeAuthorizatorImpl.class);
    private final static String DOMAIN_NAME = "domain.name";
   
    private HashMap<String, String> sidsMap;

    private VirtualCodeAuthorizatorImpl() {
        ResourcesUtil   ru  =   ResourcesUtil.getResourcesUtil();
             
        
        //ResourceBundle bundle = ResourceBundle.getBundle("com.virtualcode.resources.activedirauthenticator");
        configProperties = ru.convertResourceUtilToProperties(SystemCodeType.ACTIVE_DIR_AUTH);//VirtualcodeUtil.convertResourceBundleToProperties(bundle);
        
        log.debug("configProperties " + configProperties);

        log.debug("VirtualCodeAuthorizatorImpl configuration loaded.");
        sidsMap = new HashMap<String, String>();
        sidsMap = getDefaultSids();
        log.debug("default SIDs loaded");

    }

    public synchronized static VirtualCodeAuthorizatorImpl getInstance() throws Exception {
        if (vcAuthorizator == null) {
            vcAuthorizator = new VirtualCodeAuthorizatorImpl();
        }
        return vcAuthorizator;
    }

    public boolean authorize(String username, List sidsLists) throws Exception {

        log.debug("Authorization Started ............");
        log.debug("username  : " + username);
        log.debug("sidsLists : " + sidsLists);

        boolean authorize = false;
        try {
            username += configProperties.getProperty(DOMAIN_NAME);
            log.debug("UserID After appending domain name: " + username);

            String[] attrib = {"tokenGroups","objectSid"};
            lsp = new LdapSecurityProvider(configProperties);
            LdapAccount acct = (LdapAccount) lsp.getAccount(username, attrib);
            log.debug("Account :" + acct);
            
            ArrayList<String> userGroup = new ArrayList<String>();
            List lval = (List) acct.getProperty("tokenGroups");
            Iterator liter = lval.iterator();
            while (liter.hasNext()) {
                String sid = ((SID) liter.next()).toString();
                userGroup.add(sid);                
            }
            //add user sid to the collection
           // jespa.util.SID sid = acct.getProperty("objectSid");
            
            String val = acct.getProperty("objectSid").toString();
            System.out.println("user sid: " + val +"------------------------");
//            Iterator iter = lval.iterator();
//            while (iter.hasNext()) {
//                String sid = ((SID) iter.next()).toString();
//                userGroup.add(sid);                
//            }
            userGroup.add(val);
            
            //adding the default everyone group
//            userGroup.add("S-1-1-0");
            log.debug("adding groups from csv file");
            Set<String> keys = sidsMap.keySet();
            userGroup.addAll(keys);

            log.debug("Group Size :" + userGroup.size());
            log.debug("Files sidsLists Size :" + sidsLists.size());            

            if (userGroup != null && userGroup.size() > 0 && sidsLists != null && sidsLists.size() > 0 && sidsLists.size() == 4) {

                boolean checkComplete = false;
                for (int i = 0; i < userGroup.size() && !checkComplete; i++) {
                    String memberGrp = userGroup.get(i);
                    log.debug("Checking for member Group.... :" + memberGrp);
                    System.out.println("Checking for member Group for deny.... :" + memberGrp);
                    List notAllowedSids = (List) sidsLists.get(1);
                    List sharedNotAllowedSids = (List) sidsLists.get(3);
                    // User is member of not AllowedSID Normal OR Shared
                    if (isMemberOf(notAllowedSids, memberGrp) || isMemberOf(sharedNotAllowedSids, memberGrp)) {
                        checkComplete = true;
                        authorize = false;
                    }
                }//for(int i=0;i<userGroup.size() && !checkComplete;i++){
//          modified to allow access if any group is found in any allowed SID (one group in shared allow and one in security allow)
                boolean found = false;
                for (int i = 0; i < userGroup.size() && !checkComplete; i++) {

                    String memberGrp = userGroup.get(i);
                    log.debug("Checking for member Group.... :" + memberGrp);                    
//                    List allowedSids = (List) sidsLists.get(0);
                    List sharedAllowedSids = (List) sidsLists.get(2);

//                    if(isMemberOf(allowedSids, memberGrp) || isMemberOf(sharedAllowedSids, memberGrp)){
                    if (isMemberOf(sharedAllowedSids, memberGrp)) {
                        found = true;
//                        authorize = true;
                        log.debug("one SID found, breaking loop for share allow group: " + memberGrp);                        

                        break;
                    }                    
                }//for(int i=0;i<userGroup.size() && !checkComplete;i++){
                
                log.debug("found: " + found);
                //System.out.println("found: " + found);
                if(!found){
                     checkComplete = true;
                }

                for (int i = 0; i < userGroup.size() && !checkComplete; i++) {
                     String memberGrp = userGroup.get(i);
                    log.debug("Checking for member Group.... :" + memberGrp);                    
                    List allowedSids = (List) sidsLists.get(0);
//                    List sharedAllowedSids = (List) sidsLists.get(2);

//                    if(isMemberOf(allowedSids, memberGrp) || isMemberOf(sharedAllowedSids, memberGrp)){
                    if ((found) && isMemberOf(allowedSids, memberGrp)) {                        
                        log.debug("one SID found, breaking loop for security allow group: " + memberGrp);

                        checkComplete = true;
                        authorize = true;
                    }
                }//for(int i=0;i<userGroup.size() && !checkComplete;i++){

//                end 
//                for (int i = 0; i < userGroup.size() && !checkComplete; i++) {
//                    String memberGrp = userGroup.get(i);
//                    log.debug("Checking for member Group.... :" + memberGrp);
//                    List allowedSids = (List) sidsLists.get(0);
//                    List sharedAllowedSids = (List) sidsLists.get(2);
//
////                    if(isMemberOf(allowedSids, memberGrp) || isMemberOf(sharedAllowedSids, memberGrp)){
//                    if (isMemberOf(sharedAllowedSids, memberGrp) && isMemberOf(allowedSids, memberGrp)) {
//                        checkComplete = true;
//                        authorize = true;
//                    }
//                }//for(int i=0;i<userGroup.size() && !checkComplete;i++){


            }
        } finally {
            lsp.dispose();
        }
        return authorize;
    }

    private boolean isMemberOf(List sidList, String sidToSearch) {

        boolean found = false;
        try {
            Collections.sort(sidList);

            found = ((Collections.binarySearch(sidList, sidToSearch) < 0) ? false : true);
        } catch (Exception ex) {
            log.error("Error while searching SID in List " + ex);
        }

        return found;
    }

    public HashMap<String, String> getDefaultSids() {
        HashMap<String, String> defaultSids = new HashMap<String, String>();
        String filePath="/defaultSids.csv";
        
        URL url = getClass().getResource(filePath);
        File sidFile = new File(url.getPath().replaceAll("%20", " "));
        try {
            BufferedReader br = new BufferedReader(new FileReader(sidFile));
            String line = "";
            while (br != null && (line = br.readLine()) != null) {
                String values[] = line.split(",");
                if(values.length==2){
                   defaultSids.put(values[0], values[1]);
                }
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            log.error("error while loading default SID file" + ex.getMessage());
        } catch (IOException ex) {
            ex.printStackTrace();
            log.error("error while loading default SID file" + ex.getMessage());
        }
        return defaultSids;
    }
//function to return sid values for a username

    public String sidString(String username) throws Exception {
        String sids = "";
        try {
            username += configProperties.getProperty(DOMAIN_NAME);
            log.debug("UserID After appending domain name: " + username);
            //sid string added to suport user level permissions
            String[] attrib = {"tokenGroups", "objectSid"};
            lsp = new LdapSecurityProvider(configProperties);
            LdapAccount acct = (LdapAccount) lsp.getAccount(username, attrib);
            log.debug("Account :" + acct);

            ArrayList<String> userGroup = new ArrayList<String>();
            List lval = (List) acct.getProperty("tokenGroups");
            Iterator liter = lval.iterator();
            while (liter.hasNext()) {
                String sid = ((SID) liter.next()).toString();
                sids = sids + sid + ",";
                //userGroup.add(sid);
            }
            String val = acct.getProperty("objectSid").toString();
            System.out.println("user sid: " + val +"------------------------");
            sids = sids + val +",";
            

            //adding the everyone SID alongwith other group SIDs
            sids = sids + "S-1-1-0";
//            if(sids.endsWith(",")){
//                sids = sids.substring(0, sids.length()-1);
//            }
            log.debug("SIDs found:" + sids);
        } finally {
            lsp.dispose();
        }
        return sids;
    }

    public static void main(String[] arg) {

        try {
            VirtualCodeAuthorizatorImpl authorizator = VirtualCodeAuthorizatorImpl.getInstance();
            List list = new ArrayList();
//            [[S-1-5-21-176684331-2343474825-1308384615-1117], [], [S-1-5-21-176684331-2343474825-1308384615-1117], []]
//                                if (innerNode.hasProperty(CustomProperty.VC_ALLOWED_SIDS)) {
//
//                        Property prop = innerNode.getProperty(CustomProperty.VC_ALLOWED_SIDS);
//                        String[] allowSids = prop.getString().split(",");
//                        allowAces.addAll(Arrays.asList(allowSids));
//                    }
//            String str = "S-1-5-21-176684331-2343474825-1308384615-1117, , S-1-5-21-176684331-2343474825-1308384615-1117,S-2-2";
//            String[] array = str.split(",");
//            list.addAll(Arrays.asList(array));
            ArrayList a = new ArrayList();
            a.add("S-1-5-21-176684331-2343474825-1308384615-513"); // security allow
            ArrayList b = new ArrayList();
            b.add("S-1-5-32-5554");     //security deny
            ArrayList c = new ArrayList();
            c.add("S-1-5-32-545");      //share allow
            ArrayList d = new ArrayList();
            d.add("");                  //share deny

            list.add(a);
            list.add(b);
            list.add(c);
            list.add(d);
            boolean found = authorizator.authorize("jawwad", list);
            System.out.println("found = " + found);
//                   HashMap<String, String> map = authorizator.getDefaultSids();
//       Set<String> keys= map.keySet();
//       for(String key:keys){
//           System.out.println("key: " + key + " value: " + map.get(key));
//       }
//            System.out.println(authorizator.sidString("bilal"));

        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(VirtualCodeAuthorizatorImpl.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }


    }
}
