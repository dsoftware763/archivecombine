/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.security.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.ResourceBundle;

import javax.jcr.Credentials;
import javax.jcr.SimpleCredentials;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.virtualcode.base.core.db.DatabaseManager;
import com.virtualcode.security.VirtualCodeAuthenticator;

/**
 * Class will handle user authentication from DB. User name and password will be loaded first time when authenticate class will be called.
 *
 * @author Akhnukh
 */
public class DBBasedAuthenticator implements VirtualCodeAuthenticator {

    static final Logger log = Logger.getLogger(DBBasedAuthenticator.class);
    private static DBBasedAuthenticator dbBaseAuthenticator = null;
    //private final String DB_CONFIG_FILE = "../webapps/archive/WEB-INF/dbconfig.properties";
    private final String CONNECTION_URL = "connection.url";
    private final String CONNECTION_DRIVER = "connection.driver";
    private final static String INVALID_USERNAME = "User name doesen't exist, Please try again with valid user.";
    private final static String INVALID_PASSWORD = "Invalid Password, Please try again with valid password.";
    private final static String INIT_ERROR = "Internal system error, Please try again or contact administrator.";

    private String connectionUrl        =   "";//jdbc:mysql://localhost/bitarch?user=root&password=passw0rd";
    private String connectionDriver     =   "";//com.mysql.jdbc.Driver";

    /**
     *
     * @throws Exception
     */
    public DBBasedAuthenticator() throws AuthenticatorException {
        /****   Now Properties are stored into ResourceBundle, so omitted this code
        try {

            Properties properties = new Properties();
            properties.load(new FileInputStream(DB_CONFIG_FILE));
            this.connectionUrl= (String) properties.getProperty(CONNECTION_URL);
            this.connectionDriver= (String) properties.getProperty(CONNECTION_DRIVER);
            
            log.debug("Respository path property loaded with URL : "+connectionUrl+"\nDriver : "+this.connectionDriver);
        } catch (IOException e) {
            log.error("init: "+e);
            throw new AuthenticatorException("Error while loading user repositry, Please check users file on server.");
        }
        */

        ResourceBundle bundle   =   ResourceBundle.getBundle("com.virtualcode.resources.bootstrap");
        this.connectionUrl      =   bundle.getString(CONNECTION_URL);
        this.connectionDriver   =   bundle.getString(CONNECTION_DRIVER);
        /*
        ResourcesUtil   ru  =   ResourcesUtil.getResourcesUtil();
        this.connectionUrl  =   ru.getCodeValue(CONNECTION_URL, SystemCodeType.SHAREARCHIVER_PROP);
        this.connectionDriver   =   ru.getCodeValue(CONNECTION_DRIVER, SystemCodeType.SHAREARCHIVER_PROP);
*/
        
        log.debug("Respository path property loaded with URL : "+connectionUrl+"\nDriver : "+this.connectionDriver);
    }

    public synchronized static DBBasedAuthenticator getInstance() throws Exception {
        if (dbBaseAuthenticator == null) {
            dbBaseAuthenticator = new DBBasedAuthenticator();
        }
        return dbBaseAuthenticator;
    }

    /**
     * Method will authenticate and return success/failure based on input credential matching using hibernate.
     *
     * @param userCredentials
     * @return
     */

    /* TODO: For time being because of hibernate database exception on test enviornment and other issue related
     * to query result caching we will use plain JDBC calling.
     * For this purpose created another method which will use plain JDBC.
     * Using session.clear, tx.clear and close etc solved caching issue.
     * So this method is not in use currently.
     */
//    @Override 
    public boolean authenticateUsingHibernate(Credentials userCredentials) throws AuthenticatorException {

        log.info("DB authenticate: started");
        boolean authenticated = false;

        String username = ((SimpleCredentials) userCredentials).getUserID();
        String password = new String(((SimpleCredentials) userCredentials).getPassword());
        log.debug("UserID: " + username);
        log.debug("Password: " + password);

        if (username != null && username.length() > 0) {

            DatabaseManager dbManager = DatabaseManager.getInstance();
            Session session = dbManager.getSessionFactory().openSession();

            // Perform life-cycle operations under a transaction
            Transaction tx = null;
            try {
                tx = session.beginTransaction();
                String SQL_QUERY = "select agnt.login from agent agnt where agnt.login='" + username + "' and agnt.password='" + password + "'";
                Query query = session.createQuery(SQL_QUERY);
                for (Iterator it = query.iterate(); it.hasNext();) {
                    String row = (String) it.next();
                    authenticated = true;
                }
                tx.commit();
//                    session.evict(tx);
            } catch (HibernateException e) {
                if (tx != null) {
                    tx.rollback();
                }
                log.error("Error while execution: " + e);
                e.printStackTrace();
            } finally {
                session.clear();
                session.close();
            }

            if (authenticated) {
                log.debug(username+" Authenticated");
                return true;
            } else {
                log.debug("Invalid User Name or Password");
                throw new AuthenticatorException(INVALID_PASSWORD);
            }

        } else {
            throw new AuthenticatorException(INVALID_USERNAME);
        }
    }

     /**
     * Method will authenticate and return success/failure based on input credential matching using plain
      * JDBC calling.
     *
     * @param userCredentials
     * @return
     */
    @Override
    public boolean authenticate(Credentials userCredentials) throws AuthenticatorException {


        log.info("DB authenticate: started");
        boolean authenticated = false;
        Connection con =null;
        Statement st =null;
      

        String username = ((SimpleCredentials) userCredentials).getUserID();
        String password = new String(((SimpleCredentials) userCredentials).getPassword());
        log.debug("UserID: " + username);
        log.debug("Password: " + password);

        if (username != null && username.length() > 0) {
            try {
                Class.forName(connectionDriver);
                con = DriverManager.getConnection(connectionUrl);
                st = con.createStatement();
                String SQL_QUERY = "select agnt.login from agent agnt where agnt.login='" + username + "' and agnt.password='" + password + "'";
                log.debug("Query : " + SQL_QUERY);
                ResultSet rs = st.executeQuery(SQL_QUERY);

                while (rs != null && rs.next()) {
                    String row = (String) rs.getString("login");
                    authenticated = true;
                }
                rs.close();
            } catch (SQLException e) {
                 log.error("Error while execution: " + e);
            } catch (ClassNotFoundException cE) {
                  log.error("Error while execution: " + cE);
            } finally {
                try {
                    st.close();
                    con.close();
                } catch (SQLException ex) {
                      log.error("Error while closing st,con: " + ex);
                }
                
            }
            if (authenticated) {
                log.debug(username+" Authenticated");
                return true;
            } else {
                log.debug("Invalid User Name or Password");
                throw new AuthenticatorException(INVALID_PASSWORD);
            }

        } else {
            throw new AuthenticatorException(INVALID_USERNAME);
        }
    }
}
