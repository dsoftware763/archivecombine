/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.virtualcode.security.impl;


/**
 *
 * @author Akhnukh
 */
public class AuthenticatorException  extends Exception{

    private String message;

    @Override
    public String toString() {
        return message;
    }
    public AuthenticatorException(){
        super();
    }
    public AuthenticatorException(String message){
        super(message);
        this.message=message;

    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage(){
        return this.message;
    }

}
