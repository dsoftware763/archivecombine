/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.security.impl;

import java.util.Properties;

import javax.jcr.Credentials;
import javax.jcr.SimpleCredentials;

import jespa.ldap.LdapSecurityProvider;
import jespa.security.PasswordCredential;
import jespa.security.SecurityProviderException;

import org.apache.log4j.Logger;

import com.virtualcode.vo.SystemCodeType;
import com.virtualcode.security.VirtualCodeAuthenticator;
import com.virtualcode.util.ResourcesUtil;

/**
 * Class will handle user authentication from DB. User name and password will be loaded first time when authenticate class will be called.
 *
 * @author Akhnukh
 */
public class ActiveDirAuthenticator implements VirtualCodeAuthenticator {
    
    static final Logger log = Logger.getLogger(ActiveDirAuthenticator.class);
    private static ActiveDirAuthenticator activeDiectoryAuthenticator = null;
    private final static String INVALID_USERNAME = "User name doesen't exist, Please try again with valid user.";
    private final static String INVALID_PASSWORD = "Invalid Password, Please try again with valid password.";
    private final static String INIT_ERROR = "Internal system error, Please try again or contact administrator.";
    private final static String DOMAIN_NAME="domain.name";
    
    Properties configProperties;
    LdapSecurityProvider lsp;
    

    /**
     *
     * @throws Exception
     */
    private ActiveDirAuthenticator() throws AuthenticatorException {
        
        //ResourceBundle bundle = ResourceBundle.getBundle("com.virtualcode.resources.activedirauthenticator");
        ResourcesUtil   ru  =   ResourcesUtil.getResourcesUtil();
        configProperties = ru.convertResourceUtilToProperties(SystemCodeType.ACTIVE_DIR_AUTH);//VirtualcodeUtil.convertResourceBundleToProperties(bundle);
        System.out.println(configProperties);
        lsp= new LdapSecurityProvider(configProperties);
        log.debug("ActiveDirAuthenticator configuration loaded.");
    }
    
    public synchronized static ActiveDirAuthenticator getInstance() throws Exception {
        if (activeDiectoryAuthenticator == null) {
            activeDiectoryAuthenticator = new ActiveDirAuthenticator();
        }
        return activeDiectoryAuthenticator;
    }    
    
    public synchronized static ActiveDirAuthenticator getInstance(boolean force) throws Exception {
        if (force) {
            activeDiectoryAuthenticator = new ActiveDirAuthenticator();
        }
        return activeDiectoryAuthenticator;
    }

    /**
     * Method will authenticate and return success/failure based on input credential matching with credentials in AD
     *
     * @param userCredentials
     * @return
     */
    @Override
    public boolean authenticate(Credentials userCredentials) throws AuthenticatorException {
        
        
        log.info("AD authenticate: started");
        boolean authenticated = false;

        String username = ((SimpleCredentials) userCredentials).getUserID();
        String password = new String(((SimpleCredentials) userCredentials).getPassword());
        log.debug("UserID: " + username);
        log.debug("Password: " + password);
        
       
        
            if (username != null && username.length() > 0) {
                
                username+=configProperties.getProperty(DOMAIN_NAME);
                log.debug("UserID After appending domain name: " + username);
                
                try {
                    if(lsp!=null){
                         PasswordCredential credentials=new PasswordCredential(username, password.toCharArray());
                         lsp.authenticate(credentials);
                         authenticated = true;
                    }else{
                        log.error("LdapSecurityProvider is not in valid state.");
                    }
                     
                } catch (SecurityProviderException e) {
                    log.error("Error while execution: " + e);
                }
                
                if (authenticated) {
                    log.debug(username + " Authenticated");
                    return true;
                } else {
                    log.debug("Invalid User Name or Password");
                    throw new AuthenticatorException(INVALID_PASSWORD);
                }

            } else {
                throw new AuthenticatorException(INVALID_USERNAME);
            }
     }
    
 
}
