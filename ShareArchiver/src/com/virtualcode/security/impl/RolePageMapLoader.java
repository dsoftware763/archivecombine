package com.virtualcode.security.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;

import com.virtualcode.common.VCSConstants;
import com.virtualcode.dao.DAOManager;
import com.virtualcode.services.PageRightsService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.services.SystemCodeService;
import com.virtualcode.util.SpringApplicationContext;
import com.virtualcode.vo.PageRights;

public class RolePageMapLoader {
	private static RolePageMapLoader rolePageMapLoader;
	
	private Map<String, String> adminRolePageMap; 
	private Map<String, String> privRolePageMap; 
	private Map<String, String> emailRolePageMap;
	private RolePageMapLoader() {
		// TODO Auto-generated constructor stub
		adminRolePageMap = new HashMap<String, String>();
		privRolePageMap = new HashMap<String, String>();
		emailRolePageMap = new HashMap<String, String>();
		com.virtualcode.services.ServiceManager serviceManager = (ServiceManager) SpringApplicationContext.getBean("serviceManager");
		PageRightsService pageRightsService = serviceManager.getPageRightsService();
		List<PageRights> rightsList = pageRightsService.getAll();
		for(PageRights pageRights:rightsList){
			
			if("ADMINISTRATOR".equals(pageRights.getRole().getName())){
				adminRolePageMap.put(pageRights.getPageUri(), pageRights.getRole().getName());
			}else if("PRIVILEGED".equals(pageRights.getRole().getName())){
				privRolePageMap.put(pageRights.getPageUri(), pageRights.getRole().getName());
			}else{
				emailRolePageMap.put(pageRights.getPageUri(), pageRights.getRole().getName());
			}
		}
	}
		

	public Map<String, String> getAdminRolePageMap() {
		return adminRolePageMap;
	}

	public void setAdminRolePageMap(Map<String, String> adminRolePageMap) {
		this.adminRolePageMap = adminRolePageMap;
	}
		

	public Map<String, String> getPrivRolePageMap() {
		return privRolePageMap;
	}


	public void setPrivRolePageMap(Map<String, String> privRolePageMap) {
		this.privRolePageMap = privRolePageMap;
	}


	public static RolePageMapLoader getInstance(){
		if(rolePageMapLoader == null)
			rolePageMapLoader = new RolePageMapLoader();		
			
		return rolePageMapLoader;
	}	
	
	public boolean isAllowed(String roleName,String pageName){
		if(roleName.equals("ADMINISTRATOR")){
			return adminRolePageMap.containsKey(pageName);
		}else if(roleName.equals("PRIVILEGED")){
			return privRolePageMap.containsKey(pageName);
		}else if(roleName.equals(VCSConstants.ROLE_EMAIL)){
			return emailRolePageMap.containsKey(pageName);
		}
		
		return false;
	}
	
	public String getRole1(String pageName){
		if(adminRolePageMap.containsKey(pageName)){
			return adminRolePageMap.get(pageName);
		}else if(privRolePageMap.containsKey(pageName)){
			return privRolePageMap.get(pageName);
		}
		return null;
	}


	public Map<String, String> getEmailRolePageMap() {
		return emailRolePageMap;
	}


	public void setEmailRolePageMap(Map<String, String> emailRolePageMap) {
		this.emailRolePageMap = emailRolePageMap;
	}

}
