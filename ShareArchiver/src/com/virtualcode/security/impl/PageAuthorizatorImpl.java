package com.virtualcode.security.impl;

import com.virtualcode.common.VCSConstants;
import com.virtualcode.security.PageAuthorizator;

public class PageAuthorizatorImpl implements PageAuthorizator {

//	Virtu
	@Override
	public boolean isAuthorizeRole(String view, String role) {
		// TODO Auto-generated method stub
		RolePageMapLoader mapLoader = RolePageMapLoader.getInstance();
		
		//handle JSF components like menus, images etc.
		if(view.contains("rfRes/") ){
			return true;
		}
		if(view.contains("ViewFile.faces")){
			return true;
		}
		return mapLoader.isAllowed(role, view);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		PageAuthorizator authorizator = new PageAuthorizatorImpl();
		System.out.println(authorizator.isAuthorizeRole("AddUser.faces", VCSConstants.ROLE_ADMIN));
		System.out.println(authorizator.isAuthorizeRole("AddUser.faces", VCSConstants.ROLE_PRIVILEGED));
	}

}
