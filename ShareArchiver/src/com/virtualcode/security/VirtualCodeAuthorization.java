package com.virtualcode.security;

import java.util.List;

/**
 *
 * @author Akhnukh
 */
public interface VirtualCodeAuthorization {

    public boolean authorize(String username, List sidsLists) throws Exception;

}
