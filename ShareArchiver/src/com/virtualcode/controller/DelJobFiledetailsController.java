package com.virtualcode.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.virtualcode.vo.DelJob;
import com.virtualcode.vo.DelJobFiledetails;

@ManagedBean(name="delJobFiledetailsController")
@ViewScoped
public class DelJobFiledetailsController extends AbstractController {

	public DelJobFiledetailsController(){
		super();
		delJobFiledetails=new DelJobFiledetails();
	}
	
	private DelJobFiledetails delJobFiledetails;
	
	public DelJobFiledetails getDelJobFiledetails() {
		return delJobFiledetails;
	}

	public void setDelJobFiledetails(DelJobFiledetails delJobFiledetails) {
		this.delJobFiledetails = delJobFiledetails;
	}
}
