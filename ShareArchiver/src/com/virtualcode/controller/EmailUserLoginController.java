package com.virtualcode.controller;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import javax.jcr.SimpleCredentials;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import com.virtualcode.common.AdvancedSecureURL;
import com.virtualcode.common.VCSConstants;
import com.virtualcode.security.VirtualCodeAuthenticator;
import com.virtualcode.security.impl.ActiveDirAuthenticator;
import com.virtualcode.util.PasswordGenerator;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.LinkAccessDocuments;
import com.virtualcode.vo.LinkAccessUsers;
import com.virtualcode.vo.Role;
import com.virtualcode.vo.SystemCodeType;
import com.virtualcode.vo.User;
import org.apache.shiro.crypto.hash.Sha256Hash;

@ManagedBean(name="emailUserLoginController")
@ViewScoped
public class EmailUserLoginController extends AbstractController {
	
	public EmailUserLoginController(){
		   super();		   
	}
		
	private String username;
	
	private String password;
	
	private String oldPassword;
	
	private String confirmPassword;
	
	boolean refresh = true;
	
	private String email;
	
	AdvancedSecureURL advancedSecureURL = AdvancedSecureURL.getInstance();
	
	public void preRenderView(){
//		System.out.println("calling pre prender view");
		
	}
	public void validateSession(){
		 HttpSession httpSession=getHttpSession();
	        HttpServletResponse httpResponse=getHttpResponse();	
			Object sessionUser=httpSession.getAttribute(VCSConstants.SESSION_USER_NAME);
			if(sessionUser!=null && sessionUser.toString()!=null && ! sessionUser.toString().trim().equals("")){
				//refresh = true;
			}
		
		if(FacesContext.getCurrentInstance().isPostback()){
	        //HttpSession httpSession=getHttpSession();
	        //HttpServletResponse httpResponse=getHttpResponse();	
			//Object sessionUser=httpSession.getAttribute(VCSConstants.SESSION_USER_NAME);
			
			if(sessionUser!=null && sessionUser.toString()!=null && ! sessionUser.toString().trim().equals("")){
				
				try {
					refresh = false;
					httpResponse.sendRedirect("Browse.faces");
				} catch (IOException e) {
					System.out.println("Redirection to Repository page failed."+e.getMessage());
				}
			}			
		}
		
	}
	
	public boolean isRefresh() {
		return refresh;
	}

	public void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}

	public String login(){
	   	
		
		if(username==null || username.trim().equals("") || password==null || password.trim().equals("")){
			   addErrorMessage("Please enter username and password", "Please enter username and password");
			   return null;
	   }		
		//to make username and password case in-sensitive
		username = username.toLowerCase();
	   boolean authenticate = authenticate(username, password);   
	   
	   if(!authenticate){
		   addErrorMessage("Username/Password incorrect", "Username/Password incorrect");
		   return null;
	   }	  
	   System.out.println("Session Create Time "+getHttpSession().getCreationTime());
	   System.out.println("Session Create Time in date format "+new Date(getHttpSession().getCreationTime()));
	   userActivityService.userLogin(username, getHttpRequest().getRemoteAddr(),getHttpSession().getId(),getHttpSession().getCreationTime());
	   
	   String roleName=(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_ROLE_NAME);
//	   
//	   if(roleName.equalsIgnoreCase("priv") || roleName.equalsIgnoreCase(VCSConstants.ROLE_PRIVILEGED)){
//		   return "Browse.faces?faces-redirect=true";
//	   }
	   return "EmailDocumentAccess.faces?faces-redirect=true";
	      
	}
	
	public void resetPassword(){
		if(email==null || email.length()<1){
			addErrorMessage("Please enter a valid email address", "Please enter a valid email address");
			 return;
		}
		if(!VCSUtil.validateEmailAddress(email)){
			addErrorMessage("Invalid email address.", "Invalid email address.");
			return;
		}
		AdvancedSecureURL advSecureUrl = AdvancedSecureURL.getInstance();
		 LinkAccessUsers user = linkAccessUsersService.getLinkAccessUsersByRecipient(email);
		 String password = "";
		 if(user==null){
			 addErrorMessage("No documents have been shared with email address "+ email, "No documents have been shared with email address "+ email);
			 return;
//			 user = new LinkAccessUsers();
//			 user.setRecipient(username);
//			 password = PasswordGenerator.randomAlphaNumeric();
//			 
//			 try {
////				advSecureUrl.getEncryptedURL(password);
//				user.setPassword(advSecureUrl.getEncryptedURL(password));
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			 
//			 user.setSender(username);
			 				 
		 }else{
			 try {
				 password = PasswordGenerator.randomAlphaNumeric();
				 user.setPassword(advSecureUrl.getEncryptedURL(password));
				 
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 linkAccessUsersService.saveLinkAccessUsers(user);	
		 }
		 ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		 String emailBody = "Thank you for going through the registration process, your account password is set to \""+ password +"\" use your email address and this password to gain access to shared files.<br/><br/>";
		 if(ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY)!=null)
		 	emailBody += ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY) + " ";
		 emailBody += "File Share Service";
		 mailService.sendMail(this.email, "ShareArchiver User Credentials", emailBody,null, null);
		 addInfoMessage("A generated password has been sent to your email address, please use it to access your documents", "A generated password has been sent to your email address, please use it to access your documents");
	}
	
	
	public String logout(){
	    HttpSession httpSession=getHttpSession();
	    if(httpSession!=null){
	    	httpSession.invalidate();
	    }
		return "login.faces?faces-redirect=true";
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}	
	
	public boolean authenticate(String username, String password){
//		List<com.virtualcode.vo.SystemCode> systemCodeList = systemCodeService.getSystemCodeByCodename("authentication.mode");
		boolean authenticate = false;
		authenticate = authenticateDB(username, password);
		
//		String mode = "1";	// set default to DB only
//		if(systemCodeList!=null && systemCodeList.size()>0){			
//			mode = systemCodeList.get(0).getCodevalue();
//		}
//		//to make username and password case in-sensitive
//		System.out.println("authentication mode: " + mode);
//		if(mode.equals(VCSConstants.AUTH_DB_ONLY)){	//DB only
//			System.out.println("Authentication mode: DB only");
//			authenticate = authenticateDB(username, password);
//		}else if(mode.equals(VCSConstants.AUTH_DB_FIRST_WITH_AD)){		//Both DB and AD -  DB first
//			System.out.println("Authentication mode: DB and AD");
//			authenticate = authenticateDB(username, password);
//			if(!authenticate){
//				System.out.println("DB authentication failed, going to AD");
//				authenticate = authenticateAD(username, password);
//			}
//		}else if(mode.equals(VCSConstants.AUTH_AD_FIRST_WITH_DB)){		//Both DB and AD - AD first
//			System.out.println("Authentication mode: DB and AD");
//			authenticate = authenticateAD(username, password);
//			if(!authenticate){
//				System.out.println("AD authentication failed, going to DB");
//				authenticate = authenticateDB(username, password);
//			}
//		}
		
		return authenticate;
		
		
	}
	
	public boolean authenticateDB(String username, String password){
		try {
			password=AdvancedSecureURL.getInstance().getEncryptedURL(password);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(linkAccessUsersService.login(username, password)){
			HttpSession httpSession=getHttpSession();
			httpSession.setAttribute(VCSConstants.SESSION_USER_ROLE_NAME,VCSConstants.ROLE_EMAIL);
			httpSession.setAttribute(VCSConstants.SESSION_USER_NAME, username);
			httpSession.setAttribute(VCSConstants.SESSION_USER_AUTH_TYPE, "db");
			return true;			
		}
		
		
//		User user= userService.login(username, password);
//		if(user==null){
//			addErrorMessage("username/password mismatch", "username/password mismatch");
//			return false;
//		}else if(!user.getUsername().equals(username) || ! user.getPasswordHash().equals(password)){
//			addErrorMessage("username/password mismatch", "username/password mismatch");
//			return false;
//		}else if(user.getLockStatus().equalsIgnoreCase("INACTIVE")){
//			   addErrorMessage("User is inactive", "User is inactive");
//			   return false;
//		}else {
//			HttpSession httpSession=getHttpSession();
//			   
//			httpSession.setAttribute(VCSConstants.SESSION_USER_NAME, user.getUsername());
//			Iterator<Role> ite=user.getUserRoles().iterator();
//			String roleName=null;
//			while(ite.hasNext()){
//			  roleName=ite.next().getName();
//			   break;
//			}
//			httpSession.setAttribute(VCSConstants.SESSION_USER_ROLE_NAME,roleName);
//			httpSession.setAttribute(VCSConstants.SESSION_USER_AUTH_TYPE, "db");
//			return true;
//		}
		return false;
	}
	
	public boolean authenticateAD(String username, String password){
		boolean authenticate = false;
		  
		try {
			SimpleCredentials credentials = new SimpleCredentials(username, password.toCharArray());
			VirtualCodeAuthenticator authenticator = ActiveDirAuthenticator.getInstance();
	        authenticate = authenticator.authenticate(credentials);
	        
	        //log.debug("authenticate: " + authenticate);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(authenticate){
			HttpSession httpSession=getHttpSession();
			httpSession.setAttribute(VCSConstants.SESSION_USER_NAME,username);
			httpSession.setAttribute(VCSConstants.SESSION_USER_ROLE_NAME,VCSConstants.ROLE_PRIVILEGED);
			httpSession.setAttribute(VCSConstants.SESSION_USER_AUTH_TYPE, "ad");
		}
		return authenticate;
	}
	
	public String savePassword(){		
		String username = (String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
//		System.out.println("----------change password for user: " + username);
		int errorCount = 0;
		String pattern= "^[a-zA-Z0-9]*$";
      
		if(oldPassword.trim().equals("")){
			addErrorMessage("old password cannot be empty", "old password cannot be empty");
			errorCount++;
		}
		if(password.trim().equals("")){
			addErrorMessage("Password cannot be empty", "Password cannot be empty");
			errorCount++;
		}
		if(confirmPassword.trim().equals("")){
			addErrorMessage("Confirm password cannot be empty", "Confirm password cannot be empty");
			errorCount++;
		}
		
		if(!password.matches(pattern)){
	       	addErrorMessage("Password must contain both alphabets and digits", "Password must contain both alphabets and digits");
			errorCount++;
	    }
			
		if(password.trim().length()<8){
			addErrorMessage("Password must be at least 8 characters", "Password must be at least 8 characters");
			errorCount++;
		}
		if(errorCount>0){
			return null;
		}else if(!password.equals(confirmPassword)){
			addErrorMessage("Password and confirm password do not match", "Password and confirm password do not match");
			return null;
		}else{
			LinkAccessUsers user =  linkAccessUsersService.getLinkAccessUsersByRecipient(username);
			
			List<User> userList = userService.getByUsername(username);
			
			if(user!=null){				
				try {
					String oldPassword = advancedSecureURL.getEncryptedURL(this.oldPassword);

					if(!oldPassword.equals(user.getPassword())){
						addErrorMessage("old password is incorrect", "old password is incorrect");
						return null;
					}
					user.setPassword(advancedSecureURL.getEncryptedURL(password));
					linkAccessUsersService.saveLinkAccessUsers(user);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
//			if(userList !=null || userList.size()>0){
//				String oldPasswordHash = new Sha256Hash(getOldPassword()).toHex();
//				User currentUser = userList.get(0);
//				if(!oldPasswordHash.equals(currentUser.getPasswordHash())){
//					addErrorMessage("old password is incorrect", "old password is incorrect");
//					return null;
//				}
//				currentUser.setPasswordHash(new Sha256Hash(user.getPasswordHash()).toHex());
//				userService.save(currentUser);				
//			}
			
		}
		return "EmailDocumentAccess.faces?faces-redirect=true";
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
//    public void navigate(PhaseEvent event) {
//        FacesContext facesContext = FacesContext.getCurrentInstance();
//        String outcome = action; // Do your thing?
//        facesContext.getApplication().getNavigationHandler().handleNavigation(facesContext, null, outcome);
//    }
		
}
