package com.virtualcode.controller;

import java.io.IOException;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.virtualcode.common.VCSConstants;
import com.virtualcode.repository.monitor.ArchiveDateRunnerMonitor;
import com.virtualcode.repository.monitor.FileSizeRunnerMonitor;
import com.virtualcode.schedular.FileSizeRunner;
import com.virtualcode.vo.SystemAlerts;
 
@ManagedBean(name="manageSystemController")
@ViewScoped
public class ManageSystemController extends AbstractController implements Serializable {
			
	public ManageSystemController(){		
		super();		
		systemAlert =  new SystemAlerts();
		FileSizeRunnerMonitor fileSizeRunnerMonitor = FileSizeRunnerMonitor.getInstance();
		totalFilesProcessed = fileSizeRunnerMonitor.getCurrentFileCount();
		batchJobRunning = fileSizeRunnerMonitor.isRunning();
		
		ArchiveDateRunnerMonitor archiveDateRunnerMonitor = ArchiveDateRunnerMonitor.getInstance();
		totalFilesProcessedForArchiveDate = archiveDateRunnerMonitor.getCurrentFileCount();
		batchJobRunningForArchiveDate = archiveDateRunnerMonitor.isRunning();
		
	}
	
	public void restartServer(){
		saveMessage("restartServer");
		System.out.println("going to restart server");
		
		try {
			getHttpResponse().sendRedirect("/ShareArchiver/restart.jsp");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (Exception ex) {
            System.out.println("Error while restart: " + ex);
            ex.printStackTrace();
        }
	}
	
	public void restartAppliance(){
		saveMessage("restartAppliance");
		System.out.println("going to restart");
		try {
			 Runtime rt = Runtime.getRuntime();
			 rt.exec("/sbin/shutdown -r now"); //only restart machine
//			 rt.exec("/sbin/reboot");
//			getHttpResponse().sendRedirect("/ShareArchiver/restart.jsp");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (Exception ex) {
            System.out.println("Error while restart: " + ex);
            ex.printStackTrace();
        }
	}
	
	public void shutdownServer(){
		saveMessage("shutdownServer");
		System.out.println("going to shutdown server");
		try {
			 Runtime rt = Runtime.getRuntime();
//			 rt.exec("/etc/init.d/sharearchiver stop");
			 rt.exec("/sbin/shutdown -h now"); //only restart machine
//			getHttpResponse().sendRedirect("/ShareArchiver/restart.jsp");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (Exception ex) {
            System.out.println("Error while shutdown: " + ex);
            ex.printStackTrace();
        }
	}
	
	public void saveMessage(String action){
		String username = (String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
		systemAlert.setAlertType(action);
		systemAlert.setUsername(username);
		systemAlertsService.saveSystemAlert(systemAlert);
		
	}
	
	public void startFileSizeBatch(){		
		fileSizeRunnerService.startFileSizeRunner();
		
		batchJobRunning = true;
	}
	
	public void stopFileSizeBatch(){
		fileSizeRunnerService.stopFileSizeRunner();
		batchJobRunning = false;
	}
	
	public void startFileSizeBatchForArchiveDate(){		
		archiveDateRunnerService.startFileSizeRunner();
		
		batchJobRunningForArchiveDate = true;
	}
	
	public void stopFileSizeBatchForArchiveDate(){
		archiveDateRunnerService.stopFileSizeRunner();
		batchJobRunningForArchiveDate = false;
	}
	
	private String message;
	
	private SystemAlerts systemAlert;
	
	private boolean batchJobRunning = false;
	
	private Long totalFilesProcessed = 0l;
	
	private boolean batchJobRunningForArchiveDate = false;
	
	private Long totalFilesProcessedForArchiveDate = 0l;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public SystemAlerts getSystemAlert() {
		return systemAlert;
	}

	public void setSystemAlert(SystemAlerts systemAlert) {
		this.systemAlert = systemAlert;
	}

	public boolean isBatchJobRunning() {
		return batchJobRunning;
	}

	public void setBatchJobRunning(boolean batchJobRunning) {
		this.batchJobRunning = batchJobRunning;
	}

	public Long getTotalFilesProcessed() {
		return totalFilesProcessed;
	}

	public void setTotalFilesProcessed(Long totalFilesProcessed) {
		this.totalFilesProcessed = totalFilesProcessed;
	}

	public boolean isBatchJobRunningForArchiveDate() {
		return batchJobRunningForArchiveDate;
	}

	public void setBatchJobRunningForArchiveDate(
			boolean batchJobRunningForArchiveDate) {
		this.batchJobRunningForArchiveDate = batchJobRunningForArchiveDate;
	}

	public Long getTotalFilesProcessedForArchiveDate() {
		return totalFilesProcessedForArchiveDate;
	}

	public void setTotalFilesProcessedForArchiveDate(
			Long totalFilesProcessedForArchiveDate) {
		this.totalFilesProcessedForArchiveDate = totalFilesProcessedForArchiveDate;
	}
    
   
	


}
