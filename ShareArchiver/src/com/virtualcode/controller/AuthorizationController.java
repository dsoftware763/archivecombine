package com.virtualcode.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.servlet.http.HttpSession;

import org.richfaces.event.ItemChangeEvent;

import com.virtualcode.common.VCSConstants;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.VirtualcodeUtil;
import com.virtualcode.vo.SystemCodeType;

@ManagedBean(name="authController")
@SessionScoped	//set to session scope as we need the bean to be available through out the application for menu state
public class AuthorizationController  extends AbstractController{

	public AuthorizationController() {
		super();
		try{
		   setAuthorization();
		}catch(Exception ex){}
		
	}
		
	
	private boolean VC_SYSTEM_MAIN=false;
	
	private boolean VC_SYSTEM_SYS_STATUS=false;
	private boolean VC_SYSTEM_JOB_STATUS=false;
	private boolean VC_SYSTEM_F_EXPLORER=false;
	
	private boolean VC_CONFIG_MAIN=false;
	private boolean VC_CONFIG_JOB=false;
	private boolean VC_CONFIG_POLICY=false;
	private boolean VC_CONFIG_AGENT=false;
	private boolean VC_CONFIG_DOC_CLASSIFIER=false;
		
	private boolean VC_ACC_MGMT=false;
	private boolean VC_DELJOB_MGMT=false;
	
	private boolean VC_ACC_CHANGE_PASSWORD=false;
	
	private boolean VC_ACC_SIGNOUT=true;
	
	private boolean VC_HELP=false;
	
	private String LOGGED_IN_USER_TYPE;
	
	private String LOGGED_IN_USER_NAME;
	
	private boolean VC_USER_FEATURES=false;
	
	

	

	private void setAuthorization(){
		HttpSession httpSession=getHttpSession();
		String roleName=(String)httpSession.getAttribute(VCSConstants.SESSION_USER_ROLE_NAME);
		//added to display user name in header 
		String username =(String)httpSession.getAttribute(VCSConstants.SESSION_USER_NAME);
		if(username !=null && username.length()>0){
			//first letter capital
			LOGGED_IN_USER_NAME = username.substring(0,1).toUpperCase() + username.substring(1);			
		}		
		String loggedInUserType =(String)httpSession.getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE);
		
		if(loggedInUserType!=null && loggedInUserType.equalsIgnoreCase("db")){
			VC_ACC_CHANGE_PASSWORD=true;
		}else{
			VC_ACC_CHANGE_PASSWORD=false;
		}
		if(roleName.equals(VCSConstants.ROLE_ADMIN)){
			LOGGED_IN_USER_TYPE=VCSConstants.ROLE_ADMIN;
			setAdminViews();
		}else if (roleName.equals(VCSConstants.ROLE_PRIVILEGED)){
			LOGGED_IN_USER_TYPE=VCSConstants.ROLE_PRIVILEGED;
			setPrivViews();
		}else{
			if (roleName.equals(VCSConstants.ROLE_EMAIL)){
				LOGGED_IN_USER_TYPE=VCSConstants.ROLE_EMAIL;
			}
			setCommonViews();
		}
		
	}
	
	
	
	private void setAdminViews(){
		
		VC_SYSTEM_MAIN=true;
		
		VC_SYSTEM_SYS_STATUS=true;
		VC_SYSTEM_JOB_STATUS=true;
		//changes for compliance mode
		VirtualcodeUtil util = VirtualcodeUtil.getInstance();
		if(util.getComplianceMode()){
			ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
			String enableFEAdminCM = ru.getCodeValue("enable_file_explorer_for_admin_cm", SystemCodeType.GENERAL);
			if(enableFEAdminCM!=null && enableFEAdminCM.trim().length()>0)
				VC_SYSTEM_F_EXPLORER=enableFEAdminCM.equalsIgnoreCase("yes")?true:false;
			VC_USER_FEATURES=false;
		}else{
			VC_SYSTEM_F_EXPLORER=true;
			VC_USER_FEATURES=true;
		}
		
		VC_CONFIG_MAIN=true;
	    VC_CONFIG_JOB=true;
		VC_CONFIG_POLICY=true;
		VC_CONFIG_AGENT=true;
		VC_CONFIG_DOC_CLASSIFIER=true;		
		
		VC_ACC_MGMT=true;		
		VC_DELJOB_MGMT=true;
		
		VC_HELP=false;
		
	}
	
	private void setPrivViews(){
		
        VC_SYSTEM_MAIN=false;
		
		VC_SYSTEM_SYS_STATUS=false;
		VC_SYSTEM_JOB_STATUS=false;
		VC_SYSTEM_F_EXPLORER=false;
		
		VC_CONFIG_MAIN=false;
	    VC_CONFIG_JOB=false;
		VC_CONFIG_POLICY=false;
		VC_CONFIG_AGENT=false;
		VC_CONFIG_DOC_CLASSIFIER=false;		
		
		VC_ACC_MGMT=false;	
		VC_DELJOB_MGMT=false;
		VC_HELP=true;
			
	}
	
	private void setCommonViews(){
		
        VC_SYSTEM_MAIN=false;
		
		VC_SYSTEM_SYS_STATUS=false;
		VC_SYSTEM_JOB_STATUS=false;
		VC_SYSTEM_F_EXPLORER=false;
		
		VC_CONFIG_MAIN=false;
	    VC_CONFIG_JOB=false;
		VC_CONFIG_POLICY=false;
		VC_CONFIG_AGENT=false;
		VC_CONFIG_DOC_CLASSIFIER=false;		
		
		VC_ACC_MGMT=false;
		VC_DELJOB_MGMT=false;
		VC_ACC_SIGNOUT=true;
	}
	
	public void updateCurrent(ItemChangeEvent event){
		//System.out.println("item changed:---------------"+event.getComponent().getId());
		//System.out.println("item changed:---------------"+event.getNewItemName());
		/*String selection = event.getNewItemName();
		String current = "";
		if(selection.equals("systemStatus") || selection.equals("jobStatus") || selection.equals("fExplorer") || selection.equals("fExplorer1")){
			current = "system";
		}else if(selection.equals("jobMgmt") || selection.equals("policyMgmt") 
				|| selection.equals("documentClassifier") || selection.equals("agentReg")){
			current = "configs";
		}else if(selection.equals("userMgmt")){
			current = "accountMgmt";
		}else if(selection.equals("shareMappings") || selection.equals("globalConf")){
			current="systemConfigs";
		}
		setCurrent(current);*/
	}
	
	public String handleMenuClick(){
		String selection = getRequestParameter("id");
		String current = "";
		String destination = "";
		
		if(selection.equals("Status.faces") || selection.equals("JobStatus.faces") || selection.equals("FileExplorer.faces?faces-redirect=true")){
			current = "system";			
		}else if(selection.equals("Jobs.faces?faces-redirect=true") || selection.equals("Policies.faces") 
				|| selection.equals("DocumentCategories.faces") || selection.equals("Agents.faces") || selection.equals("IndexingJobs.faces?faces-redirect=true")){
			current = "configs";
		}else if(selection.equals("Users.faces") || selection.equals("AccountPolicy.faces?faces-redirect=true")){
			current = "accountMgmt";
		}else if(selection.equals("DelJob.faces")){
			current = "repoMgmt";
		}else if(selection.equals("GlobalCodes.faces") || selection.equals("LDAPCodes.faces") || selection.equals("DriveLetters.faces?faces-redirect=true") || selection.equals("UserFeatures.faces?faces-redirect=true")
				|| selection.equals("ManageSystem.faces") || selection.equals("EditLDAPConfig.faces?faces-redirect=true") || selection.equals("EditGlobalConfig.faces?faces-redirect=true")
				|| selection.equals("EditCompanyConfig.faces?faces-redirect=true") || selection.equals("ToolsDownloader.faces")){
			current="systemConfigs";
		}
		setCurrent(current);
		return selection;
	}
	
	public boolean isVC_SYSTEM_MAIN() {
		return VC_SYSTEM_MAIN;
	}

	public void setVC_SYSTEM_MAIN(boolean vC_SYSTEM_MAIN) {
		VC_SYSTEM_MAIN = vC_SYSTEM_MAIN;
	}

	public boolean isVC_CONFIG_MAIN() {
		return VC_CONFIG_MAIN;
	}

	public void setVC_CONFIG_MAIN(boolean vC_CONFIG_MAIN) {
		VC_CONFIG_MAIN = vC_CONFIG_MAIN;
	}

	public boolean isVC_SYSTEM_SYS_STATUS() {
		return VC_SYSTEM_SYS_STATUS;
	}


	public void setVC_SYSTEM_SYS_STATUS(boolean vC_SYSTEM_SYS_STATUS) {
		VC_SYSTEM_SYS_STATUS = vC_SYSTEM_SYS_STATUS;
	}


	public boolean isVC_SYSTEM_JOB_STATUS() {
		return VC_SYSTEM_JOB_STATUS;
	}


	public void setVC_SYSTEM_JOB_STATUS(boolean vC_SYSTEM_JOB_STATUS) {
		VC_SYSTEM_JOB_STATUS = vC_SYSTEM_JOB_STATUS;
	}


	public boolean isVC_SYSTEM_F_EXPLORER() {
		return VC_SYSTEM_F_EXPLORER;
	}


	public void setVC_SYSTEM_F_EXPLORER(boolean vC_SYSTEM_F_EXPLORER) {
		VC_SYSTEM_F_EXPLORER = vC_SYSTEM_F_EXPLORER;
	}


	public boolean isVC_CONFIG_JOB() {
		return VC_CONFIG_JOB;
	}


	public void setVC_CONFIG_JOB(boolean vC_CONFIG_JOB) {
		VC_CONFIG_JOB = vC_CONFIG_JOB;
	}


	public boolean isVC_CONFIG_POLICY() {
		return VC_CONFIG_POLICY;
	}


	public void setVC_CONFIG_POLICY(boolean vC_CONFIG_POLICY) {
		VC_CONFIG_POLICY = vC_CONFIG_POLICY;
	}


	public boolean isVC_CONFIG_AGENT() {
		return VC_CONFIG_AGENT;
	}


	public void setVC_CONFIG_AGENT(boolean vC_CONFIG_AGENT) {
		VC_CONFIG_AGENT = vC_CONFIG_AGENT;
	}


	public boolean isVC_CONFIG_DOC_CLASSIFIER() {
		return VC_CONFIG_DOC_CLASSIFIER;
	}


	public void setVC_CONFIG_DOC_CLASSIFIER(boolean vC_CONFIG_DOC_CLASSIFIER) {
		VC_CONFIG_DOC_CLASSIFIER = vC_CONFIG_DOC_CLASSIFIER;
	}


	public boolean isVC_ACC_MGMT() {
		return VC_ACC_MGMT;
	}


	public void setVC_ACC_MGMT(boolean vC_ACC_MGMT) {
		VC_ACC_MGMT = vC_ACC_MGMT;
	}

	
	public boolean isVC_DELJOB_MGMT() {
		return VC_DELJOB_MGMT;
	}

	public void setVC_DELJOB_MGMT(boolean vC_DELJOB_MGMT) {
		VC_DELJOB_MGMT = vC_DELJOB_MGMT;
	}

	public String getLOGGED_IN_USER_TYPE() {
		return LOGGED_IN_USER_TYPE;
	}

	public void setLOGGED_IN_USER_TYPE(String lOGGED_IN_USER_TYPE) {
		LOGGED_IN_USER_TYPE = lOGGED_IN_USER_TYPE;
	}
			
	public boolean isVC_ACC_CHANGE_PASSWORD() {
		return VC_ACC_CHANGE_PASSWORD;
	}

	public void setVC_ACC_CHANGE_PASSWORD(boolean vC_ACC_CHANGE_PASSWORD) {
		VC_ACC_CHANGE_PASSWORD = vC_ACC_CHANGE_PASSWORD;
	}

	public boolean isVC_ACC_SIGNOUT() {
		return VC_ACC_SIGNOUT;
	}

	public void setVC_ACC_SIGNOUT(boolean vC_ACC_SIGNOUT) {
		VC_ACC_SIGNOUT = vC_ACC_SIGNOUT;
	}

	public boolean isVC_HELP() {
		return VC_HELP;
	}

	public void setVC_HELP(boolean vC_HELP) {
		VC_HELP = vC_HELP;
	}
	

	private String current = "system";
	
    private boolean singleMode;



	public String getCurrent() {
		//System.out.println("current  = " + current);
		return current;
	}

	public void setCurrent(String current) {
		this.current = current;
	}

	public boolean isSingleMode() {
		return singleMode;
	}

	public void setSingleMode(boolean singleMode) {
		this.singleMode = singleMode;
	}

	public String getLOGGED_IN_USER_NAME() {
		
		try{
			HttpSession httpSession=getHttpSession();
			String sessionUserName=(String)httpSession.getAttribute(VCSConstants.SESSION_USER_NAME);
			sessionUserName=sessionUserName.substring(0,1).toUpperCase() + sessionUserName.substring(1);
			if(!LOGGED_IN_USER_NAME.equals(sessionUserName)){
				setAuthorization();
			}
		}catch(Exception ex){
			
		}
		
		return LOGGED_IN_USER_NAME;
		
		//extra code to re-initilaize values.
		
		
		
	}

	public void setLOGGED_IN_USER_NAME(String lOGGED_IN_USER_NAME) {
		LOGGED_IN_USER_NAME = lOGGED_IN_USER_NAME; 
	}



	public boolean isVC_USER_FEATURES() {
		return VC_USER_FEATURES;
	}



	public void setVC_USER_FEATURES(boolean vC_USER_FEATURES) {
		VC_USER_FEATURES = vC_USER_FEATURES;
	}
    
		
	
	
}
