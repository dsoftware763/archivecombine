package com.virtualcode.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;

import com.virtualcode.agent.das.dataIndexer.IndexerManager;
import com.virtualcode.common.VCSConstants;
import com.virtualcode.security.impl.VirtualCodeAuthorizatorImpl;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.DriveLetters;
import com.virtualcode.vo.SearchDocument;
import com.virtualcode.vo.SearchReport;
import com.virtualcode.vo.SearchResult;

@ManagedBean(name="analysisController")
@ViewScoped
public class AnalysisController extends AbstractController{
	ResourcesUtil   ru  =   ResourcesUtil.getResourcesUtil();
	public AnalysisController(){
		super();
//		ResourcesUtil   ru  =   ResourcesUtil.getResourcesUtil();
		String authType = (String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE); 
		
		//Build the DriveLetters List
		List<DriveLetters> tempList = driveLettersService.getAll();
		driveLettersList	=	jobService.getSpDocLibByDriveLetters(tempList);
	}
	
	private List<DriveLetters> driveLettersList;
		
	private String spDocLibID;
	
	private String uncPath;
	
	private String documentName;
	
	private String documentExtension;
	
	private String minSize;
	
	private String maxSize;
	
	private Date modifiedStartDate;
	
	private Long modifiedStartDateLong;
	
	private Date modifiedEndDate;
	
	private Long modifiedEndDateLong;

	private Date createdStartDate;
	
	private Long createdStartDateLong;
	
	private Date createdEndDate;
	
	private Long createdEndDateLong;
	
	private String dateType	=	"modified";
	
	private long executionTime=0;
	
	private int currentPage=1;
	
	private List<String> curPagesList	=	new ArrayList<String>();
	
	private int itemsPerPage=20;
	
	private Integer maxSearchResults=-1;
	
	private int maxPageCtr=5;
	
	private int totalPages=1;
	
	private Integer totalRecords=-1;
	
	private String sortOption=IndexerManager.Constants.FILE_PATH;

	private String sortMode="Asc";
	
	private List<SearchDocument> searchResults=new ArrayList<SearchDocument>();
	
	private SearchDocument selectedDocument = new SearchDocument();
	
	private String totalFileCount;
	
	private String totalFileSize;
	
	private boolean selectAll = false;
	
	private String searchQuery;
		
	public void search(){
	
		try{
			this.searchResults.clear();
			totalRecords=0;
			this.curPagesList.clear();
			if(getRequestParameter("currentPage")!=null && !getRequestParameter("currentPage").isEmpty())
				currentPage	=	Integer.parseInt(getRequestParameter("currentPage"));
			else
				currentPage	=	1;
				
			/*
			if(getRequestParameter("sortMode")!=null && !getRequestParameter("sortMode").isEmpty())
				sortMode	=	getRequestParameter("sortMode");
			else
				sortMode	=	"Asc";
			*/
			if(getRequestParameter("sortOption")!=null && !getRequestParameter("sortOption").isEmpty()) {
				sortOption	=	getRequestParameter("sortOption");
				
				//if sortMode is not specified in the request, than Reverse the SortMode...
				if(getRequestParameter("sortMode")==null || getRequestParameter("sortMode").isEmpty()) {
					if(sortMode.equalsIgnoreCase("Asc"))
						sortMode	=	"Desc";
					else
						sortMode	=	"Asc";
				} else {
					sortMode	=	getRequestParameter("sortMode");
				}
					
			} else {//default sorting params
				sortOption	=	IndexerManager.Constants.FILE_PATH;
				sortMode	=	"Asc";
			}
			System.out.println("sortOption: "+sortOption +" mode:"+sortMode);
			
			if(getRequestParameter("maxSearchResults")!=null && !getRequestParameter("maxSearchResults").isEmpty())
				maxSearchResults	=	new Integer(getRequestParameter("maxSearchResults"));
			else
				maxSearchResults	=	-1;
			
		   String startCreateDateStr=VCSUtil.getDateAsString(createdStartDate);
		   String endCreateDateStr=VCSUtil.getDateAsString(createdEndDate);
		   String startModifDateStr=VCSUtil.getDateAsString(modifiedStartDate);		   
		   String endModifDateStr=VCSUtil.getDateAsString(modifiedEndDate);
		   
		   
		   if(getRequestParameter("startDate")!=null && !getRequestParameter("startDate").isEmpty()) {
			   if("modified".equalsIgnoreCase(dateType)) {
				   startModifDateStr	=	getRequestParameter("startDate");
				   modifiedStartDate	=	VCSUtil.getDate(startModifDateStr, VCSConstants.SEACH_DATE_FORMAT);
			   } else if("created".equalsIgnoreCase(dateType)) {
				   startCreateDateStr	=	getRequestParameter("startDate");
				   createdStartDate	=	VCSUtil.getDate(startCreateDateStr, VCSConstants.SEACH_DATE_FORMAT);
			   }
		   }
		   if(getRequestParameter("endDate")!=null && !getRequestParameter("endDate").isEmpty()) {
			   if("modified".equalsIgnoreCase(dateType)) {
				   endModifDateStr	=	getRequestParameter("endDate");
				   modifiedEndDate	=	VCSUtil.getDate(endModifDateStr, VCSConstants.SEACH_DATE_FORMAT);
			   } else if("created".equalsIgnoreCase(dateType)) {
				   endCreateDateStr	=	getRequestParameter("endDate");
				   createdEndDate	=	VCSUtil.getDate(endCreateDateStr, VCSConstants.SEACH_DATE_FORMAT);
			   }
		   }
		   //System.out.println("date: "+modifiedStartDate);
		   
		   long start=new Date().getTime();
		   
		   if((spDocLibID==null || spDocLibID.trim().equals("")) ) {//spDocLibID is Necessory, to select the navigated path ID

		    	FacesMessage fmessage=new FacesMessage();
		    	fmessage.setSummary("Drive letter must be selected, to search in.");
		    	fmessage.setDetail("Drive letter must be selected, to search in.");
		    	
		    	fmessage.setSeverity(FacesMessage.SEVERITY_WARN);
		    	
		    	FacesContext.getCurrentInstance().addMessage("analysisForm:spDocLibID", fmessage);
			   return;
		   }
		   System.out.println("---session type:"+getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE));
		   String sidString = "";
		   if(getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE).equals("db")){
			   sidString = null;
		   }else{
			   System.out.println("--getting sids for user: " + getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME));
			   VirtualCodeAuthorizatorImpl authorizator = VirtualCodeAuthorizatorImpl.getInstance();
			   sidString = authorizator.sidString(getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME)+"");
		   }
		   
		   SearchResult result= analysisService.searchDocument(spDocLibID, null, minSize, maxSize, 
				   startCreateDateStr, endCreateDateStr, startModifDateStr, endModifDateStr, 
				   uncPath, documentName, documentExtension, sidString, sortMode, sortOption, 
				   null, null,null, currentPage, itemsPerPage, maxSearchResults);		   
		  		  
		   this.searchResults=result.getResults();
		   this.searchQuery = result.getSearchQuery();
		   
		   calculatePages(result);
		   
		   long end=new Date().getTime();
		   executionTime=(end-start)/1000;
		   totalFileSize=VCSUtil.getSizeToDisplay(result.getComulativeSizeFound()+"");
		   
		   //UserActivityService userActivityService=serviceManager.getUserActivityService();
		   //HttpSession httpSession=getHttpSession();
		   //String userName=httpSession.getAttribute(VCSConstants.SESSION_USER_NAME).toString();
		   //String userIP=null;
		   //if(httpSession.getAttribute(VCSConstants.SESSION_USER_NAME)!=null)
		   //  userIP=httpSession.getAttribute(VCSConstants.SESSION_USER_NAME).toString();
		   //userActivityService.recordSearchActivity(userName, userIP, httpSession.getId(), httpSession.getCreationTime(), result.getSearchQuery(), result.getTotalRecordsFound());
		
		}catch(Exception ex){
			ex.printStackTrace();			
		}
	}
	
	public void updateUNC(AjaxBehaviorEvent e){
		//System.out.println("updateUNC invoked "+spDocLibID);
		Integer spID	=	new Integer(spDocLibID);
		for(int i=0; i<driveLettersList.size(); i++) {
			DriveLetters	dL	=	driveLettersList.get(i);
			if(spID.compareTo(dL.getSpDocLibId())==0) {
				uncPath	=	dL.getSharePath();
				//System.out.println("matched "+uncPath);
				break;
			}
		}
	}
	
	public void calculatePages(SearchResult result){
		 
		if(searchResults==null){
			totalRecords=0;
			totalPages=0;
			currentPage=1;
			curPagesList.clear();
			return;
		}
		
		////totalRecords=searchResults.size();
		totalRecords=result.getTotalRecordsFound();
	    	    
	    if(totalRecords>0){
		   float t=((float)totalRecords)/itemsPerPage;
		   totalPages= (int)Math.ceil(t);
		   
		   System.out.println("totalPages: "+totalPages + " where devident: "+t);
		   for(int i=0; i<totalPages; i++) {
			   if(i>=(currentPage-maxPageCtr) && (i<currentPage+maxPageCtr-1)) {
				   curPagesList.add(""+(i+1));
			   }
		   }
	    }
	}
	
	private List<SearchDocument> sortByName(List<SearchDocument> list){
					
			Collections.sort(list, new Comparator() {

	            public int compare(Object o1, Object o2) {
	            	SearchDocument doc1=(SearchDocument)o1;
	            	SearchDocument doc2=(SearchDocument)o2;
	                
	            	if(sortMode.equalsIgnoreCase("desc")){
	                return doc2.getDocumentName().compareToIgnoreCase(doc1.getDocumentName());
	            	}else{
	            		return doc1.getDocumentName().compareToIgnoreCase(doc2.getDocumentName());
	            	}
	            }
	        });
			
		return list;
		
	}
	
	private List<SearchDocument> sortBySize(List<SearchDocument> list){
		
		Collections.sort(list, new Comparator() {

            public int compare(Object o1, Object o2) {
            	SearchDocument doc1=(SearchDocument)o1;
            	SearchDocument doc2=(SearchDocument)o2;
                Integer doc1Size=VCSUtil.getIntValue(doc1.getDocumentSize());
                Integer doc2Size=VCSUtil.getIntValue(doc2.getDocumentSize());
                System.out.println("*****doc1Siz ***"+doc1Size+" ********"+doc2Size);
                
                if(sortMode.equalsIgnoreCase("desc")){ 
                   return doc2Size.compareTo(doc1Size);
                }else{
                   return doc1Size.compareTo(doc2Size);
                }
            }
        });
		
	return list;	
   }
	
	
   private List<SearchDocument> sortByDate(List<SearchDocument> list){
		
		Collections.sort(list, new Comparator() {

            public int compare(Object o1, Object o2) {
            	SearchDocument doc1=(SearchDocument)o1;
            	SearchDocument doc2=(SearchDocument)o2;            	
                Date date1=VCSUtil.getDate(doc1.getLastModificationDate(),"MMMM dd, yyyy");
                Date date2=VCSUtil.getDate(doc2.getLastModificationDate(),"MMMM dd, yyyy");                 
                
                if(sortMode.equalsIgnoreCase("desc")){ 
                   return date2.compareTo(date1);
                }else{
                   return date1.compareTo(date2);
                }
            }
               
        });
		
	return list;
	
   }
   
   public SearchReport printSearchResults(){
	   SearchReport searchReport = SearchReport.getInstance();
//	   searchReport.setSearchString(searchString);
//	   searchReport.setUsername((String)getHttpRequest().getSession().getAttribute(VCSConstants.SESSION_USER_NAME));
//	   searchReport.setStartDate(modifiedStartDate.toString());
//	   searchReport.setEndDate(modifiedEndDate.toString());
//	   searchReport.setSearchResults(searchResults);
//	   try {
		//getHttpResponse().sendRedirect("PdfPrinter?faces-redirect=true;");
//	} catch (IOException e) {

//		e.printStackTrace();
//	}
	   return searchReport;
   }
	
   /*
   public String processSearchFolderPath(String searchPath){
		String orignalPath=searchPath;
		searchPath=searchPath.replaceAll("\\\\", "/");
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		String companyName = ru.getCodeValue("companyName", SystemCodeType.GENERAL);
		if(searchPath.startsWith("//")){
			searchPath=searchPath.substring(2,searchPath.length());
			searchPath="/"+companyName+"/"+"FS"+"/"+searchPath;
			if(fileExplorerService.getNodeByPath(searchPath)==null){
				searchPath="/"+companyName+"/"+"SP"+"/"+searchPath;
			}
		}
		if(searchPath.endsWith("/")){
			searchPath = searchPath.substring(0, searchPath.length()-1);
		}
		
		Pattern pathPattern=Pattern.compile("(^[a-zA-Z]:)", Pattern.UNICODE_CASE);
		
		Matcher matcher=pathPattern.matcher(searchPath);
		if(matcher.find()){
			System.out.print("***** Its a drive mapping path:*****");
	
   		String searchPathDriveLetter=searchPath.substring(0,2);
   		DriveLettersService driveLetterService=serviceManager.getDriveLettersService();
   		List<DriveLetters> driveLetterList=driveLetterService.getAll();
   	    String drivePath;
   	    DriveLetters driveLetter=null;
   	    for(int i=0;driveLetterList!=null && i<driveLetterList.size();i++){
   	    	driveLetter=driveLetterList.get(i);
   	    	if(searchPathDriveLetter.equalsIgnoreCase(driveLetter.getDriveLetter())){break;}
   	    }
   	    
   		if(driveLetter!=null){
   			System.out.println("***** Drive mapping found.....");
   	    	drivePath=driveLetter.getSharePath();    	    	
			if(drivePath.startsWith("\\\\")){	
				drivePath ="/"+ ru.getCodeValue("companyName", SystemCodeType.GENERAL) + "/FS/" + drivePath.substring(2, drivePath.lastIndexOf("\\")).replaceAll("\\\\", "/");
			}
   	    	searchPath=drivePath+"/"+searchPath.substring(2);
   	    	searchPath=searchPath.replaceAll("//", "/");
   	    	if(!searchPath.startsWith("/")){
   	    		searchPath = "/" + searchPath;   	    		
   	    	}
   	    }else{
//   	    	System.out.println("***** No drive mapping found.....");
//   	    	this.firstLevelFiles=null;
//   	    	return;
   	    }
   	}		
   	
   	System.out.println("Search Path formatted:-----> " + searchPath);
   	return searchPath;
   }*/
   
   public void switchDateType() {
	   String dT	=	new String(dateType);
	   String spID	=	new String(spDocLibID);
	   this.clear();
	   dateType		=	dT;
	   spDocLibID	=	spID;
   }
	public void clear(){
		
		spDocLibID=null;
		documentName=null;
		dateType="modified";
		minSize=null;
		maxSize=null;
		createdStartDate=null;
		createdEndDate=null;
		modifiedEndDate=null;
		modifiedStartDate=null;
		searchResults.clear();
		executionTime=0;
		totalRecords=0;
		totalPages=1;
		sortMode="Asc";
		documentExtension="ALL";

		currentPage=1;
		curPagesList.clear();
	}

	
	public String getSpDocLibID() {
		return spDocLibID;
	}

	public void setSpDocLibID(String spDocLibID) {
		this.spDocLibID = spDocLibID;
	}

	public String getUncPath() {
		return uncPath;
	}

	public void setUncPath(String uncPath) {
		this.uncPath = uncPath;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getDocumentExtension() {
		return documentExtension;
	}

	public void setDocumentExtension(String documentExtension) {
		this.documentExtension = documentExtension;
	}

	public String getDateType() {
		return dateType;
	}

	public void setDateType(String dateType) {
		this.dateType = dateType;
	}

	public String getMinSize() {
		return minSize;
	}

	public void setMinSize(String minSize) {
		this.minSize = minSize;
	}
	
	public String getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(String maxSize) {
		this.maxSize = maxSize;
	}

	public Date getModifiedStartDate() {
		return modifiedStartDate;
	}

	public void setModifiedStartDate(Date modifiedStartDate) {
		this.modifiedStartDate = modifiedStartDate;
	}

	public Date getModifiedEndDate() {
		return modifiedEndDate;
	}

	public void setModifiedEndDate(Date modifiedEndDate) {
		this.modifiedEndDate = modifiedEndDate;
	}

	public Date getCreatedStartDate() {
		return createdStartDate;
	}

	public void setCreatedStartDate(Date createdStartDate) {
		this.createdStartDate = createdStartDate;
	}

	public Date getCreatedEndDate() {
		return createdEndDate;
	}

	public void setCreatedEndDate(Date createdEndDate) {
		this.createdEndDate = createdEndDate;
	}

	public Integer getTotalRecords() {
		return totalRecords;
	}


	public void setTotalRecords(Integer totalRecords) {
		this.totalRecords = totalRecords;
	}


	public List<SearchDocument> getSearchResults() {
		return searchResults;
	}


	public void setSearchResults(List<SearchDocument> searchResults) {
		this.searchResults = searchResults;
	}

	public long getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(long executionTime) {
		this.executionTime = executionTime;
	}


	public float getItemsPerPage() {
		return itemsPerPage;
	}


	public void setItemsPerPage(float itemsPerPage) {
		this.itemsPerPage = (int)itemsPerPage;
	}


	public int getTotalPages() {
		return totalPages;
	}


	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public List<String> getCurPagesList() {
		return curPagesList;
	}

	public void setCurPagesList(List<String> curPagesList) {
		this.curPagesList = curPagesList;
	}

	public String getSortMode() {
		return sortMode;
	}

	public void setSortMode(String sortMode) {
		this.sortMode = sortMode;
	}

	public String getSortOption() {
		return sortOption;
	}

	public void setSortOption(String sortOption) {
		this.sortOption = sortOption;
	}

	public SearchDocument getSelectedDocument() {
		return selectedDocument;
	}

	public void setSelectedDocument(SearchDocument selectedDocument) {
		this.selectedDocument = selectedDocument;
	}

	public String getTotalFileCount() {
		return totalFileCount;
	}

	public void setTotalFileCount(String totalFileCount) {
		this.totalFileCount = totalFileCount;
	}

	public String getTotalFileSize() {
		return totalFileSize;
	}

	public void setTotalFileSize(String totalFileSize) {
		this.totalFileSize = totalFileSize;
	}

	public boolean isSelectAll() {
		return selectAll;
	}

	public void setSelectAll(boolean selectAll) {
		this.selectAll = selectAll;
	}

	public String getSearchQuery() {
		return searchQuery;
	}

	public void setSearchQuery(String searchQuery) {
		this.searchQuery = searchQuery;
	}

	public List<DriveLetters> getDriveLettersList() {
		return driveLettersList;
	}

	public void setDriveLettersList(List<DriveLetters> driveLettersList) {
		this.driveLettersList = driveLettersList;
	}

	public Integer getMaxSearchResults() {
		return maxSearchResults;
	}

	public void setMaxSearchResults(Integer maxSearchResults) {
		this.maxSearchResults = maxSearchResults;
	}

	public Long getModifiedStartDateLong() {
		return modifiedStartDateLong;
	}

	public void setModifiedStartDateLong(Long modifiedStartDateLong) {
		this.modifiedStartDateLong = modifiedStartDateLong;
	}

	public Long getModifiedEndDateLong() {
		return modifiedEndDateLong;
	}

	public void setModifiedEndDateLong(Long modifiedEndDateLong) {
		this.modifiedEndDateLong = modifiedEndDateLong;
	}

	public Long getCreatedStartDateLong() {
		return createdStartDateLong;
	}

	public void setCreatedStartDateLong(Long createdStartDateLong) {
		this.createdStartDateLong = createdStartDateLong;
	}

	public Long getCreatedEndDateLong() {
		return createdEndDateLong;
	}

	public void setCreatedEndDateLong(Long createdEndDateLong) {
		this.createdEndDateLong = createdEndDateLong;
	}	
	
	
}
