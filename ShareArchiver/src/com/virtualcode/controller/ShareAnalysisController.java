package com.virtualcode.controller;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import jcifs.smb.SmbFile;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.PieChartModel;

import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.DriveLetters;
import com.virtualcode.vo.ShareAnalysisStats;

@ManagedBean(name="shareAnalysisController")
@ViewScoped
public class ShareAnalysisController extends AbstractController implements Serializable{
	
	private Integer driveLetterId;
		
	private CartesianChartModel catModel;  
	
	//Last Modified
	private CartesianChartModel lmAgeModel;
		
	// Last Accessed.  
	private CartesianChartModel laAgeModel;
	
	// Created Date Accessed.  
	private CartesianChartModel cdAgeModel;
		
	private boolean showLMGraph=true;
	
	private boolean showLAGraph=false;
	
	private boolean showCDGraph=false;
	
	
    public void init(){
		
		if (driveLetterId != null && driveLetterId.intValue() > 0
				&& !FacesContext.getCurrentInstance().isPostback()) {
			
			loadCatModel(driveLetterId);
			loadLMAgeModel(driveLetterId);
			loadLAAgeModel(driveLetterId);
			loadCDAgeModel(driveLetterId);			
		}
	}
	
	
    private void loadCatModel(Integer driveLtterId){
    	
    	List<ShareAnalysisStats> docCatList=shareAnaylysisService.getByShare(driveLtterId);
		if(docCatList==null || docCatList.isEmpty()){
			return;
		}
		
		try{
			ShareAnalysisStats shareAStats;
			catModel=new CartesianChartModel();	
			float vol=0;
			ChartSeries chartSeries;
			chartSeries=new ChartSeries("Categories");
		    for(int i=0;i<docCatList.size();i++){		    	  
		    	shareAStats=docCatList.get(i);
		    	vol=shareAStats.getTotalVolume();
		    	vol=vol/1024;
		    	
		    	chartSeries.set(shareAStats.getDocumentCatName(), roundTwoDecimals(vol));  
		    	
		    }
		    catModel.addSeries(chartSeries);
		    
		}catch(Exception ex){
			ex.printStackTrace();
		}				
	}
    
    
    private void  loadLMAgeModel(Integer driveLtterId){
		
    	List<ShareAnalysisStats> docCatList=shareAnaylysisService.getByShare(driveLtterId);
		if(docCatList==null || docCatList.isEmpty()){
			return;
		}		
		
		try{
			ShareAnalysisStats shareAStats;
			
		    float month_3_volume=0;
		    float month_6_volume=0;
		    
		    float month_12_volume=0;
		    float month_24_volume=0;
		    float month_36_volume=0;
		    lmAgeModel=new CartesianChartModel();
		    ChartSeries ageSeries;
		    
			for(int i=0;i<docCatList.size();i++){		    	  
		    	shareAStats=docCatList.get(i);	
		    	month_3_volume=shareAStats.getLm3();	
		    	month_6_volume=shareAStats.getLm6();	
		    	month_12_volume=shareAStats.getLm12();	
		    	month_24_volume=shareAStats.getLm24();
		    	month_36_volume=shareAStats.getLm36();	
		    	
		    	month_3_volume= (month_3_volume/1024);
				month_6_volume= (month_6_volume/1024);
				month_12_volume= (month_12_volume/1024);
				
				month_24_volume= (month_24_volume/1024);
				month_36_volume= (month_36_volume/1024);
				
				ageSeries=new ChartSeries(shareAStats.getDocumentCatName());
				ageSeries.set("0-6", roundTwoDecimals(month_3_volume));
				ageSeries.set("6-12", roundTwoDecimals(month_6_volume));
				ageSeries.set("12-24", roundTwoDecimals(month_12_volume));
				ageSeries.set("24-36", roundTwoDecimals(month_24_volume));
				ageSeries.set("> 36", roundTwoDecimals(month_36_volume));
					            
				lmAgeModel.addSeries(ageSeries);
		    }			

			
		}catch(Exception ex){
			ex.printStackTrace();
		}		
			
	}
    
    
   private void  loadLAAgeModel(Integer driveLtterId){
		
    	List<ShareAnalysisStats> docCatList=shareAnaylysisService.getByShare(driveLtterId);
		if(docCatList==null || docCatList.isEmpty()){
			return;
		}		
		
		try{
			ShareAnalysisStats shareAStats;
			
		    float month_3_volume=0;
		    float month_6_volume=0;
		    
		    float month_12_volume=0;
		    float month_24_volume=0;
		    float month_36_volume=0;
		    laAgeModel=new CartesianChartModel();
		    ChartSeries ageSeries;
		    
			for(int i=0;i<docCatList.size();i++){		    	  
		    	shareAStats=docCatList.get(i);	
		    	month_3_volume=shareAStats.getLa3();	
		    	month_6_volume=shareAStats.getLa6();	
		    	month_12_volume=shareAStats.getLa12();	
		    	month_24_volume=shareAStats.getLa24();
		    	month_36_volume=shareAStats.getLa36();	
		    	
		    	month_3_volume= (month_3_volume/1024);
				month_6_volume= (month_6_volume/1024);
				month_12_volume= (month_12_volume/1024);
				
				month_24_volume= (month_24_volume/1024);
				month_36_volume= (month_36_volume/1024);
				
				ageSeries=new ChartSeries(shareAStats.getDocumentCatName());
				ageSeries.set("0-6", roundTwoDecimals(month_3_volume));
				ageSeries.set("6-12", roundTwoDecimals(month_6_volume));
				ageSeries.set("12-24", roundTwoDecimals(month_12_volume));
				ageSeries.set("24-36", roundTwoDecimals(month_24_volume));
				ageSeries.set("> 36", roundTwoDecimals(month_36_volume));
					            
				laAgeModel.addSeries(ageSeries);
		    }			

			
		}catch(Exception ex){
			ex.printStackTrace();
		}		
			
	}

   
   private void  loadCDAgeModel(Integer driveLtterId){
		
   	List<ShareAnalysisStats> docCatList=shareAnaylysisService.getByShare(driveLtterId);
		if(docCatList==null || docCatList.isEmpty()){
			return;
		}		
		
		try{
			ShareAnalysisStats shareAStats;
			
		    float month_3_volume=0;
		    float month_6_volume=0;
		    
		    float month_12_volume=0;
		    float month_24_volume=0;
		    float month_36_volume=0;
		    cdAgeModel=new CartesianChartModel();
		    ChartSeries ageSeries;
		    
			for(int i=0;i<docCatList.size();i++){		    	  
		    	shareAStats=docCatList.get(i);	
		    	month_3_volume=shareAStats.getCd3();	
		    	month_6_volume=shareAStats.getCd6();	
		    	month_12_volume=shareAStats.getCd12();	
		    	month_24_volume=shareAStats.getCd24();
		    	month_36_volume=shareAStats.getCd36();	
		    	
		    	month_3_volume= (month_3_volume/1024);
				month_6_volume= (month_6_volume/1024);
				month_12_volume= (month_12_volume/1024);
				
				month_24_volume= (month_24_volume/1024);
				month_36_volume= (month_36_volume/1024);
				
				ageSeries=new ChartSeries(shareAStats.getDocumentCatName());
				ageSeries.set("0-6", roundTwoDecimals(month_3_volume));
				ageSeries.set("6-12", roundTwoDecimals(month_6_volume));
				ageSeries.set("12-24", roundTwoDecimals(month_12_volume));
				ageSeries.set("24-36", roundTwoDecimals(month_24_volume));
				ageSeries.set("> 36", roundTwoDecimals(month_36_volume));
					            
				cdAgeModel.addSeries(ageSeries);
		    }
			
		}catch(Exception ex){
			ex.printStackTrace();
		}	
			
	}
    
    
    
    //setters & getters
    public Integer getDriveLetterId() {
		return driveLetterId;
	}

	public void setDriveLetterId(Integer driveLetterId) {
		this.driveLetterId = driveLetterId;
	}
	
	public CartesianChartModel getLmAgeModel() {
		return lmAgeModel;
	}

	public void setLmAgeModel(CartesianChartModel lmAgeModel) {
		this.lmAgeModel = lmAgeModel;
	}
	
	
	public CartesianChartModel getCatModel() {
		return catModel;
	}

	public void setCatModel(CartesianChartModel catModel) {
		this.catModel = catModel;
	}


	public CartesianChartModel getLaAgeModel() {
		return laAgeModel;
	}


	public void setLaAgeModel(CartesianChartModel laAgeModel) {
		this.laAgeModel = laAgeModel;
	}


	public CartesianChartModel getCdAgeModel() {
		return cdAgeModel;
	}


	public void setCdAgeModel(CartesianChartModel cdAgeModel) {
		this.cdAgeModel = cdAgeModel;
	}
	
	
	public boolean isShowLMGraph() {
		return showLMGraph;
	}


	public void setShowLMGraph(boolean showLMGraph) {
		this.showLMGraph = showLMGraph;
	}


	public boolean isShowLAGraph() {
		return showLAGraph;
	}


	public void setShowLAGraph(boolean showLAGraph) {
		this.showLAGraph = showLAGraph;
	}


	public boolean isShowCDGraph() {
		return showCDGraph;
	}


	public void setShowCDGraph(boolean showCDGraph) {
		this.showCDGraph = showCDGraph;
	}


	float roundTwoDecimals(float d) {
        DecimalFormat twoDForm = new DecimalFormat("#.##");
    return Float.valueOf(twoDForm.format(d));
   }
	
	/*
	
	private PieChartModel categoryModel;
	
	private PieChartModel docAgeModel;
	
	public PieChartModel getCategoryModel() {
		return categoryModel;
	}

	public PieChartModel getDocAgeModel() {
		return docAgeModel;
	}
	
	public PieChartModel getShareDiskUsageChart(){
		
		String driveLetterId=getRequestParameter("id");
		DriveLetters driveLetter=driveLettersService.getDriveLetterById(VCSUtil.getIntValue(driveLetterId));
		if(driveLetter==null){
			return null;
		}
		try{
		
			String destPath = VCSUtil.getSambaPath(driveLetter.getRestoreDomain(), driveLetter.getRestoreUsername(), driveLetter.getRestorePassword(), driveLetter.getSharePath());
			
				SmbFile smbFile = new SmbFile(destPath);
				if (!smbFile.exists()) {
				         System.out.println("UNC Path Invalid....");
				} else {
					
					float totalSpace=smbFile.length();
					long freeSpace=smbFile.getDiskFreeSpace();
					
					if(totalSpace>0){						
						float freePer=(freeSpace/totalSpace);
						freePer=freePer*100;
						int usedPer=(int)(100-freePer);
						
						PieChartModel pieModel=new PieChartModel();
						pieModel.set("Free Space", freePer);
						pieModel.set("Used Space", usedPer);
						return pieModel;
					}				
			 }
		
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return null;		
	}
	
	
    private void loadShareChartByDocumentCategoriesPercentage(Integer driveLtterId){
		    	
    	List<ShareAnalysisStats> docCatList=shareAnaylysisService.getByShare(driveLtterId);
		if(docCatList==null || docCatList.isEmpty()){
			return;
		}
		
		try{
			ShareAnalysisStats shareAStats;
									
			float allCategoryVolume=0;			
			for(int i=0;i<docCatList.size();i++){		    	  
				allCategoryVolume+=docCatList.get(i).getTotalVolume();			   	    	
			}
			categoryModel=new PieChartModel();	
			int vol=0;
			int subTotal=0;
		    for(int i=0;i<docCatList.size();i++){		    	  
		    	shareAStats=docCatList.get(i);
		    	vol=(int)((shareAStats.getTotalVolume()/allCategoryVolume)*100.0);
		    	
		    	if(i==(docCatList.size()-1)){
		    		categoryModel.set(shareAStats.getDocumentCatName(), (100-subTotal));
		    	}else{
		    	categoryModel.set(shareAStats.getDocumentCatName(), vol);
		    	subTotal+=vol;
		    	}
		    }
		    
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
				
	}
	
    
    private void  loadChartByDocAgePercentage(Integer driveLtterId){
		
    	List<ShareAnalysisStats> docCatList=shareAnaylysisService.getByShare(driveLtterId);
		if(docCatList==null || docCatList.isEmpty()){
			return;
		}		
		
		try{
			ShareAnalysisStats shareAStats;
			
			float tot_volume=0;
		    float month_3_volume=0;
		    float month_6_volume=0;
		    
		    float month_12_volume=0;
		    float month_24_volume=0;
		    float month_36_volume=0;
		    
			for(int i=0;i<docCatList.size();i++){		    	  
		    	shareAStats=docCatList.get(i);	
		    	tot_volume+=shareAStats.getTotalVolume();
		    	month_3_volume+=shareAStats.getLm3();	
		    	month_6_volume+=shareAStats.getLm6();	
		    	month_12_volume+=shareAStats.getLm12();	
		    	month_24_volume+=shareAStats.getLm24();
		    	month_36_volume+=shareAStats.getLm36();		    	
		    }
			
			month_3_volume= (month_3_volume/tot_volume)*100;
			month_6_volume= (month_6_volume/tot_volume)*100;
			month_12_volume=(int) (month_12_volume/tot_volume)*100;
			
			month_24_volume= (month_24_volume/tot_volume)*100;
			month_36_volume= (month_36_volume/tot_volume)*100;
			float sum=month_3_volume+month_6_volume+month_12_volume+month_24_volume+month_36_volume;
			
			month_3_volume=(month_3_volume/sum)*100;
			month_6_volume=(month_6_volume/sum)*100;
			month_12_volume=(month_12_volume/sum)*100;
			month_24_volume=(month_24_volume/sum)*100;
			month_36_volume=(month_36_volume/sum)*100;
			
			int m_3=(int)month_3_volume;
			int m_6=(int)month_6_volume;
			int m_12=(int)month_12_volume;
			
			int m_24=(int)month_24_volume;
			int m_36=100-(m_3+m_6+m_12+m_24);
			
			docAgeModel=new PieChartModel();
			
			docAgeModel.set("0-6", m_3);
			docAgeModel.set("6-12", m_6);
			docAgeModel.set("12-24", m_12);
			
			docAgeModel.set("24-36", m_24);
            docAgeModel.set("> 36", m_36);
			
		}catch(Exception ex){
			ex.printStackTrace();
		}		
			
	}
	
	*/
   
	

}
