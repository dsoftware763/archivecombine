package com.virtualcode.controller;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.jcr.RepositoryException;

import jcifs.smb.SmbAuthException;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;
import jespa.security.SecurityProviderException;

import org.apache.commons.io.FileUtils;
import org.icepdf.core.exceptions.PDFException;
import org.icepdf.core.exceptions.PDFSecurityException;
import org.icepdf.core.pobjects.Document;
import org.icepdf.core.pobjects.Page;
import org.icepdf.core.util.GraphicsRenderingHints;

import net.coobird.thumbnailator.Thumbnails;

import com.virtualcode.cifs.SmbWriter;
import com.virtualcode.common.AdvancedSecureURL;
import com.virtualcode.common.FileTypeMapper;
import com.virtualcode.common.SecureURL;
import com.virtualcode.common.VCSConstants;
import com.virtualcode.pdfviewer.viewer.view.DocumentManager;
import com.virtualcode.repository.UserActivityService;
import com.virtualcode.security.impl.VirtualCodeAuthorizatorImpl;
import com.virtualcode.util.JespaUtil;
import com.virtualcode.util.PasswordGenerator;
import com.virtualcode.util.RepositoryBrowserUtil;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.RoleFeatureUtil;
import com.virtualcode.util.StringUtils;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.util.VirtualcodeUtil;
import com.virtualcode.vo.CustomTreeNode;
import com.virtualcode.vo.DataGuardian;
import com.virtualcode.vo.DriveLetters;
import com.virtualcode.vo.JobSpDocLib;
import com.virtualcode.vo.LdapGroups;
import com.virtualcode.vo.LinkAccessDocuments;
import com.virtualcode.vo.LinkAccessUsers;
import com.virtualcode.vo.RoleFeature;
import com.virtualcode.vo.SearchDocument;
import com.virtualcode.vo.SystemCode;
import com.virtualcode.vo.SystemCodeType;

import edu.emory.mathcs.backport.java.util.Arrays;

@ManagedBean(name = "searchFileController")
@ViewScoped
public class SearchFileController extends AbstractController {

	public SearchFileController() {
		activeDeActivateUserControls();
		
    	if(showDocumentUrl){
    		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
    		dnsServer = ru.getCodeValue("DASUrl", SystemCodeType.ZUES_AGENT);
    	}
	}

	private String nodeID;

	private boolean authorize;

	private CustomTreeNode selectedNode;

	private String restorePath; // only for stub access direct restore

	private SecureURL secureURL = SecureURL.getInstance();
	
	private boolean buttonRendered = true;

	private boolean enabled = false;

	private String message = null;

	private boolean showProgress;

	private boolean completed = false;

	private String restoreLocation;

	private String receiverName;
	
	private String receiverEmailAddress;
	
	private String emailComments;
	
    private String mediaFilePath;
    
    private String emailSubject = "Shared File";
    
    private String receiverNameLink;
    
	private String receiverEmailAddressLink;
	
	private String emailCommentsLink;
	
    private String mediaFilePathLink;
    
    private String emailSubjectLink = "Shared Link";
    
	private boolean filesShareEnabled=true;
	
	private boolean linksShareEnabled=true;
	
	private boolean downloadEnabled=false;
	
	private boolean fileRestoreEanbled=false;
	
	private boolean mediaStreamingEnabled=false;
	
	private boolean enableDocumentTagging=false;
	
	private boolean enablePdfView = false;
	
	private boolean showPdfIcon = false;
	
	private boolean emailUsingDefaultClient = false; //non html client like Outlook etc.
	
	private String urlForPdf = "";
	
	private boolean showFrame = false;
	
	@ManagedProperty(value="#{documentManager}")
	private DocumentManager documentManager;
	
	private String emailString;
	
	private boolean emailFileSizeExceeded = false;
	
	private boolean emulateEmailSender = false;
	
	private boolean showDocumentUrl = false;
	
	private String dnsServer;
	
	private String errorMessage;
	
	private boolean alreadyExists; 
	
//	private GroupGuardian groupGuardian;C

	public void populateFileInfo() {
		showFrame = false;

		String nodeid = getRequestParameter("nodeID");
		
		nodeid = nodeid.substring(nodeid.indexOf("=")+1);
		
		nodeID = nodeid;
		
		if (nodeID == null)// || FacesContext.getCurrentInstance().isPostback())
			return;
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
//		nodeID = StringUtils.decodeUrl(nodeID);
		System.out
				.println("username for stub access : "
						+ getHttpSession().getAttribute(
								VCSConstants.SESSION_USER_NAME));
		String username = (String) getHttpSession().getAttribute(
				VCSConstants.SESSION_USER_NAME);
		selectedNode = fileExplorerService.getNodeByUUID(nodeID);

		if (selectedNode == null) {
			System.out
					.println("getNodeByUUID not found, going to search by path");
			selectedNode = fileExplorerService.getNodeByPath(nodeID);
		}

		if (selectedNode != null) {
			String authType = (String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE);
			if(authType!=null && authType.equals("db")){
				authorize = true; // if the user is db
			}else{
				authorize = fileExplorerService.authorizeStub(nodeID, username);
			}
//			 authorize=true;
			if (!authorize) {
				String accessDeniedMessage = ru.getCodeValue(
						"accessDeniedMessage", SystemCodeType.GENERAL);

				addCompErrorMessage("searchForm:hiddenPercent",accessDeniedMessage, accessDeniedMessage);
				return;
			}
			System.out.println("Decoded UUID:" + selectedNode.getPath());

			String showDriveLetter = ru.getCodeValue("enable.stub.driveLetter",
					SystemCodeType.GENERAL);

			String path = "";
			String[] elements = StringUtils.getPathElements(selectedNode
					.getPath());
			if (showDriveLetter != null && showDriveLetter.trim().equals("yes")) {

				if (!(selectedNode.getPath().contains("/SP/"))) {
					Map<String, String> mappingList = fileExplorerService
							.getDriveLettersMap();
					String matchStr = elements[1] + "/" + elements[2] + "/"
							+ elements[3] + "/" + elements[4]; // e.g.
																// TestCompany/FS/192.168.30.24/LargeFile
					if (mappingList.containsKey(matchStr.toLowerCase())) {
						String driveLtr = (String) mappingList.get(matchStr
								.toLowerCase());
						path = driveLtr;
					} else {
						path = "\\\\" + elements[3] + "\\" + elements[4];// if
																			// drive
																			// letter
																			// is
																			// enabled
																			// for
																			// stub
																			// access
																			// but
																			// not
																			// found
					}
				}
			} else {
				path = "\\\\" + elements[3] + "\\" + elements[4]; // if drive
																	// letter is
																	// disabled
																	// for stub
																	// access
			}

			path += "\\";

			for (int i = 5; i < elements.length; i++) {
				path += elements[i];
				if (i != elements.length - 1) {
					path += "\\";
				}
			}
			System.out.println(path);

			selectedNode.setCompressedURL(path);
		} else {
			String fileNotFoundMessage = ru.getCodeValue(
					"documentNotFoundMessage", SystemCodeType.GENERAL);			
			addCompErrorMessage("searchForm:hiddenPercent",fileNotFoundMessage, fileNotFoundMessage);
			return;
		}
		// authorize = true;

		if (!authorize) {
			String accessDeniedMessage = ru.getCodeValue("accessDeniedMessage",
					SystemCodeType.GENERAL);

			addCompErrorMessage("searchForm:hiddenPercent",accessDeniedMessage, accessDeniedMessage);
			return;
		}

		if (authorize && selectedNode != null) {
			UserActivityService userActivityService = serviceManager
					.getUserActivityService();
			userActivityService.recordDocAccessedActivity(username,
					getHttpRequest().getRemoteAddr(), getHttpSession().getId(),
					getHttpSession().getCreationTime(), selectedNode.getPath());
		}
		
		//get content thumbnail
		currentThumbnailPath = "";
		imageFile = false;
		
		String fileExt = selectedNode.getName().substring(selectedNode.getName().indexOf(".")).toLowerCase();
		if(ru.getCodeValue("enable_thumbnails_search",SystemCodeType.GENERAL).equals("yes")){
			
			if(fileExt.contains(".jpeg") || fileExt.contains(".bmp") || fileExt.contains(".gif")
					|| fileExt.contains(".png") || fileExt.contains(".jpg") || fileExt.contains(".pdf")){
				
				long start = System.currentTimeMillis();
				
				// TODO Auto-generated method stub
				try {
    					InputStream is = fileExplorerService.downloadFile(selectedNode.getPath()).getStream();
    					
    					if(fileExt.contains(".pdf")){
    						try {
    							Document document = new Document();
    							document.setInputStream(is, "/temp/");
    							BufferedImage image;// =page.convertToImage();//convertToImage();
    							ByteArrayOutputStream baos = new ByteArrayOutputStream();
    							

    							image = (BufferedImage) document.getPageImage(0, GraphicsRenderingHints.SCREEN,
    			                        Page.BOUNDARY_CROPBOX, 0.0f, 1.0f);

    							ImageIO.write(image, "png", baos);
    							is = new ByteArrayInputStream(baos.toByteArray());
    							
    						} catch (IOException | PDFException | PDFSecurityException e) {
    							// TODO Auto-generated catch block
    							e.printStackTrace();
    						}
    					}    					
					
			        String sessionId=getHttpSession().getId();
			              
		            File outputDir=new File(getContextRealPath()+"/thumbnails/"+sessionId);
		            if(!outputDir.exists()){
		           	  outputDir.mkdirs();
		            }
		            FileUtils.cleanDirectory(outputDir);	// delete any existing files in the dir
		            Thumbnails.of(is).size(175, 175).toFile(new File(getContextRealPath()+"/thumbnails/"+sessionId +"/" + selectedNode.getName().substring(0, selectedNode.getName().lastIndexOf("."))+".png"));
					System.out.println("Thumbnail creation successful....");
					imageFile = true;
					currentThumbnailPath = "thumbnails/"+sessionId +"/" + selectedNode.getName().substring(0, selectedNode.getName().lastIndexOf(".")) + ".png";
					currentThumbnailPath = currentThumbnailPath.replaceAll(" ", "%20");
					
//					if(enablePdfView)
//		    			showPdfIcon = VCSUtil.isPdfViewable(selectedNode.getName());

				} catch (IOException | RepositoryException e) {
					// TODO Auto-generated catch block
					System.out.println("Thumbnail cannot be generated for " + selectedNode.getName() + ". Reason: " + e.getMessage());
	//				e.printStackTrace();
				}
				
				System.out.println("Time taken: "+ (System.currentTimeMillis()-start) + " ms");
			}
			
		}
		if(emailUsingDefaultClient)
			emailUsingDefaultClient();
		if(enablePdfView)
			showPdfIcon = VCSUtil.isPdfViewable(selectedNode.getName());

//		  List<SystemCode> codesList=systemCodeService.getSystemCodeByCodename("max_attachment_size");		  
		  int maxAttachmentSize=0;
		  maxAttachmentSize = VCSUtil.getIntValue(ru.getCodeValue("max_attachment_size", SystemCodeType.GENERAL));
//		  if(codesList!=null && codesList.size()>0){
//			  maxAttachmentSize=VCSUtil.getIntValue(codesList.get(0).getCodevalue());
//		    }
		    String fs=selectedNode.getFileSize();
		    float fileSize=-1;
		    if(fs!=null){
		    	String[] arr=fs.split(" ");
		    	if(arr!=null && arr.length==2){
		    		fs=arr[0];
		    		fileSize=VCSUtil.getFloatValue(fs);
		    		if(arr[1].equalsIgnoreCase("MB")){
		    			fileSize=fileSize*1024;
		    		}
		    		if(arr[1].equalsIgnoreCase("GB")){
		    			fileSize=fileSize*1024*1024;
		    		}
		    	}
		    }
		    maxAttachmentSize=maxAttachmentSize*1024;
		   
		    if(maxAttachmentSize>0 && fileSize>0 && fileSize<maxAttachmentSize){
		    	emailFileSizeExceeded = false;
		    }else{
		    	emailFileSizeExceeded = true;
//		    	addErrorMessage("Email document size exceeds max attachment size", "Email document size exceeds max attachment size");
//		    	return;
		    }
	}
	
	public void populateFileInfoCifs(){
//		try {
			imageFile = false;
			currentThumbnailPath = null;
			selectedNode = null;
			String nodeID = getRequestParameter("nodeID");
			String modifiedDate = getRequestParameter("modifiedDate");
			
    		List<DriveLetters> tempList = driveLettersService.getAll();
    		List<DriveLetters> driveLettersList	=	jobService.getSpDocLibByDriveLetters(tempList);
    		
			ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
//			String domain = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
//    		String username = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
//    		String password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
    		
    		DriveLetters driveLetter = null;
  	      	List<DriveLetters> driveLetterList=driveLettersService.getAll();
  	      	String originalID = nodeID;
  	      	nodeID = "\\\\" + nodeID.substring(nodeID.indexOf("@")+1);
  	      	for(DriveLetters letter: driveLetterList){
  				String sharePath = letter.getSharePath().toLowerCase();
  				if(sharePath.startsWith("\\\\")){
//  					ResourcesUtil ru = ResourcesUtil.getResourcesUtil();				
//  					sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase() + "/fs/" + sharePath.substring(2).replaceAll("\\\\", "/");
  				}
  				if(nodeID.contains("/"))
  					nodeID = nodeID.replaceAll("/", "\\\\");
//  					sharePath = sharePath.substring(0, sharePath.lastIndexOf("/"));
//  				if(!nodeID.endsWith("\\"))
//  					nodeID = nodeID+ "\\";
  				
  				System.out.println(sharePath + ", " + nodeID.toLowerCase());
  	      	if(nodeID.toLowerCase().contains(sharePath)){
  	      		driveLetter = letter;
  	      		break;
  	      	}
  	      }
//  		  ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
//  		  String domain = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
//  		  String username = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
//  		  String password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
//  		  if(driveLetter!=null && driveLetter.getRestoreDomain()!=null){
//  	 			domain =  driveLetter.getRestoreDomain();
//  	 			username = driveLetter.getRestoreUsername();
//  	 			password = driveLetter.getRestorePassword();
//  	 		}
//			String searchPath = getRequestParameter("nodeID");//"smb://"+ domain +";"+ username +":"+ password +"@"+remotePath.substring(2).replaceAll("\\\\", "/")+"/";
  	      	String spDocLib = null;
  	      	if(driveLetter==null){
  	      		
  	      		List<JobSpDocLib> libsList = jobService.getLibsByNameLike("");
  	      		for(JobSpDocLib lib: libsList){
  	      			System.out.println(lib.getName());
  	      			if(originalID.contains(lib.getName()))
  	      				spDocLib = lib.getId()+"";
  	      		}
  	      		  	      	
  	      	}else{
  	      		spDocLib = driveLetter.getSpDocLibId()+"";
  	      	}
  	      	
			String documentName = nodeID.substring(nodeID.lastIndexOf("\\")+1);
			SearchDocument document = searchService.searchDocument(spDocLib+"", documentName, modifiedDate);
			if(document!=null){
				CustomTreeNode node = new CustomTreeNode();
				node.setUuid(document.getUid());
				node.setName(document.getDocumentName());
				node.setPath(document.getDocumentPath());
				node.setCompressedURL(document.getDocumentPath()+document.getDocumentName());
				node.setLastModified(document.getLastModificationDate());
				node.setCreatedDate(document.getCreatedDate());
				node.setArchive(false);
				node.setLastAccessed(document.getLastAccessDate());
				node.setType("nt:file");
				node.setFileSize(document.getDocumentSizeToDisplay());
				RepositoryBrowserUtil repoBrwUtil = RepositoryBrowserUtil.getInstance();
				FileTypeMapper fileTypeMapper = repoBrwUtil.getFileTypeMapperFromName(documentName, false);

//				System.out.println(document.getDocumentUrl());
				node.setFileTypeDesc(fileTypeMapper.getTypeText());
				node.setIconUrl(fileTypeMapper.getIconName());
				node.setDownloadUrl(document.getDocumentUrl());
                //manually handling results to apply permissions
				String sidString = "";
				   if(getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE).equals("db")){
					   sidString = null;
						selectedNode = node;
						authorize = true;
				   }else{
					   System.out.println("--getting sids for user: " + getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME));					   
					   try {
						   VirtualCodeAuthorizatorImpl authorizator = VirtualCodeAuthorizatorImpl.getInstance();
						sidString = authorizator.sidString(getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME)+"");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				   }
                if(sidString!=null && sidString.trim().length()>0){
                	String[] sidsArr = sidString.split(",");
//                    boolean authorize = false;
//	                List<SearchDocument> finalList = result.getResults();
//	                List<SearchDocument> finalAuthorizedList = new ArrayList<SearchDocument>();
//	                for(SearchDocument doc: finalList)
//	                {
//	                	System.out.println("checking doc: " + doc.getDocumentName() + " with sids: " + doc.getSecurityAllowSids());
//	                	System.out.println("checking doc: " + doc.getDocumentName() + " with sids: " + doc.getShareAllowSids());
//	                	System.out.println("checking doc: " + doc.getDocumentName() + " with sids: " + doc.getSecurityDenySids());
//	                	System.out.println("checking doc: " + doc.getDocumentName() + " with sids: " + doc.getShareDenySids());
	                	
	                	if(document.getSecurityDenySids()!=null){
	                        boolean checkComplete = false;
	                        for (int i = 0; i < sidsArr.length && !checkComplete; i++) {
	                            String memberGrp = sidsArr[i];
//	                            log.debug("Checking for member Group.... :" + memberGrp);
	                            System.out.println("Checking for member Group for deny.... :" + memberGrp);
	                            List notAllowedSids = Arrays.asList(document.getSecurityDenySids().split(","));
	                            List sharedNotAllowedSids = Arrays.asList(document.getShareDenySids().split(","));
	                            
	                            // User is member of not AllowedSID Normal OR Shared
	                            if (isMemberOf(notAllowedSids, memberGrp) || isMemberOf(sharedNotAllowedSids, memberGrp)) {
	                                checkComplete = true;
	                                authorize = false;
	                            }
	                        }//for(int i=0;i<userGroup.size() && !checkComplete;i++){
//	                  modified to allow access if any group is found in any allowed SID (one group in shared allow and one in security allow)
	                        boolean found = false;
	                        for (int i = 0; i < sidsArr.length  && !checkComplete; i++) {

	                        	String memberGrp = sidsArr[i];
//	                            log.debug("Checking for member Group.... :" + memberGrp);                    
//	                            List allowedSids = (List) sidsLists.get(0);
	                            List sharedAllowedSids = Arrays.asList(document.getShareAllowSids().split(","));

//	                            if(isMemberOf(allowedSids, memberGrp) || isMemberOf(sharedAllowedSids, memberGrp)){
	                            if (isMemberOf(sharedAllowedSids, memberGrp)) {
	                                found = true;
//	                                authorize = true;
//	                                log.debug("one SID found, breaking loop for share allow group: " + memberGrp);                        

	                                break;
	                            }                    
	                        }//for(int i=0;i<userGroup.size() && !checkComplete;i++){
	                        
//	                        log.debug("found: " + found);
	                        //System.out.println("found: " + found);
	                        if(!found){
	                             checkComplete = true;
	                        }

	                        for (int i = 0; i < sidsArr.length && !checkComplete; i++) {
	                             String memberGrp = sidsArr[i];
//	                            log.debug("Checking for member Group.... :" + memberGrp);                    
	                            List allowedSids = Arrays.asList(document.getSecurityAllowSids().split(","));
//	                            List sharedAllowedSids = (List) sidsLists.get(2);

//	                            if(isMemberOf(allowedSids, memberGrp) || isMemberOf(sharedAllowedSids, memberGrp)){
	                            if ((found) && isMemberOf(allowedSids, memberGrp)) {                        
//	                                log.debug("one SID found, breaking loop for security allow group: " + memberGrp);

	                                checkComplete = true;
	                                authorize = true;
	                            }
	                        }//for(int i=0;i<userGroup.size() && !checkComplete;i++){
	                	}	 
	                	
//	                }
				selectedNode = node;
//				authorize = true;
			}
			}
			
			/*long startTime = System.currentTimeMillis();
			
			System.out.println("start time: " + startTime);
			SmbFile remoteFile = new SmbFile(searchPath);
			 DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
			if(remoteFile.exists()){
//				if(firstLevelFiles==null)
//					firstLevelFiles = new ArrayList<CustomTreeNode>();
				System.out.println("file exists");
				System.out.println(df.format(new Date(remoteFile.getLastModified())));
				System.out.println(df.format(new Date(remoteFile.createTime())));
//				String[] dirList = remoteFile.list();
//				for(int i = 0; i < dirList.length ; i++){
//					SmbFile file = new SmbFile(searchPath + dirList[i]);
//					if(file.exists()){
						CustomTreeNode node = new CustomTreeNode();
						node.setName(remoteFile.getName());
						if(node.getName().endsWith("/"))
							node.setName(node.getName().substring(0, node.getName().length()-1));
						node.setLastModified(df.format(new Date(remoteFile.getLastModified())));
						node.setCreatedDate(df.format(new Date(remoteFile.createTime())));
						node.setLastAccessed(df.format(new Date(remoteFile.lastAccess())));
                        DecimalFormat decf = new DecimalFormat("#.##");
                        double length = remoteFile.getContentLength();
                        boolean isFolder = remoteFile.isDirectory();
                        if(!isFolder){
	                        node.setSizeinKbs(Math.round(length)+"");
	                        length = length / 1000d;
	                        String fileSize = decf.format(length)+" KB";                        
	                        
	                        //convert to MBs
	                        if(length>1024d){
	                       	 length = length / 1024d;
	                       	fileSize = decf.format(length)+" MB";
	                        }
	                      //convert to GBs
	                        if(length>1024d){
	                       	 length = length / 1024d;
	                         fileSize = decf.format(length)+" GB";
	                        }	                        
							node.setFileSize(fileSize);
                        }
                        if(isFolder)
                        	node.setType("nt:folder");
                        else
                        	node.setType("nt:file");
	                    RepositoryBrowserUtil repoBrwUtil = RepositoryBrowserUtil.getInstance();
	                    FileTypeMapper fileTypeMapper = repoBrwUtil.getFileTypeMapperFromName(node.getName(), isFolder);
	                    
	                    node.setFileTypeDesc(fileTypeMapper.getTypeText());
	                    node.setIconUrl(fileTypeMapper.getIconName());
//						System.out.println("name " + i + " : " + file.getName());
//						System.out.println(new Date(file.getLastModified()));
//						System.out.println(new Date(file.createTime()));
//						System.out.println(file.getContentLength());
	                    node.setArchive(false);
//	                    System.out.println(remoteFile.getPath());
//	                    System.out.println(remoteFile.getCanonicalPath());
//	                    System.out.println(remoteFile.getDfsPath());
	                    node.setPath("\\\\"+remoteFile.getPath().substring(remoteFile.getPath().indexOf("@")+1).replaceAll("//", "/").replaceAll("/", "\\\\"));
						if(node.getPath().endsWith("\\"))
							node.setPath(node.getPath().substring(0, node.getPath().length()-1));
//						node.setHash(SHAsum((remoteFile.getLastModified()+"").getBytes()));
//						System.out.println("hash: "+SHAsum((remoteFile.getLastModified()+"").getBytes()));
						
	                    selectedNode = node;
	                    selectedNode.setCompressedURL(node.getPath());
	                    authorize = true;
	                    
//						firstLevelFiles.add(node);
	            		if(enablePdfView)
	            			showPdfIcon = VCSUtil.isPdfViewable(selectedNode.getName());
//					}
					
//				}
			}
			// for thumbnail generation
			String fileExt = selectedNode.getName().substring(selectedNode.getName().lastIndexOf(".")).toLowerCase();
			if(fileExt.contains(".jpeg") || fileExt.contains(".bmp") || fileExt.contains(".gif")
					|| fileExt.contains(".png") || fileExt.contains(".jpg") || fileExt.contains(".pdf")){
				
				long start = System.currentTimeMillis();
				
				// TODO Auto-generated method stub
				try {
					InputStream is = remoteFile.getInputStream();// fileExplorerService.downloadFile(selectedNode.getPath()).getStream();
					if(fileExt.contains(".pdf")){
						try {
//    							FileOutputStream fos = new FileOutputStream(new File("D:/images/newpdf.pdf"));
//    							IOUtils.copyLarge(is, fos);
//    							fos.close();
							Document document = new Document();
							document.setInputStream(is, "/tmp/");
//    							PDDocument doc = PDDocument.load(is);
//    							List<PDPage>pages =  doc.getDocumentCatalog().getAllPages();
//    							PDPage page = pages.get(0);
//    							PDFImageWriter imageWrite = new PDFImageWriter();
//    							imageWrite.writeImage(doc, "png", null, 1 , 3, "D:/images/HeapSizepdf");
							BufferedImage image;// =page.convertToImage();//convertToImage();
							ByteArrayOutputStream baos = new ByteArrayOutputStream();
							
//    							ImageIO.write(image, "jpg", new File("D:/images/pdf.jpg"));
							image = (BufferedImage) document.getPageImage(0, GraphicsRenderingHints.SCREEN,
			                        Page.BOUNDARY_CROPBOX, 0.0f, 1.0f);
//    							image1.g
							ImageIO.write(image, "png", baos);
							is = new ByteArrayInputStream(baos.toByteArray());
							
//    							PDFImageWriter imageWrite = new PDFImageWriter();
//    							imageWrite.writeImage(doc, "png", null, 1 , pages.size(), "D:/images/Heap Size");
//    							File outputfile = new File("D:/images/Heap Size.png");
//    							ImageIO.write(image, "png", outputfile);
//    							baos.close();
//    							fos.close();
//    							doc.close();			
						} catch (IOException | PDFException | PDFSecurityException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					
			        String sessionId=getHttpSession().getId();
			              
		            File outputDir=new File(getContextRealPath()+"/thumbnails/"+sessionId);
		            if(!outputDir.exists()){
		           	  outputDir.mkdirs();
		            }
		            FileUtils.cleanDirectory(outputDir);	// delete any existing files in the dir
					Thumbnails.of(is).size(200, 200).toFile(new File(getContextRealPath()+"/thumbnails/"+sessionId +"/" + selectedNode.getName().substring(0, selectedNode.getName().lastIndexOf("."))+".png"));
					System.out.println("Thumbnail creation successful....");
//					showThumbnail = true;
					currentThumbnailPath = "thumbnails/"+sessionId +"/" + selectedNode.getName().substring(0, selectedNode.getName().lastIndexOf(".")) + ".png";
					currentThumbnailPath = currentThumbnailPath.replaceAll(" ", "%20");
					is.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					System.out.println("Thumbnail cannot be generated for " + selectedNode.getName() + ". Reason: " + e.getMessage());
	//				e.printStackTrace();
				}
				System.out.println("Time taken: "+ (System.currentTimeMillis()-start) + " ms");				
			}else{
//				showThumbnail = false;
			}
//			System.out.println("Total time taken for " + firstLevelFiles.size() + " = " + (System.currentTimeMillis() -  startTime) + " ms");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SmbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} /*catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}

    private boolean isMemberOf(List sidList, String sidToSearch) {

        boolean found = false;
        try {
            Collections.sort(sidList);

            found = ((Collections.binarySearch(sidList, sidToSearch) < 0) ? false : true);
        } catch (Exception ex) {
//            log.error("Error while searching SID in List " + ex);
        }

        return found;
    }
	public void searchById(){
		System.out.println("entered ID: " + nodeID);
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();

		selectedNode = null;
		if(nodeID==null || nodeID.trim().length()<1){
			addCompErrorMessage("searchByIdForm:documentId","Please add document ID", "Please add document ID");
			return;
		}else if(nodeID.trim().length()<32){
			addCompErrorMessage("searchByIdForm:documentId","Document ID is too short", "Document ID is too short");
			return;
		}
		
			
		// handle hiphens: to convert f66041ae56fd4571aa0ed5d6d38e4c5a to f66041ae-56fd-4571-aa0e-d5d6d38e4c5a
		if(!nodeID.contains("-")){
			System.out.println("adding hiphens");
			String tempId = nodeID.substring(0, 8) + "-" + nodeID.substring(8, 12) + "-" + nodeID.substring(12, 16)
					+ "-" + nodeID.substring(16, 20) + "-" + nodeID.substring(20);
//			tempId = tempId + nodeID.substring(tempId.length()-1, nodeID.indexOf("-"));
//			System.out.println(tempId);
			nodeID = tempId;
//			String idSection1 = tempId.substring(0,tempId.indexOf("-"));
//			System.out.println(idSection1);
//			tempId = tempId.substring(tempId.indexOf("-")+1);
//			String idSection2 = tempId.substring(0,tempId.indexOf("-"));
//			System.out.println(idSection2);
//			tempId = tempId.substring(tempId.indexOf("-")+1);
//			String idSection3 = tempId.substring(0,tempId.indexOf("-"));
//			System.out.println(idSection3);
//			tempId = tempId.substring(tempId.indexOf("-")+1);
//			String idSection4 = tempId.substring(0,tempId.indexOf("-"));
//			System.out.println(idSection4);
//			tempId = tempId.substring(tempId.indexOf("-")+1);
		}
		selectedNode = fileExplorerService.getNodeByUUID(nodeID);
		
				
//		if (selectedNode == null) {
//			System.out.println("getNodeByUUID not found, going to search by path");
//			selectedNode = fileExplorerService.getNodeByPath(nodeID);
//		}

		if (selectedNode != null && !selectedNode.getType().equals("nt:folder")) {			
			String authType = (String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE);
			String username = (String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
			if(authType!=null && authType.equals("db")){
				authorize = true; // if the user is db
			}else{
				authorize = fileExplorerService.authorizeStub(nodeID, username);
			}
//			 authorize=true;
			if (!authorize) {
				String accessDeniedMessage = ru.getCodeValue(
						"accessDeniedMessage", SystemCodeType.GENERAL);

				addCompErrorMessage("searchByIdForm:documentId",accessDeniedMessage, accessDeniedMessage);
//				addErrorMessage(accessDeniedMessage, accessDeniedMessage);
				return;
			}
			System.out.println("Decoded UUID:" + selectedNode.getPath());

			String showDriveLetter = ru.getCodeValue("enable.stub.driveLetter",
					SystemCodeType.GENERAL);

			String path = "";
			String[] elements = StringUtils.getPathElements(selectedNode
					.getPath());
			if (showDriveLetter != null && showDriveLetter.trim().equals("yes")) {

				if (!(selectedNode.getPath().contains("/SP/"))) {
					Map<String, String> mappingList = fileExplorerService
							.getDriveLettersMap();
					String matchStr = elements[1] + "/" + elements[2] + "/"
							+ elements[3] + "/" + elements[4]; // e.g.
																// TestCompany/FS/192.168.30.24/LargeFile
					if (mappingList.containsKey(matchStr.toLowerCase())) {
						String driveLtr = (String) mappingList.get(matchStr
								.toLowerCase());
						path = driveLtr;
					} else {
						path = "\\\\" + elements[3] + "\\" + elements[4];// if
																			// drive
																			// letter
																			// is
																			// enabled
																			// for
																			// stub
																			// access
																			// but
																			// not
																			// found
					}
				}
			} else {
				path = "\\\\" + elements[3] + "\\" + elements[4]; // if drive
																	// letter is
																	// disabled
																	// for stub
																	// access
			}

			path += "\\";

			for (int i = 5; i < elements.length; i++) {
				path += elements[i];
				if (i != elements.length - 1) {
					path += "\\";
				}
			}
//			System.out.println(path);

			selectedNode.setCompressedURL(path);
		} else {
			String fileNotFoundMessage = ru.getCodeValue(
					"documentNotFoundMessage", SystemCodeType.GENERAL);			
			addCompErrorMessage("searchByIdForm:documentId",fileNotFoundMessage, fileNotFoundMessage);
			selectedNode = null;
//			addErrorMessage(fileNotFoundMessage, fileNotFoundMessage);
			return;
		}
		
		//get content thumbnail
		currentThumbnailPath = "";
		imageFile = false;
		
		String fileExt = selectedNode.getName().substring(selectedNode.getName().indexOf(".")).toLowerCase();
		if(ru.getCodeValue("enable_thumbnails_search",SystemCodeType.GENERAL).equals("yes")){
			
			if(fileExt.contains(".jpeg") || fileExt.contains(".bmp") || fileExt.contains(".gif")
					|| fileExt.contains(".png") || fileExt.contains(".jpg") || fileExt.contains(".pdf")){
				
				long start = System.currentTimeMillis();
				
				// TODO Auto-generated method stub
				try {
    					InputStream is = fileExplorerService.downloadFile(selectedNode.getPath()).getStream();
    					
    					if(fileExt.contains(".pdf")){
    						try {
    							Document document = new Document();
    							document.setInputStream(is, "/temp/");
    							BufferedImage image;// =page.convertToImage();//convertToImage();
    							ByteArrayOutputStream baos = new ByteArrayOutputStream();
    							

    							image = (BufferedImage) document.getPageImage(0, GraphicsRenderingHints.SCREEN,
    			                        Page.BOUNDARY_CROPBOX, 0.0f, 1.0f);

    							ImageIO.write(image, "png", baos);
    							is = new ByteArrayInputStream(baos.toByteArray());
    							
    						} catch (IOException | PDFException | PDFSecurityException e) {
    							// TODO Auto-generated catch block
    							e.printStackTrace();
    						}
    					}    					
					
			        String sessionId=getHttpSession().getId();
			              
		            File outputDir=new File(getContextRealPath()+"/thumbnails/"+sessionId);
		            if(!outputDir.exists()){
		           	  outputDir.mkdirs();
		            }
		            FileUtils.cleanDirectory(outputDir);	// delete any existing files in the dir
		            Thumbnails.of(is).size(175, 175).toFile(new File(getContextRealPath()+"/thumbnails/"+sessionId +"/" + selectedNode.getName().substring(0, selectedNode.getName().lastIndexOf("."))+".png"));
					System.out.println("Thumbnail creation successful....");
					imageFile = true;
					currentThumbnailPath = "thumbnails/"+sessionId +"/" + selectedNode.getName().substring(0, selectedNode.getName().lastIndexOf(".")) + ".png";
					currentThumbnailPath = currentThumbnailPath.replaceAll(" ", "%20");
					
//					if(enablePdfView)
//		    			showPdfIcon = VCSUtil.isPdfViewable(selectedNode.getName());

				} catch (IOException | RepositoryException e) {
					// TODO Auto-generated catch block
					System.out.println("Thumbnail cannot be generated for " + selectedNode.getName() + ". Reason: " + e.getMessage());
	//				e.printStackTrace();
				}
				
				System.out.println("Time taken: "+ (System.currentTimeMillis()-start) + " ms");
			}else{
				imageFile = false;
				currentThumbnailPath = null;
				
			}
			
		}
		
		if(emailUsingDefaultClient)
			emailUsingDefaultClient();
		if(enablePdfView)
			showPdfIcon = VCSUtil.isPdfViewable(selectedNode.getName());

//		  List<SystemCode> codesList=systemCodeService.getSystemCodeByCodename("max_attachment_size");		  
		  int maxAttachmentSize=0;
		  maxAttachmentSize = VCSUtil.getIntValue(ru.getCodeValue("max_attachment_size", SystemCodeType.GENERAL));
//		  if(codesList!=null && codesList.size()>0){
//			  maxAttachmentSize=VCSUtil.getIntValue(codesList.get(0).getCodevalue());
//		    }
		    String fs=selectedNode.getFileSize();
		    float fileSize=-1;
		    if(fs!=null){
		    	String[] arr=fs.split(" ");
		    	if(arr!=null && arr.length==2){
		    		fs=arr[0];
		    		fileSize=VCSUtil.getFloatValue(fs);
		    		if(arr[1].equalsIgnoreCase("MB")){
		    			fileSize=fileSize*1024;
		    		}
		    		if(arr[1].equalsIgnoreCase("GB")){
		    			fileSize=fileSize*1024*1024;
		    		}
		    	}
		    }
		    maxAttachmentSize=maxAttachmentSize*1024;
		   
		    if(maxAttachmentSize>0 && fileSize>0 && fileSize<maxAttachmentSize){
		    	emailFileSizeExceeded = false;
		    }else{
		    	emailFileSizeExceeded = true;
//		    	addErrorMessage("Email document size exceeds max attachment size", "Email document size exceeds max attachment size");
//		    	return;
		    }
		
	}
	public void loadImageFile(){
		String nodeid = getRequestParameter("downloadId");
		if(nodeid!=null && nodeid.length()>0){
			nodeid = nodeid.substring(nodeid.indexOf("=")+1);
			selectedNode = fileExplorerService.getNodeByUUID(nodeid);
		}
		
		long start = System.currentTimeMillis();
		
		// TODO Auto-generated method stub
		try {
				InputStream is = fileExplorerService.downloadFile(selectedNode.getPath()).getStream();
				
//				if(fileExt.contains(".pdf")){
//					try {
//						Document document = new Document();
//						document.setInputStream(is, "/temp/");
//						BufferedImage image;// =page.convertToImage();//convertToImage();
//						ByteArrayOutputStream baos = new ByteArrayOutputStream();
//						
//
//						image = (BufferedImage) document.getPageImage(0, GraphicsRenderingHints.SCREEN,
//		                        Page.BOUNDARY_CROPBOX, 0.0f, 1.0f);
//
//						ImageIO.write(image, "png", baos);
//						is = new ByteArrayInputStream(baos.toByteArray());
//						
//					} catch (IOException | PDFException | PDFSecurityException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}    					
			
	        String sessionId=getHttpSession().getId();
	              
            File outputDir=new File(getContextRealPath()+"/thumbnails/"+sessionId);
            if(!outputDir.exists()){
           	  outputDir.mkdirs();
            }
            FileUtils.cleanDirectory(outputDir);	// delete any existing files in the dir
            Thumbnails.of(is).size(600, 600).toFile(new File(getContextRealPath()+"/thumbnails/"+sessionId +"/" + selectedNode.getName().substring(0, selectedNode.getName().lastIndexOf("."))+".png"));
			System.out.println("Thumbnail creation successful....");
			imageFile = true;
			currentThumbnailPath = "thumbnails/"+sessionId +"/" + selectedNode.getName().substring(0, selectedNode.getName().lastIndexOf(".")) + ".png";
			currentThumbnailPath = currentThumbnailPath.replaceAll(" ", "%20");
			
//			if(enablePdfView)
//    			showPdfIcon = VCSUtil.isPdfViewable(selectedNode.getName());

		} catch (IOException | RepositoryException e) {
			// TODO Auto-generated catch block
			System.out.println("Thumbnail cannot be generated for " + selectedNode.getName() + ". Reason: " + e.getMessage());
//				e.printStackTrace();
		}
		
		System.out.println("Time taken: "+ (System.currentTimeMillis()-start) + " ms");
	}
	
	public void downloadFile() {
		try {
			String url = "downloadfile.jsp?nodeID="
					+ selectedNode.getDownloadUrl();

			if (selectedNode != null) {
				String username = (String) getHttpSession().getAttribute(
						VCSConstants.SESSION_USER_NAME);
				UserActivityService userActivityService = serviceManager
						.getUserActivityService();
				userActivityService.recordDownloadActivity(username,
						getHttpRequest().getRemoteAddr(), getHttpSession()
								.getId(), getHttpSession().getCreationTime(),
						selectedNode.getPath());
			}

			getHttpResponse().sendRedirect(url);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void downloadFileCifs() {
		try {
			String url = selectedNode.getDownloadUrl();

			if (selectedNode != null) {
				String username = (String) getHttpSession().getAttribute(
						VCSConstants.SESSION_USER_NAME);
				UserActivityService userActivityService = serviceManager
						.getUserActivityService();
				userActivityService.recordDownloadActivity(username,
						getHttpRequest().getRemoteAddr(), getHttpSession()
								.getId(), getHttpSession().getCreationTime(),
						selectedNode.getPath());
			}

			getHttpResponse().sendRedirect(url);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
    public String viewPDF(){
    	   String url=	"PdfViewer.faces?faces-redirect=true&documentPath="+selectedNode.getDownloadUrl(); 
    	   showFrame = true;
    	   return null;
    }
    
    public void show(){   		
// 	   String url=	"common/simple_document.jsp?doc="+selectedNode.getDownloadUrl();
//    	while(true){
//    		AdvSearchUtil util = AdvSearchUtil.getAdvSearchUtil();
//    		if(util.isDocumentReady())
//    			break;
//    	}
		String nodeid = getRequestParameter("downloadId");
		if(nodeid!=null && nodeid.length()>0){
			nodeid = nodeid.substring(nodeid.indexOf("=")+1);
			selectedNode = fileExplorerService.getNodeByUUID(nodeid);
		}			
		
		
//    	String url = getRequestParameter("downloadUrl");
//    	System.out.println(url);
    	documentManager.setDocumentPath(selectedNode.getDownloadUrl());
    	documentManager.openDocument();
    	String filename = selectedNode.getName();
    	filename = filename.substring(0, filename.lastIndexOf("."))+".pdf";
        MessageDigest md;
        StringBuffer sb = null;
		try {
			md = MessageDigest.getInstance("MD5");
			System.out.println(filename.substring(0, filename.indexOf(".")));
			md.update(filename.substring(0, filename.indexOf(".")).getBytes());
			byte[] digest = md.digest();
			sb = new StringBuffer();
			for (byte b : digest) {
				sb.append(Integer.toHexString((int) (b & 0xff)));
			}
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

        filename = sb.toString() + filename.substring(filename.indexOf("."));//System.currentTimeMillis() + filename.substring(filename.indexOf(".")); //to handle foreign characters

    	urlForPdf = filename.replaceAll(" ", "_").toLowerCase();
//    	boolean b = filename.matches("^[\u0000-\u0080]+$");
//    	try {
//			byte[] bytes = filename.getBytes("US-ASCII");
//			System.out.println(bytes);
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//    	
    		
 	   showFrame = true;
// 	   return null;
    }
    
    public void showUncPdf(){
//    	String smbPath = getCompleteSmbPath(selectedNode.getPath());
    	String smbPath = getRequestParameter("documentPath");
    	if(smbPath.endsWith("/")){
    		smbPath = smbPath.substring(0, smbPath.lastIndexOf("/"));
    	}
    	if(smbPath.endsWith("/")){
    		smbPath = smbPath.substring(0, smbPath.lastIndexOf("/"));
    	}
     	documentManager.setDocumentPath(smbPath);
     	documentManager.openDocument();
     	String filename = documentManager.getDocumentName();
//     	filename = filename.substring(0, filename.lastIndexOf("."))+".pdf";
//        MessageDigest md;
//        StringBuffer sb = null;
//		try {
//			md = MessageDigest.getInstance("MD5");
//			System.out.println(filename.substring(0, filename.indexOf(".")));
//			md.update(filename.substring(0, filename.indexOf(".")).getBytes());
//			byte[] digest = md.digest();
//			sb = new StringBuffer();
//			for (byte b : digest) {
//				sb.append(Integer.toHexString((int) (b & 0xff)));
//			}
//		} catch (NoSuchAlgorithmException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}

//        filename = sb.toString() + filename.substring(filename.indexOf("."));//System.currentTimeMillis() + filename.substring(filename.indexOf(".")); //to handle foreign characters
     	urlForPdf = filename.replaceAll(" ", "_").toLowerCase();
     	showFrame = true;
     }
    
    public String getCompleteSmbPath(String path){
		ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
		String domain = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
		String username = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
		String password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
	      

//		  ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
//		  String domain = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
//		  String username = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
//		  String password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);

		
		DriveLetters driveLetter = null;
	      	List<DriveLetters> driveLetterList=driveLettersService.getAll();
	      
	      	for(DriveLetters letter: driveLetterList){
				String sharePath = letter.getSharePath().toLowerCase();
				if(sharePath.startsWith("\\\\")){
//					ResourcesUtil ru = ResourcesUtil.getResourcesUtil();				
//					sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase() + "/fs/" + sharePath.substring(2).replaceAll("\\\\", "/");
				}
//				if(remotePath.endsWith("/"))
//					sharePath = sharePath.substring(0, sharePath.lastIndexOf("/"));
				if(!path.endsWith("\\"))
					path = path+ "\\";
				System.out.println(sharePath + ", " + path.toLowerCase());
	      	if(path.toLowerCase().contains(sharePath)){
	      		driveLetter = letter;
	      		break;
	      	}
	      }
			  if(driveLetter!=null && driveLetter.getRestoreDomain()!=null){
		 			domain =  driveLetter.getRestoreDomain();
		 			username = driveLetter.getRestoreUsername();
		 			password = driveLetter.getRestorePassword();
		 		}
			  
			  String searchPath = "smb://"+ domain +";"+ username +":"+ password +"@"+path.substring(2).replaceAll("\\\\", "/")+"/";
		return searchPath;
	}

    public void linkOptionListner(){
    	System.out.println("Selected email option: "+linkOption);
    	emailUsingDefaultClient();
    }
    
	public Integer getCompletedPercentage() {

		if (selectedNode == null) {
			return 0;
		}
		String msg = getJobMessage(selectedNode.getPath().substring(1));

		// if(enabled){
		Integer percentage = SmbWriter.getCompletedPercentage(selectedNode
				.getPath().substring(1));
		if (percentage != null && percentage.intValue() == 100) {
			SmbWriter.removeJob(selectedNode.getPath().substring(1));
			enabled = false;
			// showProgress = false;
			completed = true;
		}
		if (completed)
			return 100;

		return percentage;
		// }

		// return 100;
	}

	public void setCompletedPercentage(Integer i) {

		// return 100;
	}

	public void restoreFile() {
		try {
			// startProcess();
			showProgress = true;
			enabled = true;
			message = null;
			String filePath = null;
			System.out.println("restore path : " + restorePath);
			if (restorePath != null && !(restorePath.isEmpty())){
				if(FacesContext.getCurrentInstance().isPostback())
					return;
//				return;
			
				restorePath = StringUtils.decodeUrl(restorePath);
				String username = (String) getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
				selectedNode = fileExplorerService.getNodeByUUID(restorePath);
	
				if (selectedNode == null) {
					System.out.println("getNodeByUUID not found, going to search by path");
					selectedNode = fileExplorerService.getNodeByPath(restorePath);
				}
				ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
	
				if (selectedNode != null) {
					authorize = fileExplorerService.authorizeStub(restorePath, username);
	//				 authorize=true;
					if (!authorize) {
						String accessDeniedMessage = ru.getCodeValue(
								"accessDeniedMessage", SystemCodeType.GENERAL);

						addCompErrorMessage("searchForm:hiddenPercent",accessDeniedMessage, accessDeniedMessage);
						return;
					}
				}else {
					String fileNotFoundMessage = ru.getCodeValue(
							"documentNotFoundMessage", SystemCodeType.GENERAL);
 					addCompErrorMessage("searchForm:hiddenPercent",fileNotFoundMessage, fileNotFoundMessage);
					return;
				}
				// authorize = true;
	
				if (!authorize) {
					String accessDeniedMessage = ru.getCodeValue("accessDeniedMessage",
							SystemCodeType.GENERAL);
					addCompErrorMessage("searchForm:hiddenPercent",accessDeniedMessage, accessDeniedMessage);
					return;
				}
			}

			// restorePath=StringUtils.decodeUrl(restorePath);
//			if (restorePath != null && restorePath.trim().length() > 0)
//				filePath = restorePath; // will work only if stub_access_mode is
										// set to 3 (restore)

			if (selectedNode != null)
				filePath = selectedNode.getPath();

			if (filePath != null && filePath.indexOf("/") == 0) {
				filePath = filePath.substring(1);
			}
			if (restoreLocation != null && restoreLocation.trim().length() > 0) {
				restoreLocation = restoreLocation.replaceAll("\\\\", "/");
				if (restoreLocation.startsWith("//"))
					restoreLocation = restoreLocation.substring(2);
			}
			if(SmbWriter.isJobRunning(selectedNode.getPath().substring(1))){
				Integer percentage = SmbWriter.getCompletedPercentage(selectedNode.getPath().substring(1));
				if(percentage!=100){
					addCompInfoMessage("searchForm:hiddenPercent","File restore already in progress", "File restore already in progress");
					return;	//exit if the file is already being restored
				}
			}
			if(testUNC().equals("failure"))
					return;
			String uncPath=getUNCPath(filePath);
			if(restoreLocation==null || restoreLocation.length()<1){
//				String uncPath=getUNCPath(filePath);
				restoreLocation=uncPath.substring(2).replaceAll("\\\\", "/");
				restoreLocation = restoreLocation.substring(0, restoreLocation.lastIndexOf("/"));//remove filename
			}	
			
			//first check if the file already exists
			alreadyExists = false;
			alreadyExists = fileExplorerService.alreadyExistsForRestore(uncPath.substring(2).replaceAll("\\\\", "/"));
//			alreadyExists = true;
			if(alreadyExists){
				return;
			}
			
			String message = fileExplorerService.restoreFile(filePath,
					restoreLocation, false);
			addCompInfoMessage("searchForm:hiddenPercent","Restoring file \""+selectedNode.getName()+"\" at "+selectedNode.getCompressedURL()+" as background process", "Restoring file \""+selectedNode.getName()+"\" at "+selectedNode.getPath()+" as background process");
			// start=false;
		} catch (Exception ex) {
			ex.printStackTrace();
			// start=false;
		}
	}
	
	public void restoreFileAE() {
		try {
			// startProcess();
			showProgress = true;
			enabled = true;
			message = null;
			String filePath = null;
			System.out.println("restore path : " + restorePath);
			if (restorePath != null && !(restorePath.isEmpty())){
				if(FacesContext.getCurrentInstance().isPostback())
					return;
//				return;
			
				restorePath = StringUtils.decodeUrl(restorePath);
				String username = (String) getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
				selectedNode = fileExplorerService.getNodeByUUID(restorePath);
	
				if (selectedNode == null) {
					System.out.println("getNodeByUUID not found, going to search by path");
					selectedNode = fileExplorerService.getNodeByPath(restorePath);
				}
				ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
	
				if (selectedNode != null) {
					authorize = fileExplorerService.authorizeStub(restorePath, username);
	//				 authorize=true;
					if (!authorize) {
						String accessDeniedMessage = ru.getCodeValue(
								"accessDeniedMessage", SystemCodeType.GENERAL);

						addCompErrorMessage("searchForm:hiddenPercent",accessDeniedMessage, accessDeniedMessage);
						return;
					}
				}else {
					String fileNotFoundMessage = ru.getCodeValue(
							"documentNotFoundMessage", SystemCodeType.GENERAL);
 					addCompErrorMessage("searchForm:hiddenPercent",fileNotFoundMessage, fileNotFoundMessage);
					return;
				}
				// authorize = true;
	
				if (!authorize) {
					String accessDeniedMessage = ru.getCodeValue("accessDeniedMessage",
							SystemCodeType.GENERAL);
					addCompErrorMessage("searchForm:hiddenPercent",accessDeniedMessage, accessDeniedMessage);
					return;
				}
			}

			// restorePath=StringUtils.decodeUrl(restorePath);
//			if (restorePath != null && restorePath.trim().length() > 0)
//				filePath = restorePath; // will work only if stub_access_mode is
										// set to 3 (restore)

			if (selectedNode != null)
				filePath = selectedNode.getPath();

			if (filePath != null && filePath.indexOf("/") == 0) {
				filePath = filePath.substring(1);
			}
			if (restoreLocation != null && restoreLocation.trim().length() > 0) {
				restoreLocation = restoreLocation.replaceAll("\\\\", "/");
				if (restoreLocation.startsWith("//"))
					restoreLocation = restoreLocation.substring(2);
			}
			if(SmbWriter.isJobRunning(selectedNode.getPath().substring(1))){
				Integer percentage = SmbWriter.getCompletedPercentage(selectedNode.getPath().substring(1));
				if(percentage!=100){
					addCompInfoMessage("searchForm:hiddenPercent","File restore already in progress", "File restore already in progress");
					return;	//exit if the file is already being restored
				}
			}
			if(testUNC().equals("failure"))
					return;
			if(restoreLocation==null || restoreLocation.length()<1){
				String uncPath=getUNCPath(filePath);
				restoreLocation=uncPath.substring(2).replaceAll("\\\\", "/");
				restoreLocation = restoreLocation.substring(0, restoreLocation.lastIndexOf("/"));//remove filename
			}	
			
			String message = fileExplorerService.restoreFile(filePath,
					restoreLocation, false);
			addCompInfoMessage("searchForm:hiddenPercent","Restoring file \""+selectedNode.getName()+"\" at "+selectedNode.getCompressedURL()+" as background process", "Restoring file \""+selectedNode.getName()+"\" at "+selectedNode.getPath()+" as background process");
			// start=false;
		} catch (Exception ex) {
			ex.printStackTrace();
			// start=false;
		}
	}	
	
	 public String testUNC(){
		 ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
		 
		 String remoteUNC = "";//selectedNode.getPath();
		 String[] pathElements = com.virtualcode.util.StringUtils.getPathElements(selectedNode.getPath());
         for (int i = 0; i < pathElements.length; i++) {
             System.out.println("elements: "+pathElements[i]);
             if (i > 2) {
                 remoteUNC += pathElements[i] +"/";                 
             }
         }
         remoteUNC = remoteUNC.substring(0, remoteUNC.length()-1);
         remoteUNC = remoteUNC.substring(0, remoteUNC.lastIndexOf("/"));
		 
         DriveLetters driveLetter = null;
		 String remoteDomainName = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
		 String remoteUserName = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
		 String remoteUserPassword = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
		 
		 if(remoteDomainName==null || remoteDomainName.isEmpty()){
			 
				List<DriveLetters> driveLetterList=driveLettersService.getAll();

				for(DriveLetters letter: driveLetterList){
					String sharePath = letter.getSharePath().toLowerCase();
					if(sharePath.startsWith("\\\\")){
//						ResourcesUtil ru = ResourcesUtil.getResourcesUtil();				
						sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase() + "/fs/" + sharePath.substring(2).replaceAll("\\\\", "/");
					}
					if(sharePath.endsWith("/"))
						sharePath = sharePath.substring(0, sharePath.lastIndexOf("/"));
					String compareStr = remoteUNC.trim();
					if(!compareStr.contains("/fs/"))
						compareStr = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase()+"/fs/"+remoteUNC.trim().toLowerCase();
					System.out.println(compareStr+", " + sharePath);
					if(remoteUNC.trim()!=null && compareStr.contains(sharePath)){
						driveLetter = letter;
						break;
					}
				}
		 }
		 
		 if(driveLetter!=null){
			 remoteDomainName = driveLetter.getRestoreDomain();
			 remoteUserName = driveLetter.getRestoreUsername();
			 remoteUserPassword = driveLetter.getRestorePassword();
		 }

		 String testUNCResponse = "";
		 System.out.println("---------------------verifying UNC-------------------");
		 boolean error = false;
		 if(remoteUNC.trim().equals("") ||remoteUNC.trim().length()<3){// test as in existing app
			 addCompErrorMessage("searchForm:hiddenPercent", "UNC incorrect", "Invalid UNC Path");
//			 setTestUNCResponse("wrong");
			 error = true;
		 }else if(remoteUNC.trim().indexOf("\\\\", 2)!=-1){		 
			 addCompErrorMessage("searchForm:hiddenPercent", "UNC format incorrect(Only two \\\\ allowed in the start)", "UNC format incorrect(Only two \\\\ allowed in the start)");
			 error = true;
		 }else if(remoteUserPassword.trim().contains("@")){		 
			 addCompErrorMessage("searchForm:hiddenPercent", "Password cannot contain '@'","Password cannot contain '@'");
			 error = true;
		 }else{
			 String destPath = "";
			 destPath = VCSUtil.getSambaPath(remoteDomainName, remoteUserName, remoteUserPassword, remoteUNC.trim());
//			 if(remoteDomainName!=null && remoteDomainName.trim().length()<1)				 
//				 destPath = "smb://" + remoteDomainName + ";" + remoteUserName + ":" + remoteUserPassword + "@" + remoteUNC.substring(2,remoteUNC.length()).replaceAll("\\\\","/").trim() + "/";
//			 else
//				 destPath = "smb://"+ remoteUserName + ":" + remoteUserPassword + "@" + remoteUNC.substring(2,remoteUNC.length()).replaceAll("\\\\","/").trim() + "/";
			 
			 System.out.println("destination path: " + destPath);
			 try {
				SmbFile file = new SmbFile(destPath);
				if(file==null || !file.exists()){
					addCompErrorMessage("searchForm:hiddenPercent", "UNC path not found","UNC path not found");
					error = true;
				}else{
					System.out.println(file.list()); //throws exception access denied if user does not have permission
					//now to check if the user has write permissions on the directory
					SmbFile fileTemp = new SmbFile(destPath+"/sacheck.tmp");
					SmbFileOutputStream sfos = new SmbFileOutputStream(fileTemp);
					sfos.write("Testing write permissions".getBytes());
					sfos.flush();
					sfos.close();
					fileTemp.delete();	//delete after testing
					
//					addCompInfoMessage("searchForm:hiddenPercent", "UNC path verified","UNC path verified");
				}
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				addCompErrorMessage("searchForm:hiddenPercent","Error verifying credentials","Error verifying credentials");
				error = true;
			}catch(SmbAuthException ex){
                ex.printStackTrace();
                addCompErrorMessage("searchForm:hiddenPercent","Incorrect domain, username and/or password, or Access is denied","Incorrect domain, username and/or password, or Access is denied");
                error = true;
			}catch (SmbException e) {
				// TODO Auto-generated catch block
				addCompErrorMessage("searchForm:hiddenPercent","Server or Sharename not found","Server or Sharename not found");
				error = true;
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();				
			}			 
			 if(!error){
				 testUNCResponse = "success";
			 }else{
				 testUNCResponse = "failure";
			 }
			 
		 }
		 
		 return testUNCResponse;
	 }	
	 
	 public String getUNCPath(String path){
			String unc=null;
			if(path!=null){
				if(path.indexOf("/FS/")>0){
				  unc=path.substring(path.indexOf("/FS/")+4, path.length());
				}else if(path.indexOf("/SP/")>0){
					 unc=path.substring(path.indexOf("/SP/")+4, path.length());
			    }
			}
			if(unc!=null){
//				System.out.println("unc before replacing: "+unc);
				unc="\\\\"+unc.replaceAll("/", "\\\\");
			}
			return unc;
		}

	public String getJobMessage(String filePath) {

		String msg = "mailto://Jawwad.Muzaffar@sharearchiver.com";//SmbWriter.getJobStatusMessage(filePath);
//		if (msg == null || msg.trim().equals("")) {
//			return null;
//		}
//		System.out.println("Adding Message to View...****" + msg);
//		if (msg.indexOf("File restored successfully") != -1) {
////			addInfoMessage(msg, msg);
//			selectedNode.setCompressedURL(msg.substring(msg.indexOf("\\")));
//		} else {
//			addErrorMessage(msg, msg);
//		}
//		this.message = msg;
//		enabled = false;
		return msg;
	}
	
	public void emailUsingDefaultClient() {
		//mailto:lastname.firstname@xxx.com?subject=APPname%20support%20issue&amp;body=Version%20x.x%0D%0A%0D%0APlease%20make%20some%20descriptions%20here:%0D%0A%0D%0A%0D%0A&amp;attachment=C:\\resolv.conf
		String username = (String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		String dasUrl = ru.getCodeValue("DASUrl", "ZuesAgent");
		if(dasUrl==null || dasUrl.trim().length()<1)
			dasUrl = "http://sharearchiver:8080/ShareArchiver";
		String downloadLink = "";
		selectedNode.isMediaFile();
		if(linkOption.equals("1") || !showPdfIcon)
			downloadLink = dasUrl+"/downloadfile.jsp?nodeID="+ selectedNode.getDownloadUrl();
		else
			downloadLink = dasUrl+"/ViewFile.faces?nodeID="+ selectedNode.getDownloadUrl();
//		String mailTo = "mailto:?";//SmbWriter.getJobStatusMessage(filePath);
		
		String msg = "";//"subject=Download Link";
		msg += "Hi" + ",  \n \t" + username + " wants to share this document link with you. \n";
//		msg += "Please click <a href="+downloadLink+">here</a> to download the file :\n";
		msg += "Please click the following link to download the file: \n"+downloadLink+"\n";
		msg += "Comments from " + username + ":" +	"\n";
		/*msg ="<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" " +
				"xmlns:w=\"urn:schemas-microsoft-com:office:word\" xmlns:m=\"http://schemas.microsoft.com/office/2004/12/omml\" " +
				"xmlns=\"http://www.w3.org/TR/REC-html40\"><head><META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=us-ascii\">" +
				"<meta name=Generator content=\"Microsoft Word 14 (filtered medium)\">";/* +
				"<style><!--@font-face{font-family:Calibri;panose-1:2 15 5 2 2 2 4 3 2 4;}"
				+"				p.MsoNormal, li.MsoNormal, div.MsoNormal				 {margin:0in;				 margin-bottom:.0001pt;				 font-size:11.0pt;font-family:\"Calibri\",\"sans-serif\";}"
				+"a:link, span.MsoHyperlink"
				 +"{mso-style-priority:99;"
				 +"color:blue;"
				 +"text-decoration:underline;}"
				+"a:visited, span.MsoHyperlinkFollowed"
				 +"{mso-style-priority:99;"
				 +"color:purple;"
				 +"text-decoration:underline;}"
				+"pan.EmailStyle17"
				 +"{mso-style-type:personal-compose;"
				 +"font-family:\"Calibri\",\"sans-serif\";"
				 +"color:windowtext;}"
				+".MsoChpDefault"
				 +"{mso-style-type:export-only;"
				 +"font-family:\"Calibri\",\"sans-serif\";}"
				+"@page WordSection1"
				 +"{size:8.5in 11.0in;"
				 +"margin:1.0in 1.0in 1.0in 1.0in;}"
				+"div.WordSection1"
				 +"{page:WordSection1;}"
				+"--></style><!--[if gte mso 9]><xml>"
				+"<o:shapedefaults v:ext=\"edit\" spidmax=\"1026\" />"
				+"</xml><![endif]--><!--[if gte mso 9]><xml>"
				+"<o:shapelayout v:ext=\"edit\">"
				+"<o:idmap v:ext=\"edit\" data=\"1\" />"
				+"</o:shapelayout></xml><![endif]-->";
				msg +="</head><body lang=EN-US link=blue vlink=purple><div class=WordSection1><p class=MsoNormal><a href=\"https://support.mozilla.org/en-US/questions/958289\">hello</a><o:p></o:p></p></div></body></html>";
		*/
		//msg = mailTo + msg;
		System.out.println(msg);
//		if (msg == null || msg.trim().equals("")) {
//			return null;
//		}
//		System.out.println("Adding Message to View...****" + msg);
//		if (msg.indexOf("File restored successfully") != -1) {
////			addInfoMessage(msg, msg);
//			selectedNode.setCompressedURL(msg.substring(msg.indexOf("\\")));
//		} else {
//			addErrorMessage(msg, msg);
//		}
//		this.message = msg;
//		enabled = false;
//		try {
		emailString = msg;
//			getHttpResponse().sendRedirect(msg);
//			return;
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return msg;
	}

	public void emailDocumentLink() {
		if(!VCSUtil.validateEmailAddress(receiverEmailAddressLink)){
			addCompErrorMessage("searchForm:hiddenPercent","Invalid email address", "Invalid email address");
			return;
		}

		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		String dasUrl = ru.getCodeValue("DASUrl", "ZuesAgent");
		if(dasUrl==null || dasUrl.trim().length()<1)
			dasUrl = "http://sharearchiver:8080/ShareArchiver";
		String downloadLink = "";
		String enableSecurityString = ru.getCodeValue("enable_file_sharing_security", SystemCodeType.GENERAL);
		boolean enableSecurity = false;
		if(enableSecurityString!=null)
			enableSecurity = enableSecurityString.equals("yes")?true:false;
//		if(enableSecurity){
//			downloadLink = dasUrl+"/EmailUserLogin.faces?nodeID="+ selectedNode.getDownloadUrl();
//		}else{
			if(linkOption.equals("1") || !showPdfIcon)
				downloadLink = dasUrl+"/downloadfile.jsp?nodeID="+ selectedNode.getDownloadUrl();
			else
				downloadLink = dasUrl+"/ViewFile.faces?nodeID="+ selectedNode.getDownloadUrl();
//		}
		
//		receiverName =  "Bilal";
//		receiverEmailAddress = "jawwad.muzaffar@sharearchiver.com";
//		emailComments = "for your refference";
		String username = (String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
		if(username !=null && username.length()>0){			
			username = username.substring(0,1).toUpperCase() + username.substring(1);		
		} 
		
		String emailBody = "Dear " + receiverNameLink + ", ";
		emailBody += "<br><br>";// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		emailBody += username + " wants to share this document link with you.<br>";
		emailBody += "Please click <a href="+downloadLink+">here</a> to gain access to the document \""+ selectedNode.getName()+"\"";
		if(enableSecurity){
			emailBody += "You will be required to use your email ID and a password, the password will be sent to you shortly.";
		}else{
//			emailBody += "<br><br>";// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
//			emailBody += username + " wants to share this document link with you.<br>";
//			emailBody += "Please click <a href="+downloadLink+">here</a> to gain access to the document \""+ selectedNode.getName()+"\"";			
//			emailBody += "<br><br>";
//			emailBody += "Comments from " + username + ":<br>" +
//					"<br>" + emailCommentsLink;
		}
		emailBody += "<br><br>";
		emailBody += "Comments from " + username + ":<br>" +
				"<br>" + emailCommentsLink;
		
		emailBody+="<br/><br/>Thanks<br/><br/>";
		 if(ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY)!=null)
			 emailBody += ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY) + " ";
		 emailBody += "File Share Service";
		
		if(emailSubjectLink==null || emailSubjectLink.length()<1)
			emailSubjectLink = "Shared Link";
		
		boolean status = false;
		
		if(emulateEmailSender){
			JespaUtil util = JespaUtil.getInstance();
			String emailFrom = util.getUserEmail(username);
			if(emailFrom!=null && emailFrom.length()>0)
				status=mailService.sendMail(emailFrom, this.receiverEmailAddressLink, emailSubjectLink, emailBody,null, null);
			else
				status=mailService.sendMail(this.receiverEmailAddressLink, emailSubjectLink, emailBody,null, null);
		}else{
			status = mailService.sendMail(receiverEmailAddressLink, emailSubjectLink, emailBody, null, null);
		}
		
		if(status){
			addCompInfoMessage("searchForm:hiddenPercent","Email Sent Successfully", "Email Sent Successfully!");
			boolean userExists = false;
			 AdvancedSecureURL advSecureUrl = AdvancedSecureURL.getInstance();
			 LinkAccessUsers user = linkAccessUsersService.getLinkAccessUsersByRecipient(receiverEmailAddressLink);
//			 if(user!=null && !(user.getSender().equals(username))){	// if the recipient is same but sender is different. 
//				 user=null;
//			 }
			 String password = "";
			 if(user==null){
				 user = new LinkAccessUsers();
				 user.setRecipient(receiverEmailAddressLink);
				 password = PasswordGenerator.randomAlphaNumeric();
				 
				 try {
//					advSecureUrl.getEncryptedURL(password);
					user.setPassword(advSecureUrl.getEncryptedURL(password));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 
//				 user.setSender(username);
				 linkAccessUsersService.saveLinkAccessUsers(user);				 
			 }else{
				 userExists = true;
				 try {
					password = advSecureUrl.getDecryptedURL(user.getPassword());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			 }
			 LinkAccessDocuments document = null;
			 if(user.getId()!=null){
				 document = linkAccessDocumentsService.getLinkAccessDocumentsByUuidAndUser(user.getId()+"", selectedNode.getUuid());//modifcation required to select on basis of path and email
			 }
			 
			 if(document!=null && !document.getSender().equalsIgnoreCase(username)){ //if the document and the sender is different
				 document=null;
			 }
			 
			 if(document==null){
				 document = new LinkAccessDocuments();
				 document.setUuidDocuments(selectedNode.getUuid());
				 document.setLinkAccessUsers(user);
				 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				 document.setSharedOn(timestamp);
				 Calendar c = Calendar.getInstance();
				 c.setTime(new Date(timestamp.getTime()));
				 int noOfDays = 7;
				 if(ru.getCodeValue("shared_link_validity", SystemCodeType.GENERAL)!=null){
					 noOfDays = new Integer(ru.getCodeValue("shared_link_validity", SystemCodeType.GENERAL));
				 }
				 if(noOfDays==0){
					 noOfDays=10000;
					 document.setLinkExpireDate(null);
				 }else{
					 c.add(Calendar.DATE, noOfDays);
					 document.setLinkExpireDate(new Timestamp(c.getTimeInMillis()));
				 }
				 document.setSender(username);				 
				 linkAccessDocumentsService.saveLinkAccessDocuments(document);				 
			 }else{
				 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				 document.setSharedOn(timestamp);
				 Calendar c = Calendar.getInstance();
				 c.setTime(new Date(timestamp.getTime()));
				 int noOfDays = 7;
				 if(ru.getCodeValue("shared_link_validity", SystemCodeType.GENERAL)!=null){
					 noOfDays = new Integer(ru.getCodeValue("shared_link_validity", SystemCodeType.GENERAL));
				 }
				 if(noOfDays==0){
					 noOfDays=10000;
					 document.setLinkExpireDate(null);
				 }else{
					 c.add(Calendar.DATE, noOfDays);
					 document.setLinkExpireDate(new Timestamp(c.getTimeInMillis()));
				 }
				 linkAccessDocumentsService.saveLinkAccessDocuments(document);
			 }
			 
			 
			 
			 
			 if(!userExists){
//				 mailService.sendMail(this.receiverEmailAddressLink, "ShareArchiver User Credentials", "Your ShareArchiver password is: " + password,null, null);
				 emailBody = "Dear user <br/><br/>";
				 emailBody += "A file has been shared with you, a link was sent to you in a separate email. <br/>";
				 emailBody += "Since you are using this service for the first time, a password has been generated for you as \""+password+"\".<br/>";
				 emailBody += "You may use your account details and access the file. you may also update the password in the user preferences area.<br/>";
				 emailBody += "Thank You <br/><br/>";
				 if(ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY)!=null)
					 emailBody += ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY) + " ";
				 emailBody += "File Share Service";

				 mailService.sendMail(this.receiverEmailAddressLink, "File Access Password", emailBody,null, null);
			 }
			 
			 document.setLinkAccessUsers(user);
			 
			 //send notification to Data gaurdian
//			 if(groupGuardian!=null && groupGuardian.getGroupGuardian()!= null && groupGuardian.getGroupGuardian().length()>0)
//			 {
//		    		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
////		    		String emailBody = "";
//		    		emailBody += "File Name: " + selectedNode.getName() + "<br/>";
//		    		emailBody += "Document ID: " + selectedNode.getUuid() + "<br/><br/>";
//		    		emailBody += "Shared by user ID "+ username +" on Date:" + df.format(new Date()) +"<br/><br/>";
////		    		emailBody += "has been accessed by the recipient on Date :"+ df.format(new Date()) +"<br/><br/>";
//		    		emailBody += "Thank You";
//					 if(ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY)!=null)
//						 emailBody += ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY) + " ";
//					 emailBody += "File Share Service";
//				 mailService.sendMail(groupGuardian.getGroupGuardian(), "File Sharing Notification", emailBody,null, null);
//			 }
			//send notification to Data gaurdian(s)
			 List<DataGuardian> dataGuardianList = dataGuardianService.getAll();
			 for(DataGuardian dataGuardian: dataGuardianList){
				 
			
//			 if(groupGuardian!=null && groupGuardian.getEnableTranscript()==1 && groupGuardian.getGroupGuardian()!= null && groupGuardian.getGroupGuardian().length()>0)
//			 {
		    		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		    		emailBody = "";
		    		emailBody += "A document has been shared with user :"+ receiverEmailAddressLink;
		    		emailBody += "Shared on: " +  df.format(new Date());
		    		emailBody += "File Name: " + selectedNode.getName() + "<br/>";
		    		emailBody += "File UUID: " + selectedNode.getUuid() + "<br/>";
		    		emailBody += "File Path: " + selectedNode.getPath() + "<br/><br/>";
		    		emailBody += "Shared By: " + username + "<br/><br/>";
//		    		emailBody += "Shared by user ID "+ username +" on Date:" + df.format(new Date()) +"<br/><br/>";
//		    		emailBody += "has been accessed by the recipient on Date :"+ df.format(new Date()) +"<br/><br/>";
		    		emailBody += "Thank You <br/><br/>";
					 if(ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY)!=null)
						 emailBody += ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY) + " ";
					 emailBody += "File Share Service";
					 String fromEmail = ru.getCodeValue("guardian_from_address", SystemCodeType.GENERAL);
					 
					 if(fromEmail==null || fromEmail.length()<1)
						 mailService.sendMail(dataGuardian.getDataGuardianEmail(), "File Sharing Notification", emailBody,null, null);
					 else
						 mailService.sendMail(fromEmail, dataGuardian.getDataGuardianEmail(), "File Sharing Notification", emailBody,null, null);
//			 }
			 }			 
		}
		else{
			addCompErrorMessage("searchForm:hiddenPercent","Email sending failed to ["+ receiverEmailAddressLink+"]", "Email sending failed to ["+ receiverEmailAddressLink+"]");
		}
	}
	
	public void emailDocument() {

		if(!VCSUtil.validateEmailAddress(receiverEmailAddress)){
			addCompErrorMessage("searchForm:hiddenPercent","Invalid email address", "Invalid email address");
			return;
		}
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		String dasUrl = ru.getCodeValue("DASUrl", "ZuesAgent");
		if(dasUrl==null || dasUrl.trim().length()<1)
			dasUrl = "http://sharearchiver:8080/ShareArchiver";
		String sizeString = ru.getCodeValue("max_attachment_size", "General");
		Integer maxAttachmentSize = 10;
		if(sizeString!=null)
			maxAttachmentSize = new Integer(sizeString);
		System.out.println(maxAttachmentSize*1024*1024 + ", " +new Integer(selectedNode.getSizeinKbs()));
		System.out.println(maxAttachmentSize*1024*1024 < new Integer(selectedNode.getSizeinKbs()));
		if(new Integer(selectedNode.getSizeinKbs()) > maxAttachmentSize*1024*1024){
			addCompErrorMessage("searchForm:hiddenPercent","File attachment cannot be greater than [" + maxAttachmentSize +"] MBs", "File attachment cannot be greater than [" + maxAttachmentSize +"] MBs");
			return;
		}
//		receiverName =  "Bilal";
//		receiverEmailAddress = "bilal.mujtaba@sharearchiver.com";
//		emailComments = "ye document ap ka hua";
		String username = (String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
		String emailBody = "Dear " + receiverName + ", ";
		emailBody += "<br><br>";// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		emailBody += username + " wants to share the attached document with you.<br>";
		emailBody += "<br>";
		emailBody += "Comments from " + username + ":<br>" +
				"<br>" + emailComments;
		emailBody+="<br/><br/>Thanks<br/><br/>";
		 if(ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY)!=null)
			 emailBody += ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY) + " ";
		 emailBody += "File Share Service";
//		System.out.println(emailBody);
		if(emailSubject==null || emailSubject.length()<1)
			emailSubject = "Shared File";
//		String downloadLink = dasUrl+"/downloadfile.jsp?nodeID="+ selectedNode.getDownloadUrl();
		if(emulateEmailSender){
			JespaUtil util = JespaUtil.getInstance();
			String emailFrom = util.getUserEmail(username);
			if(emailFrom!=null && emailFrom.length()>0)
//				status=mailService.sendMail(emailFrom, this.receiverEmailAddressLink, emailSubjectLink, messagebody,null, null);
				generateEmail(emailFrom, receiverEmailAddress, emailSubject,emailBody, selectedNode.getName(), selectedNode.getPath());
			else
				generateEmail(null, receiverEmailAddress, emailSubject,emailBody, selectedNode.getName(), selectedNode.getPath());
		}else{
			generateEmail(null, receiverEmailAddress, emailSubject,emailBody, selectedNode.getName(), selectedNode.getPath());
		}

	}
	
	private void generateEmail(String emailFrom, String emailTo,String subject,String msgbody,String fileName,String filePath){
		 
		 boolean status = false;
        String fileExt=fileName.substring(fileName.lastIndexOf("."));
        int fileNumber=VCSUtil.getAbsolute(filePath.hashCode());
        javax.jcr.Binary binaryStream =fileExplorerService.downloadFile(filePath);
        String sessionId=getHttpSession().getId();
        try { 
           if(binaryStream!=null){
              InputStream is=binaryStream.getStream();
              byte[] buffer = new byte[4096];
              int bit = 256;
              
              File outputDir=new File(getContextRealPath()+"/streams/"+sessionId);
              if(!outputDir.exists()){
           	   outputDir.mkdirs();
              }
              File outputFile=new File(getContextRealPath()+"/streams/"+sessionId+"/"+fileNumber+fileExt);
                
           	 if (!outputFile.exists()) {
              	    outputFile.createNewFile();
              	   
	               	FileWriter fw = new FileWriter(outputFile.getAbsoluteFile());
	           	    BufferedOutputStream buffOut = new BufferedOutputStream(new FileOutputStream(outputFile));
	               
	           	    while ((bit = is.read(buffer, 0, 4096)) != -1) {
	               	    buffOut.write(buffer,0,bit);	              
	                }
	                buffOut.flush();
	                buffOut.close();
	                fw.flush();
	                fw.close();
   			 }  
           	 if(emailFrom!=null)
           		 status = mailService.sendMail(emailFrom, emailTo, subject, msgbody,fileName, outputFile);
           	 else
           		 status =  mailService.sendMail(emailTo, subject, msgbody,fileName, outputFile);
            if(!outputFile.exists())
            {
            	outputFile.delete();
            }
           }
          } catch (Exception ioe) {
       	   System.out.println("io excpetion: " + ioe.getMessage());
       	   addCompErrorMessage("searchForm:hiddenPercent","Email sending failed, see error logs for details", "Email sending failed, see error logs for details");
          }
        
		if(status){
			addCompInfoMessage("searchForm:hiddenPercent","Email Sent Successfully", "Email Sent Successfully");
		}else{
			addCompErrorMessage("searchForm:hiddenPercent","Email sending failed", "Email sending failed");
		}
    }	
	
	public void playMediaFile(){
		 mediaFilePath=null;
		 String fp=selectedNode.getPath();
        String fileName=fp.substring(fp.lastIndexOf("/")+1);
        String fileExt=fileName.substring(fileName.lastIndexOf("."));
        int fileNumber=VCSUtil.getAbsolute(fp.hashCode());
        javax.jcr.Binary binaryStream =fileExplorerService.downloadFile(fp);
        String sessionId=getHttpSession().getId();
        try { 
           if(binaryStream!=null){
              InputStream is=binaryStream.getStream();
              byte[] buffer = new byte[4096];
              int bit = 256;
              
              File outputDir=new File(getContextRealPath()+"/streams/"+sessionId);
              if(!outputDir.exists()){
           	   outputDir.mkdirs();
              }
              File outputFile=new File(getContextRealPath()+"/streams/"+sessionId+"/"+fileNumber+fileExt);
                
           	 if (!outputFile.exists()) {
              	    outputFile.createNewFile();
              	   
	               	FileWriter fw = new FileWriter(outputFile.getAbsoluteFile());
	           	    BufferedOutputStream buffOut = new BufferedOutputStream(new FileOutputStream(outputFile));
	               
	           	    while ((bit = is.read(buffer, 0, 4096)) != -1) {
	               	    buffOut.write(buffer,0,bit);	              
	                }
	                buffOut.flush();
	                buffOut.close();
	                fw.flush();
	                fw.close();
   			 }  
           	 
                mediaFilePath="streams/"+sessionId+"/"+fileNumber+fileExt;
           }
          } catch (Exception ioe) {
       	   System.out.println("io excpetion 1 " + ioe.getMessage());
          }
         
    }
	
	public void updateTag(){
		   
		   String username=(String)getHttpRequest().getSession().getAttribute(VCSConstants.SESSION_USER_NAME);
	   		Date date = VCSUtil.getCurrentDateTime();
	       java.sql.Timestamp timestamp = new Timestamp(date.getTime());

		   String tags = fileExplorerService.updateTags(selectedNode.getUuid(), username +"|"+ timestamp +"|"+selectedNode.getTags());
		   if(tags!=null){
			   selectedNode.setTagUser(username);
			   selectedNode.setTags(selectedNode.getTags());
			   selectedNode.setTagTimeStamp(timestamp+"");
		   }

	   }
	
	 private boolean isLdapUser(){
		   String authMode=(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE);
		  if(authMode.equalsIgnoreCase("ad"))
			  return true;
		  else
		  return false;
	 } 
	
	

	public String getLoggedInRole(){
		   String roleName=(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_ROLE_NAME);
		   return roleName.toUpperCase();
    }
	private String getUserRole(){
		 String loggedInRole=getLoggedInRole();
		 if(loggedInRole.equalsIgnoreCase("ADMINISTRATOR")){
			 return loggedInRole;
		 }else{
			 String authMode=(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE);
			  if(authMode.equalsIgnoreCase("ad")){
				  return "LDAP";
			  }else{
				  return "PRIV";
			  }				  
		 }
		
	 }
	
	private void activeDeActivateUserControls(){
	    RoleFeatureUtil rfUtil=RoleFeatureUtil.getRoleFeatureUtil();   
	    RoleFeature roleFeature;
    	String userRole=getUserRole();
      
    	if(userRole.equalsIgnoreCase("PRIV")){
    		 
    		 roleFeature=rfUtil.getFeature("email_link_sp");
	   		 this.linksShareEnabled=roleFeature!=null ?roleFeature.getPrivAllowed():false;
	   		 
	   		 roleFeature=rfUtil.getFeature("email_doc_sp");
	   		 this.filesShareEnabled=roleFeature!=null ?roleFeature.getPrivAllowed():false;
	   		 
	   		 roleFeature=rfUtil.getFeature("download_sp");
	   		 this.downloadEnabled=roleFeature!=null ?roleFeature.getPrivAllowed():false;
	   		 
	   		 roleFeature=rfUtil.getFeature("restore_file_sp");
    		 this.fileRestoreEanbled=roleFeature!=null ?roleFeature.getPrivAllowed():false;
    		 
	   		 roleFeature=rfUtil.getFeature("media_streaming_sp");
	   		 this.mediaStreamingEnabled=roleFeature!=null ?roleFeature.getPrivAllowed():false;
	   		 
	   		 roleFeature=rfUtil.getFeature("document_tagging");
	   		 this.enableDocumentTagging=roleFeature!=null ?roleFeature.getPrivAllowed():false;
	   		 
	   		 roleFeature=rfUtil.getFeature("document_viewing_sp");
	   		 this.enablePdfView=roleFeature!=null ?roleFeature.getPrivAllowed():false;
	   		 
    		 roleFeature=rfUtil.getFeature("show_document_url");
    		 this.showDocumentUrl=roleFeature!=null ?roleFeature.getPrivAllowed():false;
	   		 
	   		 
    	}else if(userRole.equalsIgnoreCase("LDAP")){
    		
    		 roleFeature=rfUtil.getFeature("email_link_sp");
	   		 this.linksShareEnabled=roleFeature!=null ?roleFeature.getLdapAllowed():false;
	   		 
	   		 roleFeature=rfUtil.getFeature("email_doc_sp");
	   		 this.filesShareEnabled=roleFeature!=null ?roleFeature.getLdapAllowed():false;
	   		 
	   		 roleFeature=rfUtil.getFeature("download_sp");
	   		 this.downloadEnabled=roleFeature!=null ?roleFeature.getLdapAllowed():false;
	   		 
	   		 roleFeature=rfUtil.getFeature("restore_file_sp");
    		 this.fileRestoreEanbled=roleFeature!=null ?roleFeature.getLdapAllowed():false;
    		 
    		
	   		 roleFeature=rfUtil.getFeature("media_streaming_sp");
	   		 this.mediaStreamingEnabled=roleFeature!=null ?roleFeature.getLdapAllowed():false;
	   		 
	   		 roleFeature=rfUtil.getFeature("document_tagging");
	   		 this.enableDocumentTagging=roleFeature!=null ?roleFeature.getLdapAllowed():false;
	   		 
		   	roleFeature=rfUtil.getFeature("document_viewing_sp");
		   	this.enablePdfView=roleFeature!=null ?roleFeature.getLdapAllowed():false;
		   	
	   		 roleFeature=rfUtil.getFeature("show_document_url");
	   		 this.showDocumentUrl=roleFeature!=null ?roleFeature.getLdapAllowed():false;
	   		 
    	}else{
    		 this.linksShareEnabled=false;
    		 this.filesShareEnabled=false;
    		 this.downloadEnabled=false;
    		 this.fileRestoreEanbled=false;    		
    		 this.mediaStreamingEnabled=false;   
    		 this.enableDocumentTagging=false;
    		 this.enablePdfView = false;
    	}
    	
    	ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
    	if(ru.getCodeValue("enable_default_email_client", SystemCodeType.GENERAL)!=null)
    		emailUsingDefaultClient = ru.getCodeValue("enable_default_email_client", SystemCodeType.GENERAL).equals("yes")?true:false;
    	if(ru.getCodeValue("emulate_email_sender", SystemCodeType.GENERAL)!=null)
    		emulateEmailSender = ru.getCodeValue("emulate_email_sender", SystemCodeType.GENERAL).equals("yes")?true:false;   
    	
    	String authType = (String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE);
    	if(authType.equals("ad")){
    		if(ru.getCodeValue("restrict_cifs_files_sharing_ad", SystemCodeType.GENERAL).equals("yes")){
    			
	    		
		    	JespaUtil util = JespaUtil.getInstance();
		    	boolean exists = false;
		    	try{
		    		List<String> userGroupList = util.getGroupsForUser((String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME));
					System.out.println(util.getGroupsForUser((String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME)));
//					GroupGuardian groupGuardian = null;
					for(String group:userGroupList){
						System.out.println("Group Name" + group);
						List<LdapGroups> ldapGroupList = ldapGroupsService.getLdapGroupByGroupName(group);
//						List<GroupGuardian> groupGuardians =  groupGuardianService.getGroupGuardianByGroupName(group);
						if(ldapGroupList!=null && ldapGroupList.size()>0){
//							groupGuardian = ldapGroupList.get(0);
							exists = true;
							break;
						}
					}
					
					if(!exists){
//						if(groupGuardian!=null && groupGuardian.getRestrictGroup()==0){
//							enableCIFSFileSharing = true;
//							linksShareEnabled = true;
//						}else{
//							enableCIFSFileSharing = false;
							if(ru.getCodeValue("restrict_archive_files_sharing_ad", SystemCodeType.GENERAL).equals("yes"))
								linksShareEnabled = false;
//						}
					}
					
	//	    		System.out.println(util.getGroupsForUser("bilal"));
				} catch (SecurityProviderException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    	}    	
	 
    }
	

	
	

	
	private boolean videoStreamingEnabled=false;
	
	private boolean imageFile = false;
	
	private String currentThumbnailPath;
	
	private String linkOption = "1"; 	// 1 =  download link, 2 = view document link
    

	public boolean isVideoStreamingEnabled() {
		return videoStreamingEnabled;
	}

	public void setVideoStreamingEnabled(boolean videoStreamingEnabled) {
		this.videoStreamingEnabled = videoStreamingEnabled;
	}

	public String getNodeID() {
		return nodeID;
	}

	public void setNodeID(String nodeID) {
		this.nodeID = nodeID;
	}

	public CustomTreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(CustomTreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public boolean isAuthorize() {
		return authorize;
	}

	public void setAuthorize(boolean authorize) {
		this.authorize = authorize;
	}

	public boolean isButtonRendered() {
		return buttonRendered;
	}

	public void setButtonRendered(boolean buttonRendered) {
		this.buttonRendered = buttonRendered;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isShowProgress() {
		return showProgress;
	}

	public void setShowProgress(boolean showProgress) {
		this.showProgress = showProgress;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	public String getRestoreLocation() {
		return restoreLocation;
	}

	public void setRestoreLocation(String restoreLocation) {
		this.restoreLocation = restoreLocation;
	}

	public String getRestorePath() {
		return restorePath;
	}

	public void setRestorePath(String restorePath) {
		this.restorePath = restorePath;
	}

	public String getReceiverEmailAddress() {
		return receiverEmailAddress;
	}

	public void setReceiverEmailAddress(String receiverEmailAddress) {
		this.receiverEmailAddress = receiverEmailAddress;
	}
	
	public String getMediaFilePath() {
		return mediaFilePath;
	}
    
	public void setMediaFilePath(String mediaFilePath) {
		this.mediaFilePath = mediaFilePath;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getEmailComments() {
		return emailComments;
	}

	public void setEmailComments(String emailComments) {
		this.emailComments = emailComments;
	}

	public String getReceiverEmailAddressLink() {
		return receiverEmailAddressLink;
	}

	public void setReceiverEmailAddressLink(String receiverEmailAddressLink) {
		this.receiverEmailAddressLink = receiverEmailAddressLink;
	}

	public String getEmailCommentsLink() {
		return emailCommentsLink;
	}

	public void setEmailCommentsLink(String emailCommentsLink) {
		this.emailCommentsLink = emailCommentsLink;
	}

	public String getMediaFilePathLink() {
		return mediaFilePathLink;
	}

	public void setMediaFilePathLink(String mediaFilePathLink) {
		this.mediaFilePathLink = mediaFilePathLink;
	}

	public String getReceiverNameLink() {
		return receiverNameLink;
	}

	public void setReceiverNameLink(String receiverNameLink) {
		this.receiverNameLink = receiverNameLink;
	}

	public boolean isFilesShareEnabled() {
		return filesShareEnabled;
	}

	public boolean isLinksShareEnabled() {
		return linksShareEnabled;
	}

	
	public boolean isDownloadEnabled() {
		return downloadEnabled;
	}

	public boolean isFileRestoreEanbled() {
		return fileRestoreEanbled;
	}

	public boolean isMediaStreamingEnabled() {
		return mediaStreamingEnabled;
	}
	
	

	public boolean isImageFile() {
		return imageFile;
	}

	public void setImageFile(boolean imageFile) {
		this.imageFile = imageFile;
	}

	public String getCurrentThumbnailPath() {
		return currentThumbnailPath;
	}

	public void setCurrentThumbnailPath(String currentThumbnailPath) {
		this.currentThumbnailPath = currentThumbnailPath;
	}

	public boolean isEnablePdfView() {
		return enablePdfView;
	}

	public void setEnablePdfView(boolean enablePdfView) {
		this.enablePdfView = enablePdfView;
	}

	public boolean isShowFrame() {
		return showFrame;
	}

	public void setShowFrame(boolean showFrame) {
		this.showFrame = showFrame;
	}

	public boolean isShowPdfIcon() {
		return showPdfIcon;
	}

	public void setShowPdfIcon(boolean showPdfIcon) {
		this.showPdfIcon = showPdfIcon;
	}

	public DocumentManager getDocumentManager() {
		return documentManager;
	}

	public void setDocumentManager(DocumentManager documentManager) {
		this.documentManager = documentManager;
	}

	public String getUrlForPdf() {
		return urlForPdf;
	}

	public void setUrlForPdf(String urlForPdf) {
		this.urlForPdf = urlForPdf;
	}

	public String getEmailString() {
		return emailString;
	}

	public void setEmailString(String emailString) {
		this.emailString = emailString;
	}

	public boolean isEmailUsingDefaultClient() {
		return emailUsingDefaultClient;
	}

	public void setEmailUsingDefaultClient(boolean emailUsingDefaultClient) {
		this.emailUsingDefaultClient = emailUsingDefaultClient;
	}

	public String getLinkOption() {
		return linkOption;
	}

	public void setLinkOption(String linkOption) {
		this.linkOption = linkOption;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getEmailSubjectLink() {
		return emailSubjectLink;
	}

	public void setEmailSubjectLink(String emailSubjectLink) {
		this.emailSubjectLink = emailSubjectLink;
	}

	public boolean isEmailFileSizeExceeded() {
		return emailFileSizeExceeded;
	}

	public void setEmailFileSizeExceeded(boolean emailFileSizeExceeded) {
		this.emailFileSizeExceeded = emailFileSizeExceeded;
	}

	public boolean isEmulateEmailSender() {
		return emulateEmailSender;
	}

	public void setEmulateEmailSender(boolean emulateEmailSender) {
		this.emulateEmailSender = emulateEmailSender;
	}

	public boolean isShowDocumentUrl() {
		return showDocumentUrl;
	}

	public void setShowDocumentUrl(boolean showDocumentUrl) {
		this.showDocumentUrl = showDocumentUrl;
	}

	public String getDnsServer() {
		return dnsServer;
	}

	public void setDnsServer(String dnsServer) {
		this.dnsServer = dnsServer;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public boolean isAlreadyExists() {
		return alreadyExists;
	}

	public void setAlreadyExists(boolean alreadyExists) {
		this.alreadyExists = alreadyExists;
	}
}
