package com.virtualcode.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UISelectMany;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;

import com.virtualcode.util.JespaUtil;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.DataGuardian;
import com.virtualcode.vo.DocumentCategory;
import com.virtualcode.vo.DocumentType;
import com.virtualcode.vo.LdapGroups;
import com.virtualcode.vo.SystemCode;

@ManagedBean(name="ldapGroupsController")
@ViewScoped
public class LdapGroupsController extends AbstractController implements Serializable {
	
	public LdapGroupsController(){
		super();
		System.out.println("LdapGroupsController()");
//		documentTypeList=new ArrayList<DocumentType>();
//		documentCategory=new DocumentCategory();
//		documentType=new DocumentType();
		ldapGroup = new LdapGroups();
		ldapGroupsList = new ArrayList<LdapGroups>();
		ldapGroupsList = ldapGroupsService.getAll();
		ldapGroupList = new ArrayList<String>();
		
		JespaUtil util = JespaUtil.getInstance();
        Map<String, String> groups = util.getGroups();
        Set<String> keySet = groups.keySet();
        
        for(String key:keySet){
        	boolean exists = false; 
        	for(LdapGroups obj:ldapGroupsList){
        		if(groups.get(key).equals(obj.getGroupName())){
        			exists = true;
        			break;
        		}
        	}
        	if(!exists)        		
        		ldapGroupList.add(groups.get(key));
        }
        
        List<SystemCode> systemCodeList = systemCodeService.getSystemCodeByCodename("enable_cifs_file_sharing");
        
        SystemCode systemCode = null;
        
        if(systemCodeList!=null && systemCodeList.size()>0)
        	systemCode = systemCodeList.get(0);
        
//    	if(systemCode!= null && systemCode.getCodename().equals("enable_cifs_file_sharing"))
//    		enableCifsFileSharing = systemCode.getCodevalue().equals("yes")?true:false;
    	dataGuardian = new DataGuardian();
    	dataGuardianList = dataGuardianService.getAll();
		
	}
	
	 public void removeLdapGroup(){
		 UIViewRoot root = FacesContext.getCurrentInstance().getViewRoot();
		 UIComponent comp = root.findComponent("globalConfigForm:ldapGroupListBox");
		 UISelectMany listBox=(UISelectMany)comp;
		 Object[] selectedValues=listBox.getSelectedValues();		
		 String value;
		 for(int i=0;selectedValues!=null && i<selectedValues.length;i++){
			  value=(String)selectedValues[i];			  
			  LdapGroups ldapGroup = null;
//			  List<LdapGroups> groupList = ldapGroupsService.getLdapGroupByGroupName(value);
//			  if(groupList!=null && groupList.size()>0)
//				  ldapGroup = groupList.get(0);
			  for(LdapGroups group:ldapGroupsList){
				  if(group.getGroupName().equals(value)){
					  ldapGroup = group;
//					  ldapGroupsList.remove(group);
				  }
			  }
			  ldapGroupsList.remove(ldapGroup);
			  ldapGroupsService.deleteLdapGroup(ldapGroup);
			  ldapGroupList.add(value);
		 }
//		 for(LdapGroups group:ldapGroupsList)
//			 System.out.println(group.getGroupName());
	 }
	

	 public void removeDataGuardian(){
		 UIViewRoot root = FacesContext.getCurrentInstance().getViewRoot();
		 UIComponent comp = root.findComponent("globalConfigForm:dataGuardianListBox");
		 UISelectMany listBox=(UISelectMany)comp;
		 Object[] selectedValues=listBox.getSelectedValues();		
		 String value;
		 for(int i=0;selectedValues!=null && i<selectedValues.length;i++){
			  value=(String)selectedValues[i];			  
			  DataGuardian dataGuardian = null;
//			  List<LdapGroups> groupList = ldapGroupsService.getLdapGroupByGroupName(value);
//			  if(groupList!=null && groupList.size()>0)
//				  ldapGroup = groupList.get(0);
			  for(DataGuardian guardian:dataGuardianList){
				  if(guardian.getDataGuardianEmail().equals(value)){
					  dataGuardian = guardian;
//					  ldapGroupsList.remove(group);
				  }
			  }
			  dataGuardianList.remove(dataGuardian);
			  dataGuardianService.deleteDataGuardian(dataGuardian);
//			  ldapGroupsService.deleteLdapGroup(ldapGroup);
//			  ldapGroupList.add(value);
		 }
//		 for(LdapGroups group:ldapGroupsList)
//			 System.out.println(group.getGroupName());
	 }
	 
	private Integer ldapGroupId;
	
//	private Integer documentCategoryId;
	
//    private DocumentCategory documentCategory;
    
    private LdapGroups ldapGroup;
	
//	private DocumentType documentType;
	
	private List<LdapGroups> ldapGroupsList;
	
	private List<Integer> removedDocTypesList=Collections.synchronizedList(new ArrayList<Integer>());
	
	private List<String> ldapGroupList; 
	
	private boolean restrictGroup;
	
	private boolean enableTranscript;
	
	private boolean enableCifsFileSharing =  false;
	
	private DataGuardian dataGuardian;
	
	private List<DataGuardian> dataGuardianList;
	
	public void addGroupToList(){
		
		if(groupExists()){
			addInfoMessage("Group Name ["+ldapGroup.getGroupName()+"] already added!", null);
			return;
		}
		if(!ldapGroupList.contains(ldapGroup.getGroupName())){
			addErrorMessage("Group Name ["+ldapGroup.getGroupName()+"] is invalid!", null);
			return;
		}
		
		System.out.println("---before---"+ldapGroupList.size());
//    	newDocType.setName(documentType.getName());
//    	newDocType.setValue(documentType.getValue());
		ldapGroup.setRestrictGroup(restrictGroup?1:0);
		ldapGroup.setEnableTranscript(enableTranscript?1:0);
		ldapGroupsList.add(ldapGroup);
		ldapGroupsService.saveLdapGroup(ldapGroup);
		
		ldapGroupList.remove(ldapGroup.getGroupName());
		System.out.println("--after---"+ldapGroupList.size());
		ldapGroup = new LdapGroups();
//				
//		documentType.setName(null);//reset binded values
//		documentType.setValue(null);
	}
	public void addGuardianToList(){
		
		if(guardianExists()){
			addInfoMessage("Guardian "+dataGuardian.getDataGuardianEmail()+" already added!", null);
			return;
		}
		if(!VCSUtil.validateEmailAddress(dataGuardian.getDataGuardianEmail())){
			addErrorMessage("Invalid email address.", "Invalid email address.");
			return;
		}
		dataGuardianService.saveDataGuardian(dataGuardian);
		dataGuardianList.add(dataGuardian);
//		System.out.println("---before---"+ldapGroupList.size());
//    	newDocType.setName(documentType.getName());
//    	newDocType.setValue(documentType.getValue());
//		ldapGroup.setRestrictGroup(restrictGroup?1:0);
//		ldapGroup.setEnableTranscript(enableTranscript?1:0);
//		ldapGroupsList.add(ldapGroup);
//		ldapGroupsService.saveLdapGroup(ldapGroup);
		
//		ldapGroupList.remove(ldapGroup.getGroupName());
//		System.out.println("--after---"+ldapGroupList.size());
//				
//		documentType.setName(null);//reset binded values
//		documentType.setValue(null);
	}	
	
	 public List<String> autocomplete(String prefix) {
	        ArrayList<String> result = new ArrayList<String>();
	        
	        
	        
//	        Map<String, String> groups = util.getGroups();
//	        Set<String> keySet = groups.keySet();
//	        List<String> groupsList = new ArrayList<String>();
//	        for(String key:keySet)
//	        	groupsList.add(groups.get(key));
	        if ((prefix == null) || (prefix.length() == 0)) {
	            for (int i = 0; i < ldapGroupList.size(); i++) {
	                result.add(ldapGroupList.get(i));
	            }
	        } else {
	            Iterator<String> iterator = ldapGroupList.iterator();
	            while (iterator.hasNext()) {
	                String elem = ((String) iterator.next());
	                if ((elem != null && elem.toLowerCase().indexOf(prefix.toLowerCase()) == 0)
	                    || "".equals(prefix)) {
	                    result.add(elem);
	                }
	            }
	        }
//	        result.add("abc");
//	        result.add("def");

	        return result;
	    }
	public void removeGroupGuardianFromList(){
		
		boolean error=false;
		for(int i=0;i<ldapGroupsList.size();i++){
			if(ldapGroupsList.get(i).isSelected()){
//				if(! documentClassifyService.isDocTypeInUse(documentTypeList.get(i).getId())){
					LdapGroups ldapGroup = ldapGroupsList.get(i);
					ldapGroupsService.deleteLdapGroup(ldapGroup);
					ldapGroupList.add(ldapGroup.getGroupName());
					ldapGroupsList.remove(i);
					
					
//				}else{
//					error=true;
//					break;					
//				}				
			}
		}
		if(error){
			addErrorMessage("Can't delete document type,Its in use.", "Can't delete document type,Its in use.");
		}
	}
	
   public void removeAllDocumentTypeFromList(){
//		for(int i=0;i<documentTypeList.size();i++){
////			if(documentTypeList.get(i).getSelected().booleanValue()){
//				if(! documentClassifyService.isDocTypeInUse(documentTypeList.get(i).getId())){			         
//					removedDocTypesList.add(documentTypeList.get(i).getId());
//				}else{
////					error=true;
////					break;					
//				}				
////			}
//		}
//		documentTypeList.clear();    	
   }
   
   public void enableDisableFileSharing(){		
	   String navigateTo="DocumentCategories.faces?redirect=true";
	   
	   System.out.println("file sharing: " + enableCifsFileSharing);
	   
	   List<SystemCode> systemCodeList = systemCodeService.getSystemCodeByCodename("enable_cifs_file_sharing");
       
       SystemCode systemCode = null;
       
       if(systemCodeList!=null && systemCodeList.size()>0)
       	systemCode = systemCodeList.get(0);
       
       
		if(systemCode!=null){
			String enable = enableCifsFileSharing?"yes":"no";
			systemCode.setCodevalue(enable);
		}
//	   System.out.println("--------------------------saving document category: "+documentCategory.getName());
//	   if(documentCategory.getName()==null || documentCategory.getName().trim().equals("")){
//		   addErrorMessage("category name cannot be empty", "category name cannot be empty");
//		   return null;
//	   }
//	   try{
//		   for(int i=0;documentTypeList!=null && i<documentTypeList.size();i++){
//			   documentTypeList.get(i).setDocumentCategory(documentCategory);
//		   }
//		   for(int i=0;i<removedDocTypesList.size();i++){
//			   documentClassifyService.removeDocumentType(removedDocTypesList.get(i));
//		   }
//		   removedDocTypesList.clear();
//		   
//		   documentCategory.setDocumentTypes(fromListToSet(documentTypeList));
//		   documentClassifyService.saveDocumentCategory(documentCategory);	   
//	   }catch( org.springframework.dao.DataIntegrityViolationException ex){	//only if the category is associated with an existing policy
//		   addErrorMessage("Type cannot be updated, Please make sure that this type is not associated with a  policy", "Category edit failed");
//		   return null;
//	   }
	   
	   	   
//	   return navigateTo;
   }
   
   public <T> List<T> fromSetToList(Set<T> set) {
	    List<T> list = new ArrayList<T>();
	    for(T o : set) {
	        list.add(o);
	    }
	        return list;
	}
   
   public <T> Set<T> fromListToSet(List<T> list) {
	    Set<T> set = new HashSet<T>();
	    for(T o : list) {
	        set.add(o);
	    }
	        return set;
	}
   
   public String saveDocumentType(){		
	   String navigateTo="DocumentCategories.faces?redirect=true";
//	   documentType.setDocumentCategory(documentCategory);	   
//	   if(documentType.getId()!=null && documentType.getId().intValue()>0){
//		   documentClassifyService.saveDocumentType(documentType);
//	   }else{
//	      documentClassifyService.saveDocumentType(documentType);
//	   }
//	   	   
	   return navigateTo;
   }
   
   public List<DocumentCategory> getAllDocumentCategories(){
		return documentClassifyService.getAllDocumentCategories();
		
	}
   
   public List<DocumentType> getAllDocumentTypes(){
		return documentClassifyService.getAllDocumentTypes();
		
	}
	
	private boolean groupExists(){
		boolean exists=false;
		for(int i=0;i<ldapGroupsList.size();i++){
			if(ldapGroup.getGroupName().equals(ldapGroupsList.get(i).getGroupName())){
				exists = true;
				break;
			}
				
//			if( (documentType.getName().equalsIgnoreCase(documentTypeList.get(i).getName() ) ) ||
//					( documentType.getValue().equalsIgnoreCase(documentTypeList.get(i).getValue()) )){
//				exists=true;
//				break;
//			}
		}
		return exists;
	}
	
	private boolean guardianExists(){
		boolean exists=false;
		for(int i=0;i<dataGuardianList.size();i++){
			if(dataGuardian.getDataGuardianEmail().equals(dataGuardianList.get(i).getDataGuardianEmail())){
				exists = true;
				break;
			}
				
//			if( (documentType.getName().equalsIgnoreCase(documentTypeList.get(i).getName() ) ) ||
//					( documentType.getValue().equalsIgnoreCase(documentTypeList.get(i).getValue()) )){
//				exists=true;
//				break;
//			}
		}
		return exists;
	}
   //------------------------ General Getter/Setter---------------------------------------  
    
   public void populateDocumentCategory(ComponentSystemEvent  event){
//		if(documentCategoryId!=null && documentCategoryId.intValue()>0  && ! FacesContext.getCurrentInstance().isPostback()){
//			this.documentCategory=documentClassifyService.getDocumentCategory(documentCategoryId);
//			documentTypeList= fromSetToList(documentCategory.getDocumentTypes());
//		}
	}
	
    public void populateDocumentType(ComponentSystemEvent  event){		
//    	if(documentTypeId!=null && documentTypeId.intValue()>0 && ! FacesContext.getCurrentInstance().isPostback()){
//			this.documentType=documentClassifyService.getDocumentType(documentTypeId);			
//			this.documentCategory=documentClassifyService.getDocumentCategory(documentType.getDocumentCategory().getId());
//    	}
				
	}

	public Integer getLdapGroupId() {
		return ldapGroupId;
	}

	public void setLdapGroupId(Integer ldapGroupId) {
		this.ldapGroupId = ldapGroupId;
	}

	public LdapGroups getLdapGroup() {
		return ldapGroup;
	}

	public void setLdapGroup(LdapGroups ldapGroup) {
		this.ldapGroup = ldapGroup;
	}

	public List<LdapGroups> getLdapGroupsList() {
		return ldapGroupsList;
	}

	public void setLdapGroupsList(List<LdapGroups> ldapGroupsList) {
		this.ldapGroupsList = ldapGroupsList;
	}

	public List<Integer> getRemovedDocTypesList() {
		return removedDocTypesList;
	}

	public void setRemovedDocTypesList(List<Integer> removedDocTypesList) {
		this.removedDocTypesList = removedDocTypesList;
	}

	public List<String> getLdapGroupList() {
		return ldapGroupList;
	}

	public void setLdapGroupList(List<String> ldapGroupList) {
		this.ldapGroupList = ldapGroupList;
	}

	public boolean isRestrictGroup() {
		return restrictGroup;
	}

	public void setRestrictGroup(boolean restrictGroup) {
		this.restrictGroup = restrictGroup;
	}

	public boolean isEnableTranscript() {
		return enableTranscript;
	}

	public void setEnableTranscript(boolean enableTranscript) {
		this.enableTranscript = enableTranscript;
	}

	public boolean isEnableCifsFileSharing() {
		return enableCifsFileSharing;
	}

	public void setEnableCifsFileSharing(boolean enableCifsFileSharing) {
		this.enableCifsFileSharing = enableCifsFileSharing;
	}

	public DataGuardian getDataGuardian() {
		return dataGuardian;
	}

	public void setDataGuardian(DataGuardian dataGuardian) {
		this.dataGuardian = dataGuardian;
	}

	public List<DataGuardian> getDataGuardianList() {
		return dataGuardianList;
	}

	public void setDataGuardianList(List<DataGuardian> dataGuardianList) {
		this.dataGuardianList = dataGuardianList;
	}

}
