package com.virtualcode.controller;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UISelectMany;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.imageio.ImageIO;
import javax.jcr.RepositoryException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.tree.TreeNode;

import jcifs.smb.SmbAuthException;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbFileOutputStream;
import jespa.security.SecurityProviderException;

import net.coobird.thumbnailator.Thumbnails;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.log4j.Logger;
import org.apache.log4j.lf5.util.ResourceUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.util.PDFImageWriter;
import org.icepdf.core.exceptions.PDFException;
import org.icepdf.core.exceptions.PDFSecurityException;
import org.icepdf.core.pobjects.Document;
import org.icepdf.core.pobjects.Page;
import org.icepdf.core.util.GraphicsRenderingHints;

import org.richfaces.component.UIDataTable;
import org.richfaces.component.UIExtendedDataTable;
import org.richfaces.component.UITree;
import org.richfaces.component.UITreeNode;
import org.richfaces.event.TreeSelectionChangeEvent;
import org.richfaces.event.TreeToggleEvent;

import com.virtualcode.cifs.CifsUtil;
import com.virtualcode.common.AdvancedSecureURL;
import com.virtualcode.common.BreadCrumb;
import com.virtualcode.common.FileTypeMapper;
import com.virtualcode.common.SID;
import com.virtualcode.common.VCSConstants;
import com.virtualcode.pdfviewer.viewer.view.DocumentManager;
import com.virtualcode.security.impl.VirtualCodeAuthorizatorImpl;
import com.virtualcode.services.DriveLettersService;
import com.virtualcode.util.JespaUtil;
import com.virtualcode.util.PasswordGenerator;
import com.virtualcode.util.RepositoryBrowserUtil;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.RoleFeatureUtil;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.util.VirtualcodeUtil;
import com.virtualcode.vo.CustomTreeNode;
import com.virtualcode.vo.DataGuardian;
import com.virtualcode.vo.DriveLetters;
import com.virtualcode.vo.LdapGroups;
import com.virtualcode.vo.LinkAccessDocuments;
import com.virtualcode.vo.LinkAccessUsers;
import com.virtualcode.vo.RoleFeature;
import com.virtualcode.vo.SystemCode;
import com.virtualcode.vo.SystemCodeType;

@ManagedBean(name="explorer")
@ViewScoped
public class FileExplorerController extends AbstractController {
	
	Logger log = Logger.getLogger("com.virtualcode.controller.FileExplorerController");
	
	public FileExplorerController(){
		
    	activeDeActivateUserControls();
    	
    	if(showDocumentUrl){
    		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
    		dnsServer = ru.getCodeValue("DASUrl", SystemCodeType.ZUES_AGENT);
    	}
    		
    	
	}
	
	private List<CustomTreeNode> rootNodes;
	
	private CustomTreeNode parentNode;
	
	private List<BreadCrumb> breadCrumbsList=new ArrayList<BreadCrumb>();
	
	private List<CustomTreeNode> firstLevelFiles;
	
	private double folderSizeMb=0;//not recursive
	
	private CustomTreeNode selectedNode;
	
	private List<CustomTreeNode> nextVersionsList=null;
		
	private Collection<Object> selection;
	
	private List<CustomTreeNode> selectionItems = new ArrayList<CustomTreeNode>();

	private String selectedNodePath;
	
	private ArrayList<SID> secAllGroups=new ArrayList<SID>();
	
	private ArrayList<SID> securityAllowGroups=new ArrayList<SID>();
	
	private ArrayList<SID> securityDenyGroups=new ArrayList<SID>();
	
	private ArrayList<SID> shareAllGroups=new ArrayList<SID>();
	
	private ArrayList<SID> shareAllowGroups=new ArrayList<SID>();
	
	private ArrayList<SID> shareDenyGroups=new ArrayList<SID>();
	
	private String exportNodePath;
	
	private boolean showExport;
	
	private boolean recursive;
	
	private boolean batchUpdate;
	
	private boolean merge = false;
	
	//two properties added as the boolean checkbox was not updating value after
	//first ajax form submission

	private String tempRecursive;

	private String tempBatchUpdate;
	
	private String tempMerge;

	//added for user level permission support 
	private int permissionUsersOrGroups = 1;
	
	private String saveOrSyncPermissions = "1";
	
	private ArrayList<SID> secAllUsers = new ArrayList<SID>();
	
	private ArrayList<SID> shareAllUsers = new ArrayList<SID>();
	
	private String sortBy="name";
	
	private String sortMode;
	
	private boolean searchPathEnabled=true;
	
	private String searchPath=null;
	
	private boolean enableDocumentTagging;
	
	
	//for export job
	private String remoteDomainName;
	 
	private String remoteUserName;
	 
	private String remoteUserPassword;
	
	private String emailToLink;
	
	private String emailNameLink;
	
	private String emailBodyLink;
	
	private String emailSubjectLink = "Shared Link";
	
	private String emailToFile;
	
    private String emailNameFile;
	
	private String emailBodyFile;
	
	private String emailSubjectFile = "Shared File";
	
	private boolean filesShareEnabled=true;
	private boolean linksShareEnabled=true;
	private boolean downloadEnabled=false;
	private boolean fileRestoreEanbled=false;
	private boolean folderRestoreEanbled=false;
	private boolean mediaStreamingEnabled=false;
	
	private boolean showThumbnail = false;
	
	private boolean enablePdfView = false;
	
	private boolean showPdfIcon = false;
	
	private String urlForPdf = "";
	
	private boolean showFrame = false;
	
	private boolean restoreRecursive = false;
	
	public boolean isRestoreRecursive() {
		return restoreRecursive;
	}

	public void setRestoreRecursive(boolean restoreRecursive) {
		this.restoreRecursive = restoreRecursive;
	}

	@ManagedProperty(value="#{documentManager}")
	private DocumentManager documentManager;
	
	private String currentThumbnailPath;
	
	private String emailString;
	
	private boolean emailUsingDefaultClient = false; //non html client like Outlook etc.
	
	private String linkOption = "1"; 	// 1 =  download link, 2 = view document link
	
	private List<CustomTreeNode> oldFirstLevelFiles; //only for versions
	
	private boolean enableCIFSFileSharing = false;
	
	private String emailSmbToLink;
	
	private String emailSmbNameLink;
	
	private String emailSmbBodyLink;
	
	private String emailSmbSubjectLink = "Shared Link";
	
	private boolean folderArchived = true;
	
	private boolean emailFileSizeExceeded = false;
	
	private boolean remoteFilesLoaded = false;
	
	private boolean emulateEmailSender = false;
	
	private boolean showDocumentUrl = false;
	
	private String dnsServer;
	
	private String pdfViewerUrl;
	
	private boolean alreadyExists = false;
	
//	private GroupGuardian groupGuardian;
	
	public int getPermissionUsersOrGroups() {
		return permissionUsersOrGroups;
	}

	public void setPermissionUsersOrGroups(int permissionUsersOrGroups) {
		this.permissionUsersOrGroups = permissionUsersOrGroups;
	}

	public boolean isBatchUpdate() {
		return batchUpdate;
	}

	public void setBatchUpdate(boolean batchUpdate) {
		this.batchUpdate = batchUpdate;
	}

	public boolean isRecursive() {
		return recursive;
	}

	public void setRecursive(boolean recursive) {
		this.recursive = recursive;
	}

	public boolean isShowExport() {
		return showExport;
	}

	public void setShowExport(boolean showExport) {
		this.showExport = showExport;
	}
	
	public boolean isMerge() {
		return merge;
	}

	public void setMerge(boolean merge) {
		this.merge = merge;
	}

	public String getExportNodePath() {
		return exportNodePath;
	}

	public void setExportNodePath(String exportNodePath) {
		this.exportNodePath = exportNodePath;
	}

	@ManagedProperty(value="#{jobController}")
	private JobController jobController;
	
	
	 
	public JobController getJobController() {
		return jobController;
	}

	public void setJobController(JobController jobController) {
		this.jobController = jobController;
	}

	public String getSelectedNodePath() {
		return selectedNodePath;
	}

	public void setSelectedNodePath(String selectedNodePath) {
		this.selectedNodePath = selectedNodePath;
	}

	//called from view one time
	public void init(){
		if(! FacesContext.getCurrentInstance().isPostback()){
		    List<SystemCode> sysCodesList=systemCodeService.getSystemCodeByCodename("companyName");
		    String companyName="Archive";
		    if(sysCodesList!=null && sysCodesList.size()>0){
		    	companyName=sysCodesList.get(0).getCodevalue();
		    }
		    CustomTreeNode customTreeNode=new CustomTreeNode();
		    customTreeNode.setName(companyName);
		    customTreeNode.setType("nt:folder");
		    customTreeNode.setChildNodes(filterNodes(fileExplorerService.getRootNodes()));
			rootNodes=new ArrayList<CustomTreeNode>();
		    rootNodes.add(customTreeNode);
		   
		   
		}
	}
	
	public List<CustomTreeNode> getRootNodes(){
		  return rootNodes;
	}
	
	
	public ArrayList<SID> getSecAllUsers() {
		return secAllUsers;
	}

	public void setSecAllUsers(ArrayList<SID> secAllUsers) {
		this.secAllUsers = secAllUsers;
	}

	public ArrayList<SID> getShareAllUsers() {
		return shareAllUsers;
	}

	public void setShareAllUsers(ArrayList<SID> shareAllUsers) {
		this.shareAllUsers = shareAllUsers;
	}

	public void  loadParentSiblings(){
		
		    if(parentNode==null){
		       return;
		    }
		    
		    //System.out.println("******Back :  Parent Node Path--->"+parentNode.getPath()); 		
			if(isRootNode(parentNode)){
				this.firstLevelFiles=rootNodes.get(0).getChildNodes();
				return;
			}
			
			if(parentNode!=null){
			    parentNode=fileExplorerService.getParentNode(parentNode);				
				this.firstLevelFiles=fileExplorerService.getFirstLevelNodes(parentNode.getPath());
				this.firstLevelFiles=filterNodes(firstLevelFiles);//filter for rights
				this.oldFirstLevelFiles = this.firstLevelFiles;				
			}		
		
	}
	
	private boolean isRootNode(CustomTreeNode paramNode){
		CustomTreeNode rootNode;
		boolean exists=false;
		if(paramNode==null){
			return true;
		}
		List<CustomTreeNode> tempList=null;
		if(rootNodes!=null && rootNodes.size()>0){
			tempList=rootNodes.get(0).getChildNodes();
		}
		for(int i=0;tempList!=null && i<tempList.size();i++){
			rootNode=tempList.get(i);
			if(paramNode.getPath().equals(rootNode.getPath())){
				exists=true;
				break;
			}
		}
		return exists;
	}

	public void searchByPath(){
		System.out.println("Search Path entered:-----> " + searchPath);
		String originalPath=searchPath;
		searchPath=searchPath.replaceAll("\\\\", "/");
		
		if(searchPath.startsWith("//")){
			searchPath=searchPath.substring(2,searchPath.length());
			if(rootNodes!=null ){
				String companyName=rootNodes.get(0).getName();
				searchPath="/"+companyName+"/"+"FS"+"/"+searchPath;
				if(fileExplorerService.getNodeByPath(searchPath)==null){
					searchPath="/"+companyName+"/"+"SP"+"/"+searchPath;
				}
			}
		}
		
		Pattern pathPattern=Pattern.compile("(^[a-zA-Z]:)", Pattern.UNICODE_CASE);
		
		Matcher matcher=pathPattern.matcher(searchPath);
		if(matcher.find()){
			System.out.print("***** Its a drive mapping path:*****");
	
    		String searchPathDriveLetter=searchPath.substring(0,2);
    		DriveLettersService driveLetterService=serviceManager.getDriveLettersService();
    		List<DriveLetters> driveLetterList=driveLetterService.getAll();
    	    String drivePath;
    	    DriveLetters driveLetter=null;
    	    for(int i=0;driveLetterList!=null && i<driveLetterList.size();i++){
    	    	driveLetter=driveLetterList.get(i);
    	    	if(searchPathDriveLetter.equalsIgnoreCase(driveLetter.getDriveLetter())){break;}
    	    }
    	    
    		if(driveLetter!=null){
    			System.out.println("***** Drive mapping found.....");
    			//modified to handle UNC path in drive letters according to new requirement
    	    	drivePath=driveLetter.getSharePath();    	
    			if(drivePath.startsWith("\\\\")){
    				ResourcesUtil ru = ResourcesUtil.getResourcesUtil();				
    				drivePath ="/"+ ru.getCodeValue("companyName", SystemCodeType.GENERAL) + "/FS/" + drivePath.substring(2, drivePath.lastIndexOf("\\")).replaceAll("\\\\", "/");
    			}
    			searchPath=drivePath+"/"+searchPath.substring(2);    			
    	    	searchPath=searchPath.replaceAll("//", "/");
    	    }else{
    	    	System.out.println("***** No drive mapping found.....");
    	    	this.firstLevelFiles=null;
    	    	return;
    	    }
    	}
    	
    	System.out.println("Search Path formatted:-----> " + searchPath);
		parentNode=fileExplorerService.getNodeByPath(searchPath);
		
		/*   Code to check Ancestor in Deny Group  */
		String userSidsStr = "S-1-1-0,";
		String[] userSidsArr=null;
		String authType=(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE);
	    String userName=(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
	    	    
		if(authType!=null && authType.equalsIgnoreCase("ad")){
		   System.out.println("--getting sids for user: " + userName);
		   try{
		       VirtualCodeAuthorizatorImpl authorizator = VirtualCodeAuthorizatorImpl.getInstance();
		       userSidsStr += authorizator.sidString(userName);
		       userSidsArr=userSidsStr.split(",");
		   }catch(Exception ex){ex.printStackTrace();}
		
		 if(areAncestorsInDenyGroups(userSidsArr,parentNode)){
			this.firstLevelFiles=null;
			return;
		 }
		 
		 if(!areAncestorsInAllowedGroups(userSidsArr,parentNode)){
			this.firstLevelFiles=null;
			return;
		 }
		}
		/*   end of code to check in ancestor deny groups */
		if(parentNode!=null){
			if(searchPath.contains("//"))
				searchPath = searchPath.replaceAll("//", "/");	//error handling
			this.firstLevelFiles=fileExplorerService.getFirstLevelNodes(searchPath);
			this.firstLevelFiles=filterNodes(firstLevelFiles);
			createBreadCrumbs(parentNode.getPath());
			remoteFilesLoaded = false;
		}else{
			firstLevelFiles=null;
		}
		
		if((parentNode== null || this.firstLevelFiles== null || this.firstLevelFiles.size()<1) && enableCIFSFileSharing){
			//search in the UNC Path

//			try {
//				originalPath = searchPath;
				
				getFilesFromShare(originalPath);
//				remoteFilesLoaded = true;
//				searchPath = "smb://network.vcsl;se6:jwdmuz1@"+orignalPath.substring(2).replaceAll("\\\\", "/")+"/";
//				long startTime = System.currentTimeMillis();
//				System.out.println("start time: " + startTime);
//				SmbFile remoteFolder = new SmbFile(searchPath);
//				 DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
//				if(remoteFolder.exists()){
//					firstLevelFiles = new ArrayList<CustomTreeNode>();
//					System.out.println("folder exists");
//					System.out.println(df.format(new Date(remoteFolder.getLastModified())));
//					System.out.println(df.format(new Date(remoteFolder.createTime())));
//					String[] dirList = remoteFolder.list();
//					for(int i = 0; i < dirList.length ; i++){
//						SmbFile file = new SmbFile(searchPath + dirList[i]);
//						if(file.exists()){
//							CustomTreeNode node = new CustomTreeNode();
//							node.setName(file.getName());
//							node.setLastModified(df.format(new Date(file.getLastModified())));
//							node.setCreatedDate(df.format(new Date(file.createTime())));
//	                        DecimalFormat decf = new DecimalFormat("#.##");
//	                        double length = file.getContentLength();
//	                        boolean isFolder = file.isDirectory();
//	                        if(!isFolder){
//		                        node.setSizeinKbs(Math.round(length)+"");
//		                        length = length / 1000d;
//		                        String fileSize = decf.format(length)+" KB";                        
//		                        
//		                        //convert to MBs
//		                        if(length>1024d){
//		                       	 length = length / 1024d;
//		                       	fileSize = decf.format(length)+" MB";
//		                        }
//		                      //convert to GBs
//		                        if(length>1024d){
//		                       	 length = length / 1024d;
//		                         fileSize = decf.format(length)+" GB";
//		                        }	                        
//								node.setFileSize(fileSize);
//	                        }
//		                    RepositoryBrowserUtil repoBrwUtil = RepositoryBrowserUtil.getInstance();
//		                    FileTypeMapper fileTypeMapper = repoBrwUtil.getFileTypeMapperFromName(file.getName(), isFolder);
//		                    
//		                    node.setFileTypeDesc(fileTypeMapper.getTypeText());
//		                    node.setIconUrl(fileTypeMapper.getIconName());
////							System.out.println("name " + i + " : " + file.getName());
////							System.out.println(new Date(file.getLastModified()));
////							System.out.println(new Date(file.createTime()));
////							System.out.println(file.getContentLength());
//		                    node.setArchive(false);
//							firstLevelFiles.add(node);
//						}
//						
//					}
//				}
//				System.out.println("Total time taken for " + firstLevelFiles.size() + " = " + (System.currentTimeMillis() -  startTime) + " ms");
//			} catch (MalformedURLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (SmbException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			//end
//			firstLevelFiles=null;
			System.out.println("**** Search Path not found..");
		}
    	searchPath=originalPath;//.replaceAll("\\\\", "\\");
    	if(firstLevelFiles==null){
    		addErrorMessage("Could not load Path", "Could not load Path");
    	}
	}
	
	public void folderSelected(){
		selectedNodePath=getRequestParameter("selectedNodePath");
		boolean isArchive = true;
		if(getRequestParameter("isArchive")!=null)
				isArchive = getRequestParameter("isArchive").equals("true")?true:false;
		
		System.out.println("selection: " + selectedNodePath);
		if(!selectedNodePath.startsWith("\\") && !selectedNodePath.startsWith("/"))
			selectedNodePath = "/" + selectedNodePath;
		
		if(!isArchive || remoteFilesLoaded)
		{
			selectedNodePath = "\\\\"+selectedNodePath.substring(selectedNodePath.indexOf("FS/")+3);
			System.out.println(selectedNodePath.replaceAll("/", "\\\\"));
			getFilesFromShare(selectedNodePath.replaceAll("/", "\\\\"));
		}else{
			parentNode=fileExplorerService.getNodeByPath(selectedNodePath);
			if(parentNode != null)
				createBreadCrumbs(parentNode.getPath());		
	    	if(selectedNodePath!=null && ! selectedNodePath.trim().equals("")){
	    		this.firstLevelFiles=fileExplorerService.getFirstLevelNodes(selectedNodePath);
	    		this.firstLevelFiles=filterNodes(firstLevelFiles);
	    		this.oldFirstLevelFiles = firstLevelFiles;
	    	}
		}
//		
		if(parentNode==null && enableCIFSFileSharing)
		{
			selectedNodePath = "\\\\"+selectedNodePath.substring(selectedNodePath.indexOf("FS/")+3);
			System.out.println(selectedNodePath);
			getFilesFromShare(selectedNodePath.replaceAll("/", "\\\\")); //load from UNC
		}
//		if(isArchive)
//			remoteFilesLoaded = false;
//		else
//			remoteFilesLoaded = true;
    	if(firstLevelFiles==null){
    		addErrorMessage("Path does not exist", "path does not exist");
    	}
	}
	
	public void loadRemoteFiles(){
		if(!remoteFilesLoaded){			
			String remotePath = searchPath;
			getFilesFromShare(remotePath);
//			remoteFilesLoaded = true;
		}else{
//			if(searchPath.contains("\\\\\\\\"))
//				searchPath = searchPath.replaceAll("\\\\", "\\");
			searchByPath();
//			remoteFilesLoaded = false;
		}
//		String remotePath = searchPath;
//		getFilesFromShare(remotePath);
    	if(firstLevelFiles==null){
    		addErrorMessage("Path does not exists", "path does not exists");
    	}
	}
	
	public void getFilesFromShare(String remotePath){
		try {			
			if(remotePath==null || remotePath.length()<1)
				return;
			ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
			String domain = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
    		String username = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
    		String password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
    		
    		 DriveLetters driveLetter = null;
    	      List<DriveLetters> driveLetterList=driveLettersService.getAll();
    	      
//    	      for(DriveLetters letter: driveLetterList){
//    				String sharePath = letter.getSharePath().toLowerCase();
//    				if(sharePath.startsWith("\\\\")){
////    					ResourcesUtil ru = ResourcesUtil.getResourcesUtil();				
////    					sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase() + "/fs/" + sharePath.substring(2).replaceAll("\\\\", "/");
//    				}
////    				if(remotePath.endsWith("/"))
////    					sharePath = sharePath.substring(0, sharePath.lastIndexOf("/"));
//    				if(!remotePath.endsWith("\\"))
//    					remotePath = remotePath+ "\\";
//    				System.out.println(sharePath + ", " + remotePath.toLowerCase());
//    	      	if(remotePath.toLowerCase().contains(sharePath) || sharePath.contains(remotePath.toLowerCase())){
//    	      		driveLetter = letter;
//    	      		break;
//    	      	}
//    	      }
    	      
    	      driveLetter = VirtualcodeUtil.getInstance().getDriveLetterMappings(remotePath);
    	      
    	      System.out.println(VirtualcodeUtil.getInstance().getDriveLetterMappings(remotePath));
//    		  ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
//    		  String domain = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
//    		  String username = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
//    		  String password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
    			Pattern pathPattern=Pattern.compile("(^[a-zA-Z]:)", Pattern.UNICODE_CASE);
    			
    			Matcher matcher=pathPattern.matcher(remotePath);
    	      if(driveLetter==null){
    	  		if(matcher.find()){
    				System.out.print("***** Its a drive mapping path:*****");
    		
    	    		String searchPathDriveLetter=searchPath.substring(0,2);
    	    		DriveLettersService driveLetterService=serviceManager.getDriveLettersService();
//    	    		List<DriveLetters> driveLetterList=driveLetterService.getAll();
    	    	    String drivePath;
//    	    	    DriveLetters driveLetter=null;
    	    	    for(int i=0;driveLetterList!=null && i<driveLetterList.size();i++){
    	    	    	driveLetter=driveLetterList.get(i);
    	    	    	if(searchPathDriveLetter.equalsIgnoreCase(driveLetter.getDriveLetter())){break;}
    	    	    }
    	    	    
    	    		if(driveLetter!=null){
    	    			System.out.println("***** Drive mapping found.....");
    	    			//modified to handle UNC path in drive letters according to new requirement
    	    	    	drivePath=driveLetter.getSharePath();    	
    	    			if(drivePath.startsWith("\\\\")){
//    	    				ResourcesUtil ru = ResourcesUtil.getResourcesUtil();				
//    	    				drivePath ="/"+ ru.getCodeValue("companyName", SystemCodeType.GENERAL) + "/FS/" + drivePath.substring(2, drivePath.lastIndexOf("\\")).replaceAll("\\\\", "/");
    	    			}
    	    			remotePath = drivePath;
//    	    			searchPath=drivePath+"/"+searchPath.substring(2);  		
//    	    	    	searchPath=searchPath.replaceAll("//", "/");
    	    	    }else{
    	    	    	System.out.println("***** No drive mapping found.....");
    	    	    	this.firstLevelFiles=null;
    	    	    	return;
    	    	    }
    	    	}
    	      }
    		  if(driveLetter!=null && driveLetter.getRestoreDomain()!=null){
    	 			domain =  driveLetter.getRestoreDomain();
    	 			username = driveLetter.getRestoreUsername();
    	 			password = driveLetter.getRestorePassword();
    	 		}
    		  String searchPath = "smb://"+ domain +";"+ username +":"+ password +"@"+remotePath.substring(2).replaceAll("\\\\", "/");
    		  if(!searchPath.endsWith("/"))
    			  searchPath = searchPath +"/";
//    		  System.out.println(searchPath);
			long startTime = System.currentTimeMillis();
			System.out.println("start time: " + startTime);
			String userSidsStr = "S-1-1-0,";
			String authType=(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE);
		    String userName=(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
		    List<CustomTreeNode> resultsList=new ArrayList<CustomTreeNode>();
		    String[] userSidsArr = {};
			if(authType!=null && authType.equalsIgnoreCase("ad")){				
				try{
				       VirtualCodeAuthorizatorImpl authorizator = VirtualCodeAuthorizatorImpl.getInstance();
				       userSidsStr += authorizator.sidString(userName);
				       System.out.println("user sids list: "+userSidsStr);
				       userSidsArr=userSidsStr.split(",");
				}catch(Exception ex){ex.printStackTrace();}
				
				if(areAncestorsInDenyGroups(userSidsArr, searchPath))
					return;
				
				if(!areAncestorsInAllowedGroups(userSidsArr, searchPath))
					return;
			}
//			SmbFile remoteFolder = new SmbFile(searchPath);
			SmbFile remoteFolder =  null;
			try{
				remoteFolder =  getSmbObject(searchPath);
				if(!remoteFolder.exists()){
					domain = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
		    		username = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
		    		password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
		    		searchPath = "smb://"+ domain +";"+ username +":"+ password +"@"+remotePath.substring(2).replaceAll("\\\\", "/");
		    		  if(!searchPath.endsWith("/"))
		    			  searchPath = searchPath +"/";
		    		remoteFolder = getSmbObject(searchPath);
				}
			}catch(SmbException ex){
				log.error("Error loading sharePath credectioals, going for global credentials." + ex.getMessage());
				domain = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
	    		username = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
	    		password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
	    		searchPath = "smb://"+ domain +";"+ username +":"+ password +"@"+remotePath.substring(2).replaceAll("\\\\", "/");
	    		  if(!searchPath.endsWith("/"))
	    			  searchPath = searchPath +"/";
	    		remoteFolder = getSmbObject(searchPath);
			}
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
			
			if(remoteFolder.exists()){
//				if(firstLevelFiles==null)
					firstLevelFiles = new ArrayList<CustomTreeNode>();
				System.out.println("folder exists");
//				System.out.println(df.format(new Date(remoteFolder.getLastModified())));
//				System.out.println(df.format(new Date(remoteFolder.createTime())));
				String[] dirList = remoteFolder.list();
				for(int i = 0; i < dirList.length ; i++){
					SmbFile file = new SmbFile(searchPath + dirList[i]);
					if(file.exists()){
						String fileName = file.getName();
						//skip temporary and stubbed files
						if (fileName.isEmpty()
		                        || fileName.startsWith("~")
		                        || fileName.startsWith("$")
		                        //|| cFileName.startsWith(".")
		                        || fileName.endsWith(".lnk")
		                        || fileName.endsWith(".tmp")
		                        || fileName.endsWith(".url"))
							continue;
						String filePath = "\\\\"+file.getPath().substring(file.getPath().indexOf("@")+1).replaceAll("/", "\\\\");
						if(authType!=null && authType.equalsIgnoreCase("ad")){
							if(file.isFile()){
								boolean authorize = fileExplorerService.authorizeSmbFile(file, userName);
	//							authorize = false;
//								System.out.println("authorize=" + authorize);
								if(!authorize)
									continue;
							}else{
								System.out.println("folder name: "+ file.getName());
								boolean notAuthorize = areAncestorsInDenyGroups(userSidsArr, file.getPath());
								System.out.println(notAuthorize);
								if(notAuthorize)
									return;
							}
						}
						CustomTreeNode node = new CustomTreeNode();
						node.setName(file.getName());
						node.setLastModified(df.format(new Date(file.getLastModified())));
						node.setCreatedDate(df.format(new Date(file.createTime())));
                        DecimalFormat decf = new DecimalFormat("#.##");
//                        SmbFileInputStream smbIs = new SmbFileInputStream(file);
//                        System.out.println(Integer.MAX_VALUE);
                        double length = file.length();

                        System.out.println("length :" + file.length() +" for file: " + file.getName());
                        boolean isFolder = file.isDirectory();
                        if(!isFolder){
	                        node.setSizeinKbs(Math.round(length)+"");
	                        length = length / 1024d;
	                        String fileSize = decf.format(length)+" KB";                        
	                        
	                        //convert to MBs
	                        if(length>1024d){
	                       	 length = length / 1024d;
	                       	fileSize = decf.format(length)+" MB";
	                        }
	                      //convert to GBs
	                        if(length>1024d){
	                       	 length = length / 1024d;
	                         fileSize = decf.format(length)+" GB";
	                        }	                        
							node.setFileSize(fileSize);
                        }
                        if(isFolder)
                        	node.setType("nt:folder");
                        else
                        	node.setType("nt:file");
	                    RepositoryBrowserUtil repoBrwUtil = RepositoryBrowserUtil.getInstance();
	                    FileTypeMapper fileTypeMapper = repoBrwUtil.getFileTypeMapperFromName(file.getName(), isFolder);
	                    
	                    node.setFileTypeDesc(fileTypeMapper.getTypeText());
	                    node.setIconUrl(fileTypeMapper.getIconName());
//						System.out.println("name " + i + " : " + file.getName());
//						System.out.println(new Date(file.getLastModified()));
//						System.out.println(new Date(file.createTime()));
//						System.out.println(file.getContentLength());
	                    node.setArchive(false);
//	                    System.out.println(file.getPath());
//	                    System.out.println(file.getCanonicalPath());
//	                    System.out.println(file.getDfsPath());
	                    node.setPath(filePath);
						firstLevelFiles.add(node);
					}
					
				}
			}else{
				firstLevelFiles = new ArrayList<CustomTreeNode>();
			}
			String companyName = "archive";
			if(ru.getCodeValue("companyName", SystemCodeType.GENERAL)!=null)
				companyName = ru.getCodeValue("companyName", SystemCodeType.GENERAL);
			String filePath = "/"+ companyName +"/FS/"+remoteFolder.getPath().substring(remoteFolder.getPath().indexOf("@")+1);
			createBreadCrumbs(filePath);
			folderArchived = false;
			System.out.println("Total time taken for " + firstLevelFiles.size() + " = " + (System.currentTimeMillis() -  startTime) + " ms");
			remoteFilesLoaded = true;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			remoteFilesLoaded = false;
			e.printStackTrace();
		} catch (SmbException e) {
			// TODO Auto-generated catch block
			remoteFilesLoaded = false;
			e.printStackTrace();
		}
	}	
	
	public void getRemoteFileDetails(String remotePath){
		try {
			ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
			String domain = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
    		String username = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
    		String password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
    		
    		DriveLetters driveLetter = null;
    		
    		driveLetter = VirtualcodeUtil.getInstance().getDriveLetterMappings(remotePath);
/*  	      	List<DriveLetters> driveLetterList=driveLettersService.getAll();
  	      
  	      	for(DriveLetters letter: driveLetterList){
  				String sharePath = letter.getSharePath().toLowerCase();
  				if(sharePath.startsWith("\\\\")){
//  					ResourcesUtil ru = ResourcesUtil.getResourcesUtil();				
//  					sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase() + "/fs/" + sharePath.substring(2).replaceAll("\\\\", "/");
  				}
//  				if(remotePath.endsWith("/"))
//  					sharePath = sharePath.substring(0, sharePath.lastIndexOf("/"));
  				if(!remotePath.endsWith("\\"))
  					remotePath = remotePath+ "\\";
  				System.out.println(sharePath + ", " + remotePath.toLowerCase());
  	      	if(remotePath.toLowerCase().contains(sharePath)){
  	      		driveLetter = letter;
  	      		break;
  	      	}
  	      }*/
//  		  ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
//  		  String domain = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
//  		  String username = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
//  		  String password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
  		  if(driveLetter!=null && driveLetter.getRestoreDomain()!=null){
  	 			domain =  driveLetter.getRestoreDomain();
  	 			username = driveLetter.getRestoreUsername();
  	 			password = driveLetter.getRestorePassword();
  	 		}
			String searchPath = "smb://"+ domain +";"+ username +":"+ password +"@"+remotePath.substring(2).replaceAll("\\\\", "/")+"/";
			long startTime = System.currentTimeMillis();
			System.out.println("start time: " + startTime);
			SmbFile remoteFile = new SmbFile(searchPath);
			 DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
			if(remoteFile.exists()){
//				if(firstLevelFiles==null)
//					firstLevelFiles = new ArrayList<CustomTreeNode>();
				System.out.println("file exists");
				System.out.println(df.format(new Date(remoteFile.getLastModified())));
				System.out.println(df.format(new Date(remoteFile.createTime())));
//				String[] dirList = remoteFile.list();
//				for(int i = 0; i < dirList.length ; i++){
//					SmbFile file = new SmbFile(searchPath + dirList[i]);
//					if(file.exists()){
						CustomTreeNode node = new CustomTreeNode();
						node.setName(remoteFile.getName());
						if(node.getName().endsWith("/"))
							node.setName(node.getName().substring(0, node.getName().length()-1));
						node.setLastModified(df.format(new Date(remoteFile.getLastModified())));
						node.setCreatedDate(df.format(new Date(remoteFile.createTime())));
						node.setLastAccessed(df.format(new Date(remoteFile.lastAccess())));
                        DecimalFormat decf = new DecimalFormat("#.##");
                        double length = remoteFile.getContentLength();
                        boolean isFolder = remoteFile.isDirectory();
                        if(!isFolder){
	                        node.setSizeinKbs(Math.round(length)+"");
	                        length = length / 1000d;
	                        String fileSize = decf.format(length)+" KB";                        
	                        
	                        //convert to MBs
	                        if(length>1024d){
	                       	 length = length / 1024d;
	                       	fileSize = decf.format(length)+" MB";
	                        }
	                      //convert to GBs
	                        if(length>1024d){
	                       	 length = length / 1024d;
	                         fileSize = decf.format(length)+" GB";
	                        }	                        
							node.setFileSize(fileSize);
                        }
                        if(isFolder)
                        	node.setType("nt:folder");
                        else
                        	node.setType("nt:file");
	                    RepositoryBrowserUtil repoBrwUtil = RepositoryBrowserUtil.getInstance();
	                    FileTypeMapper fileTypeMapper = repoBrwUtil.getFileTypeMapperFromName(node.getName(), isFolder);
	                    
	                    node.setFileTypeDesc(fileTypeMapper.getTypeText());
	                    node.setIconUrl(fileTypeMapper.getIconName());
//						System.out.println("name " + i + " : " + file.getName());
//						System.out.println(new Date(file.getLastModified()));
//						System.out.println(new Date(file.createTime()));
//						System.out.println(file.getContentLength());
	                    node.setArchive(false);
//	                    System.out.println(remoteFile.getPath());
//	                    System.out.println(remoteFile.getCanonicalPath());
//	                    System.out.println(remoteFile.getDfsPath());
	                    node.setPath("\\\\"+remoteFile.getPath().substring(remoteFile.getPath().indexOf("@")+1).replaceAll("//", "/").replaceAll("/", "\\\\"));
						if(node.getPath().endsWith("\\"))
							node.setPath(node.getPath().substring(0, node.getPath().length()-1));
						node.setHash(SHAsum((remoteFile.getLastModified()+"").getBytes()));
						System.out.println("hash: "+SHAsum((remoteFile.getLastModified()+"").getBytes()));
	                    selectedNode = node;
//						firstLevelFiles.add(node);
	            		if(enablePdfView)
	            			showPdfIcon = VCSUtil.isPdfViewable(selectedNode.getName());
//					}
					
//				}
			}
			// for thumbnail generation
			String fileExt = selectedNode.getName().substring(selectedNode.getName().lastIndexOf(".")).toLowerCase();
			if(fileExt.contains(".jpeg") || fileExt.contains(".bmp") || fileExt.contains(".gif")
					|| fileExt.contains(".png") || fileExt.contains(".jpg") || fileExt.contains(".pdf")){
				
				long start = System.currentTimeMillis();
				
				// TODO Auto-generated method stub
				try {
					InputStream is = remoteFile.getInputStream();// fileExplorerService.downloadFile(selectedNode.getPath()).getStream();
					if(fileExt.contains(".pdf")){
						try {
//    							FileOutputStream fos = new FileOutputStream(new File("D:/images/newpdf.pdf"));
//    							IOUtils.copyLarge(is, fos);
//    							fos.close();
							Document document = new Document();
							document.setInputStream(is, "/tmp/");
//    							PDDocument doc = PDDocument.load(is);
//    							List<PDPage>pages =  doc.getDocumentCatalog().getAllPages();
//    							PDPage page = pages.get(0);
//    							PDFImageWriter imageWrite = new PDFImageWriter();
//    							imageWrite.writeImage(doc, "png", null, 1 , 3, "D:/images/HeapSizepdf");
							BufferedImage image;// =page.convertToImage();//convertToImage();
							ByteArrayOutputStream baos = new ByteArrayOutputStream();
							
//    							ImageIO.write(image, "jpg", new File("D:/images/pdf.jpg"));
							image = (BufferedImage) document.getPageImage(0, GraphicsRenderingHints.SCREEN,
			                        Page.BOUNDARY_CROPBOX, 0.0f, 1.0f);
//    							image1.g
							ImageIO.write(image, "png", baos);
							is = new ByteArrayInputStream(baos.toByteArray());
							
//    							PDFImageWriter imageWrite = new PDFImageWriter();
//    							imageWrite.writeImage(doc, "png", null, 1 , pages.size(), "D:/images/Heap Size");
//    							File outputfile = new File("D:/images/Heap Size.png");
//    							ImageIO.write(image, "png", outputfile);
//    							baos.close();
//    							fos.close();
//    							doc.close();			
						} catch (IOException | PDFException | PDFSecurityException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					
			        String sessionId=getHttpSession().getId();
			              
		            File outputDir=new File(getContextRealPath()+"/thumbnails/"+sessionId);
		            if(!outputDir.exists()){
		           	  outputDir.mkdirs();
		            }
		            FileUtils.cleanDirectory(outputDir);	// delete any existing files in the dir
					Thumbnails.of(is).size(200, 200).toFile(new File(getContextRealPath()+"/thumbnails/"+sessionId +"/" + selectedNode.getName().substring(0, selectedNode.getName().lastIndexOf("."))+".png"));
					System.out.println("Thumbnail creation successful....");
					showThumbnail = true;
					currentThumbnailPath = "thumbnails/"+sessionId +"/" + selectedNode.getName().substring(0, selectedNode.getName().lastIndexOf(".")) + ".png";
					currentThumbnailPath = currentThumbnailPath.replaceAll(" ", "%20");
					is.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					System.out.println("Thumbnail cannot be generated for " + selectedNode.getName() + ". Reason: " + e.getMessage());
	//				e.printStackTrace();
				}
				System.out.println("Time taken: "+ (System.currentTimeMillis()-start) + " ms");				
			}else{
				showThumbnail = false;
			}
//			System.out.println("Total time taken for " + firstLevelFiles.size() + " = " + (System.currentTimeMillis() -  startTime) + " ms");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SmbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private SmbFile getSmbObject(String path){
//		System.out.println(path);
		try {
			return new SmbFile(path);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public String getCompleteSmbPath(String path){
		ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
		String domain = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
		String username = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
		String password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
	      

//		  ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
//		  String domain = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
//		  String username = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
//		  String password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);

		
		DriveLetters driveLetter = null;
	      	List<DriveLetters> driveLetterList=driveLettersService.getAll();
	      	driveLetter = VirtualcodeUtil.getInstance().getDriveLetterMappings(path);
	      
	      	/*for(DriveLetters letter: driveLetterList){
				String sharePath = letter.getSharePath().toLowerCase();
				if(sharePath.startsWith("\\\\")){
//					ResourcesUtil ru = ResourcesUtil.getResourcesUtil();				
//					sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase() + "/fs/" + sharePath.substring(2).replaceAll("\\\\", "/");
				}
//				if(remotePath.endsWith("/"))
//					sharePath = sharePath.substring(0, sharePath.lastIndexOf("/"));
				if(!path.endsWith("\\"))
					path = path+ "\\";
				System.out.println(sharePath + ", " + path.toLowerCase());
	      	if(path.toLowerCase().contains(sharePath)){
	      		driveLetter = letter;
	      		break;
	      	}
	      }*/
			  if(driveLetter!=null && driveLetter.getRestoreDomain()!=null){
		 			domain =  driveLetter.getRestoreDomain();
		 			username = driveLetter.getRestoreUsername();
		 			password = driveLetter.getRestorePassword();
		 		}
			  
			  String searchPath = "smb://"+ domain +";"+ username +":"+ password +"@"+path.substring(2).replaceAll("\\\\", "/")+"/";
		return searchPath;
	}
	
	public String SHAsum(byte[] convertme) throws NoSuchAlgorithmException{
	    MessageDigest md = MessageDigest.getInstance("SHA-1"); 
	    return byteArray2Hex(md.digest(convertme));
	}

	private String byteArray2Hex(final byte[] hash) {
	    Formatter formatter = new Formatter();
	    for (byte b : hash) {
	        formatter.format("%02x", b);
	    }
	    return formatter.toString();
	}
	
	public void readyToSendLink(){
		String nodePath = getRequestParameter("nodePath");
		AdvancedSecureURL secureURL = AdvancedSecureURL.getInstance();
		try {
//			emailString = secureURL.getEncryptedURL(nodePath);
			emailString = nodePath;
//			fileExplorerService.archiveFile(nodePath, false);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private void createBreadCrumbs(String path){
		
		searchPathEnabled=false;
		
		if(path==null || path.trim().length()==0){
			return;
		}
		
		String[]  pathsArray=path.split("/");
		
		BreadCrumb breadCrumb;
		breadCrumbsList.clear();
		String name,folderPath;
		System.out.println("****create path*****  Full Path --->"+path);
		for(int i=0;pathsArray!=null && i<pathsArray.length;i++){
			
			
			if(i<4){
				continue;// ---/TestCompany/FS/192.168.30.249/
			}
			breadCrumb=new BreadCrumb();
			
			folderPath=createPath(pathsArray,i+1);
			name=pathsArray[i];
			name=createName(name,folderPath);
			System.out.println("********name after searching rootnodes...--"+pathsArray[i]);
			breadCrumb.setName(name);
			breadCrumb.setPath(folderPath);			
			breadCrumbsList.add(breadCrumb);
			
		}
		
		if(breadCrumbsList!=null && breadCrumbsList.size()>0){
			breadCrumbsList.get(breadCrumbsList.size()-1).setLast(true);
			
			while(breadCrumbsList.size() > 7){
				breadCrumbsList.remove(0);
			}
			
		}
		
		
		if(breadCrumbsList!=null && breadCrumbsList.size()>0){
			BreadCrumb bc=breadCrumbsList.get(breadCrumbsList.size()-1);
			//searchPath=bc.getPath();
			searchPath=getUNCPath(bc.getPath());
			/* String[] searchPArray=bc.getPath().split("/");//   /archive/FS/VC8/Books & Tutorials2/    
			int i=0;String temp="";
			while(searchPArray!=null && i<searchPArray.length){
				if(i<2){continue;}
				temp+=searchPArray[i];
				i++;
			}			
			searchPath="\\"+temp.replaceAll("/", "\\\\");
			*/
		}
		
	}
	
	public String getUNCPath(String path){
		String unc=null;
		if(path!=null){
			if(path.indexOf("/FS/")>0){
			  unc=path.substring(path.indexOf("/FS/")+4, path.length());
			}else if(path.indexOf("/SP/")>0){
				 unc=path.substring(path.indexOf("/SP/")+4, path.length());
		    }
		}
		if(unc!=null){
//			System.out.println("unc before replacing: "+unc);
			unc="\\\\"+unc.replaceAll("/", "\\\\");
		}
		return unc;
	}
	
	private String createName(String name,String folderPath){
		List<CustomTreeNode> shareNodesList=null;
		String tempFolderPath=folderPath,shareFolderPath;
		if(tempFolderPath.startsWith("/")){
			tempFolderPath=tempFolderPath.substring(1);
		}
		if(tempFolderPath.endsWith("/")){
			tempFolderPath=tempFolderPath.substring(0,tempFolderPath.length()-1);
		}
		if(rootNodes!=null){
			shareNodesList=rootNodes.get(0).getChildNodes();
		}
		for(int i=0;shareNodesList!=null && i<shareNodesList.size();i++){
			shareFolderPath=shareNodesList.get(i).getPath();
			if(shareFolderPath.startsWith("/")){
				shareFolderPath=shareFolderPath.substring(1);
			}
			System.out.println("path foldr:  "+tempFolderPath+"  ---sharefolderpath--- "+shareFolderPath);
			if(tempFolderPath.equals(shareFolderPath) ){
				
			if(shareFolderPath.endsWith("/")){
				shareFolderPath=shareFolderPath.substring(0,shareFolderPath.length()-1);
			}
				return shareNodesList.get(i).getName();
			}
		}
		return name;
	}
	
	private String createPath(String[] pathsArray,int index){
		String path="";
		for(int i=0;i<index;i++){
			path+=pathsArray[i]+"/";
			
		}
		return path;
	}
	
	private CustomTreeNode findNode(CustomTreeNode customTreeNode,String nodePath){
				
		    CustomTreeNode resultNode=null;
			if(customTreeNode.getPath().equalsIgnoreCase(nodePath)){
				return customTreeNode;
			}
		 List<CustomTreeNode> childsList=	customTreeNode.getChildNodes();
		 if(childsList==null || childsList.size()==0){
			 childsList=fileExplorerService.getFirstLevelDirctories(customTreeNode.getPath());
			 customTreeNode.setChildNodes(childsList);
		 }
		 for(int j=0;childsList!=null && j<childsList.size();j++){
			 resultNode=findNode(childsList.get(j),nodePath);
			 if(resultNode!=null){
				 break;
			 }
		 }	
		
		 return resultNode;
	}
	
    public void fileSelected(){
    	selectedNodePath=getRequestParameter("selectedNodePath");
    	String version=getRequestParameter("isVersion");
    	if(selectedNodePath!=null && ! selectedNodePath.trim().equals("")){
    		if(version.equals("true")){
    			this.selectedNode=fileExplorerService.getNodeByPath(selectedNodePath, true);
    			this.selectedNode.setOriginalPath(selectedNodePath);
    			this.selectedNode.setOriginalName(selectedNode.getName());
    			this.selectedNode.setDownloadUrl(com.virtualcode.util.StringUtils.encodeUrl(selectedNode.getDownloadUrl()));
    			this.selectedNode.setNextVersions(fileExplorerService.getAllVersions(selectedNode.getUuid()));    			
    			this.selectedNode.setVersion(true);
    			//set the name without version number
    			String tempName = this.selectedNode.getName();
    			if(tempName.indexOf("-")!=-1){
    				tempName = tempName.substring(0, tempName.lastIndexOf("-")).trim() + tempName.substring(tempName.lastIndexOf("-")+4).trim();
    			}
    			
//    			System.out.println(tempName);
    			this.selectedNode.setName(tempName);
    			tempName = this.selectedNode.getPath();
    			System.out.println(tempName);
    			if(tempName.indexOf("-")!=-1){
    				tempName = tempName.substring(0, tempName.lastIndexOf("-")).trim() + tempName.substring(tempName.lastIndexOf("-")+4).trim();
    			}
    			this.selectedNode.setPath(tempName); 			
    		}
    		else{
    			this.selectedNode=fileExplorerService.getNodeByPath(selectedNodePath);
    		}
    		if(selectedNode.getType().equals("nt:folder")){
    			selectedNode.setFileFoldersCount(fileExplorerService.getFileFoldersCount(selectedNode.getPath()));
    			//folderSizeMb=fileExplorerService.getFolderSize(selectedNode.getPath());
    			
    		}
    		if(selectedNode.getNextVersionsSize()>0) {
    			nextVersionsList=selectedNode.getNextVersions();
    			if(!version.equals("true")){
					for(CustomTreeNode tempNode:nextVersionsList){
						tempNode.setName(selectedNode.getName());
					}
    			}
    		}else if(selectedNode.getNextVersionsSize()<=0 && (selectedNode.getVersionOf()==null || selectedNode.getVersionOf().isEmpty())){
    			nextVersionsList=null;
    		}
    		selectedNodePath=null;
    		//get content thumbnail
    		currentThumbnailPath = "";
    		showThumbnail = false;
    		if(selectedNode.getType().equals("nt:file")){
	    		String fileExt = selectedNode.getName().substring(selectedNode.getName().lastIndexOf(".")).toLowerCase();
	    		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
	    		if("yes".equals(ru.getCodeValue("enable_thumbnails_summary",SystemCodeType.GENERAL))){
	    			
	    			if(fileExt.contains(".jpeg") || fileExt.contains(".bmp") || fileExt.contains(".gif")
	    					|| fileExt.contains(".png") || fileExt.contains(".jpg") || fileExt.contains(".pdf")){
	    				
	    				long start = System.currentTimeMillis();
	    				
	    				// TODO Auto-generated method stub
	    				try {
	    					String filePath = selectedNode.getPath();
	    					CustomTreeNode tempNode = null;
//	    					System.out.println("checking for version");
	    					if(selectedNode.isVersion()){
//	    						System.out.println("checking for version------------1");
//	    						CustomTreeNode node = fileExplorerService.getNodeByUUID(selectedNode.getUuid());
//	    						if(node!=null){
//	    							System.out.println("node name:" + node.getName());
//	    							filePath = node.getPath().substring(1);
//	    							System.out.println(filePath+"--------------------2");
//	    						}
	    						System.out.println(selectedNode.getOriginalPath()+"-------------");
	    						filePath = selectedNode.getOriginalPath(); 
	    					}else{
	    						if(selectedNode.getNextVersions()!=null && selectedNode.getNextVersionsSize()>0){
	    		            		tempNode = selectedNode.getNextVersions().get(selectedNode.getNextVersionsSize()-1);
	    		            		if(tempNode!=null){
	    		            			filePath = tempNode.getPath().substring(1);
	    		            		}
	    		            	}
	    					}
	    					InputStream is = fileExplorerService.downloadFile(filePath).getStream();
	    					if(fileExt.contains(".pdf")){
	    						try {
	//    							FileOutputStream fos = new FileOutputStream(new File("D:/images/newpdf.pdf"));
	//    							IOUtils.copyLarge(is, fos);
	//    							fos.close();
	    							Document document = new Document();
	    							document.setInputStream(is, "/tmp/");
	//    							PDDocument doc = PDDocument.load(is);
	//    							List<PDPage>pages =  doc.getDocumentCatalog().getAllPages();
	//    							PDPage page = pages.get(0);
	//    							PDFImageWriter imageWrite = new PDFImageWriter();
	//    							imageWrite.writeImage(doc, "png", null, 1 , 3, "D:/images/HeapSizepdf");
	    							BufferedImage image;// =page.convertToImage();//convertToImage();
	    							ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    							
	//    							ImageIO.write(image, "jpg", new File("D:/images/pdf.jpg"));
	    							image = (BufferedImage) document.getPageImage(0, GraphicsRenderingHints.SCREEN,
	    			                        Page.BOUNDARY_CROPBOX, 0.0f, 1.0f);
	//    							image1.g
	    							ImageIO.write(image, "png", baos);
	    							is = new ByteArrayInputStream(baos.toByteArray());
	    							
	//    							PDFImageWriter imageWrite = new PDFImageWriter();
	//    							imageWrite.writeImage(doc, "png", null, 1 , pages.size(), "D:/images/Heap Size");
	//    							File outputfile = new File("D:/images/Heap Size.png");
	//    							ImageIO.write(image, "png", outputfile);
	//    							baos.close();
	//    							fos.close();
	//    							doc.close();			
	    						} catch (IOException | PDFException | PDFSecurityException e) {
	    							// TODO Auto-generated catch block
	    							e.printStackTrace();
	    						}
	    					}
	    					
	    			        String sessionId=getHttpSession().getId();
	    			              
	    		            File outputDir=new File(getContextRealPath()+"/thumbnails/"+sessionId);
	    		            if(!outputDir.exists()){
	    		           	  outputDir.mkdirs();
	    		            }
	    		            String fileName = System.currentTimeMillis()+selectedNode.getName();
//	    		            if(!selectedNode.isVersion()){
//	    		            	fileName = tempNode.getName();	    		            	
//	    		            }else{
//	    		            	fileName = selectedNode.getOriginalName();
//	    		            }
	    		            FileUtils.cleanDirectory(outputDir);	// delete any existing files in the dir
	    					Thumbnails.of(is).size(200, 200).toFile(new File(getContextRealPath()+"/thumbnails/"+sessionId +"/" + fileName.substring(0, fileName.lastIndexOf("."))+".png"));
	    					System.out.println("Thumbnail creation successful....");
	    					showThumbnail = true;
	    					currentThumbnailPath = "thumbnails/"+sessionId +"/" + fileName.substring(0, fileName.lastIndexOf(".")) + ".png";
	    					currentThumbnailPath = currentThumbnailPath.replaceAll(" ", "%20");
	    					is.close();
	    				} catch (IOException | RepositoryException e) {
	    					// TODO Auto-generated catch block
	    					System.out.println("Thumbnail cannot be generated for " + selectedNode.getName() + ". Reason: " + e.getMessage());
	    	//				e.printStackTrace();
	    				}
	    				System.out.println("Time taken: "+ (System.currentTimeMillis()-start) + " ms");				
	    			}else{
	    				showThumbnail = false;
	    			}
	    		}
    		}
    		if(enablePdfView)
    			showPdfIcon = VCSUtil.isPdfViewable(selectedNode.getName());
    		if(emailUsingDefaultClient)
    			emailUsingDefaultClient();
    	}else{
    		selectedNodePath=null;
    		selectedNode=null;
    	}
    	
		  List<SystemCode> codesList=systemCodeService.getSystemCodeByCodename("max_attachment_size");  
		  int maxAttachmentSize=0;
		  if(codesList!=null && codesList.size()>0){
			  maxAttachmentSize=VCSUtil.getIntValue(codesList.get(0).getCodevalue());
		    }
		    String fs=selectedNode.getFileSize();
		    float fileSize=-1;
		    if(fs!=null){
		    	String[] arr=fs.split(" ");
		    	if(arr!=null && arr.length==2){
		    		fs=arr[0];
		    		fileSize=VCSUtil.getFloatValue(fs);
		    		if(arr[1].equalsIgnoreCase("MB")){
		    			fileSize=fileSize*1024;
		    		}
		    		if(arr[1].equalsIgnoreCase("GB")){
		    			fileSize=fileSize*1024*1024;
		    		}
		    	}
		    }
		    maxAttachmentSize=maxAttachmentSize*1024;
		   
		    if(maxAttachmentSize>0 && fileSize>0 && fileSize<maxAttachmentSize){
		    	emailFileSizeExceeded = false;
		    }else{
		    	emailFileSizeExceeded = true;
//		    	addErrorMessage("Email document size exceeds max attachment size", "Email document size exceeds max attachment size");
//		    	return;
		    }
	}
    
    public void smbFileSelected(){
    	String selectedNodePath = getRequestParameter("selectedNodePath");
    	showThumbnail = false;
    	getRemoteFileDetails(selectedNodePath);
    	
    }

    public void versionSelected(){
    	selectedNodePath=getRequestParameter("selectedNodePath");
    	if(selectedNodePath!=null && ! selectedNodePath.trim().equals("")){
    		
    		this.selectedNode=fileExplorerService.getNodeByPath(selectedNodePath, false);
    		if(selectedNode.getType().equals("nt:folder")){
    			selectedNode.setFileFoldersCount(fileExplorerService.getFileFoldersCount(selectedNode.getPath()));
    			//folderSizeMb=fileExplorerService.getFolderSize(selectedNode.getPath());
    			
    		}
    		if(selectedNode.getNextVersionsSize()>0) {
    			nextVersionsList=selectedNode.getNextVersions();
    			
				for(CustomTreeNode tempNode:nextVersionsList){
					tempNode.setName(selectedNode.getName());
				}
    		}else if(selectedNode.getNextVersionsSize()<=0 && (selectedNode.getVersionOf()==null || selectedNode.getVersionOf().isEmpty())){
    			nextVersionsList=null;
    		}
    		selectedNodePath=null;
    		//get content thumbnail
    		currentThumbnailPath = "";
    		showThumbnail = false;
    		
    		String fileExt = selectedNode.getName().substring(selectedNode.getName().lastIndexOf(".")).toLowerCase();
    		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
    		if(ru.getCodeValue("enable_thumbnails_summary",SystemCodeType.GENERAL).equals("yes")){
    			
    			if(fileExt.contains(".jpeg") || fileExt.contains(".bmp") || fileExt.contains(".gif")
    					|| fileExt.contains(".png") || fileExt.contains(".jpg") || fileExt.contains(".pdf")){
    				
    				long start = System.currentTimeMillis();
    				
    				// TODO Auto-generated method stub
    				try {
    					InputStream is = fileExplorerService.downloadFile(selectedNode.getPath()).getStream();
    					if(fileExt.contains(".pdf")){
    						try {
//    							FileOutputStream fos = new FileOutputStream(new File("D:/images/newpdf.pdf"));
//    							IOUtils.copyLarge(is, fos);
//    							fos.close();
    							Document document = new Document();
    							document.setInputStream(is, "/tmp/");
//    							PDDocument doc = PDDocument.load(is);
//    							List<PDPage>pages =  doc.getDocumentCatalog().getAllPages();
//    							PDPage page = pages.get(0);
//    							PDFImageWriter imageWrite = new PDFImageWriter();
//    							imageWrite.writeImage(doc, "png", null, 1 , 3, "D:/images/HeapSizepdf");
    							BufferedImage image;// =page.convertToImage();//convertToImage();
    							ByteArrayOutputStream baos = new ByteArrayOutputStream();
    							
//    							ImageIO.write(image, "jpg", new File("D:/images/pdf.jpg"));
    							image = (BufferedImage) document.getPageImage(0, GraphicsRenderingHints.SCREEN,
    			                        Page.BOUNDARY_CROPBOX, 0.0f, 1.0f);
//    							image1.g
    							ImageIO.write(image, "png", baos);
    							is = new ByteArrayInputStream(baos.toByteArray());
    							
//    							PDFImageWriter imageWrite = new PDFImageWriter();
//    							imageWrite.writeImage(doc, "png", null, 1 , pages.size(), "D:/images/Heap Size");
//    							File outputfile = new File("D:/images/Heap Size.png");
//    							ImageIO.write(image, "png", outputfile);
//    							baos.close();
//    							fos.close();
//    							doc.close();			
    						} catch (IOException | PDFException | PDFSecurityException e) {
    							// TODO Auto-generated catch block
    							e.printStackTrace();
    						}
    					}
    					
    			        String sessionId=getHttpSession().getId();
    			              
    		            File outputDir=new File(getContextRealPath()+"/thumbnails/"+sessionId);
    		            if(!outputDir.exists()){
    		           	  outputDir.mkdirs();
    		            }
    		            FileUtils.cleanDirectory(outputDir);	// delete any existing files in the dir
    					Thumbnails.of(is).size(200, 200).toFile(new File(getContextRealPath()+"/thumbnails/"+sessionId +"/" + selectedNode.getName().substring(0, selectedNode.getName().lastIndexOf("."))+".png"));
    					System.out.println("Thumbnail creation successful....");
    					showThumbnail = true;
    					currentThumbnailPath = "thumbnails/"+sessionId +"/" + selectedNode.getName().substring(0, selectedNode.getName().lastIndexOf(".")) + ".png";
    					currentThumbnailPath = currentThumbnailPath.replaceAll(" ", "%20");
    					is.close();
    				} catch (IOException | RepositoryException e) {
    					// TODO Auto-generated catch block
    					System.out.println("Thumbnail cannot be generated for " + selectedNode.getName() + ". Reason: " + e.getMessage());
    	//				e.printStackTrace();
    				}
    				System.out.println("Time taken: "+ (System.currentTimeMillis()-start) + " ms");				
    			}
    		}
    		if(enablePdfView)
    			showPdfIcon = VCSUtil.isPdfViewable(selectedNode.getName());
    		if(emailUsingDefaultClient)
    			emailUsingDefaultClient();
    	}else{
    		selectedNodePath=null;
    		selectedNode=null;
    	}
	}
    
    public void expandVersions(){
    	firstLevelFiles = oldFirstLevelFiles;

//    	if(selectedNode == null)
//    	{
    		selectedNodePath=getRequestParameter("selectedNodePath");
        	if(selectedNodePath!=null && ! selectedNodePath.trim().equals("")){        		
        		this.selectedNode=fileExplorerService.getNodeByPath(selectedNodePath);
        	}
//    	}
	 ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
	 String maxNoOfVersionStr = ru.getCodeValue("max_no_of_versions", "General");	
	 int maxNoOfVersion = 5;//new Integer(maxNoOfVersionStr);
	 if(maxNoOfVersionStr!=null)
		 maxNoOfVersion = new Integer(maxNoOfVersionStr);
	 
     List<CustomTreeNode> firstLevelFiles = null;
     List<CustomTreeNode> nextVersionsList = null;
   	 System.out.println("Selected Row Index: "+selectedNodePath);
		if(selectedNode.getNextVersionsSize()>0) {
			nextVersionsList=selectedNode.getNextVersions();			
		}else if(selectedNode.getNextVersionsSize()<=0 && (selectedNode.getVersionOf()==null || selectedNode.getVersionOf().isEmpty())){
			nextVersionsList=null;
		}
		if(nextVersionsList!=null && nextVersionsList.size()>0){
			selectedNode.setVersionExpanded(true);
			firstLevelFiles = new ArrayList<CustomTreeNode>();
			for(CustomTreeNode node:this.firstLevelFiles)
			{
				firstLevelFiles.add(node);
				if(node.getUuid().equals(selectedNode.getUuid()))
				{
					Collections.reverse(nextVersionsList);	//to 
//					nextVersionsList.add(selectedNode);
//					if(nextVersionsList.size()<=maxNoOfVersion){						
//						firstLevelFiles.addAll(nextVersionsList);
//					}else{
						int count = 0;
						//adding them in reverse order to show the latest first
						while(count<maxNoOfVersion){							
							CustomTreeNode tempNode = nextVersionsList.get(count);
							System.out.println(tempNode.getLastModified());
							tempNode.setName(selectedNode.getName());
							firstLevelFiles.add(tempNode);
							if(tempNode.getUuid().equals(selectedNode.getUuid())){
								tempNode.setVersion(true);
								tempNode.setNextVersions(null);
							}
							count++;
							if(count==nextVersionsList.size()){
								break;
							}
						}
//					}
				}
			}
			this.firstLevelFiles=filterNodes(firstLevelFiles);
		}
//    	if(selectedNodePath!=null && ! selectedNodePath.trim().equals("")){
//    		this.firstLevelFiles=fileExplorerService.getFirstLevelNodes(selectedNodePath);
//    		this.firstLevelFiles=filterNodes(firstLevelFiles);
//    	}
	 
//   	 	UIViewRoot root = FacesContext.getCurrentInstance().getViewRoot();
//		 UIComponent comp = root.findComponent("explorerGrid:searchGrid");    	
//       UIDataTable dataTable = (UIDataTable) comp;
//       Object originalKey = dataTable.getRowKey();
//       selectionItems.clear();
//      // for (Object selectionKey : selection) {
//           dataTable.setRowKey(selectedNodePath);
//           if (dataTable.isRowAvailable()) {
//           	CustomTreeNode node=(CustomTreeNode) dataTable.getRowData();
//           	selectionItems.add(node);
//           	
//               
//           }
       //}
//       dataTable.setRowKey(originalKey);
    }
    
    
    public void loadPermissions(){
    	String path=getRequestParameter("id");    	
    	if(path!=null && ! path.trim().equals("")){
    		//path=StringUtils.decodeUrl(path)==null?path:StringUtils.decodeUrl(path);	//modified for linked file support
    		this.selectedNode=fileExplorerService.getNodeByPath(path);
    		System.out.println("selection: " + selectedNode.getPath());
    		selectedNodePath = selectedNode.getPath();
    		//added for the pop-up security editor
    		secAllGroups = new ArrayList<SID>();
    		securityAllowGroups = new ArrayList<SID>();
    		securityDenyGroups = new ArrayList<SID>();
    		shareAllowGroups = new ArrayList<SID>();
    		shareDenyGroups = new ArrayList<SID>();
    		secAllUsers = new ArrayList<SID>();
    		shareAllUsers = new ArrayList<SID>();
    		shareAllGroups = new ArrayList<SID>();
    		
    		List sidsList = fileExplorerService.loadPermissions(selectedNode.getPath(),secAllGroups,securityAllowGroups,securityDenyGroups,shareAllGroups,shareAllowGroups,shareDenyGroups);
    		secAllGroups = (ArrayList<SID>)sidsList.get(0);
    		securityAllowGroups = (ArrayList<SID>)sidsList.get(1);
    		securityDenyGroups = (ArrayList<SID>)sidsList.get(2);
    		shareAllowGroups = (ArrayList<SID>)sidsList.get(3);
    		shareDenyGroups = (ArrayList<SID>)sidsList.get(4);
    		secAllUsers = (ArrayList<SID>)sidsList.get(5);
    		shareAllUsers = (ArrayList<SID>)sidsList.get(6);    				
    		
    	}else{    		
    		selectedNode=null;
    	}
	}
    
       
    public void selectionChanged(TreeSelectionChangeEvent selectionChangeEvent) {
    	System.out.println("-------------selection changed-----------------");
		long startTime = System.currentTimeMillis();
		log.debug("selectionChanged started.....................");
    	
        List<Object> selection = new ArrayList<Object>(selectionChangeEvent.getNewSelection());
        System.out.println(selection.size());
        if(selection!=null && selection.size()>0){
        Object currentSelectionKey = selection.get(0);
        UITree tree = (UITree) selectionChangeEvent.getSource();
//        System.out.println(tree.getSelection().size()+"------");
//        for(Object obj:tree.getSelection()){
//        	org.richfaces.model.SequenceRowKey key = (org.richfaces.model.SequenceRowKey)obj;
//        	System.out.println(key.getSimpleKeys().getClass());
////        	key.
//        }
        selectionChangeEvent.getSource();
        Object storedKey = tree.getRowKey();
        tree.setRowKey(currentSelectionKey);
       
        TreeNode sNode = (TreeNode) tree.getRowData();
        
       
    	CustomTreeNode customNode=(CustomTreeNode)sNode; 
    	System.out.println("Selected Node Path: --"+customNode.getPath());
    	if(customNode.getPath()==null || customNode.getPath().trim().equals("") || customNode.getPath().trim().equals("/")){
    		this.firstLevelFiles=filterNodes(fileExplorerService.getRootNodes());
    		return;
    	}
    	if(customNode.getType().trim().equals("nt:folder")){    		
    		this.parentNode=customNode;//assign it to parent node;
    		createBreadCrumbs(parentNode.getPath());//
    		this.firstLevelFiles=fileExplorerService.getFirstLevelNodes(customNode.getPath());
    		if(firstLevelFiles!=null){
    			System.out.println("Before Filtering childs: "+firstLevelFiles.size());
    		}
    		this.firstLevelFiles=filterNodes(firstLevelFiles);
    		if(firstLevelFiles!=null){
    			System.out.println("After Filtering childs: "+firstLevelFiles.size());
    		}
    	}else{
    		firstLevelFiles=null;
    	}     
    	oldFirstLevelFiles = firstLevelFiles;
    	//----
    	System.out.println("Selection: " + customNode.getName()+", selected:"+ customNode.isSelected());
    	log.debug("selectionChanged completed in: " + (System.currentTimeMillis() - startTime));
        }
        
        folderArchived = true;
        remoteFilesLoaded = false;
    }
    
    
	
    public void treeNodeToggled(TreeToggleEvent tge) {	    
    	System.out.println("-------------Tree node toggled-----------------");
    	long startTime = System.currentTimeMillis();
    	log.debug("treeNodeToggled started .......");
    	
		UITreeNode node = (UITreeNode)tge.getSource();
		UITree tree = (UITree)node.getParent();
		
		CustomTreeNode currentSelection = (CustomTreeNode)tree.getRowData();

		if(currentSelection.getChildNodes()==null || currentSelection.getChildNodes().size()<1){
			currentSelection.getChildNodes().clear();
			parentNode=currentSelection;
			
			if(currentSelection.getPath()!=null && !currentSelection.getPath().isEmpty()){
			   List<CustomTreeNode> firstLevelDirs=fileExplorerService.getFirstLevelDirctories(currentSelection.getPath());
			   firstLevelDirs=filterNodes(firstLevelDirs);
			   oldFirstLevelFiles = firstLevelDirs;
			   currentSelection.setChildNodes(firstLevelDirs);
			}
       // companyNode.getChildNodes().add(currentSelection);
		}else{
			//firstLevelDirs=currentSelection.getChildNodes();
		}
		
		folderArchived = true;
		
		log.debug("treeNodeToggled completed in ....... " + (System.currentTimeMillis() -  startTime));
	
	}
    
    
    
    public void selectionListener(AjaxBehaviorEvent event) {
        UIExtendedDataTable dataTable = (UIExtendedDataTable) event.getComponent();
        Object originalKey = dataTable.getRowKey();
        selectionItems.clear();
        for (Object selectionKey : selection) {
            dataTable.setRowKey(selectionKey);
            if (dataTable.isRowAvailable()) {
                selectionItems.add((CustomTreeNode) dataTable.getRowData());
            }
        }
        dataTable.setRowKey(originalKey);
    }
    
    
    public void  gridRowSelListener() {
    	 System.out.println("Selected Row Index: "+selectedNodePath);
    	 
    	 UIViewRoot root = FacesContext.getCurrentInstance().getViewRoot();
		 UIComponent comp = root.findComponent("explorerForm:searchGrid");    	
        UIExtendedDataTable dataTable = (UIExtendedDataTable) comp;
        Object originalKey = dataTable.getRowKey();
        selectionItems.clear();
       // for (Object selectionKey : selection) {
            dataTable.setRowKey(selectedNodePath);
            if (dataTable.isRowAvailable()) {
            	CustomTreeNode node=(CustomTreeNode) dataTable.getRowData();
            	selectionItems.add(node);
            	
                
            }
        //}
        dataTable.setRowKey(originalKey);
    }
    
    
    public void downloadFile(){
    	try{
    		System.out.println(selectedNode.isVersion());
    		
    	   String url=	"DownloadFile?nodeID="+selectedNode.getDownloadUrl();
    	   if(selectedNode.isVersion()){
    		   url += "&version=1"; 
    	   }
//    	   		"ownloadfile.jsp?
//    	   if(selectedNode.getName().endsWith(".pdf"))
//    		   url = "";
    	getHttpResponse().sendRedirect(url);
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    }
    
    public void downloadUncFile(){
    	try{
    	
    	   HttpServletResponse response = getHttpResponse();
    	   String fileName = selectedNode.getName();
           try {
        	String searchPath = getCompleteSmbPath(selectedNode.getPath());
   			long startTime = System.currentTimeMillis();
   			System.out.println("start time: " + startTime);
   			SmbFile remoteFile = new SmbFile(searchPath);
   			InputStream is = remoteFile.getInputStream();
               ServletOutputStream outs = response.getOutputStream();
               String contentType ;//= uCon.getContentType();

                String mimeType = URLConnection.guessContentTypeFromName(fileName);
                if (mimeType == null) {
                    if (fileName.endsWith(".doc")) {
                        mimeType = "application/msword";
                    } else if (fileName.endsWith(".xls")) {
                        mimeType = "application/vnd.ms-excel";
                    } else if (fileName.endsWith(".ppt")) {
                        mimeType = "application/mspowerpoint";
                    } else {
                        mimeType = "application/octet-stream";
                    }
                }
                //byte[] bytes = IOUtils.toByteArray(is);

                response.setContentLength(remoteFile.getContentLength());
                response.setContentType((mimeType != null) ? mimeType : "application/octet-stream");
                response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");



                int bit = 256;
                ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
                
                String tempSize	=	ru.getCodeValue("BUFF_SIZE_W", SystemCodeType.GENERAL);
	            int buffSize	=	8192;//defaultBufferSize
	            if(tempSize!=null && !tempSize.isEmpty()) {
	            	try {
	            		buffSize	=	Integer.parseInt(tempSize);
	            	}catch(Exception ex) {
	            		log.error(" Error to parse BUFF_SIZE_W: "+tempSize);
	            	}
	            }
	            log.debug(" write_buffered is: "+buffSize);
	            
                byte[] buffer = new byte[buffSize];
//                int bit;
                try {
                   
                      while ((bit = is.read(buffer, 0, buffSize)) != -1) {
                            outs.write(buffer, 0, bit);
                      }

                } catch (IOException ioe) {
                    log.debug("io excpetion 1 " + ioe.getMessage(),ioe);
                    ioe.printStackTrace();
                }

                try {
                    is.close();
                    outs.flush();
//                    log.debug("5555");
                    outs.close();
                } catch (IOException e) {
                    log.debug("io exception 2 " + e.getMessage(),e);
                    e.printStackTrace();
                }


        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            log.debug("malformed url exception " + e.getMessage(),e);
            throw new ServletException();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            log.debug("io expection 3 " + e.getMessage(),e);
            throw new ServletException();
        }
//    	getHttpResponse().sendRedirect(url);
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    }
    
    public String viewPDF(){
//    	try{    		
//    	   String url=	"PdfViewer.faces?faces-redirect=true&documentPath="+selectedNode.getDownloadUrl();
    	   String url= "../common/simple_document.jsp?doc=GroovyinAction.pdf"	;
//    	   
    	   
    	   return url;
//    	getHttpResponse().sendRedirect(url);    	   
//    	}catch(Exception ex){
//    		ex.printStackTrace();
//    	}
    }
    
    public void showPdf(){
     	documentManager.setDocumentPath(selectedNode.getDownloadUrl());
     	documentManager.openDocument();
     	String filename = documentManager.getDocumentName();
//     	filename = filename.substring(0, filename.lastIndexOf("."))+".pdf";
//        MessageDigest md;
//        StringBuffer sb = null;
//		try {
//			md = MessageDigest.getInstance("MD5");
//			System.out.println(filename.substring(0, filename.indexOf(".")));
//			md.update(filename.substring(0, filename.indexOf(".")).getBytes());
//			byte[] digest = md.digest();
//			sb = new StringBuffer();
//			for (byte b : digest) {
//				sb.append(Integer.toHexString((int) (b & 0xff)));
//			}
//		} catch (NoSuchAlgorithmException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}

//        filename = sb.toString() + filename.substring(filename.indexOf("."));//System.currentTimeMillis() + filename.substring(filename.indexOf(".")); //to handle foreign characters
     	urlForPdf = filename.replaceAll(" ", "_").toLowerCase();
     }
    
    public void showUncPdf(){
    	String smbPath = getCompleteSmbPath(selectedNode.getPath());
    	if(smbPath.endsWith("/")){
    		smbPath = smbPath.substring(0, smbPath.lastIndexOf("/"));
    	}
    	if(smbPath.endsWith("/")){
    		smbPath = smbPath.substring(0, smbPath.lastIndexOf("/"));
    	}
     	documentManager.setDocumentPath(smbPath);
     	documentManager.openDocument();
     	String filename = documentManager.getDocumentName();
//     	filename = filename.substring(0, filename.lastIndexOf("."))+".pdf";
//        MessageDigest md;
//        StringBuffer sb = null;
//		try {
//			md = MessageDigest.getInstance("MD5");
//			System.out.println(filename.substring(0, filename.indexOf(".")));
//			md.update(filename.substring(0, filename.indexOf(".")).getBytes());
//			byte[] digest = md.digest();
//			sb = new StringBuffer();
//			for (byte b : digest) {
//				sb.append(Integer.toHexString((int) (b & 0xff)));
//			}
//		} catch (NoSuchAlgorithmException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}

//        filename = sb.toString() + filename.substring(filename.indexOf("."));//System.currentTimeMillis() + filename.substring(filename.indexOf(".")); //to handle foreign characters
     	urlForPdf = filename.replaceAll(" ", "_").toLowerCase();
     }
    
    public void securityAllow(){
    	
    	UIViewRoot root = FacesContext.getCurrentInstance().getViewRoot();
//		 UIComponent comp = root.findComponent("permissionsForm:securityAllListBox");
		 UIComponent comp = root.findComponent("explorerGrid:securityAllListBox");
		 UISelectMany listBox=(UISelectMany)comp;
		 Object[] selectedValues=listBox.getSelectedValues();		 
		 String value;
		 for(int i=0;selectedValues!=null && i<selectedValues.length;i++){
			  value=(String)selectedValues[i];
			  for(int j=0;secAllGroups!=null && j<secAllGroups.size();j++){
				  SID sid=secAllGroups.get(j);
				  if(sid.getSid().equalsIgnoreCase(value)){
					  secAllGroups.remove(j);
					  securityAllowGroups.add(sid);
				  }
			  }
			  
			 
		 }
		 
		 //add user SID
		 //comp = root.findComponent("permissionsForm:securityAllusersListBox");
		 comp = root.findComponent("explorerGrid:securityAllusersListBox");
		 listBox=(UISelectMany)comp;
		 selectedValues=listBox.getSelectedValues();	 
		 
		 for(int i=0;selectedValues!=null && i<selectedValues.length;i++){
			  value=(String)selectedValues[i];
			  for(int j=0;secAllUsers!=null && j<secAllUsers.size();j++){
				  SID sid=secAllUsers.get(j);
				  if(sid.getSid().equalsIgnoreCase(value)){
					  secAllUsers.remove(j);
					  securityAllowGroups.add(sid);
				  }
			  }
		 }
    }
    
    public void removeSecurityAllow(){
    	
    	UIViewRoot root = FacesContext.getCurrentInstance().getViewRoot();
//		 UIComponent comp = root.findComponent("permissionsForm:securityAllowListBox");
		 UIComponent comp = root.findComponent("explorerGrid:securityAllowListBox");
		 UISelectMany listBox=(UISelectMany)comp;
		 Object[] selectedValues=listBox.getSelectedValues();		 
		 String value;
		 for(int i=0;selectedValues!=null && i<selectedValues.length;i++){
			  value=(String)selectedValues[i];
			  for(int j=0;securityAllowGroups!=null && j<securityAllowGroups.size();j++){
				  SID sid=securityAllowGroups.get(j);
				  if(sid.getSid().equalsIgnoreCase(value)){
					  if(!sid.isUserSid())	
						  secAllGroups.add(sid);
					  else secAllUsers.add(sid);
					  securityAllowGroups.remove(j);
				  }
			  }
			  
			 
		 }
    }    
    
 public void securityDeny(){
    	
    	UIViewRoot root = FacesContext.getCurrentInstance().getViewRoot();
//		 UIComponent comp = root.findComponent("permissionsForm:securityAllListBox");
		 UIComponent comp = root.findComponent("explorerGrid:securityAllListBox");
		 UISelectMany listBox=(UISelectMany)comp;
		 Object[] selectedValues=listBox.getSelectedValues();		 
		 String value;
		 for(int i=0;selectedValues!=null && i<selectedValues.length;i++){
			  value=(String)selectedValues[i];
			  for(int j=0;secAllGroups!=null && j<secAllGroups.size();j++){
				  SID sid=secAllGroups.get(j);
				  if(sid.getSid().equalsIgnoreCase(value)){
					  secAllGroups.remove(j);
					  securityDenyGroups.add(sid);
				  }
			  }
			  
			 
		 }
		 
		 //add user SID
//		 comp = root.findComponent("permissionsForm:securityAllusersListBox");
		 comp = root.findComponent("explorerGrid:securityAllusersListBox");
		 listBox=(UISelectMany)comp;
		 selectedValues=listBox.getSelectedValues();	 
		 
		 for(int i=0;selectedValues!=null && i<selectedValues.length;i++){
			  value=(String)selectedValues[i];
			  for(int j=0;secAllUsers!=null && j<secAllUsers.size();j++){
				  SID sid=secAllUsers.get(j);
				  if(sid.getSid().equalsIgnoreCase(value)){
					  secAllUsers.remove(j);
					  securityDenyGroups.add(sid);
				  }
			  }
		 }
    }

 public void removeSecurityDeny(){
 	
 	UIViewRoot root = FacesContext.getCurrentInstance().getViewRoot();
//		 UIComponent comp = root.findComponent("permissionsForm:securityDenyListBox");
		 UIComponent comp = root.findComponent("explorerGrid:securityDenyListBox");
		 UISelectMany listBox=(UISelectMany)comp;
		 Object[] selectedValues=listBox.getSelectedValues();		 
		 String value;
		 for(int i=0;selectedValues!=null && i<selectedValues.length;i++){
			  value=(String)selectedValues[i];			  
			  for(int j=0;securityDenyGroups!=null && j<securityDenyGroups.size();j++){
				  SID sid=securityDenyGroups.get(j);
				  if(sid.getSid().equalsIgnoreCase(value)){
					  if(!sid.isUserSid())								  
						  secAllGroups.add(sid);
					  else secAllUsers.add(sid);
					  securityDenyGroups.remove(j);
				  }
			  }
			  
			 
		 }
 }
    
    
	 public void shareAllow(){
	 	
	 	UIViewRoot root = FacesContext.getCurrentInstance().getViewRoot();
//			 UIComponent comp = root.findComponent("permissionsForm:shareAllListBox");
			 UIComponent comp = root.findComponent("explorerGrid:shareAllListBox");
			 UISelectMany listBox=(UISelectMany)comp;
			 Object[] selectedValues=listBox.getSelectedValues();		 
			 String value;
			 for(int i=0;selectedValues!=null && i<selectedValues.length;i++){
				  value=(String)selectedValues[i];
				  for(int j=0;shareAllGroups!=null && j<shareAllGroups.size();j++){
					  SID sid=shareAllGroups.get(j);
					  if(sid.getSid().equalsIgnoreCase(value)){
						  shareAllGroups.remove(j);
						  shareAllowGroups.add(sid);
					  }
				  }
				 
			 }
			 
			 //add user SID
//			 comp = root.findComponent("permissionsForm:shareAllusersListBox");
			 comp = root.findComponent("explorerGrid:shareAllusersListBox");
			 listBox=(UISelectMany)comp;
			 selectedValues=listBox.getSelectedValues();	 
			 
			 for(int i=0;selectedValues!=null && i<selectedValues.length;i++){
				  value=(String)selectedValues[i];
				  for(int j=0;shareAllUsers!=null && j<shareAllUsers.size();j++){
					  SID sid=shareAllUsers.get(j);
					  if(sid.getSid().equalsIgnoreCase(value)){
						  shareAllUsers.remove(j);
						  shareAllowGroups.add(sid);
					  }
				  }
			 }
	 }
	 
	 public void removeShareAllow(){
		 	System.out.println("removing share allow");
		 	UIViewRoot root = FacesContext.getCurrentInstance().getViewRoot();
//				 UIComponent comp = root.findComponent("permissionsForm:shareAllowListBox");
				 UIComponent comp = root.findComponent("explorerGrid:shareAllowListBox");
				 UISelectMany listBox=(UISelectMany)comp;
				 Object[] selectedValues=listBox.getSelectedValues();		 
				 String value;
				 for(int i=0;selectedValues!=null && i<selectedValues.length;i++){
					  value=(String)selectedValues[i];
					  for(int j=0;shareAllowGroups!=null && j<shareAllowGroups.size();j++){
						  SID sid=shareAllowGroups.get(j);
						  if(sid.getSid().equalsIgnoreCase(value)){
							  if(!sid.isUserSid())								  
								  shareAllGroups.add(sid);
							  else shareAllUsers.add(sid);
							  
							  shareAllowGroups.remove(j);
						  }
					  }
					  
					 
				 }
		 }
 
	public void shareDeny(){
	 	
	 	UIViewRoot root = FacesContext.getCurrentInstance().getViewRoot();
//			 UIComponent comp = root.findComponent("permissionsForm:shareAllListBox");
			 UIComponent comp = root.findComponent("explorerGrid:shareAllListBox");
			 UISelectMany listBox=(UISelectMany)comp;
			 Object[] selectedValues=listBox.getSelectedValues();		 
			 String value;
			 for(int i=0;selectedValues!=null && i<selectedValues.length;i++){
				  value=(String)selectedValues[i];
				  for(int j=0;shareAllGroups!=null && j<shareAllGroups.size();j++){
					  SID sid=shareAllGroups.get(j);
					  if(sid.getSid().equalsIgnoreCase(value)){
						  shareAllGroups.remove(j);
						  shareDenyGroups.add(sid);
					  }
				  }
				  
				 
			 }
			 
			 //add user SID
//			 comp = root.findComponent("permissionsForm:shareAllusersListBox");
			 comp = root.findComponent("explorerGrid:shareAllusersListBox");
			 listBox=(UISelectMany)comp;
			 selectedValues=listBox.getSelectedValues();	 
			 
			 for(int i=0;selectedValues!=null && i<selectedValues.length;i++){
				  value=(String)selectedValues[i];
				  for(int j=0;shareAllUsers!=null && j<shareAllUsers.size();j++){
					  SID sid=shareAllUsers.get(j);
					  if(sid.getSid().equalsIgnoreCase(value)){
						  shareAllUsers.remove(j);
						  shareDenyGroups.add(sid);
					  }
				  }
			 }
	 }

	public void removeShareDeny(){
	 	
	 	UIViewRoot root = FacesContext.getCurrentInstance().getViewRoot();
//			 UIComponent comp = root.findComponent("permissionsForm:shareDenyListBox");
			 UIComponent comp = root.findComponent("explorerGrid:shareDenyListBox");
			 UISelectMany listBox=(UISelectMany)comp;
			 Object[] selectedValues=listBox.getSelectedValues();		 
			 String value;
			 for(int i=0;selectedValues!=null && i<selectedValues.length;i++){
				  value=(String)selectedValues[i];
				  for(int j=0;shareDenyGroups!=null && j<shareDenyGroups.size();j++){
					  SID sid=shareDenyGroups.get(j);
					  if(sid.getSid().equalsIgnoreCase(value)){
						  if(!sid.isUserSid())	
							  shareAllGroups.add(sid);
						  else shareAllUsers.add(sid);
						  shareDenyGroups.remove(j);
					  }
				  }
				  
				 
			 }
	 }	
	

	
/*	public String savePermissions(){
		String success="FileExplorer";
		
		fileExplorerService.savePermissions(selectedNode.getPath(),securityAllowGroups,securityDenyGroups,shareAllowGroups,shareDenyGroups);
		
		
		return success;		
	}*/
	
	//---------------------Export Job
	public void checkBoxToggled(){
		System.out.println("-------------check Box toggled-----------------");
		String path = getRequestParameter("selectedFolderPath");
		path = selectedNodePath;
		System.out.println("path : " + path);
		if(path!=null && !path.trim().equals("")){
    		CustomTreeNode customTreeNode=null;
    		for(int i=0;rootNodes!=null && i<rootNodes.size();i++){
    			customTreeNode=findNode(rootNodes.get(i), selectedNodePath);    			
    			if(customTreeNode!=null)
    				break;
    		}
    		if(customTreeNode!=null){
    			System.out.println("Node name: "+ customTreeNode.getName());			
    			System.out.println("Node path: "+ customTreeNode.getPath());
    			System.out.println("Node selected: "+ customTreeNode.isSelected());
    		}
		}
		
		exportNodePath = jobController.getExportNodePath();
		if(exportNodePath==null || exportNodePath.trim().equals("")){
			setShowExport(false);
		}else{
			setShowExport(true);
		}
		
	}
	public void createExport(){
		System.out.println("------Setting up export job----------------------"+new Date());
		if(exportNodePath==null||exportNodePath.trim().length()<1){
//			return null;
		}
		String decodeStr="";
		try {
			decodeStr = URLDecoder.decode(exportNodePath, "UTF-8");
			System.out.println(decodeStr +"----"+new Date());			
		} catch (UnsupportedEncodingException e) {
			
			e.printStackTrace();
		}
		String[] paths = decodeStr.split("[|]");
		List<String> exportPathList = new ArrayList<String>();
		for(int i =0; i< paths.length;i++){
			System.out.println(paths[i]+"---------");
			if(!(paths[i].trim().equals(""))){
				exportPathList.add(paths[i]);	
			}			
		}
		
		if(exportPathList.size()<1){
//			return null;
		}else{
			// getHttpRequest().setAttribute("exportPathList", exportPathList);
			jobController.setExportPathStringList(exportPathList);
			jobController.createExportJob(exportPathList);
			
//			return "AddExportJob.faces";
			System.out.println("----Returning from createExport Job--"+new Date());
		}
	}
	
	public List<String> recursiveNodeIterator(CustomTreeNode node, List<String> exportPathList){
		System.out.println("node name: " + node.getName());
		if(node.isSelected()){
			exportPathList.add(node.getPath());
		}
		if(node.getChildCount()>0){			
			recursiveNodeIterator(node, exportPathList);			
		}
		return exportPathList;
	}

	 private boolean isLdapUser(){
		   String authMode=(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE);
		  if(authMode.equalsIgnoreCase("ad"))
			  return true;
		  else
		  return false;
	 } 
	
   public String getLoggedInRole(){
	   String roleName=(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_ROLE_NAME);
	   if(roleName==null || roleName.length()<1)
		   return "";
	   return roleName.toUpperCase();
   }
   
   public String syncPermissions(){
		String success="FileExplorer.faces?faces-redirect=true";
		System.out.println("selection: --------------"+selectedNodePath +", recursive="+isRecursive()+", is batch="+isBatchUpdate());		

		String tempRec = tempRecursive+tempBatchUpdate;
		setRecursive(tempRecursive.equals("true"));
		setBatchUpdate(tempBatchUpdate.equals("true"));
		setMerge(tempMerge.equals("true"));		
		
		success = fileExplorerService.syncPermissions(selectedNodePath,isRecursive(),isBatchUpdate(), isMerge())+"";
		selectedNode = fileExplorerService.getNodeByUUID(selectedNode.getUuid());//reload to show modified properties
		return success;		
	}
   
   public void prepareSyncPermissions(){
	
		System.out.println("selection: --------------"+selectedNodePath +", recursive="+isRecursive()+", is batch="+isBatchUpdate());
		if(selectedNodePath==null)
			selectedNodePath = selectedNode.getPath();

		tempRecursive = "false";
		tempBatchUpdate = "false";
		tempMerge = "false";
		boolean success = false;
		try{
			success = syncPermissions().trim().equals("true");
//			addInfoMessage("Permission update successful", "Permission update successful");
		}catch(Exception ex){
			ex.printStackTrace();			
//			addErrorMessage("Sync permissions failed, please verify tha the share exists", "failed");
		}
		
		if(success)
			addCompInfoMessage("explorerGrid:syncSource","Permission update successful", "Permission update successful");
		else
			addCompErrorMessage("explorerGrid:syncSource","Sync permissions failed, please verify that the share exists", "Sync permissions failed, please verify that the share exists");
		
		selectedNode = fileExplorerService.getNodeByUUID(selectedNode.getUuid());//reload to show modified properties
	}
	
	public String savePermissions(){
		String success="FileExplorer.faces?faces-redirect=true";
		System.out.println("selection: --------------"+selectedNodePath +", recursive="+isRecursive()+", is batch="+isBatchUpdate());
		
//		boolean flags[] = recursiveSet;
		String tempRec = tempRecursive+tempBatchUpdate;
		setRecursive(tempRecursive.equals("true"));
		setBatchUpdate(tempBatchUpdate.equals("true"));
		setMerge(tempMerge.equals("true"));
		
//		boolean flag = recursive;
//		flag = batchUpdate;
		
		//fileExplorerService.savePermissions(selectedNode.getPath(),securityAllowGroups,securityDenyGroups,shareAllowGroups,shareDenyGroups);
		fileExplorerService.savePermissions(selectedNodePath,securityAllowGroups,securityDenyGroups,shareAllowGroups,shareDenyGroups,isRecursive(),isBatchUpdate(), isMerge());
		selectedNode = fileExplorerService.getNodeByUUID(selectedNode.getUuid());//reload to show modified properties
		return success;		
	}
	
	   public void editTag(){
		   
//		   fileExplorerService.updateTags(uuid, tags);
		   String uid = getRequestParameter("selectedNode");

		   if(selectedNode.getTags()!=null && selectedNode.getTags().indexOf("|")!=-1){
//			   selectedNode.setTags(selectedNode.getTags().substring(selectedNode.getTags().lastIndexOf("|")+1));		   
		   }
		   
	   }
	
	public void updateTags(){
		System.out.println("tags added: " +selectedNode.getTags());
		String username=(String)getHttpRequest().getSession().getAttribute(VCSConstants.SESSION_USER_NAME);
   		
   		SimpleDateFormat sdf=new SimpleDateFormat("d MMM yyyy HH:mm:ss");
   		String timestamp =sdf.format(new Date());
   		String documentTags = fileExplorerService.updateTags(selectedNode.getUuid(), username +"|"+ timestamp +"|"+ selectedNode.getTags());
   		selectedNode.setTagUser(documentTags.substring(0, documentTags.indexOf("|")));
   		selectedNode.setTagTimeStamp(documentTags.substring(documentTags.indexOf("|")+1, documentTags.lastIndexOf("|")));        
        selectedNode.setTags(documentTags.substring(documentTags.lastIndexOf("|")+1));
		
	}

	
	private List<CustomTreeNode> filterNodes(List<CustomTreeNode> nodesList){
		
		String userSidsStr = "S-1-1-0,";
		String authType=(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE);
	    String userName=(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
	    List<CustomTreeNode> resultsList=new ArrayList<CustomTreeNode>();
	    
		if(authType!=null && authType.equalsIgnoreCase("ad")){			  
		   System.out.println("--getting sids for user: " + userName);
		   try{
		       VirtualCodeAuthorizatorImpl authorizator = VirtualCodeAuthorizatorImpl.getInstance();
		       userSidsStr += authorizator.sidString(userName);
		   }catch(Exception ex){ex.printStackTrace();}
	   
			String[] userSidsArr=userSidsStr.split(",");
			
			CustomTreeNode customTreeNode;
			for(int i=0;nodesList!=null && i<nodesList.size();i++){
			     customTreeNode=nodesList.get(i);
			     if(customTreeNode.getType().equalsIgnoreCase("nt:folder") && ! permissionsExist(customTreeNode)){
			    	 resultsList.add(customTreeNode);
//			    	 selection.add(customTreeNode);
			    	 continue;	
			     }
			     
				 if( ! isUserInDenyGroups(userSidsArr, customTreeNode.getSecurityDenySIDs(),customTreeNode.getShareDenySIDs())  &&  isUserInAllowedGroups(userSidsArr, customTreeNode.getSecurityAllowSIDs(),customTreeNode.getShareAllowSIDs()) ){
					 resultsList.add(customTreeNode);
//					 selection.add(customTreeNode);
				 }			  
			}	
				 
		  return resultsList;//ldap user,return filtered list		
	   }	    
	   return nodesList;//return orignal
	}
	
	
  private boolean permissionsExist(CustomTreeNode customTreeNode){
	  boolean exists=false;
	  String secAllow=customTreeNode.getSecurityAllowSIDs();
	  String secDeny=customTreeNode.getSecurityDenySIDs();
	  
	  String shareAllow=customTreeNode.getShareAllowSIDs();
	  String shareDeny=customTreeNode.getShareDenySIDs();
	  
	  if(secAllow!=null && ! secAllow.trim().equals("")){
		  exists=true;
	  }
	  
	  if(secDeny!=null && ! secDeny.trim().equals("")){
		  exists=true;
	  }
	  if(shareAllow!=null && ! shareAllow.trim().equals("")){
		  exists=true;
	  }
	 
	  if(shareDeny!=null && ! shareDeny.trim().equals("")){
		  exists=true;
	  }
	  return exists;
	  
  }
  
  private boolean areAncestorsInDenyGroups(String[] userSidsArray,CustomTreeNode customTreeNode){
	  boolean exists=false;
	  String secDeny=customTreeNode.getSecurityDenySIDs();
	  String shareDeny=customTreeNode.getShareDenySIDs();
	  
	  if(customTreeNode==null || customTreeNode.getPath()==null){
		  return false;
	  }
	  
	  if(isUserInDenyGroups(userSidsArray, secDeny, shareDeny)){
		  return true;
	  }
	  
	  String[] pathsArray=customTreeNode.getPath().split("/");
	  
	  int i=1;
	  String path="";
	  CustomTreeNode customTreeNodeTemp=null;
	  while(pathsArray!=null && i<pathsArray.length){
		  path+=pathsArray[i]+"/";
		  customTreeNodeTemp=fileExplorerService.getNodeByPath(path);
		  if(customTreeNodeTemp==null){continue;}
		  secDeny=customTreeNodeTemp.getSecurityDenySIDs();
		  shareDeny=customTreeNodeTemp.getShareDenySIDs();
		  
		  if(isUserInDenyGroups(userSidsArray, secDeny, shareDeny)){
			  exists=true;
			  break;
		  } 
		  
		  i++;		  
	  }
	  
	  return exists;
	 
  }
  
  private boolean areAncestorsInAllowedGroups(String[] userSidsArray,CustomTreeNode customTreeNode){
	  boolean exists=false;
	  String secAllow=customTreeNode.getSecurityDenySIDs();
	  String shareAllow=customTreeNode.getShareDenySIDs();
	  
	  if(customTreeNode==null || customTreeNode.getPath()==null){
		  return false;
	  }
	  
	  if(isUserInDenyGroups(userSidsArray, secAllow, shareAllow)){
		  return true;
	  }
	  
	  String[] pathsArray=customTreeNode.getPath().split("/");
	  
	  int i=1;
	  String path="";
	  CustomTreeNode customTreeNodeTemp=null;
	  while(pathsArray!=null && i<pathsArray.length){
		  path+=pathsArray[i]+"/";
		  customTreeNodeTemp=fileExplorerService.getNodeByPath(path);
		  if(customTreeNodeTemp==null){continue;}
		  secAllow=customTreeNodeTemp.getSecurityAllowSIDs();
		  shareAllow=customTreeNodeTemp.getShareAllowSIDs();
		  
		  if(isUserInAllowedGroups(userSidsArray, secAllow, shareAllow)){
			  exists=true;
			  break;
		  } 
		  
		  i++;		  
	  }
	  
	  return exists;
	 
  }
  
  private boolean areAncestorsInDenyGroups(String[] userSidsArray,String smbPath){
	  boolean exists=false;
	  
	  String secDeny= "";
	  String shareDeny= "";
	  
//	  DriveLetters driveLetter = null;
//      List<DriveLetters> driveLetterList=driveLettersService.getAll();
//      
//      for(DriveLetters letter: driveLetterList){
//			String sharePath = letter.getSharePath().toLowerCase();
//			if(sharePath.startsWith("\\\\")){
//				ResourcesUtil ru = ResourcesUtil.getResourcesUtil();				
//				sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase() + "/fs/" + sharePath.substring(2).replaceAll("\\\\", "/");
//			}
//			if(sharePath.endsWith("/"))
//				sharePath = sharePath.substring(0, sharePath.lastIndexOf("/"));			
//      	if(smbPath.toLowerCase().contains(sharePath)){
//      		driveLetter = letter;
//      		break;
//      	}
//      }
//	  	ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
//	  	String domain = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
// 		String username = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
// 		String password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
// 		if(driveLetter!=null){
// 			domain =  driveLetter.getRestoreDomain();
// 			username = driveLetter.getRestoreUsername();
// 			password = driveLetter.getRestorePassword();
// 		}
 		
// 		String searchPath = "smb://"+ domain +";"+ username +":"+ password +"@"+smbPath.replaceAll("\\\\", "/");
			SmbFile smbFile;
		try {
			smbFile = new SmbFile(smbPath);
 			if(smbFile.isDirectory()){
 				CifsUtil cifsutil =  new CifsUtil();
 				HashMap aclAttribs   =   CifsUtil.getAclAttribs(cifsutil.getACL(smbFile));
// 				List sidsList = new ArrayList();
 				
//              ArrayList<String> allowAces = new ArrayList<String>();
//              ArrayList<String> denyAces = new ArrayList<String>();
//              ArrayList<String> shareAllowAces = new ArrayList<String>();
//              ArrayList<String> shareDenyAces = new ArrayList<String>();
 				
//              if (aclAttribs.get("vc:shareAllowedSIDs")!=null) {
//                  String[] allowSids = aclAttribs.get("vc:shareAllowedSIDs").toString().split(",");
//                  shareAllowAces.addAll(Arrays.asList(allowSids));
//              }
//              sidsList.add(shareAllowAces);

              if (aclAttribs.get("vc:shareDeniedSIDs")!=null) {	                        
            	  shareDeny = aclAttribs.get("vc:shareDeniedSIDs").toString();
            	  System.out.println("shareDeny: " + shareDeny);
                  
//                  shareDenyAces.addAll(Arrays.asList(denySids));
              }
//              sidsList.add(shareDenyAces);

//              if (aclAttribs.get("vc:allowedSIDs")!=null) {	                        
//                  String[] allowSids = aclAttribs.get("vc:allowedSIDs").toString().split(",");
//                  allowAces.addAll(Arrays.asList(allowSids));
//              }
//              sidsList.add(allowAces);

              if (aclAttribs.get("vc:deniedSIDs")!=null) {
                  secDeny = aclAttribs.get("vc:deniedSIDs").toString();
                  System.out.println("secDeny: " + secDeny);
//                  denyAces.addAll(Arrays.asList(denySids));
              }
//              sidsList.add(denyAces);
//              String enable_stub_authorization = ru.getCodeValue("enable.stub.authorization", SystemCodeType.GENERAL);//resBundle.getString("enable.stub.authorization");
//              if (enable_stub_authorization != null && enable_stub_authorization.equalsIgnoreCase("yes")) {                            
//                  VirtualCodeAuthorizatorImpl authorizator = VirtualCodeAuthorizatorImpl.getInstance();
//                  return authorizator.authorize(username, sidsList);
//              }else{
//              	return true;
//              }	   				
 			
 			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SmbException e) {
			// TODO Auto-generated catch block
	        //handle smbException "The parameter is incorrect." in case of server name
			System.out.println(e.getMessage().trim());
			if(e.getMessage().trim().contains("The parameter is incorrect."))
				return exists;
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}

	 
	  
//	  if(customTreeNode==null || customTreeNode.getPath()==null){
//		  return false;
//	  }
	  
	  if(isUserInDenyGroups(userSidsArray, secDeny, shareDeny)){
		  return true;
	  }
	  
//	  String[] pathsArray=customTreeNode.getPath().split("/");
//	  
//	  int i=1;
//	  String path="";
//	  CustomTreeNode customTreeNodeTemp=null;
//	  while(pathsArray!=null && i<pathsArray.length){
//		  path+=pathsArray[i]+"/";
//		  customTreeNodeTemp=fileExplorerService.getNodeByPath(path);
//		  if(customTreeNodeTemp==null){continue;}
//		  secDeny=customTreeNodeTemp.getSecurityDenySIDs();
//		  shareDeny=customTreeNodeTemp.getShareDenySIDs();
//		  
//		  if(isUserInDenyGroups(userSidsArray, secDeny, shareDeny)){
//			  exists=true;
//			  break;
//		  } 
//		  
//		  i++;		  
//	  }
	  
	  return exists;
	 
  }
  
  private boolean areAncestorsInAllowedGroups(String[] userSidsArray,String smbPath){
	  boolean exists=false;
	  
	  String secAllow= "";
	  String shareAllow= "";

			SmbFile smbFile;
		try {
			smbFile = new SmbFile(smbPath);
 			if(smbFile.isDirectory()){
 				CifsUtil cifsutil =  new CifsUtil();
 				HashMap aclAttribs   =   CifsUtil.getAclAttribs(cifsutil.getACL(smbFile));

 				if (aclAttribs.get("vc:shareAllowedSIDs")!=null) {	                        
            	  shareAllow = aclAttribs.get("vc:shareAllowedSIDs").toString();
            	  System.out.println("shareAllow: " + shareAllow);
              }

              if (aclAttribs.get("vc:allowedSIDs")!=null) {
                  secAllow = aclAttribs.get("vc:allowedSIDs").toString();
                  System.out.println("secAllow: " + secAllow);
              }
 			
 			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SmbException e) {
			// TODO Auto-generated catch block
	        //handle smbException "The parameter is incorrect." in case of server name
			System.out.println(e.getMessage().trim());
			if(e.getMessage().trim().contains("The parameter is incorrect."))
				return exists;
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}

	 
	  
//	  if(customTreeNode==null || customTreeNode.getPath()==null){
//		  return false;
//	  }
	  
	  if(isUserInAllowedGroups(userSidsArray, secAllow, shareAllow)){
		  return true;
	  }
	  
//	  String[] pathsArray=customTreeNode.getPath().split("/");
//	  
//	  int i=1;
//	  String path="";
//	  CustomTreeNode customTreeNodeTemp=null;
//	  while(pathsArray!=null && i<pathsArray.length){
//		  path+=pathsArray[i]+"/";
//		  customTreeNodeTemp=fileExplorerService.getNodeByPath(path);
//		  if(customTreeNodeTemp==null){continue;}
//		  secDeny=customTreeNodeTemp.getSecurityDenySIDs();
//		  shareDeny=customTreeNodeTemp.getShareDenySIDs();
//		  
//		  if(isUserInDenyGroups(userSidsArray, secDeny, shareDeny)){
//			  exists=true;
//			  break;
//		  } 
//		  
//		  i++;		  
//	  }
	  
	  return exists;
	 
  }
	
  private boolean isUserInDenyGroups(String[] userSidsArray,String nodeSecurityDenySids,String nodeShareDenySids){
		
		String userSid;
		boolean exists=false;
		if( (nodeSecurityDenySids==null || nodeSecurityDenySids.trim().equals("") ) && (nodeShareDenySids==null || nodeShareDenySids.trim().equals("") ) ){
			return false;
		}
		for(int i=0;userSidsArray!=null && i<userSidsArray.length;i++){
			userSid=userSidsArray[i];
			if(nodeSecurityDenySids.contains(userSid) || nodeShareDenySids.contains(userSid)){
				exists= true;
				break;
			}
		}		
		return exists;
	}
  
  
  private boolean isUserInAllowedGroups(String[] userSidsArray,String nodeSecurityAllowSids,String nodeShareAllowSids){
		
		String userSid;
		boolean exists=false;
		
		if(nodeSecurityAllowSids==null || nodeShareAllowSids==null){
			return false;
		}
		 
		System.out.println("isUserInAllowedGroups:userSidsArray -->"+userSidsArray.toString());
		System.out.println("isUserInAllowedGroups: nodeSecurityAllowSids -->"+nodeSecurityAllowSids.toString());
		for(int i=0;userSidsArray!=null && i<userSidsArray.length;i++){
			userSid=userSidsArray[i];
			if(nodeSecurityAllowSids.contains(userSid) ){
				for(int j=0;j<userSidsArray.length;j++){					
					
					if(nodeShareAllowSids.contains(userSidsArray[j])){
						exists= true;
						break;
					}
				}
			 }
			
			if(exists){	break;}
		}				
		
		return exists;
	}
  
    public void sort(){
    	String sortBy = getRequestParameter("sortBy");
    	this.sortBy = sortBy;
    	if(this.sortBy.trim().equals(sortBy)){
    		toggleSortMode();
    	}else{
    		sortMode = "asc";
    	}
    	
    	if(sortBy.equalsIgnoreCase("name")){    		
    		sortByName();
    	}else if(sortBy.equalsIgnoreCase("date")){
    		sortByDate();
    	}else if(sortBy.equalsIgnoreCase("type")){
    		sortByType();
    	}else if(sortBy.equalsIgnoreCase("size")){
    		sortBySize();
    	}
    }
    
    public void toggleSortMode(){
    	if(sortMode !=null && sortMode.trim().equals("asc")){
    		sortMode = "desc";
    	}else{
    		sortMode = "asc";
    	}
    }
    
    private void sortByName(){
    	
    	if(firstLevelFiles!=null && firstLevelFiles.size()>0){
    		
    		Collections.sort(firstLevelFiles, new Comparator() {

	            public int compare(Object o1, Object o2) {
	            	CustomTreeNode doc1=(CustomTreeNode)o1;
	            	CustomTreeNode doc2=(CustomTreeNode)o2;
	                if(sortMode.trim().equals("asc"))
	                	return doc1.getName().compareToIgnoreCase(doc2.getName());
	                else
	                	return doc2.getName().compareToIgnoreCase(doc1.getName());
	            	
	            }
	        });
    	}    	
    }
    
    private void sortByDate(){
    	if(firstLevelFiles!=null && firstLevelFiles.size()>0){
    		
    	
 		Collections.sort(firstLevelFiles, new Comparator() {

             public int compare(Object o1, Object o2) {
            	CustomTreeNode doc1=(CustomTreeNode)o1;
            	CustomTreeNode doc2=(CustomTreeNode)o2;         	
                 Date date1=VCSUtil.getDate(doc1.getLastModified(),"yyyy-MM-dd HH:mm");
                 Date date2=VCSUtil.getDate(doc2.getLastModified(),"yyyy-MM-dd HH:mm");                
                 System.out.println(date1 + ", " + date2);
                 if(date1 == null){
                	 date1 = VCSUtil.getDate("3012-00-00 00:00", "yyyy-MM-dd HH:mm");
                 }
                 if(date2 == null){
                	 date2 = VCSUtil.getDate("3012-00-00 00:00", "yyyy-MM-dd HH:mm");
                 }
                 System.out.println(date1 + ", " + date2);
//                 if(sortMode.equalsIgnoreCase("desc")){ 
//                    return date2.compareTo(date1);
//                 }else{
                 if(sortMode.trim().equals("asc"))
                	 return date1.compareTo(date2);
                 else
                	 return date2.compareTo(date1);
//                 }
             }
                
         });
    	}
 		
// 	return list;
 	
    }
    
    private void sortByType(){
    	
    	if(firstLevelFiles!=null && firstLevelFiles.size()>0){
    		
    		Collections.sort(firstLevelFiles, new Comparator() {

	            public int compare(Object o1, Object o2) {
	            	CustomTreeNode doc1=(CustomTreeNode)o1;
	            	CustomTreeNode doc2=(CustomTreeNode)o2;
	                        //  	System.out.println("comparing : " + doc1.getFileTypeDesc() +" and " + doc2.getFileTypeDesc()+" : " +doc1.getFileTypeDesc().toLowerCase().compareTo(doc2.getFileTypeDesc().toLowerCase()));
	            	if(sortMode.trim().equals("asc"))
	            		return doc1.getFileTypeDesc().toLowerCase().compareTo(doc2.getFileTypeDesc().toLowerCase());
	            	else
	            		return doc2.getFileTypeDesc().toLowerCase().compareTo(doc1.getFileTypeDesc().toLowerCase());	            	
	            }
	        });
    	}    	
    }
    
    private void sortBySize(){
    	
    	if(firstLevelFiles!=null && firstLevelFiles.size()>0){
    		
    		Collections.sort(firstLevelFiles, new Comparator<Object>() {

	            public int compare(Object o1, Object o2) {
	            	CustomTreeNode doc1=(CustomTreeNode)o1;
	            	CustomTreeNode doc2=(CustomTreeNode)o2;
	            	Long size1 = 0l;
	            	Long  size2 = 0l;
//	            	String size1Str = doc1.getFileSize();
//	            	String size2Str = doc2.getFileSize();
	            	String size1Str = doc1.getSizeinKbs();
	            	String size2Str = doc2.getSizeinKbs();	            	
	                if(size1Str==null || size1Str.trim().length()<1 || !(doc1.getType().equals("nt:file"))){
	                	size1 = 0l;
	                
	                }else{
	                	size1 = Long.parseLong(doc1.getSizeinKbs().replaceAll(" KB", "").trim());
	                }
	                
	                if(size2Str==null || size2Str.trim().length()<1 || !(doc2.getType().equals("nt:file"))){
	                	size2 = 0l;
	                
	                }else{
	                	size2 = Long.parseLong(doc2.getSizeinKbs().replaceAll(" KB", "").trim());
	                }
	                System.out.println(size1 + ", " + size2);

//	                try{
	                
//	                }catch(Exception ex){
//	                	
//	                }
	                
//	                System.out.println(" ====="+ doc1.getFileSize().compareToIgnoreCase(doc2.getFileSize()));
	                if(size1 < size2){
	                	if(sortMode.trim().equals("asc"))
	                		return -1;
	                	else
	                		return 1;
	                }else if(size1 == size2){
	                	return 0;
	                }else{
	                	if(sortMode.trim().equals("asc"))
	                		return 1;
	                	else
	                		return -1;
	                }
	                
	            	//return (size1 > size2)?1:0;
	            	
	            }
	        });
    	}    	
    }
  
    
    private String mediaFilePath;
    
    public String getMediaFilePath() {
		return mediaFilePath;
	}
    
	public void setMediaFilePath(String mediaFilePath) {
		this.mediaFilePath = mediaFilePath;
	}

	public void playMediaFile(){
		 mediaFilePath=null;
    	 String fp=getRequestParameter("filePath");
    	 if(fp == null || fp.length()<1)
    		 fp = (String)getHttpRequest().getAttribute("filePath");
         String fileName=fp.substring(fp.lastIndexOf("/")+1);
         String fileExt=fileName.substring(fileName.lastIndexOf("."));
         int fileNumber=VCSUtil.getAbsolute(fp.hashCode());
         javax.jcr.Binary binaryStream =fileExplorerService.downloadFile(fp);
         String sessionId=getHttpSession().getId();
         try { 
            if(binaryStream!=null){
               InputStream is=binaryStream.getStream();
               byte[] buffer = new byte[4096];
               int bit = 256;
               
               File outputDir=new File(getContextRealPath()+"/streams/"+sessionId);
               if(!outputDir.exists()){
            	   outputDir.mkdirs();
               }
               File outputFile=new File(getContextRealPath()+"/streams/"+sessionId+"/"+fileNumber+fileExt);
                 
            	 if (!outputFile.exists()) {
               	    outputFile.createNewFile();
               	   
	               	FileWriter fw = new FileWriter(outputFile.getAbsoluteFile());
	           	    BufferedOutputStream buffOut = new BufferedOutputStream(new FileOutputStream(outputFile));
	               
	           	    while ((bit = is.read(buffer, 0, 4096)) != -1) {
	               	    buffOut.write(buffer,0,bit);	              
	                }
	                buffOut.flush();
	                buffOut.close();
	                fw.flush();
	                fw.close();
    			 }  
            	 
                 mediaFilePath="streams/"+sessionId+"/"+fileNumber+fileExt;
            }
           } catch (Exception ioe) {
        	   System.out.println("io excpetion 1 " + ioe.getMessage());
           }
          
     }
	
	public void emailUsingDefaultClient() {
		//mailto:lastname.firstname@xxx.com?subject=APPname%20support%20issue&amp;body=Version%20x.x%0D%0A%0D%0APlease%20make%20some%20descriptions%20here:%0D%0A%0D%0A%0D%0A&amp;attachment=C:\\resolv.conf
		String username = (String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		String dasUrl = ru.getCodeValue("DASUrl", "ZuesAgent");
		if(dasUrl==null || dasUrl.trim().length()<1)
			dasUrl = "http://sharearchiver:8080/ShareArchiver";
		String downloadLink = "";//dasUrl+"/downloadfile.jsp?nodeID="+ selectedNode.getDownloadUrl();
		if(linkOption.equals("1")|| !showPdfIcon)
			downloadLink = dasUrl+"/downloadfile.jsp?nodeID="+ selectedNode.getDownloadUrl();
		else
			downloadLink = dasUrl+"/ViewFile.faces?nodeID="+ selectedNode.getDownloadUrl();
		
		if(selectedNode.isVersion())
			downloadLink += "&version=1";
		String mailTo = "mailto:?";//SmbWriter.getJobStatusMessage(filePath);
		
		String msg = "";//"subject=Download Link";
		msg += "Hi" + ",  \n \t" + username + " wants to share this document link with you. \n";
//		msg += "Please click <a href="+downloadLink+">here</a> to download the file :\n";
		msg += "Please click the following link to download the file: \n"+downloadLink+"\n";
		msg += "Comments from " + username + ":" +	"\n";
		
//		msg = mailTo + msg;
//		System.out.println(msg);
//		if (msg == null || msg.trim().equals("")) {
//			return null;
//		}
//		System.out.println("Adding Message to View...****" + msg);
//		if (msg.indexOf("File restored successfully") != -1) {
////			addInfoMessage(msg, msg);
//			selectedNode.setCompressedURL(msg.substring(msg.indexOf("\\")));
//		} else {
//			addErrorMessage(msg, msg);
//		}
//		this.message = msg;
//		enabled = false;
//		try {
			emailString = msg;
//			fileExplorerService.updateEmailUserList(selectedNode.getUuid(), emailToLink);
			
//			getHttpResponse().sendRedirect(msg);
//			return;
//		} catch (IOException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return null;
	}
	
	public void emailLink(){
		
		if(!VCSUtil.validateEmailAddress(emailToLink)){
			addErrorMessage("Invalid email address.", "Invalid email address.");
			return;
		}
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		String dasUrl = ru.getCodeValue("DASUrl", "ZuesAgent");
		String url=	"";//dasUrl+"/downloadfile.jsp?nodeID="+selectedNode.getDownloadUrl();
		String enableSecurityString = ru.getCodeValue("enable_file_sharing_security", SystemCodeType.GENERAL);
		boolean enableSecurity = false;
		if(enableSecurityString!=null)
			enableSecurity = enableSecurityString.equals("yes")?true:false;
//		if(enableSecurity){
//			url = dasUrl+"/EmailUserLogin.faces?nodeID="+ selectedNode.getDownloadUrl();
//		}else{
			if(linkOption.equals("1") || !showPdfIcon)
				url = dasUrl+"/downloadfile.jsp?nodeID="+ selectedNode.getDownloadUrl();
			else
				url = dasUrl+"/ViewFile.faces?nodeID="+ selectedNode.getDownloadUrl();
//		}
		if(selectedNode.isArchive())
			url+="&version=1";
		
		String username =(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
		if(username !=null && username.length()>0){			
			username = username.substring(0,1).toUpperCase() + username.substring(1);		
		}       
		String messagebody="Dear,<br/>";		
		if(emailNameLink!=null && ! emailNameLink.isEmpty()){messagebody="Dear "+emailNameLink+",<br/><br/>";}
		
		messagebody+=username+" wants to share the following document link with you.<br/><br/>";
		
		messagebody+="Please click <a href="+url+">here</a> to gain access to the document \""+ selectedNode.getName()+"\"";
		
		if(enableSecurity){
			messagebody += "<br/><br/>You will be required to use your email ID and a password, the password will be sent to you shortly.";
		}
//		messagebody+="Document ID:"+selectedNode.getUuid();
		
		if(emailBodyLink!=null && !emailBodyLink.trim().isEmpty()){			
			messagebody+="<br/><br/>Comments from "+username+":<br/><br/>"+emailBodyLink;
		}		
		
		messagebody+="<br/><br/>Thanks<br/><br/>";
		 if(ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY)!=null)
			 messagebody += ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY) + " ";
		 messagebody += "File Share Service";
		if(emailSubjectLink==null || emailSubjectLink.length()<1)
			emailSubjectLink = "Shared Link";
		
		boolean success = false;
		if(emulateEmailSender){
			JespaUtil util = JespaUtil.getInstance();
			String emailFrom = util.getUserEmail(username);
			if(emailFrom!=null && emailFrom.length()>0)
				success=mailService.sendMail(emailFrom, this.emailToLink, emailSubjectLink, messagebody,null, null);
			else
				success=mailService.sendMail(this.emailToLink, emailSubjectLink, messagebody,null, null);
		}else{
			success=mailService.sendMail(this.emailToLink, emailSubjectLink, messagebody,null, null);
		}
		
		 if(success){
			 addInfoMessage("Email sent successfully!", "Email sent successfully!");		
			 fileExplorerService.updateEmailUserList(selectedNode.getUuid(), emailToLink);
			 //add email to DB
			 boolean userExists = false;
			 AdvancedSecureURL advSecureUrl = AdvancedSecureURL.getInstance();
			 LinkAccessUsers user = linkAccessUsersService.getLinkAccessUsersByRecipient(emailToLink);
//			 if(user!=null && !(user.getSender().equals(username))){	// if the recipient is same but sender is different. 
//				 user=null;
//			 }
			 String password = "";
			 if(user==null){
				 user = new LinkAccessUsers();
				 user.setRecipient(emailToLink);
				 password = PasswordGenerator.randomAlphaNumeric();
				 
				 try {
//					advSecureUrl.getEncryptedURL(password);
					user.setPassword(advSecureUrl.getEncryptedURL(password));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 
//				 user.setSender(username);
				 linkAccessUsersService.saveLinkAccessUsers(user);
			 }else{
				 userExists = true;
				 try {
					password = advSecureUrl.getDecryptedURL(user.getPassword());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			 }
			 LinkAccessDocuments document = null;
			 if(user.getId()!=null){
				 document = linkAccessDocumentsService.getLinkAccessDocumentsByUuidAndUser(user.getId()+"", selectedNode.getUuid());//modifcation required to select on basis of path and email
			 }
			 
			 if(document!=null && !document.getSender().equalsIgnoreCase(username)){ //if the document and the sender is different
				 document=null;
			 }
			 
			 if(document==null){
				 document = new LinkAccessDocuments();
				 document.setUuidDocuments(selectedNode.getUuid());
				 document.setLinkAccessUsers(user);
				 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				 document.setSharedOn(timestamp);
				 document.setSender(username);				 
				 Calendar c = Calendar.getInstance();
				 c.setTime(new Date(timestamp.getTime()));
				 int noOfDays = 7;
				 if(ru.getCodeValue("shared_link_validity", SystemCodeType.GENERAL)!=null){
					 noOfDays = new Integer(ru.getCodeValue("shared_link_validity", SystemCodeType.GENERAL));
				 }
				 if(noOfDays==0){
					 noOfDays=10000;	//never expires
					 document.setLinkExpireDate(null);
				 }else{
					 c.add(Calendar.DATE, noOfDays);
					 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					 System.out.println(sdf.format(c.getTime()));
					 document.setLinkExpireDate(new Timestamp(c.getTimeInMillis()));
				 }
				 linkAccessDocumentsService.saveLinkAccessDocuments(document);				 
			 }else{
				 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				 document.setSharedOn(timestamp);
				 Calendar c = Calendar.getInstance();
				 c.setTime(new Date(timestamp.getTime()));
				 int noOfDays = 7;
				 if(ru.getCodeValue("shared_link_validity", SystemCodeType.GENERAL)!=null){
					 noOfDays = new Integer(ru.getCodeValue("shared_link_validity", SystemCodeType.GENERAL));
				 }
				 if(noOfDays==0){
					 noOfDays=10000;	//never expires
					 document.setLinkExpireDate(null);
				 }else{
					 c.add(Calendar.DATE, noOfDays);
					 document.setLinkExpireDate(new Timestamp(c.getTimeInMillis()));
				 }
				 linkAccessDocumentsService.saveLinkAccessDocuments(document);
			 }
			 
			 
			 
			 
			 if(!userExists){
//				 Subject should be "File Access Password"
//
//				 Dear user
//
//				 A file has been shared with you, a link was sent to you in a separate email.
//				  
//				 Since you are using this service for the first time, a password has been generated for you as "password".
//
//				 You may use your account details and access the file. you may also update the password in the user preferences area.
//
//				 Thank you 
//
//				 Prefferred-Name File Share Service
				 String emailBody = "Dear user <br/><br/>";
				 emailBody += "A file has been shared with you, a link was sent to you in a separate email. <br/>";
				 emailBody += "Since you are using this service for the first time, a password has been generated for you as \""+password+"\".<br/>";
				 emailBody += "You may use your account details and access the file. you may also update the password in the user preferences area.<br/>";
				 emailBody += "Thank You <br/><br/>";
				 if(ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY)!=null)
					 emailBody += ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY) + " ";
				 emailBody += "File Share Service";
				 
//				 ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY);

//				 mailService.sendMail(this.emailToLink, "File Access Password", "Your ShareArchiver password is: " + password,null, null);
				 mailService.sendMail(this.emailToLink, "File Access Password", emailBody,null, null);
			 }
			 
			 document.setLinkAccessUsers(user);
			 
			 //send notification to Data gaurdian(s)
			 List<DataGuardian> dataGuardianList = dataGuardianService.getAll();
			 for(DataGuardian dataGuardian: dataGuardianList){
				 
			
//			 if(groupGuardian!=null && groupGuardian.getEnableTranscript()==1 && groupGuardian.getGroupGuardian()!= null && groupGuardian.getGroupGuardian().length()>0)
//			 {
		    		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		    		String emailBody = "";
		    		emailBody += "A document has been shared with user :"+ emailToLink;
		    		emailBody += "Shared on: " +  df.format(new Date());
		    		emailBody += "File Name: " + selectedNode.getName() + "<br/>";
		    		emailBody += "File UUID: " + selectedNode.getUuid() + "<br/>";
		    		emailBody += "File Path: " + selectedNode.getPath() + "<br/><br/>";
		    		emailBody += "Shared By: " + username + "<br/><br/>";
//		    		emailBody += "Shared by user ID "+ username +" on Date:" + df.format(new Date()) +"<br/><br/>";
//		    		emailBody += "has been accessed by the recipient on Date :"+ df.format(new Date()) +"<br/><br/>";
		    		emailBody += "Thank You <br/><br/>";
					 if(ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY)!=null)
						 emailBody += ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY) + " ";
					 emailBody += "File Share Service";
					 
					 String fromEmail = ru.getCodeValue("guardian_from_address", SystemCodeType.GENERAL);
					 if(fromEmail==null || fromEmail.length()<1)
						 mailService.sendMail(dataGuardian.getDataGuardianEmail(), "File Sharing Notification", emailBody,null, null);
					 else
						 mailService.sendMail(fromEmail, dataGuardian.getDataGuardianEmail(), "File Sharing Notification", emailBody,null, null);
//			 }
			 }
			 
			 
		 }else{
			 addErrorMessage("Error in sending email!", "Error in sending email!");
		 }
	}
	
	public void emailUncUsingDefaultClient() {
		//mailto:lastname.firstname@xxx.com?subject=APPname%20support%20issue&amp;body=Version%20x.x%0D%0A%0D%0APlease%20make%20some%20descriptions%20here:%0D%0A%0D%0A%0D%0A&amp;attachment=C:\\resolv.conf
		String username = (String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		String dasUrl = ru.getCodeValue("DASUrl", "ZuesAgent");
		if(dasUrl==null || dasUrl.trim().length()<1)
			dasUrl = "http://sharearchiver:8080/ShareArchiver";
		String nodePath = getRequestParameter("nodePath");
		AdvancedSecureURL secureURL = AdvancedSecureURL.getInstance();
		try {
			nodePath = secureURL.getEncryptedURL(nodePath);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String downloadLink = "";//dasUrl+"/downloadfile.jsp?nodeID="+ selectedNode.getDownloadUrl();
//		if(linkOption.equals("1")|| !showPdfIcon)
			downloadLink = dasUrl+"/downloadfile.jsp?nodeID=na-"+ nodePath;
			System.out.println(downloadLink);
//		else
//			downloadLink = dasUrl+"/ViewFile.faces?nodeID="+ selectedNode.getDownloadUrl();
		String mailTo = "mailto:?";//SmbWriter.getJobStatusMessage(filePath);
		
		String msg = "";//"subject=Download Link";
		msg += "Hi" + ",  \n \t" + username + " wants to share this document link with you. \n";

		msg += "Please click the following link to download the file: \n"+downloadLink+"\n";
		//to gain access to the document \""+ selectedNode.getName()+"\""
		msg += "Comments from " + username + ":" +	"\n";
		

			emailString = msg;

	}
	
	public void emailUncLink(){
		
		if(!VCSUtil.validateEmailAddress(emailSmbToLink)){
			addErrorMessage("Invalid email address.", "Invalid email address.");
			return;
		}
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		String dasUrl = ru.getCodeValue("DASUrl", "ZuesAgent");
		String url=	"";//dasUrl+"/downloadfile.jsp?nodeID="+selectedNode.getDownloadUrl();
		String nodePath = "";//getRequestParameter("nodePath");
		Long identifier = System.currentTimeMillis();
		AdvancedSecureURL secureURL = AdvancedSecureURL.getInstance();
//		try {
//			nodePath = secureURL.getDecryptedURL(emailString);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
//		System.out.println(System.currentTimeMillis() + ",  " + new String(emailString.getBytes()));
		getRemoteFileDetails(emailString);
//		System.out.println(selectedNode.getHash());
//		if(linkOption.equals("1") || !showPdfIcon)
		String enableSecurityString = ru.getCodeValue("enable_file_sharing_security", SystemCodeType.GENERAL);
		boolean enableSecurity = false;
		if(enableSecurityString!=null)
			enableSecurity = enableSecurityString.equals("yes")?true:false;
		try {
			nodePath = emailString;
			emailString = secureURL.getEncryptedURL(emailString);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			url = dasUrl+"/downloadfile.jsp?nodeID=na-"+ identifier;//emailString;
//		else
//			url = dasUrl+"/ViewFile.faces?nodeID="+ selectedNode.getDownloadUrl();
		
		String username =(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
		if(username !=null && username.length()>0){			
			username = username.substring(0,1).toUpperCase() + username.substring(1);		
		}       
		String messagebody="Dear,<br/>";		
		if(emailSmbNameLink!=null && ! emailSmbNameLink.isEmpty()){messagebody="Dear "+emailSmbNameLink+",<br/><br/>";}
		
		messagebody+=username+" wants to share the following document link with you.<br/><br/>";
		
		messagebody+="Please click <a href="+url+">here</a> to download the file";
		
		if(enableSecurity){
			messagebody += "<br/><br/>You will be required to use your email ID and a password, the password will be sent to you shortly.";
		}
		
		if(emailSmbBodyLink!=null && !emailSmbBodyLink.trim().isEmpty()){			
			messagebody+="<br/><br/>Comments from "+username+":<br/><br/>"+emailSmbBodyLink;
		}	
		
		messagebody+="<br/><br/>Thanks<br/><br/>";
		 if(ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY)!=null)
			 messagebody += ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY) + " ";
		 messagebody += "File Share Service";
		
		if(emailSmbSubjectLink==null || emailSmbSubjectLink.length()<1)
			emailSmbSubjectLink = "Shared Link";
		
		boolean success=false;//mailService.sendMail(this.emailSmbToLink, emailSmbSubjectLink, messagebody,null, null);
		
//		boolean success = false;
		if(emulateEmailSender){
			JespaUtil util = JespaUtil.getInstance();
			String emailFrom = util.getUserEmail(username);
			if(emailFrom!=null && emailFrom.length()>0)
				success=mailService.sendMail(emailFrom, this.emailSmbToLink, emailSmbSubjectLink, messagebody,null, null);
			else
				success=mailService.sendMail(this.emailSmbToLink, emailSmbSubjectLink, messagebody,null, null);
		}else{
			success=mailService.sendMail(this.emailSmbToLink, emailSmbSubjectLink, messagebody,null, null);
		}
		
		 if(success){
			 addInfoMessage("Email sent successfully!", "Email sent successfully!");
			 String archiveTarget = ru.getCodeValue("copy_target_file_to_archive", SystemCodeType.GENERAL); 
			 if(archiveTarget!=null && archiveTarget.equals("yes"))	 
				 fileExplorerService.archiveFile(nodePath, false);
			 
			 boolean userExists = false;
			 AdvancedSecureURL advSecureUrl = AdvancedSecureURL.getInstance();
			 LinkAccessUsers user = linkAccessUsersService.getLinkAccessUsersByRecipient(emailSmbToLink);
//			 if(user!=null && !(user.getSender().equals(username))){	// if the recipient is same but sender is different. 
//				 user=null;
//			 }
			 String password = "";
			 if(user==null){
				 user = new LinkAccessUsers();
				 user.setRecipient(emailSmbToLink);
				 password = PasswordGenerator.randomAlphaNumeric();
				 
				 try {
//					advSecureUrl.getEncryptedURL(password);
					user.setPassword(advSecureUrl.getEncryptedURL(password));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 
//				 user.setSender(username);
				 linkAccessUsersService.saveLinkAccessUsers(user);				 
			 }else{
				 userExists = true;
				 try {
					password = advSecureUrl.getDecryptedURL(user.getPassword());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			 }
			 LinkAccessDocuments document = null;
			 if(user.getId()!=null){
				 document = linkAccessDocumentsService.getLinkAccessDocumentsByUuidAndUser(user.getId()+"", "na-"+emailString);//modifcation required to select on basis of path and email
			 }
			 
			 if(document!=null && !document.getSender().equalsIgnoreCase(username)){ //if the document and the sender is different
				 document=null;
			 }
			 
			 if(document==null){
				 document = new LinkAccessDocuments();
				 document.setUuidDocuments("na-"+emailString);
				 document.setLinkAccessUsers(user);
//				 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//				 document.setSharedOn(timestamp);
//				 Calendar c = Calendar.getInstance();
//				 c.setTime(new Date(timestamp.getTime()));
//				 int noOfDays = 7;
//				 if(ru.getCodeValue("shared_link_validity", SystemCodeType.GENERAL)!=null){
//					 noOfDays = new Integer(ru.getCodeValue("shared_link_validity", SystemCodeType.GENERAL));
//				 }
//				 if(noOfDays==0){
//					 noOfDays=10000;
//					 
//				 }
//				 c.add(Calendar.DATE, noOfDays);
//				 document.setLinkExpireDate(new Timestamp(c.getTimeInMillis()));
//				 document.setSender(username);
//				 document.setHash(selectedNode.getHash());
//				 document.setIdentifier(identifier);
//				 linkAccessDocumentsService.saveLinkAccessDocuments(document);				 
			 }else{				 
//				 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//				 document.setSharedOn(timestamp);
//				 Calendar c = Calendar.getInstance();
//				 c.setTime(new Date(timestamp.getTime()));
//				 int noOfDays = 7;
//				 if(ru.getCodeValue("shared_link_validity", SystemCodeType.GENERAL)!=null){
//					 noOfDays = new Integer(ru.getCodeValue("shared_link_validity", SystemCodeType.GENERAL));
//				 }
//				 if(noOfDays==0)
//					 noOfDays=10000;
//				 c.add(Calendar.DATE, noOfDays);
//				 document.setLinkExpireDate(new Timestamp(c.getTimeInMillis()));
//				 document.setIdentifier(identifier);
//				 document.setHash(selectedNode.getHash());
//				 linkAccessDocumentsService.saveLinkAccessDocuments(document);
			 }
			 
			 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			 document.setSharedOn(timestamp);
			 Calendar c = Calendar.getInstance();
			 c.setTime(new Date(timestamp.getTime()));
			 int noOfDays = 7;
			 if(ru.getCodeValue("shared_link_validity", SystemCodeType.GENERAL)!=null){
				 noOfDays = new Integer(ru.getCodeValue("shared_link_validity", SystemCodeType.GENERAL));
			 }
			 if(noOfDays==0){
				 noOfDays=10000;
				 document.setLinkExpireDate(null);
			 }else{
				 c.add(Calendar.DATE, noOfDays);
				 document.setLinkExpireDate(new Timestamp(c.getTimeInMillis()));
			 }
			 document.setSender(username);
			 document.setHash(selectedNode.getHash());
			 document.setIdentifier(identifier);				 

			 linkAccessDocumentsService.saveLinkAccessDocuments(document);
			 
			 
			 if(!userExists){
//				 mailService.sendMail(this.emailSmbToLink, "ShareArchiver User Credentials", "Your ShareArchiver password is: " + password,null, null);
				 String emailBody = "Dear user <br/><br/>";
				 emailBody += "A file has been shared with you, a link was sent to you in a separate email. <br/>";
				 emailBody += "Since you are using this service for the first time, a password has been generated for you as \""+password+"\".<br/>";
				 emailBody += "You may use your account details and access the file. you may also update the password in the user preferences area.<br/>";
				 emailBody += "Thank You <br/><br/>";
				 if(ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY)!=null)
					 emailBody += ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY) + " ";
				 emailBody += "File Share Service";

				 mailService.sendMail(this.emailSmbToLink, "File Access Password", emailBody,null, null);
			 }
				 
			 
			 document.setLinkAccessUsers(user);
			 
			 //send notification to Data gaurdian(s)
			 List<DataGuardian> dataGuardianList = dataGuardianService.getAll();
			 for(DataGuardian dataGuardian: dataGuardianList){
				 
			
//			 if(groupGuardian!=null && groupGuardian.getEnableTranscript()==1 && groupGuardian.getGroupGuardian()!= null && groupGuardian.getGroupGuardian().length()>0)
//			 {
		    		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		    		String emailBody = "";
		    		emailBody += "A document has been shared with user :"+ emailSmbToLink;
		    		emailBody += "Shared on: " +  df.format(new Date());
		    		emailBody += "File Name: " + selectedNode.getName() + "<br/>";
		    		emailBody += "File UUID: " + selectedNode.getUuid() + "<br/>";
		    		emailBody += "File Path: " + selectedNode.getPath() + "<br/><br/>";
		    		emailBody += "Shared By: " + username + "<br/><br/>";
//		    		emailBody += "Shared by user ID "+ username +" on Date:" + df.format(new Date()) +"<br/><br/>";
//		    		emailBody += "has been accessed by the recipient on Date :"+ df.format(new Date()) +"<br/><br/>";
		    		emailBody += "Thank You <br/><br/>";
					 if(ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY)!=null)
						 emailBody += ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY) + " ";
					 emailBody += "File Share Service";
					 
					 String fromEmail = ru.getCodeValue("guardian_from_address", SystemCodeType.GENERAL);
					 if(fromEmail==null || fromEmail.length()<1)
						 mailService.sendMail(dataGuardian.getDataGuardianEmail(), "File Sharing Notification", emailBody,null, null);
					 else
						 mailService.sendMail(fromEmail, dataGuardian.getDataGuardianEmail(), "File Sharing Notification", emailBody,null, null);
//			 }
			 }
		 }else{
			 addErrorMessage("Error in sending email!", "Error in sending email!");
		 }
	}
	
	public void emailFile(){
		
		if(!VCSUtil.validateEmailAddress(emailToFile)){
			addErrorMessage("Invalid email address", "Invalid email address");
			return;
		}
		 String fileName=selectedNode.getName();
		 String filePath=this.selectedNode.getPath();	
		  List<SystemCode> codesList=systemCodeService.getSystemCodeByCodename("max_attachment_size");  
		  int maxAttachmentSize=0;
		  if(codesList!=null && codesList.size()>0){
			  maxAttachmentSize=VCSUtil.getIntValue(codesList.get(0).getCodevalue());
		    }
		    String fs=selectedNode.getFileSize();
		    float fileSize=-1;
		    if(fs!=null){
		    	String[] arr=fs.split(" ");
		    	if(arr!=null && arr.length==2){
		    		fs=arr[0];
		    		fileSize=VCSUtil.getFloatValue(fs);
		    		if(arr[1].equalsIgnoreCase("MB")){
		    			fileSize=fileSize*1024;
		    		}
		    		if(arr[1].equalsIgnoreCase("GB")){
		    			fileSize=fileSize*1024*1024;
		    		}
		    	}
		    }
		    maxAttachmentSize=maxAttachmentSize*1024;
		   
		    if(maxAttachmentSize>0 && fileSize>0 && fileSize<maxAttachmentSize){
		    	
		    }else{
		    	addErrorMessage("Email document size exceeds max attachment size", "Email document size exceeds max attachment size");
		    	return;
		    }
			String username =(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
			if(username !=null && username.length()>0){			
				username = username.substring(0,1).toUpperCase() + username.substring(1);		
			}       
			String messagebody="Dear,<br/>";		
			if(emailNameFile!=null && ! emailNameFile.isEmpty()){messagebody="Dear "+emailNameFile+",<br/><br/>";}
			
			messagebody+=username+" wants to share the attached document with you.<br/><br/>";
			if(emailBodyFile!=null && !emailBodyFile.trim().isEmpty()){
				
				messagebody+="Comments from "+username+":<br/><br/>"+emailBodyFile;
			}
						
			messagebody+="<br/><br/>Thanks<br/><br/>";
			ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
			 if(ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY)!=null)
				 messagebody += ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY) + " ";
			 messagebody += "File Share Service";
			
			if(emailSubjectFile==null || emailSubjectFile.length()<1)
				emailSubjectFile = "Shared File";
		 		 
			boolean success=false;//generateEmail(emailToFile, emailSubjectFile,messagebody,fileName, filePath);
		 
			//handling versions
			CustomTreeNode tempNode = fileExplorerService.getNodeByUUID(selectedNode.getUuid());
			if(tempNode!=null)
				filePath = tempNode.getPath().substring(1);
			tempNode = null;
//			boolean success = false;
			if(emulateEmailSender){
				JespaUtil util = JespaUtil.getInstance();
				String emailFrom = util.getUserEmail(username);
				if(emailFrom!=null && emailFrom.length()>0)
					success=generateEmail(emailFrom, emailToFile, emailSubjectFile,messagebody,fileName, filePath);
				else
					success=generateEmail(null, emailToFile, emailSubjectFile,messagebody,fileName, filePath);
			}else{
				success=generateEmail(null, emailToFile, emailSubjectFile,messagebody,fileName, filePath);
			}
		 
		 if(success){
			 addInfoMessage("Email sent successfully!", "Email sent successfully!");
		 }else{
			 addErrorMessage("Error in sending email!", "Error in sending email!");
		 }
		 
	}
	
    public void linkOptionListner(){
    	System.out.println("Selected email option: "+linkOption);
    	emailUsingDefaultClient();
    }	
	
	private boolean generateEmail(String emailFrom, String emailTo,String subject,String msgbody,String fileName,String filePath){
		 
        String fileExt=fileName.substring(fileName.lastIndexOf("."));
        int fileNumber=VCSUtil.getAbsolute(filePath.hashCode());
        javax.jcr.Binary binaryStream =fileExplorerService.downloadFile(filePath);
        String sessionId=getHttpSession().getId();
        try { 
           if(binaryStream!=null){
              InputStream is=binaryStream.getStream();
              byte[] buffer = new byte[4096];
              int bit = 256;
              
              File outputDir=new File(getContextRealPath()+"/streams/"+sessionId);
              if(!outputDir.exists()){
           	   outputDir.mkdirs();
              }
              File outputFile=new File(getContextRealPath()+"/streams/"+sessionId+"/"+fileNumber+fileExt);
                
           	 if (!outputFile.exists()) {
              	    outputFile.createNewFile();
              	   
	               	FileWriter fw = new FileWriter(outputFile.getAbsoluteFile());
	           	    BufferedOutputStream buffOut = new BufferedOutputStream(new FileOutputStream(outputFile));
	               
	           	    while ((bit = is.read(buffer, 0, 4096)) != -1) {
	               	    buffOut.write(buffer,0,bit);	              
	                }
	                buffOut.flush();
	                buffOut.close();
	                fw.flush();
	                fw.close();
   			 }  
           	 if(emailFrom!=null)
           		return mailService.sendMail(emailFrom, emailTo, subject, msgbody,fileName, outputFile);
           		 
             return mailService.sendMail(emailTo, subject, msgbody,fileName, outputFile);
           }
          } catch (Exception ioe) {
       	      System.out.println("io excpetion 1 " + ioe.getMessage());
          }        
        return false;
         
    }
	
	
	 public void restoreFile(){
		if(selectedNode==null)
			return;
		String filePath=selectedNode.getPath().substring(1);
		String uncPath=getUNCPath(filePath);
		String destPath=uncPath.substring(2).replaceAll("\\\\", "/");
		String fileName=selectedNode.getName();
		destPath=destPath.substring(0,destPath.lastIndexOf("/"));
//		if(testUNC().equals("failure"))
//			return;
		
		//first check if the file already exists
		alreadyExists = fileExplorerService.alreadyExistsForRestore(destPath+"/"+fileName);
//		alreadyExists = true;
		if(alreadyExists){
			return;
		}
		
		String message=null;
		if(!selectedNode.isVersion())
			message = fileExplorerService.restoreFile(filePath, destPath, selectedNode.isVersion());
		else
			message = fileExplorerService.restoreFile(filePath, destPath, selectedNode.getUuid(), selectedNode.isVersion());
		addInfoMessage("Restoring file \""+fileName+"\" at "+uncPath+" as background process", "Restoring file \""+fileName+"\" at "+uncPath+" as background process");

	 }
	 
	 public void restoreFileAE(){
		if(selectedNode==null)
			return;
		String filePath=selectedNode.getPath().substring(1);
		String uncPath=getUNCPath(filePath);
		String destPath=uncPath.substring(2).replaceAll("\\\\", "/");
		String fileName=selectedNode.getName();
		destPath=destPath.substring(0,destPath.lastIndexOf("/"));
		
		String message=null;
		if(!selectedNode.isVersion())
			message = fileExplorerService.restoreFile(filePath, destPath, selectedNode.isVersion());
		else
			message = fileExplorerService.restoreFile(filePath, destPath, selectedNode.getUuid(), selectedNode.isVersion());
		addInfoMessage("Restoring file \""+fileName+"\" at "+uncPath+" as background process", "Restoring file \""+fileName+"\" at "+uncPath+" as background process");

	 }
	 
	 public String testUNC(){
		 ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
		 
		 String remoteUNC = "";//selectedNode.getPath();
		 String[] pathElements = com.virtualcode.util.StringUtils.getPathElements(selectedNode.getPath());
         for (int i = 0; i < pathElements.length; i++) {
             System.out.println("elements: "+pathElements[i]);
             if (i > 2) {
                 remoteUNC += pathElements[i] +"/";
             }
         }
         remoteUNC = remoteUNC.substring(0, remoteUNC.length()-1);
         remoteUNC = remoteUNC.substring(0, remoteUNC.lastIndexOf("/"));
		 
         DriveLetters driveLetter = null;
		 String remoteDomainName = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
		 String remoteUserName = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
		 String remoteUserPassword = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
		 
		 
		 if(remoteDomainName==null || remoteDomainName.isEmpty()){
			 
				List<DriveLetters> driveLetterList=driveLettersService.getAll();

				for(DriveLetters letter: driveLetterList){
					String sharePath = letter.getSharePath().toLowerCase();
					if(sharePath.startsWith("\\\\")){
//						ResourcesUtil ru = ResourcesUtil.getResourcesUtil();				
						sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase() + "/fs/" + sharePath.substring(2).replaceAll("\\\\", "/");
					}
					if(sharePath.endsWith("/"))
						sharePath = sharePath.substring(0, sharePath.lastIndexOf("/"));
					String compareStr = remoteUNC.trim();
					if(!compareStr.contains("/fs/"))
						compareStr = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase()+"/fs/"+remoteUNC.trim().toLowerCase();
					System.out.println(compareStr+", " + sharePath);
					if(remoteUNC.trim()!=null && compareStr.contains(sharePath)){
						driveLetter = letter;
						break;
					}
				}
		 }
		 
		 if(driveLetter!=null){
			 remoteDomainName = driveLetter.getRestoreDomain();
			 remoteUserName = driveLetter.getRestoreUsername();
			 remoteUserPassword = driveLetter.getRestorePassword();
		 }
			 
		 String testUNCResponse = "";
		 System.out.println("---------------------verifying UNC-------------------");
		 boolean error = false;
		 if(remoteUNC.trim().equals("") ||remoteUNC.trim().length()<3){// test as in existing app
			 addErrorMessage("UNC incorrect", "Invalid UNC Path");
//			 addCompErrorMessage("searchForm:hiddenPercent", "UNC incorrect", "Invalid UNC Path");
//			 setTestUNCResponse("wrong");
			 error = true;
		 }else if(remoteUNC.trim().indexOf("\\\\", 2)!=-1){		 
			 addErrorMessage("UNC format incorrect(Only two \\\\ allowed in the start)", "UNC format incorrect(Only two \\\\ allowed in the start)");
//			 addCompErrorMessage("searchForm:hiddenPercent", "UNC format incorrect(Only two \\\\ allowed in the start)", "UNC format incorrect(Only two \\\\ allowed in the start)");
			 error = true;
		 }else if(remoteUserPassword.trim().contains("@")){		 
			 addErrorMessage("Password cannot contain '@'","Password cannot contain '@'");
//			 addCompErrorMessage("searchForm:hiddenPercent", "Password cannot contain '@'","Password cannot contain '@'");
			 error = true;
		 }else{
			 String destPath = "";
			 destPath = VCSUtil.getSambaPath(remoteDomainName, remoteUserName, remoteUserPassword, remoteUNC.trim());
//			 if(remoteDomainName!=null && remoteDomainName.trim().length()<1)				 
//				 destPath = "smb://" + remoteDomainName + ";" + remoteUserName + ":" + remoteUserPassword + "@" + remoteUNC.substring(2,remoteUNC.length()).replaceAll("\\\\","/").trim() + "/";
//			 else
//				 destPath = "smb://"+ remoteUserName + ":" + remoteUserPassword + "@" + remoteUNC.substring(2,remoteUNC.length()).replaceAll("\\\\","/").trim() + "/";
			 
			 System.out.println("destination path: " + destPath);
			 try {
				SmbFile file = new SmbFile(destPath);
				if(file==null || !file.exists()){
					addErrorMessage("UNC path not found","UNC path not found");
//					addCompErrorMessage("searchForm:hiddenPercent", "UNC path not found","UNC path not found");
					error = true;
				}else{
					System.out.println(file.list()); //throws exception access denied if user does not have permission
					//now to check if the user has write permissions on the directory
					SmbFile fileTemp = new SmbFile(destPath+"/sacheck.tmp");
					SmbFileOutputStream sfos = new SmbFileOutputStream(fileTemp);
					sfos.write("Testing write permissions".getBytes());
					sfos.flush();
					sfos.close();
					fileTemp.delete();	//delete after testing
					
//					addCompInfoMessage("searchForm:hiddenPercent", "UNC path verified","UNC path verified");
				}
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				addErrorMessage("Error verifying credentials","Error verifying credentials");
//				addCompErrorMessage("searchForm:hiddenPercent","Error verifying credentials","Error verifying credentials");
				error = true;
			}catch(SmbAuthException ex){
                ex.printStackTrace();
                addErrorMessage("Incorrect domain, username and/or password, or Access is denied","Incorrect domain, username and/or password, or Access is denied");
//                addCompErrorMessage("searchForm:hiddenPercent","Incorrect domain, username and/or password, or Access is denied","Incorrect domain, username and/or password, or Access is denied");
                error = true;
			}catch (SmbException e) {
				// TODO Auto-generated catch block
				addErrorMessage("Server or Sharename not found","Server or Sharename not found");
//				addCompErrorMessage("searchForm:hiddenPercent","Server or Sharename not found","Server or Sharename not found");
				error = true;
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();				
			}			 
			 if(!error){
				 testUNCResponse = "success";
			 }else{
				 testUNCResponse = "failure";
			 }
			 
		 }
		 
		 return testUNCResponse;
	 }	
	 
	 
	 public void restoreFolder(){
			if(selectedNode==null)
				return;			
			System.out.println("recursive: " + restoreRecursive);
			String folderPath=selectedNode.getPath().substring(1);
			String folderName=selectedNode.getName();
			String folderUNC=getUNCPath(folderPath);
		
			String filePath=null,fileUNCPath=null,fileDestPath=null;
			if(restoreRecursive){
				recursiveRestoreFolder(selectedNode.getPath());
			}else{
				System.out.println("restoring " + selectedNode.getPath());
				List<CustomTreeNode> filesList=fileExplorerService.getFirstLevelNodes(selectedNode.getPath());
				CustomTreeNode tempNode;
				for(int i=0;filesList!=null && i<filesList.size();i++){
					tempNode=filesList.get(i);
					filePath=tempNode.getPath().substring(1);
					fileUNCPath=getUNCPath(filePath);
					fileDestPath=fileUNCPath.substring(2).replaceAll("\\\\", "/");				
					fileDestPath=fileDestPath.substring(0,fileDestPath.lastIndexOf("/"));
					System.out.println("restoring " + filePath);
					fileExplorerService.restoreFile(filePath, fileDestPath, false);
				}
			}
			
			addInfoMessage("Restoring folder \""+folderName+"\" at "+folderUNC+" as background process", "Restoring file \""+folderName+"\" at "+folderUNC+" as background process");

		 }
	 
	 public void recursiveRestoreFolder(String folderPath){
//			String folderPath=selectedNode.getPath().substring(1);
//			String folderName=selectedNode.getName();
//			String folderUNC=getUNCPath(folderPath);
//		 	System.out.println("restoring " + folderPath);
			String filePath=null,fileUNCPath=null,fileDestPath=null;
			
			List<CustomTreeNode> filesList=fileExplorerService.getFirstLevelNodes(folderPath);
			CustomTreeNode tempNode;
			for(int i=0;filesList!=null && i<filesList.size();i++){
				tempNode=filesList.get(i);
				filePath=tempNode.getPath().substring(1);
				fileUNCPath=getUNCPath(filePath);
				fileDestPath=fileUNCPath.substring(2).replaceAll("\\\\", "/");				
				fileDestPath=fileDestPath.substring(0,fileDestPath.lastIndexOf("/"));
				System.out.println("restoring " + filePath);
				if(tempNode.getType().equals("nt:folder")){
					recursiveRestoreFolder(filePath);
				}else{
					fileExplorerService.restoreFile(filePath, fileDestPath, false);					
				}
			}
			System.out.println("--------------------");
	 }
	 private String getUserRole(){
		 String loggedInRole=getLoggedInRole();
		 if(loggedInRole.equalsIgnoreCase("ADMINISTRATOR")){
			 return loggedInRole;
		 }else{
			 String authMode=(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE);
			 if(authMode==null || authMode.length()<1)
				 return "";
			  if(authMode.equalsIgnoreCase("ad")){
				  return "LDAP";
			  }else{
				  return "PRIV";
			  }				  
		 }
		
	 }
	 
	 private void activeDeActivateUserControls(){
		    RoleFeatureUtil rfUtil=RoleFeatureUtil.getRoleFeatureUtil();   
		    RoleFeature roleFeature;
	    	String userRole=getUserRole();
	    	if(userRole.equalsIgnoreCase("ADMINISTRATOR")){
	    		 
	    		 roleFeature=rfUtil.getFeature("email_link");
	    		 this.linksShareEnabled=roleFeature!=null ? roleFeature.getAdminAllowed().booleanValue():false;
	    		 
	    		 roleFeature=rfUtil.getFeature("email_doc");
	    		 this.filesShareEnabled=roleFeature!=null ?roleFeature.getAdminAllowed():false;
	    		 
	    		 roleFeature=rfUtil.getFeature("download");
	    		 this.downloadEnabled=roleFeature!=null ?roleFeature.getAdminAllowed():false;
	    		 
	    		 roleFeature=rfUtil.getFeature("restore_file");
	    		 this.fileRestoreEanbled=roleFeature!=null ?roleFeature.getAdminAllowed():false;
	    		 
	    		 roleFeature=rfUtil.getFeature("restore_folder");
	    		 this.folderRestoreEanbled=roleFeature!=null ?roleFeature.getAdminAllowed():false;
	    		 
	    		 roleFeature=rfUtil.getFeature("media_streaming");
	    		 this.mediaStreamingEnabled=roleFeature!=null ?roleFeature.getAdminAllowed():false;
	    		 
	    		 roleFeature=rfUtil.getFeature("document_tagging");
	    		 this.enableDocumentTagging=roleFeature!=null ?roleFeature.getAdminAllowed():false;
	    		 
	    		 roleFeature=rfUtil.getFeature("document_viewing");
	    		 this.enablePdfView=roleFeature!=null ?roleFeature.getAdminAllowed():false;
	    		 
	    		 roleFeature=rfUtil.getFeature("show_document_url");
	    		 this.showDocumentUrl=roleFeature!=null ?roleFeature.getAdminAllowed():false;
	    		 
	    	}else if(userRole.equalsIgnoreCase("PRIV")){
	    		 
	    		 roleFeature=rfUtil.getFeature("email_link");
		   		 this.linksShareEnabled=roleFeature!=null ?roleFeature.getPrivAllowed():false;
		   		 
		   		 roleFeature=rfUtil.getFeature("email_doc");
		   		 this.filesShareEnabled=roleFeature!=null ?roleFeature.getPrivAllowed():false;
		   		 
		   		 roleFeature=rfUtil.getFeature("download");
		   		 this.downloadEnabled=roleFeature!=null ?roleFeature.getPrivAllowed():false;
		   		 
		   		 roleFeature=rfUtil.getFeature("restore_file");
	    		 this.fileRestoreEanbled=roleFeature!=null ?roleFeature.getPrivAllowed():false;
	    		 
	    		 roleFeature=rfUtil.getFeature("restore_folder");
	    		 this.folderRestoreEanbled=roleFeature!=null ?roleFeature.getPrivAllowed():false;
		   		 
		   		 roleFeature=rfUtil.getFeature("media_streaming");
		   		 this.mediaStreamingEnabled=roleFeature!=null ?roleFeature.getPrivAllowed():false;
		   		 
		   		 roleFeature=rfUtil.getFeature("document_tagging");
	    		 this.enableDocumentTagging=roleFeature!=null ?roleFeature.getPrivAllowed():false;
	    		 
	    		 roleFeature=rfUtil.getFeature("document_viewing");
	    		 this.enablePdfView=roleFeature!=null ?roleFeature.getPrivAllowed():false;	    
	    		 
	    		 roleFeature=rfUtil.getFeature("show_document_url");
	    		 this.showDocumentUrl=roleFeature!=null ?roleFeature.getPrivAllowed():false;
		   		 
	    	}else if(userRole.equalsIgnoreCase("LDAP")){
	    		
	    		 roleFeature=rfUtil.getFeature("email_link");
		   		 this.linksShareEnabled=roleFeature!=null ?roleFeature.getLdapAllowed():false;
		   		 
		   		 roleFeature=rfUtil.getFeature("email_doc");
		   		 this.filesShareEnabled=roleFeature!=null ?roleFeature.getLdapAllowed():false;
		   		 
		   		 roleFeature=rfUtil.getFeature("download");
		   		 this.downloadEnabled=roleFeature!=null ?roleFeature.getLdapAllowed():false;
		   		 
		   		 roleFeature=rfUtil.getFeature("restore_file");
	    		 this.fileRestoreEanbled=roleFeature!=null ?roleFeature.getLdapAllowed():false;
	    		 
	    		 roleFeature=rfUtil.getFeature("restore_folder");
	    		 this.folderRestoreEanbled=roleFeature!=null ?roleFeature.getLdapAllowed():false;
		   		 
		   		 roleFeature=rfUtil.getFeature("media_streaming");
		   		 this.mediaStreamingEnabled=roleFeature!=null ?roleFeature.getLdapAllowed():false;
		   		 
		   		 roleFeature=rfUtil.getFeature("document_tagging");
	    		 this.enableDocumentTagging=roleFeature!=null ?roleFeature.getLdapAllowed():false;
	    		 
	    		 roleFeature=rfUtil.getFeature("document_viewing");
	    		 this.enablePdfView=roleFeature!=null ?roleFeature.getLdapAllowed():false;
	    		 
	    		 roleFeature=rfUtil.getFeature("show_document_url");
	    		 this.showDocumentUrl=roleFeature!=null ?roleFeature.getLdapAllowed():false;
		   		 
	    	}else{
	    		 this.linksShareEnabled=false;
	    		 this.filesShareEnabled=false;
	    		 this.downloadEnabled=false;
	    		 this.fileRestoreEanbled=false;
	    		 this.folderRestoreEanbled=false;
	    		 this.mediaStreamingEnabled=false;
	    		 this.enableDocumentTagging=false;
	    		 this.enablePdfView = false;
	    	}

	    	ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
	    	if(ru.getCodeValue("enable_default_email_client", SystemCodeType.GENERAL)!=null)
	    		emailUsingDefaultClient = ru.getCodeValue("enable_default_email_client", SystemCodeType.GENERAL).equals("yes")?true:false;
	    	if(ru.getCodeValue("enable_cifs_file_sharing", SystemCodeType.GENERAL)!=null)
	    		enableCIFSFileSharing = ru.getCodeValue("enable_cifs_file_sharing", SystemCodeType.GENERAL).equals("yes")?true:false;
	    	if(ru.getCodeValue("emulate_email_sender", SystemCodeType.GENERAL)!=null)
	    		emulateEmailSender = ru.getCodeValue("emulate_email_sender", SystemCodeType.GENERAL).equals("yes")?true:false;
	    	
	    	com.virtualcode.pdfviewer.flexpaper.Config config = new com.virtualcode.pdfviewer.flexpaper.Config();
	    	if(!"true".equals(config.getConfig("splitmode")))
	    			pdfViewerUrl = "common/simple_document.jsp?doc";
	    	else
	    			pdfViewerUrl = "common/split_document.jsp?doc";
	    	
	    	String authType = (String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE);
	    	if(enableCIFSFileSharing && authType.equals("ad")){
	    		if(ru.getCodeValue("restrict_cifs_files_sharing_ad", SystemCodeType.GENERAL).equals("yes")){
	    			
	    		
			    	JespaUtil util = JespaUtil.getInstance();
			    	boolean exists = false;
			    	try{
			    		List<String> userGroupList = util.getGroupsForUser((String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME));
						System.out.println(util.getGroupsForUser((String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME)));
	//					GroupGuardian groupGuardian = null;
						for(String group:userGroupList){
							System.out.println("Group Name" + group);
							List<LdapGroups> ldapGroupList = ldapGroupsService.getLdapGroupByGroupName(group);
//							List<GroupGuardian> groupGuardians =  groupGuardianService.getGroupGuardianByGroupName(group);
							if(ldapGroupList!=null && ldapGroupList.size()>0){
//								groupGuardian = ldapGroupList.get(0);
								exists = true;
								break;
							}
						}
						
						if(!exists){
//							if(groupGuardian!=null && groupGuardian.getRestrictGroup()==0){
//								enableCIFSFileSharing = true;
//								linksShareEnabled = true;
//							}else{
								enableCIFSFileSharing = false;
								if(ru.getCodeValue("restrict_archive_files_sharing_ad", SystemCodeType.GENERAL).equals("yes"))
									linksShareEnabled = false;
//							}
						}
						
		//	    		System.out.println(util.getGroupsForUser("bilal"));
					} catch (SecurityProviderException | IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	    		}
	    	}
	 }
	 
    
	 public void groupsUsersSelectionChange(){
		 System.out.println("Selection changed to: " + permissionUsersOrGroups);		 
	 }
  
	public void setRootNodes(List<CustomTreeNode> list) {
		this.rootNodes = list;
	}

	public CustomTreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(CustomTreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public List<CustomTreeNode> getFirstLevelFiles() {
		return firstLevelFiles;
	}

	public void setFirstLevelFiles(List<CustomTreeNode> firstLevelFiles) {
		this.firstLevelFiles = firstLevelFiles;
	}
				
	public double getFolderSizeMb() {
		return folderSizeMb;
	}


	public Collection<Object> getSelection() {
		return selection;
	}

	public void setSelection(Collection<Object> selection) {
		this.selection = selection;
	}

	public CustomTreeNode getSelectionItem() {
	        if (selectionItems == null || selectionItems.isEmpty()) {
	            return null;
	        }
	        return selectionItems.get(0);
	}

	public List<CustomTreeNode> getSelectionItems() {
		return selectionItems;
	}

	public void setSelectionItems(List<CustomTreeNode> selectionItems) {
		this.selectionItems = selectionItems;
	}

	public List<SID> getSecAllGroups() {
		return secAllGroups;
	}

	public ArrayList<SID> getSecurityAllowGroups() {
		return securityAllowGroups;
	}

	public void setSecurityAllowGroups(ArrayList<SID> securityAllowGroups) {
		this.securityAllowGroups = securityAllowGroups;
	}

	public ArrayList<SID> getSecurityDenyGroups() {
		return securityDenyGroups;
	}

	public void setSecurityDenyGroups(ArrayList<SID> securityDenyGroups) {
		this.securityDenyGroups = securityDenyGroups;
	}

	public ArrayList<SID> getShareAllGroups() {
		return shareAllGroups;
	}

	public void setShareAllGroups(ArrayList<SID> shareAllGroups) {
		this.shareAllGroups = shareAllGroups;
	}

	public ArrayList<SID> getShareAllowGroups() {
		return shareAllowGroups;
	}

	public void setShareAllowGroups(ArrayList<SID> shareAllowGroups) {
		this.shareAllowGroups = shareAllowGroups;
	}

	public ArrayList<SID> getShareDenyGroups() {
		return shareDenyGroups;
	}

	public void setShareDenyGroups(ArrayList<SID> shareDenyGroups) {
		this.shareDenyGroups = shareDenyGroups;
	}

	public void setSecAllGroups(ArrayList<SID> secAllGroups) {
		this.secAllGroups = secAllGroups;
	}
	
	private boolean[] recursiveSet;

	public boolean[] getRecursiveSet() {
		return recursiveSet;
	}

	public String getTempRecursive() {
		return tempRecursive;
	}

	public void setTempRecursive(String tempRecursive) {
		this.tempRecursive = tempRecursive;
	}

	public void setRecursiveSet(boolean[] recursiveSet) {
		this.recursiveSet = recursiveSet;
	}

	public String getTempBatchUpdate() {
		return tempBatchUpdate;
	}

	public void setTempBatchUpdate(String tempBatchUpdate) {
		this.tempBatchUpdate = tempBatchUpdate;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public List<BreadCrumb> getBreadCrumbsList() {
		return breadCrumbsList;
	}

	public void setBreadCrumbsList(List<BreadCrumb> breadCrumbsList) {
		this.breadCrumbsList = breadCrumbsList;
	}

	public boolean isSearchPathEnabled() {
		return searchPathEnabled;
	}

	public void setSearchPathEnabled(boolean searchPathEnabled) {
		this.searchPathEnabled = searchPathEnabled;
	}

	public String getSearchPath() {
		return searchPath;
	}

	public void setSearchPath(String searchPath) {
		this.searchPath = searchPath;
	}

	public boolean isEnableDocumentTagging() {
		return enableDocumentTagging;
	}

	public void setEnableDocumentTagging(boolean enableDocumentTagging) {
		this.enableDocumentTagging = enableDocumentTagging;
	}
	
	public boolean isMediaStreamingEnabled() {
		return mediaStreamingEnabled;
	}

	public void setMediaStreamingEnabled(boolean mediaStreamingEnabled) {
		this.mediaStreamingEnabled = mediaStreamingEnabled;
	}

	public String getSortMode() {
		return sortMode;
	}

	public void setSortMode(String sortMode) {
		this.sortMode = sortMode;
	}

	public String getTempMerge() {
		return tempMerge;
	}

	public void setTempMerge(String tempMerge) {
		this.tempMerge = tempMerge;
	}

	public String getSaveOrSyncPermissions() {
		return saveOrSyncPermissions;
	}

	public void setSaveOrSyncPermissions(String saveOrSyncPermissions) {
		this.saveOrSyncPermissions = saveOrSyncPermissions;
	}

	public String getRemoteDomainName() {
		return remoteDomainName;
	}

	public void setRemoteDomainName(String remoteDomainName) {
		this.remoteDomainName = remoteDomainName;
	}

	public String getRemoteUserName() {
		return remoteUserName;
	}

	public void setRemoteUserName(String remoteUserName) {
		this.remoteUserName = remoteUserName;
	}

	public String getRemoteUserPassword() {
		return remoteUserPassword;
	}

	public void setRemoteUserPassword(String remoteUserPassword) {
		this.remoteUserPassword = remoteUserPassword;
	}

	public String getEmailToLink() {
		return emailToLink;
	}

	public void setEmailToLink(String emailToLink) {
		this.emailToLink = emailToLink;
	}

	public String getEmailNameLink() {
		return emailNameLink;
	}

	public void setEmailNameLink(String emailNameLink) {
		this.emailNameLink = emailNameLink;
	}

	public String getEmailBodyLink() {
		return emailBodyLink;
	}

	public void setEmailBodyLink(String emailBodyLink) {
		this.emailBodyLink = emailBodyLink;
	}

	public String getEmailToFile() {
		return emailToFile;
	}

	public void setEmailToFile(String emailToFile) {
		this.emailToFile = emailToFile;
	}

	public String getEmailNameFile() {
		return emailNameFile;
	}

	public void setEmailNameFile(String emailNameFile) {
		this.emailNameFile = emailNameFile;
	}

	public String getEmailBodyFile() {
		return emailBodyFile;
	}

	public void setEmailBodyFile(String emailBodyFile) {
		this.emailBodyFile = emailBodyFile;
	}

	public List<CustomTreeNode> getNextVersionsList() {
		return nextVersionsList;
	}

	public void setNextVersionsList(List<CustomTreeNode> nextVersionsList) {
		this.nextVersionsList = nextVersionsList;
	}
	
	public int getNextVersionsSize() {
		return (this.nextVersionsList!=null)?this.nextVersionsList.size():0;
	}

	public boolean isFilesShareEnabled() {
		return filesShareEnabled;
	}

	public void setFilesShareEnabled(boolean filesShareEnabled) {
		this.filesShareEnabled = filesShareEnabled;
	}

	public boolean isLinksShareEnabled() {
		return linksShareEnabled;
	}

	public void setLinksShareEnabled(boolean linksShareEnabled) {
		this.linksShareEnabled = linksShareEnabled;
	}

	public boolean isShowThumbnail() {
		return showThumbnail;
	}

	public void setShowThumbnail(boolean showThumbnail) {
		this.showThumbnail = showThumbnail;
	}

	public String getCurrentThumbnailPath() {
		return currentThumbnailPath;
	}

	public void setCurrentThumbnailPath(String currentThumbnailPath) {
		this.currentThumbnailPath = currentThumbnailPath;
	}
	public boolean isDownloadEnabled() {
		return downloadEnabled;
	}

	public boolean isFileRestoreEanbled() {
		return fileRestoreEanbled;
	}

	public boolean isFolderRestoreEanbled() {
		return folderRestoreEanbled;
	}

	public boolean isEnablePdfView() {
		return enablePdfView;
	}

	public void setEnablePdfView(boolean enablePdfView) {
		this.enablePdfView = enablePdfView;
	}

	public boolean isShowPdfIcon() {
		return showPdfIcon;
	}

	public void setShowPdfIcon(boolean showPdfIcon) {
		this.showPdfIcon = showPdfIcon;
	}

	public String getUrlForPdf() {
		return urlForPdf;
	}

	public void setUrlForPdf(String urlForPdf) {
		this.urlForPdf = urlForPdf;
	}

	public boolean isShowFrame() {
		return showFrame;
	}

	public void setShowFrame(boolean showFrame) {
		this.showFrame = showFrame;
	}

	public DocumentManager getDocumentManager() {
		return documentManager;
	}

	public void setDocumentManager(DocumentManager documentManager) {
		this.documentManager = documentManager;
	}

	public String getEmailString() {
		return emailString;
	}

	public void setEmailString(String emailString) {
		this.emailString = emailString;
	}

	public boolean isEmailUsingDefaultClient() {
		return emailUsingDefaultClient;
	}

	public void setEmailUsingDefaultClient(boolean emailUsingDefaultClient) {
		this.emailUsingDefaultClient = emailUsingDefaultClient;
	}

	public String getLinkOption() {
		return linkOption;
	}

	public void setLinkOption(String linkOption) {
		this.linkOption = linkOption;
	}

	public List<CustomTreeNode> getOldFirstLevelFiles() {
		return oldFirstLevelFiles;
	}

	public void setOldFirstLevelFiles(List<CustomTreeNode> oldFirstLevelFiles) {
		this.oldFirstLevelFiles = oldFirstLevelFiles;
	}

	public boolean isEnableCIFSFileSharing() {
		return enableCIFSFileSharing;
	}

	public void setEnableCIFSFileSharing(boolean enableCIFSFileSharing) {
		this.enableCIFSFileSharing = enableCIFSFileSharing;
	}

	public String getEmailSmbToLink() {
		return emailSmbToLink;
	}

	public void setEmailSmbToLink(String emailSmbToLink) {
		this.emailSmbToLink = emailSmbToLink;
	}

	public String getEmailSmbNameLink() {
		return emailSmbNameLink;
	}

	public void setEmailSmbNameLink(String emailSmbNameLink) {
		this.emailSmbNameLink = emailSmbNameLink;
	}

	public String getEmailSmbBodyLink() {
		return emailSmbBodyLink;
	}

	public void setEmailSmbBodyLink(String emailSmbBodyLink) {
		this.emailSmbBodyLink = emailSmbBodyLink;
	}

	public boolean isFolderArchived() {
		return folderArchived;
	}

	public void setFolderArchived(boolean folderArchived) {
		this.folderArchived = folderArchived;
	}

	public String getEmailSubjectLink() {
		return emailSubjectLink;
	}

	public void setEmailSubjectLink(String emailSubjectLink) {
		this.emailSubjectLink = emailSubjectLink;
	}

	public String getEmailSubjectFile() {
		return emailSubjectFile;
	}

	public void setEmailSubjectFile(String emailSubjectFile) {
		this.emailSubjectFile = emailSubjectFile;
	}

	public String getEmailSmbSubjectLink() {
		return emailSmbSubjectLink;
	}

	public void setEmailSmbSubjectLink(String emailSmbSubjectLink) {
		this.emailSmbSubjectLink = emailSmbSubjectLink;
	}

	public boolean isEmailFileSizeExceeded() {
		return emailFileSizeExceeded;
	}

	public void setEmailFileSizeExceeded(boolean emailFileSizeExceeded) {
		this.emailFileSizeExceeded = emailFileSizeExceeded;
	}

	public boolean isRemoteFilesLoaded() {
		return remoteFilesLoaded;
	}

	public void setRemoteFilesLoaded(boolean remoteFilesLoaded) {
		this.remoteFilesLoaded = remoteFilesLoaded;
	}

	public boolean isEmulateEmailSender() {
		return emulateEmailSender;
	}

	public void setEmulateEmailSender(boolean emulateEmailSender) {
		this.emulateEmailSender = emulateEmailSender;
	}

	public boolean isShowDocumentUrl() {
		return showDocumentUrl;
	}

	public void setShowDocumentUrl(boolean showDocumentUrl) {
		this.showDocumentUrl = showDocumentUrl;
	}

	public String getDnsServer() {
		return dnsServer;
	}

	public void setDnsServer(String dnsServer) {
		this.dnsServer = dnsServer;
	}

	public String getPdfViewerUrl() {
		return pdfViewerUrl;
	}

	public void setPdfViewerUrl(String pdfViewerUrl) {
		this.pdfViewerUrl = pdfViewerUrl;
	}

	public boolean isAlreadyExists() {
		return alreadyExists;
	}

	public void setAlreadyExists(boolean alreadyExists) {
		this.alreadyExists = alreadyExists;
	}

//	public GroupGuardian getGroupGuardian() {
//		return groupGuardian;
//	}
//
//	public void setGroupGuardian(GroupGuardian groupGuardian) {
//		this.groupGuardian = groupGuardian;
//	}

}
