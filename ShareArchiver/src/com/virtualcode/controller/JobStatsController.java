package com.virtualcode.controller;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UISelectItem;

import com.virtualcode.common.VCSConstants;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.ErrorCode;
import com.virtualcode.vo.ExportJobPath;
import com.virtualcode.vo.HotStatistics;
import com.virtualcode.vo.Job;
import com.virtualcode.vo.JobStatistics;
import com.virtualcode.vo.JobStatus;
import com.virtualcode.vo.SystemCodeType;

@ManagedBean(name="jobStatsController")
@ViewScoped
public class JobStatsController extends AbstractController {

	public JobStatsController(){
		init();
		
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
    	String enableSHP = ru.getCodeValue("enable_sharepoint", SystemCodeType.GENERAL);
    	String enablePrintA = ru.getCodeValue("enable_print_archiving", SystemCodeType.GENERAL);
    	enableSP=((enableSHP!=null && enableSHP.equals("yes"))?true:false);
    	enablePrintArch=((enablePrintA!=null && enablePrintA.equals("yes"))?true:false);
    	
    	populateJobSources(enableSP,enablePrintArch);
    	
    	enableDisableTabs(ru);   	
    }
	
	private Job job;
	
	private String jobSource=null;//by default select all jobs
	
	private List<UISelectItem> jobSourceList=new ArrayList<UISelectItem>();
	
	private boolean showStatusGrid=false;
	
	private List<Job> allJobsList=new ArrayList<Job>();
	
	private List<Job> archivingJobsList=new ArrayList<Job>();
	
	private List<Job> stubbingJobsList=new ArrayList<Job>();
	
	private List<Job> evaluationJobsList=new ArrayList<Job>();
	
	private List<Job> indexingJobsList=new ArrayList<Job>();
	
	private List<Job> exportJobsList=new ArrayList<Job>();
	
	private List<Job> retentionJobsList=new ArrayList<Job>();
	
	private List<Job> activeArchivingJobsList=new ArrayList<Job>();
	
	private List<Job> dynaArchivingJobsList=new ArrayList<Job>();
	
	private List<Job> scheduledJobsList=new ArrayList<Job>();
	
	private List<Job> withoutStubJobsList =  new ArrayList<Job>();
		
	private List<JobStatistics> jobStatisticsList=new ArrayList<JobStatistics>();
	
	private List<JobStatus> jobStatusList=new ArrayList<JobStatus>();
	
	@ManagedProperty(value="#{statController}")
	private StatisticsController statisticsController;
	
	private boolean exportJob = false;
	
    private String jobFlag = "OTHERS";
	 
	private boolean hotStatistics	=	false;
	
	private List<ExportJobPath> exportPathList;
	
	private boolean enableSP=false;
	
	private boolean enablePrintArch=false;
	
	private Date execTimeStartDate;
	
	private boolean reRunEnabled=false;
	
	
	private boolean enableArchiveTab=false;
	private boolean enableStubTab=false;
	private boolean enableEvalTab=false;
	private boolean enableAnalysisTab=false;
	
	private boolean enableExportTab=false;
	private boolean enableActiveArchiveTab=false;
	private boolean enableDynaArchiveTab=false;
	private boolean enableScheduledTab=false;
	private boolean enableWithoutStubTab = false;
	
	private void populateJobSources(boolean enableSP,boolean enablePrintArch){
		
		jobSourceList.clear();
		if(enableSP || enablePrintArch){
			UISelectItem fsItem=new UISelectItem();
			fsItem.setItemLabel("File");
			fsItem.setValue("FS");
			jobSourceList.add(fsItem);
		}
			
		if(enableSP){
			UISelectItem spItem=new UISelectItem();
			spItem.setItemLabel("SharePoint");
			spItem.setValue("SP");
			jobSourceList.add(spItem);
		}
		
		if(enablePrintArch){
//			UISelectItem printAItem=new UISelectItem();
//			printAItem.setItemLabel("Printer");
//			printAItem.setValue("PS");
//			jobSourceList.add(printAItem);
		}
	}
	
	private void enableDisableTabs(ResourcesUtil ru){
		
		String archTab=	ru.getCodeValue("enable_archiving_tab", SystemCodeType.GENERAL);
		String stubTab= ru.getCodeValue("enable_stubbing_tab", SystemCodeType.GENERAL);
		String evalTab= ru.getCodeValue("enable_evaluation_tab", SystemCodeType.GENERAL);
		String analysisTab= ru.getCodeValue("enable_analysis_tab", SystemCodeType.GENERAL);
		String exportTab=ru.getCodeValue("enable_export_tab", SystemCodeType.GENERAL);
	   	 
		String activeArchTab= ru.getCodeValue("enable_active_archiving_tab", SystemCodeType.GENERAL);
		String dynaArchTab= ru.getCodeValue("enable_dynamic_archiving_tab", SystemCodeType.GENERAL);
		String scheduledTab= ru.getCodeValue("enable_scheduling_tab", SystemCodeType.GENERAL);
		String withoutStubTab= ru.getCodeValue("enable_withoutStub_tab", SystemCodeType.GENERAL);
		enableArchiveTab=((archTab!=null && archTab.equals("yes"))?true:false);
		enableStubTab=((stubTab!=null && stubTab.equals("yes"))?true:false);
		
		enableEvalTab=((evalTab!=null && evalTab.equals("yes"))?true:false);
		enableAnalysisTab=((analysisTab!=null && analysisTab.equals("yes"))?true:false);
		enableExportTab=((exportTab!=null && exportTab.equals("yes"))?true:false);
		enableActiveArchiveTab=((activeArchTab!=null && activeArchTab.equals("yes"))?true:false);
		enableDynaArchiveTab=((dynaArchTab!=null && dynaArchTab.equals("yes"))?true:false);
		enableScheduledTab=((scheduledTab!=null && scheduledTab.equals("yes"))?true:false);
		enableWithoutStubTab=((withoutStubTab!=null && withoutStubTab.equals("yes"))?true:false);
	}
	
	
	
	public void init(){
		
		List<JobStatus> statusList=null;
		clearJobsList();
		
		Job tempJob=null;
		List<Job> tempList=jobService.getAllForStatus(jobSource);
		
		 for(int i=0;tempList!=null && i<tempList.size();i++){
			 Object obj=tempList.get(i);
			 Object[] objArray=(Object[])obj;
			 tempJob=(Job)objArray[1];
			 statusList = jobStatusService.getJobStatusByJob(tempJob.getId());
			 
			 if(statusList==null||statusList.size()<1){
				 tempJob.setJobStatus(VCSConstants.JOB_STATUS_PENDING);
			 }else{
				 JobStatus lastJobInstance = statusList.get(0);
				 if(lastJobInstance.getErrorCode().getId()==1){
					 tempJob.setJobStatus(VCSConstants.JOB_STATUS_FAILED);
				 }else if(lastJobInstance.getErrorCode().getId()==0){
					 if(lastJobInstance.getStatus().toString().equals("COMPLETED")){
						 tempJob.setJobStatus(VCSConstants.JOB_STATUS_COMPLETED);
					 }else if(lastJobInstance.getStatus().toString().equals("RUNNING")){
						 tempJob.setJobStatus(VCSConstants.JOB_STATUS_PROCESSING);
					 }else if(lastJobInstance.getStatus().toString().equals("EVALUATING")){
						 tempJob.setJobStatus(VCSConstants.JOB_STATUS_PROCESSING);
					 }else if(lastJobInstance.getStatus().equals("CANCEL")){
						 tempJob.setJobStatus(VCSConstants.JOB_STATUS_CANCEL);
					 } else if(lastJobInstance.getStatus().equals("CANCELED")){
						 tempJob.setJobStatus(VCSConstants.JOB_STATUS_CANCELED);
					 }
				 }
			 }				 
			 addJobToList(tempJob);
		 }
		
	}	
	
	private void addJobToList(Job tempJob){
		
		allJobsList.add(tempJob);
		
		if(tempJob.getIsScheduled().booleanValue()){
			scheduledJobsList.add(tempJob);
		}
		else if(tempJob.getActionType().equals("ARCHIVE")){
			archivingJobsList.add(tempJob);
		}else if(tempJob.getActionType().equals("STUB")){
			stubbingJobsList.add(tempJob);
		}else if(tempJob.getActionType().equals("EVALUATE")){
			evaluationJobsList.add(tempJob);
		}else if(tempJob.getActionType().equals("INDEXING")){
			indexingJobsList.add(tempJob);
		}else if(tempJob.getActionType().equals("EXPORT") || tempJob.getActionType().equals("EXPORTSEARCHED")|| tempJob.getActionType().equals("RESTORE")){
			exportJobsList.add(tempJob);
		}else if(tempJob.getActionType().equals("RETENTION")){
			retentionJobsList.add(tempJob);
		}else if(tempJob.getActionType().equals("ACTIVEARCH")){
			activeArchivingJobsList.add(tempJob);
		}else if(tempJob.getActionType().equals("DYNARCH")){
			dynaArchivingJobsList.add(tempJob);
		}else if(tempJob.getActionType().equals("WITHOUTSTUB")){
			withoutStubJobsList.add(tempJob);
		}else{
			System.err.println("No matching List for job type "+tempJob.getActionType());
		}		
		
	}
	
	
	public void getJobStatus(){
		 String currJobID=getRequestParameter("id");
		 
		 if( jobStatusList!=null){
			 jobStatusList.clear();
		 }
		 if(jobStatisticsList!=null){
			 jobStatisticsList.clear();
		 }	
		 
		 List<JobStatus> statusList = jobStatusService.getJobStatusByJob(Integer.parseInt(currJobID));
		
		 if(statusList==null||statusList.size()<1){
			 showStatusGrid=false;
			return;
		 }else{
			 //setShowStatusGrid(true);
			
			 System.out.println("Found status size: " + statusList.size());
			 List<JobStatistics> statisticsList = jobStatisticsService.getJobStatisticsByJob(Integer.parseInt(currJobID));
			 			
			 System.out.println("Found statistics size: " + statisticsList.size()+"-");
			 if(statisticsList.size()>0){
				 setShowStatusGrid(true);
			 }else{
				 setShowStatusGrid(false);
				 System.out.println("----job statistics not found-------");
			 }
			 
			 int sr = statisticsList.size();			 
			 String serial = "";
			 sr = sr+1;
			 if(sr==1)serial=sr+"st";//jobStatistics.setSerialNo((sr)+"th (latest)");
			 else if(sr==2)serial=sr+"nd";//  jobStatistics.setSerialNo((sr)+"th");
			 else if(sr==3)serial=sr+"rd";
			 else serial=sr+"th";
			 
			 JobStatus status = statusList.get(0);
			 if(status.getStatus().trim().contains("RUNNING")){
				 setShowStatusGrid(true);
				 setHotStatistics(true);
				 System.out.println("----going for hot statistics-------");
				 JobStatus jobStatus =  new JobStatus();
				
				 jobStatus.setExecutionId(status.getExecutionId());
				 jobStatus.setId(-1);
				 jobStatus.setJob(status.getJob());
					 
				 jobStatus.setExecStartTime(status.getExecStartTime());
				
				 jobStatus.setSerialNumber(serial);
				 jobStatus.setStatus(VCSConstants.JOB_STATUS_PROCESSING);
				 jobStatusList.add(jobStatus);				 
				 populateHotStatistics(VCSUtil.getIntValue(currJobID), status.getExecutionId(), serial);
				
			 }
			 sr = statisticsList.size();	//set to max size
			 JobStatus jobStatus=null;
			 for(JobStatistics jobStatistics:statisticsList){
								 
				 if(sr==1)serial=sr+"st";//jobStatistics.setSerialNo((sr)+"th (latest)");
				 else if(sr==2)serial=sr+"nd";//  jobStatistics.setSerialNo((sr)+"th");
				 else if(sr==3)serial=sr+"rd";
				 else serial=sr+"th";
				 
				 if(sr==statisticsList.size())serial=serial+"(latest)";
				 				 
				 populateNormalStatistics(jobStatistics.getId(), serial);
				 
				 jobStatus=new JobStatus();
				 List<JobStatus> tempStatusList = jobStatusService.getJobStatusByProperty("executionId", jobStatistics.getExecutionId());
				 if(tempStatusList!=null && tempStatusList.size()>0){
					 JobStatus tempJobStatus = tempStatusList.get(0);
					 if(tempJobStatus.getErrorCode().getId()==1){
						 jobStatus.setStatus(VCSConstants.JOB_STATUS_FAILED);					 
					 }else if(tempJobStatus.getStatus().equals("COMPLETED")){
						 jobStatus.setStatus(VCSConstants.JOB_STATUS_COMPLETED);
					 }else if(tempJobStatus.getStatus().equals("RUNNING")){
						 jobStatus.setStatus(VCSConstants.JOB_STATUS_PROCESSING);
					 }else if(tempJobStatus.getStatus().equals("CANCELED")){
						 jobStatus.setStatus(VCSConstants.JOB_STATUS_CANCELED);
					 }else{
						 jobStatus.setStatus(VCSConstants.JOB_STATUS_FAILED);
					 }
				 }
				
				 jobStatus.setSerialNumber(serial);
				
				 jobStatus.setId(jobStatistics.getId());
				 jobStatus.setExecStartTime(jobStatistics.getJobStartTime());
				 jobStatus.setExecEndTime(jobStatistics.getJobEndTime());
				 jobStatus.setExecutionId(jobStatistics.getExecutionId());
				 jobStatus.setJob(jobStatistics.getJob());
				 jobStatusList.add(jobStatus);				 
				
				 sr = sr - 1;
			 }
			 
		 }
		
		 this.job = jobService.getJobById(Integer.parseInt(currJobID));
		 if(job!=null){
			 if(job.getIsScheduled().booleanValue()){
				 job.setScheduled("yes");
			 }else{
				 job.setScheduled("no");
			 }
			 
			 if("RETENSION".equals(job.getActionType())) {//An option for Retention job
				 job.setNextAction(job.getActiveActionType());
			 }
		 }
		 //set the date value of DB into temp Date param to show on interface
		 Date dateObj	=	((job.getExecTimeStart()!=null)? (Date)job.getExecTimeStart() : VCSUtil.getCurrentDateTime());
		 this.job.setExecTimeStartDate(dateObj);	
	 } 
	
	
	 
	 public void getCurrentExecutionDetails(){
		 String id=getRequestParameter("id");
		 String serialNo=getRequestParameter("serial");
		 
		 int jobId = VCSUtil.getIntValue(getRequestParameter("jobId"));
		 String executionId = getRequestParameter("executionId");		
		 System.out.println("---------execution details--------: statId: "+id+"  jobId: "+jobId+"  serialNumber: "+serialNo);
		
		 if(jobStatisticsList!=null){
			 jobStatisticsList.clear();
		 }		 
		 
		 //if id is -1, this means jobs is processing
		 if(id.equals("-1")){
			 setHotStatistics(true);
			 populateHotStatistics(jobId, executionId, serialNo);
					
		 }else{		
			 setHotStatistics(false);
			 populateNormalStatistics(Integer.parseInt(id), serialNo);			 
		 }
	 }
	 
	 
	 private void populateHotStatistics(int jobId,String executionId,String serialNumber){
		 
		
		 JobStatistics jobStatistics =  new JobStatistics();
		 jobStatistics.setExecutionId(executionId);
		 jobStatistics.setId(-1);
		 jobStatistics.setJob(jobService.getJobById(jobId));
		 //jobStatistics.setJobStartTime(status.getExecStartTime());
		 jobStatistics.setSerialNo(serialNumber);
		 jobStatistics.setStatus(VCSConstants.JOB_STATUS_PROCESSING);
		 
		 if(jobStatistics.getJob().getActionType().contains("EXPORT")){
			 
			 Long totalFailedCount = (statisticsController.getExportFailedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getExportFailedCount(executionId));
			 jobStatistics.setTotalFailed(totalFailedCount);
			 Long totalEvaluatedCount = (statisticsController.getExportEvaluatedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getExportEvaluatedCount(executionId));
			 jobStatistics.setTotalEvaluated(totalEvaluatedCount);
			 
			 Long totalExportedCount = (statisticsController.getExportedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getExportedCount(executionId));
			 jobStatistics.setTotalFreshArchived(totalExportedCount);
			 //jobStatistics.setAlreadyArchived(0l);
			 setJobFlag("EXPORT");
			 
			 Long totalFilesSize = (statisticsController.getTotalExportedFilesSize(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getTotalExportedFilesSize(executionId));
			 totalFilesSize	=	Math.round(totalFilesSize.doubleValue()/(1024*1024));
			 jobStatistics.setArchivedVolume(totalFilesSize);
		
		 }else if(jobStatistics.getJob().getActionType().contains("RESTORE")){

			 String val = statisticsController.getRestoredFailedCount(executionId);
			 System.out.println(val);
			 if(val.equals("null")){
				 System.out.println(statisticsController.getRestoredFailedCount(executionId)+"-------------");
			 }
			 Long totalFailedCount = val.equals("null")?0l:Long.parseLong(val);
			 jobStatistics.setTotalFailed(totalFailedCount);
			 Long totalEvaluatedCount = (statisticsController.getRestoredEvaluatedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getRestoredEvaluatedCount(executionId));
			 jobStatistics.setTotalEvaluated(totalEvaluatedCount); 
			 
			 Long totalAnalysedCount = (statisticsController.getRestoredCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getRestoredCount(executionId));
			 jobStatistics.setTotalFreshArchived(totalAnalysedCount);
			 //jobStatistics.setTAlreadyArchived(totalExportedCount);
			 setJobFlag("RESTORE");//basically its Restore job

			 Long totalFilesSize = (statisticsController.getTotalRestoredFilesSize(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getTotalRestoredFilesSize(executionId));
			 totalFilesSize	=	Math.round(totalFilesSize.doubleValue()/(1024*1024));
			 jobStatistics.setArchivedVolume(totalFilesSize);
	
		 }else if(jobStatistics.getJob().getActionType().contains("INDEXING")){

				 String val = statisticsController.getAnalysedFailedCount(executionId);
				 System.out.println(val);
				 if(val.equals("null")){
					 System.out.println(statisticsController.getAnalysedFailedCount(executionId)+"-------------");
				 }
				 Long totalFailedCount = val.equals("null")?0l:Long.parseLong(val);
				 jobStatistics.setTotalFailed(totalFailedCount);
				 Long totalEvaluatedCount = (statisticsController.getAnalysedEvaluatedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getAnalysedEvaluatedCount(executionId));
				 jobStatistics.setTotalEvaluated(totalEvaluatedCount); 
				 
				 Long totalAnalysedCount = (statisticsController.getAnalysedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getAnalysedCount(executionId));
				 jobStatistics.setTotalFreshArchived(totalAnalysedCount);
				 //jobStatistics.setAlreadyArchived(totalExportedCount);
				 setJobFlag("INDEXING");//basically its Analysed job

				 Long totalFilesSize = (statisticsController.getTotalAnalysedFilesSize(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getTotalAnalysedFilesSize(executionId));
				 totalFilesSize	=	Math.round(totalFilesSize.doubleValue()/(1024*1024));
				 jobStatistics.setArchivedVolume(totalFilesSize);
		
		 }else{
			 Long totalFailedCount = (statisticsController.getFailedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getFailedCount(executionId));
			 jobStatistics.setTotalFailed(totalFailedCount);
			 Long totalEvaluatedCount = (statisticsController.getEvaluatedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getEvaluatedCount(executionId));
			 jobStatistics.setTotalEvaluated(totalEvaluatedCount);
			 
			 Long alreadyArchivedCount = (statisticsController.getAlreadyArchivedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getAlreadyArchivedCount(executionId));
			 jobStatistics.setAlreadyArchived(alreadyArchivedCount);

			 Long totalArchivedCount = (statisticsController.getArchivedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getArchivedCount(executionId));
			 jobStatistics.setTotalFreshArchived(totalArchivedCount-alreadyArchivedCount-totalFailedCount);
			 
			 Long totalStubbedCount = (statisticsController.getStubbedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getStubbedCount(executionId));
			 jobStatistics.setTotalStubbed(totalStubbedCount);
			 setJobFlag("OTHERS");
			 
			 Long totalFilesSize = (statisticsController.getTotalArchivedFilesSize(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getTotalArchivedFilesSize(executionId));
			 totalFilesSize	=	Math.round(totalFilesSize.doubleValue()/(1024*1024));
			 jobStatistics.setArchivedVolume(totalFilesSize);
			 
		 }
		 
		 //check in remote agent hot statistics (through WebServices) if any record of execution exists
		 HotStatistics hotStatistics = HotStatistics.getInstance();
		 //String execId = status.getExecutionId();
		 if(!hotStatistics.getEvaluatedCtr(executionId).equals("null"))
			 jobStatistics.setTotalEvaluated(Long.parseLong(hotStatistics.getEvaluatedCtr(executionId)));
		 
		 if(!hotStatistics.getArchivedCtr(executionId).equals("null"))
			 jobStatistics.setTotalFreshArchived(Long.parseLong(hotStatistics.getArchivedCtr(executionId)));
		 
		 if(!hotStatistics.getExportedCtr(executionId).equals("null"))
			 jobStatistics.setTotalFreshArchived(Long.parseLong(hotStatistics.getExportedCtr(executionId)));
		 
		 if(!hotStatistics.getAlreadyArchivedCtr(executionId).equals("null"))
			 jobStatistics.setAlreadyArchived(Long.parseLong(hotStatistics.getAlreadyArchivedCtr(executionId)));
		 
		 if(!hotStatistics.getStubbedCtr(executionId).equals("null"))
			 jobStatistics.setTotalStubbed(Long.parseLong(hotStatistics.getStubbedCtr(executionId)));
		 
		 if(!hotStatistics.getFailedCtr(executionId).equals("null"))
			 jobStatistics.setTotalFailed(Long.parseLong(hotStatistics.getFailedCtr(executionId)));
		 
		 Long totalSkipped = jobStatistics.getTotalEvaluated() - (jobStatistics.getTotalFailed() + jobStatistics.getTotalFreshArchived() + jobStatistics.getAlreadyArchived());				 
		 jobStatistics.setTotalSkipped(totalSkipped);
		 
		 jobStatisticsList.add(jobStatistics);
		
	 }
	 
	 
	 public void populateNormalStatistics(int jobStatId,String serialNumber){
		 
		
		 JobStatistics jobStatistics = jobStatisticsService.getJobStatisticsById(jobStatId);
		 
		 if(jobStatistics.getJob().getActionType().contains("EXPORT")){
			 setJobFlag("EXPORT");
		 }else if(jobStatistics.getJob().getActionType().contains("RESTORE") || 
				 jobStatistics.getJob().getActionType().contains("RETENSION")){
			 setJobFlag("RESTORE");
		 }else if(jobStatistics.getJob().getActionType().contains("INDEXING")){
			 setJobFlag("INDEXING");
		 } else {
			 setJobFlag("OTHERS");
		 }
		 List<JobStatus> tempStatusList = jobStatusService.getJobStatusByProperty("executionId", jobStatistics.getExecutionId());
		 JobStatus tempJobStatus = tempStatusList.get(0);
		 if(tempJobStatus.getErrorCode().getId()==1){
			 jobStatistics.setStatus(VCSConstants.JOB_STATUS_FAILED);					 
		 }else if(tempJobStatus.getStatus().equals("COMPLETED")){
			 jobStatistics.setStatus(VCSConstants.JOB_STATUS_COMPLETED);
		 } else if(tempJobStatus.getStatus().equals("CANCEL")){
			 jobStatistics.setStatus(VCSConstants.JOB_STATUS_CANCEL);
		 } else if(tempJobStatus.getStatus().equals("CANCELED")){
			 jobStatistics.setStatus(VCSConstants.JOB_STATUS_CANCELED);
		 } else if(tempJobStatus.getStatus().equals("RUNNING")){
			 jobStatistics.setStatus(VCSConstants.JOB_STATUS_PROCESSING);
		 }else{
			 jobStatistics.setStatus(VCSConstants.JOB_STATUS_FAILED);
		 }

		 
		 jobStatistics.setSerialNo(serialNumber);
		 long totalSkipped = jobStatistics.getSkippedProcessfileSkippedHiddenlected()+
				 		 jobStatistics.getSkippedProcessfileSkippedIsdir()+
						 jobStatistics.getSkippedProcessfileSkippedMismatchAge()+
						 jobStatistics.getSkippedProcessfileSkippedMismatchExtension()+
						 jobStatistics.getSkippedProcessfileSkippedMismatchLastaccesstime()+
						 jobStatistics.getSkippedProcessfileSkippedMismatchLastmodifiedtime()+
						 jobStatistics.getSkippedProcessfileSkippedReadOnly()+
						 jobStatistics.getSkippedProcessfileSkippedSymbollink()+
						 jobStatistics.getSkippedProcessfileSkippedTemporary()+
						 jobStatistics.getSkippedProcessfileSkippedTooLarge();
		 long totalFailed = jobStatistics.getTotalFailedArchiving()+
				 		 jobStatistics.getTotalFailedDeduplication()+
						 jobStatistics.getTotalFailedEvaluating()+
						 jobStatistics.getTotalFailedStubbing();
		 jobStatistics.setTotalSkipped(totalSkipped);
		 jobStatistics.setTotalFailed(totalFailed);
	
		 jobStatisticsList.add(jobStatistics);
		 	 
	 }
	 
	 
     public String reRunJob(){
		
		 //Set the current Timestamp, if user selected to Schedule job, but did'nt entered valid time
    	 System.out.println("execTimeStartDate:---> "+execTimeStartDate);
		 if(job.getScheduled()!=null && job.getScheduled().equals("yes")) {
			  job.setIsScheduled(true);
			 if(job.getExecTimeStartDate()==null || job.getExecTimeStartDate().toString().isEmpty()){
				 Date date = VCSUtil.getCurrentDateTime();
				 Timestamp timestamp = new Timestamp(date.getTime());			 
				 job.setExecTimeStartDate(timestamp);
			 }
		 }
		 
		 if("RETENSION".equals(job.getActionType())) {//An option for Retention job
			 job.setActiveActionType(job.getNextAction());
		 }
		 
	        Timestamp timestamp;
	        if(job.getScheduled()!=null && job.getScheduled().equals("no")){
	        	 job.setIsScheduled(false);
	        	 Date date = VCSUtil.getCurrentDateTime();
		         timestamp = new Timestamp(date.getTime());
		         job.setReadyToExecute(true);
	        }else{
	        	timestamp = new Timestamp(job.getExecTimeStartDate().getTime());

		        job.setReadyToExecute(false);
	        }	        
	        job.setExecTimeStart(timestamp);
	        
        if("RETENSION".equals(job.getActionType())) {
        	jobService.saveRetentionJob(job);
        	System.out.println("retention job saved");
        	
        } else {
        	jobService.saveJob(job);
        	System.out.println("job saved");
        }
	        
		 return "JobStatus.faces?faces-redirect=true";
	 }
	
 	public void pauseJob(){
		   String jobId=getRequestParameter("jobId");
		   Integer id=VCSUtil.getIntValue(jobId);
		   Job job = jobService.getJobById(id);
		   if(job!=null){
			   if(job.getActionType().equals("DYNARCH") || job.getActionType().equals(VCSConstants.ACTIVE_ARCHIVING)){
//				   job.setReadyToExecute(false);
				   job.setCurrentStatus("PAUSE");
				   jobService.saveJob(job);
				   ErrorCode errorCode  =   errorCodeService.getById(0);
				   	JobStatus prevJobStatus = getPreviousJobStatusInstance(job);
			        JobStatus jobStatus=new JobStatus();
			        
			        jobStatus.setExecutionId(prevJobStatus.getExecutionId());
			        jobStatus.setExecStartTime(getCurrentDateTime());
			        jobStatus.setErrorCode(errorCode);
	                jobStatus.setJob(job);
	                jobStatus.setStatus(VCSConstants.JOB_STATUS_PAUSE);
	                jobStatus.setDescription("pause ... > job.actionType: " + job.getActionType());
	                jobStatus.setCurrent(1);
	                jobStatus.setPreviousJobStatusId(prevJobStatus.getId());
	                jobStatusService.saveJobStatus(jobStatus);
			   }
		   }
	}

	public void resumeJob(){
		   String jobId=getRequestParameter("jobId");
		   Integer id=VCSUtil.getIntValue(jobId);
		   Job job = jobService.getJobById(id);
		   if(job!=null){
			   if(job.getActionType().equals("DYNARCH") || job.getActionType().equals(VCSConstants.ACTIVE_ARCHIVING)){
				   job.setReadyToExecute(true);
				   job.setCurrentStatus(VCSConstants.JOB_STATUS_RUN);
				   jobService.saveJob(job);
				   
				   ErrorCode errorCode  =   errorCodeService.getById(0);
				   JobStatus prevJobStatus = getPreviousJobStatusInstance(job);
			       JobStatus jobStatus=new JobStatus();
			        
			       jobStatus.setExecutionId(prevJobStatus.getExecutionId());
			       jobStatus.setExecStartTime(getCurrentDateTime());
			       jobStatus.setErrorCode(errorCode);
	               jobStatus.setJob(job);
	               jobStatus.setStatus(VCSConstants.JOB_STATUS_RUN);
	               jobStatus.setDescription("run ... > job.actionType: " + job.getActionType());
	               jobStatus.setCurrent(1);
	               jobStatus.setPreviousJobStatusId(prevJobStatus.getId());
	               jobStatusService.saveJobStatus(jobStatus);
			   }
		   }
	}
     
     
	public void cancelJob(){
		   String jobId=getRequestParameter("jobId");
		   Integer id=VCSUtil.getIntValue(jobId);
		   List<JobStatus> jobStatsList=jobStatusService.getJobStatusByJob(id);
		   Integer jobStatusId=null;		   
		   Job tempJob = null;
		   if(jobStatsList!=null && jobStatsList.size()>0){
			   JobStatus jobStatus=jobStatsList.get(0);
			   JobStatus _instance=new JobStatus();
			   _instance.setDescription("blocking...");
			   _instance.setExecStartTime(jobStatus.getExecStartTime());
			   _instance.setErrorCode(jobStatus.getErrorCode());
			   _instance.setExecutionId(jobStatus.getExecutionId());
			   _instance.setCurrent(1);
			   tempJob = jobStatus.getJob();
			   _instance.setJob(jobStatus.getJob());
			   _instance.setPreviousJobStatusId(jobStatus.getId());
			   _instance.setStatus(VCSConstants.JOB_STATUS_CANCEL);
			  
			   jobStatusService.saveJobStatus(_instance);
		   }
		   
		   if(job.getActionType().contains(VCSConstants.ACTIVE_ARCHIVING)){
			   tempJob.setCurrentStatus(VCSConstants.JOB_STATUS_CANCEL);
			   jobService.saveJob(tempJob);
		   }
		   
		   if(jobStatusId!=null){			 
			   List<Integer> list=jobService.getCancelJobs();
			   System.out.println("Cancel Jobs List --->"+list.toString());
		   }
	}
	
	 private JobStatus getPreviousJobStatusInstance(Job jobInstance) {
	      
	      JobStatus jobStatusInstance   =   null;
         String jobId=jobInstance.getId()+"";
         System.out.println("Getting prev job status for job id-->"+jobId);
	      List<JobStatus> jobStatusList=jobStatusService.getJobStatusByJob(Integer.parseInt(jobId));

	        if(jobStatusList!=null && jobStatusList.size() >   0){
	            jobStatusInstance   =   (JobStatus) jobStatusList.get(0);
	            System.out.println("Prev Job Status Id:--:--: "+jobStatusInstance.getId()); 
	            return jobStatusInstance;
	        
	        }
	  return null;
}
     
	 private Timestamp getCurrentDateTime() {
	       
	        Calendar cal = new GregorianCalendar();
	        java.sql.Timestamp timeStampDate=null;
	        SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");

	        SimpleDateFormat dateFormat = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
	        Date convertedDate=null;
			try {
				convertedDate = dateFormat.parse(sdf.format(cal.getTime()));
				timeStampDate = new 	Timestamp(convertedDate.getTime());
			} catch (ParseException e) {				
				e.printStackTrace();
			}
	        return timeStampDate;
	    }
	
	public void swithTabAction(){
		showStatusGrid=false;
		
		if(jobStatusList!=null){
			jobStatusList.clear();
		}
		if(jobStatisticsList!=null){
			jobStatisticsList.clear();
		}
		
	}

	private void clearJobsList(){
		allJobsList.clear();
		archivingJobsList.clear();		
		stubbingJobsList.clear();		
		evaluationJobsList.clear();		
		exportJobsList.clear();		
		retentionJobsList.clear();	
		indexingJobsList.clear();
		activeArchivingJobsList.clear();
		dynaArchivingJobsList.clear();
		scheduledJobsList.clear();
		withoutStubJobsList.clear();
		
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public String getJobSource() {
		return jobSource;
	}

	public void setJobSource(String jobSource) {
		this.jobSource = jobSource;
	}
	
	
	public List<UISelectItem> getJobSourceList() {
		return jobSourceList;
	}

	public void setJobSourceList(List<UISelectItem> jobSourceList) {
		this.jobSourceList = jobSourceList;
	}

	public boolean isShowStatusGrid() {
		return showStatusGrid;
	}

	public void setShowStatusGrid(boolean showStatusGrid) {
		this.showStatusGrid = showStatusGrid;
	}

	
	public List<Job> getAllJobsList() {
		return allJobsList;
	}

	public void setAllJobsList(List<Job> allJobsList) {
		this.allJobsList = allJobsList;
	}

	public List<Job> getArchivingJobsList() {
		return archivingJobsList;
	}

	public void setArchivingJobsList(List<Job> archivingJobsList) {
		this.archivingJobsList = archivingJobsList;
	}

	public List<Job> getStubbingJobsList() {
		return stubbingJobsList;
	}

	public void setStubbingJobsList(List<Job> stubbingJobsList) {
		this.stubbingJobsList = stubbingJobsList;
	}

	public List<Job> getEvaluationJobsList() {
		return evaluationJobsList;
	}

	public void setEvaluationJobsList(List<Job> evaluationJobsList) {
		this.evaluationJobsList = evaluationJobsList;
	}

	public List<Job> getIndexingJobsList() {
		return indexingJobsList;
	}

	public void setIndexingJobsList(List<Job> indexingJobsList) {
		this.indexingJobsList = indexingJobsList;
	}

	public List<Job> getExportJobsList() {
		return exportJobsList;
	}

	public void setExportJobsList(List<Job> exportJobsList) {
		this.exportJobsList = exportJobsList;
	}

	public List<Job> getRetentionJobsList() {
		return retentionJobsList;
	}

	public void setRetentionJobsList(List<Job> retentionJobsList) {
		this.retentionJobsList = retentionJobsList;
	}

	public List<Job> getActiveArchivingJobsList() {
		return activeArchivingJobsList;
	}

	public void setActiveArchivingJobsList(List<Job> activeArchivingJobsList) {
		this.activeArchivingJobsList = activeArchivingJobsList;
	}

	public List<Job> getDynaArchivingJobsList() {
		return dynaArchivingJobsList;
	}

	public void setDynaArchivingJobsList(List<Job> dynaArchivingJobsList) {
		this.dynaArchivingJobsList = dynaArchivingJobsList;
	}
	
	public List<Job> getScheduledJobsList() {
		return scheduledJobsList;
	}

	public void setScheduledJobsList(List<Job> scheduledJobsList) {
		this.scheduledJobsList = scheduledJobsList;
	}

	public List<JobStatistics> getJobStatisticsList() {
		return jobStatisticsList;
	}

	public void setJobStatisticsList(List<JobStatistics> jobStatisticsList) {
		this.jobStatisticsList = jobStatisticsList;
	}

	public List<JobStatus> getJobStatusList() {
		return jobStatusList;
	}

	public void setJobStatusList(List<JobStatus> jobStatusList) {
		this.jobStatusList = jobStatusList;
	}

	public StatisticsController getStatisticsController() {
		return statisticsController;
	}

	public void setStatisticsController(StatisticsController statisticsController) {
		this.statisticsController = statisticsController;
	}

	public boolean isExportJob() {
		return exportJob;
	}

	public void setExportJob(boolean exportJob) {
		this.exportJob = exportJob;
	}

	public String getJobFlag() {
		return jobFlag;
	}

	public void setJobFlag(String jobFlag) {
		this.jobFlag = jobFlag;
	}

	public boolean isHotStatistics() {
		return hotStatistics;
	}

	public void setHotStatistics(boolean hotStatistics) {
		this.hotStatistics = hotStatistics;
	}

	public List<ExportJobPath> getExportPathList() {
		return exportPathList;
	}

	public void setExportPathList(List<ExportJobPath> exportPathList) {
		this.exportPathList = exportPathList;
	}

	public boolean isEnableSP() {
		return enableSP;
	}

	public void setEnableSP(boolean enableSP) {
		this.enableSP = enableSP;
	}

	public boolean isEnablePrintArch() {
		return enablePrintArch;
	}

	public void setEnablePrintArch(boolean enablePrintArch) {
		this.enablePrintArch = enablePrintArch;
	}


	public Date getExecTimeStartDate() {
		return execTimeStartDate;
	}


	public void setExecTimeStartDate(Date execTimeStartDate) {
		this.execTimeStartDate = execTimeStartDate;
	}


	public boolean isReRunEnabled() {
		return reRunEnabled;
	}


	public void setReRunEnabled(boolean reRunEnabled) {
		this.reRunEnabled = reRunEnabled;
	}
	
	
	public void enableReRun(){
		System.out.println("$$$$ "+reRunEnabled);
	}


	public boolean isEnableArchiveTab() {
		return enableArchiveTab;
	}


	public void setEnableArchiveTab(boolean enableArchiveTab) {
		this.enableArchiveTab = enableArchiveTab;
	}


	public boolean isEnableStubTab() {
		return enableStubTab;
	}


	public void setEnableStubTab(boolean enableStubTab) {
		this.enableStubTab = enableStubTab;
	}


	public boolean isEnableEvalTab() {
		return enableEvalTab;
	}


	public void setEnableEvalTab(boolean enableEvalTab) {
		this.enableEvalTab = enableEvalTab;
	}


	public boolean isEnableAnalysisTab() {
		return enableAnalysisTab;
	}


	public void setEnableAnalysisTab(boolean enableAnalysisTab) {
		this.enableAnalysisTab = enableAnalysisTab;
	}


	public boolean isEnableExportTab() {
		return enableExportTab;
	}


	public void setEnableExportTab(boolean enableExportTab) {
		this.enableExportTab = enableExportTab;
	}


	public boolean isEnableActiveArchiveTab() {
		return enableActiveArchiveTab;
	}


	public void setEnableActiveArchiveTab(boolean enableActiveArchiveTab) {
		this.enableActiveArchiveTab = enableActiveArchiveTab;
	}


	public boolean isEnableDynaArchiveTab() {
		return enableDynaArchiveTab;
	}


	public void setEnableDynaArchiveTab(boolean enableDynaArchiveTab) {
		this.enableDynaArchiveTab = enableDynaArchiveTab;
	}

	public boolean isEnableScheduledTab() {
		return enableScheduledTab;
	}

	public void setEnableScheduledTab(boolean enableScheduledTab) {
		this.enableScheduledTab = enableScheduledTab;
	}

	public List<Job> getWithoutStubJobsList() {
		return withoutStubJobsList;
	}

	public void setWithoutStubJobsList(List<Job> withoutStubJobsList) {
		this.withoutStubJobsList = withoutStubJobsList;
	}

	public boolean isEnableWithoutStubTab() {
		return enableWithoutStubTab;
	}

	public void setEnableWithoutStubTab(boolean enableWithoutStubTab) {
		this.enableWithoutStubTab = enableWithoutStubTab;
	}	
	
	
	
}
