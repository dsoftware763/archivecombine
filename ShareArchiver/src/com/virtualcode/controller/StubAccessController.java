package com.virtualcode.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.jcr.RepositoryException;

import jcifs.smb.SmbAuthException;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;

import net.coobird.thumbnailator.Thumbnails;

import org.apache.commons.io.FileUtils;
import org.icepdf.core.exceptions.PDFException;
import org.icepdf.core.exceptions.PDFSecurityException;
import org.icepdf.core.pobjects.Document;
import org.icepdf.core.pobjects.Page;
import org.icepdf.core.util.GraphicsRenderingHints;

import sun.util.logging.resources.logging;

import com.virtualcode.cifs.SmbWriter;
import com.virtualcode.common.SecureURL;
import com.virtualcode.common.VCSConstants;
import com.virtualcode.repository.UserActivityService;
import com.virtualcode.schedular.PLVStatus;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.StringUtils;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.CustomTreeNode;
import com.virtualcode.vo.DriveLetters;
import com.virtualcode.vo.SystemCodeType;

@ManagedBean(name = "stubAccessController")
@ViewScoped
public class StubAccessController extends AbstractController {

	public StubAccessController() {
		super();
		// System.out.println("polled");

	}

	private String nodeID;

	private boolean authorize;

	private CustomTreeNode selectedNode;
		
	private List<CustomTreeNode> nextVersionsList=null;

	private String restorePath; // only for stub access direct restore

	SecureURL secureURL = SecureURL.getInstance();
	
	private boolean alreadyExists = false;

	public void populateStubInfo() {

		if (nodeID == null || FacesContext.getCurrentInstance().isPostback())
			return;
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		nodeID = StringUtils.decodeUrl(nodeID);
		System.out
				.println("username for stub access : "
						+ getHttpSession().getAttribute(
								VCSConstants.SESSION_USER_NAME));
		String username = (String) getHttpSession().getAttribute(
				VCSConstants.SESSION_USER_NAME);
		selectedNode = fileExplorerService.getNodeByUUID(nodeID);

		if (selectedNode == null) {
			System.out
					.println("getNodeByUUID not found, going to search by path");
			selectedNode = fileExplorerService.getNodeByPath(nodeID);
		}

		if (selectedNode != null) {
			
			//Swap the node to display the very first version of Versioned Stub.. as we'll show the version in sub-grid
			if(selectedNode.getVersionOf()!=null && !selectedNode.getVersionOf().isEmpty()) {
				selectedNode	=	fileExplorerService.getNodeByUUIDOfJcr(selectedNode.getVersionOf().trim());
				System.out.println("--It has version "+selectedNode.getNextVersionsSize());
				
				nextVersionsList	=	selectedNode.getNextVersions();
			} //else do nothing
			nextVersionsList	=	selectedNode.getNextVersions();
			authorize = fileExplorerService.authorizeStub(nodeID, username);
//			 authorize=true;
			if (!authorize) {
				String accessDeniedMessage = ru.getCodeValue(
						"accessDeniedMessage", SystemCodeType.GENERAL);
				// addErrorMessage("Access Denied, Please contact your systems administrator",
				// "Access denied");
				addErrorMessage(accessDeniedMessage, accessDeniedMessage);
				return;
			}
			System.out.println("Decoded UUID:" + selectedNode.getPath());

			String showDriveLetter = ru.getCodeValue("enable.stub.driveLetter",
					SystemCodeType.GENERAL);

			String path = "";
			String[] elements = StringUtils.getPathElements(selectedNode
					.getPath());
			if (showDriveLetter != null && showDriveLetter.trim().equals("yes")) {

				if (!(selectedNode.getPath().contains("/SP/"))) {
					Map<String, String> mappingList = fileExplorerService
							.getDriveLettersMap();
					String matchStr = elements[1] + "/" + elements[2] + "/"
							+ elements[3] + "/" + elements[4]; // e.g.
																// TestCompany/FS/192.168.30.24/LargeFile
					if (mappingList.containsKey(matchStr.toLowerCase())) {
						String driveLtr = (String) mappingList.get(matchStr
								.toLowerCase());
						path = driveLtr;
					} else {
						path = "\\\\" + elements[3] + "\\" + elements[4];// if
																			// drive
																			// letter
																			// is
																			// enabled
																			// for
																			// stub
																			// access
																			// but
																			// not
																			// found
					}
				}
			} else {
				path = "\\\\" + elements[3] + "\\" + elements[4]; // if drive
																	// letter is
																	// disabled
																	// for stub
																	// access
			}

			path += "\\";

			for (int i = 5; i < elements.length; i++) {
				path += elements[i];
				if (i != elements.length - 1) {
					path += "\\";
				}
			}
			System.out.println(path);

			selectedNode.setCompressedURL(path);
			
			/* /Associate Versions (if its a version doc)
			if(selectedNode.getVersionOf()!=null && !selectedNode.getVersionOf().isEmpty()) {
				parentVersion	=	fileExplorerService.getNodeByUUIDOfJcr(selectedNode.getVersionOf().trim());
				System.out.println("--It has version of "+parentVersion.getNextVersionsSize());
			} else {
				parentVersion	=	null;
			}*/
			
		} else {
			String fileNotFoundMessage = ru.getCodeValue(
					"documentNotFoundMessage", SystemCodeType.GENERAL);
			// addErrorMessage("Requested file has not been found, you may try searching for the file in the Archive or contact your System Administrator for further assistance","File not found");
			addErrorMessage(fileNotFoundMessage, fileNotFoundMessage);
			return;
		}
		// authorize = true;

		if (!authorize) {
			String accessDeniedMessage = ru.getCodeValue("accessDeniedMessage",
					SystemCodeType.GENERAL);
			// addErrorMessage("Access Denied, Please contact your systems administrator",
			// "Access denied");
			addErrorMessage(accessDeniedMessage, accessDeniedMessage);
			return;
		}

		if (authorize && selectedNode != null) {
			UserActivityService userActivityService = serviceManager
					.getUserActivityService();
			userActivityService.recordDocAccessedActivity(username,
					getHttpRequest().getRemoteAddr(), getHttpSession().getId(),
					getHttpSession().getCreationTime(), selectedNode.getPath());
		}
		
		
		if(authorize && selectedNode!=null){
			
			PLVStatus plvStatus=PLVStatus.getInstance();
			if(!plvStatus.isValid()){
				String strToday=VCSUtil.getDateAsString(new Date());
				Map<String, Integer> stubAccessMap=plvStatus.getStubAccessMap();
				
				if (stubAccessMap.containsKey(strToday) && stubAccessMap.get(strToday)>5){
					addErrorMessage("Your License has expired,You can't access more than 5 documents/day.", "Your License has expired,You can't access more than 5 documents/day.");
				    authorize=false;
					return;
				}
		   }
		}
		
		//get content thumbnail
		currentThumbnailPath = "";
		showThumbnail = false;
		
		String fileExt = selectedNode.getName().substring(selectedNode.getName().indexOf(".")).toLowerCase();
		if("yes".equals(ru.getCodeValue("enable_thumbnails_stub",SystemCodeType.GENERAL))){
			
			if(fileExt.contains(".jpeg") || fileExt.contains(".bmp") || fileExt.contains(".gif")
					|| fileExt.contains(".png") || fileExt.contains(".jpg") || fileExt.contains(".pdf")){
				
				long start = System.currentTimeMillis();
				
				// TODO Auto-generated method stub
				try {
					InputStream is = fileExplorerService.downloadFile(selectedNode.getPath()).getStream();
					if(fileExt.contains(".pdf")){
						try {
							Document document = new Document();
							document.setInputStream(is, "/temp/");
							BufferedImage image;// =page.convertToImage();//convertToImage();
							ByteArrayOutputStream baos = new ByteArrayOutputStream();
							

							image = (BufferedImage) document.getPageImage(0, GraphicsRenderingHints.SCREEN,
			                        Page.BOUNDARY_CROPBOX, 0.0f, 1.0f);

							ImageIO.write(image, "png", baos);
							is = new ByteArrayInputStream(baos.toByteArray());
							
						} catch (IOException | PDFException | PDFSecurityException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}  
			        String sessionId=getHttpSession().getId();
			              
		            File outputDir=new File(getContextRealPath()+"/thumbnails/"+sessionId);
		            if(!outputDir.exists()){
		           	  outputDir.mkdirs();
		            }
		            FileUtils.cleanDirectory(outputDir);	// delete any existing files in the dir
					Thumbnails.of(is).size(160, 160).toFile(new File(getContextRealPath()+"/thumbnails/"+sessionId +"/" + selectedNode.getName().substring(0, selectedNode.getName().lastIndexOf(".")) + ".png"));
					System.out.println("Thumbnail creation successful....");
					showThumbnail = true;
					currentThumbnailPath = "thumbnails/"+sessionId +"/" + selectedNode.getName().substring(0, selectedNode.getName().lastIndexOf(".")) + ".png";

				} catch (IOException | RepositoryException e) {
					// TODO Auto-generated catch block
					System.out.println("Thumbnail cannot be generated for " + selectedNode.getName() + ". Reason: " + e.getMessage());
	//				e.printStackTrace();
				}
				System.out.println("Time taken: "+ (System.currentTimeMillis()-start) + " ms");				
			}
		}

	}


    public void fileSelected(){
    	String selectedNodePath=getRequestParameter("selectedNodePath");
    	if(selectedNodePath!=null && ! selectedNodePath.trim().equals("")){
    		
    		this.selectedNode=fileExplorerService.getNodeByPath(selectedNodePath);
    		if(selectedNode.getType().equals("nt:folder")){
    			selectedNode.setFileFoldersCount(fileExplorerService.getFileFoldersCount(selectedNode.getPath()));
    			//folderSizeMb=fileExplorerService.getFolderSize(selectedNode.getPath());
    			
    		}
    		//selectedNodePath=null;
    	}else{
    		//selectedNodePath=null;
    		selectedNode=null;
    	}
	}
    
	public void downloadFile() {
		try {
			String url = "downloadfile.jsp?nodeID="
					+ selectedNode.getDownloadUrl();

			if (selectedNode != null) {
				String username = (String) getHttpSession().getAttribute(
						VCSConstants.SESSION_USER_NAME);
				UserActivityService userActivityService = serviceManager
						.getUserActivityService();
				userActivityService.recordDownloadActivity(username,
						getHttpRequest().getRemoteAddr(), getHttpSession()
								.getId(), getHttpSession().getCreationTime(),
						selectedNode.getPath());
			}

			getHttpResponse().sendRedirect(url);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public Integer getCompletedPercentage() {

		if (selectedNode == null) {
			return 0;
		}
		String msg = getJobMessage(selectedNode.getPath().substring(1));

		// if(enabled){
		Integer percentage = SmbWriter.getCompletedPercentage(selectedNode
				.getPath().substring(1));
		if (percentage != null && percentage.intValue() == 100) {
			SmbWriter.removeJob(selectedNode.getPath().substring(1));
			enabled = false;
			// showProgress = false;
			completed = true;
		}
		if (completed)
			return 100;

		return percentage;
		// }

		// return 100;
	}

	public void setCompletedPercentage(Integer i) {

		// return 100;
	}

	public void restoreFile() {
		try {
			// startProcess();

			message = null;
			String filePath = null;
			System.out.println("restore path : " + restorePath);
			if (restorePath != null && !(restorePath.isEmpty())){
				if(FacesContext.getCurrentInstance().isPostback())
					return;
//				return;
			
				restorePath = StringUtils.decodeUrl(restorePath);
				String username = (String) getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
				selectedNode = fileExplorerService.getNodeByUUID(restorePath);
	
				if (selectedNode == null) {
					System.out.println("getNodeByUUID not found, going to search by path");
					selectedNode = fileExplorerService.getNodeByPath(restorePath);
				}
				ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
	
				if (selectedNode != null) {
					authorize = fileExplorerService.authorizeStub(restorePath, username);
	//				 authorize=true;
					if (!authorize) {
						String accessDeniedMessage = ru.getCodeValue(
								"accessDeniedMessage", SystemCodeType.GENERAL);
						// addErrorMessage("Access Denied, Please contact your systems administrator",
						// "Access denied");
						addErrorMessage(accessDeniedMessage, accessDeniedMessage);
						return;
					}
				}else {
					String fileNotFoundMessage = ru.getCodeValue(
							"documentNotFoundMessage", SystemCodeType.GENERAL);
					// addErrorMessage("Requested file has not been found, you may try searching for the file in the Archive or contact your System Administrator for further assistance","File not found");
					addErrorMessage(fileNotFoundMessage, fileNotFoundMessage);
					return;
				}
				// authorize = true;
	
				if (!authorize) {
					String accessDeniedMessage = ru.getCodeValue("accessDeniedMessage",
							SystemCodeType.GENERAL);
					// addErrorMessage("Access Denied, Please contact your systems administrator",
					// "Access denied");
					addErrorMessage(accessDeniedMessage, accessDeniedMessage);
					return;
				}
			}

			// restorePath=StringUtils.decodeUrl(restorePath);
//			if (restorePath != null && restorePath.trim().length() > 0)
//				filePath = restorePath; // will work only if stub_access_mode is
										// set to 3 (restore)

			if (selectedNode != null)
				filePath = selectedNode.getPath();

			if (filePath != null && filePath.indexOf("/") == 0) {
				filePath = filePath.substring(1);
			}
			if (restoreLocation != null && restoreLocation.trim().length() > 0) {
				restoreLocation = restoreLocation.replaceAll("\\\\", "/");
				if (restoreLocation.startsWith("//"))
					restoreLocation = restoreLocation.substring(2);
			}
			if(SmbWriter.isJobRunning(selectedNode.getPath().substring(1))){
				addInfoMessage("File restore already in progress", "File restore already in progress");
				return;	//exit if the file is already being restored
			}
			if(testUNC().equals("failure"))
				return;
			
			if(restoreLocation==null || restoreLocation.length()<1){
				String uncPath=getUNCPath(filePath);
				restoreLocation=uncPath.substring(2).replaceAll("\\\\", "/");
				restoreLocation = restoreLocation.substring(0, restoreLocation.lastIndexOf("/"));//remove filename
			}
			
			
			//first check if the file already exists
			alreadyExists = fileExplorerService.alreadyExistsForRestore(restoreLocation + "/" + selectedNode.getName());
//			alreadyExists = true;
			if(alreadyExists){
				return;
			}
			showProgress = true;
			enabled = true;
			String message = fileExplorerService.restoreFile(filePath,
					restoreLocation, false);

			// start=false;
		} catch (Exception ex) {
			ex.printStackTrace();
			// start=false;
		}
	}
	
	public void restoreFileAE() {
		try {
			// startProcess();
			showProgress = true;
			enabled = true;
			message = null;
			String filePath = null;
			System.out.println("restore path : " + restorePath);
			if (restorePath != null && !(restorePath.isEmpty())){
				if(FacesContext.getCurrentInstance().isPostback())
					return;
//				return;
			
				restorePath = StringUtils.decodeUrl(restorePath);
				String username = (String) getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
				selectedNode = fileExplorerService.getNodeByUUID(restorePath);
	
				if (selectedNode == null) {
					System.out.println("getNodeByUUID not found, going to search by path");
					selectedNode = fileExplorerService.getNodeByPath(restorePath);
				}
				ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
	
				if (selectedNode != null) {
					authorize = fileExplorerService.authorizeStub(restorePath, username);
	//				 authorize=true;
					if (!authorize) {
						String accessDeniedMessage = ru.getCodeValue(
								"accessDeniedMessage", SystemCodeType.GENERAL);
						// addErrorMessage("Access Denied, Please contact your systems administrator",
						// "Access denied");
						addErrorMessage(accessDeniedMessage, accessDeniedMessage);
						return;
					}
				}else {
					String fileNotFoundMessage = ru.getCodeValue(
							"documentNotFoundMessage", SystemCodeType.GENERAL);
					// addErrorMessage("Requested file has not been found, you may try searching for the file in the Archive or contact your System Administrator for further assistance","File not found");
					addErrorMessage(fileNotFoundMessage, fileNotFoundMessage);
					return;
				}
				// authorize = true;
	
				if (!authorize) {
					String accessDeniedMessage = ru.getCodeValue("accessDeniedMessage",
							SystemCodeType.GENERAL);
					// addErrorMessage("Access Denied, Please contact your systems administrator",
					// "Access denied");
					addErrorMessage(accessDeniedMessage, accessDeniedMessage);
					return;
				}
			}

			// restorePath=StringUtils.decodeUrl(restorePath);
//			if (restorePath != null && restorePath.trim().length() > 0)
//				filePath = restorePath; // will work only if stub_access_mode is
										// set to 3 (restore)

			if (selectedNode != null)
				filePath = selectedNode.getPath();

			if (filePath != null && filePath.indexOf("/") == 0) {
				filePath = filePath.substring(1);
			}
			if (restoreLocation != null && restoreLocation.trim().length() > 0) {
				restoreLocation = restoreLocation.replaceAll("\\\\", "/");
				if (restoreLocation.startsWith("//"))
					restoreLocation = restoreLocation.substring(2);
			}
			if(SmbWriter.isJobRunning(selectedNode.getPath().substring(1))){
				addInfoMessage("File restore already in progress", "File restore already in progress");
				return;	//exit if the file is already being restored
			}
			if(testUNC().equals("failure"))
				return;
			
			if(restoreLocation==null || restoreLocation.length()<1){
				String uncPath=getUNCPath(filePath);
				restoreLocation=uncPath.substring(2).replaceAll("\\\\", "/");
				restoreLocation = restoreLocation.substring(0, restoreLocation.lastIndexOf("/"));//remove filename
			}
			
			
			String message = fileExplorerService.restoreFile(filePath,
					restoreLocation, false);

			// start=false;
		} catch (Exception ex) {
			ex.printStackTrace();
			// start=false;
		}
	}	
	public String getUNCPath(String path){
		String unc=null;
		if(path!=null){
			if(path.indexOf("/FS/")>0){
			  unc=path.substring(path.indexOf("/FS/")+4, path.length());
			}else if(path.indexOf("/SP/")>0){
				 unc=path.substring(path.indexOf("/SP/")+4, path.length());
		    }
		}
		if(unc!=null){
			System.out.println("unc before replacing: "+unc);
			unc="\\\\"+unc.replaceAll("/", "\\\\");
		}
		return unc;
	}
	
	public String testUNC(){
		 ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
		 String remoteUNC = "";//selectedNode.getPath();
		if (restoreLocation != null && restoreLocation.trim().length() > 0) {
			restoreLocation = restoreLocation.replaceAll("\\\\", "/");
			if (restoreLocation.startsWith("//"))
				restoreLocation = restoreLocation.substring(2);
			
			remoteUNC = restoreLocation;
		}else{	 
		 
			 String[] pathElements = com.virtualcode.util.StringUtils.getPathElements(selectedNode.getPath());
	        for (int i = 0; i < pathElements.length; i++) {
	            System.out.println("elements: "+pathElements[i]);
	            if (i > 2) {
	                remoteUNC += pathElements[i] +"/";
	            }
	        }
	        remoteUNC = remoteUNC.substring(0, remoteUNC.length()-1);
	        remoteUNC = remoteUNC.substring(0, remoteUNC.lastIndexOf("/"));
		}
		DriveLetters driveLetter = null;
		 String remoteDomainName = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
		 String remoteUserName = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
		 String remoteUserPassword = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);

		 if(remoteDomainName==null || remoteDomainName.isEmpty()){
			 
				List<DriveLetters> driveLetterList=driveLettersService.getAll();

				for(DriveLetters letter: driveLetterList){
					String sharePath = letter.getSharePath().toLowerCase();
					if(sharePath.startsWith("\\\\")){
//						ResourcesUtil ru = ResourcesUtil.getResourcesUtil();				
						sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase() + "/fs/" + sharePath.substring(2).replaceAll("\\\\", "/");
					}
					if(sharePath.endsWith("/"))
						sharePath = sharePath.substring(0, sharePath.lastIndexOf("/"));
					String compareStr = remoteUNC.trim();
					if(!compareStr.contains("/fs/"))
						compareStr = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase()+"/fs/"+remoteUNC.trim().toLowerCase();
					System.out.println(compareStr+", " + sharePath);
					if(remoteUNC.trim()!=null && compareStr.contains(sharePath)){
						driveLetter = letter;
						break;
					}
				}
		 }
		 
		 if(driveLetter!=null){
			 remoteDomainName = driveLetter.getRestoreDomain();
			 remoteUserName = driveLetter.getRestoreUsername();
			 remoteUserPassword = driveLetter.getRestorePassword();
		 }
		 String testUNCResponse = "";
		 System.out.println("---------------------verifying UNC-------------------");
		 boolean error = false;
		 if(remoteUNC.trim().equals("") ||remoteUNC.trim().length()<3){// test as in existing app
			 addErrorMessage("UNC incorrect", "Invalid UNC Path");
//			 addCompErrorMessage("searchForm:hiddenPercent", "UNC incorrect", "Invalid UNC Path");
//			 setTestUNCResponse("wrong");
			 error = true;
		 }else if(remoteUNC.trim().indexOf("\\\\", 2)!=-1){		 
			 addErrorMessage("UNC format incorrect(Only two \\\\ allowed in the start)", "UNC format incorrect(Only two \\\\ allowed in the start)");
//			 addCompErrorMessage("searchForm:hiddenPercent", "UNC format incorrect(Only two \\\\ allowed in the start)", "UNC format incorrect(Only two \\\\ allowed in the start)");
			 error = true;
		 }else if(remoteUserPassword.trim().contains("@")){		 
			 addErrorMessage("Password cannot contain '@'","Password cannot contain '@'");
//			 addCompErrorMessage("searchForm:hiddenPercent", "Password cannot contain '@'","Password cannot contain '@'");
			 error = true;
		 }else{
			 String destPath = "";
			 destPath = VCSUtil.getSambaPath(remoteDomainName, remoteUserName, remoteUserPassword, remoteUNC.trim());
//			 if(remoteDomainName!=null && remoteDomainName.trim().length()<1)				 
//				 destPath = "smb://" + remoteDomainName + ";" + remoteUserName + ":" + remoteUserPassword + "@" + remoteUNC.substring(2,remoteUNC.length()).replaceAll("\\\\","/").trim() + "/";
//			 else
//				 destPath = "smb://"+ remoteUserName + ":" + remoteUserPassword + "@" + remoteUNC.substring(2,remoteUNC.length()).replaceAll("\\\\","/").trim() + "/";
			 
			 System.out.println("destination path: " + destPath);
			 try {
				SmbFile file = new SmbFile(destPath);
				if(file==null || !file.exists()){
					addErrorMessage("UNC path not found","UNC path not found");
//					addCompErrorMessage("searchForm:hiddenPercent", "UNC path not found","UNC path not found");
					error = true;
				}else{
					System.out.println(file.list()); //throws exception access denied if user does not have permission
					//now to check if the user has write permissions on the directory
					SmbFile fileTemp = new SmbFile(destPath+"/sacheck.tmp");
					SmbFileOutputStream sfos = new SmbFileOutputStream(fileTemp);
					sfos.write("Testing write permissions".getBytes());
					sfos.flush();
					sfos.close();
					fileTemp.delete();	//delete after testing
					
//					addCompInfoMessage("searchForm:hiddenPercent", "UNC path verified","UNC path verified");
				}
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				addErrorMessage("Error verifying credentials","Error verifying credentials");
//				addCompErrorMessage("searchForm:hiddenPercent","Error verifying credentials","Error verifying credentials");
				error = true;
			}catch(SmbAuthException ex){
               ex.printStackTrace();
               addErrorMessage("Incorrect domain, username and/or password, or Access is denied","Incorrect domain, username and/or password, or Access is denied");
//               addCompErrorMessage("searchForm:hiddenPercent","Incorrect domain, username and/or password, or Access is denied","Incorrect domain, username and/or password, or Access is denied");
               error = true;
			}catch (SmbException e) {
				// TODO Auto-generated catch block
				addErrorMessage("Server or Sharename not found","Server or Sharename not found");
//				addCompErrorMessage("searchForm:hiddenPercent","Server or Sharename not found","Server or Sharename not found");
				error = true;
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();				
			}			 
			 if(!error){
				 testUNCResponse = "success";
			 }else{
				 testUNCResponse = "failure";
			 }
			 
		 }
		 
		 return testUNCResponse;
	 }

	public String getJobMessage(String filePath) {

		String msg = SmbWriter.getJobStatusMessage(filePath);
		if (msg == null || msg.trim().equals("")) {
			return null;
		}
		System.out.println("Adding Message to View...****" + msg);
		if (msg.indexOf("File restored successfully") != -1) {
			addInfoMessage(msg, msg);
			selectedNode.setCompressedURL(msg.substring(msg.indexOf("\\")));
		} else {
			addErrorMessage(msg, msg);
		}
		this.message = msg;
		enabled = false;
		return msg;
	}

	private boolean buttonRendered = true;

	private boolean enabled = false;

	private String message = null;

	private boolean showProgress;

	private boolean completed = false;

	private String restoreLocation;
	
	private boolean showThumbnail = false;
	
	private String currentThumbnailPath;

	public String getNodeID() {
		return nodeID;
	}

	public void setNodeID(String nodeID) {
		this.nodeID = nodeID;
	}

	public CustomTreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(CustomTreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public boolean isAuthorize() {
		return authorize;
	}

	public void setAuthorize(boolean authorize) {
		this.authorize = authorize;
	}

	public boolean isButtonRendered() {
		return buttonRendered;
	}

	public void setButtonRendered(boolean buttonRendered) {
		this.buttonRendered = buttonRendered;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isShowProgress() {
		return showProgress;
	}

	public void setShowProgress(boolean showProgress) {
		this.showProgress = showProgress;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	public String getRestoreLocation() {
		return restoreLocation;
	}

	public void setRestoreLocation(String restoreLocation) {
		this.restoreLocation = restoreLocation;
	}

	public String getRestorePath() {
		return restorePath;
	}

	public void setRestorePath(String restorePath) {
		this.restorePath = restorePath;
	}


	public List<CustomTreeNode> getNextVersionsList() {
		return nextVersionsList;
	}


	public void setNextVersionsList(List<CustomTreeNode> nextVersionsList) {
		this.nextVersionsList = nextVersionsList;
	}
	
	public int getVersionsListSize() {
		return (this.nextVersionsList!=null)?this.nextVersionsList.size():0;
	}


	public boolean isShowThumbnail() {
		return showThumbnail;
	}


	public void setShowThumbnail(boolean showThumbnail) {
		this.showThumbnail = showThumbnail;
	}


	public String getCurrentThumbnailPath() {
		return currentThumbnailPath;
	}


	public void setCurrentThumbnailPath(String currentThumbnailPath) {
		this.currentThumbnailPath = currentThumbnailPath;
	}


	public boolean isAlreadyExists() {
		return alreadyExists;
	}


	public void setAlreadyExists(boolean alreadyExists) {
		this.alreadyExists = alreadyExists;
	}
	

}
