package com.virtualcode.controller;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import jcifs.smb.SmbFile;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.PieChartModel;

import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.CustomChartModel;
import com.virtualcode.vo.DriveLetters;
import com.virtualcode.vo.Job;
import com.virtualcode.vo.JobSpDocLib;
import com.virtualcode.vo.ShareAnalysisStats;

@ManagedBean(name="jobAnalysisController")
@ViewScoped
public class JobAnalysisController extends AbstractController implements Serializable{
	
	
	// for bar chart...
	private Integer jobId;
	
	private List<CustomChartModel> categoryVolumeModelList=new ArrayList<CustomChartModel>();  
	
	//last modified
	private List<CustomChartModel> lmAgeModelList=new ArrayList<CustomChartModel>();
	
	//last access
	private List<CustomChartModel> laAgeModelList=new ArrayList<CustomChartModel>();
	
	//created date
	private List<CustomChartModel> cdAgeModelList=new ArrayList<CustomChartModel>();
	
	private boolean showLMGraph=true;
	
	private boolean showLAGraph=false;
	
	private boolean showCDGraph=false;
	
	
	public List<CustomChartModel> getCategoryVolumeModelList() {
		return categoryVolumeModelList;
	}

	public void setCategoryVolumeModelList(List<CustomChartModel> categoryVolumeModelList) {
		this.categoryVolumeModelList = categoryVolumeModelList;
	}

	public List<CustomChartModel> getLmAgeModelList() {
		return lmAgeModelList;
	}

	public void setLmAgeModelList(List<CustomChartModel> lmAgeModelList) {
		this.lmAgeModelList = lmAgeModelList;
	}
		
	public List<CustomChartModel> getLaAgeModelList() {
		return laAgeModelList;
	}

	public void setLaAgeModelList(List<CustomChartModel> laAgeModelList) {
		this.laAgeModelList = laAgeModelList;
	}

	public List<CustomChartModel> getCdAgeModelList() {
		return cdAgeModelList;
	}

	public void setCdAgeModelList(List<CustomChartModel> cdAgeModelList) {
		this.cdAgeModelList = cdAgeModelList;
	}

	public Integer getJobId() {
		return jobId;
	}

	public void setJobId(Integer jobId) {
		this.jobId = jobId;
	}
	
	
	public boolean isShowLMGraph() {
		return showLMGraph;
	}

	public void setShowLMGraph(boolean showLMGraph) {
		this.showLMGraph = showLMGraph;
	}

	public boolean isShowLAGraph() {
		return showLAGraph;
	}

	public void setShowLAGraph(boolean showLAGraph) {
		this.showLAGraph = showLAGraph;
	}

	public boolean isShowCDGraph() {
		return showCDGraph;
	}

	public void setShowCDGraph(boolean showCDGraph) {
		this.showCDGraph = showCDGraph;
	}
	
	

	public void init(){
		
		jobId=VCSUtil.getIntValue(getRequestParameter("id"));
		System.out.println("Job Id: "+jobId);
	
		categoryVolumeModelList.clear();
		lmAgeModelList.clear();
		laAgeModelList.clear();
		cdAgeModelList.clear();
		
		Job job=jobService.getJobById(jobId);
		Set<JobSpDocLib> libsSet=job.getJobSpDocLibs();
		CustomChartModel customChartModel;
		if(libsSet!=null){
			Iterator<JobSpDocLib> libsIte=libsSet.iterator();
			String sharePath;
			List<DriveLetters> driveLettersList;
			while(libsIte.hasNext()){
				sharePath=libsIte.next().getName();
				sharePath=VCSUtil.getDirPathFromSambaPath(sharePath);
				driveLettersList=driveLettersService.getDriveLettersBySharePath(sharePath);
				
				for(int i=0;driveLettersList!=null && i<driveLettersList.size();i++){
					
					customChartModel=loadLmAgeModel(driveLettersList.get(i));
					if(customChartModel!=null){
					   lmAgeModelList.add(customChartModel);
					}
					
					customChartModel=loadLaAgeModel(driveLettersList.get(i));
					if(customChartModel!=null){
					   laAgeModelList.add(customChartModel);
					}
					
					customChartModel=loadCDAgeModel(driveLettersList.get(i));
					if(customChartModel!=null){
					   cdAgeModelList.add(customChartModel);
					}
					
					customChartModel=loadShareCategoriesModel(driveLettersList.get(i));					
					if(customChartModel!=null){
					  categoryVolumeModelList.add(customChartModel);
					}
				}
			}
		}		
				
	}
		
    
    private CustomChartModel loadShareCategoriesModel(DriveLetters driveLetter){
    	
    	List<ShareAnalysisStats> docCatList=shareAnaylysisService.getByShare(driveLetter.getId());
		    	
    	if(docCatList==null || docCatList.isEmpty()){
			return null;
		}
    	CustomChartModel customChartModel=new CustomChartModel();	
    	customChartModel.setSharePath(driveLetter.getSharePath());
		try{
			ShareAnalysisStats shareAStats;
			
			float vol=0;
			ChartSeries chartSeries;
			chartSeries=new ChartSeries("Categories");
		    for(int i=0;i<docCatList.size();i++){		    	  
		    	shareAStats=docCatList.get(i);
		    	vol=shareAStats.getTotalVolume();
		    	vol=vol/1024;
		    	
		    	chartSeries.set(shareAStats.getDocumentCatName(), roundTwoDecimals(vol));  
		    	
		    }
		    customChartModel.addSeries(chartSeries);
		    
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return customChartModel;
	}
    
    
    private CustomChartModel  loadLmAgeModel(DriveLetters driveLetter){
		
    	List<ShareAnalysisStats> docCatList=shareAnaylysisService.getByShare(driveLetter.getId());
		if(docCatList==null || docCatList.isEmpty()){
			return null;
		}		
		CustomChartModel customChartModel=new CustomChartModel();
		customChartModel.setSharePath(driveLetter.getSharePath());
		try{
			ShareAnalysisStats shareAStats;
			
		    float month_3_volume=0;
		    float month_6_volume=0;
		    
		    float month_12_volume=0;
		    float month_24_volume=0;
		    float month_36_volume=0;
		   
		    ChartSeries ageSeries;
		    
			for(int i=0;i<docCatList.size();i++){		    	  
		    	shareAStats=docCatList.get(i);	
		    	month_3_volume=shareAStats.getLm3();	
		    	month_6_volume=shareAStats.getLm6();	
		    	month_12_volume=shareAStats.getLm12();	
		    	month_24_volume=shareAStats.getLm24();
		    	month_36_volume=shareAStats.getLm36();	
		    	
		    	month_3_volume= (month_3_volume/1024);
				month_6_volume= (month_6_volume/1024);
				month_12_volume= (month_12_volume/1024);
				
				month_24_volume= (month_24_volume/1024);
				month_36_volume= (month_36_volume/1024);
				
				ageSeries=new ChartSeries(shareAStats.getDocumentCatName());
				ageSeries.set("0-6", roundTwoDecimals(month_3_volume));
				ageSeries.set("6-12", roundTwoDecimals(month_6_volume));
				ageSeries.set("12-24", roundTwoDecimals(month_12_volume));
				ageSeries.set("24-36", roundTwoDecimals(month_24_volume));
				ageSeries.set("> 36", roundTwoDecimals(month_36_volume));
					            
				customChartModel.addSeries(ageSeries);
		    }			

			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return customChartModel;
			
	}
    
    
    private CustomChartModel  loadLaAgeModel(DriveLetters driveLetter){
		
    	List<ShareAnalysisStats> docCatList=shareAnaylysisService.getByShare(driveLetter.getId());
		if(docCatList==null || docCatList.isEmpty()){
			return null;
		}		
		CustomChartModel customChartModel=new CustomChartModel();
		customChartModel.setSharePath(driveLetter.getSharePath());
		try{
			ShareAnalysisStats shareAStats;
			
		    float month_3_volume=0;
		    float month_6_volume=0;
		    
		    float month_12_volume=0;
		    float month_24_volume=0;
		    float month_36_volume=0;
		   
		    ChartSeries ageSeries;
		    
			for(int i=0;i<docCatList.size();i++){		    	  
		    	shareAStats=docCatList.get(i);	
		    	month_3_volume=shareAStats.getLa3();	
		    	month_6_volume=shareAStats.getLa6();	
		    	month_12_volume=shareAStats.getLa12();	
		    	month_24_volume=shareAStats.getLa24();
		    	month_36_volume=shareAStats.getLa36();	
		    	
		    	month_3_volume= (month_3_volume/1024);
				month_6_volume= (month_6_volume/1024);
				month_12_volume= (month_12_volume/1024);
				
				month_24_volume= (month_24_volume/1024);
				month_36_volume= (month_36_volume/1024);
				
				ageSeries=new ChartSeries(shareAStats.getDocumentCatName());
				ageSeries.set("0-6", roundTwoDecimals(month_3_volume));
				ageSeries.set("6-12", roundTwoDecimals(month_6_volume));
				ageSeries.set("12-24", roundTwoDecimals(month_12_volume));
				ageSeries.set("24-36", roundTwoDecimals(month_24_volume));
				ageSeries.set("> 36", roundTwoDecimals(month_36_volume));
					            
				customChartModel.addSeries(ageSeries);
		    }			

			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return customChartModel;
			
	}
    
    
    private CustomChartModel  loadCDAgeModel(DriveLetters driveLetter){
		
    	List<ShareAnalysisStats> docCatList=shareAnaylysisService.getByShare(driveLetter.getId());
		if(docCatList==null || docCatList.isEmpty()){
			return null;
		}		
		CustomChartModel customChartModel=new CustomChartModel();
		customChartModel.setSharePath(driveLetter.getSharePath());
		try{
			ShareAnalysisStats shareAStats;
			
		    float month_3_volume=0;
		    float month_6_volume=0;
		    
		    float month_12_volume=0;
		    float month_24_volume=0;
		    float month_36_volume=0;
		   
		    ChartSeries ageSeries;
		    
			for(int i=0;i<docCatList.size();i++){		    	  
		    	shareAStats=docCatList.get(i);	
		    	month_3_volume=shareAStats.getCd3();	
		    	month_6_volume=shareAStats.getCd6();	
		    	month_12_volume=shareAStats.getCd12();	
		    	month_24_volume=shareAStats.getCd24();
		    	month_36_volume=shareAStats.getCd36();	
		    	
		    	month_3_volume= (month_3_volume/1024);
				month_6_volume= (month_6_volume/1024);
				month_12_volume= (month_12_volume/1024);
				
				month_24_volume= (month_24_volume/1024);
				month_36_volume= (month_36_volume/1024);
				
				ageSeries=new ChartSeries(shareAStats.getDocumentCatName());
				ageSeries.set("0-6", roundTwoDecimals(month_3_volume));
				ageSeries.set("6-12", roundTwoDecimals(month_6_volume));
				ageSeries.set("12-24", roundTwoDecimals(month_12_volume));
				ageSeries.set("24-36", roundTwoDecimals(month_24_volume));
				ageSeries.set("> 36", roundTwoDecimals(month_36_volume));
					            
				customChartModel.addSeries(ageSeries);
		    }			

			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return customChartModel;
			
	}
    
    
   	
	float roundTwoDecimals(float d) {
        DecimalFormat twoDForm = new DecimalFormat("#.##");
    return Float.valueOf(twoDForm.format(d));
   }

}
