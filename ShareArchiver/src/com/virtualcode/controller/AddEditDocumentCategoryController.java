package com.virtualcode.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;

import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.DocumentCategory;
import com.virtualcode.vo.DocumentType;

@ManagedBean(name="addEditDocCatController")
@ViewScoped
public class AddEditDocumentCategoryController extends AbstractController implements Serializable {
	
	public AddEditDocumentCategoryController(){
		super();
		System.out.println("AddEditDocumentCategoryController()");
		documentTypeList=new ArrayList<DocumentType>();
		documentCategory=new DocumentCategory();
		documentType=new DocumentType();
		
	}
	
	
	private Integer documentTypeId;
	
	private Integer documentCategoryId;
	
    private DocumentCategory documentCategory;
	
	private DocumentType documentType;
	
	private List<DocumentType> documentTypeList;
	
	private List<Integer> removedDocTypesList=Collections.synchronizedList(new ArrayList<Integer>());
	
	public void addDocumentTypeToList(){
		
		if(documentTypeExists()){
			addInfoMessage("Document Type "+documentType.getName()+" already added!", null);
			return;
		}
		
    	DocumentType newDocType=new DocumentType();
    	newDocType.setName(documentType.getName());
    	newDocType.setValue(documentType.getValue());
		documentTypeList.add(newDocType);
				
		documentType.setName(null);//reset binded values
		documentType.setValue(null);
	}
	
	public void removeDocumentTypeFromList(){
		
		boolean error=false;
		for(int i=0;i<documentTypeList.size();i++){
			if(documentTypeList.get(i).getSelected().booleanValue()){
				if(! documentClassifyService.isDocTypeInUse(documentTypeList.get(i).getId())){			         
					removedDocTypesList.add(documentTypeList.remove(i).getId());			       
				}else{
					error=true;
					break;					
				}				
			}
		}
		if(error){
			addErrorMessage("Can't delete document type,Its in use.", "Can't delete document type,Its in use.");
		}
	}
	
   public void removeAllDocumentTypeFromList(){
		for(int i=0;i<documentTypeList.size();i++){
//			if(documentTypeList.get(i).getSelected().booleanValue()){
				if(! documentClassifyService.isDocTypeInUse(documentTypeList.get(i).getId())){			         
					removedDocTypesList.add(documentTypeList.get(i).getId());
				}else{
//					error=true;
//					break;					
				}				
//			}
		}
		documentTypeList.clear();    	
   }
   
   public String saveDocumentCategory(){		
	   String navigateTo="DocumentCategories.faces?redirect=true";
	   System.out.println("--------------------------saving document category: "+documentCategory.getName());
	   if(documentCategory.getName()==null || documentCategory.getName().trim().equals("")){
		   addErrorMessage("category name cannot be empty", "category name cannot be empty");
		   return null;
	   }
	   try{
		   for(int i=0;documentTypeList!=null && i<documentTypeList.size();i++){
			   documentTypeList.get(i).setDocumentCategory(documentCategory);
		   }
		   for(int i=0;i<removedDocTypesList.size();i++){
			   documentClassifyService.removeDocumentType(removedDocTypesList.get(i));
		   }
		   removedDocTypesList.clear();
		   
		   documentCategory.setDocumentTypes(fromListToSet(documentTypeList));
		   documentClassifyService.saveDocumentCategory(documentCategory);	   
	   }catch( org.springframework.dao.DataIntegrityViolationException ex){	//only if the category is associated with an existing policy
		   addErrorMessage("Type cannot be updated, Please make sure that this type is not associated with a  policy", "Category edit failed");
		   return null;
	   }
	   
	   	   
	   return navigateTo;
   }
   
   public <T> List<T> fromSetToList(Set<T> set) {
	    List<T> list = new ArrayList<T>();
	    for(T o : set) {
	        list.add(o);
	    }
	        return list;
	}
   
   public <T> Set<T> fromListToSet(List<T> list) {
	    Set<T> set = new HashSet<T>();
	    for(T o : list) {
	        set.add(o);
	    }
	        return set;
	}
   
   public String saveDocumentType(){		
	   String navigateTo="DocumentCategories.faces?redirect=true";
	   documentType.setDocumentCategory(documentCategory);	   
	   if(documentType.getId()!=null && documentType.getId().intValue()>0){
		   documentClassifyService.saveDocumentType(documentType);
	   }else{
	      documentClassifyService.saveDocumentType(documentType);
	   }
	   	   
	   return navigateTo;
   }
   
   public List<DocumentCategory> getAllDocumentCategories(){
		return documentClassifyService.getAllDocumentCategories();
		
	}
   
   public List<DocumentType> getAllDocumentTypes(){
		return documentClassifyService.getAllDocumentTypes();
		
	}
	
	private boolean documentTypeExists(){
		boolean exists=false;
		for(int i=0;i<documentTypeList.size();i++){
			if( (documentType.getName().equalsIgnoreCase(documentTypeList.get(i).getName() ) ) ||
					( documentType.getValue().equalsIgnoreCase(documentTypeList.get(i).getValue()) )){
				exists=true;
				break;
			}
		}
		return exists;
	}
   //------------------------ General Getter/Setter---------------------------------------  
    
   public void populateDocumentCategory(ComponentSystemEvent  event){
		if(documentCategoryId!=null && documentCategoryId.intValue()>0  && ! FacesContext.getCurrentInstance().isPostback()){
			this.documentCategory=documentClassifyService.getDocumentCategory(documentCategoryId);
			documentTypeList= fromSetToList(documentCategory.getDocumentTypes());
		}
	}
	
    public void populateDocumentType(ComponentSystemEvent  event){		
    	if(documentTypeId!=null && documentTypeId.intValue()>0 && ! FacesContext.getCurrentInstance().isPostback()){
			this.documentType=documentClassifyService.getDocumentType(documentTypeId);			
			this.documentCategory=documentClassifyService.getDocumentCategory(documentType.getDocumentCategory().getId());
    	}
				
	}
	
	
	public DocumentCategory getDocumentCategory() {
		return documentCategory;
	}

	public void setDocumentCategory(DocumentCategory documentCategory) {
		this.documentCategory = documentCategory;
	}

	public DocumentType getDocumentType() {
		return documentType;
	}

	public void setDocumentType(DocumentType documentType) {
		this.documentType = documentType;
	}

	public List<DocumentType> getDocumentTypeList() {
		return documentTypeList;
	}

	public void setDocumentTypeList(List<DocumentType> documentTypeList) {
		this.documentTypeList = documentTypeList;
	}

	public Integer getDocumentTypeId() {
		return documentTypeId;
	}

	public void setDocumentTypeId(Integer documentTypeId) {
		this.documentTypeId = documentTypeId;
	}

	public Integer getDocumentCategoryId() {
		return documentCategoryId;
	}

	public void setDocumentCategoryId(Integer documentCategoryId) {
		this.documentCategoryId = documentCategoryId;
	}
	
	
    
        	

}
