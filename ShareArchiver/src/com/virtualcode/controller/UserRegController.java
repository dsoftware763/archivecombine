package com.virtualcode.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.shiro.crypto.hash.Sha256Hash;

import com.virtualcode.common.VCSConstants;
import com.virtualcode.util.PasswordGenerator;
import com.virtualcode.vo.Role;
import com.virtualcode.vo.User;

@ManagedBean(name="userRegController")
@ViewScoped
public class UserRegController extends AbstractController {
	
	public UserRegController(){
		   super();
		   user=new User();
		   userRoles=new ArrayList<String>();
	}
	

	public List<User> getAllUsers(){
		List<User> list= userService.getAll();
		
		User user;
		Iterator<Role> ite=null;
		Role role;
		String roleStr="";
		for (int i=0;list!=null && i<list.size(); i++){
		      
			user=list.get(i);
			roleStr="";
		      if(user.getUserRoles()!=null)ite=user.getUserRoles().iterator();
		       while(ite!=null  && ite.hasNext()){
		    	    role=ite.next();
		    	   roleStr+=role.getName()+", ";
		    	
		       }
		       if(roleStr.length()>2)
		       user.setUserRolesStr(roleStr.substring(0,roleStr.length()-2));
		}
		
		return list;
	}
	
		
	public void populateUserInfo(){
		Iterator<Role> ite=null;
		Role role;
		if(userId!=null && userId.intValue()>0 && ! FacesContext.getCurrentInstance().isPostback()){
		   user=userService.getById(userId);
		   if(user==null){
			   return;
		   }
		   	
		   if(user!=null && user.getUserRoles()!=null){
		       ite=user.getUserRoles().iterator();
		       userRoles.clear();
		       while(ite!=null && ite.hasNext()){
		    	    role=ite.next();
		    	    userRoles.add(role.getName());
		    	    user.setRole(role);
		    	    System.out.println("Adding user role -->"+role.getName());
		    	    
		       }
		   }
		}
		
	}
	
	public String save(){
		  Long selectedRoleId=user.getRole().getId().longValue();
		  if(!(user.getPasswordHash().equals(user.getConfirmPassword()))){
			  System.out.println("password and confirm password do not match");
			  addErrorMessage("Password and confirm password do not match", "Password and confirm password do not match");			 
			  return null;
		  }else if(userService.getByUsername(user.getUsername())!=null){
			  System.out.println("User already exists");
			  FacesContext context = FacesContext.getCurrentInstance();
			  FacesMessage message = new FacesMessage("User name must be unique");
			  context.addMessage(null, message);			 
			  return null;
		  }
		  String pattern = PasswordGenerator.generateRegularExpression();
		  if(!user.getPasswordHash().matches(pattern)){
			  List<String> errorMessages = PasswordGenerator.generateErrorMessages();
			  if(errorMessages!=null && errorMessages.size()>0){
				  for(String errorMessage: errorMessages){
					  addErrorMessage(errorMessage, errorMessage);
				  }
			  }
			  
			  System.out.println("Password format incorrect");
//			  addErrorMessage("Password and confirm password do not match", "Password and confirm password do not match");			 
			  return null;
		  }
		
		for(int i=0;allRoles!=null&& i<allRoles.size();i++){
			if(allRoles.get(i).getId().longValue()==selectedRoleId){
				user.setRole(allRoles.get(i));
			}
		}
      
	    Role role=user.getRole();	
	    //role.getUsers().add(user);
		user.getUserRoles().add(role);
		user.setPasswordHash(new Sha256Hash(user.getPasswordHash()).toHex());
		user.setLastPasswordChange(new Timestamp(System.currentTimeMillis()));
	    userService.save(user);
		return "Users.faces?faces-redirect=true";
	}
	
	public String update(){
		  Long selectedRoleId=user.getRole().getId().longValue();
		  		
		for(int i=0;allRoles!=null&& i<allRoles.size();i++){
			if(allRoles.get(i).getId().longValue()==selectedRoleId){
				user.setRole(allRoles.get(i));
				break;
			}
		}
    
	    Role role=user.getRole();	
	    Set<Role> rolesSet=new HashSet<Role>();
	    rolesSet.add(role);
	    
	    user.setUserRoles(rolesSet);	
	   	
	   
	    
	    for(int i=0;allRoles!=null&& i<allRoles.size();i++){
			if(allRoles.get(i).getId().longValue()==selectedRoleId){
				user.setRole(allRoles.get(i));
			}
		}
      	   
		user.getUserRoles().add(role);		
	    userService.save(user);
		return "Users.faces?faces-redirect=true";
	    
	    
	}
	
	public String delete(){
		String loggedInUser = (String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
		System.out.println("user logged in : " + loggedInUser);
		if(user.getUsername().trim().equals(loggedInUser)){
			addErrorMessage("User ["+ user.getUsername() +"] logged in, Cannot delete", "user logged in");
			return null;
		}else{
			System.out.println("user not logged in : " + user.getUsername());
			userService.delete(user);
		}
		//userService.delete(user);
		return "Users.faces?faces-redirect=true";
	}
	
	public String savePassword(){
		String username = (String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
		System.out.println("----------change password for user: " + username);
		int errorCount = 0;
		if(getOldPassword().trim().equals("")){
			addErrorMessage("old password cannot be empty", "old password cannot be empty");
			errorCount++;
		}
		if(user.getPasswordHash().trim().equals("")){
			addErrorMessage("password cannot be empty", "password cannot be empty");
			errorCount++;
		}
		if(user.getConfirmPassword().trim().equals("")){
			addErrorMessage("confirm password cannot be empty", "confirm password cannot be empty");
			errorCount++;
		}
		
		  String pattern = PasswordGenerator.generateRegularExpression();
		  if(!user.getPasswordHash().matches(pattern)){
			  List<String> errorMessages = PasswordGenerator.generateErrorMessages();
			  if(errorMessages!=null && errorMessages.size()>0){
				  for(String errorMessage: errorMessages){
					  addErrorMessage(errorMessage, errorMessage);
				  }
			  }
			  
			  System.out.println("Password format incorrect");
//			  addErrorMessage("Password and confirm password do not match", "Password and confirm password do not match");			 
//			  return null;
			  errorCount++;
		  }
		if(errorCount>0){
			return null;
		}else if(!user.getPasswordHash().equals(user.getConfirmPassword())){
			addErrorMessage("password and confirm password do not match", "password and confirm password do not match");
			return null;
		}else{
			List<User> userList = userService.getByUsername(username);
			
			if(userList !=null || userList.size()>0){
				String oldPasswordHash = new Sha256Hash(getOldPassword()).toHex();
				User currentUser = userList.get(0);
				if(!oldPasswordHash.equals(currentUser.getPasswordHash())){
					addErrorMessage("old password is incorrect", "old password is incorrect");
					return null;
				}
				currentUser.setPasswordHash(new Sha256Hash(user.getPasswordHash()).toHex());
				currentUser.setLastPasswordChange(new Timestamp(System.currentTimeMillis()));
				userService.save(currentUser);				
			}
			
		}
		String roleName=(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_ROLE_NAME);
		if(roleName.equals(VCSConstants.ROLE_ADMIN)){
			return "Status.faces?faces-redirect=true";
		}else{
			return "Browse.faces?faces-redirect=true";
		}
		
		
	}
	
	
	public void resetPassword(){
		
		if(user.getResetPassword().trim().equals("")){
			addErrorMessage("password cannot be empty", "password cannot be empty");
			return;
			
		}
		if(user.getConfirmResetPassword().trim().equals("")){
			addErrorMessage("confirm password cannot be empty", "confirm password cannot be empty");
			return;
		}
		
		if(!user.getResetPassword().equals(user.getConfirmResetPassword())){
			addErrorMessage("password and confirm password do not match", "password and confirm password do not match");
			return;
		}
		
		  String pattern = PasswordGenerator.generateRegularExpression();
		  if(!user.getResetPassword().matches(pattern)){
			  List<String> errorMessages = PasswordGenerator.generateErrorMessages();
			  if(errorMessages!=null && errorMessages.size()>0){
				  for(String errorMessage: errorMessages){
					  addErrorMessage(errorMessage, errorMessage);
				  }
			  }
			  
			  System.out.println("Password format incorrect");
//			  addErrorMessage("Password and confirm password do not match", "Password and confirm password do not match");			 
			  return;
		  }
		
		  User dbUser=userService.getById(user.getId());		  
		  dbUser.setPasswordHash(new Sha256Hash(user.getResetPassword()).toHex());
		  dbUser.setLastPasswordChange(new Timestamp(System.currentTimeMillis()));
		  try{
		  userService.save(dbUser);		  
		  addInfoMessage("Password reset successfully", "Password reset successfully");
		  this.user=dbUser;
		  this.userId=user.getId();
		  }catch(Exception ex){
			  ex.printStackTrace();
			  addErrorMessage("Password can't be reset", "Password can't be reset");
		  }
		 
		
	}
	
  public String resetPassword2(){
		
		if(user.getResetPassword()==null || user.getResetPassword().trim().equals("")){
			addErrorMessage("password cannot be empty", "password cannot be empty");
			return "Users.faces";
			
		}
		if(user.getConfirmResetPassword()==null || user.getConfirmResetPassword().trim().equals("")){
			addErrorMessage("confirm password cannot be empty", "confirm password cannot be empty");
			return "Users.faces";
		}
		
		if(!user.getResetPassword().equals(user.getConfirmResetPassword())){
			addErrorMessage("password and confirm password do not match", "password and confirm password do not match");
			return "Users.faces";
		}
		
		  User dbUser=userService.getById(user.getId());		  
		  dbUser.setPasswordHash(new Sha256Hash(user.getResetPassword()).toHex());
		  try{
		  userService.save(dbUser);		  
		  addInfoMessage("Password reset successfully", "Password reset successfully");
		  this.user=dbUser;
		  this.userId=user.getId();
		  }catch(Exception ex){
			  ex.printStackTrace();
			  addErrorMessage("Password can't be reset", "Password can't be reset");
		  }
		 
		  return "Users.faces";
		
	}
	
	

	private Long userId;
	
	private User user;
	
	private List<Role> allRoles;
	
	private List<String> userRoles;
	
	private String oldPassword;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}


	public List<String> getUserRoles() {
		return userRoles;
	}


	public void setUserRoles(List<String> userRoles) {
		this.userRoles = userRoles;
	}

	public List<Role> getAllRoles(){
		allRoles= roleService.getAll();		
		return allRoles;
	}
	
	public void setAllRoles(List<Role> allRoles) {
		this.allRoles = allRoles;
	}


	public String getOldPassword() {
		return oldPassword;
	}


	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
			

}
