package com.virtualcode.controller;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.virtualcode.services.RoleFeatureService;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.RoleFeatureUtil;
import com.virtualcode.vo.RoleFeature;
import com.virtualcode.vo.SystemCode;
import com.virtualcode.vo.SystemCodeType;

@ManagedBean(name="roleFeatureController")
@ViewScoped
public class RoleFeatureController extends AbstractController {

	public RoleFeatureController(){
		
	}
	
	private List<RoleFeature> roleFeaturesList;
	
	private boolean enableFExplorerCM = false;
	public void init(){
		
		if(! FacesContext.getCurrentInstance().isPostback()){
			
			roleFeaturesList=roleFeatureService.getAll();
		}
		
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		if(ru.getCodeValue("enable_file_explorer_for_admin_cm", SystemCodeType.GENERAL)!=null)
			enableFExplorerCM = ru.getCodeValue("enable_file_explorer_for_admin_cm", SystemCodeType.GENERAL).equalsIgnoreCase("yes")?true:false;
		
	}
	
	public void save(){
		RoleFeature roleFeature;
		try{
			for(int i=0;roleFeaturesList!=null && i<roleFeaturesList.size();i++){
				roleFeature=roleFeaturesList.get(i);
				roleFeatureService.save(roleFeature);
			}
			addInfoMessage("User Options updated successfully!", "User Options updated successfully!");
		}catch(Exception ex){
			ex.printStackTrace();
			addInfoMessage("Error in updating user options!", "Error in updating user options!");
		}
		
		RoleFeatureUtil.forceValueLoad();//reload in memory values.
		SystemCode systemCode = systemCodeService.getSystemCodeByName("enable_file_explorer_for_admin_cm");
		if(systemCode!=null){
			systemCode.setCodevalue(enableFExplorerCM?"yes":"no");
			systemCodeService.saveSystemCode(systemCode);
		}
		ResourcesUtil.forceValueLoad();
		
	}
	

	public List<RoleFeature> getRoleFeaturesList() {
		return roleFeaturesList;
	}

	public void setRoleFeaturesList(List<RoleFeature> roleFeaturesList) {
		this.roleFeaturesList = roleFeaturesList;
	}

	public boolean isEnableFExplorerCM() {
		return enableFExplorerCM;
	}

	public void setEnableFExplorerCM(boolean enableFExplorerCM) {
		this.enableFExplorerCM = enableFExplorerCM;
	}
	
	
	
	
	
	
}
