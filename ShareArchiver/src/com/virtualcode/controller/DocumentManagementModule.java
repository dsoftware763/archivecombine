/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.controller;

//import com.virtualcode.agent.das.Executors.StartAgentJobs;
//import com.virtualcode.RepositoryOpManager;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.cifs.Acl;
import com.virtualcode.cifs.CifsUtil;
import com.virtualcode.repository.UploadDocumentService;
import com.virtualcode.util.StringUtils;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import javax.activation.DataHandler;
//import javax.jcr.Repository;
//import javax.servlet.ServletContext;
//import org.apache.jackrabbit.j2ee.RepositoryAccessServlet;
import org.apache.log4j.Logger;

/**
 *
 * @author YAwar
 */
public class DocumentManagementModule {
    static final String SECURE_INFO = "secureInfo";
    static final Logger log = Logger.getLogger(DocumentManagementModule.class);    
   // RepositoryOpManager opManager;
    
    public String uploadDocument(String fileContextPath,
            FileTaskInterface task,
            String layersList,
            Acl acl, String transID) {
        
       //synchronized (this.getClass()){
            log.info(transID + " uploadDocument: Started");
            log.debug(transID + " Base-64 encoded Path: " + fileContextPath);
            log.debug(transID + " File Path(b64): " + fileContextPath);
            log.debug(transID + " LayersList in Request : "+layersList);

//            ServletContext sc = StartAgentJobs.getContext();
//            Repository repository = RepositoryAccessServlet.getRepository(sc);

            //InputStream in = null;
            //DataHandler sdh = null;
            String url = null;
            //String dataHandlerClassName = null;

            try {
              //  opManager = RepositoryOpManager.getOpManager(repository);

                log.debug(transID + " Encoded File Path: " + fileContextPath);
                System.out.println("Encoded File Path: " + fileContextPath);
                fileContextPath = new String(org.apache.commons.codec.binary.Base64.decodeBase64(fileContextPath.getBytes()), "UTF-8");
                log.debug(transID + " File Path: " + fileContextPath);
                System.out.println("File Path: " + fileContextPath);

                String[] pathElements = StringUtils.getPathElements(fileContextPath);
                if (pathElements == null || pathElements.length < 1) {
                    throw new Exception("Context Path of the file could not be parsed");
                }            

                HashMap layerAttribs = StringUtils.getLayerAttribs(layersList, SECURE_INFO);
                HashMap aclAttribs   =   CifsUtil.getAclAttribs(acl);

                //dataHandlerClassName = content.getClass().getName();
                //log.debug("DataHandler Class Name: " + dataHandlerClassName);
                //System.out.println("DataHandler Class Name: " + dataHandlerClassName);
                
                //sdh = content;
                //in = sdh.getInputStream();
                System.out.println("Uploading document 2");
                //url = opManager.uploadDocument(pathElements, in, layerAttribs, aclAttribs);
                //uploadDocumentService = new com.virtualcode.repository.impl.UploadDocumentServiceImpl();
                uploadDocumentService = (UploadDocumentService)com.virtualcode.util.SpringApplicationContext.getBean("uploadDocumentService");
                System.out.println("value: " + uploadDocumentService);
                url = uploadDocumentService.uploadDocumentByBuiltInAgent(pathElements, task, layerAttribs, aclAttribs, transID);

            } catch (Exception ex) {
                log.error(transID + " Exception in WebService Method EX: "+ex);
                System.out.println("Exception in WebService Method EX: "+ex);
                ex.printStackTrace();
                //throw new WebServiceException(ex);
            } finally {
                /* try {
                    if (in != null) {
                        in.close();
                    }
                } catch (IOException ioe) {
                    log.error("Input Stream for the file was not created. Ex: " + ioe.getMessage());
                    System.out.println("Input Stream for the file was not created. Ex: " + ioe.getMessage());
                }*/
                
            }
            return url;
        //}//ending Sync Block
    }
    
    private UploadDocumentService uploadDocumentService;

	public void setUploadDocumentService(UploadDocumentService uploadDocumentService) {
		this.uploadDocumentService = uploadDocumentService;
	}
}
