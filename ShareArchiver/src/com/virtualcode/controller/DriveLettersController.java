package com.virtualcode.controller;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import jcifs.smb.SmbAuthException;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;

import org.primefaces.model.chart.PieChartModel;

import com.virtualcode.common.ShareSizeRunner;
import com.virtualcode.common.VCSConstants;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.util.VirtualcodeUtil;
import com.virtualcode.vo.Agent;
import com.virtualcode.vo.CustomTreeNode;
import com.virtualcode.vo.DriveLetters;
import com.virtualcode.vo.Job;
import com.virtualcode.vo.JobSpDocLib;
import com.virtualcode.vo.Policy;
import com.virtualcode.vo.SystemCode;
import com.virtualcode.vo.SystemCodeType;

/**
 * @author se8
 *
 */
@ManagedBean(name = "driveLetterController")
@SessionScoped
public class DriveLettersController extends AbstractController implements	Serializable {

	public DriveLettersController() {		
		super();		
		driveLetter = new DriveLetters();
		VirtualcodeUtil util = VirtualcodeUtil.getInstance();
		if(util.getComplianceMode())
			enableMonitoring = false;		
	}

	private Integer driveLetterId;

	private Agent agent;

	private DriveLetters driveLetter;
	
	private List<DriveLetters> driveLettersList;

	private boolean analyze = false;

	// domain name settings for host file "resolv.conf" in linux
	private String domainName;

	private String ipAddress1;

	private String ipAddress2;
	
	
	private String[] letters = {"--Select--", "A:", "B:", "C:", "D:", "E:", "F:", "G:",
			"H:", "I:", "J:", "K:", "L:", "M:", "N:", "O:", "P:", "Q:", "R:",
			"S:", "T:", "U:", "V:", "W:", "X:", "Y:", "Z:" };

	private String[] shares;
	
	private boolean enableMonitoring = true;

	public Map<String, String> populateShares() {
		// List<String> list = fileExplorerService.getAllShareNames();
		Map<String, String> map = fileExplorerService.getAllShareNames();
		return map;
	}
	
	private long pollingStartTime=0;
	private boolean pollingEnabled=false;
		
		
	public boolean isPollingEnabled() {
					
		if(pollingStartTime==0){
			return true;
		}
		long laps=System.currentTimeMillis()-pollingStartTime;
		if(laps>180000){
			return false;
		}
		return true;		
	}


	public void setPollingEnabled(boolean pollingEnabled) {
		this.pollingEnabled = pollingEnabled;
	}


	public void init(){
	    if(!FacesContext.getCurrentInstance().isPostback()){
		    List<DriveLetters> tempList=driveLettersService.getAll();
	    	
		    if(tempList==null || tempList.isEmpty()){
		    	return;
		    }
		    
		    if(driveLettersList==null || driveLettersList.isEmpty() || driveLettersList.size()!=tempList.size() ){
		        driveLettersList=tempList;
		    }else{
		    	return;
		    }
			
		    for(int i=0;driveLettersList!=null && i<driveLettersList.size();i++){
		    	ShareSizeRunner shSizeRunner=new ShareSizeRunner();
				shSizeRunner.setDriveLetter(driveLettersList.get(i));
				Thread shareSizeThread=new Thread(shSizeRunner);
				shareSizeThread.start();	
				pollingStartTime=System.currentTimeMillis();
			}				
	    }
	}

	public List<DriveLetters> getAllDriveLetters() {
		List<DriveLetters> list = driveLettersService.getAll();
		
		
		
		DriveLetters driveLetter;
		String smbPath;
		int usedPer=0;
		int freePer=0;
		/*
		for(int i=0;C!=null && i<list.size();i++){
			driveLetter=list.get(i);
			smbPath=VCSUtil.getSambaPath(driveLetter.getRestoreDomain(), driveLetter.getRestoreUsername(), driveLetter.getRestorePassword(), driveLetter.getSharePath());
			freePer=getShareFreeSpace(smbPath);
			freePer=(freePer>0?freePer:0);
			usedPer=100-freePer;
			if(usedPer>=driveLetter.getCriticalLimit()){
				driveLetter.setWarningColour(2);//critical
			}			
			if(usedPer>=driveLetter.getWarningLimit() && usedPer<driveLetter.getCriticalLimit()){
				driveLetter.setWarningColour(1);//warning
			}			
			if(usedPer<driveLetter.getWarningLimit()){
				driveLetter.setWarningColour(0);//no-warning
			}		
			
			driveLetter.setShareUsedPercentage(usedPer);
			
		} */
		System.out.println("Total SystemCode Records found: " + list.size());
		return list;
	}
	
	public List<SystemCode> getAllLdapSettings(){
		List<SystemCode> ldapList = systemCodeService.getSystemCodeByCodetype(SystemCodeType.GENERAL);
		return ldapList;
	}
	
	public void refreshDriveLetterInfo() {
		if(!FacesContext.getCurrentInstance().isPostback()){
			driveLetter = new DriveLetters();
		}
	}

	public void populateDriveLetterInfo() {

		if (driveLetterId != null && driveLetterId.intValue() > 0
				&& !FacesContext.getCurrentInstance().isPostback()) {
			driveLetter = driveLettersService.getDriveLetterById(driveLetterId);
			// DriveLetters tempDriveLetter = new DriveLetters();
			// tempDriveLetter = driveLetter;
			// tempDriveLetter.setId(driveLetter.getId());
			// tempDriveLetter.setDriveLetter(driveLetter.getDriveLetter());
			// tempDriveLetter.setRestoreDomain(driveLetter.getRestoreDomain());
			// tempDriveLetter.setRestoreUsername(driveLetter.getRestoreUsername());
			// tempDriveLetter.setRestorePassword(driveLetter.getRestorePassword());
			// tempDriveLetter.setStatus(driveLetter.getStatus());
			// tempDriveLetter.setSharePath(driveLetter.getSharePath());
			ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
			String companyName = ru.getCodeValue("companyName",
					SystemCodeType.GENERAL);
			if (driveLetter.getSharePath().startsWith(companyName)) {
				driveLetter.setSharePath("\\\\"
						+ driveLetter.getSharePath()
								.substring(companyName.length() + 4)
								.replaceAll("/", "\\\\"));
			}
			String smbPath=VCSUtil.getSambaPath(driveLetter.getRestoreDomain(), driveLetter.getRestoreUsername(), driveLetter.getRestorePassword(), driveLetter.getSharePath());
			List<JobSpDocLib> jobsList=jobService.getShareAllThreshholdJobs(smbPath);
			if(jobsList!=null && jobsList.size()>0){
				driveLetter.setPolicyId(jobsList.get(0).getJob().getPolicy().getId());
//			  saveShareThreshholdJob();
			}			
			// setDriveLetter(tempDriveLetter);

		}
	}

	public String save() {

		if (!(driveLetter.getSharePath().startsWith("\\\\"))) {
			addErrorMessage("share path must be a valid UNC path",
					"Share path invalid");
			return null;
		}

		if (driveLetter.getRestoreDomain() != null
				&& driveLetter.getRestoreDomain().trim().length() > 0) {
			if (driveLetter.getRestoreUsername() == null
					|| driveLetter.getRestoreUsername().trim().length() < 1
					|| driveLetter.getRestorePassword() == null
					|| driveLetter.getRestorePassword().trim().length() < 1) {
				System.out.println("domain, username, or password is missing");
				addErrorMessage(
						"Please provide all three fields i.e, remote domain, username, or password",
						"domain, username, or password is missing");
				return null;
			}
		} else if (driveLetter.getRestoreUsername() != null
				&& driveLetter.getRestoreUsername().trim().length() > 0) {
			if (driveLetter.getRestoreDomain() == null
					|| driveLetter.getRestoreDomain().trim().length() < 1
					|| driveLetter.getRestorePassword() == null
					|| driveLetter.getRestorePassword().trim().length() < 1) {
				System.out.println("domain, username, or password is missing");
				addErrorMessage(
						"Please provide all three fields i.e, remote domain, username, or password",
						"domain, username, or password is missing");
				return null;
			}
		} else if (driveLetter.getRestorePassword() != null
				&& driveLetter.getRestorePassword().trim().length() > 0) {
			if (driveLetter.getRestoreDomain() == null
					|| driveLetter.getRestoreDomain().trim().length() < 1
					|| driveLetter.getRestoreUsername() == null
					|| driveLetter.getRestoreUsername().trim().length() < 1) {
				System.out.println("domain, username, or password is missing");
				addErrorMessage(
						"Please provide all three fields i.e, remote domain, username, or password",
						"domain, username, or password is missing");
				return null;
			}
		}

		List<DriveLetters> driveLetterList1 = driveLettersService
				.getDriveLettersByDriveLetter(this.driveLetter.getDriveLetter());
		boolean exists = false;
		DriveLetters tempDriveLetter;
		for (int i = 0; driveLetterList1 != null && i < driveLetterList1.size(); i++) {
			tempDriveLetter = driveLetterList1.get(i);
			if ((tempDriveLetter.getDriveLetter().trim().length()>0) && tempDriveLetter.getDriveLetter().equalsIgnoreCase(
					this.driveLetter.getDriveLetter())
					&& !(tempDriveLetter.getId().equals(this.driveLetter
							.getId()))) {
				exists = true;
			}
		}
		
		//add a slash '\' at the end
		if(!driveLetter.getSharePath().endsWith("\\"))
			driveLetter.setSharePath(driveLetter.getSharePath()+"\\");

		driveLetterList1 = driveLettersService
				.getDriveLettersBySharePath(driveLetter.getSharePath());
		for (int i = 0; driveLetterList1 != null && i < driveLetterList1.size(); i++) {
			tempDriveLetter = driveLetterList1.get(i);
			if (tempDriveLetter.getSharePath().equalsIgnoreCase(
					this.driveLetter.getSharePath())
					&& !(tempDriveLetter.getId().equals(this.driveLetter
							.getId()))) {
					if(!tempDriveLetter.getIsFileShare() && tempDriveLetter.getDriveLetter()==null){
						driveLetter.setId(tempDriveLetter.getId());		// if share path exists but it is hidden then use existing drive
					}else{
						exists = true;
					}
				
			}
		}
		if (exists) {
			System.out.println("Mapping already exists");
			addErrorMessage("Mapping already exists", "Mapping already exists");
			return null;
		}
		// to fix a problem with hibernate updating record
		DriveLetters driveLetter = null;
		if (this.driveLetter.getId() != null) {
			driveLetter = driveLettersService
					.getDriveLetterById(this.driveLetter.getId());
		} else {
			driveLetter = new DriveLetters();
		}
		String sharePath = this.driveLetter.getSharePath();
		if(!(this.driveLetter.getDriveLetter().trim().equals("--Select--")))
			driveLetter.setDriveLetter(this.driveLetter.getDriveLetter());
		else
			driveLetter.setDriveLetter(null);
//		if(!sharePath.endsWith("\\"))
//			sharePath = sharePath + "\\";	//remove last "/"
		
		driveLetter.setSharePath(sharePath);
		driveLetter.setRestoreDomain(this.driveLetter.getRestoreDomain());
		driveLetter.setRestoreUsername(this.driveLetter.getRestoreUsername());
		driveLetter.setRestorePassword(this.driveLetter.getRestorePassword());
		driveLetter.setWarningLimit(this.driveLetter.getWarningLimit());
		driveLetter.setCriticalLimit(this.driveLetter.getCriticalLimit());
		driveLetter.setAnalyzeShare(this.driveLetter.isAnalyzeShare());
		driveLetter.setWarningColour(this.driveLetter.getWarningColour());
		driveLetter.setShareUsedPercentage(this.driveLetter.getShareUsedPercentage());
		driveLetter.setShareFreePercentage(this.driveLetter.getShareFreePercentage());
		driveLetter.setTriggerJobOnThreshhold(this.driveLetter.isTriggerJobOnThreshhold());
		// }

		driveLetter.setStatus(true);
		driveLetter.setIsFileShare(true);
		driveLettersService.saveDriveLetter(driveLetter);
	
		for(int i=0;driveLettersList!=null && i<driveLettersList.size();i++){
			if(driveLettersList.get(i).getId().intValue()==driveLetter.getId().intValue()){
				driveLettersList.set(i, driveLetter);
				ShareSizeRunner shSizeRunner=new ShareSizeRunner();
				shSizeRunner.setDriveLetter(driveLettersList.get(i));
				Thread shareSizeThread=new Thread(shSizeRunner);
				shareSizeThread.start();	
				
			}
		}
		
		
		if (this.driveLetter.isAnalyzeShare() ){
			
			saveAnalysisJob();
		}
		
		if (this.driveLetter.isTriggerJobOnThreshhold()){
			
			
			  saveShareThreshholdJob();
			
		}
		
		//after save sync permissions with the source
		boolean success = false;
		CustomTreeNode share = fileExplorerService.createShareFolder(driveLetter.getSharePath());
		if(share!=null){
			success = fileExplorerService.syncPermissions(share.getPath(), false, false, false);
		}
		
		if(!success)
			addErrorMessage("share sync permissions failed", "share sync failed");
		return "DriveLetters.faces?faces-redirect=true";
	}

	public void saveAnalysisJob() {
		
		
		Job job = new Job();
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();

		Policy policy = null;
		List<Policy> policyList = policyService.getPolicyByName("DefaultPolicy");
		if (policyList != null && policyList.size() > 0) {
			policy = policyList.get(0);
		}

		if (policy == null) {
			addErrorMessage("'Default policy for jobs does not exist",
					"'Default policy' for jobs does not exist");
			return;
		}

		System.out.println(policy.getName());

		Date date = VCSUtil.getCurrentDateTime();
		Timestamp timestamp = new Timestamp(date.getTime());
		job.setExecTimeStart(timestamp);
		job.setPolicy(policy);
		// getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
//		job.setName("Drive-" + driveLetter.getDriveLetter() + "-analysis-job-"
//				+ System.currentTimeMillis());// for unique name every time

		job.setProcessCurrentPublishedVersion(true);
		job.setProcessLegacyPublishedVersion(true);
		job.setProcessLegacyDraftVersion(true);
		job.setProcessReadOnly(true);
		job.setProcessArchive(true);
		job.setProcessHidden(true);

//		job.setReadyToExecute(true);
		job.setActive(1);
		job.setExecutionInterval(1d);
		job.setActionType(VCSConstants.INDEXING);
		Set<JobSpDocLib> setJobSpDocLibs = new HashSet<JobSpDocLib>();
		// limit = 2l;
		long fileCount = 0l;
		String restoreDomain = driveLetter.getRestoreDomain();
		String restoreUsername = driveLetter.getRestoreUsername();
		String restorePassword = driveLetter.getRestorePassword();
		if (restoreDomain == null || restoreDomain.trim().length() < 1) {
			restoreDomain = ru.getCodeValue("restore.domain_name",
					SystemCodeType.GENERAL);
			restoreUsername = ru.getCodeValue("restore.username",
					SystemCodeType.GENERAL);
			restorePassword = ru.getCodeValue("restore.password",
					SystemCodeType.GENERAL);

		}

		String sharePath = driveLetter.getSharePath();
		if (sharePath != null && sharePath.trim().length() > 0) {
			sharePath = sharePath.replaceAll("\\\\", "/");
			sharePath = sharePath.substring(2); // remove the first two slashes
		}
		if(!sharePath.endsWith("/"))
			sharePath = sharePath + "/";	//add last "/"
		
		List libsList = jobService.getLibsByNameLike(sharePath.toLowerCase());
		JobSpDocLib lib = null;
		if(libsList!=null && libsList.size()>0){
			lib =  (JobSpDocLib)libsList.get(0);
			job = lib.getJob();
//			jobService.deleteJobDependencies(job);
		}
		job.setName("Drive-" + driveLetter.getDriveLetter() + "-analysis-job-"
				+ System.currentTimeMillis());// for unique name every time
		
		job.setReadyToExecute(true);
		
		job.setIsShareAnalysis(true);

		JobSpDocLib jobSpDocLib = new JobSpDocLib();
		if(lib!=null)
			jobSpDocLib.setId(lib.getId());

		String smbPath = "smb://" + restoreDomain + ";" + restoreUsername + ":"
				+ restorePassword + "@" + sharePath;

		System.out.println("-------" + smbPath + "-----");
		jobSpDocLib.setName(smbPath);
		jobSpDocLib.setGuid(null);
		jobSpDocLib.setType("FS");
		jobSpDocLib.setRecursive(true);

		jobSpDocLib.setJobSpDocLibExcludedPaths(null);
		setJobSpDocLibs.add(jobSpDocLib);
		jobSpDocLib.setJob(job);
		fileCount = fileCount + 1;

		List<Agent> agentList = agentService.getAgentByName(ru.getCodeValue("AgentName", SystemCodeType.ZUES_AGENT));
		if (agentList != null && agentList.size() > 0) {
			job.setAgent(agentList.get(0));
		}
		jobSpDocLib.setJobSpDocLibExcludedPaths(new HashSet());
		job.setJobSpDocLibs(new HashSet());
		job.getJobSpDocLibs().addAll(setJobSpDocLibs);
		try {
			jobService.saveJob(job);
			jobService.saveSpDocLib(jobSpDocLib);
		} catch (Exception ex) {
			ex.printStackTrace();
			addErrorMessage("internal error, could not create index job",
					"internal error");
		}
		addInfoMessage("Export job created successfully", "success");
	}

	
	
	public void saveShareThreshholdJob() {
		
		Policy policy = policyService.getPolicyById(this.driveLetter.getPolicyId());
		
		if (policy == null) {
			addErrorMessage("'Default policy' for jobs does not exist",
					"'Default policy' for jobs does not exist");
			return;
		}
		System.out.println(policy.getName());
		
		String jobName="Drive-" + driveLetter.getDriveLetter() + "-threshhold-job-"+ System.currentTimeMillis();
		Date date = VCSUtil.getCurrentDateTime();
		Timestamp timestamp = new Timestamp(date.getTime());
		
		Job job = new Job();
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		
		job.setExecTimeStart(timestamp);
		job.setPolicy(policy);
		// getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
//		job.setName(jobName);// for unique name every time

		job.setProcessCurrentPublishedVersion(true);
		job.setProcessLegacyPublishedVersion(true);
		job.setProcessLegacyDraftVersion(true);
		job.setProcessReadOnly(true);
		job.setProcessArchive(true);
		job.setProcessHidden(true);

//		job.setReadyToExecute(true);
		job.setActive(1);
		job.setExecutionInterval(1d);
		job.setActionType(VCSConstants.ARCHIVE_AND_STUB);
		Set<JobSpDocLib> setJobSpDocLibs = new HashSet<JobSpDocLib>();
		// limit = 2l;
		long fileCount = 0l;
		String restoreDomain = driveLetter.getRestoreDomain();
		String restoreUsername = driveLetter.getRestoreUsername();
		String restorePassword = driveLetter.getRestorePassword();
		if (restoreDomain == null || restoreDomain.trim().length() < 1) {
			restoreDomain = ru.getCodeValue("restore.domain_name",
					SystemCodeType.GENERAL);
			restoreUsername = ru.getCodeValue("restore.username",
					SystemCodeType.GENERAL);
			restorePassword = ru.getCodeValue("restore.password",
					SystemCodeType.GENERAL);

		}

		String sharePath = driveLetter.getSharePath();
		if (sharePath != null && sharePath.trim().length() > 0) {
			sharePath = sharePath.replaceAll("\\\\", "/");
			sharePath = sharePath.substring(2); // remove the first two slashes
		}
		if(!sharePath.endsWith("/"))
			sharePath = sharePath + "/";	//add last "/"
		
		String smbPath=VCSUtil.getSambaPath(driveLetter.getRestoreDomain(), driveLetter.getRestoreUsername(), driveLetter.getRestorePassword(), driveLetter.getSharePath());
		List<JobSpDocLib> jobsList=jobService.getShareAllThreshholdJobs(smbPath);
	
		if(jobsList!=null && jobsList.size()>0){
			JobSpDocLib lib =  (JobSpDocLib)jobsList.get(0);
			job = lib.getJob();
			job.setPolicy(policy);
			jobService.deleteJobDependencies(job);
		}
		job.setName(jobName);// for unique name every time
		job.setIsShareAnalysis(false);
		job.setThreshholdJob(true);
		job.setReadyToExecute(false);

		JobSpDocLib jobSpDocLib = new JobSpDocLib();

		System.out.println("-------" + smbPath + "-----");
		jobSpDocLib.setName(smbPath);
		jobSpDocLib.setGuid(null);
		jobSpDocLib.setType("FS");
		jobSpDocLib.setRecursive(true);

		jobSpDocLib.setJobSpDocLibExcludedPaths(null);
		setJobSpDocLibs.add(jobSpDocLib);
		jobSpDocLib.setJob(job);
		fileCount = fileCount + 1;

		List<Agent> agentList = agentService.getAgentByName(ru.getCodeValue(
				"AgentName", SystemCodeType.ZUES_AGENT));
		if (agentList != null && agentList.size() > 0) {
			job.setAgent(agentList.get(0));
		}
		jobSpDocLib.setJobSpDocLibExcludedPaths(new HashSet());
		job.setJobSpDocLibs(new HashSet());
		job.getJobSpDocLibs().addAll(setJobSpDocLibs);
		try {
			jobService.saveJob(job);
		} catch (Exception ex) {
			ex.printStackTrace();
			addErrorMessage("internal error, could not create index job",
					"internal error");
		}
		addInfoMessage("Threshhold job created successfully", "success");
	}

	public void testCredentials() {
		System.out.println("---------------------verifying UNC-------------------");
		String remoteUNC = driveLetter.getSharePath();
		if (!(driveLetter.getSharePath().startsWith("\\\\"))) {
			addErrorMessage("share path must be a valid UNC path",
					"Share path invalid");
			return;
		}
		if (remoteUNC.trim().equals("") || remoteUNC.trim().length() < 3) {// test as in existing app
			addErrorMessage("share path incorrect",
					"Invalid UNC Path");			
		} else if (remoteUNC.trim().indexOf("\\\\", 2) != -1) {
			addErrorMessage("share path format incorrect(Only two \\\\ allowed in the start)",
					"share format incorrect(Only two \\\\ allowed in the start)");
			return;
		} else if (driveLetter.getRestorePassword().trim().contains("@")) {
			addErrorMessage("Password cannot contain '@'",
					"Password cannot contain '@'");
			return;
		} else {
			String destPath = "smb://"
					+ driveLetter.getRestoreDomain()
					+ ";"
					+ driveLetter.getRestoreUsername()
					+ ":"
					+ driveLetter.getRestorePassword()
					+ "@"
					+ remoteUNC.substring(2, remoteUNC.length())
							.replaceAll("\\\\", "/").trim() + "/";
//			System.out.println("destination path: " + destPath);
			try {
				SmbFile file = new SmbFile(destPath);
				if (!file.exists()) {
					addErrorMessage("share path not found",
							"share path not found");
				} else {
					System.out.println(file.list()); //throws exception access denied if user does not have permission
					addInfoMessage("share path and credectials verified",
							"share path verified");
				}
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				addErrorMessage("Error verifying credentials",
						"Error verifying credentials");
			} catch (SmbAuthException ex) {
				ex.printStackTrace();
				addErrorMessage("Incorrect domain, username and/or password, or Access is denied ",
						"Incorrect domain, username and/or password, or Access is denied");
			} catch (SmbException e) {
				// TODO Auto-generated catch block
				addErrorMessage("Server or Sharename not found",
						"Server or Sharename not found");
				e.printStackTrace();
			}

		}
		
	}
	
	
	private boolean analysisChart;
		
	public boolean isAnalysisChart() {
		return analysisChart;
	}

	public void setAnalysisChart(boolean analysisChart) {
		this.analysisChart = analysisChart;
	}

	private PieChartModel pieModel;
		
	public PieChartModel getPieModel() {
		return pieModel;
	}

	public void setPieModel(PieChartModel pieModel) {
		this.pieModel = pieModel;
	}

	public int getShareFreeSpace(String smbPath){
		
		try{				
				SmbFile smbFile = new SmbFile(smbPath);
				if (!smbFile.exists()) {
				         System.out.println("UNC Path Invalid....");
				         return 0;
				} else {
					System.out.println(smbFile.list()); //throws exception access denied if user does not have permission
				
					float totalSpace=smbFile.length();
					long freeSpace=smbFile.getDiskFreeSpace();
					
					if(totalSpace>0 && totalSpace>freeSpace){
						analysisChart=true;
						float freePer=(freeSpace/totalSpace);
						freePer=freePer*100;
						return (int)freePer;
					}else{
						return 100;
					}
					
				}
		
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return 0;
		
	}

	/*
	 * smbFile.getDiskFreeSpace(): 74319040512
                smbFile.length(): 123003203584
	 * 
	 * public String update(){
	 * 
	 * if(driveLetter.getRestoreDomain()!=null &&
	 * driveLetter.getRestoreDomain().trim().length()>0){
	 * if(driveLetter.getRestoreUsername() == null ||
	 * driveLetter.getRestoreUsername().trim().length()<1 ||
	 * driveLetter.getRestorePassword() == null ||
	 * driveLetter.getRestorePassword().trim().length()<1){
	 * System.out.println("domain, username, or password is missing");
	 * addErrorMessage
	 * ("Please provide all three fields i.e, remote domain, username, or password"
	 * , "domain, username, or password is missing"); return null; } }else
	 * if(driveLetter.getRestoreUsername() !=null &&
	 * driveLetter.getRestoreUsername().trim().length()>0){
	 * if(driveLetter.getRestoreDomain() == null ||
	 * driveLetter.getRestoreDomain().trim().length()<1 ||
	 * driveLetter.getRestorePassword() == null ||
	 * driveLetter.getRestorePassword().trim().length()<1){
	 * System.out.println("domain, username, or password is missing");
	 * addErrorMessage
	 * ("Please provide all three fields i.e, remote domain, username, or password"
	 * , "domain, username, or password is missing"); return null; } }else
	 * if(driveLetter.getRestorePassword() !=null &&
	 * driveLetter.getRestorePassword().trim().length()>0){
	 * if(driveLetter.getRestoreDomain() == null ||
	 * driveLetter.getRestoreDomain().trim().length()<1 ||
	 * driveLetter.getRestoreUsername() == null ||
	 * driveLetter.getRestoreUsername().trim().length()<1){
	 * System.out.println("domain, username, or password is missing");
	 * addErrorMessage
	 * ("Please provide all three fields i.e, remote domain, username, or password"
	 * , "domain, username, or password is missing"); return null; } }
	 * 
	 * List<Agent>
	 * storedAgentsList=agentService.getAgentByName(agent.getName()); boolean
	 * exists=false; Agent tempAgent; for(int i=0;storedAgentsList!=null &&
	 * i<storedAgentsList.size();i++){ tempAgent=storedAgentsList.get(i);
	 * if(tempAgent.getName().equalsIgnoreCase(agent.getName()) &&
	 * tempAgent.getId()!=agent.getId()){ exists=true; } }
	 * 
	 * if(exists){ System.out.println("Agent already exists");
	 * addErrorMessage("Agent name already exists",
	 * "Agent name already exists"); return null; }
	 * 
	 * storedAgentsList=agentService.getAgentByLogin(agent.getLogin()); for(int
	 * i=0;storedAgentsList!=null && i<storedAgentsList.size();i++){
	 * tempAgent=storedAgentsList.get(i);
	 * if(tempAgent.getLogin().equalsIgnoreCase(agent.getLogin()) &&
	 * tempAgent.getId()!=agent.getId() ){ exists=true; } }
	 * 
	 * if(exists){ System.out.println("Agent login already exists");
	 * addErrorMessage("Agent login already exists",
	 * "Agent login already exists"); return null; } agent.setActive(1);
	 * agentService.updateAgent(agent); return
	 * "Agents.faces?faces-redirect=true"; }
	 */

	public String delete() {
        shareAnaylysisService.deleteByDriveId(driveLetter.getId());
		driveLettersService.deleteDriveLetter(driveLetter);
		System.out.println("after delete: "+driveLettersService.getAll().size());
		driveLettersList = new ArrayList<DriveLetters>();
		return "DriveLetters.faces?faces-redirect=true";
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public Agent getAgent() {
		return agent;
	}

	public DriveLetters getDriveLetter() {
		return driveLetter;
	}

	public void setDriveLetter(DriveLetters driveLetter) {
		this.driveLetter = driveLetter;
	}
	
	public List<DriveLetters> getDriveLettersList() {
		return driveLettersList;
	}

	public void setDriveLettersList(List<DriveLetters> driveLettersList) {
		this.driveLettersList = driveLettersList;
	}

	public Integer getDriveLetterId() {
		return driveLetterId;
	}

	public void setDriveLetterId(Integer driveLetterId) {
		this.driveLetterId = driveLetterId;
	}

	public String[] getLetters() {
		return letters;
	}

	public void setLetters(String[] letters) {
		this.letters = letters;
	}

	public String[] getShares() {
		return shares;
	}

	public void setShares(String[] shares) {
		this.shares = shares;
	}

	public boolean isAnalyze() {
		return analyze;
	}

	public void setAnalyze(boolean analyze) {
		this.analyze = analyze;
	}


	public boolean isEnableMonitoring() {
		return enableMonitoring;
	}


	public void setEnableMonitoring(boolean enableMonitoring) {
		this.enableMonitoring = enableMonitoring;
	}

	
	

}
