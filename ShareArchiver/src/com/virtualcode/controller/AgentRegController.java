package com.virtualcode.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.FacesComponent;
import javax.faces.context.FacesContext;

import com.virtualcode.services.AgentService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.services.impl.AgentServiceImpl;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.Agent;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.jsf.FacesContextUtils;
 
@ManagedBean(name="agentRegController")
@ViewScoped
public class AgentRegController extends AbstractController implements Serializable {
			
	public AgentRegController(){		
		super();
		agent=new Agent();
	}
	
	private Integer agentId;
	
    private Agent agent;
	
    
			
	public List<Agent> getAllAgents(){		
		List<Agent> list= agentService.getAllAgents();
//		System.out.println("Total Agents Records found: "+list.size());
		return list;
	}
	
	public List<Agent> getAllFSAgents(){		
		List<Agent> list= agentService.getAgentByType("FS");
//		System.out.println("Total Agents Records found: "+list.size());
		return list;
	}
	
	public List<Agent> getAllSPAgents(){		
		List<Agent> list= agentService.getAgentByType("SP");
//		System.out.println("Total Agents Records found: "+list.size());
		return list;
	}
	
	public void populateAgentInfo(){
		
		if(agentId!=null && agentId.intValue()>0  && ! FacesContext.getCurrentInstance().isPostback()){
			agent=agentService.getAgentById(agentId);
		}
	}
	
	
	public String save(){
		List<Agent> storedAgentsList=agentService.getAgentByName(agent.getName());
		boolean exists=false;
		Agent tempAgent;
		for(int i=0;storedAgentsList!=null && i<storedAgentsList.size();i++){
			tempAgent=storedAgentsList.get(i);
			if(tempAgent.getName().equalsIgnoreCase(agent.getName()) && tempAgent.getId()!=agent.getId()){
				exists=true;
			}
		}
		
		if(exists){
			System.out.println("Agent already exists");
			addErrorMessage("Agent name already exists", "Agent name already exists");				 
			return null;
		}
		
		storedAgentsList=agentService.getAgentByLogin(agent.getLogin());
		for(int i=0;storedAgentsList!=null && i<storedAgentsList.size();i++){
			tempAgent=storedAgentsList.get(i);
			if(tempAgent.getLogin().equalsIgnoreCase(agent.getLogin()) && tempAgent.getId()!=agent.getId() ){
				exists=true;
			}
		}
		
		if(exists){
			  System.out.println("Agent login already exists");
			  addErrorMessage("Agent login already exists", "Agent login already exists");				 
			  return null;
		}
		agent.setActive(1);
		agentService.saveAgent(agent);
		return "Agents.faces?faces-redirect=true";
	}
	
	
	
	public String update(){
		/*
		List<Agent> storedAgentsList=agentService.getAgentByName(agent.getName());
		boolean exists=false;
		Agent tempAgent;
		for(int i=0;storedAgentsList!=null && i<storedAgentsList.size();i++){
			tempAgent=storedAgentsList.get(i);
			if(tempAgent.getName().equalsIgnoreCase(agent.getName()) && tempAgent.getId()!=agent.getId()){
				exists=true;
			}
		}
		
		if(exists){
			System.out.println("Agent already exists");
			addErrorMessage("Agent name already exists", "Agent name already exists");				 
			return null;
		}
			
		storedAgentsList=agentService.getAgentByLogin(agent.getLogin());
		for(int i=0;storedAgentsList!=null && i<storedAgentsList.size();i++){
			tempAgent=storedAgentsList.get(i);
			if(tempAgent.getLogin().equalsIgnoreCase(agent.getLogin()) && tempAgent.getId()!=agent.getId() ){
				exists=true;
			}
		}
		
		if(exists){
			System.out.println("Agent login already exists");
			addErrorMessage("Agent login already exists", "Agent login already exists");				 
			return null;
		}*/
		agent.setActive(1);
		agentService.updateAgent(agent);
		return "Agents.faces?faces-redirect=true";
	}
	
	
	public String delete(){
		List list = jobService.getJobByProperty("agent.id", agent.getId());
		System.out.println("agent with jobs list size: " + list.size());
		if(list!=null && list.size()>0){
			addErrorMessage("Agent has jobs assiciated, Cannot delete agent ["+ agent.getName() +"]", "agent cannot be deleted");
			return null;
		}else{
			agentService.deleteAgent(agent.getId());
		}
		return "Agents.faces?faces-redirect=true";
	}
	
	public void testSPService(){
		System.out.println("testing SP service at: " + agent.getAllowedIps());
		boolean flag = sharePointService.testSPAgentService(agent.getAllowedIps());
		if(!flag)
			addErrorMessage("Sharepoint server not available", "Sharepoint server not available");
		else
			addInfoMessage("Sharepoint server connected successfully", "Sharepoint server connected successfully");
	}
	
	
	public Integer getAgentId() {
		return agentId;
	}

	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public Agent getAgent() {
		return agent;
	}
	

}
