/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.controller;

//import com.virtualcode.agent.das.Executors.StartAgentJobs;
//import com.virtualcode.RepositoryOpManager;
//import com.virtualcode.base.web.RepositoryAccessServlet;
import com.virtualcode.cifs.Document;
import com.virtualcode.repository.UploadDocumentService;

import java.util.ArrayList;
import java.util.List;
//import javax.jcr.Repository;
//import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceException;
import org.apache.log4j.Logger;

/**
 *
 * @author YAwar
 */
public class DataExportModule {
    static final String SECURE_INFO = "secureInfo";
    static final Logger log = Logger.getLogger(DataExportModule.class);
    //RepositoryOpManager opManager;
    
    public List<Document> getAllChildren(String parent, String layersList) {
        //TODO write your implementation code here:
        //synchronized (context) {
            log.info("getAllChilds: Started");
            log.debug("Base-64 encoded Path: " + parent);

            //ServletContext sc = StartAgentJobs.getContext();
            //Repository repository = RepositoryAccessServlet.getRepository(sc);
            
            ArrayList<Document> children = null;
            

            try {
      //          opManager = RepositoryOpManager.getOpManager(repository);

                log.debug("Encoded File Path: " + parent);
        //        children = opManager.getAllChildren(parent, layersList);
                uploadDocumentService = (UploadDocumentService)com.virtualcode.util.SpringApplicationContext.getBean("uploadDocumentService");
                System.out.println("value: " + uploadDocumentService);
                children = uploadDocumentService.getAllChildren(parent, layersList);

            } catch (Exception ex) {
            	ex.printStackTrace();
                log.error("Exception in WebService Method EX: " + ex);
                throw new WebServiceException(ex);
            } finally {
                
            }
            return children;
        //}//ending Sync Block

    }
    private UploadDocumentService uploadDocumentService;
    
    public void setUploadDocumentService(UploadDocumentService uploadDocumentService) {
		this.uploadDocumentService = uploadDocumentService;
	}

	
}
