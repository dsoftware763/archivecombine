package com.virtualcode.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.virtualcode.common.PLFormatException;
import com.virtualcode.repository.monitor.RepositoryMonitor;
import com.virtualcode.util.License;
import com.virtualcode.util.PLVManager;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.RepositoryStatus;
import com.virtualcode.vo.SystemCode;
import com.virtualcode.vo.SystemCodeType;

@ManagedBean(name="repoStatusController")
@RequestScoped
public class RepositoryStatusController  extends AbstractController{
	
		
	public RepositoryStatusController() {
		super();
		populateLicensingInfo();
		getRepositoryStatus();
	}
	
	private RepositoryStatus repositoryStatus;
	public RepositoryStatus getRepositoryStatus() {
		 RepositoryMonitor repositoryMonitor = RepositoryMonitor.getInstance();
		 RepositoryStatus repositoryStatus = repositoryMonitor.getRepositoryStatus(null);
		 ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		 
		 Long criticalLimit = 95l;
		 
		 if(ru.getCodeValue("server_critical_limit", SystemCodeType.GENERAL)!=null){
			 criticalLimit = new Long(ru.getCodeValue("server_critical_limit", SystemCodeType.GENERAL));
		 }
		 if(repositoryStatus.getTotalFreeSpaceOnDisk()>=criticalLimit){
			 storageLevelCritical = true;
		 }
		 this.repositoryStatus= repositoryStatus;
		 return repositoryStatus;
	}
	
	
	private void populateLicensingInfo(){
		try{
			
			List<SystemCode> sysCodesList= systemCodeService.getSystemCodeByCodename("sh_license");
            if(sysCodesList!=null && sysCodesList.size()>0){
            	String licenseStr=sysCodesList.get(0).getCodevalue();
            	try{
            	 License license=PLVManager.getInstance().readLicense(licenseStr);
            	 licenseType=license.getLicenseType();
            	 licenseParam=license.getParameter();
            	
            	 PLVManager plvManager=PLVManager.getInstance();
            	 active=plvManager.isValidLicense(license);
            	 repoLimit=license.getRepoLimitGb();
            	 if(licenseParam.equalsIgnoreCase("Time") || licenseParam.equalsIgnoreCase("both")){
	            	 Calendar validTo=Calendar.getInstance();
	            	 validTo.setTime(license.getValidUntil());
	            	 validTo.add(Calendar.DATE, 1);
	            	 Calendar today=Calendar.getInstance();
	            	 today.setTime(new Date());
	            	 
	            	 validUntil= VCSUtil.getDateAsString(validTo.getTime());
            	 }
            	 
            	}catch(PLFormatException ex){
            		ex.printStackTrace();
            		licenseType="unknown";
            	}
            }
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	
	private String licenseType;
	
	private boolean active=false;
	
	private String licenseParam=null;
	
	private String validUntil=null;
	
	private double repoLimit=-1;
	
	private boolean storageLevelCritical = false;
	
	public boolean isActive() {
		return active;
	}


	public String getValidUntil() {
		return validUntil;
	}


	public String getLicenseType() {
		return licenseType;
	}


	public String getLicenseParam() {
		return licenseParam;
	}


	public double getRepoLimit() {
		return repoLimit;
	}


	public boolean isStorageLevelCritical() {
		return storageLevelCritical;
	}


	public void setStorageLevelCritical(boolean storageLevelCritical) {
		this.storageLevelCritical = storageLevelCritical;
	}

	
	
	


}
