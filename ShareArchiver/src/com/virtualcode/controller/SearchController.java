package com.virtualcode.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.jcr.RepositoryException;
import javax.jcr.query.RowIterator;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jcifs.smb.SmbFile;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import com.virtualcode.common.VCSConstants;
import com.virtualcode.repository.UserActivityService;
import com.virtualcode.security.impl.VirtualCodeAuthorizatorImpl;
import com.virtualcode.services.DriveLettersService;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.RoleFeatureUtil;
import com.virtualcode.util.StringUtils;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.util.VirtualcodeUtil;
import com.virtualcode.vo.Agent;
import com.virtualcode.vo.CustomTreeNode;
import com.virtualcode.vo.DriveLetters;
import com.virtualcode.vo.Job;
import com.virtualcode.vo.JobSpDocLib;
import com.virtualcode.vo.Policy;
import com.virtualcode.vo.RoleFeature;
import com.virtualcode.vo.SearchDocument;
import com.virtualcode.vo.SearchReport;
import com.virtualcode.vo.SearchResult;
import com.virtualcode.vo.SystemCodeType;

@ManagedBean(name="searchController")
@ViewScoped
public class SearchController extends AbstractController{
	ResourcesUtil   ru  =   ResourcesUtil.getResourcesUtil();
	public SearchController(){		
		super();		
		String authType = (String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE); 
    	
    	String enablePrivExport = ru.getCodeValue("enable.priv.export", SystemCodeType.GENERAL);
    	
    	if(enablePrivExport!=null && enablePrivExport.equalsIgnoreCase("yes") && authType.trim().equalsIgnoreCase("db")){
    		this.enablePrivExport = true;
    	}
    	
    	String enablePrivReport = ru.getCodeValue("enable.priv.report", SystemCodeType.GENERAL);
    	
    	if(enablePrivReport!=null && enablePrivReport.equalsIgnoreCase("yes") && authType.trim().equalsIgnoreCase("db")){
    		this.enablePrivReport = true;
    	}
    	
      String showPathInSearchResults = ru.getCodeValue("show_path_in_search_results", SystemCodeType.GENERAL);
      if(showPathInSearchResults!=null && showPathInSearchResults.equals("yes")){                	 
     	 showDocumentPath = true;
      }

    
        job = new Job();
        
        activeDeActivateUserControls();
        
        if(enableCifsSearch && enableRepoSearch){
        	repoTabTitle = "File Server";
        	cifsTabTitle = "Repository";
        }else{
        	repoTabTitle = "Results";
        	cifsTabTitle = "Results";
        }
        	
	}
	
	private String searchString;
	
	private String documentName;
	
	private Date modifiedStartDate;
	
	private Date modifiedEndDate;
	
	private String sortMode="Desc";
	
	private long executionTime=0;
	
	private int currentPage=1;
	
	private List<String> curPagesList	=	new ArrayList<String>();
	
	private int itemsPerPage=100;
	
	private int maxPageCtr=5;

	private int currentPageCifs=1;
	
	private List<String> curPagesListCifs	=	new ArrayList<String>();
	
	private int itemsPerPageCifs=100;
	
	private int maxPageCtrCifs=5;

	private int totalPages=1;
	
	private int totalPagesCifs=1;
	
	private Integer totalRecords=-1;
	
	private Integer totalRecordsCifs=-1;
	
	private String sortOption="jcr:title";
	
	private List<SearchDocument> searchResults=new ArrayList<SearchDocument>();
	
	private List<SearchDocument> searchResultsCifs=new ArrayList<SearchDocument>();
	
	@ManagedProperty(value="#{jobController}")
	private JobController jobController;
	
	@ManagedProperty(value="#{searchFileController}")
	private SearchFileController searchFileController;
	
	private boolean enablePrivExport = false;
	
	private boolean enablePrivReport = false;
	
	private boolean enableDocumentTagging=false;
	
	private boolean showTagsinSearchResults=false;
	
	private boolean enableFolderSearch=false;
	
	private String documentTags;
	
	private SearchDocument selectedDocument = new SearchDocument();
	
	private String targetFolder;
	
	private boolean contentTypeSearchEnabled=false;
	
	private String contentType;
	
	private Job job;	
	
	private String totalFileCount;
	
	private String totalFileSize;
	
	private String remoteDomainName;
	 
	private String remoteUserName;
	 
	private String remoteUserPassword;
	 
	private String remoteUNC;
	 
	private String exportNodePath;
	 
	private boolean selectAll = false;
		
	private boolean preserveFilePaths = true;
		
	private boolean exportDuplicates = false;
	
	private String excludedDocumentName;
	
	private String searchQuery;
	
	private boolean showDocumentPath = false;
	
	private Long fileSizeStart;
	
	private Long fileSizeEnd;
	
	private String unitForFileSizeStart = "mbs";
	
	private String unitForFileSizeEnd = "mbs";
	
	private Date archivedStartDate;
	
	private Date archivedEndDate;
	
	private Long modifiedStartDateLong;
	
	private Long modifiedEndDateLong;
	
	private Long archivedStartDateLong;
	
	private Long archivedEndDateLong;
	
	private boolean archiveDateSearchEnabled=false;
	
	private boolean enableSizeRangeSelector = false;
	
	private boolean enableModDateRangeSlider = false;
	
	private boolean enableArcDateRangeSlider = false;
	
	private boolean enableCrtDateRangeSlider = false;
	
	private boolean enableSearchById;
	
	private String documentUUid;
	
	private boolean enableCifsSearch = false;
	
	private boolean enableRepoSearch = false;
	
	private Date createdStartDate;
	
	private Date createdEndDate;
	
	private Long createdStartDateLong;
	
	private Long createdEndDateLong;
	
	private String uncPath;
	
    private boolean searchCifs = false;
    
    private boolean searchArchive = true;
    
    private String repoTabTitle;
    
    private String cifsTabTitle;
    
    private String sortOptionCifs="fileName";
    
    private boolean enableUserFeatures = false;
    
    private boolean splitmodeEnabled = false;
    
    private String pdfViewerUrl;
    
	public void searchAll(){
		if(searchArchive){
			search();
		}else{
			if(searchResults!=null)
				this.searchResults.clear();
			totalRecords=0;
			this.curPagesList.clear();
			
		}
		if(searchCifs){
			if(searchString!=null && searchString.trim().length()>0){
		    	FacesMessage fmessage=new FacesMessage();
		    	fmessage.setSummary("Keyword search is not currently supported for remote file servers");
		    	fmessage.setDetail("Keyword search is not currently supported for remote file servers");
		    	
		    	fmessage.setSeverity(FacesMessage.SEVERITY_WARN);
		    	
		    	FacesContext.getCurrentInstance().addMessage("searchForm:searchString", fmessage);
			}else{
				searchCifs();
			}
		}else{
			if(searchResultsCifs!=null)
				this.searchResultsCifs.clear();
			totalRecordsCifs=0;
			this.curPagesListCifs.clear();
			
		}
	}
	

	public void search(){
		System.out.println("checkbox value: " + enableSizeRangeSelector);
		try{
			this.searchResults.clear();
			totalRecords=0;
			this.curPagesList.clear();
			if(getRequestParameter("currentPage")!=null && !getRequestParameter("currentPage").isEmpty())
				currentPage	=	Integer.parseInt(getRequestParameter("currentPage"));
			else
				currentPage	=	1;
			/*
			if(getRequestParameter("sortMode")!=null && !getRequestParameter("sortMode").isEmpty())
				sortMode	=	getRequestParameter("sortMode");
			else
				sortMode	=	"asc";
			if(getRequestParameter("sortOption")!=null && !getRequestParameter("sortOption").isEmpty())
				sortOption	=	getRequestParameter("sortOption");
			else
				sortOption	=	"jcr:title";
			*/
			Calendar calendar = Calendar.getInstance();
			if(enableModDateRangeSlider && modifiedStartDate==null && modifiedEndDate==null){
				
				if(modifiedStartDateLong!=null){
					calendar.setTimeInMillis(modifiedStartDateLong);
					modifiedStartDate = calendar.getTime();
				}
				if(modifiedEndDateLong!=null){
					calendar.setTimeInMillis(modifiedEndDateLong);
					modifiedEndDate = calendar.getTime();
				}
			}
			if(enableArcDateRangeSlider && archivedStartDate == null && archivedEndDate == null){
				if(archivedStartDateLong!=null){
					calendar.setTimeInMillis(archivedStartDateLong);
					archivedStartDate = calendar.getTime();
				}
				if(archivedEndDateLong!=null){
					calendar.setTimeInMillis(archivedEndDateLong);
					archivedEndDate = calendar.getTime();
				}
			}
		   long start=new Date().getTime();
		   
		   String  startDateStr= enableModDateRangeSlider?VCSUtil.getDateAsString(modifiedStartDate):null;
		   String endDateStr=enableModDateRangeSlider?VCSUtil.getDateAsString(modifiedEndDate):null;
		   String startArchiveDateStr = enableArcDateRangeSlider?VCSUtil.getDateAsString(archivedStartDate):null;
		   String endArchiveDateStr = enableArcDateRangeSlider?VCSUtil.getDateAsString(archivedEndDate):null;

		   
		   if(!enableSizeRangeSelector)
		   {
			   fileSizeStart = null;
			   fileSizeEnd = null;
		   }
		   
		   if(startDateStr==null && endDateStr==null &&  (searchString==null || searchString.trim().equals("")) &&(documentName==null || documentName.trim().equals("")) 
				   && (documentTags==null || documentTags.trim().equals("")) && (targetFolder==null || targetFolder.trim().equals("")) && (contentType==null || contentType.isEmpty())
				   &&(excludedDocumentName==null || excludedDocumentName.trim().equals("")) && (fileSizeStart==null || fileSizeStart<1l)
				   && (fileSizeEnd==null || fileSizeEnd<1l) && startArchiveDateStr==null && endArchiveDateStr==null){
//			   addInfoMessage("Search requires atleast one  parameter for searching.", "Search requires atleast one  parameter for searching.");
			   
		    	FacesMessage fmessage=new FacesMessage();
		    	fmessage.setSummary("At least enter one parameter is required for searching.");
		    	fmessage.setDetail("At least enter one parameter is required for searching.");
		    	
		    	fmessage.setSeverity(FacesMessage.SEVERITY_WARN);
		    	
		    	FacesContext.getCurrentInstance().addMessage("searchForm:searchString", fmessage);
			   return;
		   }
		   System.out.println("---session type:"+getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE));
		   String sidString = "";
		   if(getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE).equals("db")){
			   sidString = null;
		   }else{
			   System.out.println("--getting sids for user: " + getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME));
			   VirtualCodeAuthorizatorImpl authorizator = VirtualCodeAuthorizatorImpl.getInstance();
			   sidString = authorizator.sidString(getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME)+"");
		   }
		   
		   String tempTargetFolder = null;
		   
		   if(targetFolder!=null && targetFolder.trim().length()>0)
			   tempTargetFolder = processSearchFolderPath(targetFolder);	
		   //set to null as the functionality is incomplete yet
//		   startArchiveDateStr = null;
//		   endArchiveDateStr = null;
		   String documentName = this.documentName;
		   
		   if(documentName!=null && documentName.trim().length()>0 && !documentName.endsWith("*")){
			   documentName+="*";	
		   }
		   //to exclude Thumbs.db from search		   
		   if(documentName!=null && documentName.trim().length()>0){
			   documentName+=",-Thumbs.db";
		   }else{
			   documentName ="-Thumbs.db";
		   }
//		   if(documentTags.equals("archive")){
//			   documentTags ;= "[" + documentTags +"]";
//		   }

		  SearchResult result= searchService.searchDocument("DISPLAY", searchString, null, null, startDateStr, endDateStr, documentName, sidString, sortMode, sortOption, documentTags, 
				  tempTargetFolder,contentType, currentPage, itemsPerPage, convertFileSizeToBytes(fileSizeStart, unitForFileSizeStart), 
				  convertFileSizeToBytes(fileSizeEnd, unitForFileSizeStart), startArchiveDateStr, endArchiveDateStr);		   
		  		  
		   this.searchResults=result.getResults();
		   this.searchQuery = result.getSearchQuery();
		   jobController.setSearchResults(searchResults);
		   calculatePages(result);
		   
		   long end=new Date().getTime();
		   executionTime=(end-start)/1000;	   
		   
		   UserActivityService userActivityService=serviceManager.getUserActivityService();
		   HttpSession httpSession=getHttpSession();
		   String userName=httpSession.getAttribute(VCSConstants.SESSION_USER_NAME).toString();
		   String userIP=null;
		   if(httpSession.getAttribute(VCSConstants.SESSION_USER_NAME)!=null)
		     userIP=httpSession.getAttribute(VCSConstants.SESSION_USER_NAME).toString();
		   
		   userActivityService.recordSearchActivity(userName, userIP, httpSession.getId(), httpSession.getCreationTime(), result.getSearchQuery(), result.getTotalRecordsFound());
		  
		  
		
		}catch(Exception ex){
			ex.printStackTrace();			
		}
	}
	
	public void searchCifs(){
		System.out.println("checkbox value: " + enableSizeRangeSelector);
		try{
			this.searchResultsCifs.clear();
			totalRecordsCifs=0;
			this.curPagesListCifs.clear();
			if(getRequestParameter("currentPage")!=null && !getRequestParameter("currentPage").isEmpty())
				currentPageCifs	=	Integer.parseInt(getRequestParameter("currentPage"));
			else
				currentPageCifs	=	1;
			System.out.println(currentPageCifs);
			/*
			if(getRequestParameter("sortMode")!=null && !getRequestParameter("sortMode").isEmpty())
				sortMode	=	getRequestParameter("sortMode");
			else
				sortMode	=	"asc";
			if(getRequestParameter("sortOption")!=null && !getRequestParameter("sortOption").isEmpty())
				sortOption	=	getRequestParameter("sortOption");
			else
				sortOption	=	"jcr:title";
			*/
			Calendar calendar = Calendar.getInstance();
			if(enableModDateRangeSlider && modifiedStartDate==null && modifiedEndDate==null){
				
				if(modifiedStartDateLong!=null){
					calendar.setTimeInMillis(modifiedStartDateLong);
					modifiedStartDate = calendar.getTime();
				}
				if(modifiedEndDateLong!=null){
					calendar.setTimeInMillis(modifiedEndDateLong);
					modifiedEndDate = calendar.getTime();
				}
			}
			if(enableArcDateRangeSlider && archivedStartDate == null && archivedEndDate == null){
				if(archivedStartDateLong!=null){
					calendar.setTimeInMillis(archivedStartDateLong);
					archivedStartDate = calendar.getTime();
				}
				if(archivedEndDateLong!=null){
					calendar.setTimeInMillis(archivedEndDateLong);
					archivedEndDate = calendar.getTime();
				}
			}
			if(enableCrtDateRangeSlider && createdStartDate == null && createdEndDate == null){
				if(createdStartDateLong!=null){
					calendar.setTimeInMillis(createdStartDateLong);
					createdStartDate = calendar.getTime();
				}
				if(createdEndDateLong!=null){
					calendar.setTimeInMillis(createdEndDateLong);
					createdEndDate = calendar.getTime();
				}
			}
		   long start=new Date().getTime();
		   
		   String  startDateStr= enableModDateRangeSlider?VCSUtil.getDateAsString(modifiedStartDate):null;
		   String endDateStr=enableModDateRangeSlider?VCSUtil.getDateAsString(modifiedEndDate):null;
		   String startArchiveDateStr = enableArcDateRangeSlider?VCSUtil.getDateAsString(archivedStartDate):null;
		   String endArchiveDateStr = enableArcDateRangeSlider?VCSUtil.getDateAsString(archivedEndDate):null;
		   String startCreateDateStr = enableCrtDateRangeSlider?VCSUtil.getDateAsString(createdStartDate):null;
		   String endCreateDateStr = enableCrtDateRangeSlider?VCSUtil.getDateAsString(createdEndDate):null;
		   
		   if(!enableSizeRangeSelector)
		   {
			   fileSizeStart = null;
			   fileSizeEnd = null;
		   }
		   
//		   if(uncPath==null || uncPath.trim().length()<1){
//		    	FacesMessage fmessage=new FacesMessage();
//		    	fmessage.setSummary("Please enter a path for searching");
//		    	fmessage.setDetail("Please enter a path for searching");
//		    	
//		    	fmessage.setSeverity(FacesMessage.SEVERITY_WARN);
//		    	
//		    	FacesContext.getCurrentInstance().addMessage("searchForm:searchString", fmessage);
//			   return;
//		   }
			   
		   
		   if(startDateStr==null && endDateStr==null &&  (documentName==null || documentName.trim().equals("")) 
				   && (targetFolder==null || targetFolder.trim().equals("")) 
				   &&(excludedDocumentName==null || excludedDocumentName.trim().equals("")) && (fileSizeStart==null || fileSizeStart<1l)
				   && (fileSizeEnd==null || fileSizeEnd<1l) && startCreateDateStr==null && endCreateDateStr==null){// && startArchiveDateStr==null && endArchiveDateStr==null){
//			   addInfoMessage("Search requires atleast one  parameter for searching.", "Search requires atleast one  parameter for searching.");
			   
		    	FacesMessage fmessage=new FacesMessage();
		    	fmessage.setSummary("At least enter one parameter is required for searching.");
		    	fmessage.setDetail("At least enter one parameter is required for searching.");
		    	
		    	fmessage.setSeverity(FacesMessage.SEVERITY_WARN);
		    	
		    	FacesContext.getCurrentInstance().addMessage("searchForm:searchString", fmessage);
			   return;
		   }
		   System.out.println("---session type:"+getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE));
		   String sidString = "";
		   if(getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE).equals("db")){
			   sidString = null;
		   }else{
			   System.out.println("--getting sids for user: " + getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME));
			   VirtualCodeAuthorizatorImpl authorizator = VirtualCodeAuthorizatorImpl.getInstance();
			   sidString = authorizator.sidString(getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME)+"");
		   }
		   
//		   if(uncPath!=null && uncPath.trim().length()>0)
//			   uncPath = processSearchFolderPath(uncPath);	
		   //set to null as the functionality is incomplete yet
//		   startArchiveDateStr = null;
//		   endArchiveDateStr = null;
		   String documentName = this.documentName;
		   
		   if(documentName!=null && documentName.trim().length()>0 && !documentName.endsWith("*")){
			   documentName+="*";	
		   }
		   //to exclude Thumbs.db from search		   
//		   if(documentName!=null && documentName.trim().length()>0){
//			   documentName+=",-Thumbs.db";
//		   }else{
//			   documentName ="-Thumbs.db";
//		   }
//		   if(documentTags.equals("archive")){
//			   documentTags ;= "[" + documentTags +"]";
//		   }
		   
//		   String sidString = "";
//		   if(getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE).equals("db")){
//			   sidString = null;
//		   }else{
//			   System.out.println("--getting sids for user: " + getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME));
//			   VirtualCodeAuthorizatorImpl authorizator = VirtualCodeAuthorizatorImpl.getInstance();
//			   sidString = authorizator.sidString(getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME)+"");
//		   }

//		   SearchResult result= searchService.searchCifsDocument(spDocLibID, authorName, minSize, maxSize, createStartDate, createEndDate, modifiedStartDate, modifiedEndDate, uncPath, documentName, documentExtension, sidString, documentName, startDateStr, tags, sidString, documentName, currentPage, itemsPerPage, maxSearchRes)
//		   SearchResult searchCifsDocument(String spDocLibID, String authorName,String minSize, String maxSize, 
//					String createStartDate, String createEndDate, String modifiedStartDate, String modifiedEndDate, 
//					String uncPath, String documentName, String documentExtension, String securitySids, String sortMode, String sortOption, 
//					String tags, String targetFolder,String contentType, int currentPage, int itemsPerPage, Integer maxSearchRes)
		   //get the Drive letter for searching in the indexes
		   String spDocLibID = null;
//		   if(uncPath!=null && uncPath.length()>0){
		   String targetFolder = null;
		   if(this.targetFolder!=null && this.targetFolder.trim().length()>0){
			   
			   Pattern pathPattern=Pattern.compile("(^[a-zA-Z]:)", Pattern.UNICODE_CASE);
				
				Matcher matcher=pathPattern.matcher(this.targetFolder);
				if(matcher.find()){
					targetFolder = processSearchFolderPathFS(this.targetFolder);
				}else{
					targetFolder = this.targetFolder;
				}
				
				if(targetFolder.endsWith("\\"))
					targetFolder = targetFolder.substring(0, targetFolder.length()-1);
		   }
		   if(targetFolder!=null && targetFolder.length()>0){
//			   ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
				DriveLetters driveLetter = null;
				driveLetter = VirtualcodeUtil.getInstance().getDriveLetterMappingsForSearch(targetFolder.toLowerCase());
//				List<DriveLetters> driveLetterList=driveLettersService.getAll();
//
//				for(DriveLetters letter: driveLetterList){
//					String sharePath = letter.getSharePath().toLowerCase();
//					if(sharePath.startsWith("\\\\")){
//										
//						sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase() + "/fs/" + sharePath.substring(2).replaceAll("\\\\", "/");
//					}
//					if(sharePath.endsWith("/"))
//						sharePath = sharePath.substring(0, sharePath.lastIndexOf("/"));		
//					String compareStr = uncPath.trim();
//					if(!compareStr.contains("/fs/"))
//						compareStr = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase()+"/fs/"+uncPath.trim().toLowerCase();
//					System.out.println(uncPath +", " + sharePath);
//					if(uncPath!=null && compareStr.toLowerCase().contains(sharePath)){
//						driveLetter = letter;
//						break;
//					}
//				}
				if(driveLetter!=null){
					spDocLibID = driveLetter.getSpDocLibId()+"";
//					spDocLibID = "17";
				}else{
					String sharePath = targetFolder;
					if (sharePath != null && sharePath.trim().length() > 0) {
						sharePath = sharePath.replaceAll("\\\\", "/");
						sharePath = sharePath.substring(2)+"/"; // remove the first two slashes
					}
					List libsList = jobService.getLibsByNameLike(sharePath);
					JobSpDocLib lib = null;
					if(libsList!=null && libsList.size()>0){
						lib =  (JobSpDocLib)libsList.get(0);
						spDocLibID = lib.getId()+"";
//						jobService.deleteJobDependencies(job);
					}else{
				    	FacesMessage fmessage=new FacesMessage();
				    	fmessage.setSummary("Entered path or drive letter is not indexed.");
				    	fmessage.setDetail("Entered path or drive letter is not indexed.");
				    	
				    	fmessage.setSeverity(FacesMessage.SEVERITY_WARN);
				    	
				    	FacesContext.getCurrentInstance().addMessage("searchForm:searchString", fmessage);
				    	return;
					}
				}
		   }
//		   SearchResult searchCifsDocument(String spDocLibID, String authorName,String minSize, String maxSize, 
//					String createStartDate, String createEndDate, String modifiedStartDate, String modifiedEndDate, 
//					String uncPath, String documentName, String documentExtension, String securitySids, String sortMode, String sortOption, 
//					String tags, String targetFolder,String contentType, int currentPage, int itemsPerPage, Integer maxSearchRes)		   
//		  sidString = "S-1-1-0,S-1-12323-123123123,S-1-5-18";
		   
		   int currentPage = currentPageCifs;
		   if(targetFolder==null || targetFolder.trim().length()<1){
			   if(currentPage==totalPagesCifs){
	//			   System.out.println("max pages reached");
				   currentPage = currentPage-4;		   
			   }else if(currentPage==totalPagesCifs-1){
	//			   System.out.println("max pages reached");
				   currentPage = currentPage-3;
			   }else if(currentPage==totalPagesCifs-2){
	//			   System.out.println("max pages reached");
				   currentPage = currentPage-2;		   
			   }else if(currentPage==totalPagesCifs-3){
	//			   System.out.println("max pages reached");
				   currentPage = currentPage-1;
			   }
			   
			   if(currentPage<=0)
				   currentPage = 1;
		   }
		  SearchResult result= searchService.searchCifsDocument(spDocLibID, null, convertFileSizeToBytes(fileSizeStart, unitForFileSizeStart)+"", convertFileSizeToBytes(fileSizeEnd, unitForFileSizeStart)+"", 
				  startCreateDateStr, endCreateDateStr, startDateStr, endDateStr, 
				  targetFolder, documentName, null, sidString, sortMode, sortOptionCifs, 
				  null, targetFolder, null, currentPage, itemsPerPageCifs, 0);
//				  ("DISPLAY", searchString, null, null, startDateStr, endDateStr, documentName, sidString, sortMode, sortOption, documentTags, 
//				  targetFolder,contentType, currentPage, itemsPerPage, convertFileSizeToBytes(fileSizeStart, unitForFileSizeStart), 
//				  convertFileSizeToBytes(fileSizeEnd, unitForFileSizeStart));//, startArchiveDateStr, endArchiveDateStr);		   
		  		  
		   this.searchResultsCifs=result.getResults();
		   this.searchQuery = result.getSearchQuery();
		   jobController.setSearchResults(searchResultsCifs);
		   calculatePagesCifs(result);
		   
		   long end=new Date().getTime();
		   executionTime=(end-start)/1000;	   
		   
		   UserActivityService userActivityService=serviceManager.getUserActivityService();
		   HttpSession httpSession=getHttpSession();
		   String userName=httpSession.getAttribute(VCSConstants.SESSION_USER_NAME).toString();
		   String userIP=null;
		   if(httpSession.getAttribute(VCSConstants.SESSION_USER_NAME)!=null)
		     userIP=httpSession.getAttribute(VCSConstants.SESSION_USER_NAME).toString();
		   
		   userActivityService.recordSearchActivity(userName, userIP, httpSession.getId(), httpSession.getCreationTime(), result.getSearchQuery(), result.getTotalRecordsFound());
		  
		  
		
		}catch(Exception ex){
			ex.printStackTrace();			
		}
	}
	
	public void downloadUncFile(){
    	try{
    		String filePath = getRequestParameter("nodePath");
    	   HttpServletResponse response = getHttpResponse();
    	   String fileName = filePath.substring(filePath.lastIndexOf("\\\\"));//.getName();
           try {
        	String searchPath = getCompleteSmbPath(filePath);
   			long startTime = System.currentTimeMillis();
   			System.out.println("start time: " + startTime);
   			SmbFile remoteFile = new SmbFile(searchPath);
   			InputStream is = remoteFile.getInputStream();
               ServletOutputStream outs = response.getOutputStream();
               String contentType ;//= uCon.getContentType();

                String mimeType = URLConnection.guessContentTypeFromName(fileName);
                if (mimeType == null) {
                    if (fileName.endsWith(".doc")) {
                        mimeType = "application/msword";
                    } else if (fileName.endsWith(".xls")) {
                        mimeType = "application/vnd.ms-excel";
                    } else if (fileName.endsWith(".ppt")) {
                        mimeType = "application/mspowerpoint";
                    } else {
                        mimeType = "application/octet-stream";
                    }
                }
                //byte[] bytes = IOUtils.toByteArray(is);

                response.setContentLength(remoteFile.getContentLength());
                response.setContentType((mimeType != null) ? mimeType : "application/octet-stream");
                response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");



                int bit = 256;
                ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
                
                String tempSize	=	ru.getCodeValue("BUFF_SIZE_W", SystemCodeType.GENERAL);
	            int buffSize	=	8192;//defaultBufferSize
	            if(tempSize!=null && !tempSize.isEmpty()) {
	            	try {
	            		buffSize	=	Integer.parseInt(tempSize);
	            	}catch(Exception ex) {
//	            		log.error(" Error to parse BUFF_SIZE_W: "+tempSize);
	            	}
	            }
//	            log.debug(" write_buffered is: "+buffSize);
	            
                byte[] buffer = new byte[buffSize];
//                int bit;
                try {
                   
                      while ((bit = is.read(buffer, 0, buffSize)) != -1) {
                            outs.write(buffer, 0, bit);
                      }

                } catch (IOException ioe) {
//                    log.debug("io excpetion 1 " + ioe.getMessage(),ioe);
                    ioe.printStackTrace();
                }

                try {
                    is.close();
                    outs.flush();
//                    log.debug("5555");
                    outs.close();
                } catch (IOException e) {
//                    log.debug("io exception 2 " + e.getMessage(),e);
                    e.printStackTrace();
                }


        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
//            log.debug("malformed url exception " + e.getMessage(),e);
            throw new ServletException();
        } catch (IOException e) {
            // TODO Auto-generated catch block
//            log.debug("io expection 3 " + e.getMessage(),e);
            throw new ServletException();
        }
//    	getHttpResponse().sendRedirect(url);
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    }
	
	public String getCompleteSmbPath(String path){
		ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
		String domain = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
		String username = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
		String password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
	      

//		  ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
//		  String domain = ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
//		  String username = ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
//		  String password = ru.getCodeValue("restore.password", SystemCodeType.GENERAL);

		
		DriveLetters driveLetter = null;
	      	List<DriveLetters> driveLetterList=driveLettersService.getAll();
	      
	      	for(DriveLetters letter: driveLetterList){
				String sharePath = letter.getSharePath().toLowerCase();
				if(sharePath.startsWith("\\\\")){
//					ResourcesUtil ru = ResourcesUtil.getResourcesUtil();				
//					sharePath = ru.getCodeValue("companyName", SystemCodeType.GENERAL).toLowerCase() + "/fs/" + sharePath.substring(2).replaceAll("\\\\", "/");
				}
//				if(remotePath.endsWith("/"))
//					sharePath = sharePath.substring(0, sharePath.lastIndexOf("/"));
				if(!path.endsWith("\\"))
					path = path+ "\\";
				System.out.println(sharePath + ", " + path.toLowerCase());
	      	if(path.toLowerCase().contains(sharePath)){
	      		driveLetter = letter;
	      		break;
	      	}
	      }
			  if(driveLetter!=null && driveLetter.getRestoreDomain()!=null){
		 			domain =  driveLetter.getRestoreDomain();
		 			username = driveLetter.getRestoreUsername();
		 			password = driveLetter.getRestorePassword();
		 		}
			  
			  String searchPath = "smb://"+ domain +";"+ username +":"+ password +"@"+path.substring(2).replaceAll("\\\\", "/")+"/";
		return searchPath;
	}
	
	public void showFile(){
		CustomTreeNode selectedNode = null;
		String nodeId = getRequestParameter("nodeId");
		if(nodeId!=null && nodeId.length()>0){
			nodeId = nodeId.substring(nodeId.indexOf("=")+1);
			selectedNode = fileExplorerService.getNodeByUUID(nodeId);
		}
	}
	public void calculatePages(SearchResult result){
		 
		if(searchResults==null){
			totalRecords=0;
			totalPages=0;
			currentPage=1;
			curPagesList.clear();
			return;
		}
		
		////totalRecords=searchResults.size();
		totalRecords=result.getTotalRecordsFound();
	    	    
	    if(totalRecords>0){
		   float t=((float)totalRecords)/itemsPerPage;
		   totalPages= (int)Math.ceil(t);
		   
		   System.out.println("totalPages: "+totalPages + " where devident: "+t);
		   for(int i=0; i<totalPages; i++) {
			   if(i>=(currentPage-maxPageCtr) && (i<currentPage+maxPageCtr-1)) {
				   curPagesList.add(""+(i+1));
			   }
		   }
	    }
	}
	
	public void calculatePagesCifs(SearchResult result){
		 
		if(searchResultsCifs==null){
			totalRecordsCifs=0;
			totalPagesCifs=0;
			currentPageCifs=1;
			curPagesListCifs.clear();
			return;
		}
		
		////totalRecords=searchResults.size();
		totalRecordsCifs=result.getTotalRecordsFound();
	    	    
	    if(totalRecordsCifs>0){
		   float t=((float)totalRecordsCifs)/itemsPerPageCifs;
		   totalPagesCifs= (int)Math.ceil(t);
		   
		   System.out.println("totalPages: "+totalPagesCifs + " where devident: "+t);
		   for(int i=0; i<totalPagesCifs; i++) {
			   if(i>=(currentPageCifs-maxPageCtrCifs) && (i<currentPageCifs+maxPageCtrCifs-1)) {
				   curPagesListCifs.add(""+(i+1));
			   }
		   }
	    }
	}
	 /*                     length = length / 1000d;
    sizeToDisplay = df.format(length)+" KB";
    //convert to MBs
    if(length>1024d){
   	 length = length / 1024d;
   	 sizeToDisplay = df.format(length)+" MB";
    }
  //convert to GBs
    if(length>1024d){
   	 length = length / 1024d;
   	 sizeToDisplay = df.format(length)+" GB";
    }*/
	public Long convertFileSizeToBytes(Long fileSize, String unitForFileSize){
		if(fileSize==null)
			return 0l;
		if(unitForFileSize.equals("kbs"))
			return new Long(fileSize*1024);
		if(unitForFileSize.equals("mbs"))
			return new Long(fileSize*1024*1024);
		if(unitForFileSize.equals("gbs"))
			return new Long(fileSize*1024*1024*1024);
		
		return null;
	}
	
	/*
	public void sortResults(){
		
		if(searchResults==null || searchResults.size()<2){
			return;
		}
		if(sortOption==1){
			sortByName(searchResults);
		}else if(sortOption==2){
			sortByDate(searchResults);
		}else if(sortOption==3){
			sortBySize(searchResults);
		}else{
			return;
		}
		
	}*/
	
	
	private List<SearchDocument> sortByName(List<SearchDocument> list){
					
			Collections.sort(list, new Comparator() {

	            public int compare(Object o1, Object o2) {
	            	SearchDocument doc1=(SearchDocument)o1;
	            	SearchDocument doc2=(SearchDocument)o2;
	                
	            	if(sortMode.equalsIgnoreCase("desc")){
	                return doc2.getDocumentName().compareToIgnoreCase(doc1.getDocumentName());
	            	}else{
	            		return doc1.getDocumentName().compareToIgnoreCase(doc2.getDocumentName());
	            	}
	            }
	        });
			
		return list;
		
	}
	
	private List<SearchDocument> sortBySize(List<SearchDocument> list){
		
		Collections.sort(list, new Comparator() {

            public int compare(Object o1, Object o2) {
            	SearchDocument doc1=(SearchDocument)o1;
            	SearchDocument doc2=(SearchDocument)o2;
                Integer doc1Size=VCSUtil.getIntValue(doc1.getDocumentSize());
                Integer doc2Size=VCSUtil.getIntValue(doc2.getDocumentSize());
                System.out.println("*****doc1Siz ***"+doc1Size+" ********"+doc2Size);
                
                if(sortMode.equalsIgnoreCase("desc")){ 
                   return doc2Size.compareTo(doc1Size);
                }else{
                   return doc1Size.compareTo(doc2Size);
                }
            }
        });
		
	return list;	
   }
	
	
   private List<SearchDocument> sortByDate(List<SearchDocument> list){
		
		Collections.sort(list, new Comparator() {

            public int compare(Object o1, Object o2) {
            	SearchDocument doc1=(SearchDocument)o1;
            	SearchDocument doc2=(SearchDocument)o2;            	
                Date date1=VCSUtil.getDate(doc1.getLastModificationDate(),"MMMM dd, yyyy");
                Date date2=VCSUtil.getDate(doc2.getLastModificationDate(),"MMMM dd, yyyy");                 
                
                if(sortMode.equalsIgnoreCase("desc")){ 
                   return date2.compareTo(date1);
                }else{
                   return date1.compareTo(date2);
                }
            }
               
        });
		
	return list;
	
   }
   
   public SearchReport printSearchResults(){
	   SearchReport searchReport = SearchReport.getInstance();
//	   searchReport.setSearchString(searchString);
//	   searchReport.setUsername((String)getHttpRequest().getSession().getAttribute(VCSConstants.SESSION_USER_NAME));
//	   searchReport.setStartDate(modifiedStartDate.toString());
//	   searchReport.setEndDate(modifiedEndDate.toString());
//	   searchReport.setSearchResults(searchResults);
//	   try {
		//getHttpResponse().sendRedirect("PdfPrinter?faces-redirect=true;");
//	} catch (IOException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
//	}
	   return searchReport;
   }
	
   public String printSearchReport() throws JRException, IOException{
	   if(searchResults==null || searchResults.size()<1){
		   return null;		   
	   }
	   
		if(exportNodePath==null || exportNodePath.trim().length()<1){
			addErrorMessage("Please select records to print.", "Please select records to print.");
			return null;
		}
		
	   SearchReport searchReport = SearchReport.getInstance();
	   searchReport.setSearchString(searchString);
	   searchReport.setUsername((String)getHttpRequest().getSession().getAttribute(VCSConstants.SESSION_USER_NAME));
	   
	   if(modifiedStartDate!=null)
		   searchReport.setStartDate(modifiedStartDate.toString());
	   if(modifiedEndDate!=null)
		   searchReport.setEndDate(modifiedEndDate.toString());
	   
	   //according to requirement by Sir Mazhar, allow printing only on selected results
		List<SearchDocument> reportDocumentList = new ArrayList<SearchDocument>();
		int fileCount = 0;
		String decodeStr="";
		try {
			decodeStr = URLDecoder.decode(exportNodePath, "UTF-8");
			System.out.println(decodeStr);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(!selectAll){
			String[] paths = decodeStr.split("[|]");		
			System.out.println("-------------Setting up print pdf----------------------  ");

			
			for(int i =0; i< paths.length;i++){
				
				if(!(paths[i].trim().equals(""))){
					for(SearchDocument document:searchResults){
						
						if(document.getRepoPath().equals(paths[i])){
							fileCount=fileCount+1;
							reportDocumentList.add(document);
							totalFileSize = totalFileSize + Long.parseLong(document.getDocumentSize());
							break;
						}
					}
				}			
			}
			searchReport.setSearchResults(reportDocumentList);
			searchReport.setTotalRecords(fileCount+"");
		}else{
			
			searchReport.setSearchResults(searchService.searchForReport(searchQuery));	
			searchReport.setTotalRecords(totalRecords+"");
		}

	   //end
	   
//	   searchReport.setSearchResults(searchResults);
//	   searchReport.setTotalRecords(totalRecords+"");
	   searchReport.setDate(new Date()+"");
	   searchReport.setFileTypes(documentName);
	   searchReport.setTags(documentTags);
	   printPDF();
	   return "include/PrintSearchContent.faces";//+"?faces-redirect=true&RenderOutputType=pdf";
   }
   
   public void editTag(){
	   
	   String uid = getRequestParameter("selectedNode");

	   for(SearchDocument document: searchResults){
		   if(document.getUid().equals(uid)){
			   selectedDocument = document;
			   if(selectedDocument.getTags()!=null && selectedDocument.getTags().indexOf("|")!=-1){
				   selectedDocument.setTags(selectedDocument.getTags());
				   selectedDocument.setTags(selectedDocument.getTags().substring(selectedDocument.getTags().lastIndexOf("|")+1));				   
//				   selectedDocument.setTags(selectedDocument.getTags().substring(selectedDocument.getTags().lastIndexOf("|")+1));
			   }
			   break;
		   }
	   }
	   
   }
	

   public void updateTag(){
	   
	   String username=(String)getHttpRequest().getSession().getAttribute(VCSConstants.SESSION_USER_NAME);
   		Date date = VCSUtil.getCurrentDateTime();
       java.sql.Timestamp timestamp = new Timestamp(date.getTime());

	   fileExplorerService.updateTags(selectedDocument.getUid(), username +"|"+ timestamp +"|"+selectedDocument.getTags());	   

   }
   JasperPrint jasperPrint;
   
   public void initReport() throws JRException{  
	   SearchReport searchReport = SearchReport.getInstance();
	   Map<String, Object> map = new HashMap<String, Object>();
	   map.put("username", searchReport.getUsername());
	   map.put("keyword", searchReport.getSearchString());
	   map.put("tags", searchReport.getTags());
	   map.put("fileTypes", searchReport.getFileTypes());
	   map.put("startDate", searchReport.getStartDate());
	   map.put("endDate", searchReport.getEndDate());
	   
	   map.put("totalRecords", searchReport.getTotalRecords());
	   
//	   List<SearchReport> searchReportList = new ArrayList<SearchReport>();
//	   searchReportList.add(searchReport);
	   for(SearchDocument doc: searchReport.getSearchResults())
		   System.out.println(doc.getDocumentPath());
       JRBeanCollectionDataSource beanCollectionDataSource=new JRBeanCollectionDataSource(searchReport.getSearchResults());  
       String  reportPath=  javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/Search_Report.jasper"); 
       jasperPrint=JasperFillManager.fillReport(reportPath, map,beanCollectionDataSource);  
   }
   
   public void printPDF() throws JRException, IOException{  
       initReport(); 
       Date date = VCSUtil.getCurrentDateTime();
		//       timestamp = new Timestamp(date.getTime());
	   	SimpleDateFormat smf = new SimpleDateFormat("yyyyMMddHHmmss");
	   	String timestamp = smf.format(date);
	   	String filename = timestamp+"_"+getHttpRequest().getSession().getAttribute(VCSConstants.SESSION_USER_NAME)+".pdf";
       getHttpResponse().addHeader("Content-disposition", "attachment; filename=\"" + filename + "\"");  
       ServletOutputStream servletOutputStream=getHttpResponse().getOutputStream();  
       JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);  
       javax.faces.context.FacesContext.getCurrentInstance().responseComplete();
         
         
   }  
   
   public String processSearchFolderPath(String searchPath){
		String orignalPath=searchPath;
		searchPath=searchPath.replaceAll("\\\\", "/");
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		String companyName = ru.getCodeValue("companyName", SystemCodeType.GENERAL);
		if(searchPath.startsWith("//")){
			searchPath=searchPath.substring(2,searchPath.length());
			searchPath="/"+companyName+"/"+"FS"+"/"+searchPath;
			if(fileExplorerService.getNodeByPath(searchPath)==null){
				searchPath="/"+companyName+"/"+"SP"+"/"+searchPath;
			}
		}
		if(searchPath.endsWith("/")){
			searchPath = searchPath.substring(0, searchPath.length()-1);
		}
		
		Pattern pathPattern=Pattern.compile("(^[a-zA-Z]:)", Pattern.UNICODE_CASE);
		
		Matcher matcher=pathPattern.matcher(searchPath);
		if(matcher.find()){
			System.out.print("***** Its a drive mapping path:*****");
	
   		String searchPathDriveLetter=searchPath.substring(0,2);
   		DriveLettersService driveLetterService=serviceManager.getDriveLettersService();
   		List<DriveLetters> driveLetterList=driveLetterService.getAll();
   	    String drivePath;
   	    DriveLetters driveLetter=null;
   	    for(int i=0;driveLetterList!=null && i<driveLetterList.size();i++){
   	    	driveLetter=driveLetterList.get(i);
   	    	if(searchPathDriveLetter.equalsIgnoreCase(driveLetter.getDriveLetter())){break;}
   	    }
   	    
   		if(driveLetter!=null){
   			System.out.println("***** Drive mapping found.....");
   	    	drivePath=driveLetter.getSharePath();    	    	
			if(drivePath.startsWith("\\\\")){	
				drivePath ="/"+ ru.getCodeValue("companyName", SystemCodeType.GENERAL) + "/FS/" + drivePath.substring(2, drivePath.lastIndexOf("\\")).replaceAll("\\\\", "/");
			}
   	    	searchPath=drivePath+"/"+searchPath.substring(2);
   	    	searchPath=searchPath.replaceAll("//", "/");
   	    	if(!searchPath.startsWith("/")){
   	    		searchPath = "/" + searchPath;   	    		
   	    	}
   	    	if(searchPath.endsWith("/")){
   	    		searchPath = searchPath.substring(0, searchPath.length()-1);   	    		
   	    	}
   	    }else{
//   	    	System.out.println("***** No drive mapping found.....");
//   	    	this.firstLevelFiles=null;
//   	    	return;
   	    }
   	}		
   	
   	System.out.println("Search Path formatted:-----> " + searchPath);
   	return searchPath;
   }
   
   public String processSearchFolderPathFS(String searchPath){
	   System.out.print("***** Its a drive mapping path:*****");
		
  		String searchPathDriveLetter=searchPath.substring(0,2);
  		DriveLettersService driveLetterService=serviceManager.getDriveLettersService();
  		List<DriveLetters> driveLetterList=driveLetterService.getAll();
  	    String drivePath;
  	    DriveLetters driveLetter=null;
  	    for(int i=0;driveLetterList!=null && i<driveLetterList.size();i++){
  	    	driveLetter=driveLetterList.get(i);
  	    	if(searchPathDriveLetter.equalsIgnoreCase(driveLetter.getDriveLetter())){break;}
  	    }
  	    
  		if(driveLetter!=null){
  			System.out.println("***** Drive mapping found.....");
  	    	drivePath=driveLetter.getSharePath();    	    	
			if(drivePath.startsWith("\\\\")){	
//				drivePath ="/"+ ru.getCodeValue("companyName", SystemCodeType.GENERAL) + "/FS/" + drivePath.substring(2, drivePath.lastIndexOf("\\")).replaceAll("\\\\", "/");
			}
			if(drivePath.endsWith("\\"))
				drivePath = drivePath.substring(0, drivePath.length()-1);
			if(searchPath.length()>3)				
				searchPath=drivePath+"\\"+searchPath.substring(3);
			else
				searchPath = drivePath;
//  	    	searchPath=searchPath.replaceAll("//", "/");
//  	    	if(!searchPath.startsWith("/")){
//  	    		searchPath = "/" + searchPath;   	    		
//  	    	}
  	    	if(searchPath.endsWith("/")){
  	    		searchPath = searchPath.substring(0, searchPath.length()-1);   	    		
  	    	}
  	    }else{
//  	    	System.out.println("***** No drive mapping found.....");
//  	    	this.firstLevelFiles=null;
//  	    	return;
  	    }
	   return searchPath;
   }
   
   public void saveExportPriv(){
		System.out.println("------------------"+exportNodePath);
		
		if(exportNodePath==null || exportNodePath.trim().length()<1){
			addErrorMessage("Please select files to export", "Please select files to export");
			return;
		}

		if(remoteUNC==null || remoteUNC.trim().equals("")){
			addCompErrorMessage("exportPath","UNC cannot be empty", "UNC cannot be empty");
			return;
		}
		
//		ru  =   ResourcesUtil.getResourcesUtil();
		
		// as per instructions by Sir Mazhar system code values will be used for export domain, username and password
		
		Policy policy = null;
		List<Policy> policyList = policyService.getPolicyByName("DefaultPolicy");
		if(policyList!=null && policyList.size()>0){
			policy = policyList.get(0);
		}
//		policy = policyService.getPolicyByName("DefaultPolicy");
		if(policy == null){
			addErrorMessage("'Default policy' for export does not exist", "'Default policy' for export does not exist");
			return;
		}
		
		if(preserveFilePaths==true){
			exportDuplicates = false;	//set false forcibly
		}
		System.out.println(policy.getName());
//
//		Date date = VCSUtil.getCurrentDateTime();
//		Timestamp timestamp = new Timestamp(date.getTime());
//		job.setExecTimeStart(timestamp);
		job.setPolicy(policy);
//		getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
//	//	job.setName("("+getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME)+")"+"-Priv-export-job-"+System.currentTimeMillis());//for unique name every time
//       
//       job.setProcessCurrentPublishedVersion(false);
//       job.setProcessLegacyPublishedVersion(false);
//       job.setProcessLegacyDraftVersion(false);
//       job.setProcessReadOnly(false);
//       job.setProcessArchive(false);
//       job.setProcessHidden(false);
//       
//       job.setReadyToExecute(true);
//       job.setActive(1);
//       job.setExecutionInterval(1d);
//       job.setActionType("EXPORTSEARCHED");
//       Set<JobSpDocLib> setJobSpDocLibs = new HashSet<JobSpDocLib>();
//		searchResults.toString();
//		System.out.println(selectAll);
		String decodeStr="";
		try {
			decodeStr = URLDecoder.decode(exportNodePath, "UTF-8");
			System.out.println(decodeStr);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String[] paths = decodeStr.split("[|]");
		List<String> exportPathList = new ArrayList<String>();
		if(!selectAll){
			System.out.println("-------------Setting up export job----------------------");

			
			for(int i =0; i< paths.length;i++){
				//System.out.println(paths[i]+"---------");
				if(!(paths[i].trim().equals(""))){
					exportPathList.add(paths[i]);	
				}			
			}
		}else{
			List<SearchDocument> searchResults=searchService.searchForReport(searchQuery);
			for(SearchDocument document:searchResults){
				exportPathList.add(document.getDocumentPath());
			}
		}
		long limit = 0l;
		String exportLimit = ru.getCodeValue("MAX_RESULTS_TO_EXPORT", SystemCodeType.GENERAL);
		if(exportLimit!=null && exportLimit.trim().length()>0){
			try{
				limit = Long.parseLong(exportLimit);
			
				if(limit>0 && exportPathList.size()>limit){
					addErrorMessage("Selected documents exceeding maximum limit [" + limit +"]", "Number of docs exceeding limits");
					return;
				}
			}catch(java.lang.NumberFormatException ex){
				System.out.println("Invalid value for limit.");
				limit = 0l;
			}
		}
		/*
//		limit = 2l;
		long fileCount = 0l;

			for(String exportPath:exportPathList){
				JobSpDocLib jobSpDocLib = new JobSpDocLib();        	
//				String[] elements = StringUtils.getPathElements(exportPath);
//				String path ="";
//				for(int i = 2; i <elements.length;i++ ){
//					path = path + elements[i];
//					if(i < elements.length-1){
//						path+="/";
//					}
//				}
				//only upto maximum limit
//				if(limit>0 && fileCount>=limit){
//					break;
//				}
	    	
	        	String smbPath = "smb://"+ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL) +";"+ru.getCodeValue("restore.username", SystemCodeType.GENERAL)+":"+ru.getCodeValue("restore.password", SystemCodeType.GENERAL)+"@"
	        					   + remoteUNC.substring(2).replaceAll("\\\\", "/");
	        	if(job.isPreserveFilePaths()){
	        		if(!(smbPath.endsWith("/"))){
	        			smbPath+="/";
	        		}
	        	
	        		String[] elements = StringUtils.getPathElements(exportPath);
	        		
	        		for(int i = 4; i <elements.length-1; i++){	        			
	        			smbPath+=elements[i]+"/";
	        		}
	        		
	        	}
				System.out.println("-------"+smbPath+"-----");
				jobSpDocLib.setRecursive(false);
				jobSpDocLib.setName(exportPath);
				jobSpDocLib.setGuid(null);
	//			if(job.getAgent().getType().trim().equals("SP")){
	//				jobSpDocLib.setType("SP");
	//				jobSpDocLib.setDestinationPath(path);
	//			}else{
					jobSpDocLib.setType("CIFS");
					jobSpDocLib.setDestinationPath(smbPath);
	//			}
				
				jobSpDocLib.setJobSpDocLibExcludedPaths(null);
				setJobSpDocLibs.add(jobSpDocLib);
				jobSpDocLib.setJob(job);
				fileCount = fileCount +1;
			}*/
		
		List<Agent> agentList = agentService.getAgentByName(ru.getCodeValue("AgentName", SystemCodeType.ZUES_AGENT));
		if(agentList != null && agentList.size()>0){
			job.setAgent(agentList.get(0));
		}
//       job.setJobSpDocLibs(new HashSet());
//       job.getJobSpDocLibs().addAll(setJobSpDocLibs);
       try{
       jobService.saveExportJobPriv(job, exportPathList, remoteUNC);
       }catch(Exception ex){
       	ex.printStackTrace();
       	addErrorMessage("internal error, could not create export job", "internal error");
       }
//       addInfoMessage("Export job created successfully", "success");
	}
	
	public void readyToExport(){
		
		int fileCount = 0;
		long totalFileSize = 0l;
		String decodeStr="";
		try {
			decodeStr = URLDecoder.decode(exportNodePath, "UTF-8");
			System.out.println(decodeStr);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		
		List<String> exportPathList = new ArrayList<String>();
	
		if(!selectAll){
			String[] paths = decodeStr.split("[|]");		
			System.out.println("-------------Setting up export job----------------------");

			
			for(int i =0; i< paths.length;i++){
				//System.out.println(paths[i]+"---------");
				
				if(!(paths[i].trim().equals(""))){
					for(SearchDocument document:searchResults){
						if(document.getRepoPath().equals(paths[i])){
							fileCount=fileCount+1;
							totalFileSize = totalFileSize + Long.parseLong(document.getDocumentSize());
							break;
						}
					}
					exportPathList.add(paths[i]);
				}			
			}
		}else{
			  RowIterator rows = searchService.searchForExport(searchQuery);			  
			  while(rows.hasNext()){
				  try {
					  javax.jcr.Node node = rows.nextRow().getNode();
					  String size = null;
					  exportPathList.add(node.getParent().getPath());
					  fileCount=fileCount+1;
					  if(node.hasProperty("jcr:data")){
		                   double length = node.getProperty("jcr:data").getLength();
		                   size = String.valueOf(Math.round(Math.ceil(length / 1000d))) ;
		                   totalFileSize = totalFileSize + Long.parseLong(size);
					  }					  
					  
//					System.out.println("--"+node.getParent().getPath());
				} catch (RepositoryException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			  }
//			  System.out.println("-------------"+rows.getSize());	
//			for(SearchDocument document:searchResults){
//				exportPathList.add(document.getDocumentPath());
//				fileCount=fileCount+1;
//				totalFileSize = totalFileSize + Long.parseLong(document.getDocumentSize());
//			}
			
			
		/*	JobSpDocLib jobSpDocLib = new JobSpDocLib();        	
				String[] elements = StringUtils.getPathElements(document.getDocumentPath());
				String path ="";
				for(int i = 2; i <elements.length;i++ ){
					path = path + elements[i];
					if(i < elements.length-1){
						path+="/";
					}
				}
       	
	        	String smbPath = "smb://"+getRemoteDomainName() +";"+getRemoteUserName()+":"+getRemoteUserPassword()+"@"
	        					   + path;
				System.out.println("-------"+smbPath+"-----");
				jobSpDocLib.setRecursive(false);
				jobSpDocLib.setName(path.substring(path.lastIndexOf("/")));
				jobSpDocLib.setGuid(null);
//				if(job.getAgent().getType().trim().equals("SP")){
//					jobSpDocLib.setType("SP");
//					jobSpDocLib.setDestinationPath(path);
//				}else{
					jobSpDocLib.setType("CIFS");
					jobSpDocLib.setDestinationPath(smbPath);
//				}
				
				jobSpDocLib.setJobSpDocLibExcludedPaths(null);
				setJobSpDocLibs.add(jobSpDocLib);
				jobSpDocLib.setJob(job);
			}*/
		}
		job.setName("("+getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME)+")"+"-Priv-export-job-"+System.currentTimeMillis());//for unique name every time
		setTotalFileCount(fileCount+"");
		
		//add as KBs
		setTotalFileSize(totalFileSize+" KB");
		//convert to MBs
		if(totalFileSize>1024l){
			totalFileSize = totalFileSize/1024l;
			setTotalFileSize(totalFileSize+" MB");
		}
		//convert to GBs
		if(totalFileSize>1024l){
			totalFileSize = totalFileSize/1024l;
			setTotalFileSize(totalFileSize+ " GB");
		}
		

	}
   
	public void folderSearch(){
		String selectedNodePath=getRequestParameter("selectedNodePath");

		targetFolder = selectedNodePath;
	}
   
	public void clear(){
		
		searchString=null;
		documentName=null;
		modifiedStartDate=null;
		modifiedEndDate=null;
		searchResults.clear();
		executionTime=0;
		totalRecords=0;
		totalPages=1;
		searchFileController.setSelectedNode(null);
		currentPage=1;
		curPagesList.clear();
		targetFolder=null;
		documentTags=null;
		contentType=null;
		archivedStartDate=null;
		archivedEndDate=null;
		createdStartDate=null;
		createdEndDate=null;
		totalRecordsCifs=null;
		searchResultsCifs.clear();
		totalRecordsCifs=0;
		totalPagesCifs=1;
	}
	
	

	public String getLoggedInRole(){
		   String roleName=(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_ROLE_NAME);
		   return roleName.toUpperCase();
    }
	private String getUserRole(){
		 String loggedInRole=getLoggedInRole();
		 if(loggedInRole.equalsIgnoreCase("ADMINISTRATOR")){
			 return loggedInRole;
		 }else{
			 String authMode=(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE);
			  if(authMode.equalsIgnoreCase("ad")){
				  return "LDAP";
			  }else{
				  return "PRIV";
			  }				  
		 }
		
	 }
	
	private void activeDeActivateUserControls(){
	    RoleFeatureUtil rfUtil=RoleFeatureUtil.getRoleFeatureUtil();   
	    RoleFeature roleFeature;
    	String userRole=getUserRole();
      
    	if(userRole.equalsIgnoreCase("PRIV")){
    		 
    		//enableDocumentTagging showTagsInSearchResults EnableFolderSearch ContentTypeSearchEnabled
	   		 roleFeature=rfUtil.getFeature("document_tagging");
	   		 this.enableDocumentTagging=roleFeature!=null ?roleFeature.getPrivAllowed():false;
	   	    
	   		 roleFeature=rfUtil.getFeature("show_tags_sp");
	   		 this.showTagsinSearchResults=roleFeature!=null ?roleFeature.getPrivAllowed():false;
	   		
	   		 roleFeature=rfUtil.getFeature("path_based_search_sp");
	   		 this.enableFolderSearch=roleFeature!=null ?roleFeature.getPrivAllowed():false;
	   		
	   		 roleFeature=rfUtil.getFeature("content_type_search_sp");
	   		 this.contentTypeSearchEnabled=roleFeature!=null ?roleFeature.getPrivAllowed():false;
	   		 
	   		 roleFeature=rfUtil.getFeature("archive_date_search_sp");
	   		 this.archiveDateSearchEnabled=roleFeature!=null ?roleFeature.getPrivAllowed():false;
	   		 
	   		 roleFeature=rfUtil.getFeature("search_by_id_sp");
	   		 this.enableSearchById=roleFeature!=null ?roleFeature.getPrivAllowed():false;
	   		 
	     	VirtualcodeUtil util = VirtualcodeUtil.getInstance();
	    	if(util.getComplianceMode())
	    		enableUserFeatures = true;
	   		
	   		 
    	}else if(userRole.equalsIgnoreCase("LDAP")){
    		
    	
    		 roleFeature=rfUtil.getFeature("document_tagging");
	   		 this.enableDocumentTagging=roleFeature!=null ?roleFeature.getLdapAllowed():false;
	   	    
	   		 roleFeature=rfUtil.getFeature("show_tags_sp");
	   		 this.showTagsinSearchResults=roleFeature!=null ?roleFeature.getLdapAllowed():false;
	   		
	   		 roleFeature=rfUtil.getFeature("path_based_search_sp");
	   		 this.enableFolderSearch=roleFeature!=null ?roleFeature.getLdapAllowed():false;
	   		
	   		 roleFeature=rfUtil.getFeature("content_type_search_sp");
	   		 this.contentTypeSearchEnabled=roleFeature!=null ?roleFeature.getLdapAllowed():false;
	   		 
	   		 roleFeature=rfUtil.getFeature("archive_date_search_sp");
	   		 this.archiveDateSearchEnabled=roleFeature!=null ?roleFeature.getLdapAllowed():false;
	   		 
	   		 roleFeature=rfUtil.getFeature("search_by_id_sp");
	   		 this.enableSearchById=roleFeature!=null ?roleFeature.getLdapAllowed():false;
	   		 
	   		enableUserFeatures = false;
	   		 
    	}else{
      
    		 this.enableDocumentTagging=false;
    		 this.showTagsinSearchResults=false;
    		 this.enableFolderSearch=false;
    		 this.contentTypeSearchEnabled=false;
    		 this.archiveDateSearchEnabled=false;
    	}
    	
    	ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
    	String enableCifsSearchStr = ru.getCodeValue("enable_cifs_search", SystemCodeType.GENERAL); 
    	enableCifsSearch = (enableCifsSearchStr!=null && enableCifsSearchStr.equalsIgnoreCase("yes"))?true:false;
    	
    	String enableRepoSearchStr = ru.getCodeValue("enable_repo_search", SystemCodeType.GENERAL); 
    	enableRepoSearch = (enableRepoSearchStr!=null && enableRepoSearchStr.equalsIgnoreCase("yes"))?true:false;
    	
    	com.virtualcode.pdfviewer.flexpaper.Config config = new com.virtualcode.pdfviewer.flexpaper.Config();
    	if(!"true".equals(config.getConfig("splitmode")))
    			pdfViewerUrl = "common/simple_document.jsp?doc";
    	else
    			pdfViewerUrl = "common/split_document.jsp?doc";
	 
    }


	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}
	
	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public Date getModifiedStartDate() {
		return modifiedStartDate;
	}

	public void setModifiedStartDate(Date modifiedStartDate) {
		this.modifiedStartDate = modifiedStartDate;
	}

	public Date getModifiedEndDate() {
		return modifiedEndDate;
	}

	public void setModifiedEndDate(Date modifiedEndDate) {
		this.modifiedEndDate = modifiedEndDate;
	}


	public Integer getTotalRecords() {
		return totalRecords;
	}


	public void setTotalRecords(Integer totalRecords) {
		this.totalRecords = totalRecords;
	}


	public List<SearchDocument> getSearchResults() {
		return searchResults;
	}


	public void setSearchResults(List<SearchDocument> searchResults) {
		this.searchResults = searchResults;
	}

	public long getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(long executionTime) {
		this.executionTime = executionTime;
	}


	public float getItemsPerPage() {
		return itemsPerPage;
	}


	public void setItemsPerPage(float itemsPerPage) {
		this.itemsPerPage = (int)itemsPerPage;
	}


	public int getTotalPages() {
		return totalPages;
	}


	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public List<String> getCurPagesList() {
		return curPagesList;
	}

	public void setCurPagesList(List<String> curPagesList) {
		this.curPagesList = curPagesList;
	}

	public String getSortMode() {
		return sortMode;
	}

	public void setSortMode(String sortMode) {
		this.sortMode = sortMode;
	}

	public String getSortOption() {
		return sortOption;
	}

	public void setSortOption(String sortOption) {
		this.sortOption = sortOption;
	}

	public JobController getJobController() {
		return jobController;
	}

	public void setJobController(JobController jobController) {
		this.jobController = jobController;
	}

	public boolean isEnablePrivExport() {
		return enablePrivExport;
	}

	public void setEnablePrivExport(boolean enablePrivExport) {
		this.enablePrivExport = enablePrivExport;
	}

	public boolean isEnablePrivReport() {
		return enablePrivReport;
	}

	public void setEnablePrivReport(boolean enablePrivReport) {
		this.enablePrivReport = enablePrivReport;
	}

	public String getDocumentTags() {
		return documentTags;
	}

	public void setDocumentTags(String documentTags) {
		this.documentTags = documentTags;
	}

	public SearchDocument getSelectedDocument() {
		return selectedDocument;
	}

	public void setSelectedDocument(SearchDocument selectedDocument) {
		this.selectedDocument = selectedDocument;
	}

	public boolean isEnableDocumentTagging() {
		return enableDocumentTagging;
	}

	public void setEnableDocumentTagging(boolean enableDocumentTagging) {
		this.enableDocumentTagging = enableDocumentTagging;
	}

	public boolean isShowTagsinSearchResults() {
		return showTagsinSearchResults;
	}

	public void setShowTagsinSearchResults(boolean showTagsinSearchResults) {
		this.showTagsinSearchResults = showTagsinSearchResults;
	}

	public String getTargetFolder() {
		return targetFolder;
	}

	public void setTargetFolder(String targetFolder) {
		this.targetFolder = targetFolder;
	}

	public boolean isEnableFolderSearch() {
		return enableFolderSearch;
	}

	public void setEnableFolderSearch(boolean enableFolderSearch) {
		this.enableFolderSearch = enableFolderSearch;
	}

	public boolean isContentTypeSearchEnabled() {
		return contentTypeSearchEnabled;
	}

	public void setContentTypeSearchEnabled(boolean contentTypeSearchEnabled) {
		this.contentTypeSearchEnabled = contentTypeSearchEnabled;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public String getTotalFileCount() {
		return totalFileCount;
	}

	public void setTotalFileCount(String totalFileCount) {
		this.totalFileCount = totalFileCount;
	}

	public String getTotalFileSize() {
		return totalFileSize;
	}

	public void setTotalFileSize(String totalFileSize) {
		this.totalFileSize = totalFileSize;
	}

	public String getRemoteDomainName() {
		return remoteDomainName;
	}

	public void setRemoteDomainName(String remoteDomainName) {
		this.remoteDomainName = remoteDomainName;
	}

	public String getRemoteUserName() {
		return remoteUserName;
	}

	public void setRemoteUserName(String remoteUserName) {
		this.remoteUserName = remoteUserName;
	}

	public String getRemoteUserPassword() {
		return remoteUserPassword;
	}

	public void setRemoteUserPassword(String remoteUserPassword) {
		this.remoteUserPassword = remoteUserPassword;
	}

	public String getRemoteUNC() {
		return remoteUNC;
	}

	public void setRemoteUNC(String remoteUNC) {
		this.remoteUNC = remoteUNC;
	}

	public String getExportNodePath() {
		return exportNodePath;
	}

	public void setExportNodePath(String exportNodePath) {
		this.exportNodePath = exportNodePath;
	}

	public boolean isSelectAll() {
		return selectAll;
	}

	public void setSelectAll(boolean selectAll) {
		this.selectAll = selectAll;
	}

	public boolean isPreserveFilePaths() {
		return preserveFilePaths;
	}

	public void setPreserveFilePaths(boolean preserveFilePaths) {
		this.preserveFilePaths = preserveFilePaths;
	}

	public boolean isExportDuplicates() {
		return exportDuplicates;
	}

	public void setExportDuplicates(boolean exportDuplicates) {
		this.exportDuplicates = exportDuplicates;
	}

	public String getExcludedDocumentName() {
		return excludedDocumentName;
	}

	public void setExcludedDocumentName(String excludedDocumentName) {
		this.excludedDocumentName = excludedDocumentName;
	}

	public SearchFileController getSearchFileController() {
		return searchFileController;
	}

	public void setSearchFileController(SearchFileController searchFileController) {
		this.searchFileController = searchFileController;
	}

	public String getSearchQuery() {
		return searchQuery;
	}

	public void setSearchQuery(String searchQuery) {
		this.searchQuery = searchQuery;
	}

	public boolean isShowDocumentPath() {
		return showDocumentPath;
	}

	public void setShowDocumentPath(boolean showDocumentPath) {
		this.showDocumentPath = showDocumentPath;
	}

	public Long getFileSizeStart() {
		return fileSizeStart;
	}

	public void setFileSizeStart(Long fileSizeStart) {
		this.fileSizeStart = fileSizeStart;
	}

	public Long getFileSizeEnd() {
		return fileSizeEnd;
	}

	public void setFileSizeEnd(Long fileSizeEnd) {
		this.fileSizeEnd = fileSizeEnd;
	}

	public String getUnitForFileSizeStart() {
		return unitForFileSizeStart;
	}

	public void setUnitForFileSizeStart(String unitForFileSizeStart) {
		this.unitForFileSizeStart = unitForFileSizeStart;
	}

	public String getUnitForFileSizeEnd() {
		return unitForFileSizeEnd;
	}

	public void setUnitForFileSizeEnd(String unitForFileSizeEnd) {
		this.unitForFileSizeEnd = unitForFileSizeEnd;
	}

	public Date getArchivedStartDate() {
		return archivedStartDate;
	}

	public void setArchivedStartDate(Date archivedStartDate) {
		this.archivedStartDate = archivedStartDate;
	}

	public Date getArchivedEndDate() {
		return archivedEndDate;
	}

	public void setArchivedEndDate(Date archivedEndDate) {
		this.archivedEndDate = archivedEndDate;
	}

	public boolean isArchiveDateSearchEnabled() {
		return archiveDateSearchEnabled;
	}

	public void setArchiveDateSearchEnabled(boolean archiveDateSearchEnabled) {
		this.archiveDateSearchEnabled = archiveDateSearchEnabled;
	}

	public Long getModifiedStartDateLong() {
		return modifiedStartDateLong;
	}

	public void setModifiedStartDateLong(Long modifiedStartDateLong) {
		this.modifiedStartDateLong = modifiedStartDateLong;
	}

	public Long getModifiedEndDateLong() {
		return modifiedEndDateLong;
	}

	public void setModifiedEndDateLong(Long modifiedEndDateLong) {
		this.modifiedEndDateLong = modifiedEndDateLong;
	}

	public Long getArchivedEndDateLong() {
		return archivedEndDateLong;
	}

	public void setArchivedEndDateLong(Long archivedEndDateLong) {
		this.archivedEndDateLong = archivedEndDateLong;
	}

	public Long getArchivedStartDateLong() {
		return archivedStartDateLong;
	}

	public void setArchivedStartDateLong(Long archivedStartDateLong) {
		this.archivedStartDateLong = archivedStartDateLong;
	}
	
	public boolean isEnableSizeRangeSelector() {
		return enableSizeRangeSelector;
	}

	public void setEnableSizeRangeSelector(boolean enableSizeRangeSelector) {
		this.enableSizeRangeSelector = enableSizeRangeSelector;
	}

	public boolean isEnableModDateRangeSlider() {
		return enableModDateRangeSlider;
	}

	public void setEnableModDateRangeSlider(boolean enableModDateRangeSlider) {
		this.enableModDateRangeSlider = enableModDateRangeSlider;
	}

	public boolean isEnableArcDateRangeSlider() {
		return enableArcDateRangeSlider;
	}

	public void setEnableArcDateRangeSlider(boolean enableArcDateRangeSlider) {
		this.enableArcDateRangeSlider = enableArcDateRangeSlider;
	}

	public String getDocumentUUid() {
		return documentUUid;
	}

	public void setDocumentUUid(String documentUUid) {
		this.documentUUid = documentUUid;
	}

	public boolean isEnableSearchById() {
		return enableSearchById;
	}

	public void setEnableSearchById(boolean enableSearchById) {
		this.enableSearchById = enableSearchById;
	}

	public boolean isEnableCifsSearch() {
		return enableCifsSearch;
	}

	public void setEnableCifsSearch(boolean enableCifsSearch) {
		this.enableCifsSearch = enableCifsSearch;
	}

	public Date getCreatedStartDate() {
		return createdStartDate;
	}

	public void setCreatedStartDate(Date createdStartDate) {
		this.createdStartDate = createdStartDate;
	}

	public Date getCreatedEndDate() {
		return createdEndDate;
	}

	public void setCreatedEndDate(Date createdEndDate) {
		this.createdEndDate = createdEndDate;
	}

	public Long getCreatedStartDateLong() {
		return createdStartDateLong;
	}

	public void setCreatedStartDateLong(Long createdStartDateLong) {
		this.createdStartDateLong = createdStartDateLong;
	}

	public Long getCreatedEndDateLong() {
		return createdEndDateLong;
	}

	public void setCreatedEndDateLong(Long createdEndDateLong) {
		this.createdEndDateLong = createdEndDateLong;
	}

	public String getUncPath() {
		return uncPath;
	}

	public void setUncPath(String uncPath) {
		this.uncPath = uncPath;
	}

	public boolean isEnableRepoSearch() {
		return enableRepoSearch;
	}

	public void setEnableRepoSearch(boolean enableRepoSearch) {
		this.enableRepoSearch = enableRepoSearch;
	}

	public boolean isEnableCrtDateRangeSlider() {
		return enableCrtDateRangeSlider;
	}

	public void setEnableCrtDateRangeSlider(boolean enableCrtDateRangeSlider) {
		this.enableCrtDateRangeSlider = enableCrtDateRangeSlider;
	}

	public List<SearchDocument> getSearchResultsCifs() {
		return searchResultsCifs;
	}

	public void setSearchResultsCifs(List<SearchDocument> searchResultsCifs) {
		this.searchResultsCifs = searchResultsCifs;
	}

	public Integer getTotalRecordsCifs() {
		return totalRecordsCifs;
	}

	public void setTotalRecordsCifs(Integer totalRecordsCifs) {
		this.totalRecordsCifs = totalRecordsCifs;
	}

	public int getCurrentPageCifs() {
		return currentPageCifs;
	}


	public void setCurrentPageCifs(int currentPageCofs) {
		this.currentPageCifs = currentPageCofs;
	}


	public List<String> getCurPagesListCifs() {
		return curPagesListCifs;
	}


	public void setCurPagesListCifs(List<String> curPagesListCifs) {
		this.curPagesListCifs = curPagesListCifs;
	}


	public int getTotalPagesCifs() {
		return totalPagesCifs;
	}


	public void setTotalPagesCifs(int totalPagesCifs) {
		this.totalPagesCifs = totalPagesCifs;
	}


	public int getItemsPerPageCifs() {
		return itemsPerPageCifs;
	}


	public void setItemsPerPageCifs(int itemsPerPageCifs) {
		this.itemsPerPageCifs = itemsPerPageCifs;
	}


	public boolean isSearchCifs() {
		return searchCifs;
	}


	public void setSearchCifs(boolean searchCifs) {
		this.searchCifs = searchCifs;
	}


	public boolean isSearchArchive() {
		return searchArchive;
	}


	public void setSearchArchive(boolean searchArchive) {
		this.searchArchive = searchArchive;
	}


	public String getRepoTabTitle() {
		return repoTabTitle;
	}


	public void setRepoTabTitle(String repoTabTitle) {
		this.repoTabTitle = repoTabTitle;
	}


	public String getCifsTabTitle() {
		return cifsTabTitle;
	}


	public void setCifsTabTitle(String cifsTabTitle) {
		this.cifsTabTitle = cifsTabTitle;
	}


	public String getSortOptionCifs() {
		return sortOptionCifs;
	}


	public void setSortOptionCifs(String sortOptionCifs) {
		this.sortOptionCifs = sortOptionCifs;
	}


	public boolean isEnableUserFeatures() {
		return enableUserFeatures;
	}


	public void setEnableUserFeatures(boolean enableUserFeatures) {
		this.enableUserFeatures = enableUserFeatures;
	}


	public boolean isSplitmodeEnabled() {
		return splitmodeEnabled;
	}


	public void setSplitmodeEnabled(boolean splitmodeEnabled) {
		this.splitmodeEnabled = splitmodeEnabled;
	}


	public String getPdfViewerUrl() {
		return pdfViewerUrl;
	}


	public void setPdfViewerUrl(String pdfViewerUrl) {
		this.pdfViewerUrl = pdfViewerUrl;
	}

	
}
