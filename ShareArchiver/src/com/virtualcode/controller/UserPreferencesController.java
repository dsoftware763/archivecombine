package com.virtualcode.controller;

import java.io.IOException;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import jespa.security.SecurityProviderException;

import com.virtualcode.common.VCSConstants;
import com.virtualcode.util.JespaUtil;

@ManagedBean(name="userPreferenceController")
@ViewScoped

public class UserPreferencesController extends AbstractController {
	private String username;
	private List<String> userGroups;
	
	public void init(){
		username = (String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
		JespaUtil jespaUtil = JespaUtil.getInstance();
		userGroups = userPreferenceService.getGroupsForUser(username);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<String> getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(List<String> userGroups) {
		this.userGroups = userGroups;
	}
}
