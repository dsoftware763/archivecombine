package com.virtualcode.controller;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.component.visit.VisitCallback;
import javax.faces.component.visit.VisitContext;
import javax.faces.component.visit.VisitResult;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.jsf.FacesContextUtils;

import com.sun.faces.component.visit.FullVisitContext;
import com.virtualcode.repository.AnalysisService;
import com.virtualcode.repository.ArchiveDateRunnerService;
import com.virtualcode.repository.FileExplorerService;
import com.virtualcode.repository.FileSizeRunnerService;
import com.virtualcode.repository.SearchService;
import com.virtualcode.repository.UserActivityService;
import com.virtualcode.services.AgentService;
import com.virtualcode.services.DelJobFiledetailsService;
import com.virtualcode.services.DelJobService;
import com.virtualcode.services.DelPolicyService;
import com.virtualcode.services.DataGuardianService;
import com.virtualcode.services.DocumentClassifyService;
import com.virtualcode.services.DriveLettersService;

import com.virtualcode.services.ErrorCodeService;
import com.virtualcode.services.JobService;
import com.virtualcode.services.JobStatisticsService;
import com.virtualcode.services.JobStatusService;
import com.virtualcode.services.LdapGroupsService;
import com.virtualcode.services.LinkAccessDocumentsService;
import com.virtualcode.services.LinkAccessUsersService;
import com.virtualcode.services.MailService;
import com.virtualcode.services.NodePermissionJobService;
import com.virtualcode.services.PolicyService;
import com.virtualcode.services.RoleFeatureService;
import com.virtualcode.services.RolePermissionsService;
import com.virtualcode.services.RoleService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.services.ShareAnalysisStatsService;
import com.virtualcode.services.SharePointService;
import com.virtualcode.services.SystemAlertsService;
import com.virtualcode.services.SystemCodeService;
import com.virtualcode.services.UserPermissionsService;
import com.virtualcode.services.UserPreferencesService;
import com.virtualcode.services.UserService;

public class AbstractController implements Serializable {
	
	public AbstractController(){
		 injectServices();
		 System.out.println("AbstractController Instantiated...");
	}
	
   public HttpSession getHttpSession(){
		
		HttpServletRequest request=(HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		return request.getSession();
	}
	
	
	public HttpServletRequest getHttpRequest(){
		
		HttpServletRequest request=(HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		return request;
	}
	
	
    public HttpServletResponse getHttpResponse(){
		
		HttpServletResponse response=(HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();
		return response;
	}
    
    public WebApplicationContext getWebAppContext(){
    	WebApplicationContext webAppContext=FacesContextUtils.getRequiredWebApplicationContext(FacesContext.getCurrentInstance());
    	return webAppContext;
    }
    
    public String getRequestParameter(String param){
    	
    	return getHttpRequest().getParameter(param);
    }
    
   public void addErrorMessage(String message,String detail){
    	FacesMessage fmessage=new FacesMessage();
    	fmessage.setSummary(message);
    	fmessage.setDetail(detail);
    	
    	fmessage.setSeverity(FacesMessage.SEVERITY_ERROR);
    	
    	FacesContext.getCurrentInstance().addMessage(message, fmessage);
    }
   
   public void addCompErrorMessage(String componentId,String message,String detail){
	   	FacesMessage fmessage=new FacesMessage();
	   	fmessage.setSummary(message);
	   	fmessage.setDetail(detail);
	   	
	   	fmessage.setSeverity(FacesMessage.SEVERITY_ERROR);
	   	
	   	FacesContext.getCurrentInstance().addMessage(componentId,fmessage);
   }
  
   public void addCompInfoMessage(String componentId,String message,String detail){
	   	FacesMessage fmessage=new FacesMessage();
	   	fmessage.setSummary(message);
	   	fmessage.setDetail(detail);
	   	
	   	fmessage.setSeverity(FacesMessage.SEVERITY_INFO);
	   	
	   	FacesContext.getCurrentInstance().addMessage(componentId,fmessage);
   }
   
   
   
   public void addInfoMessage(String message,String detail){
	   	FacesMessage fmessage=new FacesMessage();
	   	fmessage.setSummary(message);
	   	fmessage.setDetail(detail);
	   	fmessage.setSeverity(FacesMessage.SEVERITY_INFO);
	   	
	   	FacesContext.getCurrentInstance().addMessage(message, fmessage);
   }
   
   public String getContextRealPath(){
	   ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	   return  ctx.getRealPath("/");	
   }
   
   public UIComponent findComponent(final String id){
	    FacesContext context = FacesContext.getCurrentInstance(); 
	    UIViewRoot root = context.getViewRoot();
	    final UIComponent[] found = new UIComponent[1];
	    root.visitTree(new FullVisitContext(context), new VisitCallback() {     
	        @Override
	        public VisitResult visit(VisitContext context, UIComponent component) {
	            if(component.getId().equals(id)){
	                found[0] = component;
	                return VisitResult.COMPLETE;
	            }
	            return VisitResult.ACCEPT;              
	        }
	    });
	    return found[0];

	}
	
	
	
	protected ServiceManager serviceManager;
	protected AgentService agentService;
	protected DocumentClassifyService documentClassifyService;
	protected UserService userService;
	protected RoleService roleService;
	protected UserPermissionsService userPermissionsService;
	protected RolePermissionsService rolePermissionsService;
	
	protected PolicyService policyService;
	protected JobService jobService;
	
	protected SearchService searchService;
	
	protected AnalysisService analysisService;
	
	protected FileExplorerService fileExplorerService;
	
	protected SystemCodeService systemCodeService;
	
	protected JobStatusService jobStatusService;
	
	protected JobStatisticsService jobStatisticsService;
	
	protected NodePermissionJobService nodePermissionJobService;
	
	protected DriveLettersService driveLettersService;
	
	protected SharePointService sharePointService;
	
	protected UserActivityService userActivityService;
	
	protected SystemAlertsService systemAlertsService;
	
	protected ShareAnalysisStatsService shareAnaylysisService;
	
	protected UserPreferencesService userPreferenceService; 
	
	protected MailService mailService;
	
	protected RoleFeatureService roleFeatureService;
	
	protected DelJobService delJobService;
	
	protected DelPolicyService delPolicyService;
	
	protected DelJobFiledetailsService delJobFiledetailsService;
	
	protected FileSizeRunnerService fileSizeRunnerService;
	
	protected LinkAccessUsersService linkAccessUsersService;
	
	protected LinkAccessDocumentsService linkAccessDocumentsService;
		
	
	protected DataGuardianService dataGuardianService;
	
	protected LdapGroupsService ldapGroupsService;
	
	protected ArchiveDateRunnerService archiveDateRunnerService;	
	
	protected ErrorCodeService errorCodeService;
		
	public void injectServices() {
		
		this.serviceManager=(ServiceManager)getWebAppContext().getBean("serviceManager");
		this.agentService=serviceManager.getAgentService();
		this.documentClassifyService=serviceManager.getDocumentClassifyService();
		
		this.userService=serviceManager.getUserService();
		this.roleService=serviceManager.getRoleService();
		this.userPermissionsService=serviceManager.getUserPermissionsService();
		this.rolePermissionsService=serviceManager.getRolePermissionsService();
		
		this.policyService=serviceManager.getPolicyService();
		this.jobService=serviceManager.getJobService();
		
		this.searchService=serviceManager.getSearchService();
		this.analysisService=serviceManager.getAnalysisService();
		this.fileExplorerService=serviceManager.getFileExplorerService();
		
		this.systemCodeService=serviceManager.getSystemCodeService();
		this.jobStatusService=serviceManager.getJobStatusService();
		this.jobStatisticsService=serviceManager.getJobStatisticsService();
		this.nodePermissionJobService=serviceManager.getNodePermissionJobService();
		
		this.driveLettersService=serviceManager.getDriveLettersService();
		
		this.sharePointService = serviceManager.getSharePointService();
		
		this.userActivityService=serviceManager.getUserActivityService();
		
		this.systemAlertsService = serviceManager.getSystemAlertsService();
		this.shareAnaylysisService=serviceManager.getShareAnalysisStatsService();
		
		this.userPreferenceService = serviceManager.getUserPreferenceService();
		
		this.mailService=serviceManager.getMailService();
		this.roleFeatureService=serviceManager.getRoleFeatureService();
		
		this.delJobService	=	serviceManager.getDelJobService();
		this.delPolicyService	=	serviceManager.getDelPolicyService();
		this.delJobFiledetailsService	=	serviceManager.getDelJobFiledetailsService();

		this.fileSizeRunnerService=serviceManager.getFileSizeRunnerService();
		
		this.linkAccessUsersService=serviceManager.getLinkAccessUsersService();
		this.linkAccessDocumentsService=serviceManager.getLinkAccessDocumentsService();
//		this.groupGuardianService = serviceManager.getGroupGuardianService();
		
		this.dataGuardianService =serviceManager.getDataGuardianService();
		this.ldapGroupsService = serviceManager.getLdapGroupsService();
		
		this.archiveDateRunnerService = serviceManager.getArchiveDateRunnerService();
		this.errorCodeService = serviceManager.getErrorCodeService();
	}
	
}
