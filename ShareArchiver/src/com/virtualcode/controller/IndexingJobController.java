package com.virtualcode.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UISelectMany;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.jcr.RepositoryException;
import javax.jcr.query.RowIterator;
import javax.swing.tree.TreeNode;

import jcifs.smb.SmbAuthException;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;


import org.apache.poi.util.SystemOutLogger;
import org.richfaces.component.UITree;
import org.richfaces.component.UITreeNode;
import org.richfaces.event.TreeSelectionChangeEvent;
import org.richfaces.event.TreeToggleEvent;

import com.virtualcode.agent.das.archivePolicy.dto.SPDocLib;
import com.virtualcode.common.SID;

import com.virtualcode.common.VCSConstants;
import com.virtualcode.dao.impl.DelJobDAOImpl;
import com.virtualcode.dao.impl.JobDAOImpl;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.StringUtils;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.Agent;
import com.virtualcode.vo.CustomTreeNode;
import com.virtualcode.vo.DocumentCategory;
import com.virtualcode.vo.DocumentType;
import com.virtualcode.vo.DriveLetters;
import com.virtualcode.vo.ExportJobPath;
import com.virtualcode.vo.HotStatistics;
import com.virtualcode.vo.Job;
import com.virtualcode.vo.JobSpDocLib;
import com.virtualcode.vo.JobSpDocLibExcludedPath;
import com.virtualcode.vo.JobStatistics;
import com.virtualcode.vo.JobStatus;
import com.virtualcode.vo.Policy;
import com.virtualcode.vo.SPTreeNode;
import com.virtualcode.vo.SearchDocument;
import com.virtualcode.vo.SystemCodeType;

@ManagedBean(name="indexingJobController")
@ViewScoped
public class IndexingJobController extends AbstractController {
	
	ResourcesUtil   ru;
	
	 public IndexingJobController(){
		super();	
		job=new Job();
		getAllJobsForStatus();
		ru = ResourcesUtil.getResourcesUtil();
		List<Agent> list= agentService.getAgentByType("FS");
		if(list!=null && list.size()>0){
//			job.setAgent(agent)
			if(list.get(0).getAllowedIps().equals("127.0.0.1")){
				setEnableActiveArchiving(false);
			}else{
				setEnableActiveArchiving(true);
			}
		}
		
		remoteDomainName=ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
		remoteUserName=ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
		remoteUserPassword=ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
		String enableSP = ru.getCodeValue("enable_sharepoint", SystemCodeType.GENERAL);
		enableSharePoint = (enableSP!=null && enableSP.equalsIgnoreCase("yes"))?true:false;
		rootNodes = new ArrayList<TreeNode>();
	 }
	 
	 public void loadAllJobs(){
		 
		 if(!FacesContext.getCurrentInstance().isPostback()){
		   jobs= jobService.getJobByProperty(JobDAOImpl.ACTION_TYPE, "INDEXING");
		 }		 
	 }
	 
	 public void populateJobInfo(){
		 if(jobId==null || jobId.intValue()<0  ||  FacesContext.getCurrentInstance().isPostback()){
			 return;
		 }
		 this.job=jobService.getJobById(jobId);
		 Iterator docLibIterator=job.getJobSpDocLibs().iterator();
		 Iterator docLibExPathIterator=null;
		 JobSpDocLib jobSpDocLib;
		 JobSpDocLibExcludedPath jobSpDocLibExPath;
		 String path;
		 String destinationPath;
		 List<JobSpDocLib> includedLibrariesList = new ArrayList<JobSpDocLib>();
		 currentSpLibs = new HashMap<String, String>();
		 exportNodePath = "";
		 while(docLibIterator!=null && docLibIterator.hasNext()){
			 jobSpDocLib=(JobSpDocLib)docLibIterator.next();
			 jobSpDocLib.setJob(job);
		
			 path=jobSpDocLib.getName();
			 if(path.startsWith("smb://")){
				 setFileSystemType("remote");
				 setRemoteDomainName(VCSUtil.getDomainName(path));
				 setRemoteUserName(VCSUtil.getUserName(path));
				 setRemoteUserPassword(VCSUtil.getUserPassword(path));
				 
				 /*if(job.getActionType().equals(VCSConstants.ACTIVE_ARCHIVING) || job.getActionType().equals(VCSConstants.PRINT_ARCHIVING)){
//					 remotePathsList.add(VCSUtil.getDirPathFromSambaPath(path)+"("+ jobSpDocLib.getDrivePath() +")");
					 if(remotePathsMap==null)
						 remotePathsMap = new HashMap<String, String>();
					 remotePathsMap.put(VCSUtil.getDirPathFromSambaPath(path), jobSpDocLib.getDrivePath());
				 }*/
				 
				 remotePathsList.add(VCSUtil.getDirPathFromSambaPath(path));
				 remoteUNC = VCSUtil.getDirPathFromSambaPath(path);
				 JobSpDocLib tempLib = new JobSpDocLib();
				 tempLib.setName(VCSUtil.getDirPathFromSambaPath(path));
				 tempLib.setExcludedPathList(new ArrayList<String>());
				 tempLib.setId(jobSpDocLib.getId());
				 
				 docLibExPathIterator=jobSpDocLib.getJobSpDocLibExcludedPaths().iterator();
				 while(docLibExPathIterator!=null && docLibExPathIterator.hasNext()){
					 
					 jobSpDocLibExPath=(JobSpDocLibExcludedPath)docLibExPathIterator.next();
					 jobSpDocLibExPath.setJobSpDocLib(jobSpDocLib);
					 remoteExPathsList.add(VCSUtil.getDirPathFromSambaPath(jobSpDocLibExPath.getPath()));
					 tempLib.getExcludedPathList().add(VCSUtil.getDirPathFromSambaPath(jobSpDocLibExPath.getPath()));
				 }	
				 //tempLib.setExcludedPathList(remoteExPathsList);
				 if(jobSpDocLib.getContentTypes()!=null && jobSpDocLib.getContentTypes().length()>0){
					 docTypes = jobSpDocLib.getContentTypes().split(",");
					 indexingType = "2";
				 }else{
					 indexingType = "1";
				 }
				 
				 includedLibrariesList.add(tempLib);

				 
		 }
		 }
	 }
	 
	 
	 private List<Job> jobsStatusList=new ArrayList<Job>();
	 
	 public void getAllJobsForStatus(){
		 jobsStatusList.clear();
		 //jobs= jobService.getAllJobs();		
		 //jobs=jobService.getAllFoStatus();
		 jobs=jobService.getJobByProperty(DelJobDAOImpl.ACTION_TYPE, "INDEXING");
		 for(Job job:this.jobs){
			 /*Job tempJob = new Job();			 
			 if(job.getActionType().equals(VCSConstants.ARCHIVE)){
				 job.setActionType(VCSConstants.ARCHIVE);
			 }else if(job.getActionType().equals(VCSConstants.ARCHIVE_AND_STUB)){
				 job.setActionType(VCSConstants.ARCHIVE_AND_STUB);
			 }else if(job.getActionType().equals(VCSConstants.EXPORT)){
				 job.setActionType(VCSConstants.EXPORT);
			 }else if(job.getActionType().equals(VCSConstants.EXPORT_STUB_ONLY)){
				 job.setActionType(VCSConstants.EXPORT_STUB_ONLY);
			 }*/
			// System.out.println("------------------job status:"+job.getActionType());
			 List<JobStatus> statusList = jobStatusService.getJobStatusByJob(job.getId());			 
			 if(statusList==null||statusList.size()<1){
				 //job.setJobStatus("<font color='orange'>"+VCSConstants.JOB_STATUS_PENDING+"</font>");
				 job.setJobStatus(VCSConstants.JOB_STATUS_PENDING);
			 }else{
				 JobStatus lastJobInstance = statusList.get(0);
				 if(lastJobInstance.getErrorCode().getId()==1){
					 //job.setJobStatus("<font color='red'>"+VCSConstants.JOB_STATUS_FAILED+"</font>");
					 job.setJobStatus(VCSConstants.JOB_STATUS_FAILED);
				 }else if(lastJobInstance.getErrorCode().getId()==0){
					 if(lastJobInstance.getStatus().toString().equals("COMPLETED")){
						 //job.setJobStatus("<font color='green'>"+VCSConstants.JOB_STATUS_COMPLETED+"</font>");
						 job.setJobStatus(VCSConstants.JOB_STATUS_COMPLETED);
					 }else if(lastJobInstance.getStatus().toString().equals("RUNNING")){
						 job.setJobStatus(VCSConstants.JOB_STATUS_PROCESSING);
					 }else if(lastJobInstance.getStatus().toString().equals("EVALUATING")){
						 job.setJobStatus(VCSConstants.JOB_STATUS_PROCESSING);
					 }else if(lastJobInstance.getStatus().equals("CANCEL")){
						 job.setJobStatus(VCSConstants.JOB_STATUS_CANCEL);
					 } else if(lastJobInstance.getStatus().equals("CANCELED")){
						 job.setJobStatus(VCSConstants.JOB_STATUS_CANCELED);
					 }
				 }
			 }

			 jobsStatusList.add(job);
		 }
		 
		
	 }
	 
	 
	 public void getCurrentExecutionDetails(){
		 String id=getRequestParameter("id");
		 String serialNo=getRequestParameter("serial");
		 
		 System.out.println("----------execution details for job id : "+id);
		 List<JobStatistics> jobStatisticsList1 = new ArrayList<JobStatistics>();
		 //if id is -1, this means jobs is processing
		 if(id.equals("-1")){
			 //JobStatus status = statusList.get(0);
			// if(status.getStatus().trim().contains("RUNNING")){
			 //only to show if the entry does not esist in jobStatistics
			 String executionId = getRequestParameter("executionId");
			 String jobId = getRequestParameter("jobId");
				 System.out.println("----going for hot statistics-------");
				 setHotStatistics(true);//Mark it as for Hot Stats...
				 JobStatistics jobStatistics =  new JobStatistics();
				 jobStatistics.setExecutionId(executionId);
				 jobStatistics.setId(-1);
				 jobStatistics.setJob(jobService.getJobById(Integer.parseInt(jobId)));
				 //jobStatistics.setJobStartTime(status.getExecStartTime());
				 jobStatistics.setSerialNo(serialNo);
				 jobStatistics.setStatus(VCSConstants.JOB_STATUS_PROCESSING);
				 
				 if(jobStatistics.getJob().getActionType().contains("EXPORT")){
					 Long totalFailedCount = (statisticsController.getExportFailedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getExportFailedCount(executionId));
					 jobStatistics.setTotalFailed(totalFailedCount);
					 Long totalEvaluatedCount = (statisticsController.getExportEvaluatedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getExportEvaluatedCount(executionId));
					 jobStatistics.setTotalEvaluated(totalEvaluatedCount);
					 
					 Long totalExportedCount = (statisticsController.getExportedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getExportedCount(executionId));
					 jobStatistics.setTotalFreshArchived(totalExportedCount);
					 setJobFlag("EXPORT");
					 
					 Long totalFilesSize = (statisticsController.getTotalExportedFilesSize(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getTotalExportedFilesSize(executionId));
					 totalFilesSize	=	Math.round(totalFilesSize.doubleValue()/(1024*1024));
					 jobStatistics.setArchivedVolume(totalFilesSize);
					 
				 } else if(jobStatistics.getJob().getActionType().contains("INDEXING")){
					 Long totalFailedCount = (statisticsController.getAnalysedFailedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getAnalysedFailedCount(executionId));
					 jobStatistics.setTotalFailed(totalFailedCount);
					 Long totalEvaluatedCount = (statisticsController.getAnalysedEvaluatedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getAnalysedEvaluatedCount(executionId));
					 jobStatistics.setTotalEvaluated(totalEvaluatedCount);
					 
					 Long totalAnalysedCount = (statisticsController.getAnalysedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getAnalysedCount(executionId));
					 jobStatistics.setTotalFreshArchived(totalAnalysedCount);
					 setJobFlag("INDEXING");
					 
					 Long totalFilesSize = (statisticsController.getTotalAnalysedFilesSize(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getTotalAnalysedFilesSize(executionId));
					 totalFilesSize	=	Math.round(totalFilesSize.doubleValue()/(1024*1024));
					 jobStatistics.setArchivedVolume(totalFilesSize);
					 
				 } else if(jobStatistics.getJob().getActionType().contains("RESTORE")){
					 Long totalFailedCount = (statisticsController.getRestoredFailedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getRestoredFailedCount(executionId));
					 jobStatistics.setTotalFailed(totalFailedCount);
					 Long totalEvaluatedCount = (statisticsController.getRestoredEvaluatedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getRestoredEvaluatedCount(executionId));
					 jobStatistics.setTotalEvaluated(totalEvaluatedCount);
					 
					 Long totalAnalysedCount = (statisticsController.getRestoredCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getRestoredCount(executionId));
					 jobStatistics.setTotalFreshArchived(totalAnalysedCount);
					 setJobFlag("EXPORT");
					 
					 Long totalFilesSize = (statisticsController.getTotalRestoredFilesSize(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getTotalRestoredFilesSize(executionId));
					 totalFilesSize	=	Math.round(totalFilesSize.doubleValue()/(1024*1024));
					 jobStatistics.setArchivedVolume(totalFilesSize);
					 
				 } else {
					 Long totalFailedCount = (statisticsController.getFailedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getFailedCount(executionId));
					 jobStatistics.setTotalFailed(totalFailedCount);
					 Long totalEvaluatedCount = (statisticsController.getEvaluatedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getEvaluatedCount(executionId));
					 jobStatistics.setTotalEvaluated(totalEvaluatedCount);
					 
					 Long alreadyArchivedCount = (statisticsController.getAlreadyArchivedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getAlreadyArchivedCount(executionId));
					 jobStatistics.setAlreadyArchived(alreadyArchivedCount);
					 Long totalArchivedCount = (statisticsController.getArchivedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getArchivedCount(executionId));
					 jobStatistics.setTotalFreshArchived(totalArchivedCount-alreadyArchivedCount);//TODO - Yawar
					 Long totalStubbedCount = (statisticsController.getStubbedCount(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getStubbedCount(executionId));
					 jobStatistics.setTotalStubbed(totalStubbedCount);
					 setJobFlag("OTHERS");
					 
					 Long totalFilesSize = (statisticsController.getTotalArchivedFilesSize(executionId)).equals("null")?0l:Long.parseLong(statisticsController.getTotalArchivedFilesSize(executionId));
					 totalFilesSize	=	Math.round(totalFilesSize.doubleValue()/(1024*1024));
					 jobStatistics.setArchivedVolume(totalFilesSize);
					 
				 }
				 Long totalSkipped = jobStatistics.getTotalEvaluated() - (jobStatistics.getTotalFailed() + jobStatistics.getTotalFreshArchived() + jobStatistics.getAlreadyArchived());				 
				 jobStatistics.setTotalSkipped(totalSkipped);
				 
				 //check in remote agent hot statistics if any record of execution exists
				 HotStatistics hotStatistics = HotStatistics.getInstance();
//				 String execId = status.getExecutionId();
				 if(!hotStatistics.getEvaluatedCtr(executionId).equals("null"))
					 jobStatistics.setTotalEvaluated(Long.parseLong(hotStatistics.getEvaluatedCtr(executionId)));
				 if(!hotStatistics.getArchivedCtr(executionId).equals("null"))
					 jobStatistics.setTotalFreshArchived(Long.parseLong(hotStatistics.getArchivedCtr(executionId)));
				 if(!hotStatistics.getExportedCtr(executionId).equals("null"))
					 jobStatistics.setTotalFreshArchived(Long.parseLong(hotStatistics.getExportedCtr(executionId)));				 
				 if(!hotStatistics.getAlreadyArchivedCtr(executionId).equals("null"))
					 jobStatistics.setAlreadyArchived(Long.parseLong(hotStatistics.getAlreadyArchivedCtr(executionId)));
				 if(!hotStatistics.getStubbedCtr(executionId).equals("null"))
					 jobStatistics.setTotalStubbed(Long.parseLong(hotStatistics.getStubbedCtr(executionId)));
				 if(!hotStatistics.getFailedCtr(executionId).equals("null"))
					 jobStatistics.setTotalFailed(Long.parseLong(hotStatistics.getFailedCtr(executionId)));
				 
				 jobStatisticsList1.add(jobStatistics);
				 setJobStatisticsList1(jobStatisticsList1);
			// }
		 }else{
			 
			 setHotStatistics(false);//Mark it as for Hot Stats...
			 
			 JobStatistics jobStatistics = jobStatisticsService.getJobStatisticsById(Integer.parseInt(id));
			 if(jobStatistics.getJob().getActionType().contains("EXPORT") || jobStatistics.getJob().getActionType().contains("RESTORE")){
				 setJobFlag("EXPORT");
			 } else if(jobStatistics.getJob().getActionType().contains("INDEXING")) {
				 setJobFlag("INDEXING");
			 }else{
				 setJobFlag("OTHERS");
			 }
			 List<JobStatus> tempStatusList = jobStatusService.getJobStatusByProperty("executionId", jobStatistics.getExecutionId());
			 JobStatus tempJobStatus = tempStatusList.get(0);
			 if(tempJobStatus.getErrorCode().getId()==1){
				 jobStatistics.setStatus(VCSConstants.JOB_STATUS_FAILED);					 
			 }else if(tempJobStatus.getStatus().equals("COMPLETED")){
				 jobStatistics.setStatus(VCSConstants.JOB_STATUS_COMPLETED);
			 } else if(tempJobStatus.getStatus().equals("CANCEL")){
				 jobStatistics.setStatus(VCSConstants.JOB_STATUS_CANCEL);
			 } else if(tempJobStatus.getStatus().equals("CANCELED")){
				 jobStatistics.setStatus(VCSConstants.JOB_STATUS_CANCELED);
			 } else if(tempJobStatus.getStatus().equals("RUNNING")){
				 jobStatistics.setStatus(VCSConstants.JOB_STATUS_PROCESSING);
			 }else{
				 jobStatistics.setStatus(VCSConstants.JOB_STATUS_FAILED);
			 }
	
			 
			 jobStatistics.setSerialNo(serialNo);
			 long totalSkipped = jobStatistics.getSkippedProcessfileSkippedHiddenlected()+
					 		 jobStatistics.getSkippedProcessfileSkippedIsdir()+
							 jobStatistics.getSkippedProcessfileSkippedMismatchAge()+
							 jobStatistics.getSkippedProcessfileSkippedMismatchExtension()+
							 jobStatistics.getSkippedProcessfileSkippedMismatchLastaccesstime()+
							 jobStatistics.getSkippedProcessfileSkippedMismatchLastmodifiedtime()+
							 jobStatistics.getSkippedProcessfileSkippedReadOnly()+
							 jobStatistics.getSkippedProcessfileSkippedSymbollink()+
							 jobStatistics.getSkippedProcessfileSkippedTemporary()+
							 jobStatistics.getSkippedProcessfileSkippedTooLarge();
			 long totalFailed = jobStatistics.getTotalFailedArchiving()+
					 		 jobStatistics.getTotalFailedDeduplication()+
							 jobStatistics.getTotalFailedEvaluating()+
							 jobStatistics.getTotalFailedStubbing();
			 jobStatistics.setTotalSkipped(totalSkipped);
			 jobStatistics.setTotalFailed(totalFailed);
		
			 //jobStatistics.setArchivedVolume(0l);//Its only available for Hot Stats
		 
			 jobStatisticsList1.add(jobStatistics);
			 setJobStatisticsList1(jobStatisticsList1);
		 }
	 }
	 
	 public void preSave(){
		 Job tempJob=job;
		 Agent agent = agentService.getAgentById(job.getAgent().getId());
		 job.getAgent().setName(agent.getName());
		 
		 job.setActiveActionType("MARKONLY");//First time it should be to lauch the MarkForRemovalThread...
		 
//		 if(job.getAgent().getType().trim().equals("SP")){
			 if(job.getActionType().equals(VCSConstants.DYNAMIC_ARCHIVING)){
				Policy policy = policyService.getPolicyById(0);
				if(policy == null){
					addErrorMessage("'Default policy' for export does not exist", "'Default policy' for export does not exist");
				//	return null;
				}
				 job.setPolicy(policy);
			 }
		 job.getPolicy().setName(policyService.getPolicyById(job.getPolicy().getId()).getName());		
		 Timestamp timestamp;
	        if(!job.getIsScheduled()){
	        	Date date = VCSUtil.getCurrentDateTime();
		        timestamp = new Timestamp(date.getTime());

		        job.setReadyToExecute(true);	//set explicitly for edit job
		        
	        }else{
	        	timestamp = new Timestamp(job.getExecTimeStartDate().getTime());

		        job.setReadyToExecute(false);	//set explicitly for edit job
	        }
	        job.setExecTimeStart(timestamp);

			String[] paths = getDecodedExportPath();
			List<String> exportPathList = new ArrayList<String>();
//			if(selectedLibrariesMap==null || (paths!=null && paths.length>0) ){
//			selectedLibraries = new ArrayList<String>();
//			selectedLibrariesMap = new HashMap<String, String>();
//			if(true){
				System.out.println("-------------Setting up export job----------------------");

				
				for(int i =0; i< paths.length;i++){
					//System.out.println(paths[i]+"---------");
					if(!(paths[i].trim().equals(""))){
//						selectedLibrariesMap.put(paths[i].substring(0,paths[i].indexOf(",")), paths[i].substring(paths[i].indexOf(",")+1).trim());
//						selectedLibraries.add(paths[i].substring(0,paths[i].indexOf(",")));
//						exportPathList.add(paths[i]);	
					}			
				}
//			}
//			}else{
//				for(SearchDocument document:searchResults){
//					exportPathList.add(document.getDocumentPath());
//				}
//			}
		 System.out.println(tempJob.getName());
	 }
	 
	 public String save(){
		 
		 if(job.getId()!=null && job.getId().intValue()>0){
			 //modified to handle problem with indexes for shares. 
			 //Now Id of SPDocLib will remain the same but only for Analysis/Indexing job
			 if(!(job.getActionType().equals("INDEXING"))){
				 jobService.deleteJobDependencies(job);
			 }else{		
				 Set<String> keySet = currentSpLibs.keySet();
				 List<String> tempList = new ArrayList<String>();
				 for(String path: keySet){
					 if(!remotePathsList.contains(path)){
						 jobService.deleteSpDocLib(path.substring(2).replaceAll("\\\\", "/"));
						 tempList.add(path);
					 }
				 }
				 for(String remPath: tempList){
					 currentSpLibs.remove(remPath);
				 }
			 
			 }
		 }else{
				if(jobService.getJobByName(job.getName())!=null){
					addErrorMessage("jobName already exists", "jobName already exists");
					return null;
				}				

		 }
		 
		 if(job.getName()==null || job.getName().trim().isEmpty()){
			 addErrorMessage("'jobName' cannot be empty","'jobName' cannot be empty");
			 return null;
		 }
//			if(job.getAgent().getType().equals("SP") && (getSelectedLibraries() == null || getSelectedLibraries().size()<1)){
//				addErrorMessage("SP library not selected", "SP library not selected");
//				return null;
//			}
		 
		 //ask user to enter valid DateTime for Scheduled Job case (in Add/Edit Job)
//		 if(job.getIsScheduled() && (job.getExecTimeStartDate()==null || (job.getExecTimeStartDate()+"").isEmpty())) {
//			 addErrorMessage("Please select Execution Start time for Scheduled Job", "Please select Start time");
//			 return null;
//		 }
		 System.out.println(docTypes);
		 fileSystemType = "remote";
		 job.setActionType(VCSConstants.INDEXING);
			Policy policy = policyService.getPolicyById(0);
			if(policy == null){
				addErrorMessage("'Default policy' for export does not exist", "'Default policy' for export does not exist");
				return null;
			}
			 job.setPolicy(policy);
		 if(fileSystemType!=null && fileSystemType.trim().equalsIgnoreCase("local") && job.getAgent().getType().trim().equals("FS")){
			/*if(localFSPathsList == null || localFSPathsList.size()<1){
				 	addErrorMessage("No Folders selected", "No Folders selected");
					return null;
			}*/
			 Set<JobSpDocLib> spDocLibsSet=new HashSet<JobSpDocLib>();
			 JobSpDocLib jobSpDocLib;
			 job.getJobSpDocLibs().clear();
			 for(int i=0;localFSPathsList!=null && i<localFSPathsList.size();i++){
				 jobSpDocLib=new JobSpDocLib();
				 jobSpDocLib.setName(localFSPathsList.get(i));
				 jobSpDocLib.setType("FS");
				 
				 jobSpDocLib.setRecursive(true);
				 spDocLibsSet.add(jobSpDocLib);
				 jobSpDocLib.setJob(job);
			 }
			 job.getJobSpDocLibs().addAll(spDocLibsSet);
		 }
		 Set<JobSpDocLib> spDocLibIncSet=new HashSet<JobSpDocLib>(); 
		if(fileSystemType!=null && fileSystemType.trim().equalsIgnoreCase("remote") && job.getAgent().getType().trim().equals("FS")){
			/*if(remotePathsList == null || remotePathsList.size()<1){
			 	addErrorMessage("No Folders selected", "No Folders selected");
				return null;
			}*/
				 spDocLibIncSet=new HashSet<JobSpDocLib>();
				 job.getJobSpDocLibs().clear();
				 JobSpDocLib jobSpDocLib;
				
				 jobSpDocLib=new JobSpDocLib();
				 
				 if(job.getActionType().equalsIgnoreCase(VCSConstants.INDEXING)){
					 job.setIsShareAnalysis(true);
					 saveDrivePath(remoteDomainName, remoteUserName, remoteUserPassword, remoteUNC);
				 }
				 
				 jobSpDocLib.setName(VCSUtil.getSambaPath(remoteDomainName, remoteUserName, remoteUserPassword, remoteUNC));
				 jobSpDocLib.setType("FS");
				 jobSpDocLib.setRecursive(true);					 
				 jobSpDocLib.setJob(this.job);
				 if(docTypes!=null && docTypes.length>0){
					 String docTypesStr = "";
					 for(String docType:docTypes){
						 docTypesStr += docType;
						 docTypesStr +=",";
					 }
					 docTypesStr = docTypesStr.substring(0, docTypesStr.length()-1);
					 jobSpDocLib.setContentTypes(docTypesStr);
				 }
				 spDocLibIncSet.add(jobSpDocLib);
				 
				 for(int i=0;remotePathsList!=null && i<remotePathsList.size();i++){
//					 jobSpDocLib=new JobSpDocLib();
					 jobSpDocLib.getJobSpDocLibExcludedPaths().clear();
					 String remotePath = remotePathsList.get(i);
					 if(job.getActionType().equals(VCSConstants.PRINT_ARCHIVING) || job.getActionType().equals(VCSConstants.ACTIVE_ARCHIVING)){
//						 jobSpDocLib.setDrivePath(remotePath.substring(remotePath.lastIndexOf("(")+1, remotePath.lastIndexOf(")")));
						 jobSpDocLib.setDrivePath(remotePathsMap.get(remotePath));
					 }
					 //create a drive letter entry
					 if(job.getActionType().equalsIgnoreCase(VCSConstants.INDEXING)){
						 job.setIsShareAnalysis(true);
						 saveDrivePath(remoteDomainName, remoteUserName, remoteUserPassword, remotePathsList.get(i));
					 }
					 if(currentSpLibs!=null && currentSpLibs.get(remotePathsList.get(i))!=null)
						 jobSpDocLib.setId(new Integer(currentSpLibs.get(remotePathsList.get(i))));
					 
					 jobSpDocLib.setName(VCSUtil.getSambaPath(remoteDomainName, remoteUserName, remoteUserPassword, remotePathsList.get(i)));
					 jobSpDocLib.setType("FS");
					 jobSpDocLib.setRecursive(true);					 
					 jobSpDocLib.setJob(this.job);
//					 List<String > excludedPathsList=getExcludedPaths(remotePathsList.get(i));
					 //create a new list and remove all previously added
					 //as entry were shown two time for the same location
//					 jobSpDocLib.setJobSpDocLibExcludedPaths(new HashSet()); 
					 
//					 for(int k=0;excludedPathsList!=null && k < excludedPathsList.size();k++){
//						 String childPath=excludedPathsList.get(k);
//						 
//						 if(childPath!=null && !childPath.trim().equals("")){
//							 JobSpDocLibExcludedPath exPathLib= new JobSpDocLibExcludedPath();
//							 exPathLib.setPath(VCSUtil.getSambaPath(remoteDomainName, remoteUserName, remoteUserPassword, childPath));
//							 exPathLib.setJobSpDocLib(jobSpDocLib);							
//							 jobSpDocLib.getJobSpDocLibExcludedPaths().add(exPathLib);
//							 
//							 remoteExPathsList.remove(childPath);//remove from list
//						 }						 
//						 
//					 }
					 
					spDocLibIncSet.add(jobSpDocLib);
				 }
				 if(!job.getActionType().equals("INDEXING")){
					 job.setJobSpDocLibs(spDocLibIncSet);
				 }
				 
				 job.setExecutionInterval(1d);
				 
				 job.setProcessCurrentPublishedVersion(false);
				 job.setProcessLegacyPublishedVersion(false);
				 job.setProcessLegacyDraftVersion(false);
				 
				 if(job.getActionType().equals(VCSConstants.PRINT_ARCHIVING)){
					 job.setActiveActionType("4");
				 }
				 
		 }else if(job.getAgent().getType().trim().equals("SP")){
			 if(job.getActionType().equals(VCSConstants.DYNAMIC_ARCHIVING)){
				policy = policyService.getPolicyById(0);
				if(policy == null){
					addErrorMessage("'Default policy' for export does not exist", "'Default policy' for export does not exist");
					return null;
				}
				 job.setPolicy(policy);
			 }
			 System.out.println(exportNodePath);
			 job.setActiveActionType(null);
			 Agent agent = agentService.getAgentById(job.getAgent().getId());
			 job.setSpSiteId(sharePointService.getSiteId(job.getSpPath(), agent.getAllowedIps()));
			 if(selectedLibraries.size()>0){
				 spDocLibIncSet=new HashSet<JobSpDocLib>();
				 for(String lib:selectedLibraries){
					 JobSpDocLib spDocLib=new JobSpDocLib();
//					 String libName = selectedLibrariesMap.g;					 
//					 String guidAndSite = selectedLibrariesMap.get(lib).getPath();//lib.substring(lib.lastIndexOf(",")+1);
					 SPTreeNode guidAndSite = selectedLibrariesMap.get(lib);//lib.substring(lib.lastIndexOf(",")+1);
//					 replace <space> with %20 in path for SP
					 spDocLib.setName(lib.replaceAll(" ", "%20"));	//saving the complete lib path
	                 
	                 spDocLib.setJob(job);
	                 spDocLib.setGuid(guidAndSite.getGuid());
	                 spDocLib.setSite(guidAndSite.getSitePath());
//	                 spDocLib.setGuid(sharePointService.getLibraryId(job.getSpPath(), lib, agent.getAllowedIps()));
	                 //spDocLib.libraryGUID=jobService.getLibraryID(jobInstance.sitePath.toString(),spDocLib.libraryName.toString(), agent?.allowedAgentIPs)
	                 spDocLib.setType("SP");
	                 spDocLib.setRecursive(false);
	                 if(contentTypesMap!=null && !contentTypesMap.isEmpty()){
	                	 spDocLib.setContentTypes(contentTypesMap.get(spDocLib.getSite()));
	                 }	                 
	                 //spDocLib.libraryType=AGENT_TYPE
	                 spDocLibIncSet.add(spDocLib);
	                 //spDocLibsSet.add(spDocLib)
				 }
				 job.setJobSpDocLibs(spDocLibIncSet);
			 }
		 }
		 
		 Timestamp timestamp;
        if(!job.getIsScheduled()){
        	Date date = VCSUtil.getCurrentDateTime();
	        timestamp = new Timestamp(date.getTime());

	        job.setReadyToExecute(true);	//set explicitly for edit job
	        
        }else{
        	timestamp = new Timestamp(job.getExecTimeStartDate().getTime());

	        job.setReadyToExecute(false);	//set explicitly for edit job
        }
        job.setExecTimeStart(timestamp);
	        
		 jobService.saveJob(job);
		 
		 if(job.getActionType().equals("INDEXING")){
			 for(JobSpDocLib jobSpDocLib: spDocLibIncSet)
				 jobService.saveSpDocLib(jobSpDocLib);
		 }
		 
		 
		 return "DelJob.faces?faces-redirect=true";
	 }
	 
	 public void saveDrivePath(String domain, String username, String password, String sharePath){
		 if(!sharePath.endsWith("\\"))
			 sharePath = sharePath + "\\";
			 
		 List<DriveLetters> driveList = driveLettersService.getDriveLettersBySharePath(sharePath);
		 if(driveList!=null && driveList.size()>0){		
			 DriveLetters driveLetter = driveList.get(0);
			 driveLetter.setAnalyzeShare(true);
			 driveLettersService.saveDriveLetter(driveLetter);
			 return;	//exit without creating new share
		 }
		 DriveLetters driveLetter = new DriveLetters();
		 driveLetter.setDriveLetter(null);
		 driveLetter.setSharePath(sharePath);
		 driveLetter.setRestoreDomain(domain);
		 driveLetter.setRestoreUsername(username);
		 driveLetter.setRestorePassword(password);
		 driveLetter.setStatus(true);
		 driveLetter.setAnalyzeShare(true);
		 driveLetter.setIsFileShare(false);
		 driveLettersService.saveDriveLetter(driveLetter);
	 }
	 
	 private String hasExcludedPath(String basePath){
		 String childPath;
		 for(int i=0;remoteExPathsList!=null && i<remoteExPathsList.size();i++){
			 childPath=remoteExPathsList.get(i);
			 if(VCSUtil.pathExists(basePath, childPath)){
				 return childPath;
			 }
		 }
		 return null;
	 }
	 
	 private List<String> getExcludedPaths(String basePath){
		 String childPath;
		 List<String> results=new ArrayList<String>();
		 for(int i=0;remoteExPathsList!=null && i<remoteExPathsList.size();i++){
			 childPath=remoteExPathsList.get(i);
			 if(VCSUtil.pathExists(basePath, childPath)){
				 results.add(childPath);
			 }
		 }
		 return results;
	 }
	
	
	 public String delete(){
		 jobService.deleteJob(jobService.getJobById(job.getId()));
		 return "DelJob.faces?faces-redirect=true";
	 }
	 
	
	//------------Local FS Paths-------------------------------------------- 
	 
	 public void addToLocalFSPaths(){
		 if(localFSPath==null || localFSPath.trim().equals("") || localFSPathsList.indexOf(localFSPath.trim())!=-1){
			 return;
		 }
		 localFSPathsList.add(localFSPath.trim());
		 localFSPath=null;
	 }
	 
	 public void removeFromLocalFSPaths(){
		 UIViewRoot root = FacesContext.getCurrentInstance().getViewRoot();
		 UIComponent comp = root.findComponent("addJobForm:pathsListBox");
		 UISelectMany listBox=(UISelectMany)comp;
		 Object[] selectedValues=listBox.getSelectedValues();		 
		 String value;
		 for(int i=0;selectedValues!=null && i<selectedValues.length;i++){
			  value=(String)selectedValues[i];
			  localFSPathsList.remove(value);
		 }
		
	 }
	 
	 public void removeAllLocalFSPaths(){
		 localFSPathsList.clear();
	 }
	//----------------Remote FS Paths Include--------------------------------
	 
	 public void addToRemotePaths(){
		 
		 
		 if(remoteUNC==null || remoteUNC.trim().equals("")){
			 return;
		 }else if(remoteUNC.indexOf("\\\\")==-1){
			 addErrorMessage("UNC format incorrect (must start with a '\\\\')", "UNC format incorrect (must start with a '\\\\')");
			 return;
		 }else if(remoteUNC.trim().indexOf("\\\\", 2)!=-1){		 
			 addCompErrorMessage("remoteUNC", "UNC format incorrect(Only two \\\\ allowed in the start)", "UNC format incorrect(Only two \\\\ allowed in the start)");
			 return;
		 }
		 
		 if(remotePathsList.indexOf(remoteUNC.trim())!=-1){
			 addCompErrorMessage("remoteUNC","Path already exists", "Path already exists");
			 return;
		 }

		 testUNC();	// test UNC path		 
		 if(job.getActionType().equals(VCSConstants.ACTIVE_ARCHIVING) || job.getActionType().equals(VCSConstants.PRINT_ARCHIVING)){
			 if(drivePath==null || drivePath.trim().length()<1){				 
				 addCompErrorMessage("remoteUNC","original drive path cannot be empty", "original drive path cannot be empty");
				 return;
			 }else{
				 if(remotePathsMap==null)
					 remotePathsMap = new HashMap<String, String>();
//				 remoteUNC = remoteUNC +"("+drivePath+")";
				 remotePathsMap.put(remoteUNC.trim(), drivePath.trim());
			 }
		 }
//		 if(drivePath!=null && drivePath.trim().length()>0){
//			 remoteUNC = remoteUNC +"("+drivePath+")";
//		 }
		 remotePathsList.add(remoteUNC.trim());
		 remoteUNC=null;
		 drivePath=null;
	 }
	 
	 public void removeFromRemotePaths(){
		 UIViewRoot root = FacesContext.getCurrentInstance().getViewRoot();
		 UIComponent comp = root.findComponent("addJobForm:remotePathsListBox");
		 UISelectMany listBox=(UISelectMany)comp;
		 Object[] selectedValues=listBox.getSelectedValues();		
		 String value;
		 for(int i=0;selectedValues!=null && i<selectedValues.length;i++){
			  value=(String)selectedValues[i];
			  remotePathsList.remove(value);
			  if(remotePathsMap!=null)
				  remotePathsMap.remove(value);
		 }		
	 }
	 
	 public void removeAllRemotePaths(){
		 remotePathsList.clear();
	 }
	 
	//----------------Remote FS Paths Exclude--------------------------------
	 
	 public void addToRemoteExPaths(){
		 
		 if(remotePathsList==null || remotePathsList.isEmpty()){			 
			 return;
		 } else if(remoteUNC.indexOf("\\\\")!=0){
			 addCompErrorMessage("remoteUNC","UNC format incorrect (must start with a '\\\\')", "UNC format incorrect (must start with a '\\\\')");
			 return;
		 }else if(remoteUNC.trim().indexOf("\\\\", 2)!=-1){		 
			 addCompErrorMessage("remoteUNC", "UNC format incorrect(Only two \\\\ allowed in the start)", "UNC format incorrect(Only two \\\\ allowed in the start)");
			 return;
		 }
		 if(remoteExPathsList.indexOf(remoteUNC)!=-1){
			 addCompErrorMessage("remoteUNC","Path already exists", "Path already exists");
			 return;
		 }
		 boolean exists=false;
		 String remotePathTemp;
		 
		 for(int i=0;i<remotePathsList.size();i++){
			 remotePathTemp=remotePathsList.get(i);
			if(VCSUtil.pathExists(remotePathTemp, remoteUNC))
				exists=true;
		 }
		 if(exists==false){
			 addCompErrorMessage("remoteUNC","Excluded path must be a sub folder of an included path", "Excluded path is not a sub-folder of an Included path");
			 return;
		 }	 
		 
		 remoteExPathsList.add(remoteUNC.trim());
		 remoteUNC=null;
	 }
	 
	 public void removeFromRemoteExPaths(){
		 UIViewRoot root = FacesContext.getCurrentInstance().getViewRoot();
		 UIComponent comp = root.findComponent("addJobForm:remoteExPathsListBox");
		 UISelectMany listBox=(UISelectMany)comp;
		 Object[] selectedValues=listBox.getSelectedValues();		
		 String value;
		 for(int i=0;selectedValues!=null && i<selectedValues.length;i++){
			  value=(String)selectedValues[i];
			  remoteExPathsList.remove(value);
		 }		
	 }
	 
	 public void removeFromSPLibPaths(){
		 UIViewRoot root = FacesContext.getCurrentInstance().getViewRoot();
		 UIComponent comp = root.findComponent("addJobForm:spIncludedPathsListBox");
		 UISelectMany listBox=(UISelectMany)comp;
		 Object[] selectedValues=listBox.getSelectedValues();		
		 String value;
		 for(int i=0;selectedValues!=null && i<selectedValues.length;i++){
			  value=(String)selectedValues[i];
			  selectedLibraries.remove(value);
			  SPTreeNode tempNode = selectedLibrariesMap.get(value);
			  tempNode.setSelected(false);	//to deselect the deleted path from nodes list
			  selectedLibrariesMap.remove(value);
		 }		
	 }
	 
	 public void removeAllRemoteExPaths(){
		 remoteExPathsList.clear();
	 }
	 
	//------------------------------------------------------------------
	 
	 public void indexingTypeChangeListener(){
		 System.out.println("Local FS type "+fileSystemType + ",  " + getRequestParameter("current") + " and " + selectedPath);
	 }
	 
	 public void agentTypeChangeListener(){
		 System.out.println("Local Agent type  changed " + job.getAgent().getType() +", id: "+job.getAgent().getId());
		 if(job.getAgent().getType().equals("SP")){
			 setShowSPGrid(true);
			 List<Agent> agentList= agentService.getAgentByType("SP");
			 if(agentList==null||agentList.size()<1){
				 siteList = null;
			 }else{
				 Agent agent = agentList.get(0);
				 job.setAgent(agent);	//to select the first agent in list
				 String webServiceURL=agentList.get(0).getAllowedIps();//"http://192.168.30.32:6827/SPService/DASService.svc?wsdl";
				 System.out.println("service url: " + webServiceURL);
				 siteList = sharePointService.getSiteListByURL(webServiceURL);
				//for(String site:siteList){
				 if(siteList == null || siteList.size()<1){
					libraryList = null;
				 }else{
				    job.setSpPath(siteList.get(0));
					libraryList = sharePointService.getLibraryList(siteList.get(0), webServiceURL);
//					rootNodes = sharePointService.getLibraryNodesList(siteList.get(0), webServiceURL);
					rootNodes = sharePointService.getSiteNodesListByURL(webServiceURL); // reverted for the time being
				 }
				 if(exportPathList!=null){
					 for(ExportJobPath path:getExportPathList()){
	//					 System.out.println("path: " + path.getPath());
						 path.setDestinationPath(fileExplorerService.getNodeByPath(path.getPath()).getSpDocUrl());
	//					 path.setDestinationPath("http://sitepath:8080/lib");
					 }
				 }
			 }
			 
		 }else{
			 setShowSPGrid(false);
			 libraryList = null;
		 }

		//}
/*			
			setLibraryList(libraryList);
			
			//rootNodes.clear();
			
			//= new HashMap<String, Object>();
			/*for(String lib:libraryList){
				
				DocumentType documentType = new DocumentType();
				documentType.setName(lib);
				documentType.setValue(lib);			
				rootNodes.add(documentType);
			}
		 }*/
		 //create tree nodes for libraries
	 }
	 
	 public void agentSelectionChangeListener(){
		 //String spAgentId=getRequestParameter("spAgentId");
		 System.out.println("Local Agent type  changed " + spAgentId+" -------");
		 System.out.println("Local Agent type  changed " + job.getAgent().getId()+" -------");
		 Agent agent = agentService.getAgentById(job.getAgent().getId());
		 if(job.getAgent().getType().equals("FS")){
			 
			 if(agent.getAllowedIps().equals("127.0.0.1"))
				 setEnableActiveArchiving(false);
			 else
				 setEnableActiveArchiving(true);
			 return;			 
		 }
//		 populateSPLibraries(job.getAgent().getId());
//		 job.setAgent(agent);	//to select the first agent in list
		 String webServiceURL=agent.getAllowedIps();//"http://192.168.30.32:6827/SPService/DASService.svc?wsdl";
		 job.setAgent(agent);
		 System.out.println("service url: " + webServiceURL);
		 siteList = sharePointService.getSiteListByURL(webServiceURL);
		//for(String site:siteList){
		 if(siteList == null || siteList.size()<1){
			libraryList = null;
		 }else{
		    job.setSpPath(siteList.get(0));
			libraryList = sharePointService.getLibraryList(siteList.get(0), webServiceURL);
//			rootNodes = sharePointService.getLibraryNodesList(siteList.get(0), webServiceURL);
			rootNodes = sharePointService.getSiteNodesListByURL(webServiceURL); // reverted for the time being
		 }
		 
	 }
	 
	 public void spSiteChangeListener(){
		 System.out.println("Local Agent type  changed " + job.getAgent().getType() +", id: "+job.getAgent().getId());
		 Agent agent = agentService.getAgentById(job.getAgent().getId());
		 String webServiceURL= agent.getAllowedIps();//"http://192.168.30.32:6827/SPService/DASService.svc?wsdl";
		 System.out.println("URL web service: " + webServiceURL);
		 System.out.println("Site Selected: " + job.getSpPath());
		 libraryList = sharePointService.getLibraryList(job.getSpPath(), webServiceURL);
		 rootNodes = sharePointService.getLibraryNodesList(job.getSpPath(), webServiceURL);
		 sharePointService.getSiteContentTypes(webServiceURL, job.getSpPath());
		 if(selectedLibraries!=null){
			 selectedLibraries.clear();
			 selectedLibrariesMap.clear();
		 }
		 selectedLibrary = "";
	 }
	 
	 public void spLibrarySelectionListener(){
		 
		 //String webServiceURL="http://192.168.30.32:6827/SPService/DASService.svc?wsdl";
		 System.out.println("Source Selected: " + selectedPath);
		 System.out.println("Site Selected: " + job.getSpPath());
		 System.out.println("Selected library: " + selectedLibrary);
			//first get the guid of the site
		 Agent agent = agentService.getAgentById(job.getAgent().getId());
         //String libName = localFSPath;         
         String libId =null;
         currentLibPath = job.getSpPath()+"/"+selectedLibrary;
         currentGuid = sharePointService.getLibraryId(job.getSpPath(), selectedLibrary, agent.getAllowedIps());
         System.out.println(currentGuid);
         subLibraryList = sharePointService.getSubLibraryList(currentGuid, currentLibPath, agent.getAllowedIps());
         for(String subLib:subLibraryList){
        	 System.out.println("Sub Library: " + subLib);
         }
         selectedSubLibrary = "";
         
		 //libraryList = sharePointService.getLibraryList(job.getSpPath(), webServiceURL);		 
	 }
	 
	 public void spSubLibrarySelectionListener(){
		 
		 System.out.println("Site Selected: " + job.getSpPath());
		 System.out.println("Selected Sub library: " + selectedSubLibrary);
			//first get the guid of the site
		 Agent agent = agentService.getAgentById(job.getAgent().getId());
         //String libName = localFSPath;
		 System.out.println("guid old: "+currentGuid+", old path: "+ currentLibPath);
         String libId =null;         
         System.out.println(currentLibPath);
         currentGuid = sharePointService.getSubLibraryId(currentGuid, currentLibPath, selectedSubLibrary, agent.getAllowedIps());
         System.out.println("id--"+currentGuid);
         currentLibPath = currentLibPath+"/"+selectedSubLibrary;
         System.out.println(currentLibPath);
         
         subLibraryList = sharePointService.getSubLibraryList(currentGuid, currentLibPath, agent.getAllowedIps());
         for(String subLib:subLibraryList){
        	 System.out.println("Sub Library: " + subLib);
         }  	 
	 }	 
	 //select current path
	 public void selectPath(){
		 selectedPath =  getRequestParameter("current");
		 System.out.println("Local FS type "+fileSystemType + ",  " + getRequestParameter("current") + " and " + selectedPath);
	 }
	 
	 public void saveLibrarySelection(){
		 List<ExportJobPath> exportPathList = new ArrayList<ExportJobPath>();
		 //export does not work if a site is selected
		 if(currentNode.getType().equals("site")) {
			 addErrorMessage("Please select a library as destination", "Please select a library");
			 return;
		 }
		 for(ExportJobPath exportPath:this.exportPathList){
			 if(exportPath.getPath().trim().equals(selectedPath)){
				 System.out.println("path selected: " +selectedPath);
				 exportPath.setDestinationPath(currentLibPath);
				 exportPath.setGuid(currentNode.getGuid());
				 exportPath.setSitePath(currentNode.getSitePath());
				 System.out.println("path selected: " +exportPath.getDestinationPath());
			 }
			 exportPathList.add(exportPath);
		 }
		 setExportPathList(exportPathList);
	 }
	 
	 public void selectAllChangeListener(){
		 System.out.println("select all: " + selectAll);
		 
		 if(selectAll){
			 String[] docTypes = {"pdf", "txt", "doc", "docx","ppt", "pptx", "xls", "xlsx"};
		 
			 setDocTypes(docTypes);
		 }else{
			 String[] docTypes = {};
			 setDocTypes(docTypes);
		 }
		 
	 }

	 public void spLibrarySelectAllListener(){
		 SPTreeNode spTreeNode;
		// String webServiceURL="http://192.168.30.32:6827/SPService/DASService.svc?wsdl";
		 System.out.println("Site Selected: " + job.getSpPath());
		 if(isSelectAllCheckBox()){
			 System.out.println("Selection: select all");
//			 setSelectedLibraries(libraryList);
	    	 System.out.println("---Document Library selected---------");
	    	 if(selectedLibraries==null)
	    		 selectedLibraries = new ArrayList<String>();
	    	 if(selectedLibrariesMap==null)
	    		 selectedLibrariesMap = new HashMap<String, SPTreeNode>();
	    	 
	    	 List<SPTreeNode> childs;
	    	 for(int i=0;i<rootNodes.size();i++){
	    		 spTreeNode=(SPTreeNode)rootNodes.get(i);
	    		 spTreeNode.setSelected(true);
	    		 if(selectedLibraries!=null && !(selectedLibraries.contains(spTreeNode.getPath()))){
    				 selectedLibraries.add(spTreeNode.getPath()); 
//    				 selectedLibrariesMap.put(spTreeNode.getPath(), spTreeNode.getGuid()+"|"+spTreeNode.getSitePath());
    				 selectedLibrariesMap.put(spTreeNode.getPath(), spTreeNode);
    			 }
//	    		 childs = spTreeNode.getChildNodes();
////	    		 documentTypes=spTreeNode.getDocumentTypesList();    		 
//	    		 for(int j=0;childs!=null&&j<childs.size();j++){
//	    			 childs.get(j).setSelected(spTreeNode.getSelected());
////	    			 documentTypes.get(j).setSelected(spTreeNode.getSelected());
//	    			 
//	    		 }
	    	 }
	    	 
			 
		 }else{
			 selectedLibraries.clear();
			 selectedLibrariesMap.clear();
	    	 for(int i=0;i<rootNodes.size();i++){
	    		 spTreeNode=(SPTreeNode)rootNodes.get(i);
	    		 spTreeNode.setSelected(false);
//	    		 childs = spTreeNode.getChildNodes();
////	    		 documentTypes=spTreeNode.getDocumentTypesList();    		 
//	    		 for(int j=0;childs!=null&&j<childs.size();j++){
//	    			 childs.get(j).setSelected(spTreeNode.getSelected());
////	    			 documentTypes.get(j).setSelected(spTreeNode.getSelected());
//	    			 
//	    		 }
	    	 }
		 }
		 //libraryList = sharePointService.getLibraryList(job.getSpPath(), webServiceURL);		 
	 }
	 
	 public void spLibrarySelectListener(){
		 
		 System.out.println("Selection: select one");			 
		 setSelectAllCheckBox(false);
		 System.out.println("Selection: " + getSelectedLibraries().size());
		 		 
	 }	
	 
	 public void spSiteContentTypeSelectListener(){
		 
//		 System.out.println("Selection: select one");			 
//		 setSelectAllCheckBox(false);
		 if(contentTypesMap == null)
			 contentTypesMap = new HashMap<String, String>();
		 String contentTypeStr = "";
		 for(String contentType:getSelectedContentTypes())
			 contentTypeStr += contentType + " ,";
		 
		 if(contentTypeStr!= null && contentTypeStr.trim().length()>0)
			 contentTypeStr =  contentTypeStr.substring(0, contentTypeStr.lastIndexOf(","));
		 
		 System.out.println(contentTypeStr);
		 System.out.println(currentNode);
		 if(currentNode.getType().equals("site"))	// if site, then get the path
			 contentTypesMap.put(currentNode.getPath(), contentTypeStr.trim());
		 else	// if library then get the site path
			 contentTypesMap.put(currentNode.getSitePath(), contentTypeStr.trim());
		 System.out.println("Selection: " + getSelectedContentTypes());
		 		 
	 }		 
	 public void testUNC(){
		 System.out.println("---------------------verifying UNC-------------------");
		 boolean error = false;
		 if(remoteUNC.trim().equals("") ||remoteUNC.trim().length()<3){// test as in existing app
			 addCompErrorMessage("remoteUNC", "UNC incorrect", "Invalid UNC Path");
//			 setTestUNCResponse("wrong");
			 error = true;
		 }else if(remoteUNC.trim().indexOf("\\\\", 2)!=-1){		 
			 addCompErrorMessage("remoteUNC", "UNC format incorrect(Only two \\\\ allowed in the start)", "UNC format incorrect(Only two \\\\ allowed in the start)");
			 error = true;
		 }else if(remoteUserPassword.trim().contains("@")){		 
			 addCompErrorMessage("remoteUNC", "Password cannot contain '@'","Password cannot contain '@'");
			 error = true;
		 }else{
			 String destPath = "";
			 destPath = VCSUtil.getSambaPath(remoteDomainName, remoteUserName, remoteUserPassword, remoteUNC.substring(2,remoteUNC.length()).replaceAll("\\\\","/").trim());
//			 if(remoteDomainName!=null && remoteDomainName.trim().length()<1)				 
//				 destPath = "smb://" + remoteDomainName + ";" + remoteUserName + ":" + remoteUserPassword + "@" + remoteUNC.substring(2,remoteUNC.length()).replaceAll("\\\\","/").trim() + "/";
//			 else
//				 destPath = "smb://"+ remoteUserName + ":" + remoteUserPassword + "@" + remoteUNC.substring(2,remoteUNC.length()).replaceAll("\\\\","/").trim() + "/";
			 
			 System.out.println("destination path: " + destPath);
			 try {
				SmbFile file = new SmbFile(destPath);
				if(file==null || !file.exists()){
					addCompErrorMessage("remoteUNC", "UNC path not found","UNC path not found");
					error = true;
				}else{
					System.out.println(file.list()); //throws exception access denied if user does not have permission
					//now to check if the user has write permissions on the directory
					SmbFile fileTemp = new SmbFile(destPath+"/sacheck.tmp");
					SmbFileOutputStream sfos = new SmbFileOutputStream(fileTemp);
					sfos.write("Testing write permissions".getBytes());
					sfos.flush();
					sfos.close();
					fileTemp.delete();	//delete after testing
					
					addCompInfoMessage("remoteUNC", "UNC path verified","UNC path verified");
				}
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				addCompErrorMessage("remoteUNC","Error verifying credentials","Error verifying credentials");
				error = true;
			}catch(SmbAuthException ex){
                ex.printStackTrace();
                addCompErrorMessage("remoteUNC","Incorrect domain, username and/or password, or Access is denied","Incorrect domain, username and/or password, or Access is denied");
                error = true;
			}catch (SmbException e) {
				// TODO Auto-generated catch block
				addCompErrorMessage("remoteUNC","Server or Sharename not found","Server or Sharename not found");
				error = true;
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();				
			}			 
			 if(!error){
				 testUNCResponse = "success";
			 }else{
				 testUNCResponse = "failure";
			 }
			 
		 }
	 }
	 
	 public boolean testSmbUrl(String destPath){
		 System.out.println("---------------------verifying Smb URL-------------------");
//			 String destPath = "smb://" + remoteDomainName + ";" + remoteUserName + ":" + remoteUserPassword + "@" + remoteUNC.substring(2,remoteUNC.length()).replaceAll("\\\\","/").trim() + "/";
//			 System.out.println("destination path: " + destPath);
		 boolean error = false;
			 try {
				SmbFile file = new SmbFile(destPath);
				if(!file.exists()){
//					addCompErrorMessage("remoteUNC", "UNC path not found","UNC path not found");
				}else{
					System.out.println(file.list()); //throws exception access denied if user does not have permission
					//now to check if the user has write permissions on the directory
					SmbFile fileTemp = new SmbFile(destPath+"/sacheck.tmp");
					SmbFileOutputStream sfos = new SmbFileOutputStream(fileTemp);
					sfos.write("Testing write permissions".getBytes());
					sfos.flush();
					sfos.close();
					fileTemp.delete();	//delete after testing
//					addCompInfoMessage("remoteUNC", "UNC path verified","UNC path verified");
					error = false;
				}
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
//				addCompErrorMessage("remoteUNC","Error verifying credentials","Error verifying credentials");
				error = true;
			}catch(SmbAuthException ex){
                ex.printStackTrace();
//                addCompErrorMessage("remoteUNC","Incorrect domain, username and/or password, or Access is denied","Incorrect domain, username and/or password, or Access is denied");
                error = true;
			}catch (SmbException e) {
				// TODO Auto-generated catch block
//				addCompErrorMessage("remoteUNC","Server or Sharename not found","Server or Sharename not found");
				error = true;
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 if(!error){
				 testUNCResponse = "success";
			 }else{
				 testUNCResponse = "failure";
			 }
			 return error;
		 
	 }
	 
	 public void actionTypeChangeListener(){
		 //String spAgentId=getRequestParameter("spAgentId");
		 System.out.println("Local Agent type  changed " + spAgentId+" -------");
		 System.out.println("Local Agent type  changed " + job.getAgent().getId()+" -------");
		 if(job.getActionType().equals(VCSConstants.DYNAMIC_ARCHIVING) || job.getActionType().equals(VCSConstants.EXPORT_STUB_ONLY)){
			 setShowSPGrid(true);
			 setShowActiveGrid(false);
				 List<Agent> agentList= agentService.getAgentByType("SP");
				 if(agentList==null||agentList.size()<1){
					 siteList = null;
					 return;
				 }else{
					 Agent agent = agentList.get(0);
					 job.setAgent(agent);	//to select the first agent in list
				 }

//			 populateSPLibraries(job.getAgent().getId());
			 
			 String webServiceURL=job.getAgent().getAllowedIps();//"http://192.168.30.32:6827/SPService/DASService.svc?wsdl";
			 System.out.println("service url: " + webServiceURL);
			 siteList = sharePointService.getSiteListByURL(webServiceURL);
			//for(String site:siteList){
			 if(siteList == null || siteList.size()<1){
				libraryList = null;
			 }else{
			    job.setSpPath(siteList.get(0));
				libraryList = sharePointService.getLibraryList(siteList.get(0), webServiceURL);
//				rootNodes = sharePointService.getLibraryNodesList(siteList.get(0), webServiceURL);
				rootNodes = sharePointService.getSiteNodesListByURL(webServiceURL); // reverted for the time being
			 }
			 

		 }else if(job.getActionType().equals(VCSConstants.PRINT_ARCHIVING) || (job.getActionType().equals(VCSConstants.ACTIVE_ARCHIVING) && job.getAgent().getType().equals("FS"))){
			 setShowActiveGrid(true);
			 setShowSPGrid(false);
		 }else{		 
//			 setShowSPGrid(false);
			 setShowActiveGrid(false);
		 }
		 
	 }
	 
	 //----------------------Export Job
	 public void createExportJob(List<String> exportPathList){
		 //setExportPathList(exportPathList);		 
		 System.out.println("export job created "+ exportPathStringList.size());
		 getHttpRequest().setAttribute("exportPathList", exportPathStringList);
		 
	 }
	 
	 public void getExportPathsList(){
		 //job.setActionType(VCSConstants.EXPORT);
		 List<String> exportPathsList = (List<String>)getHttpRequest().getAttribute("exportPathList");
		 List<ExportJobPath> exportPathList = new ArrayList<ExportJobPath>();
		 //creating export job path object
		/* String jobId = getHttpRequest().getParameter("id");
		 System.out.println("export job id: "+ jobId);
		 if(jobId!=null){			 
			 job = jobService.getJobById(Integer.parseInt(jobId));
			 System.out.println("job name: " + job.getName()+", size: "+job.getJobSpDocLibs().size());
			 if(job.getJobSpDocLibs().size()>0){
				 exportPathsList = new ArrayList<String>();				 
				 for(Object obj: job.getJobSpDocLibs()){
					 JobSpDocLib lib = (JobSpDocLib)obj;
					 //System.out.println("lib name:" + lib.getName());
					 String destinationPath =  lib.getDestinationPath();
					 if(destinationPath.startsWith("smb:")){
						 String domain = destinationPath.substring(6, destinationPath.lastIndexOf(":"));
						 String username = destinationPath.substring(destinationPath.lastIndexOf(":")+1, destinationPath.lastIndexOf(";"));
						 String password = destinationPath.substring(destinationPath.lastIndexOf(";")+1, destinationPath.lastIndexOf("@"));
						 setRemoteDomainName(domain);
						 setRemoteUserName(username);
						 setRemoteUserPassword(password);
						 String destination = destinationPath.substring(destinationPath.lastIndexOf("@")+1);
						 System.out.println(domain);
						 System.out.println(username);
						 System.out.println(password);
						 System.out.println(destination);
						 exportPathsList.add(destination);
						 
						 ExportJobPath exportJobPath = new ExportJobPath();
						 exportJobPath.setPath(lib.getName());
						 exportJobPath.setDestinationPath(destination);
						 exportJobPath.setRecursive(lib.getRecursive());
						 exportJobPath.setSource(lib.getName().substring(lib.getName().lastIndexOf("/")+1));
						 exportPathList.add(exportJobPath);
					 }
				 }
					 
			 }
			 //job.getJobSpDocLibs()exportPathList.size();
		 }*/
		 if(exportPathsList!=null){
		 for(String path:exportPathsList){
			 ExportJobPath exportJobPath = new ExportJobPath();
			 System.out.println("path: " + path);
			 String[] tokens = path.substring(1).split("/");
			 String destinationPath ="";			 
			 exportJobPath.setRecursive(true);
			 exportJobPath.setPath(path);
			 exportJobPath.setSource(tokens[tokens.length-1]);
			 if(path.contains("/SP/")){
				 destinationPath = "";//empty destination				 
				 
			 }else{
				 for(int i=2;i<tokens.length;i++){				 
					 destinationPath=destinationPath+"\\"+tokens[i];
				 }
				 destinationPath="\\\\"+destinationPath.substring(1);
				 if(path.startsWith("http://")){
					 exportJobPath.setDestinationPath(path);
				 }else{
					 exportJobPath.setDestinationPath(destinationPath);
				 }
			 }
			 System.out.println(destinationPath);
			 Double size = fileExplorerService.getPathExportSize(path);
			 //converting KBs to MBs
			 if(size!=null){
				 size = size/1024d;
			 }
			 exportJobPath.setFolderSize(Math.round(size)+" MB (approx.)");
			 //if greater than 1024 MBs then convert to GBs
			 if(size!=null && size > 1024d)
			 {
				 size = size/1024d;
				 exportJobPath.setFolderSize(Math.round(size)+" GB (approx.)");
			 }
			 //calculate stub export size
			 Long stubSize = fileExplorerService.getPathExportStubsSize(path);
			 //converting KBs to MBs
			 if(stubSize!=null && stubSize>=1024l){
				 stubSize = stubSize/1024l;
				 
				 exportJobPath.setFolderStubsSize(stubSize+" MB (approx.)");
				 //if greater than 1024 MBs then convert to GBs
				 if(stubSize!=null && stubSize > 1024l)
				 {
					 stubSize = stubSize/1024l;
					 exportJobPath.setFolderStubsSize(stubSize+" GB (approx.)");
				 }
			 }else{
				 exportJobPath.setFolderStubsSize(stubSize+" KB");
			 }
			 
			 exportPathList.add(exportJobPath);
			 
			 //destinationPath = path.substring(beginIndex);
			 
		 }
		 setExportPathList(exportPathList);
		 }
		 
		 //return exportPathList;
	 }
	 
	 public void getExportPathListForExport(){
		 List<String> exportPathsList = (List<String>)getHttpRequest().getAttribute("exportPathList");
		 List<ExportJobPath> exportPathList = new ArrayList<ExportJobPath>();
		 String jobId = getHttpRequest().getParameter("id");
		 System.out.println("getExportPathListForExport() xport job id: "+ jobId);
		 if(jobId!=null){			 
			 job = jobService.getJobById(Integer.parseInt(jobId));
			 String domain = "";
			 String username = "";
			 String password = "";
			 System.out.println("job name: " + job.getName()+", size: "+job.getJobSpDocLibs().size());
			 if(job.getJobSpDocLibs().size()>0){
				 exportPathsList = new ArrayList<String>();				 
				 for(Object obj: job.getJobSpDocLibs()){
					 JobSpDocLib lib = (JobSpDocLib)obj;
					 //System.out.println("lib name:" + lib.getName());
					 String destinationPath =  lib.getDestinationPath();
					 //System.out.println("destination path: " + destinationPath);
					 if(destinationPath.startsWith("smb:")){
						 if(destinationPath.lastIndexOf(";")<0){
							 domain = "";
							 username = destinationPath.substring(6, destinationPath.lastIndexOf(":"));							 
						 }else{
							 domain = destinationPath.substring(6, destinationPath.lastIndexOf(";"));
							 username = destinationPath.substring(destinationPath.lastIndexOf(";")+1, destinationPath.lastIndexOf(":"));							 							 
						 }
						 password = destinationPath.substring(destinationPath.lastIndexOf(":")+1, destinationPath.lastIndexOf("@"));
							 
						 
						 //setRemoteDomainName(domain);
						 //setRemoteUserName(username);
						 //setRemoteUserPassword(password);
						 String destination = destinationPath.substring(destinationPath.lastIndexOf("@")+1);
						 
						 System.out.println("remote domain: "+domain);
						 System.out.println("remote user: "+username);
						 System.out.println("remote password: "+password);
	
						 System.out.println(destination);
						 exportPathsList.add(destination);
						 
						 ExportJobPath exportJobPath = new ExportJobPath();
						 exportJobPath.setPath(lib.getName());
						 exportJobPath.setDestinationPath(destination);
						 exportJobPath.setRecursive(lib.getRecursive());
						 exportJobPath.setSource(lib.getName().substring(lib.getName().lastIndexOf("/")+1));
						 exportPathList.add(exportJobPath);
					 }
				 }
					 
			 }
			 //System.out.println("--remote domain: "+domain);
			 //System.out.println("--remote user: "+username);
			 //System.out.println("--remote password: "+password);
			 setRemoteDomainName(domain);
			 setRemoteUserName(username);
			 setRemoteUserPassword(password);
			 System.out.println("--global remote domain: "+getRemoteDomainName());
			 System.out.println("--remote user: "+getRemoteUserName());
			 System.out.println("--remote password: "+getRemoteUserPassword());
			 //job.getJobSpDocLibs()exportPathList.size();
			 setExportPathList(exportPathList);
		 }
	 }
	 
public String saveExport(){
	String remoteDomainName = "";
	String remoteUserName = "";
	String remoteUserPassword = "";
	
	boolean error = false;
	String isContinue = getRequestParameter("continue")!=null?getRequestParameter("continue"):"";
	
	int errorCount = 0;
	if(job.getName()==null || job.getName().length()<1){
		addErrorMessage("jobName cannot be empty", "jobName cannot be empty");
		return null;
	}
	if(!(job.getAgent().getType().trim().equals("SP")) && !(useCustomCreds)){
		remoteDomainName=ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
		remoteUserName=ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
		remoteUserPassword=ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
		
		if(remoteDomainName==null || remoteDomainName.trim().equals("") 
				|| remoteUserName==null || remoteUserName.trim().equals("")
				|| remoteUserPassword==null || remoteUserPassword.trim().equals("")){
			addErrorMessage("Please enter the global restore credentials to create an export job", "Please enter the global restore credentials to create an export job");
			return null;
		}
		
//		if(remoteDomainName==null || remoteDomainName.trim().equals("")){
////			addErrorMessage("domain name cannot be empty", "domain name cannot be empty");
//			addErrorMessage("domain name cannot be empty", "domain name cannot be empty");
//			errorCount = errorCount + 1;
//		}
//		if(remoteUserName==null || remoteUserName.trim().equals("")){
//			addErrorMessage("user name cannot be empty", "user name cannot be empty");
//			errorCount = errorCount + 1;
//		}
//		if(remoteUserPassword==null || remoteUserPassword.trim().equals("")){
//			addErrorMessage("user password cannot be empty", "user password cannot be empty");
//			errorCount = errorCount + 1;		
//		}
	}
	if(job.getId()!=null && job.getId().intValue()>0){

		 jobService.deleteJobDependencies(job);		 
	 }else{
			if(jobService.getJobByName(job.getName())!=null){
				addErrorMessage("jobName already exists", "jobName already exists");
				return null;
			}
	 }
	
	
	
		System.out.println("----"+job.getActionType()+"----");
		//set default policy (0) for job		
//		Policy policy = policyService.getPolicyById(0);
		Policy policy = null;
		List<Policy> policyList = policyService.getPolicyByName("DefaultPolicy");
		if(policyList!=null && policyList.size()>0){
			policy = policyList.get(0);
		}
		if(policy == null){
			addErrorMessage("'Default policy' for export does not exist", "'Default policy' for export does not exist");
			return null;
		}
		System.out.println(policy.getName());

		Date date = VCSUtil.getCurrentDateTime();
		Timestamp timestamp = new Timestamp(date.getTime());
		job.setExecTimeStart(timestamp);
		job.setPolicy(policy);
        
        job.setProcessCurrentPublishedVersion(false);
        job.setProcessLegacyPublishedVersion(false);
        job.setProcessLegacyDraftVersion(false);
        job.setProcessReadOnly(false);
        job.setProcessArchive(false);
        job.setProcessHidden(false);
        
        job.setReadyToExecute(true);
        job.setActive(1);
        job.setExecutionInterval(1d);
        
        job.setPreserveFilePaths(false);
        
        Set<JobSpDocLib> setJobSpDocLibs = new HashSet<JobSpDocLib>();
        
        for(ExportJobPath path:getExportPathList()){
        	JobSpDocLib jobSpDocLib = new JobSpDocLib();        	
        	if(path.getDestinationPath()==null || path.getDestinationPath().trim().length()<1){
				addErrorMessage("destination cannot be empty for '" + path.getSource()+"'", "destination cannot be empty for '" + path.getSource() + "'");
				errorCount = errorCount + 1;			
			}else if(!(job.getAgent().getType().trim().equals("SP")) && !(path.getDestinationPath().startsWith("\\\\"))){ 
				addErrorMessage("destination format incorrect (must start with '\\\\')", "destination format incorrect (must start with '\\\\')");
				errorCount = errorCount + 1;
			}else{
				DriveLetters driveLetter = null;
				String smbPath = "";
	        	if(!(job.getAgent().getType().trim().equals("SP"))){		        		
				        List<DriveLetters> driveLetterList=driveLettersService.getAll();
				        String tempPath = path.getDestinationPath().substring(2).replaceAll("\\\\", "/");
				        if(!useCustomCreds){
					        for(DriveLetters letter: driveLetterList){
			//		        	if(path.getPath().contains(letter.getSharePath())){
					        	String tempDrivePath = "";
					        	if(letter.getSharePath().indexOf("FS/")!=-1)
					        		tempDrivePath = letter.getSharePath().substring(letter.getSharePath().indexOf("FS/")+3);
					        	else
					        		tempDrivePath = letter.getSharePath().substring(2).replaceAll("\\\\", "/");
					        	if(tempPath.contains(tempDrivePath)){
					        		driveLetter = letter;
					        		break;
					        	}
					        }
				        }
			        
		        		        
				        if( driveLetter!=null && driveLetter.getRestoreDomain()!=null && driveLetter.getRestoreUsername()!=null && driveLetter.getRestorePassword()!=null
				        		&& !(driveLetter.getRestoreDomain().trim().length()<1) && !(driveLetter.getRestoreUsername().trim().length()<1) && !(driveLetter.getRestorePassword().trim().length()<1)){
				        	smbPath = "smb://"+driveLetter.getRestoreDomain() +";"+ driveLetter.getRestoreUsername()+":"+driveLetter.getRestorePassword()+"@"
		     					   + path.getDestinationPath().substring(2).replaceAll("\\\\", "/");
				        }else{
				        	//domain name optional
				        	if(this.remoteDomainName==null || this.remoteDomainName.trim().length()<1)
				        		smbPath = "smb://"+getRemoteUserName()+":"+getRemoteUserPassword()+"@"
				        				   + path.getDestinationPath().substring(2).replaceAll("\\\\", "/");
				        	else
				        		smbPath = "smb://"+getRemoteDomainName() +";"+getRemoteUserName()+":"+getRemoteUserPassword()+"@"
			        				   + path.getDestinationPath().substring(2).replaceAll("\\\\", "/");
				        }
				        if(!(isContinue.trim().equals("true")) && !(job.getAgent().getType().trim().equals("SP"))){
				        	error = testSmbUrl(smbPath+"/");	// test the path
				        }
					}
		        
				System.out.println("-------"+path.getSource()+"-----");
				System.out.println("-------"+path.isRecursive()+"-----");
				System.out.println("-------"+smbPath+"-----");
				jobSpDocLib.setRecursive(path.isRecursive());
				jobSpDocLib.setName(path.getPath());			
				jobSpDocLib.setGuid(null);
				if(job.getAgent().getType().trim().equals("SP")){
					jobSpDocLib.setType("SP");
//					replace <space> with %20 in path for SP
					jobSpDocLib.setDestinationPath(path.getDestinationPath().replaceAll(" ", "%20"));
					jobSpDocLib.setGuid(path.getGuid());
					jobSpDocLib.setSite(path.getSitePath());
				}else{
					jobSpDocLib.setType("CIFS");
					jobSpDocLib.setDestinationPath(smbPath);
				}
				
				jobSpDocLib.setJobSpDocLibExcludedPaths(null);
				setJobSpDocLibs.add(jobSpDocLib);
				jobSpDocLib.setJob(job);
			}
		}
        System.out.println("errors: "+errorCount);
        if(errorCount>0)
        {
        	return null;
        }else{
	        job.setJobSpDocLibs(new HashSet());
	        job.getJobSpDocLibs().addAll(setJobSpDocLibs);
	        if(!error){
	        	jobService.saveExportJob(job);
	        }
	        System.out.println("-------saved-----");
        
	/*	 
		 if(job.getId()!=null && job.getId().intValue()>0){			 
			 jobService.deleteJobDependencies(job);
		 }
		
		 if(fileSystemType!=null && fileSystemType.trim().equalsIgnoreCase("local")){
			 Set<JobSpDocLib> spDocLibsSet=new HashSet<JobSpDocLib>();
			 JobSpDocLib jobSpDocLib;
			 job.getJobSpDocLibs().clear();
			 for(int i=0;i<localFSPathsList.size();i++){
				 jobSpDocLib=new JobSpDocLib();
				 jobSpDocLib.setName(localFSPathsList.get(i));
				 jobSpDocLib.setType("FS");
				 
				 jobSpDocLib.setRecursive(true);
				 spDocLibsSet.add(jobSpDocLib);
				 jobSpDocLib.setJob(job);
			 }
			 job.getJobSpDocLibs().addAll(spDocLibsSet);
		 }
		 
		 if(fileSystemType!=null && fileSystemType.trim().equalsIgnoreCase("remote")){
				 Set<JobSpDocLib> spDocLibIncSet=new HashSet<JobSpDocLib>();
				 job.getJobSpDocLibs().clear();
				 JobSpDocLib jobSpDocLib;
				
				 for(int i=0;remotePathsList!=null && i<remotePathsList.size();i++){
					 jobSpDocLib=new JobSpDocLib();
					 jobSpDocLib.getJobSpDocLibExcludedPaths().clear();
					 jobSpDocLib.setName(VCSUtil.getSambaPath(remoteDomainName, remoteUserName, remoteUserPassword, remotePathsList.get(i)));
					 jobSpDocLib.setType("FS");
					 jobSpDocLib.setRecursive(true);					 
					 jobSpDocLib.setJob(this.job);
					 List<String > excludedPathsList=getExcludedPaths(remotePathsList.get(i));
					 
					 for(int k=0;excludedPathsList!=null && k < excludedPathsList.size();k++){
						 String childPath=excludedPathsList.get(k);
						 
						 if(childPath!=null && !childPath.trim().equals("")){
							 JobSpDocLibExcludedPath exPathLib= new JobSpDocLibExcludedPath();
							 exPathLib.setPath(VCSUtil.getSambaPath(remoteDomainName, remoteUserName, remoteUserPassword, childPath));
							 exPathLib.setJobSpDocLib(jobSpDocLib);							
							 jobSpDocLib.getJobSpDocLibExcludedPaths().add(exPathLib);
							 
							 remoteExPathsList.remove(childPath);//remove from list
						 }						 
						 
					 }
					 
					spDocLibIncSet.add(jobSpDocLib);
				 }
				 job.setJobSpDocLibs(spDocLibIncSet);				 
				 
		 }
		 job.setProcessCurrentPublishedVersion(false);
		 job.setProcessLegacyPublishedVersion(false);
		 job.setProcessLegacyDraftVersion(false);
		 
		 jobService.saveJob(job);
		 */
		 
		 return "DelJob.faces?faces-redirect=true";
        }
	 }

	public void populateExportJobInfo(){
	 if(jobId==null || jobId.intValue()<0  ||  FacesContext.getCurrentInstance().isPostback()){
		 return;
	 }
	 this.job=jobService.getJobById(jobId);
	 Iterator docLibIterator=job.getJobSpDocLibs().iterator();
	 Iterator docLibExPathIterator=null;
	 JobSpDocLib jobSpDocLib;
	 //JobSpDocLibExcludedPath jobSpDocLibExPath;
	 String path;
	 String destination;
	 List<ExportJobPath> exportJobPaths = new ArrayList<ExportJobPath>();
	 while(docLibIterator!=null && docLibIterator.hasNext()){
		 jobSpDocLib=(JobSpDocLib)docLibIterator.next();
		 path=jobSpDocLib.getName();
		 destination = jobSpDocLib.getDestinationPath();
		 
		 ExportJobPath exportJobPath = new ExportJobPath();
		 exportJobPath.setPath(jobSpDocLib.getName());
		 exportJobPath.setRecursive(jobSpDocLib.getRecursive());
		 String[] tokens = jobSpDocLib.getName().split("/");
		 exportJobPath.setSource(tokens[tokens.length-1]);
		 if(job.getAgent().getType().equals("SP")){
			 exportJobPath.setDestinationPath(destination);
			 setShowSPGrid(true);
			 List<Agent> agentList= agentService.getAgentByType("SP");
			 if(agentList==null||agentList.size()<1){
				 siteList = null;
			 }else{
				 Agent agent = job.getAgent();
				 //job.setAgent(agent);	//to select the first agent in list
				 String webServiceURL=agentList.get(0).getAllowedIps();//"http://192.168.30.32:6827/SPService/DASService.svc?wsdl";
				 System.out.println("service url: " + webServiceURL);
				 siteList = sharePointService.getSiteListByURL(webServiceURL);
				//for(String site:siteList){
				 if(siteList == null || siteList.size()<1){
					libraryList = null;
				 }else{
//				    job.setSpPath(siteList.get(0));
					libraryList = sharePointService.getLibraryList(siteList.get(0), webServiceURL);
					rootNodes = sharePointService.getLibraryNodesList(job.getSpPath(), job.getAgent().getAllowedIps());
				 }
			 }
		 }else{
			 exportJobPath.setDestinationPath("\\\\"+destination.substring(destination.lastIndexOf("@")+1).replace("/", "\\"));
			 setShowSPGrid(false);
		 }		 
		 
		 Double size = fileExplorerService.getPathExportSize(path);
		 //converting KBs to MBs
		 if(size!=null){
			 size = size/1024d;
		 }
		 exportJobPath.setFolderSize(Math.round(size)+" MB (approx.)");
		 //if greater than 1024 MBs then convert to GBs
		 if(size!=null && size > 1024d)
		 {
			 size = size/1024d;
			 exportJobPath.setFolderSize(Math.round(size)+" GB (approx.)");
		 }
		 //calculate stub export size
		 Long stubSize = fileExplorerService.getPathExportStubsSize(path);
		 //converting KBs to MBs
		 if(stubSize!=null && stubSize>=1024l){
			 stubSize = stubSize/1024l;
			 
			 exportJobPath.setFolderStubsSize(stubSize+" MB (approx.)");
			 //if greater than 1024 MBs then convert to GBs
			 if(stubSize!=null && stubSize > 1024l)
			 {
				 stubSize = stubSize/1024l;
				 exportJobPath.setFolderStubsSize(stubSize+" GB (approx.)");
			 }
		 }else{
			 exportJobPath.setFolderStubsSize(stubSize+" KB");
		 }
		 exportJobPaths.add(exportJobPath);
		 
		 jobSpDocLib.setJob(job);
		 /*
		 if(path.startsWith("smb://")){
				 setFileSystemType("remote");
				 setRemoteDomainName(VCSUtil.getDomainName(path));
				 setRemoteUserName(VCSUtil.getUserName(path));
				 setRemoteUserPassword(VCSUtil.getUserPassword(path));
				 					 
				 remotePathsList.add(VCSUtil.getDirPathFromSambaPath(path));				 

		 }else{
			 setFileSystemType("local");
			 localFSPathsList.add(path);
		 }	*/
		 
		 setExportPathList(exportJobPaths);
	 }		 
	 
}
	
	 public void populateSPLibraries(Integer agentId){
		 if(agentId==null||agentId<1){
			 siteList = null;
		 }else{
			 Agent agent = agentService.getAgentById(agentId);
			 String webServiceURL=agent.getAllowedIps();//"http://192.168.30.32:6827/SPService/DASService.svc?wsdl";
			 System.out.println("service url: " + webServiceURL);
			 siteList = sharePointService.getSiteListByURL(webServiceURL);		 
			//for(String site:siteList){
			 if(siteList == null || siteList.size()<1){
				libraryList = null;
			 }else{
			    job.setSpPath(siteList.get(0));
				libraryList = sharePointService.getLibraryList(siteList.get(0), webServiceURL);
				rootNodes = sharePointService.getLibraryNodesList(siteList.get(0), webServiceURL);
			 }
		 }
	 }
	
	public void cancelJob(){
		   String jobId=getRequestParameter("jobId");
		   Integer id=VCSUtil.getIntValue(jobId);
		   List<JobStatus> jobStatsList=jobStatusService.getJobStatusByJob(id);
		   Integer jobStatusId=null;		   
		   
		   if(jobStatsList!=null && jobStatsList.size()>0){
			   JobStatus jobStatus=jobStatsList.get(0);
			   JobStatus _instance=new JobStatus();
			   _instance.setDescription("blocking...");
			   _instance.setExecStartTime(jobStatus.getExecStartTime());
			   _instance.setErrorCode(jobStatus.getErrorCode());
			   _instance.setExecutionId(jobStatus.getExecutionId());
			   _instance.setCurrent(1);
			   _instance.setJob(jobStatus.getJob());
			   _instance.setPreviousJobStatusId(jobStatus.getId());
			   _instance.setStatus(VCSConstants.JOB_STATUS_CANCEL);
			  
			   jobStatusService.saveJobStatus(_instance);
		   }
		   
		   if(jobStatusId!=null){			 
			   List<Integer> list=jobService.getCancelJobs();
			   System.out.println("Cancel Jobs List --->"+list.toString());
		   }
	}
	
	public void pauseJob(){
		   String jobId=getRequestParameter("jobId");
		   Integer id=VCSUtil.getIntValue(jobId);
		   Job job = jobService.getJobById(id);
		   if(job!=null){
			   if(job.getActionType().equals("DYNARCH") || job.getActionType().equals(VCSConstants.ACTIVE_ARCHIVING)){
				   job.setReadyToExecute(false);
				   jobService.saveJob(job);
			   }
		   }
	}

	public void resumeJob(){
		   String jobId=getRequestParameter("jobId");
		   Integer id=VCSUtil.getIntValue(jobId);
		   Job job = jobService.getJobById(id);
		   if(job!=null){
			   if(job.getActionType().equals("DYNARCH") || job.getActionType().equals(VCSConstants.ACTIVE_ARCHIVING)){
				   job.setReadyToExecute(true);
				   jobService.saveJob(job);
			   }
		   }
	}
	public void setSPLibList(){
		
	}
	
	public void saveExportPriv(){
		System.out.println("------------------"+exportNodePath);
		
		if(exportNodePath==null || exportNodePath.trim().length()<1){
			addErrorMessage("Please select files to export", "Please select files to export");
			return;
		}

		if(remoteUNC==null || remoteUNC.trim().equals("")){
			addErrorMessage("UNC cannot be empty", "UNC cannot be empty");
			return;
		}
		
//		ru  =   ResourcesUtil.getResourcesUtil();
		
		// as per instructions by Sir Mazhar system code values will be used for export domain, username and password
		
		Policy policy = null;
		List<Policy> policyList = policyService.getPolicyByName("DefaultPolicy");
		if(policyList!=null && policyList.size()>0){
			policy = policyList.get(0);
		}
//		policy = policyService.getPolicyByName("DefaultPolicy");
		if(policy == null){
			addErrorMessage("'Default policy' for export does not exist", "'Default policy' for export does not exist");
			return;
		}
		
		if(preserveFilePaths==true){
			exportDuplicates = false;	//set false forcibly
		}
		System.out.println(policy.getName());

		Date date = VCSUtil.getCurrentDateTime();
		Timestamp timestamp = new Timestamp(date.getTime());
		job.setExecTimeStart(timestamp);
		job.setPolicy(policy);
		getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
	//	job.setName("("+getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME)+")"+"-Priv-export-job-"+System.currentTimeMillis());//for unique name every time
        
        job.setProcessCurrentPublishedVersion(false);
        job.setProcessLegacyPublishedVersion(false);
        job.setProcessLegacyDraftVersion(false);
        job.setProcessReadOnly(false);
        job.setProcessArchive(false);
        job.setProcessHidden(false);
        
        job.setReadyToExecute(true);
        job.setActive(1);
        job.setExecutionInterval(1d);
        job.setActionType("EXPORTSEARCHED");
        Set<JobSpDocLib> setJobSpDocLibs = new HashSet<JobSpDocLib>();
		searchResults.toString();
		System.out.println(selectAll);
		String decodeStr="";
		try {
			decodeStr = URLDecoder.decode(exportNodePath, "UTF-8");
			System.out.println(decodeStr);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String[] paths = decodeStr.split("[|]");
		List<String> exportPathList = new ArrayList<String>();
		if(!selectAll){
			System.out.println("-------------Setting up export job----------------------");

			
			for(int i =0; i< paths.length;i++){
				//System.out.println(paths[i]+"---------");
				if(!(paths[i].trim().equals(""))){
					exportPathList.add(paths[i]);	
				}			
			}
		}else{
			for(SearchDocument document:searchResults){
				exportPathList.add(document.getDocumentPath());
			}
		}
		long limit = 0l;
		String exportLimit = ru.getCodeValue("MAX_RESULTS_TO_EXPORT", SystemCodeType.GENERAL);
		if(exportLimit!=null && exportLimit.trim().length()>0){
			try{
				limit = Long.parseLong(exportLimit);
			
				if(limit>0 && exportPathList.size()>limit){
					addErrorMessage("Selected documents exceeding maximum limit [" + limit +"]", "Number of docs exceeding limits");
					return;
				}
			}catch(java.lang.NumberFormatException ex){
				System.out.println("Invalid value for limit.");
				limit = 0l;
			}
		}
//		limit = 2l;
		long fileCount = 0l;

			for(String exportPath:exportPathList){
				JobSpDocLib jobSpDocLib = new JobSpDocLib();        	
//				String[] elements = StringUtils.getPathElements(exportPath);
//				String path ="";
//				for(int i = 2; i <elements.length;i++ ){
//					path = path + elements[i];
//					if(i < elements.length-1){
//						path+="/";
//					}
//				}
				//only upto maximum limit
//				if(limit>0 && fileCount>=limit){
//					break;
//				}
	    	
	        	String smbPath = "smb://"+ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL) +";"+ru.getCodeValue("restore.username", SystemCodeType.GENERAL)+":"+ru.getCodeValue("restore.password", SystemCodeType.GENERAL)+"@"
	        					   + remoteUNC.substring(2).replaceAll("\\\\", "/");
	        	if(job.isPreserveFilePaths()){
	        		if(!(smbPath.endsWith("/"))){
	        			smbPath+="/";
	        		}
	        	
	        		String[] elements = StringUtils.getPathElements(exportPath);
	        		
	        		for(int i = 4; i <elements.length-1; i++){	        			
	        			smbPath+=elements[i]+"/";
	        		}
	        		
	        	}
				System.out.println("-------"+smbPath+"-----");
				jobSpDocLib.setRecursive(false);
				jobSpDocLib.setName(exportPath);
				jobSpDocLib.setGuid(null);
	//			if(job.getAgent().getType().trim().equals("SP")){
	//				jobSpDocLib.setType("SP");
	//				jobSpDocLib.setDestinationPath(path);
	//			}else{
					jobSpDocLib.setType("CIFS");
					jobSpDocLib.setDestinationPath(smbPath);
	//			}
				
				jobSpDocLib.setJobSpDocLibExcludedPaths(null);
				setJobSpDocLibs.add(jobSpDocLib);
				jobSpDocLib.setJob(job);
				fileCount = fileCount +1;
			}
		
		List<Agent> agentList = agentService.getAgentByName(ru.getCodeValue("AgentName", SystemCodeType.ZUES_AGENT));
		if(agentList != null && agentList.size()>0){
			job.setAgent(agentList.get(0));
		}
        job.setJobSpDocLibs(new HashSet());
        job.getJobSpDocLibs().addAll(setJobSpDocLibs);
        try{
        jobService.saveExportJob(job);
        }catch(Exception ex){
        	ex.printStackTrace();
        	addErrorMessage("internal error, could not create export job", "internal error");
        }
        addInfoMessage("Export job created successfully", "success");
	}
	
	public void readyToExport(){
		
		int fileCount = 0;
		long totalFileSize = 0l;
		String decodeStr="";
		try {
			decodeStr = URLDecoder.decode(exportNodePath, "UTF-8");
			System.out.println(decodeStr);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		
		List<String> exportPathList = new ArrayList<String>();
	
		if(!selectAll){
			String[] paths = decodeStr.split("[|]");		
			System.out.println("-------------Setting up export job----------------------");

			
			for(int i =0; i< paths.length;i++){
				//System.out.println(paths[i]+"---------");
				
				if(!(paths[i].trim().equals(""))){
					for(SearchDocument document:searchResults){
						if(document.getDocumentPath().equals(paths[i])){
							fileCount=fileCount+1;
							totalFileSize = totalFileSize + Long.parseLong(document.getDocumentSize());
							break;
						}
					}
					exportPathList.add(paths[i]);
				}			
			}
		}else{
			  RowIterator rows = searchService.searchForExport(searchQuery);			  
			  while(rows.hasNext()){
				  try {
					  javax.jcr.Node node = rows.nextRow().getNode();
					  String size = null;
					  exportPathList.add(node.getParent().getPath());
					  fileCount=fileCount+1;
					  if(node.hasProperty("jcr:data")){
		                   double length = node.getProperty("jcr:data").getLength();
		                   size = String.valueOf(Math.round(Math.ceil(length / 1000d))) ;
		                   totalFileSize = totalFileSize + Long.parseLong(size);
					  }					  
					  
					System.out.println("--"+node.getParent().getPath());
				} catch (RepositoryException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			  }
			  System.out.println("-------------"+rows.getSize());
//			for(SearchDocument document:searchResults){
//				exportPathList.add(document.getDocumentPath());
//				fileCount=fileCount+1;
//				totalFileSize = totalFileSize + Long.parseLong(document.getDocumentSize());
//			}
			
			
		/*	JobSpDocLib jobSpDocLib = new JobSpDocLib();        	
				String[] elements = StringUtils.getPathElements(document.getDocumentPath());
				String path ="";
				for(int i = 2; i <elements.length;i++ ){
					path = path + elements[i];
					if(i < elements.length-1){
						path+="/";
					}
				}
        	
	        	String smbPath = "smb://"+getRemoteDomainName() +";"+getRemoteUserName()+":"+getRemoteUserPassword()+"@"
	        					   + path;
				System.out.println("-------"+smbPath+"-----");
				jobSpDocLib.setRecursive(false);
				jobSpDocLib.setName(path.substring(path.lastIndexOf("/")));
				jobSpDocLib.setGuid(null);
//				if(job.getAgent().getType().trim().equals("SP")){
//					jobSpDocLib.setType("SP");
//					jobSpDocLib.setDestinationPath(path);
//				}else{
					jobSpDocLib.setType("CIFS");
					jobSpDocLib.setDestinationPath(smbPath);
//				}
				
				jobSpDocLib.setJobSpDocLibExcludedPaths(null);
				setJobSpDocLibs.add(jobSpDocLib);
				jobSpDocLib.setJob(job);
			}*/
		}
		job.setName("("+getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME)+")"+"-Priv-export-job-"+System.currentTimeMillis());//for unique name every time
		setTotalFileCount(fileCount+"");
		//convert to MB
		if(totalFileSize>1024l){
			totalFileSize = totalFileSize/1024l;
			setTotalFileSize(totalFileSize+" MB");
		}
		//convert to GB
		if(totalFileSize>1024l){
			totalFileSize = totalFileSize/1024l;
			setTotalFileSize(totalFileSize+ " GB");
		}
		

	}
	
    public void spTreeNodeToggled(TreeToggleEvent tge) {	    
    	System.out.println("-------------SP Tree node toggled-----------------");   		
    	
		UITreeNode node = (UITreeNode)tge.getSource();
		UITree tree = (UITree)node.getParent();
		
		SPTreeNode currentSelection = (SPTreeNode)tree.getRowData();

		if(currentSelection.getChildNodes()==null || currentSelection.getChildNodes().size()<1){
//			currentSelection.getChildNodes().clear();
			parentNode=currentSelection;
			ArrayList<SPTreeNode> firstLevelDirs = null;

			if(!(currentSelection.isSubLibrary())){
				firstLevelDirs=spTreeLibraryToggled(currentSelection);
			}else{
				firstLevelDirs=spTreeSubLibraryToggled(currentSelection,currentSelection.getPath(), null, null);
			}
//			firstLevelDirs=filterNodes(firstLevelDirs);
			currentSelection.setChildNodes(firstLevelDirs);
       // companyNode.getChildNodes().add(currentSelection);
		}else{
			//firstLevelDirs=currentSelection.getChildNodes();
		}
	
	}
    public void spSiteToggled(TreeToggleEvent tge) {	    
    	System.out.println("-------------SP Tree node toggled-----------------");   		
    	
		UITreeNode node = (UITreeNode)tge.getSource();
		UITree tree = (UITree)node.getParent();
		
		SPTreeNode currentSelection = (SPTreeNode)tree.getRowData();

		if(currentSelection.getChildNodes()==null || currentSelection.getChildNodes().size()<1){
//			currentSelection.getChildNodes().clear();
			parentNode=currentSelection;
			ArrayList<SPTreeNode> firstLevelDirs = null;
			firstLevelDirs = spTreeSiteToggled(currentSelection);
			contentTypeList =(ArrayList<String>) sharePointService.getSiteContentTypes(job.getAgent().getAllowedIps(), currentSelection.getPath());
//			firstLevelDirs=filterNodes(firstLevelDirs);
			currentSelection.setChildNodes(firstLevelDirs);
       // companyNode.getChildNodes().add(currentSelection);
		}else{
			//firstLevelDirs=currentSelection.getChildNodes();
		}
	
	}
	 public ArrayList<SPTreeNode> spTreeSubLibraryToggled(SPTreeNode spNode, String guid, String libPath, String selectedLib){
		 
		 System.out.println("Site Selected: " + job.getSpPath());
		 System.out.println("Selected Sub library: " + selectedSubLibrary);
			//first get the guid of the site
		 Agent agent = agentService.getAgentById(job.getAgent().getId());
         //String libName = localFSPath;
		 System.out.println("guid old: "+currentGuid+", old path: "+ currentLibPath);
         String libId =null;
         ArrayList<SPTreeNode> childNodes;
         System.out.println(currentLibPath);
//         currentGuid = sharePointService.getSubLibraryId(currentGuid, currentLibPath, selectedSubLibrary, agent.getAllowedIps());
         currentGuid = sharePointService.getSubLibraryId(currentGuid, spNode.getPath().substring(0, spNode.getPath().lastIndexOf("/")), spNode.getName(), agent.getAllowedIps());
         System.out.println("id--"+currentGuid);
         currentLibPath = currentLibPath+"/"+selectedSubLibrary;
         System.out.println(currentLibPath);
         
//         subLibraryList = sharePointService.getSubLibraryList(currentGuid, currentLibPath, agent.getAllowedIps());
         if(selectedLibraries!=null && selectedLibraries.size()>0)
        	 childNodes = (ArrayList<SPTreeNode>)sharePointService.getSubLibraryNodesList(currentGuid, spNode.getPath(), selectedLibraries, agent.getAllowedIps());
         else
        	 childNodes = (ArrayList<SPTreeNode>)sharePointService.getSubLibraryNodesList(currentGuid, spNode.getPath(), agent.getAllowedIps());
//         for(String subLib:subLibraryList){
//        	 System.out.println("Sub Library: " + subLib);
//         }  	 
         return childNodes;
	 }
	 
	 public ArrayList<SPTreeNode> spTreeLibraryToggled(SPTreeNode spNode){
		 
		 //String webServiceURL="http://192.168.30.32:6827/SPService/DASService.svc?wsdl";
		 System.out.println("Source Selected: " + selectedPath);
		 System.out.println("Site Selected: " + job.getSpPath());
		 System.out.println("Selected library: " + selectedLibrary);
			//first get the guid of the site
		 Agent agent = agentService.getAgentById(job.getAgent().getId());
         //String libName = localFSPath;         
         String libId =null;
         ArrayList<SPTreeNode> childNodes;
         currentLibPath = job.getSpPath()+"/"+spNode.getPath();
         currentGuid = sharePointService.getLibraryId(spNode.getParentPath(), spNode.getName(), agent.getAllowedIps());
         if(currentGuid==null || currentGuid.trim().length()<1)
        	 currentGuid = sharePointService.getSiteId(spNode.getName(), agent.getAllowedIps());
         System.out.println(currentGuid);
         if(selectedLibraries!=null && selectedLibraries.size()>0)
        	 childNodes = (ArrayList<SPTreeNode>)sharePointService.getSubLibraryNodesList(currentGuid, spNode.getPath(), selectedLibraries, agent.getAllowedIps());
         else
        	 childNodes = (ArrayList<SPTreeNode>)sharePointService.getSubLibraryNodesList(currentGuid, spNode.getPath(), agent.getAllowedIps());
//         for(String subLib:subLibraryList){
//        	 System.out.println("Sub Library: " + subLib);
//         }
         selectedSubLibrary = "";
         return childNodes;
		 //libraryList = sharePointService.getLibraryList(job.getSpPath(), webServiceURL);		 
	 }
	 
	 public ArrayList<SPTreeNode> spTreeSiteToggled(SPTreeNode spNode){
		 
		 //String webServiceURL="http://192.168.30.32:6827/SPService/DASService.svc?wsdl";
		 System.out.println("Source Selected: " + selectedPath);
		 System.out.println("Site Selected: " + job.getSpPath());
		 System.out.println("Selected library: " + selectedLibrary);
			//first get the guid of the site
		 Agent agent = agentService.getAgentById(job.getAgent().getId());
         //String libName = localFSPath;         
         String libId =null;
         ArrayList<SPTreeNode> childNodes;
         currentLibPath =spNode.getPath();
//         currentGuid = sharePointService.getLibraryId(job.getSpPath(), spNode.getName(), agent.getAllowedIps());         
//         System.out.println(currentGuid);
         if(selectedLibraries!=null)
        	 childNodes = (ArrayList<SPTreeNode>)sharePointService.getLibraryNodesList(spNode.getPath(), selectedLibraries, agent.getAllowedIps());
         else
        	 childNodes = (ArrayList<SPTreeNode>)sharePointService.getLibraryNodesList(spNode.getPath(), agent.getAllowedIps());
//         for(String subLib:subLibraryList){
//        	 System.out.println("Sub Library: " + subLib);
//         }
         selectedSubLibrary = "";
         return childNodes;
		 //libraryList = sharePointService.getLibraryList(job.getSpPath(), webServiceURL);		 
	 }
    
     public void librarySelected(){
    	 System.out.println("---library selected---------");
    	 SPTreeNode spTreeNode;
    	 List<SPTreeNode> childs;
    	 if(selectedLibraries==null)
    		 selectedLibraries = new ArrayList<String>();
    	 if(selectedLibrariesMap==null)
    		 selectedLibrariesMap = new HashMap<String, SPTreeNode>();
    	 for(int i=0;i<rootNodes.size();i++){
    		 spTreeNode=(SPTreeNode)rootNodes.get(i);
    		 if(!(spTreeNode.getSelected())){
    			 selectAllCheckBox = false;
    			 if(selectedLibraries.contains(spTreeNode.getPath())){
//    				 selectedLibraries.remove(spTreeNode.getPath());
//    				 selectedLibrariesMap.remove(spTreeNode.getPath());
    			 }    				 
    		 }else{
    			 if(selectedLibraries!=null && !(selectedLibraries.contains(spTreeNode.getPath()))){
//    				 selectedLibraries.add(spTreeNode.getPath()); 
//    				 selectedLibrariesMap.put(spTreeNode.getPath(), spTreeNode.getGuid()+"|"+spTreeNode.getSitePath());
    			 }
    		 }
    		 childs = spTreeNode.getChildNodes();
//    		 documentTypes=spTreeNode.getDocumentTypesList();    		 
    		 for(int j=0;childs!=null&&j<childs.size();j++){
    			 SPTreeNode child = childs.get(j); 
    			 System.out.println(child.getParent());
    			 System.out.println(child.getChildCount());
    			 if(child.getSelected()){// && !(selectedLibraries.contains(child.getPath()))){
    				 System.out.println("child selected "+ child.getName());
//    				 selectedLibraries.add(child.getPath());
//    				 selectedLibrariesMap.put(child.getPath(), child.getGuid()+"|"+child.getSitePath());
    				 selectedLibrariesMap.put(child.getPath(), child);
    			 }else{
    				 if(selectedLibraries.contains(child.getPath()))
    				 {
    					 if(selectedLibraries!=null && selectedLibraries.contains(child.getPath())){
//	        				 selectedLibraries.remove(child.getPath());
	        				 selectedLibrariesMap.remove(child.getPath());
    					 }
    				 }

    			 }
				 if(child.getChildCount()>0){
					 selectRecursiveLibs(child);
				 }
				 selectedLibraries = new ArrayList<String>();
				 selectedLibraries.addAll(selectedLibrariesMap.keySet());
//    			 childs.get(j).setSelected(spTreeNode.getSelected());
//    			 documentTypes.get(j).setSelected(spTreeNode.getSelected());
    			 
    		 }
    	 }
    	 
     }
     
     public void selectRecursiveLibs(SPTreeNode child){
    	 List<SPTreeNode> childs;
    	 childs = child.getChildNodes();
    	 
    	 for(int j=0;childs!=null&&j<childs.size();j++){
			 SPTreeNode tempChild = childs.get(j); 
			 System.out.println(tempChild.getChildCount());
			 if(tempChild.getSelected()){// && !(selectedLibraries.contains(tempChild.getPath()))){
				 System.out.println("sub child selected "+ tempChild.getName());
//				 selectedLibraries.add(tempChild.getPath());
//				 selectedLibrariesMap.put(tempChild.getPath(), tempChild.getGuid()+"|"+tempChild.getSitePath());
				 selectedLibrariesMap.put(tempChild.getPath(), tempChild);
			 }else{
				 if(selectedLibraries.contains(tempChild.getPath()))
				 {
					 if(selectedLibraries!=null && selectedLibraries.contains(tempChild.getPath())){
//        				 selectedLibraries.remove(tempChild.getPath());
        				 selectedLibrariesMap.remove(tempChild.getPath());
					 }
				 }
			 }
			 if(tempChild.getChildCount()>0){
				 selectRecursiveLibs(tempChild);
			 }
    	 }
		 selectedLibraries = new ArrayList<String>();
		 selectedLibraries.addAll(selectedLibrariesMap.keySet());
			 
    	 
     }
     
     
     
     public void selectionChanged(TreeSelectionChangeEvent selectionChangeEvent) {
     	System.out.println("-------------selection changed-----------------");
     	
         List<Object> selection = new ArrayList<Object>(selectionChangeEvent.getNewSelection());
         System.out.println(selection.size());
         if(selection!=null && selection.size()>0){
         Object currentSelectionKey = selection.get(0);
         UITree tree = (UITree) selectionChangeEvent.getSource();
         selectionChangeEvent.getSource();
         Object storedKey = tree.getRowKey();
         tree.setRowKey(currentSelectionKey);
        
         TreeNode sNode = (TreeNode) tree.getRowData();
         System.out.println(sNode.toString());
         SPTreeNode spTreeNode = (SPTreeNode)sNode;
         currentLibPath = spTreeNode.getPath();
         currentNode = spTreeNode;
         
         contentTypeList =(ArrayList<String>) sharePointService.getSiteContentTypes(job.getAgent().getAllowedIps(), spTreeNode.getPath());
         System.out.println(contentTypeList);         
        

//     	System.out.println("Selection: " + customNode.getName()+", selected:"+ customNode.isSelected());
         }
     }
     
	public boolean isMemberOf(String path, String[] paths){
		for(int i =0; i< paths.length;i++){
			System.out.println(paths[i]+"---------");
			if(paths[i].trim().equals(path)){
				//exportPathList.add(paths[i]);	
				return true;
			}			
		}
		return false;
	}
	
	public String[] getDecodedExportPath(){
		 String decodeStr = null;
			try {
				decodeStr = URLDecoder.decode(exportNodePath, "UTF-8");
				System.out.println(decodeStr);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			selectAll = false;
		return decodeStr.split("[|]");		
	}
	
	public void changeCredentials(){
		String remoteDomainName=ru.getCodeValue("restore.domain_name", SystemCodeType.GENERAL);
		String remoteUserName=ru.getCodeValue("restore.username", SystemCodeType.GENERAL);
		String remoteUserPassword=ru.getCodeValue("restore.password", SystemCodeType.GENERAL);
		if(!(this.remoteDomainName.equals(remoteDomainName)) || !(this.remoteUserName.equals(remoteUserName)) || !(this.remoteUserPassword.equals(remoteUserPassword))){
			setUseCustomCreds(true);
		}else{
			setUseCustomCreds(false);
		}
	}
	
	public String getContentTypesForPath(String str){
		SPTreeNode node = selectedLibrariesMap.get(str);
		if(node!=null && contentTypesMap!=null){
			String type = contentTypesMap.get(node.getSitePath());
			return type;
		}
		return "";
	}
	
	
	 
	 private Integer jobId;
		
	 private Job job;
		
	 private List<Job> jobs;
	 
	 private String fileSystemType="local";
	 
	 private String localFSPath;
	 
	 		
	 private List<String> localFSPathsList=new ArrayList<String>(0);
	 
	 private List<String> remotePathsList=new ArrayList<String>(0);
	 
	 private List<String> remoteExPathsList=new ArrayList<String>(0);

	 private String remoteDomainName;
	 
	 private String remoteUserName;
	 
	 private String remoteUserPassword;
	 
	 private String remoteUNC;
	  
	 private List<JobStatus> jobStatusList;	
	 
	 private List<JobStatistics> jobStatisticsList;
	 
	 private List<JobStatistics> jobStatisticsList1;	//current execution details
	 
	 //private Date executionStartDateTime = new Date();
	 
	 //private boolean runNow = true; //default is true	 
	 
	 private String jobFlag = "OTHERS";
	 
	 private boolean hotStatistics	=	false;
	 
	 public boolean isHotStatistics() {
		return hotStatistics;
	}

	public void setHotStatistics(boolean hotStatistics) {
		this.hotStatistics = hotStatistics;
	}


	private List<ExportJobPath> exportPathList;
	 
	 private List<String> exportPathStringList;
	 
	 private String testUNCResponse;
	 
	 private String jobIdForStatus; //added for job status page to remove a background exception
	 
	 private List<String> siteList;
	 
	 //private Map<String, Object> libraryList;	 
	 
	 private List<String> libraryList;
	 
	 private List<String> selectedLibraries;
	 
	 private Map<String, SPTreeNode> selectedLibrariesMap;
	 
	 private String spAgentId;
	 
	 private List<String> subLibraryList;
	 
	 private String selectedLibrary;
	 
	 private String selectedSubLibrary;
	 
	 private String currentGuid;
	 
	 private String currentLibPath;
	 
	 private String selectedPath;
	 
	 private SPTreeNode currentNode;
	 
	 
	 private String exportJobSourcePath=null;
	 
	 private String exportJobDestinationPath=null;
	 
	 private ArrayList<String> exportJobSourcePathList=null;
	 
	 private ArrayList<String> exportJobDestinationPathList=null;
	 
	public List<String> getSelectedLibraries() {
		return selectedLibraries;
	}

	public void setSelectedLibraries(List<String> selectedLibraries) {
		this.selectedLibraries = selectedLibraries;
	}

	private boolean selectAllCheckBox;
	 
	private boolean showSPGrid;
	 
	private String searchString; 
	
	private String exportNodePath;
	 
	private List<SearchDocument> searchResults=new ArrayList<SearchDocument>();
	
	private String searchQuery;
	
	private boolean selectAll = false;
	
	private boolean preserveFilePaths = true;
	
	private boolean exportDuplicates = false;
	
	private String totalFileCount;
	
	private String totalFileSize;
	
	private String totalFileStubsSize;
	
	private boolean enableSharePoint;
	
	private SPTreeNode parentNode;
	
	private boolean useCustomCreds = false;
	
	private boolean showActiveGrid = false;
	
	private String drivePath;
	
	private boolean enableActiveArchiving = false;
	
	private ArrayList<String> contentTypeList;
	
	private ArrayList<String> selectedContentTypes;
	
	private HashMap<String, String> contentTypesMap;
	
	private Map<String,String> remotePathsMap;//=new ArrayList<String,String>(0);
	
	private Map<String, String> currentSpLibs;
	
	private String indexingType;
	
	private boolean showDocTypes;
	
	private String[] docTypes = {"pdf", "txt", "doc", "docx","ppt", "pptx", "xls", "xlsx"};
	
//	 public Map<String, Object> getLibraryList() {
//		return libraryList;
//	}
//
//	public void setLibraryList(Map<String, Object> libraryList) {
//		this.libraryList = libraryList;
//	}

	public boolean isShowSPGrid() {
		return showSPGrid;
	}

	public void setShowSPGrid(boolean showSPGrid) {
		this.showSPGrid = showSPGrid;
	}

	public boolean isSelectAllCheckBox() {
		return selectAllCheckBox;
	}

	public void setSelectAllCheckBox(boolean selectAllCheckBox) {
		this.selectAllCheckBox = selectAllCheckBox;
	}

	public List<String> getLibraryList() {
		return libraryList;
	}

	public void setLibraryList(List<String> libraryList) {
		this.libraryList = libraryList;
	}

	private List<TreeNode> rootNodes=new ArrayList<TreeNode>();
	 
	 public String getJobFlag() {
		return jobFlag;
	}

	public void setJobFlag(String jobFlag) {
		this.jobFlag = jobFlag;
	}

	public List<JobStatistics> getJobStatisticsList() {
		return jobStatisticsList;
	}

	public void setJobStatisticsList(List<JobStatistics> jobStatisticsList) {
		this.jobStatisticsList = jobStatisticsList;
	}


	private boolean showStatusGrid =  false;
	 
	 
	public boolean isShowStatusGrid() {
		return showStatusGrid;
	}

	public void setShowStatusGrid(boolean showStatusGrid) {
		this.showStatusGrid = showStatusGrid;
	}

	public List<JobStatus> getJobStatusList() {
		return jobStatusList;
	}

	public void setJobStatusList(List<JobStatus> jobStatusList) {
		this.jobStatusList = jobStatusList;
	}

	public Integer getJobId() {
		return jobId;
	}

	public void setJobId(Integer jobId) {
		this.jobId = jobId;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public List<Job> getJobs() {
		return jobs;
	}

	public void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}

	public String getFileSystemType() {
		return fileSystemType;
	}

	public void setFileSystemType(String fileSystemType) {
		this.fileSystemType = fileSystemType;
	}

	public String getLocalFSPath() {
		return localFSPath;
	}

	public void setLocalFSPath(String localFSPath) {
		this.localFSPath = localFSPath;
	}

	public List<String> getLocalFSPathsList() {
		return localFSPathsList;
	}

	public void setLocalFSPathsList(List<String> localFSPathsList) {
		this.localFSPathsList = localFSPathsList;
	}

	public String getRemoteDomainName() {
		return remoteDomainName;
	}

	public void setRemoteDomainName(String remoteDomainName) {
		this.remoteDomainName = remoteDomainName;
	}

	public String getRemoteUserName() {
		return remoteUserName;
	}

	public void setRemoteUserName(String remoteUserName) {
		this.remoteUserName = remoteUserName;
	}

	public String getRemoteUserPassword() {
		return remoteUserPassword;
	}

	public void setRemoteUserPassword(String remoteUserPassword) {
		this.remoteUserPassword = remoteUserPassword;
	}

	public String getRemoteUNC() {
		return remoteUNC;
	}

	public void setRemoteUNC(String remoteUNC) {
		this.remoteUNC = remoteUNC;
	}

	
	public List<String> getRemotePathsList() {
		return remotePathsList;
	}

	public void setRemotePathsList(List<String> remotePathsList) {
		this.remotePathsList = remotePathsList;
	}

	public List<String> getRemoteExPathsList() {
		return remoteExPathsList;
	}

	public void setRemoteExPathsList(List<String> remoteExPathsList) {
		this.remoteExPathsList = remoteExPathsList;
	}

	public List<JobStatistics> getJobStatisticsList1() {
		return jobStatisticsList1;
	}

	public void setJobStatisticsList1(List<JobStatistics> jobStatisticsList1) {
		this.jobStatisticsList1 = jobStatisticsList1;
	}

	/*
	public Date getExecutionStartDateTime() {
		return executionStartDateTime;
	}

	public void setExecutionStartDateTime(Date executionStartDateTime) {
		this.executionStartDateTime = executionStartDateTime;
	}

	public boolean isRunNow() {
		return runNow;
	}

	public void setRunNow(boolean runNow) {
		this.runNow = runNow;
	}*/

	public List<ExportJobPath> getExportPathList() {
		getExportPathsList();	//to store export paths
		return exportPathList;
	}
	
	public List<ExportJobPath> getExportPathList2() {
	//	getExportPathsList();	//to store export paths
		return exportPathList;
	}

	public void setExportPathList(List<ExportJobPath> exportPathList) {
		this.exportPathList = exportPathList;
	}

	public List<String> getExportPathStringList() {
		return exportPathStringList;
	}

	public void setExportPathStringList(List<String> exportPathStringList) {
		this.exportPathStringList = exportPathStringList;
	}

	public String getTestUNCResponse() {
		return testUNCResponse;
	}

	public void setTestUNCResponse(String testUNCResponse) {
		this.testUNCResponse = testUNCResponse;
	}

	public String getJobIdForStatus() {
		return jobIdForStatus;
	}

	public void setJobIdForStatus(String jobIdForStatus) {
		this.jobIdForStatus = jobIdForStatus;
	}
	
	public List<String> getSiteList() {
		return siteList;
	}

	public void setSiteList(List<String> siteList) {
		this.siteList = siteList;
	}

	public List<TreeNode> getRootNodes() {
		return rootNodes;
	}

	public List<Job> getJobsStatusList() {
		return jobsStatusList;
	}

	public void setJobsStatusList(List<Job> jobsStatusList) {
		this.jobsStatusList = jobsStatusList;
	}

	public String getSpAgentId() {
		return spAgentId;
	}

	public void setSpAgentId(String spAgentId) {
		this.spAgentId = spAgentId;
	}

	public List<String> getSubLibraryList() {
		return subLibraryList;
	}

	public void setSubLibraryList(List<String> subLibraryList) {
		this.subLibraryList = subLibraryList;
	}

	public String getSelectedLibrary() {
		return selectedLibrary;
	}

	public void setSelectedLibrary(String selectedLibrary) {
		this.selectedLibrary = selectedLibrary;
	}

	public String getSelectedSubLibrary() {
		return selectedSubLibrary;
	}

	public void setSelectedSubLibrary(String selectedSubLibrary) {
		this.selectedSubLibrary = selectedSubLibrary;
	}

	public String getCurrentGuid() {
		return currentGuid;
	}

	public void setCurrentGuid(String currentGuid) {
		this.currentGuid = currentGuid;
	}

	public String getCurrentLibPath() {
		return currentLibPath;
	}

	public void setCurrentLibPath(String currentLibPath) {
		this.currentLibPath = currentLibPath;
	}

	public String getSelectedPath() {
		return selectedPath;
	}

	public void setSelectedPath(String selectedPath) {
		this.selectedPath = selectedPath;
	}
	
		
	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	@ManagedProperty(value="#{statController}")
	private StatisticsController statisticsController;
	 
	 public StatisticsController getStatisticsController() {
		return statisticsController;
	}

	public void setStatisticsController(StatisticsController statisticsController) {
		this.statisticsController = statisticsController;
	}

	public String getExportNodePath() {
		return exportNodePath;
	}

	public void setExportNodePath(String exportNodePath) {
		this.exportNodePath = exportNodePath;
	}

	public List<SearchDocument> getSearchResults() {
		return searchResults;
	}

	public void setSearchResults(List<SearchDocument> searchResults) {
		this.searchResults = searchResults;
	}

	public String getSearchQuery() {
		return searchQuery;
	}

	public void setSearchQuery(String searchQuery) {
		this.searchQuery = searchQuery;
	}

	public boolean isSelectAll() {
		return selectAll;
	}

	public void setSelectAll(boolean selectAll) {
		this.selectAll = selectAll;
	}

	public boolean isPreserveFilePaths() {
		return preserveFilePaths;
	}

	public void setPreserveFilePaths(boolean preserveFilePaths) {
		this.preserveFilePaths = preserveFilePaths;
	}

	public boolean isExportDuplicates() {
		return exportDuplicates;
	}

	public void setExportDuplicates(boolean exportDuplicates) {
		this.exportDuplicates = exportDuplicates;
	}

	public String getTotalFileCount() {
		return totalFileCount;
	}

	public void setTotalFileCount(String totalFileCount) {
		this.totalFileCount = totalFileCount;
	}

	public String getTotalFileSize() {
		return totalFileSize;
	}

	public void setTotalFileSize(String totalFileSize) {
		this.totalFileSize = totalFileSize;
	}

	public String getExportJobSourcePath() {
		return exportJobSourcePath;
	}

	public void setExportJobSourcePath(String exportJobSourcePath) {
		this.exportJobSourcePath = exportJobSourcePath;
	}

	public String getExportJobDestinationPath() {
		return exportJobDestinationPath;
	}

	public void setExportJobDestinationPath(String exportJobDestinationPath) {
		this.exportJobDestinationPath = exportJobDestinationPath;
	}

	public boolean isEnableSharePoint() {
		return enableSharePoint;
	}

	public void setEnableSharePoint(boolean enableSharePoint) {
		this.enableSharePoint = enableSharePoint;
	}

	public String getTotalFileStubsSize() {
		return totalFileStubsSize;
	}

	public void setTotalFileStubsSize(String totalFileStubsSize) {
		this.totalFileStubsSize = totalFileStubsSize;
	}

	public SPTreeNode getParentNode() {
		return parentNode;
	}

	public void setParentNode(SPTreeNode parentNode) {
		this.parentNode = parentNode;
	}

	public Map<String, SPTreeNode> getSelectedLibrariesMap() {
		return selectedLibrariesMap;
	}

	public void setSelectedLibrariesMap(Map<String, SPTreeNode> selectedLibrariesMap) {
		this.selectedLibrariesMap = selectedLibrariesMap;
	}

	public boolean isUseCustomCreds() {
		return useCustomCreds;
	}

	public void setUseCustomCreds(boolean useCustomCreds) {
		this.useCustomCreds = useCustomCreds;
	}

	public SPTreeNode getCurrentNode() {
		return currentNode;
	}

	public void setCurrentNode(SPTreeNode currentNode) {
		this.currentNode = currentNode;
	}

	public boolean isShowActiveGrid() {
		return showActiveGrid;
	}

	public void setShowActiveGrid(boolean showActiveGrid) {
		this.showActiveGrid = showActiveGrid;
	}

	public String getDrivePath() {
		return drivePath;
	}

	public void setDrivePath(String drivePath) {
		this.drivePath = drivePath;
	}

	public Map<String, String> getRemotePathsMap() {
		return remotePathsMap;
	}

	public void setRemotePathsMap(Map<String, String> remotePathsMap) {
		this.remotePathsMap = remotePathsMap;
	}

	public boolean isEnableActiveArchiving() {
		return enableActiveArchiving;
	}

	public void setEnableActiveArchiving(boolean enableActiveArchiving) {
		this.enableActiveArchiving = enableActiveArchiving;
	}

	public ArrayList<String> getContentTypeList() {
		return contentTypeList;
	}

	public void setContentTypeList(ArrayList<String> contentTypeList) {
		this.contentTypeList = contentTypeList;
	}

	public ArrayList<String> getSelectedContentTypes() {
		return selectedContentTypes;
	}

	public void setSelectedContentTypes(ArrayList<String> selectedContentTypes) {
		this.selectedContentTypes = selectedContentTypes;
	}

	public HashMap<String, String> getContentTypesMap() {
		return contentTypesMap;
	}

	public void setContentTypesMap(HashMap<String, String> contentTypesMap) {
		this.contentTypesMap = contentTypesMap;
	}

	public ArrayList<String> getExportJobSourcePathList() {
		return exportJobSourcePathList;
	}

	public void setExportJobSourcePathList(ArrayList<String> exportJobSourcePathList) {
		this.exportJobSourcePathList = exportJobSourcePathList;
	}

	public ArrayList<String> getExportJobDestinationPathList() {
		return exportJobDestinationPathList;
	}

	public void setExportJobDestinationPathList(
			ArrayList<String> exportJobDestinationPathList) {
		this.exportJobDestinationPathList = exportJobDestinationPathList;
	}

	public Map<String, String> getCurrentSpLibs() {
		return currentSpLibs;
	}

	public void setCurrentSpLibs(Map<String, String> currentSpLibs) {
		this.currentSpLibs = currentSpLibs;
	}

	public boolean isShowDocTypes() {
		return showDocTypes;
	}

	public void setShowDocTypes(boolean showDocTypes) {
		this.showDocTypes = showDocTypes;
	}

	public String getIndexingType() {
		return indexingType;
	}

	public void setIndexingType(String indexingType) {
		this.indexingType = indexingType;
	}

	public String[] getDocTypes() {
		return docTypes;
	}

	public void setDocTypes(String[] docTypes) {
		this.docTypes = docTypes;
	}


	
}
