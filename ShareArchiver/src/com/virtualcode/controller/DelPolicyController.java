package com.virtualcode.controller;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.vo.DelJob;
import com.virtualcode.vo.DelPolicy;


@ManagedBean(name="delPolicyController")
@ViewScoped
public class DelPolicyController extends AbstractController {

	public DelPolicyController(){
		super();
		delPolicy=new DelPolicy();
	}
	
	private DelPolicy delPolicy;
	private List<DelPolicy> allPolicies;
	
	public DelPolicy getDelPolicy() {
		return delPolicy;
	}

	public void setDelPolicy(DelPolicy delPolicy) {
		this.delPolicy = delPolicy;
	}

	public List<DelPolicy> getAllPolicies() {
		allPolicies	=	delPolicyService.getAll();
		return allPolicies;
	}

	public void setAllPolicies(List<DelPolicy> allPolicies) {
		this.allPolicies = allPolicies;
	}
	
	
}
