package com.virtualcode.controller;

import java.io.IOException;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.virtualcode.common.VCSConstants;
import com.virtualcode.repository.monitor.ArchiveDateRunnerMonitor;
import com.virtualcode.repository.monitor.FileSizeRunnerMonitor;
import com.virtualcode.schedular.FileSizeRunner;
import com.virtualcode.vo.SystemAlerts;
 
@ManagedBean(name="toolsDownloader")
@ViewScoped
public class ToolsDownloaderController extends AbstractController implements Serializable {
			
	public ToolsDownloaderController(){		
		super();		
//		systemAlert =  new SystemAlerts();
//		FileSizeRunnerMonitor fileSizeRunnerMonitor = FileSizeRunnerMonitor.getInstance();
//		totalFilesProcessed = fileSizeRunnerMonitor.getCurrentFileCount();
//		batchJobRunning = fileSizeRunnerMonitor.isRunning();
//		
//		ArchiveDateRunnerMonitor archiveDateRunnerMonitor = ArchiveDateRunnerMonitor.getInstance();
//		totalFilesProcessedForArchiveDate = archiveDateRunnerMonitor.getCurrentFileCount();
//		batchJobRunningForArchiveDate = archiveDateRunnerMonitor.isRunning();
		
	}
	
	public void downloadAgent(){
		String url = "tools/SaAgent.zip";
		try {
			getHttpResponse().sendRedirect(url);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void downloadScript(){
		String url = "tools/SetComputerPass2.zip";
		try {
			getHttpResponse().sendRedirect(url);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void downloadStubbingAgent(){
		String url = "tools/ShareArchiverFileStubbing(x86)v1.4.msi";
		try {
			getHttpResponse().sendRedirect(url);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
