package com.virtualcode.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.virtualcode.agent.das.dataArchiver.CIFSEvaluater;
import com.virtualcode.agent.das.dataArchiver.FileArchiver;
import com.virtualcode.agent.das.dataArchiver.FileExecutors;
import com.virtualcode.agent.das.dataArchiver.FileStubber;
import com.virtualcode.agent.das.dataArchiver.TaskStatistic;
import com.virtualcode.agent.das.dataExporter.ExportEvaluater;
import com.virtualcode.agent.das.dataExporter.ExportExecutors;
import com.virtualcode.agent.das.dataExporter.FileExporter;
import com.virtualcode.agent.das.dataExporter.TaskStatisticsExport;
import com.virtualcode.agent.das.dataIndexer.FileIndexer;
import com.virtualcode.agent.das.dataIndexer.IndexEvaluater;
import com.virtualcode.agent.das.dataIndexer.TaskStatisticsIndex;
import com.virtualcode.agent.das.dataRestorer.FileRestorer;
import com.virtualcode.agent.das.dataRestorer.RestoreEvaluater;
import com.virtualcode.agent.das.dataRestorer.TaskStatisticsRestore;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.common.PLFormatException;
import com.virtualcode.util.License;
import com.virtualcode.util.PLVManager;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.SystemCode;

import java.util.*;

@ManagedBean(name="statController")
@ViewScoped
public class StatisticsController extends AbstractController{
	
	private ArrayList<String> allExecutions	=	null;
	
	public StatisticsController() {
		super();
		allExecutions	=	new ArrayList<String>();
		
	}

	public Job getJob(String executionID) {
		
		Job	job	=	ExportExecutors.currentJobs.get(executionID);
		
		if(job==null)//if this is not ExecutionID of some Export Job then look in Archive/Stub Jobs
			job	=	FileExecutors.currentExecs.get(executionID);
		
		return job;
	}
		
	public List<String> getAllExecutions() {
		ArrayList<String> jobExecutions	= new ArrayList<String>(FileExecutors.currentExecs.keySet());
		ArrayList<String> exportExecutions	= new ArrayList<String>(ExportExecutors.currentJobs.keySet());

		allExecutions.addAll(jobExecutions);
		allExecutions.addAll(exportExecutions);
		
		return allExecutions;
	}
	
	public String getColumnsPerRow() {
		return (allExecutions.size()<3) ? allExecutions.size()+"" : "3" ;
	}
	
	public String getEvaluatedCount(String executionID) {
		return CIFSEvaluater.getExecutionCount(executionID);
	}
	
	public String getArchivedCount(String executionID) {
		return FileArchiver.getExecutionCount(executionID);
	}

	public String getAlreadyArchivedCount(String executionID) {
		return FileArchiver.getAlreadyArchviedCount(executionID);
	}
	
	public String getStubbedCount(String executionID) {
		return FileStubber.getExecutionCount(executionID);
	}
	
	public String getFailedCount(String executionID) {
		return TaskStatistic.getFailedCount(executionID);
	}
	
	public String getCompletedCount(String executionID) {
		return TaskStatistic.getExecutionCount(executionID);
	}
	
	public String getTotalArchivedFilesSize(String executionID) {
		return TaskStatistic.getComulativeSize(executionID);
	}
		
	//////Following getters for EXPORT	
	public String getExportEvaluatedCount(String executionID) {
		return ExportEvaluater.getExecutionCount(executionID);
	}
	
	public String getExportedCount(String executionID) {
		return FileExporter.getExecutionCount(executionID);
	}
	
	public String getExportFailedCount(String executionID) {
		return TaskStatisticsExport.getFailedCount(executionID);
	}
	
	public String getExportCompletedCount(String executionID) {
		return TaskStatisticsExport.getExecutionCount(executionID);
	}

	public String getTotalExportedFilesSize(String executionID) {
		return TaskStatisticsExport.getComulativeSize(executionID);
	}
	

	/////Getters for Analysis/Indexing Job
	public String getAnalysedEvaluatedCount(String executionID) {
		return IndexEvaluater.getExecutionCount(executionID);
	}
	
	public String getAnalysedCount(String executionID) {
		return FileIndexer.getExecutionCount(executionID);
	}
	
	public String getAnalysedFailedCount(String executionID) {
		return TaskStatisticsIndex.getFailedCount(executionID);
	}
	
	public String getAnalysedCompletedCount(String executionID) {
		return TaskStatisticsIndex.getExecutionCount(executionID);
	}
	
	public String getTotalAnalysedFilesSize(String executionID) {
		return TaskStatisticsIndex.getComulativeSize(executionID);
	}

	/////Getters for Restore Job
	public String getRestoredEvaluatedCount(String executionID) {
		return RestoreEvaluater.getExecutionCount(executionID);
	}
	
	public String getRestoredCount(String executionID) {
		return FileRestorer.getExecutionCount(executionID);
	}
	
	public String getRestoredFailedCount(String executionID) {
		return TaskStatisticsRestore.getFailedCount(executionID);
	}
	
	public String getRestoredCompletedCount(String executionID) {
		return TaskStatisticsRestore.getExecutionCount(executionID);
	}
	
	public String getTotalRestoredFilesSize(String executionID) {
		return TaskStatisticsRestore.getComulativeSize(executionID);
	}
	
	
	
	
	
}
