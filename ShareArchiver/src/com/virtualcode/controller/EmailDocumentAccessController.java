package com.virtualcode.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import javax.jcr.SimpleCredentials;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import com.virtualcode.common.AdvancedSecureURL;
import com.virtualcode.common.VCSConstants;
import com.virtualcode.security.VirtualCodeAuthenticator;
import com.virtualcode.security.impl.ActiveDirAuthenticator;
import com.virtualcode.util.JespaUtil;
import com.virtualcode.util.PasswordGenerator;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.vo.CustomTreeNode;
import com.virtualcode.vo.DataGuardian;
import com.virtualcode.vo.LinkAccessDocuments;
import com.virtualcode.vo.LinkAccessUsers;
import com.virtualcode.vo.Role;
import com.virtualcode.vo.SystemCodeType;
import com.virtualcode.vo.User;
import org.apache.shiro.crypto.hash.Sha256Hash;

@ManagedBean(name="emailDocumentAccessController")
@ViewScoped
public class EmailDocumentAccessController extends AbstractController {
	
	public EmailDocumentAccessController(){
		   super();		   
	}
		
	private String username;
	
	private String password;
	
	private String oldPassword;	
	
	boolean refresh = true;
	
	private List<CustomTreeNode> sharedDocumentList=null;
	
	public void populateAllSharedDocuments(){
		sharedDocumentList = new ArrayList<CustomTreeNode>();
		String sessionUser = (String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
		LinkAccessUsers user = linkAccessUsersService.getLinkAccessUsersByRecipient(sessionUser);
		List<LinkAccessDocuments> docList =  linkAccessDocumentsService.getLinkAccessDocumentsByProperty("linkAccessUsers.id", user.getId());
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		for(LinkAccessDocuments doc: docList)
		{
			CustomTreeNode node = null;
			if(!doc.getUuidDocuments().startsWith("na-")){
				try{
				 node = fileExplorerService.getNodeByUUID(doc.getUuidDocuments());
				}catch(Exception ex){
					ex.printStackTrace();
					continue;
				}

				
	//			System.out.println(node.getDownloadUrl());
	//			System.out.println(doc.getUuidDocuments());
			}else{
				 node = new CustomTreeNode();
        		String downloadUrl= doc.getUuidDocuments();
        		String nodeID = com.virtualcode.util.StringUtils.decodeUrlAdvanced(downloadUrl.substring(3));
        		
        		System.out.println(nodeID);
        		nodeID = nodeID.substring(2).replaceAll("\\\\", "/");
        		String fileName = nodeID.substring(nodeID.lastIndexOf("/") + 1);
        		
        		node.setUuid(nodeID);
        		node.setName(fileName);
        		node.setDownloadUrl(downloadUrl);
        		node.setArchive(false);
			}
			if(node!=null && doc.getSharedOn()!=null)
				node.setSharedOn(df.format(doc.getSharedOn()));
			if(node!=null && doc.getLinkAccessUsers()!=null)
				node.setSharedBy(doc.getSender());
			
			sharedDocumentList.add(node);
		}
		
	}
	
    public void downloadFile(){
    	try{
    		
    		String downloadUrl = getRequestParameter("url");
    		String uid = getRequestParameter("id");
    		String docName = getRequestParameter("name");
    		String sharedBy = getRequestParameter("sender");
    		String sharedOn = getRequestParameter("sharedOn");
    		String isArchived = getRequestParameter("isArchived");
    		System.out.println("download url: "+downloadUrl + ", id: " + uid);
    		
    		List<LinkAccessDocuments> docList = null;
    		if(downloadUrl.startsWith("na-"))
    			docList = linkAccessDocumentsService.getLinkAccessDocumentsByProperty("uuidDocuments", downloadUrl);//modifcation required to select on basis of path and email
    		else
    			docList = linkAccessDocumentsService.getLinkAccessDocumentsByProperty("uuidDocuments", uid);//modifcation required to select on basis of path and email
    		
    		LinkAccessDocuments document = null;
    		if(docList!=null && docList.size()>0)
    			document = docList.get(0);
    		if(document!=null){
	    		System.out.println(document.getLinkExpireDate());
	    		if(document.getLinkExpireDate()!=null && document.getLinkExpireDate().compareTo(new Date())<0){
	    			addErrorMessage("Sorry, this link is no longer available or has expired", "Sorry, this link is no longer available  or has expired");
	    			return;
	    		}
    		}
//    		System.out.println(document.getLinkExpireDate().compareTo(new Date()));
//    		fileExplorerService.get
    		if(!downloadUrl.startsWith("na-"))
    			fileExplorerService.updateDocumentAccessLogs(uid, getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME)+"", getHttpRequest().getRemoteAddr());
    		String sessionUser = (String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
    		JespaUtil util = JespaUtil.getInstance();
    		String email = util.getUserEmail(sharedBy);
    		if(email.equals("NO_EMAIL")){
    			List userList = userService.getByUsername(sharedBy.toLowerCase());
    			if(userList!=null && userList.size()>0){
    				User user = (User)userList.get(0);
    				email = user.getEmail();
    			}    				
    		}
    		ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
    		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    		String emailBody = "Dear "+ sharedBy +", <br/><br/>";
    		emailBody += "File Name: " + docName + "<br/>";
    		emailBody += "Document ID: " + uid + "<br/><br/>";
    		emailBody += "Shared with user ID "+ sessionUser +" on Date:" + sharedOn +"<br/><br/>";
    		emailBody += "has been accessed by the recipient on Date :"+ df.format(new Date()) +"<br/><br/>";
    		emailBody += "Thank You <br/>";
			 if(ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY)!=null)
				 emailBody += ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY) + " ";
			 emailBody += "File Share Service";
//    		emailBody = "Document with ID: " + uid + " was accessed by user: " + sessionUser+", Accessed on:" + df.format(new Date());
//    		System.out.println(email);
    		mailService.sendMail(email, "Document Accessed", emailBody,null, null);
    		
			 //send notification to Data gaurdian(s)
			 List<DataGuardian> dataGuardianList = dataGuardianService.getAll();
			 for(DataGuardian dataGuardian: dataGuardianList){
				 
			
//			 if(groupGuardian!=null && groupGuardian.getEnableTranscript()==1 && groupGuardian.getGroupGuardian()!= null && groupGuardian.getGroupGuardian().length()>0)
//			 {
//		    		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		    		emailBody = "";
		    		emailBody += "A shared document has been accessed by user :"+ sessionUser + "<br/><br/>";
		    		emailBody += "Accessed on: " +  df.format(new Date()) + "<br/>";
		    		emailBody += "File Name: " + docName + "<br/>";		    		
		    		
		    		if(isArchived.equals("false")){	// no UID for remote Files
		    			emailBody += "File Path: " + uid + "<br/>";		    			
		    		}else{
		    			emailBody += "File UUID: " + uid + "<br/>";
		    			CustomTreeNode node = fileExplorerService.getNodeByUUID(uid);
		    			if(node!=null)
		    				emailBody += "File Path: " + fileExplorerService.getNodeByUUID(uid).getPath() + "<br/><br/>";
		    		}
		    		emailBody += "Shared By: " + sharedBy + "<br/>";
		    		emailBody += "Shared On: " + sharedOn + "<br/><br/>";
//		    		emailBody += "Shared by user ID "+ username +" on Date:" + df.format(new Date()) +"<br/><br/>";
//		    		emailBody += "has been accessed by the recipient on Date :"+ df.format(new Date()) +"<br/><br/>";
		    		emailBody += "Thank You <br/>";
					 if(ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY)!=null)
						 emailBody += ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY) + " ";
					 emailBody += "File Share Service";
					 String fromEmail = ru.getCodeValue("guardian_from_address", SystemCodeType.GENERAL);
					 if(fromEmail==null || fromEmail.length()<1)
						 mailService.sendMail(dataGuardian.getDataGuardianEmail(), "File Sharing Notification", emailBody,null, null);
					 else
						 mailService.sendMail(fromEmail, dataGuardian.getDataGuardianEmail(), "File Sharing Notification", emailBody,null, null);
//			 }
			 }
    		
    		
    	   String url= "DownloadFile?nodeID="+downloadUrl;

    	   getHttpResponse().sendRedirect(url);
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    }
    
    public void viewFile(){
    	try{
    		
    		String viewUrl = getRequestParameter("url");
    		String uid = getRequestParameter("id");
    		String docName = getRequestParameter("name");
    		String sharedBy = getRequestParameter("sender");
    		String sharedOn = getRequestParameter("sharedOn");
    		String isArchived = getRequestParameter("isArchived");
    		System.out.println("view url: "+viewUrl + ", id: " + uid);
    		
    		List<LinkAccessDocuments> docList = null;
    		if(viewUrl.startsWith("na-"))
    			docList = linkAccessDocumentsService.getLinkAccessDocumentsByProperty("uuidDocuments", viewUrl);//modifcation required to select on basis of path and email
    		else
    			docList = linkAccessDocumentsService.getLinkAccessDocumentsByProperty("uuidDocuments", uid);//modifcation required to select on basis of path and email
    		
    		LinkAccessDocuments document = null;
    		if(docList!=null && docList.size()>0)
    			document = docList.get(0);
    		if(document!=null){
	    		System.out.println(document.getLinkExpireDate());
	    		if(document.getLinkExpireDate()!=null && document.getLinkExpireDate().compareTo(new Date())<0){
	    			addErrorMessage("Sorry, this link is no longer available or has expired", "Sorry, this link is no longer available  or has expired");
	    			return;
	    		}
    		}
    			
    		
    		fileExplorerService.updateDocumentAccessLogs(uid, getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME)+"", getHttpRequest().getRemoteAddr());
    		String sessionUser = (String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME);
    		JespaUtil util = JespaUtil.getInstance();
    		String email = util.getUserEmail(sharedBy);
    		if(email.equals("NO_EMAIL")){
    			List userList = userService.getByUsername(sharedBy.toLowerCase());
    			if(userList!=null && userList.size()>0){
    				User user = (User)userList.get(0);
    				email = user.getEmail();
    			}    				
    		}
    		ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
//    		System.out.println(email);
    		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    		String emailBody = "Dear "+ sharedBy +", <br/><br/>";
    		emailBody += "File Name: " + docName + "<br/>";
    		emailBody += "Document ID: " + uid + "<br/><br/>";
    		emailBody += "Shared with user ID "+ sessionUser +" on Date:" + sharedOn +"<br/><br/>";
    		emailBody += "has been accessed by the recipient on Date :"+ df.format(new Date())+" <br/><br/>";
    		emailBody += "Thank You <br/>";
			 if(ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY)!=null)
				 emailBody += ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY) + " ";
			 emailBody += "File Share Service";
//    		mailService.sendMail(email, "Document Accessed", "Document with ID: " + uid + " was accessed by user: " + sessionUser,null, null);
    		mailService.sendMail(email, "Document Accessed", emailBody,null, null);
    	    String url=	"ViewFile.faces?nodeID="+viewUrl;
			 //send notification to Data gaurdian(s)
			 List<DataGuardian> dataGuardianList = dataGuardianService.getAll();
			 for(DataGuardian dataGuardian: dataGuardianList){
				 
			
//			 if(groupGuardian!=null && groupGuardian.getEnableTranscript()==1 && groupGuardian.getGroupGuardian()!= null && groupGuardian.getGroupGuardian().length()>0)
//			 {
//		    		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		    		emailBody = "";
		    		emailBody += "A shared document has been accessed by user :"+ sessionUser + "<br/><br/>";
		    		emailBody += "Accessed on: " +  df.format(new Date()) + "<br/>";
		    		emailBody += "File Name: " + docName + "<br/>";		    		
		    		
		    		if(isArchived.equals("false")){	// no UID for remote Files
		    			emailBody += "File Path: " + uid + "<br/>";		    			
		    		}else{
		    			emailBody += "File UUID: " + uid + "<br/>";
		    			CustomTreeNode node = fileExplorerService.getNodeByUUID(uid);
		    			if(node!=null)
		    				emailBody += "File Path: " + fileExplorerService.getNodeByUUID(uid).getPath() + "<br/><br/>";
		    		}
		    		emailBody += "Shared By: " + sharedBy + "<br/>";
		    		emailBody += "Shared On: " + sharedOn + "<br/><br/>";
//		    		emailBody += "Shared by user ID "+ username +" on Date:" + df.format(new Date()) +"<br/><br/>";
//		    		emailBody += "has been accessed by the recipient on Date :"+ df.format(new Date()) +"<br/><br/>";
		    		emailBody += "Thank You <br/>";
					 if(ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY)!=null)
						 emailBody += ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY) + " ";
					 emailBody += "File Share Service";
					 String fromEmail = ru.getCodeValue("guardian_from_address", SystemCodeType.GENERAL);
					 if(fromEmail==null || fromEmail.length()<1)
						 mailService.sendMail(dataGuardian.getDataGuardianEmail(), "File Sharing Notification", emailBody,null, null);
					 else
						 mailService.sendMail(fromEmail, dataGuardian.getDataGuardianEmail(), "File Sharing Notification", emailBody,null, null);
//			 }
			 }
    	    getHttpResponse().sendRedirect(url);
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    }    
	
	public void validateSession(){
		 HttpSession httpSession=getHttpSession();
	        HttpServletResponse httpResponse=getHttpResponse();	
			Object sessionUser=httpSession.getAttribute(VCSConstants.SESSION_USER_NAME);
			if(sessionUser!=null && sessionUser.toString()!=null && ! sessionUser.toString().trim().equals("")){
				//refresh = true;
			}
		
		if(FacesContext.getCurrentInstance().isPostback()){
	        //HttpSession httpSession=getHttpSession();
	        //HttpServletResponse httpResponse=getHttpResponse();	
			//Object sessionUser=httpSession.getAttribute(VCSConstants.SESSION_USER_NAME);
			
			if(sessionUser!=null && sessionUser.toString()!=null && ! sessionUser.toString().trim().equals("")){
				
				try {
					refresh = false;
					httpResponse.sendRedirect("Browse.faces");
				} catch (IOException e) {
					System.out.println("Redirection to Repository page failed."+e.getMessage());
				}
			}			
		}
		
	}
	
	public boolean isRefresh() {
		return refresh;
	}

	public void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}

	public String login(){
	   	
		
		if(username==null || username.trim().equals("") || password==null || password.trim().equals("")){
			   addErrorMessage("Please enter username and password", "Please enter username and password");
			   return null;
	   }		
		//to make username and password case in-sensitive
		username = username.toLowerCase();
	   boolean authenticate = authenticate(username, password);   
	   
	   if(!authenticate){
		   return null;
	   }	  
	   System.out.println("Session Create Time "+getHttpSession().getCreationTime());
	   System.out.println("Session Create Time in date format "+new Date(getHttpSession().getCreationTime()));
	   userActivityService.userLogin(username, getHttpRequest().getRemoteAddr(),getHttpSession().getId(),getHttpSession().getCreationTime());
	   
	   String roleName=(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_ROLE_NAME);
//	   
//	   if(roleName.equalsIgnoreCase("priv") || roleName.equalsIgnoreCase(VCSConstants.ROLE_PRIVILEGED)){
//		   return "Browse.faces?faces-redirect=true";
//	   }
	   return "EmailDocumentsList.faces?faces-redirect=true";	   
	      
	}
	
	public String logout(){
	    HttpSession httpSession=getHttpSession();
	    if(httpSession!=null){
	    	httpSession.invalidate();
	    }
		return "EmailUserLogin.faces?faces-redirect=true";
	}
	
	public void resetPassword(){
//		if(username==null || username.length()<1){
//			addErrorMessage("Please enter a valid email address", "Please enter a valid email address");
//			 return;
//		}
//		if(!VCSUtil.validateEmailAddress(username)){
//			addErrorMessage("Invalid email address.", "Invalid email address.");
//			return;
//		}
			
		AdvancedSecureURL advSecureUrl = AdvancedSecureURL.getInstance();		
		 LinkAccessUsers user = linkAccessUsersService.getLinkAccessUsersByRecipient(username);
		 String password = "";
		 if(user==null){
			 addErrorMessage("No documents have been shared with email address"+ username, "No documents have been shared with email address"+ username);
			 return;
//			 user = new LinkAccessUsers();
//			 user.setRecipient(username);
//			 password = PasswordGenerator.randomAlphaNumeric();
//			 
//			 try {
////				advSecureUrl.getEncryptedURL(password);
//				user.setPassword(advSecureUrl.getEncryptedURL(password));
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			 
//			 user.setSender(username);
			 				 
		 }else{
			 try {
				 password = PasswordGenerator.randomAlphaNumeric();
				 user.setPassword(advSecureUrl.getEncryptedURL(password));
				 
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 linkAccessUsersService.saveLinkAccessUsers(user);	
		 }
		 
		 
		 mailService.sendMail(this.username, "ShareArchiver User Credentials", "Your new ShareArchiver password is: " + password,null, null);
		 addInfoMessage("Password changed successfully, Please check your email", "Password changed successfully, Please check your email");
	}

	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}	
	
	public boolean authenticate(String username, String password){
//		List<com.virtualcode.vo.SystemCode> systemCodeList = systemCodeService.getSystemCodeByCodename("authentication.mode");
		boolean authenticate = false;
		authenticate = authenticateDB(username, password);
		
//		String mode = "1";	// set default to DB only
//		if(systemCodeList!=null && systemCodeList.size()>0){			
//			mode = systemCodeList.get(0).getCodevalue();
//		}
//		//to make username and password case in-sensitive
//		System.out.println("authentication mode: " + mode);
//		if(mode.equals(VCSConstants.AUTH_DB_ONLY)){	//DB only
//			System.out.println("Authentication mode: DB only");
//			authenticate = authenticateDB(username, password);
//		}else if(mode.equals(VCSConstants.AUTH_DB_FIRST_WITH_AD)){		//Both DB and AD -  DB first
//			System.out.println("Authentication mode: DB and AD");
//			authenticate = authenticateDB(username, password);
//			if(!authenticate){
//				System.out.println("DB authentication failed, going to AD");
//				authenticate = authenticateAD(username, password);
//			}
//		}else if(mode.equals(VCSConstants.AUTH_AD_FIRST_WITH_DB)){		//Both DB and AD - AD first
//			System.out.println("Authentication mode: DB and AD");
//			authenticate = authenticateAD(username, password);
//			if(!authenticate){
//				System.out.println("AD authentication failed, going to DB");
//				authenticate = authenticateDB(username, password);
//			}
//		}
		
		return authenticate;
		
		
	}
	
	public boolean authenticateDB(String username, String password){
		try {
			password=AdvancedSecureURL.getInstance().getEncryptedURL(password);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(linkAccessUsersService.login(username, password)){
			HttpSession httpSession=getHttpSession();
			httpSession.setAttribute(VCSConstants.SESSION_USER_ROLE_NAME,VCSConstants.ROLE_EMAIL);
			return true;			
		}
		
		
//		User user= userService.login(username, password);
//		if(user==null){
//			addErrorMessage("username/password mismatch", "username/password mismatch");
//			return false;
//		}else if(!user.getUsername().equals(username) || ! user.getPasswordHash().equals(password)){
//			addErrorMessage("username/password mismatch", "username/password mismatch");
//			return false;
//		}else if(user.getLockStatus().equalsIgnoreCase("INACTIVE")){
//			   addErrorMessage("User is inactive", "User is inactive");
//			   return false;
//		}else {
//			HttpSession httpSession=getHttpSession();
//			   
//			httpSession.setAttribute(VCSConstants.SESSION_USER_NAME, user.getUsername());
//			Iterator<Role> ite=user.getUserRoles().iterator();
//			String roleName=null;
//			while(ite.hasNext()){
//			  roleName=ite.next().getName();
//			   break;
//			}
//			httpSession.setAttribute(VCSConstants.SESSION_USER_ROLE_NAME,roleName);
//			httpSession.setAttribute(VCSConstants.SESSION_USER_AUTH_TYPE, "db");
//			return true;
//		}
		return false;
	}
	
	public boolean authenticateAD(String username, String password){
		boolean authenticate = false;
		  
		try {
			SimpleCredentials credentials = new SimpleCredentials(username, password.toCharArray());
			VirtualCodeAuthenticator authenticator = ActiveDirAuthenticator.getInstance();
	        authenticate = authenticator.authenticate(credentials);
	        
	        //log.debug("authenticate: " + authenticate);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(authenticate){
			HttpSession httpSession=getHttpSession();
			httpSession.setAttribute(VCSConstants.SESSION_USER_NAME,username);
			httpSession.setAttribute(VCSConstants.SESSION_USER_ROLE_NAME,VCSConstants.ROLE_PRIVILEGED);
			httpSession.setAttribute(VCSConstants.SESSION_USER_AUTH_TYPE, "ad");
		}
		return authenticate;
	}

	public List<CustomTreeNode> getSharedDocumentList() {
		return sharedDocumentList;
	}

	public void setSharedDocumentList(List<CustomTreeNode> sharedDocumentList) {
		this.sharedDocumentList = sharedDocumentList;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	
//    public void navigate(PhaseEvent event) {
//        FacesContext facesContext = FacesContext.getCurrentInstance();
//        String outcome = action; // Do your thing?
//        facesContext.getApplication().getNavigationHandler().handleNavigation(facesContext, null, outcome);
//    }
		
}
