package com.virtualcode.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;

import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.DocumentCategory;
import com.virtualcode.vo.DocumentType;

public class DocumentClassifyController extends AbstractController implements Serializable {
	
	public DocumentClassifyController(){
		super();
		documentTypeList=new ArrayList<DocumentType>();
	}
	
	private Integer documentCategoryId;
	
	private Integer documentTypeId;
	
    private DocumentCategory documentCategory;
	
	private DocumentType documentType;
	
	private List<DocumentType> documentTypeList;
	
	
	public List<DocumentCategory> getAllDocumentCategories(){
		return documentClassifyService.getAllDocumentCategories();
		
	}
	
	public void populateDocumentCategory(ComponentSystemEvent  event){
		if(documentCategoryId!=null && ! FacesContext.getCurrentInstance().isPostback()){
			this.documentCategory=documentClassifyService.getDocumentCategory(documentCategoryId);
			documentTypeList=documentClassifyService.getDocumentTypes(documentCategoryId);
		}
	}
	
    public void populateDocumentType(ComponentSystemEvent  event){		
		
		this.documentType=documentClassifyService.getDocumentType(documentTypeId);
		this.documentCategory=documentType.getDocumentCategory();
		//this.documentCategory=documentClassifyService.getDocumentCategory(documentType.getDocumentCategory().getId());
				
	}
    
    public String deleteDocumentCategory(){
    	if(documentClassifyService.isDocCategoryInUse(documentCategoryId)){
    		addErrorMessage("Can't delete document category,Its in use.", "Can't delete document category,Its in use.");
    		return null;
    	}
    	documentClassifyService.removeDocumentCategory(documentCategoryId);
    	return "DocumentCategories.faces?redirect=true";
    }
    
    public String deleteDocumentType(){
    	if(documentClassifyService.isDocTypeInUse(documentTypeId)){
    		addErrorMessage("Can't delete document type,Its in use.", "Can't delete document type,Its in use.");
    		return null;
    	}
    	
    	documentClassifyService.removeDocumentType(documentTypeId);
    	return "DocumentCategories.faces?redirect=true";
    }
    

    public void addDocumentTypeToList(){
    	documentCategory.getDocumentTypes().add(documentType);
    	
    	
    }
  
   //------------------------ General Getter/Setter---------------------------------------  
    
	public Integer getDocumentCategoryId() {
		return documentCategoryId;
	}

	public void setDocumentCategoryId(Integer documentCategoryId) {
		this.documentCategoryId = documentCategoryId;
	}

	public Integer getDocumentTypeId() {
		return documentTypeId;
	}

	public void setDocumentTypeId(Integer documentTypeId) {
		this.documentTypeId = documentTypeId;
	}

	public DocumentCategory getDocumentCategory() {
		return documentCategory;
	}

	public void setDocumentCategory(DocumentCategory documentCategory) {
		this.documentCategory = documentCategory;
	}

	public DocumentType getDocumentType() {
		return documentType;
	}

	public void setDocumentType(DocumentType documentType) {
		this.documentType = documentType;
	}

	public List<DocumentType> getDocumentTypeList() {
		return documentTypeList;
	}

	public void setDocumentTypeList(List<DocumentType> documentTypeList) {
		this.documentTypeList = documentTypeList;
	}
    
        	

}
