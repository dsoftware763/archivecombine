package com.virtualcode.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import javax.jcr.SimpleCredentials;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import com.virtualcode.common.VCSConstants;
import com.virtualcode.security.VirtualCodeAuthenticator;
import com.virtualcode.security.impl.ActiveDirAuthenticator;
import com.virtualcode.util.PasswordGenerator;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.vo.LockedUser;
import com.virtualcode.vo.Role;
import com.virtualcode.vo.SystemCodeType;
import com.virtualcode.vo.User;

import org.ajax4jsf.Messages;
import org.apache.shiro.crypto.hash.Sha256Hash;

@ManagedBean(name="loginController")
@ViewScoped
public class LoginController extends AbstractController {
	
	public LoginController(){
		   super();		   
	}
		
	String username;
	
	String password;
	
	boolean refresh = true;
	
	boolean passwordExpired = false;
	
	String emailStatus = null;
	
	public void validateSession(){
		 HttpSession httpSession=getHttpSession();
	        HttpServletResponse httpResponse=getHttpResponse();	
			Object sessionUser=httpSession.getAttribute(VCSConstants.SESSION_USER_NAME);
			if(sessionUser!=null && sessionUser.toString()!=null && ! sessionUser.toString().trim().equals("")){
				//refresh = true;
			}

			if(!FacesContext.getCurrentInstance().isPostback()){
				System.out.println(httpSession.getAttribute("passwordExpired"));
//				boolean passwordExpiredStr =(boolean)httpSession.getAttribute("passwordExpired");
				if(httpSession.getAttribute("passwordExpired")!=null){
					passwordExpired = (boolean)httpSession.getAttribute("passwordExpired");// (passwordExpiredStr!=null && passwordExpiredStr.equals("true"));				
				}
			}
			
		
		if(FacesContext.getCurrentInstance().isPostback()){
	        //HttpSession httpSession=getHttpSession();
	        //HttpServletResponse httpResponse=getHttpResponse();	
			//Object sessionUser=httpSession.getAttribute(VCSConstants.SESSION_USER_NAME);

			if(sessionUser!=null && sessionUser.toString()!=null && ! sessionUser.toString().trim().equals("")){
				
				try {
					refresh = false;
					httpResponse.sendRedirect("Browse.faces");
				} catch (IOException e) {
					System.out.println("Redirection to Repository page failed."+e.getMessage());
				}
			}			
		}			
		
	}
	
	public boolean isRefresh() {
		return refresh;
	}

	public void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}

	public String login(){
	   	
		
		if(username==null || username.trim().equals("") || password==null || password.trim().equals("")){
			   addErrorMessage("Please enter username and password", "Please enter username and password");
//			   ResourceBundle bundle = ResourceBundle.getBundle("com.virtualcode.messages.messages");
//			   addErrorMessage(bundle.getString("default.button.username.confirm.message"), "Please enter username and password");
			   return null;
	   }		
		//to make username and password case in-sensitive
		username = username.toLowerCase();
	   boolean authenticate = authenticate(username, password);   
	   
	   if(!authenticate){
		   return null;
	   }
	   if(LockedUser.lockedUsersMap!=null)
		   LockedUser.lockedUsersMap.remove(username);	// remove if login successfull (if not a db user)
	   System.out.println("Session Create Time "+getHttpSession().getCreationTime());
	   System.out.println("Session Create Time in date format "+new Date(getHttpSession().getCreationTime()));
	   userActivityService.userLogin(username, getHttpRequest().getRemoteAddr(),getHttpSession().getId(),getHttpSession().getCreationTime());
	   
	   String roleName=(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_ROLE_NAME);
	   
	   if(passwordExpired){
		   addErrorMessage("Your password has expired", "Your password has expired");
		   return "login.faces";
	   }
	   
	   if(roleName.equalsIgnoreCase("priv") || roleName.equalsIgnoreCase(VCSConstants.ROLE_PRIVILEGED)){
		   return "Browse.faces?faces-redirect=true";
	   }	   
	   
	   return "Status.faces?faces-redirect=true";	   
	      
	}
	
	
	public String logout(){
	    HttpSession httpSession=getHttpSession();
	    if(httpSession!=null){
	    	httpSession.invalidate();
	    }
		return "login.faces?faces-redirect=true";
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}	
	
	public boolean authenticate(String username, String password){
		List<com.virtualcode.vo.SystemCode> systemCodeList = systemCodeService.getSystemCodeByCodename("authentication.mode");
		boolean authenticate = false;
		
		String mode = "1";	// set default to DB only
		if(systemCodeList!=null && systemCodeList.size()>0){			
			mode = systemCodeList.get(0).getCodevalue();
		}
		//to make username and password case in-sensitive
		System.out.println("authentication mode: " + mode);
		if(mode.equals(VCSConstants.AUTH_DB_ONLY)){	//DB only
			System.out.println("Authentication mode: DB only");
			authenticate = authenticateDB(username, password);
		}else if(mode.equals(VCSConstants.AUTH_DB_FIRST_WITH_AD)){		//Both DB and AD -  DB first
			System.out.println("Authentication mode: DB and AD");
			authenticate = authenticateDB(username, password);
			if(!authenticate){
				System.out.println("DB authentication failed, going to AD");
				authenticate = authenticateAD(username, password);
			}
		}else if(mode.equals(VCSConstants.AUTH_AD_FIRST_WITH_DB)){		//Both DB and AD - AD first
			System.out.println("Authentication mode: DB and AD");
			authenticate = authenticateAD(username, password);
			if(!authenticate){
				System.out.println("AD authentication failed, going to DB");
				authenticate = authenticateDB(username, password);
			}
		}
		
		return authenticate;
		
		
	}
	
	public boolean authenticateDB(String username, String password){
		password=new Sha256Hash(password).toHex();
		if(isUserLocked(username)){
			ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
			int lockoutTimeOut = 5; // 5 min
			if(ru.getCodeValue("account_lock_timeout_period", SystemCodeType.ACCOUNT_POLICY)!=null){
				lockoutTimeOut = Integer.parseInt(ru.getCodeValue("account_lock_timeout_period", SystemCodeType.ACCOUNT_POLICY));				
			}
			addErrorMessage("Your account has been locked, Please try after "+ lockoutTimeOut +" minutes", "Your account has been locked, Please try after a few minutes");
			return false;
		}
		User user= userService.login(username, password);
		if(user==null){
			addErrorMessage("username/password mismatch", "username/password mismatch");
			processUserLockout(username);
			return false;
		}else if(!user.getUsername().equals(username) || ! user.getPasswordHash().equals(password)){
			addErrorMessage("username/password mismatch", "username/password mismatch");
			processUserLockout(username);
			return false;
		}else if(user.getLockStatus().equalsIgnoreCase("INACTIVE")){
			   addErrorMessage("User is inactive", "User is inactive");
			   return false;
		}else {
			HttpSession httpSession=getHttpSession();
			   
			httpSession.setAttribute(VCSConstants.SESSION_USER_NAME, user.getUsername());
			Iterator<Role> ite=user.getUserRoles().iterator();
			String roleName=null;
			while(ite.hasNext()){
			  roleName=ite.next().getName();
			   break;
			}
			httpSession.setAttribute(VCSConstants.SESSION_USER_ROLE_NAME,roleName);
			httpSession.setAttribute(VCSConstants.SESSION_USER_AUTH_TYPE, "db");
			if(LockedUser.lockedUsersMap!=null)
				LockedUser.lockedUsersMap.remove(username);
			
			
			if(user.getLastPasswordChange()==null){
				user.setLastPasswordChange(new Timestamp(System.currentTimeMillis()));
				userService.save(user);	// for existing users
			}else{
				java.sql.Timestamp lastPasswordChange = user.getLastPasswordChange();
				Date passwordChangeDate = new Date(lastPasswordChange.getTime());
				//check if password has expired
				ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
				long passwordExpiryTime = 30; //days
				if(ru.getCodeValue("password_expiry_time", SystemCodeType.ACCOUNT_POLICY)!=null)
					passwordExpiryTime = Long.parseLong(ru.getCodeValue("password_expiry_time", SystemCodeType.ACCOUNT_POLICY));
				
				Date passwordExpiryDate = new Date(passwordChangeDate.getTime() + (passwordExpiryTime * VCSConstants.ONE_DAY_IN_MILLIS));
				System.out.println(passwordExpiryDate);
				Date currentDate = new Date();
				if(passwordExpiryTime>0 && currentDate.after(passwordExpiryDate)){
					System.out.println("password expired");
					addErrorMessage("You password has expired", "You password has expired");
					getHttpSession().setAttribute("passwordExpired", true);
					passwordExpired = true;
//					return false;
				}else{
					passwordExpired = false;
				}
					
			}
			
			return true;
		}
	}
	
	public boolean authenticateAD(String username, String password){
		boolean authenticate = false;
		  
		try {
			SimpleCredentials credentials = new SimpleCredentials(username, password.toCharArray());
			VirtualCodeAuthenticator authenticator = ActiveDirAuthenticator.getInstance();
	        authenticate = authenticator.authenticate(credentials);
	        
	        //log.debug("authenticate: " + authenticate);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(authenticate){
			HttpSession httpSession=getHttpSession();
			httpSession.setAttribute(VCSConstants.SESSION_USER_NAME,username);
			httpSession.setAttribute(VCSConstants.SESSION_USER_ROLE_NAME,VCSConstants.ROLE_PRIVILEGED);
			httpSession.setAttribute(VCSConstants.SESSION_USER_AUTH_TYPE, "ad");
		}
		return authenticate;
	}	
	
	private void processUserLockout(String username){
		if(LockedUser.lockedUsersMap==null)
			LockedUser.lockedUsersMap = new HashMap<String, LockedUser>();
		LockedUser user = LockedUser.lockedUsersMap.get(username);
		if(LockedUser.lockedUsersMap.get(username)!=null){
			int maxNoofTries = 3;
			user.incNoOfTries();
			ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
			if(ru.getCodeValue("no_of_tries_befor_account_lock", SystemCodeType.ACCOUNT_POLICY)!=null){
				maxNoofTries = Integer.parseInt(ru.getCodeValue("no_of_tries_befor_account_lock", SystemCodeType.ACCOUNT_POLICY));				
			}
			if(user.getNoOfTries()>=maxNoofTries){
				user.setLockoutTime(new Date(System.currentTimeMillis()));
				user.setLockStatus(true);
			}else{
				user.incNoOfTries();
			}
			
		}else{
			user = new LockedUser();
			user.setUsername(username);
			user.incNoOfTries();			
		}
		LockedUser.lockedUsersMap.put(username, user);
//		return user.isLockStatus();
	}
	
	private boolean isUserLocked(String username){
		if(LockedUser.lockedUsersMap==null)
			return false;
		if(LockedUser.lockedUsersMap.get(username)!=null){
			LockedUser user = LockedUser.lockedUsersMap.get(username);
			if(user.isLockStatus()){	// check if lockout has timed out
				ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
				int lockoutTimeOut = 5; // 5 min
				if(ru.getCodeValue("account_lock_timeout_period", SystemCodeType.ACCOUNT_POLICY)!=null){
					lockoutTimeOut = Integer.parseInt(ru.getCodeValue("account_lock_timeout_period", SystemCodeType.ACCOUNT_POLICY));				
				}
				Date lockoutTime = user.getLockoutTime();
				Date lockoutExpireTime = new Date(lockoutTime.getTime() + (lockoutTimeOut * VCSConstants.ONE_MINUTE_IN_MILLIS)); //60000 to convert min to ms
				Date currentTime = new Date(System.currentTimeMillis());
				System.out.println(currentTime + ", " + lockoutExpireTime);
				if(currentTime.after(lockoutExpireTime)){					
					LockedUser.lockedUsersMap.remove(username);
					System.out.println("Lock released for: " + username);
					return false;
				}
				
				
			}
			return user.isLockStatus();
		}			
		return false;
	}
	
	public void sendResetEmail(){
		
		List<User> userList = userService.getByUsername(getHttpSession().getAttribute(VCSConstants.SESSION_USER_NAME));
		User user = null;
		if(userList!=null && userList.size()>0)
			user = userList.get(0);
		
		 try {
			 password = PasswordGenerator.randomAlphaNumeric();
			 user.setPasswordHash(new Sha256Hash(password).toHex());
			 user.setLastPasswordChange(new Timestamp(System.currentTimeMillis()));
			 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 userService.save(user);
	 
	 ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
	 String emailBody = "Your new account password is set to \""+ password +"\". Please use the new password to log into your ShareArchiver account.<br/><br/>";
	 if(ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY)!=null)
	 	emailBody += ru.getCodeValue("company_prefered_name", SystemCodeType.COMPANY) + " ";
	 emailBody += "Account Managment Service";
	 boolean status = mailService.sendMail(user.getEmail(), "ShareArchiver User Credentials", emailBody,null, null);
	 if(status){
		 addInfoMessage("password has been reset and sent to registered email address", "password has been reset and sent to registered email address");
	 }else{

		 addInfoMessage("email could not be sent, please contact your administrator or applicatoin support personnel", "email could not be sent, please contact your administrator or applicatoin support personnel");
	 }
	 
//	 addInfoMessage("A generated password has been sent to your email address, please use it to log into ShareArchiver", "A generated password has been sent to your email address, please use it to log into ShareArchiver");
	 passwordExpired = false;
	 getHttpSession().removeAttribute("passwordExpired");
	 logout();
	}

	public boolean isPasswordExpired() {
		return passwordExpired;
	}

	public void setPasswordExpired(boolean passwordExpired) {
		this.passwordExpired = passwordExpired;
	}

	public String getEmailStatus() {
		return emailStatus;
	}

	public void setEmailStatus(String emailStatus) {
		this.emailStatus = emailStatus;
	}
	
	
	
//    public void navigate(PhaseEvent event) {
//        FacesContext facesContext = FacesContext.getCurrentInstance();
//        String outcome = action; // Do your thing?
//        facesContext.getApplication().getNavigationHandler().handleNavigation(facesContext, null, outcome);
//    }
		
}
