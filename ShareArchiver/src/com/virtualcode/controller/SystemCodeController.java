package com.virtualcode.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.naming.NamingException;
import javax.naming.ldap.Control;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.PagedResultsControl;
import javax.naming.ldap.PagedResultsResponseControl;

import jespa.ldap.LdapEntry;
import jespa.ldap.LdapSecurityProvider;
import jespa.security.SecurityProviderException;

import com.virtualcode.security.impl.ActiveDirAuthenticator;
import com.virtualcode.services.impl.MailServiceImpl;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.SpringApplicationContext;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.Agent;
import com.virtualcode.vo.SystemCode;
import com.virtualcode.vo.SystemCodeType;
 
@ManagedBean(name="systemCodeController")
@ViewScoped
public class SystemCodeController extends AbstractController implements Serializable {
			
	public SystemCodeController(){		
		super();
		agent=new Agent();
		systemCode=new SystemCode();
	}
	
	private Integer systemCodeId;
	
    private Agent agent;
    
    private SystemCode systemCode;

    //domain name settings for host file "resolv.conf" in linux
    private String domainName;
    
    private String ipAddress1;
    
    private String ipAddress2;
    //end
    
    //ldap settings
    private String bindStr;
    
    private String dnsServer;
    
    private String dnsSite;
    
    private String acctUsername;
    
    private String acctPassword;
    
    private String domain;
    
    private String restoreDomainName;
    
    private String restoreUser;
    
    private String restorePassword;
    
    private String authenticationMode;

    //agent properties
    private String agentName;
    
    private String maxNoOfTries;
        
    private boolean enableStubAuth;
    
    private boolean enableSso;
    
    private boolean authenticationSuccess = false;
    
    private String maxSearchResults;
    
    private String stubIconFilePath;
    
    private String applicationPath; //DASUrl for agent
    
    private boolean enableDriveLetter;

    private boolean enablePrivExport;
    
    private String maxResultsToExport;
    
    private boolean enablePrivPrintReport;
    
    //private boolean enableDocumentTagging;
    
    //private boolean showTagInSearchResults;
    
    private String showUserAuditHistory;
    
    private String jobLoggingPath;
    
    private String dnsName;
    private int httpPort=80;
    private int httpsPort=443;
    
    private int stubProtocol=1;
    
    private boolean enableSharePoint=false;
    
    private boolean enablePrintArchiving=false;
    
    private String accessDeniedMessage=null;
    
    private String documentNotFoundMessage=null;
    
    //private boolean enableFolderSearch = false;
   
    //private boolean enableVideoStreaming = false;
    
    private boolean enableStatusMail=false;
    // private boolean enableLinksSharing=false;
    // private boolean enableFileSharing=false;
    
    private String emailFrom;
    private String emailTo;
    private String emailUserName;
    private String emailPassword;
    private String smptHost;
    private Integer smptPort=0;
	private String emailSecProtocol="SSL";
	
	private Integer maxAttachmentSize;
    
    // private boolean enableContentTypeSearch=false;
    
    private String stubAccessMode;
    
    //job status page configs
    
    private boolean enableArchivingTab;
    private boolean enableStubbingTab;
    private boolean enableEvaluationTab;
    private boolean enableAnalysisTab;
    private boolean enableExportTab;
    private boolean enableActiveArchivingTab;
    private boolean enableDynamicArchicingTab;
    private boolean enableRetentiontab;
    private boolean enableSchedulingtab;
    private boolean enableWithoutStubtab;
    
    private String licenseStr;
    
    private boolean enableThumbnailsForSearch = false;    
    private boolean enableThumbnailsForStubs = false;    
    private boolean enableThumbnailsForSummary = false;    
    private boolean enableThumbnailsForFileList = false;
    
    private boolean showPathInSearchResults = false;
    private boolean showExcerptInSearchResults = false;
    
    private boolean enableDefaultEmailClient = false;
    
    private String maxNoOfVersions;
    
    private boolean lazyArchiving;
    
    private boolean lazyExport;
    
    private boolean includeLinkedFiles = false;		//for search only
    
    private boolean calculateFolderSize = false;
    
    private boolean enableCifsFileSharing =  false;
    
    private boolean copyTargetFileToArchive = true;
    
    private String maxBatchSize;
    
    private boolean enableFileSharingSecurity = false;
    
    private String maxAnalysisResults = "99999";
    
    private boolean emulateEmailSender;
    
    //company configurations
    
    private String companyConfigName;
    
    private String companyPreferedName;
    
    private String companyAddress;
    
    private String companyTelephone;
    
    private String companySupportContact;
    
    private String companySupportEmail;
    
    private boolean restrictCifsFilesSharingAD;
    
    private boolean restrictArchiveFilesSharingAD;
    
    private String sharedLinkValidity;
    
    private String guardianFromAddress;
    
    private boolean enableSearchByArchiveDate;
    
    private String serverWarningLimit;
    
    private String serverCriticalLimit;
    
    private boolean enableActiveArchivingNotification;
    
    private boolean enableCifsSearch;
    
    private boolean enableRepoSearch;
    
    private String passwordExpiryTime;
    
    private String noOfTriesBeforAccountLock;
    
    private String accountLockTimeoutPeriod;
    
    private String passwordMinimumLength;
    
    private boolean enforceCapsInPasswords;
    
    private boolean enforceNumbersInPasswords;
    
    private boolean enforceSpecialCharsInPasswords;
    
    
    
    
	public List<SystemCode> getAllSystemCodes(){		
		List<SystemCode> list= systemCodeService.getAll();
		System.out.println("Total SystemCode Records found: "+list.size());
		return list;
	}
	
	public List<SystemCode> getAllGeneralCodes(){		
		List<SystemCode> generalCodesList=systemCodeService.getAllGeneralCodes();
		List<SystemCode> agentCodesList=systemCodeService.getAllAgentCodes();		
		if(generalCodesList!=null){
			generalCodesList.addAll(agentCodesList);
		}
		
		return generalCodesList;		
	}
	
	public List<SystemCode> getAllLDAPCodes(){		
		return  systemCodeService.getAllLDAPCodes();
	}
	
	
	public List<SystemCode> getAllLdapSettings(){
		List<SystemCode> ldapList = systemCodeService.getSystemCodeByCodetype(SystemCodeType.ACTIVE_DIR_AUTH);
		
		SystemCode enableStubAuth = systemCodeService.getSystemCodeByName("enable.stub.authorization");
		if(enableStubAuth!=null){
			System.out.println("enableStubAuth: " + enableStubAuth.getCodevalue());
			ldapList.add(enableStubAuth);
		}
		
		SystemCode enableSso = systemCodeService.getSystemCodeByName("authentication.enable.sso");
		if(enableSso!=null){
			System.out.println("enableSso: " + enableSso.getCodevalue());
			ldapList.add(enableSso);
		}
		
		SystemCode authMode = systemCodeService.getSystemCodeByName("authentication.mode");
		if(authMode!=null){
			System.out.println("enableSso: " + authMode.getCodevalue());
			ldapList.add(authMode);
		}
		
		SystemCode maxSearchResults = systemCodeService.getSystemCodeByName("MAX_SEARCH_RESULTS");
		if(authMode!=null){
			System.out.println("max search results: " + maxSearchResults.getCodevalue());
			ldapList.add(maxSearchResults);
		}
		
		return ldapList;
	}
	public void populateSystemCodeInfo(){
		System.out.println("------"+enablePrivExport);
		if(systemCodeId!=null && systemCodeId.intValue()>0  && ! FacesContext.getCurrentInstance().isPostback()){
			systemCode=systemCodeService.getSystemCodeById(systemCodeId);
		}
	}
	
	
	public void testLDAPConfigured(){
		if(authenticationMode!=null && ! authenticationMode.equals("1") && !isLDAPConfigured()){
			addErrorMessage("Missing LDAP Configuration", "Missing LDAP Configuration");
		}
	}
	
	public void testEmailConfiguration(){
		    MailServiceImpl mailService=((com.virtualcode.services.impl.MailServiceImpl)SpringApplicationContext.getBean("mailService"));
		   		   
		    mailService.reInitilize(smptHost,smptPort,emailUserName,emailPassword,emailFrom,emailSecProtocol);
		    if(mailService.sendTestMail(smptHost, smptPort, emailSecProtocol, emailUserName, emailPassword, emailFrom, emailTo)){
			    addCompInfoMessage("globalConfigForm:testEmailBtn", "Email configuration verified", "Email configuration verified");
		       //addInfoMessage("Email configuration verified", "Email configuration verified");
		    }else{
		    	addCompErrorMessage("globalConfigForm:testEmailBtn", "Invalid email configuration", "Invalid email configuration");
		    	//addErrorMessage("Invalid email configuration", "Invalid email configuration");
		    }
		
	}
	
	public boolean isLDAPConfigured(){
		    boolean configured=true;
    	   
		    systemCode = systemCodeService.getSystemCodeByName("bindstr");
			if(systemCode==null || systemCode.getCodevalue()==null || systemCode.getCodevalue().trim().equals("") ){
				configured=false;
			}
			//dns.site
			systemCode = systemCodeService.getSystemCodeByName("dns.site");
			if(systemCode==null || systemCode.getCodevalue()==null ||systemCode.getCodevalue().trim().equals("")){
				configured=false;
			}
			//jespa.dns.site
			systemCode = systemCodeService.getSystemCodeByName("jespa.dns.site");
			if(systemCode==null || systemCode.getCodevalue()==null ||systemCode.getCodevalue().trim().equals("")){
				configured=false;
			}
			//service.acctname
			systemCode = systemCodeService.getSystemCodeByName("service.acctname");
			if(systemCode==null || systemCode.getCodevalue()==null ||systemCode.getCodevalue().trim().equals("")){
				configured=false;
			}
			systemCode = systemCodeService.getSystemCodeByName("service.password");
			if(systemCode==null || systemCode.getCodevalue()==null ||systemCode.getCodevalue().trim().equals("")){
				configured=false;
			}
			
			System.out.println("LDAP Configured: -->"+configured);
			
		 return configured;
	}
	
	
    public void editLDAPConfiguration(){
    	if(FacesContext.getCurrentInstance().isPostback()){
    		return;
    	}
    	   List<SystemCode> list=systemCodeService.getAllLDAPCodes();
    	   SystemCode systemCode=null;
    	   getDomainNameSettings();
    	   
    	   for(int i=0;list!=null && i<list.size();i++){
    	    	
    	    	systemCode=list.get(i);
    	    	if(systemCode==null)continue;//Go to next code in iteration.
    	    	if(systemCode.getCodename().equals("bindstr")){
    				bindStr = systemCode.getCodevalue();
    			}
    	    	
    	    	if(systemCode.getCodename().equals("dns.servers")){
    				dnsServer = systemCode.getCodevalue();
    			}
    	    	if(systemCode.getCodename().equals("dns.site")){
    				dnsSite = systemCode.getCodevalue();
    			}
    	    	if(systemCode.getCodename().equals("service.acctname")){
    				acctUsername = systemCode.getCodevalue();
    			}
    	    	if(systemCode.getCodename().equals("service.password")){
    				acctPassword = systemCode.getCodevalue();
    			}
    	    	if(systemCode.getCodename().equals("domain.name")){
    				domain = systemCode.getCodevalue();
    			}    	    	
    	    		    	
    	  }		
    }
    
    public void editGlobalConfiguration(){
		
	   List<SystemCode> list=systemCodeService.getAllGeneralCodes();
	   List<SystemCode> agentCodesList=systemCodeService.getAllAgentCodes();
	   if(list!=null){
		   list.addAll(agentCodesList);
	   }
   	   SystemCode systemCode=null;
   	   getDomainNameSettings();
   	   
   	   for(int i=0;list!=null && i<list.size();i++){
   	    	
   	    	systemCode=list.get(i);
   	    	if(systemCode==null)continue;//Go to next code in iteration.
   	    	
   	    	if(systemCode.getCodename().equals("restore.domain_name")){
				restoreDomainName = systemCode.getCodevalue();
			}
	    	if(systemCode.getCodename().equals("restore.username")){
				restoreUser = systemCode.getCodevalue();
			}
	    	if(systemCode.getCodename().equals("restore.password")){
				restorePassword = systemCode.getCodevalue();
			}
   	    	
   	    	
   	    	if(systemCode.getCodename().equals("authentication.mode")){
   	    		authenticationMode = systemCode.getCodevalue();
   			}
   	    	
   	    	if(systemCode.getCodename().equals("AgentName")){
   	    		agentName = systemCode.getCodevalue();
   			}
   	    	if(systemCode.getCodename().equals("MAX_NO_OF_TRIES")){
   	    		maxNoOfTries = systemCode.getCodevalue();
   			}
   	    	if(systemCode.getCodename().equals("MAX_SEARCH_RESULTS")){
   	    		maxSearchResults = systemCode.getCodevalue();
   			}
   	    	if(systemCode.getCodename().equals("StubPath")){
   	    		stubIconFilePath = systemCode.getCodevalue().replaceAll("\\\\\\\\", "\\\\");
   			}
   	    	if(systemCode.getCodename().equals("DASUrl")){
   	    		 //http://localhost:8080/ShareAchiver
   	    		String url=systemCode.getCodevalue();
   	    	
   	    		if(url!=null && url.indexOf("http://")!=-1){
   	    			url=url.substring(7);
   	    			stubProtocol=1;
   	    			if(url.indexOf(":")>0){
   	    				dnsName=url.substring(0,url.indexOf(":"));
   	    				httpPort=VCSUtil.getIntValue(url.substring(url.indexOf(":")+1,url.indexOf("/")));
   	    			}else{
   	    				dnsName=url.substring(0,url.indexOf("/"));
   	    				httpPort=80;
   	    			}
   	    		}
   	    		
   	    		if(url!=null && url.indexOf("https://")!=-1){
   	    			stubProtocol=2;
   	    			url=url.substring(8);
   	    			if(url.indexOf(":")>0){
   	    				dnsName=url.substring(0,url.indexOf(":"));
   	    				httpsPort=VCSUtil.getIntValue(url.substring(url.indexOf(":")+1,url.indexOf("/")));
   	    			}else{
   	    				dnsName=url.substring(0,url.indexOf("/"));
   	    				httpsPort=443;
   	    			}
   	    		}
   	    		//applicationPath = systemCode.getCodevalue();
   			}
   	    	if(systemCode.getCodename().equals("enable.stub.driveLetter")){
   	    		enableDriveLetter = systemCode.getCodevalue().equals("yes")?true:false;
   			}
   	    	
   	    	if(systemCode.getCodename().equals("enable.priv.export")){
   	    		enablePrivExport = systemCode.getCodevalue().equals("yes")?true:false;
   			}
   	    	if(systemCode.getCodename().equals("MAX_RESULTS_TO_EXPORT")){
   	    		maxResultsToExport = systemCode.getCodevalue();
   			}
   	    	if(systemCode.getCodename().equals("enable.priv.report")){
   	    		enablePrivPrintReport = systemCode.getCodevalue().equals("yes")?true:false;
   			}
//   	    	if(systemCode.getCodename().equals("enable.document.tagging")){
//   	    		enableDocumentTagging = systemCode.getCodevalue().equals("yes")?true:false;
//   			}
//   	    	if(systemCode.getCodename().equals("show.tags.searchResults")){
//   	    		showTagInSearchResults = systemCode.getCodevalue().equals("yes")?true:false;
//   			}
   	    	if(systemCode.getCodename().equals("show.user.auditHistory")){
   	    		showUserAuditHistory = systemCode.getCodevalue();
   			}
//   	    	if(systemCode.getCodename().equals("enable.document.tagging")){
//   	    		enableDocumentTagging = systemCode.getCodevalue().equals("yes")?true:false;
//   			}
//   	    	if(systemCode.getCodename().equals("show.tags.searchResults")){
//   	    		showTagInSearchResults = systemCode.getCodevalue().equals("yes")?true:false;
//   			}
   	    	if(systemCode.getCodename().equals("accessDeniedMessage")){
   	    		accessDeniedMessage = systemCode.getCodevalue();
   			}
   	    	if(systemCode.getCodename().equals("documentNotFoundMessage")){
   	    		documentNotFoundMessage = systemCode.getCodevalue();
   			}
   	    	
   	    	if(systemCode.getCodename().equals("show.user.auditHistory")){
   	    		showUserAuditHistory = systemCode.getCodevalue();
   			}
   	    	if(systemCode.getCodename().equals("logBasePath")){
   	    		jobLoggingPath = systemCode.getCodevalue();
   			}  
   	    	
   	    	if(systemCode.getCodename().equals("enable.stub.authorization")){
	    		enableStubAuth = systemCode.getCodevalue().equals("yes")?true:false;
			}
	    	if(systemCode.getCodename().equals("authentication.enable.sso")){
	    		enableSso = systemCode.getCodevalue().equals("yes")?true:false;
			}   
	    	
	    	if(systemCode.getCodename().equals("enable_sharepoint")){
	    		enableSharePoint = systemCode.getCodevalue().equals("yes")?true:false;
			}
	    	
	    	if(systemCode.getCodename().equals("enable_print_archiving")){
	    		enablePrintArchiving = systemCode.getCodevalue().equals("yes")?true:false;
			} 
	    	
	    	if(systemCode.getCodename().equals("LazyArchiving")){
	    		lazyArchiving = systemCode.getCodevalue().equals("true")?true:false;
			}
	    	
	    	if(systemCode.getCodename().equals("LazyExport")){
	    		lazyExport = systemCode.getCodevalue().equals("true")?true:false;
			}
	    	
	    	if(systemCode.getCodename().equals("include_linked_files")){
	    		includeLinkedFiles = systemCode.getCodevalue().equals("yes")?true:false;
			}
	    	if(systemCode.getCodename().equals("calculate_folder_size")){
	    		calculateFolderSize = systemCode.getCodevalue().equals("yes")?true:false;
			}
	    	
	    	if(systemCode.getCodename().equals("enable_cifs_file_sharing")){
	    		enableCifsFileSharing = systemCode.getCodevalue().equals("yes")?true:false;
			}
	    	
	    	if(systemCode.getCodename().equals("copy_target_file_to_archive")){
	    		copyTargetFileToArchive = systemCode.getCodevalue().equals("yes")?true:false;
			}
	    	
	    	if(systemCode.getCodename().equals("enable_file_sharing_security")){
	    		enableFileSharingSecurity = systemCode.getCodevalue().equals("yes")?true:false;
			}
	    	
	    	if(systemCode.getCodename().equals("MAX_ANALYSIS_RESULTS")){
	    		maxAnalysisResults = systemCode.getCodevalue();
			}
	    	
//	    	if(systemCode.getCodename().equals("enable_folder_based_search")){
//	    		enableFolderSearch = systemCode.getCodevalue().equals("yes")?true:false;
//			} 
//	    	
//	    	if(systemCode.getCodename().equals("enable_video_streaming")){
//	    		enableVideoStreaming = systemCode.getCodevalue().equals("yes")?true:false;
//			} 
	    	
	    	if(systemCode.getCodename().equals("mail_from")){
	    		emailFrom = systemCode.getCodevalue();
			} 
	    	if(systemCode.getCodename().equals("mail_to")){
	    		emailTo = systemCode.getCodevalue();
			} 
	    	if(systemCode.getCodename().equals("mail_username")){
	    		emailUserName = systemCode.getCodevalue();
			} 
	    	if(systemCode.getCodename().equals("mail_password")){
	    		emailPassword = systemCode.getCodevalue();
			}
	    	if(systemCode.getCodename().equals("smpt_host")){
	    		smptHost = systemCode.getCodevalue();
			}	
	    	if(systemCode.getCodename().equals("smpt_port")){
	    		smptPort = VCSUtil.getIntValue(systemCode.getCodevalue());
			}
	    	
	    	if(systemCode.getCodename().equals("mail_sec_protocol")){
	    		emailSecProtocol = systemCode.getCodevalue();
			}
	    	
	    	if(systemCode.getCodename().equals("enable_job_status_email")){
	    		enableStatusMail =systemCode.getCodevalue().equals("yes")?true:false;
			}
	    	
	    	if(systemCode.getCodename().equals("enable_default_email_client")){
	    		enableDefaultEmailClient =systemCode.getCodevalue().equals("yes")?true:false;
			}
	    	
//	    	if(systemCode.getCodename().equals("enable_links_sharing_in_email")){
//	    		enableLinksSharing =systemCode.getCodevalue().equals("yes")?true:false;
//			}
//	    	
//	    	if(systemCode.getCodename().equals("enable_file_sharing_in_email")){
//	    		enableFileSharing =systemCode.getCodevalue().equals("yes")?true:false;
//			}
	    	
	    	
	    	if(systemCode.getCodename().equals("max_attachment_size")){
	    		maxAttachmentSize = VCSUtil.getIntValue(systemCode.getCodevalue());
			}
	    	
//	    	if(systemCode.getCodename().equals("enable_content_type_search")){
//	    		enableContentTypeSearch =systemCode.getCodevalue().equals("yes")?true:false;
//			}
//	    	
	    	if(systemCode.getCodename().equals("stub_access_mode")){
	    		stubAccessMode =systemCode.getCodevalue();
			}
	    	
	    	//job status page configs
	    	if(systemCode.getCodename().equals("enable_archiving_tab")){
	    		enableArchivingTab =systemCode.getCodevalue().equals("yes")?true:false;
	    	}
	    	if(systemCode.getCodename().equals("enable_stubbing_tab")){
	    		enableStubbingTab =systemCode.getCodevalue().equals("yes")?true:false;
	    	}
	    	if(systemCode.getCodename().equals("enable_evaluation_tab")){
	    		enableEvaluationTab =systemCode.getCodevalue().equals("yes")?true:false;
	    	}
	    	if(systemCode.getCodename().equals("enable_analysis_tab")){
	    		enableAnalysisTab =systemCode.getCodevalue().equals("yes")?true:false;
	    	}
	    	if(systemCode.getCodename().equals("enable_export_tab")){
	    		enableExportTab =systemCode.getCodevalue().equals("yes")?true:false;
	    	}
	    	if(systemCode.getCodename().equals("enable_active_archiving_tab")){
	    		enableActiveArchivingTab =systemCode.getCodevalue().equals("yes")?true:false;
	    	}
	    	if(systemCode.getCodename().equals("enable_dynamic_archiving_tab")){
	    		enableDynamicArchicingTab =systemCode.getCodevalue().equals("yes")?true:false;
	    	}
	    	if(systemCode.getCodename().equals("enable_retention_tab")){
	    		enableRetentiontab =systemCode.getCodevalue().equals("yes")?true:false;
	    	}
	    	if(systemCode.getCodename().equals("enable_scheduling_tab")){
	    		enableSchedulingtab =systemCode.getCodevalue().equals("yes")?true:false;
	    	}
	    	if(systemCode.getCodename().equals("enable_withoutStub_tab")){
	    		enableWithoutStubtab =systemCode.getCodevalue().equals("yes")?true:false;
	    	}
	    	
	    	if(systemCode.getCodename().equals("sh_license")){
	    		licenseStr =systemCode.getCodevalue();
	    	}
	    	
	    	if(systemCode.getCodename().equals("enable_thumbnails_search")){
	    		enableThumbnailsForSearch =systemCode.getCodevalue().equals("yes")?true:false;
	    	}
	    	
	    	if(systemCode.getCodename().equals("enable_thumbnails_stub")){
	    		enableThumbnailsForStubs =systemCode.getCodevalue().equals("yes")?true:false;
	    	}
	    	
	    	if(systemCode.getCodename().equals("enable_thumbnails_summary")){
	    		enableThumbnailsForSummary =systemCode.getCodevalue().equals("yes")?true:false;
	    	}
	    	
	    	if(systemCode.getCodename().equals("enable_thumbnails_list")){
	    		enableThumbnailsForFileList =systemCode.getCodevalue().equals("yes")?true:false;
	    	}
	    	
	    	if(systemCode.getCodename().equals("show_path_in_search_results")){
	    		showPathInSearchResults = systemCode.getCodevalue().equals("yes")?true:false;
	    	}
	    	if(systemCode.getCodename().equals("show_excerpt_in_search_results")){
	    		showExcerptInSearchResults = systemCode.getCodevalue().equals("yes")?true:false;
	    	}
	    	
	    	if(systemCode.getCodename().equals("max_no_of_versions")){
	    		maxNoOfVersions = systemCode.getCodevalue();
	    	}
	    	
   	    	if(systemCode.getCodename().equals("MAX_BATCH_SIZE")){
   	    		maxBatchSize = systemCode.getCodevalue();
   			}
   	    	
   	    	if(systemCode.getCodename().equals("emulate_email_sender")){
   	    		emulateEmailSender = systemCode.getCodevalue().equals("yes")?true:false;
   			}
   	    	
   	    	if(systemCode.getCodename().equals("restrict_cifs_files_sharing_ad")){
	    		restrictCifsFilesSharingAD = systemCode.getCodevalue().equals("yes")?true:false;
	    	}
	    	
	    	if(systemCode.getCodename().equals("restrict_archive_files_sharing_ad")){
	    		restrictArchiveFilesSharingAD = systemCode.getCodevalue().equals("yes")?true:false;
	    	}
	    	
	    	if(systemCode.getCodename().equals("shared_link_validity")){
	    		sharedLinkValidity = systemCode.getCodevalue();
   			}
	    	
	    	if(systemCode.getCodename().equals("guardian_from_address")){
	    		guardianFromAddress = systemCode.getCodevalue();
   			}
	    	
	    	if(systemCode.getCodename().equals("enable_search_by_archive_date")){
	    		enableSearchByArchiveDate = systemCode.getCodevalue().equals("yes")?true:false;
	    	}
	    	
	    	if(systemCode.getCodename().equals("server_warning_limit")){
	    		serverWarningLimit = systemCode.getCodevalue();
   			}
	    	
	    	if(systemCode.getCodename().equals("server_critical_limit")){
	    		serverCriticalLimit = systemCode.getCodevalue();
   			}
	    	
	    	if(systemCode.getCodename().equals("enable_active_archiving_notification")){
	    		enableActiveArchivingNotification = systemCode.getCodevalue().equals("yes")?true:false;
	    	}
	    	
	    	if(systemCode.getCodename().equals("enable_cifs_search")){
	    		enableCifsSearch = systemCode.getCodevalue().equals("yes")?true:false;
	    	}
	    	
	    	if(systemCode.getCodename().equals("enable_repo_search")){
	    		enableRepoSearch = systemCode.getCodevalue().equals("yes")?true:false;
	    	}
   	   }	
			
	}
    
    public void editCompanyConfigurations(){
    	
 	   List<SystemCode> list=systemCodeService.getAllCompanyCodes();
 	   
    	   SystemCode systemCode=null;
    	   getDomainNameSettings();
    	   
    	   for(int i=0;list!=null && i<list.size();i++){
    	    	
    	    	systemCode=list.get(i);
		    	if(systemCode.getCodename().equals("company_name")){
			    	companyConfigName = systemCode.getCodevalue();
				}
		    	
		    	if(systemCode.getCodename().equals("company_prefered_name")){
		    		companyPreferedName = systemCode.getCodevalue();
		    	}
		    	
		    	if(systemCode.getCodename().equals("company_address")){
		    		companyAddress = systemCode.getCodevalue();
		    	}
		    	
		    	if(systemCode.getCodename().equals("company_telephone")){
		    		companyTelephone = systemCode.getCodevalue();
		    	}
		    	
		    	if(systemCode.getCodename().equals("company_support_contact")){
		    		companySupportContact = systemCode.getCodevalue();
		    	}
		    	
		    	if(systemCode.getCodename().equals("company_support_email")){
		    		companySupportEmail = systemCode.getCodevalue();
		    	}
		    	
		    	if(systemCode.getCodename().equals("sh_license")){
		    		licenseStr = systemCode.getCodevalue();
		    	}
    	
    	   }
    }
    
    public void editAccountPolicy(){
    	 List<SystemCode> list=systemCodeService.getAllAccountPolicyCodes();
    	 SystemCode systemCode=null;
    	 for(int i=0;list!=null && i<list.size();i++){ 	    	
 	    	systemCode=list.get(i);
 	    	if(systemCode.getCodename().equals("password_expiry_time"))
 	    		passwordExpiryTime = systemCode.getCodevalue();
 	    	
 	    	if(systemCode.getCodename().equals("no_of_tries_befor_account_lock"))
 	    		noOfTriesBeforAccountLock = systemCode.getCodevalue();
 	    	
 	    	if(systemCode.getCodename().equals("account_lock_timeout_period"))
 	    		accountLockTimeoutPeriod = systemCode.getCodevalue();
 	    	
 	    	if(systemCode.getCodename().equals("password_minimum_length"))
 	    		passwordMinimumLength = systemCode.getCodevalue();
 	    	
 	    	if(systemCode.getCodename().equals("enforce_caps_in_passwords"))
 	    		enforceCapsInPasswords = systemCode.getCodevalue().equals("yes")?true:false;
 	    	
 	    	if(systemCode.getCodename().equals("enforce_numbers_in_passwords"))
 	    		enforceNumbersInPasswords = systemCode.getCodevalue().equals("yes")?true:false;
 	    	
 	    	if(systemCode.getCodename().equals("enforce_special_chars_in_passwords"))
 	    		enforceSpecialCharsInPasswords = systemCode.getCodevalue().equals("yes")?true:false;
    	 }
    }
    
    public void updateCompanyConfigurations(){
    	systemCode = systemCodeService.getSystemCodeByName("company_name");
		if(systemCode!=null){
			systemCode.setCodevalue(companyConfigName);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("company_prefered_name");
		if(systemCode!=null){
			systemCode.setCodevalue(companyPreferedName);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("company_address");
		if(systemCode!=null){
			systemCode.setCodevalue(companyAddress);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("company_telephone");
		if(systemCode!=null){
			systemCode.setCodevalue(companyTelephone);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("company_support_contact");
		if(systemCode!=null){
			systemCode.setCodevalue(companySupportContact);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("company_support_email");
		if(systemCode!=null){
			systemCode.setCodevalue(companySupportEmail);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("sh_license");
		if(systemCode!=null){
			systemCode.setCodevalue(licenseStr);
		}
		refreshSystemCode();
		
		addInfoMessage("Company details saved successfully", "Company details saved successfully");
    }
	
	
	public void editLdap(){
		
		//loading Domain Name Settings
		getDomainNameSettings();
		//loading Ldap Settings
		//bindstr
		SystemCode systemCode = systemCodeService.getSystemCodeByName("bindstr");
		if(systemCode!=null){
			bindStr = systemCode.getCodevalue();
		}
		//dns.servers
		systemCode = systemCodeService.getSystemCodeByName("dns.servers");
		if(systemCode!=null){
			dnsServer = systemCode.getCodevalue();
		}
		//dns.site
		systemCode = systemCodeService.getSystemCodeByName("dns.site");
		if(systemCode!=null){
			dnsSite = systemCode.getCodevalue();
		}		
		//service.acctname
		systemCode = systemCodeService.getSystemCodeByName("service.acctname");
		if(systemCode!=null){
			acctUsername = systemCode.getCodevalue();
		}
		//service.password
		systemCode = systemCodeService.getSystemCodeByName("service.password");
		if(systemCode!=null){
			acctPassword = systemCode.getCodevalue();
		}
		//domain.name
		systemCode = systemCodeService.getSystemCodeByName("domain.name");
		if(systemCode!=null){
			domain = systemCode.getCodevalue();
		}
		
		
		
		//authentication.mode
		systemCode = systemCodeService.getSystemCodeByName("authentication.mode");
		if(systemCode!=null){
			authenticationMode = systemCode.getCodevalue();
		}
		//enable.stub.authorization
		systemCode = systemCodeService.getSystemCodeByName("enable.stub.authorization");
		if(systemCode!=null){
			enableStubAuth = systemCode.getCodevalue().equals("no")?false:true;
		}
		//authentication.enable.sso
		systemCode = systemCodeService.getSystemCodeByName("authentication.enable.sso");
		if(systemCode!=null){
			enableSso = systemCode.getCodevalue().equals("true")?true:false;
		}		
		//agent properties
		//AgentName
		systemCode = systemCodeService.getSystemCodeByName("AgentName");
		if(systemCode!=null){
			agentName = systemCode.getCodevalue();
		}
		//MAX_NO_OF_TRIES
		systemCode = systemCodeService.getSystemCodeByName("MAX_NO_OF_TRIES");
		if(systemCode!=null){
			maxNoOfTries = systemCode.getCodevalue();
		}
		
		//MAX_SEARCH_RESULTS
		systemCode = systemCodeService.getSystemCodeByName("MAX_SEARCH_RESULTS");
		if(systemCode!=null){
			maxSearchResults = systemCode.getCodevalue();
		}
		//StubPath
		systemCode = systemCodeService.getSystemCodeByName("StubPath");
		if(systemCode!=null){
			stubIconFilePath = systemCode.getCodevalue().replaceAll("\\\\\\\\", "\\\\");
		}
		
		//DAS Url for agent
		systemCode = systemCodeService.getSystemCodeByName("DASUrl");
		if(systemCode!=null){
			applicationPath = systemCode.getCodevalue();
		}
		
		
		
		//authentication.enable.driveLetter
		systemCode = systemCodeService.getSystemCodeByName("enable.stub.driveLetter");
		if(systemCode!=null){
			enableDriveLetter = systemCode.getCodevalue().equals("yes")?true:false;
		}
		
		//enable.priv.export
		systemCode = systemCodeService.getSystemCodeByName("enable.priv.export");
		if(systemCode!=null){
			enablePrivExport = systemCode.getCodevalue().equals("yes")?true:false;
		}
		
		//MAX_RESULTS_TO_EXPORT
		systemCode = systemCodeService.getSystemCodeByName("MAX_RESULTS_TO_EXPORT");
		if(systemCode!=null){
			maxResultsToExport = systemCode.getCodevalue();
		}
		//enable.priv.report
		systemCode = systemCodeService.getSystemCodeByName("enable.priv.report");
		if(systemCode!=null){
			enablePrivPrintReport = systemCode.getCodevalue().equals("yes")?true:false;
		}
		//enable.document.tagging
//		systemCode = systemCodeService.getSystemCodeByName("enable.document.tagging");
//		if(systemCode!=null){
//			enableDocumentTagging = systemCode.getCodevalue().equals("yes")?true:false;
//		}
//		//show.tags.searchResults
//		systemCode = systemCodeService.getSystemCodeByName("show.tags.searchResults");
//		if(systemCode!=null){
//			showTagInSearchResults = systemCode.getCodevalue().equals("yes")?true:false;
//		}
		//show.user.auditHistory
		systemCode = systemCodeService.getSystemCodeByName("show.user.auditHistory");
		if(systemCode!=null){
			showUserAuditHistory = systemCode.getCodevalue();
		}
		//logBasePath
		systemCode = systemCodeService.getSystemCodeByName("logBasePath");
		if(systemCode!=null){
			jobLoggingPath = systemCode.getCodevalue();
		}	
				
		System.out.println("----------------LDAP Configuration---------------------");
		System.out.println(bindStr);
		System.out.println(dnsServer);
		System.out.println(dnsSite);
		System.out.println(acctUsername);		
		System.out.println(domain);
		System.out.println(restoreUser);
		
		
	}
	
	
	public String updateGlobalConfiguration(){
		boolean error=false;		
		int num = VCSUtil.getIntValue(showUserAuditHistory);
		if(num<5 || num > 500){
			addErrorMessage("Audit value must be between 5 and 500", "value out of range");
			error = true;
		}
		
		if((restoreUser==null || restoreUser.trim().length()<1) && (restorePassword!=null && restorePassword.trim().length()>0)){
			addErrorMessage("Please provide restore Username", "restore username empty");
			error = true;
		}else if((restoreUser!=null && restoreUser.trim().length()>0) && (restorePassword==null || restorePassword.trim().length()<1)){
			addErrorMessage("Please provide restore password", "restore password empty");
			error = true;
		}
		
		if(dnsName==null || dnsName.trim().length()<1){
			addErrorMessage("Please provide DNS Name", "dns name empty");
			error = true;
		}
			
			
		if(!error){
			saveGeneralSettings();
			saveAgentSettings();
			refreshSystemCode();	// reload vales from DB
//			return "GlobalCodes";
			addInfoMessage("Global Configurations updated successfully", "Global Configurations updated successfully");
			return null;
		}else{
			return null;
		}
				
	}
	
	public String updateLdap(){
		
		    boolean error = false;
		    
		    //if all values empty then save (reset LDAP)
		    if((bindStr==null || bindStr.trim().length()<1) && (domain==null || domain.trim().length()<1) && (acctUsername==null || acctUsername.trim().length()<1) 
		    		&& (acctPassword==null || acctPassword.trim().length()<1) && (dnsServer==null || dnsServer.trim().length()<1) && (dnsSite==null || dnsSite.trim().length()<1)){
		    	saveLdapSettings();
				refreshSystemCode();	// reload vales from DB
				return "LDAPCodes";
		    }
		    if(bindStr==null || bindStr.trim().length()<1){		    	
		    	addErrorMessage("bindstr cannot be empty (Please leave all fields blank to reset settings)", "bindstr cannot be empty");
		    	error = true;
		    }else if(domain==null || domain.trim().length()<1){
		    	addErrorMessage("domain cannot be empty  (Please leave all fields blank to reset settings)", "domain cannot be empty");
		    	error = true;
		    }else if(acctUsername==null || acctUsername.trim().length()<1){
		    	addErrorMessage("username cannot be empty  (Please leave all fields blank to reset settings)", "username cannot be empty");
		    	error = true;
		    }else if(acctPassword==null || acctPassword.trim().length()<1){
		    	addErrorMessage("pasword cannot be empty  (Please leave all fields blank to reset settings)", "password cannot be empty");
		    	error = true;
		    }else if(dnsServer==null || dnsServer.trim().length()<1){
		    	addErrorMessage("dns server cannot be empty  (Please leave all fields blank to reset settings)", "dns server cannot be empty");
		    	error = true;
		    }else if(dnsSite==null || dnsSite.trim().length()<1){
		    	addErrorMessage("dns site cannot be empty  (Please leave all fields blank to reset settings)", "dnd site cannot be empty");
		    	error = true;
		    }
		
			if(domain!=null && domain.trim().length()>0 && !domain.startsWith("@")){
			    domain="@"+domain;
			}
			if(acctUsername!=null && acctUsername.trim().length()>0 && acctUsername.indexOf("@")==-1){
				acctUsername = acctUsername + domain;
			}
			if(!error && acctUsername!=null && acctUsername.trim().length()>0 && !(acctUsername.substring(acctUsername.indexOf("@")).equals(domain))){
				addErrorMessage("Domain in service account and Domain Name do not match", "Domain in service account and Domain Name do not match");
				error = true;
			}
						
			if(error){
				return null;
			}else{
				saveLdapSettings();
				refreshSystemCode();	// reload vales from DB
				addInfoMessage("LDAP settings updated successfully", "LDAP settings updated successfully");
//				return "LDAPCodes";
				return null;
			}
				
	}
	
	public void saveLdapSettings(){
		
		SystemCode systemCode = systemCodeService.getSystemCodeByName("bindstr");
		if(systemCode!=null){
			systemCode.setCodevalue(bindStr);
		}
		//jespa.bindstr
		systemCode = systemCodeService.getSystemCodeByName("jespa.bindstr");
		if(systemCode!=null){
			if(domain!=null && domain.trim().length()>0)
				systemCode.setCodevalue(domain.substring(1).toUpperCase());
			else
				systemCode.setCodevalue("");
			
		}
		//dns.servers
		systemCode = systemCodeService.getSystemCodeByName("dns.servers");
		if(systemCode!=null){
			systemCode.setCodevalue(dnsServer);
		}
		//jespa.dns.servers
		systemCode = systemCodeService.getSystemCodeByName("jespa.dns.servers");
		if(systemCode!=null){
			systemCode.setCodevalue(dnsServer);
		}
		//dns.site
		systemCode = systemCodeService.getSystemCodeByName("dns.site");
		if(systemCode!=null){
			systemCode.setCodevalue(dnsSite);
		}
		//jespa.dns.site
		systemCode = systemCodeService.getSystemCodeByName("jespa.dns.site");
		if(systemCode!=null){
			systemCode.setCodevalue(dnsSite);
		}
		//service.acctname
		systemCode = systemCodeService.getSystemCodeByName("service.acctname");
		if(systemCode!=null){
			systemCode.setCodevalue(acctUsername);
		}
		//jespa.service.acctname
		systemCode = systemCodeService.getSystemCodeByName("jespa.service.acctname");
		if(systemCode!=null){
			systemCode.setCodevalue(acctUsername);
		}
		//service.password
		systemCode = systemCodeService.getSystemCodeByName("service.password");
		if(systemCode!=null){
			systemCode.setCodevalue(acctPassword);
		}
		//jespa.service.password
		systemCode = systemCodeService.getSystemCodeByName("jespa.service.password");
		if(systemCode!=null){
			systemCode.setCodevalue(acctPassword);
		}		
		//domain.name
		systemCode = systemCodeService.getSystemCodeByName("domain.name");
		if(systemCode!=null){
			systemCode.setCodevalue(domain);
		}
		//jespa.domain.dns.name
		systemCode = systemCodeService.getSystemCodeByName("jespa.domain.dns.name");
		if(systemCode!=null){
			if(domain!=null && domain.trim().length()>0)
				systemCode.setCodevalue(domain.substring(1).toUpperCase());
			else
				systemCode.setCodevalue("");
		}	
						
		writeToFile();
		System.out.println("----------------LDAP Configuration---------------------");
		System.out.println(bindStr);
		System.out.println(dnsServer);
		System.out.println(dnsSite);
		System.out.println(acctUsername);		
		System.out.println(domain);
		System.out.println(restoreUser);
	}
	
	public void saveGeneralSettings(){
		updateDomainNameSettings();	

		systemCode = systemCodeService.getSystemCodeByName("restore.domain_name");
		if(systemCode!=null){
			systemCode.setCodevalue(restoreDomainName.replaceAll("@", ""));
		}
		//restore.acctname
		systemCode = systemCodeService.getSystemCodeByName("restore.username");
		if(systemCode!=null){		
			if(restoreUser.indexOf("@")>0){
								
				if(restoreDomainName==null || restoreDomainName.trim().equals("")){
					restoreDomainName=restoreUser.substring(restoreUser.indexOf("@")+1,restoreUser.length());
				}
				restoreUser=restoreUser.substring(0, restoreUser.indexOf("@"));
				
			}
			System.out.println("restore user name: --->"+restoreUser);
			System.out.println("restore domain name: --->"+restoreDomainName);
			systemCode.setCodevalue(restoreUser);
		}
		//restore.password
		systemCode = systemCodeService.getSystemCodeByName("restore.password");
		if(systemCode!=null){
			systemCode.setCodevalue(restorePassword);
		}
				
		//authentication.mode
		SystemCode systemCode = systemCodeService.getSystemCodeByName("authentication.mode");
		if(systemCode!=null){
			systemCode.setCodevalue(authenticationMode);
		}
		
		//MAX_SEARCH_RESULTS
		systemCode = systemCodeService.getSystemCodeByName("MAX_SEARCH_RESULTS");
		if(systemCode!=null){
			systemCode.setCodevalue(maxSearchResults);
		}
		
		//enable.priv.export
		systemCode = systemCodeService.getSystemCodeByName("enable.priv.export");
		if(systemCode!=null){
			String enable = enablePrivExport?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		//MAX_SEARCH_RESULTS
		systemCode = systemCodeService.getSystemCodeByName("MAX_RESULTS_TO_EXPORT");
		if(systemCode!=null){
			systemCode.setCodevalue(maxResultsToExport);
		}
		
		//enable.priv.report
		systemCode = systemCodeService.getSystemCodeByName("enable.priv.report");
		if(systemCode!=null){
			String enable = enablePrivPrintReport?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		//enable.document.tagging
//		systemCode = systemCodeService.getSystemCodeByName("enable.document.tagging");
//		if(systemCode!=null){
//			String enable = enableDocumentTagging?"yes":"no";
//			systemCode.setCodevalue(enable);
//		}
//		//show.tags.searchResults
//		systemCode = systemCodeService.getSystemCodeByName("show.tags.searchResults");
//		if(systemCode!=null){
//			String enable = showTagInSearchResults?"yes":"no";
//			systemCode.setCodevalue(enable);
//		}		
		
		//show.user.auditHistory
		systemCode = systemCodeService.getSystemCodeByName("show.user.auditHistory");
		if(systemCode!=null){
			systemCode.setCodevalue(showUserAuditHistory);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("accessDeniedMessage");
		if(systemCode!=null){
			systemCode.setCodevalue(accessDeniedMessage);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("documentNotFoundMessage");
		if(systemCode!=null){
			systemCode.setCodevalue(documentNotFoundMessage);
		}
		
		
		systemCode = systemCodeService.getSystemCodeByName("enable.stub.authorization");
		if(systemCode!=null){
			String enable = enableStubAuth?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		systemCode = systemCodeService.getSystemCodeByName("authentication.enable.sso");
		if(systemCode!=null){
			String enable = enableSso?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("enable_sharepoint");
		if(systemCode!=null){
			String enable = enableSharePoint?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("enable_print_archiving");
		if(systemCode!=null){
			String enable = enablePrintArchiving?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("LazyArchiving");
		if(systemCode!=null){
			String enable = lazyArchiving?"true":"false";
			systemCode.setCodevalue(enable);
		}

		systemCode = systemCodeService.getSystemCodeByName("LazyExport");
		if(systemCode!=null){
			String enable = lazyExport?"true":"false";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("include_linked_files");
		if(systemCode!=null){
			String enable = includeLinkedFiles?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("calculate_folder_size");
		if(systemCode!=null){
			String enable = calculateFolderSize?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("enable_cifs_file_sharing");
		if(systemCode!=null){
			String enable = enableCifsFileSharing?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("copy_target_file_to_archive");
		if(systemCode!=null){
			String enable = copyTargetFileToArchive?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("enable_file_sharing_security");
		if(systemCode!=null){
			String enable = enableFileSharingSecurity?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("MAX_ANALYSIS_RESULTS");
		if(systemCode!=null){			
			systemCode.setCodevalue(maxAnalysisResults);
		}
		
//		systemCode = systemCodeService.getSystemCodeByName("enable_folder_based_search");
//		if(systemCode!=null){
//			String enable = enableFolderSearch?"yes":"no";
//			systemCode.setCodevalue(enable);
//		}
//		
//		systemCode = systemCodeService.getSystemCodeByName("enable_video_streaming");
//		if(systemCode!=null){
//			String enable = enableVideoStreaming?"yes":"no";
//			systemCode.setCodevalue(enable);
//		}
		
		systemCode = systemCodeService.getSystemCodeByName("stub_access_mode");
		if(systemCode!=null){
			systemCode.setCodevalue(stubAccessMode);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("enable_archiving_tab");
		if(systemCode!=null){
			String enable = enableArchivingTab?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("enable_stubbing_tab");
		if(systemCode!=null){
			String enable = enableStubbingTab?"yes":"no";
			systemCode.setCodevalue(enable);
		}

		systemCode = systemCodeService.getSystemCodeByName("enable_evaluation_tab");
		if(systemCode!=null){
			String enable = enableEvaluationTab?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("enable_analysis_tab");
		if(systemCode!=null){
			String enable = enableAnalysisTab?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("enable_export_tab");
		if(systemCode!=null){
			String enable = enableExportTab?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("enable_active_archiving_tab");
		if(systemCode!=null){
			String enable = enableActiveArchivingTab?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("enable_dynamic_archiving_tab");
		if(systemCode!=null){
			String enable = enableDynamicArchicingTab?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("enable_retention_tab");
		if(systemCode!=null){
			String enable = enableRetentiontab?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("enable_scheduling_tab");
		if(systemCode!=null){
			String enable = enableSchedulingtab?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("enable_withoutStub_tab");
		if(systemCode!=null){
			String enable = enableWithoutStubtab?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("sh_license");
		if(systemCode!=null){
			systemCode.setCodevalue(licenseStr);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("enable_thumbnails_search");
		if(systemCode!=null){
			String enable = enableThumbnailsForSearch?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("enable_thumbnails_stub");
		if(systemCode!=null){
			String enable = enableThumbnailsForStubs?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("enable_thumbnails_summary");
		if(systemCode!=null){
			String enable = enableThumbnailsForSummary?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("enable_thumbnails_list");
		if(systemCode!=null){
			String enable = enableThumbnailsForFileList?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("show_path_in_search_results");
		if(systemCode!=null){
			String enable = showPathInSearchResults?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("show_excerpt_in_search_results");
		if(systemCode!=null){
			String enable = showExcerptInSearchResults?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("max_no_of_versions");
		if(systemCode!=null){			
			systemCode.setCodevalue(maxNoOfVersions);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("MAX_BATCH_SIZE");
		if(systemCode!=null){			
			systemCode.setCodevalue(maxBatchSize);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("emulate_email_sender");
		if(systemCode!=null){			
			String enable = emulateEmailSender?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("restrict_cifs_files_sharing_ad");
		if(systemCode!=null){			
			String enable = restrictCifsFilesSharingAD?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("restrict_archive_files_sharing_ad");
		if(systemCode!=null){			
			String enable = restrictArchiveFilesSharingAD?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("shared_link_validity");
		if(systemCode!=null){			
			systemCode.setCodevalue(sharedLinkValidity);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("guardian_from_address");
		if(systemCode!=null){			
			systemCode.setCodevalue(guardianFromAddress);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("enable_search_by_archive_date");
		if(systemCode!=null){			
			String enable = enableSearchByArchiveDate?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("server_warning_limit");
		if(systemCode!=null){			
			systemCode.setCodevalue(serverWarningLimit);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("server_critical_limit");
		if(systemCode!=null){			
			systemCode.setCodevalue(serverCriticalLimit);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("enable_active_archiving_notification");
		if(systemCode!=null){			
			String enable = enableActiveArchivingNotification?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("enable_cifs_search");
		if(systemCode!=null){			
			String enable = enableCifsSearch?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode = systemCodeService.getSystemCodeByName("enable_repo_search");
		if(systemCode!=null){			
			String enable = enableRepoSearch?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
	}
	
	public void saveAgentSettings(){
		
		//MAX_NO_OF_TRIES
		systemCode = systemCodeService.getSystemCodeByName("MAX_NO_OF_TRIES");
		if(systemCode!=null){
			systemCode.setCodevalue(maxNoOfTries);
		}
		
		//StubPath
		systemCode = systemCodeService.getSystemCodeByName("StubPath");
		if(systemCode!=null){
			systemCode.setCodevalue(stubIconFilePath.replaceAll("\\\\", "\\\\\\\\"));
		}
		
		//DASUrl
		systemCode = systemCodeService.getSystemCodeByName("DASUrl");
		if(systemCode!=null){
			String url="";
			if(stubProtocol==1)
				url="http://";
			else
				url="https://";
			
			if(dnsName!=null && !dnsName.equals("")){
				url+=dnsName;
				
				if(stubProtocol==1){
					if(httpPort>0 && httpPort!=80){
						url+=":"+httpPort+"/ShareArchiver";
					}else{
						url+="/ShareArchiver";
					}
				}else{
					if(httpsPort>0 && httpsPort!=443){
						url+=":"+httpsPort+"/ShareArchiver";
					}else{
						url+="/ShareArchiver";
					}
				}
			}
			
			systemCode.setCodevalue(url);
		}
						
		systemCode=systemCodeService.getSystemCodeByName("enable.stub.driveLetter");
		 
		if(systemCode!=null){
			String enable = enableDriveLetter?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		//------email configuration.
		
		systemCode=systemCodeService.getSystemCodeByName("mail_from");
		if(systemCode!=null){
			systemCode.setCodevalue(emailFrom);
		}
		
		systemCode=systemCodeService.getSystemCodeByName("mail_to");
		if(systemCode!=null){
			systemCode.setCodevalue(emailTo);
		}
		systemCode=systemCodeService.getSystemCodeByName("mail_username");
		if(systemCode!=null){
			systemCode.setCodevalue(emailUserName);
		}
		systemCode=systemCodeService.getSystemCodeByName("mail_password");
		if(systemCode!=null){
			if(emailPassword!=null)
			  systemCode.setCodevalue(emailPassword);
		}
		
		systemCode=systemCodeService.getSystemCodeByName("smpt_host");
		if(systemCode!=null){
			systemCode.setCodevalue(smptHost);
		}
		
		systemCode=systemCodeService.getSystemCodeByName("smpt_port");
		if(systemCode!=null){
			systemCode.setCodevalue(smptPort+"");
		}
		
		systemCode=systemCodeService.getSystemCodeByName("mail_sec_protocol");
		if(systemCode!=null){
		  systemCode.setCodevalue(emailSecProtocol);
		}
		
		systemCode=systemCodeService.getSystemCodeByName("enable_job_status_email");
		if(systemCode!=null){
			String enable = enableStatusMail?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
		systemCode=systemCodeService.getSystemCodeByName("enable_default_email_client");
		if(systemCode!=null){
			String enable = enableDefaultEmailClient?"yes":"no";
			systemCode.setCodevalue(enable);
		}
		
//		systemCode=systemCodeService.getSystemCodeByName("enable_links_sharing_in_email");
//		if(systemCode!=null){
//			String enable = enableLinksSharing?"yes":"no";
//			systemCode.setCodevalue(enable);
//		}
//		
//		systemCode=systemCodeService.getSystemCodeByName("enable_file_sharing_in_email");
//		if(systemCode!=null){
//			String enable = enableFileSharing?"yes":"no";
//			systemCode.setCodevalue(enable);
//		}
//		
		systemCode=systemCodeService.getSystemCodeByName("max_attachment_size");
		if(systemCode!=null){
			systemCode.setCodevalue(maxAttachmentSize+"");
		}
		
//		systemCode=systemCodeService.getSystemCodeByName("enable_content_type_search");
//		if(systemCode!=null){
//			String enable = enableContentTypeSearch?"yes":"no";
//			systemCode.setCodevalue(enable);
//		}
				
	}

	public void saveAccountPolicy(){
		systemCode=systemCodeService.getSystemCodeByName("password_expiry_time");
		if(systemCode!=null){
			systemCode.setCodevalue(passwordExpiryTime);
		}
		
		systemCode=systemCodeService.getSystemCodeByName("no_of_tries_befor_account_lock");
		if(systemCode!=null){
			systemCode.setCodevalue(noOfTriesBeforAccountLock);
		}
		
		systemCode=systemCodeService.getSystemCodeByName("account_lock_timeout_period");
		if(systemCode!=null){
			systemCode.setCodevalue(accountLockTimeoutPeriod);
		}
		
		systemCode=systemCodeService.getSystemCodeByName("password_minimum_length");
		if(systemCode!=null){
			systemCode.setCodevalue(passwordMinimumLength);
		}
		
		systemCode=systemCodeService.getSystemCodeByName("enforce_caps_in_passwords");
		if(systemCode!=null){
			systemCode.setCodevalue(enforceCapsInPasswords?"yes":"no");
		}
		
		systemCode=systemCodeService.getSystemCodeByName("enforce_numbers_in_passwords");
		if(systemCode!=null){
			systemCode.setCodevalue(enforceNumbersInPasswords?"yes":"no");
		}
		
		systemCode=systemCodeService.getSystemCodeByName("enforce_special_chars_in_passwords");
		if(systemCode!=null){
			systemCode.setCodevalue(enforceSpecialCharsInPasswords?"yes":"no");
		}
		refreshSystemCode();
		
		addInfoMessage("Account Policy saved successfully", "Account Policy saved successfully");
	}
	
	public void testLdapConfiguration(){
		System.out.println("testing configs ...........................");
		boolean authentication = false;
		if(domain==null || domain.trim().length()<1){
			addErrorMessage("Domain cannot be empty to test LDAP", "Domain cannot be empty to test LDAP");
		}else if(acctUsername == null || acctUsername.trim().length()<1 || acctPassword==null || acctPassword.trim().length()<1){
			addErrorMessage("Account username and password cannot be empty to test LDAP", "Account username and password cannot be empty to test LDAP");
		}
//		else if(acctUsername.indexOf("@")==-1){
//			addErrorMessage("service account name invalid", "Service account name invalid");
//		}
//		else if(!(acctUsername.substring(acctUsername.indexOf("@")).equals(domain))){
//			addErrorMessage("Domain in service account and Domain Name do not match", "Domain in service account and Domain Name do not match");
//		}
		else{
			if(!domain.startsWith("@")){
				domain = "@"+domain;
			}
			if(acctUsername.indexOf("@")==-1){
				acctUsername = acctUsername + domain;
			}
		
	        System.out.println("dnsserver:"+bindStr+", accountname: "+acctUsername+ "password: "+acctPassword);
	        //using jespa for ldap
	        HashMap<String, String> configProperties = new HashMap();
	        configProperties.put("bindstr", bindStr);
	        configProperties.put("dns.servers", dnsServer);
	        configProperties.put("dns.site", dnsSite);
	        configProperties.put("domain.name", domain);
	        configProperties.put("service.acctname", acctUsername);
	        configProperties.put("service.password", acctPassword);
	        try {
				authentication = populateGroupsSids(configProperties);
				
				
			} catch (SecurityProviderException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				authentication = false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				authentication = false;
			}
	      //  jespa.ldap.LdapSecurityProvider lsp = new jespa.ldap.LdapSecurityProvider(configProperties);
//	        System.out.println( lsp);
	//        String[] attrib = {"displayName","tokenGroups"};
	        /*try{
	            //lsp.authenticate(credentials)
	            jespa.ldap.LdapAccount acct = (jespa.ldap.LdapAccount) lsp.getAccount("administrator"+domain, attrib);
	            System.out.println("Acct: "+acct);
	            authentication = true;
	            lsp.dispose();
	        }catch(Exception ex){
	        	System.out.println("Exception : "+ ex);
	        	ex.printStackTrace();
	        }*/
	        
	        if(authentication){
	        	//removed on requirement by Sir Mazhar (22 Oct 2012)
	        	//addInfoMessage("Credentials verified successfully", "Credentials verified successfully");
	        	authenticationSuccess = true;
	        }else{
	        	addErrorMessage("Credentials verification failed", "Credentials verification failed");
	        }
	       // authenticationSuccess = true;
		}
        
	}
	
	
	public String save(){
		List<SystemCode> systemCodeList=systemCodeService.getSystemCodeByCodename(systemCode.getCodename());
		boolean exists=false;
		SystemCode tempSystemCode;
		for(int i=0;systemCodeList!=null && i<systemCodeList.size();i++){
			tempSystemCode=systemCodeList.get(i);
			if(tempSystemCode.getCodename().equalsIgnoreCase(systemCode.getCodename()) && tempSystemCode.getId()!=systemCode.getId()){
				exists=true;
			}
		}
		
		if(exists){
			System.out.println("Agent already exists");
			addErrorMessage("Agent name already exists", "Agent name already exists");				 
			return null;
		}		

		
		if(exists){
			  System.out.println("Value already exists");
			  addErrorMessage("Value already exists", "Agent login already exists");				 
			  return null;
		}
		systemCode.setViewable(1);
		systemCode.setEditable(1);
		systemCodeService.saveSystemCode(systemCode);
		return "Agents.faces?faces-redirect=true";
	}
	
	
	
	public String update(){
		
		List<Agent> storedAgentsList=agentService.getAgentByName(agent.getName());
		boolean exists=false;
		Agent tempAgent;
		for(int i=0;storedAgentsList!=null && i<storedAgentsList.size();i++){
			tempAgent=storedAgentsList.get(i);
			if(tempAgent.getName().equalsIgnoreCase(agent.getName()) && tempAgent.getId()!=agent.getId()){
				exists=true;
			}
		}
		
		if(exists){
			System.out.println("Agent already exists");
			addErrorMessage("Agent name already exists", "Agent name already exists");				 
			return null;
		}
			
		storedAgentsList=agentService.getAgentByLogin(agent.getLogin());
		for(int i=0;storedAgentsList!=null && i<storedAgentsList.size();i++){
			tempAgent=storedAgentsList.get(i);
			if(tempAgent.getLogin().equalsIgnoreCase(agent.getLogin()) && tempAgent.getId()!=agent.getId() ){
				exists=true;
			}
		}
		
		if(exists){
			System.out.println("Agent login already exists");
			addErrorMessage("Agent login already exists", "Agent login already exists");				 
			return null;
		}
		agent.setActive(1);
		agentService.updateAgent(agent);
		return "Agents.faces?faces-redirect=true";
	}
	
    public void getDomainNameSettings(){
        
        //ipAddressList = new ArrayList<String>()
        //expected format in file
        /*search virtualcode.co.uk
        nameserver 10.1.165.1
        nameserver 192.168.0.1*/
       try{
            int count = 0;
            //File config = new File("E:\\Due date\\resolv.conf")
            File config = new File("/etc/resolv.conf");
            FileReader fr = new FileReader(config);
            BufferedReader br = new BufferedReader(fr);
            String line = "";
            boolean domainFoundYN = false;
            while((line = br.readLine()) != null){
            	System.out.println(line);
                String[] elements = line.split(" ");
                System.out.println(elements[0]);
                if((elements[0].equals("search"))&&(!domainFoundYN)){
                    domainName = line.substring(line.indexOf(" ")+1);
                    domainFoundYN = true;
                }else{
                    if((elements.length == 2)){
                        if(count == 0){
                            ipAddress1 = elements[1];
                        }else if(count == 1){
                            ipAddress2 = elements[1];
                        }
                        count = count + 1;
                    }
                }
            }
            System.out.println("Domain: " + domainName);
            System.out.println("IP1: " + ipAddress1);
            System.out.println("IP2: " + ipAddress2);
            
        }catch(Exception ex){
            System.out.println("Excception while accessing file" + ex);            
        }
       
    }
    
    public void updateDomainNameSettings(){
    	 try{
             int count = 0;
             //File config = new File("E:\\Due date\\resolv.conf")
             File config = new File("/etc/resolv.conf");
             FileWriter fw = new FileWriter(config);             
             //writing the params to file:
             //overwriting the contents of the file
             BufferedWriter writer = new BufferedWriter(fw);
             writer.write("search " + domainName);
             writer.newLine();
             writer.write("nameserver "+ipAddress1);
             writer.newLine();
             writer.write("nameserver "+ipAddress2);
             writer.newLine();
             writer.flush();
             writer.close();
             System.out.println("Domain: " + domainName);
             System.out.println("IP1: " + ipAddress1);
             System.out.println("IP2: " + ipAddress2);
             
         }catch(Exception ex){
             System.out.println("Excception while accessing file" + ex);            
         }
    }
    
    public void writeToFile(){
    	
        try{
        	File ntlm = null;
        	ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
    		String outputPath = "/opt/tomcat/sa_configs/";
    		if(ru.getCodeValue("path_for_configs", SystemCodeType.GENERAL)!=null){
    			outputPath = ru.getCodeValue("path_for_configs", SystemCodeType.GENERAL);
    			if(!outputPath.endsWith("/"))
    				outputPath +="/";
    			outputPath += "/ntlm.prp";
    			ntlm = new File(outputPath.replaceAll("%20", " "));
    			System.out.println("ntlm file path: " + outputPath);
    		}else{
    			String filepath = "/ntlm.prp";
            	URL url = getClass().getResource(filepath);
                ntlm = new File(url.getPath().replaceAll("%20", " "));
                System.out.println("ntlm file path: " + filepath);
    		}
    		
    		
        	
        	
            
            
            List<SystemCode> systemCodeInstanceList = systemCodeService.getSystemCodeByCodetype("jespa");
            System.out.println( "list size for jespa : " + systemCodeInstanceList.size());

            
            //write all DB entries to .prp file
            FileWriter fw = new FileWriter(ntlm);             
            //writing the params to file:
            //overwriting the contents of the file
            BufferedWriter writer = new BufferedWriter(fw);
            writer.write("#-----------Sharearchiver generated file--------------");
            for(SystemCode systemCode: systemCodeInstanceList){
            	writer.newLine();            	
            	writer.write(systemCode.getCodename()+" = "+systemCode.getCodevalue());
            }
            writer.flush();
            writer.close();
            
        }catch(Exception ex){        	
            ex.printStackTrace();
        }
    }
	
	public String delete(){
		systemCodeService.deleteSystemCode(systemCode);
		//agentService.deleteAgent(agent.getId());
		return "SystemCodes.faces?faces-redirect=true";
	}
	
	public String getApplicationVersionNumber(){
		return VCSUtil.getProperty("SOFTWARE_VERSION");
		
	}
	
    /**
     * 
     * @param props
     * @param groupName     any name or * for all
     * @param objectClass    Person/Group
     * @throws SecurityProviderException 
     */
    private boolean populateGroupsSids(HashMap<String,String> props) throws SecurityProviderException, IOException {


        //log.debug("PopulateGroupsSids STARTED>>>>. ");

    	boolean authorize = false;
        HashMap sidMap = new HashMap<String, String>();
        LdapSecurityProvider lsp = new LdapSecurityProvider(props);
        try {

            String filter = "(&(objectCategory=Group)(cn=*))";
//            String filter = "(cn=" + input + ")";
           // log.debug("filter Value: " + filter);



            InitialLdapContext ctx = (InitialLdapContext) lsp.getProperty("ldap.context", null);
            int pageSize =5;
            Control[] ctls = new Control[]{new PagedResultsControl(pageSize, Control.CRITICAL)};
            ctx.setRequestControls(ctls);

            byte[] cookie = null;
            do {

                List results = lsp.search(LdapEntry.ALL_ATTRS, filter);
                Iterator eiter = results.iterator();
                String record = "";
                while (eiter.hasNext()) {

                    LdapEntry val = (LdapEntry) eiter.next();
//                log.debug("LdapEntry: " + val);


                    String name = "";
                    Object sid = "";

                    try {
                        name = (String) val.getProperty("name");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        name = "NO_NAME";
                    }
                    try {
                        sid = val.getProperty("objectSid");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        sid = "NO_SID";
                    }

                    record = "Name: " + name;
                    record += ",   SID: " + sid.toString();

                    try {
                        sidMap.put(sid.toString(), name);
                    } catch (Exception ex) {
                    	ex.printStackTrace();
                        //log.debug("Unable to add recordd to map : " + record);
                    }
                  //  log.debug("record: " + record);
                }

                ctls = ctx.getResponseControls();
                if (ctls != null) {
                    for (int ci = 0; ci < ctls.length; ci++) {
                        if (ctls[ci] instanceof PagedResultsResponseControl) {
                            PagedResultsResponseControl prrc = (PagedResultsResponseControl) ctls[ci];
                            cookie = prrc.getCookie();
                            break;
                        }
                    }

                    ctls = new Control[]{new PagedResultsControl(pageSize, cookie, Control.CRITICAL)};
                    ctx.setRequestControls(ctls);
                }
            } while (cookie != null);
            //authorize = true;

        } catch (NamingException ex) {
            //log.error("Error Occured in JESPAUTIL... EX: " + ex);
        	ex.printStackTrace();
        	authorize = false;
        } catch (Exception ex) {
           // log.error("Error Occured in JESPAUTIL... EX: " + ex);
        	ex.printStackTrace();
        	//exception occurs here
        	authorize = false;
        } finally {
            lsp.dispose();
        }
        
        if(sidMap.size()>1)
        	authorize = true;
        

        
        return authorize;
    }
    
    public void refreshSystemCode(){
    	//force refresh without restart
    	ResourcesUtil.forceValueLoad();    	
    	//to refresh active dir object
		try {
			
			ActiveDirAuthenticator.getInstance(true);
			//update email configurations.
			((com.virtualcode.services.impl.MailServiceImpl)SpringApplicationContext.getBean("mailService")).reInitilize();
	        
	        //log.debug("authenticate: " + authenticate);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void changeListner(){    	
    	//ldap://192.168.30.125/DC=vcsl125,DC=local
    	System.out.println("hi " + bindStr + acctUsername);
    	if(bindStr!=null && bindStr.trim().length()>0 && bindStr.indexOf("ldap://")!=-1){
    		dnsServer = bindStr.substring(7, bindStr.lastIndexOf("/"));
    		String domainStr = bindStr.substring(bindStr.lastIndexOf("/")+1);
    		String[] tokens = domainStr.split(",");
    		String domain = "@";
    		for(int i=0; i < tokens.length;i++){
    			domain  = domain + tokens[i].substring(tokens[i].indexOf("=")+1);
    			if(i<tokens.length-1){
    				domain = domain + ".";
    			}
    		}
    		this.domain= domain;
    		dnsSite = domain!=null?domain.substring(1):domain;
    		if(acctUsername!=null && acctUsername.trim().length()>0){
    			if(acctUsername.indexOf("@")!=-1){
    				acctUsername = acctUsername.substring(0, acctUsername.indexOf("@")+1)+dnsSite;    				
    			}else{
    				acctUsername = acctUsername + dnsSite;
    			}
    			
    		}else{
//    			acctUsername = acctUsername + domain;
    		}
    	}
    	
    }
    
    public void dnsChangeListner(){
    	System.out.println("dns changed ="+ dnsName);
    	System.out.println(stubIconFilePath);
    	stubIconFilePath = "\\\\"+dnsName+"\\Icons";
    	System.out.println(stubIconFilePath);
    	
    	httpPort = 555555;
    	
    	System.out.println(httpPort);
    }
	

	public Integer getSystemCodeId() {
		return systemCodeId;
	}

	public void setSystemCodeId(Integer systemCodeId) {
		this.systemCodeId = systemCodeId;
	}

	public SystemCode getSystemCode() {
		return systemCode;
	}

	public void setSystemCode(SystemCode systemCode) {
		this.systemCode = systemCode;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getIpAddress1() {
		return ipAddress1;
	}

	public void setIpAddress1(String ipAddress1) {
		this.ipAddress1 = ipAddress1;
	}

	public String getIpAddress2() {
		return ipAddress2;
	}

	public void setIpAddress2(String ipAddress2) {
		this.ipAddress2 = ipAddress2;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public String getBindStr() {
		return bindStr;
	}

	public void setBindStr(String bindStr) {
		this.bindStr = bindStr;
	}

	public String getDnsServer() {
		return dnsServer;
	}

	public void setDnsServer(String dnsServer) {
		this.dnsServer = dnsServer;
	}

	public String getDnsSite() {
		return dnsSite;
	}

	public void setDnsSite(String dnsSite) {
		this.dnsSite = dnsSite;
	}

	public String getAcctUsername() {
		return acctUsername;
	}

	public void setAcctUsername(String acctUsername) {
		this.acctUsername = acctUsername;
	}

	public String getAcctPassword() {
		return acctPassword;
	}

	public void setAcctPassword(String acctPassword) {
		this.acctPassword = acctPassword;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}
	
	public String getRestoreDomainName() {
		return restoreDomainName;
	}

	public void setRestoreDomainName(String restoreDomainName) {
		this.restoreDomainName = restoreDomainName;
	}

	public String getRestoreUser() {
		return restoreUser;
	}

	public void setRestoreUser(String restoreUser) {
		this.restoreUser = restoreUser;
	}

	public String getRestorePassword() {
		return restorePassword;
	}

	public void setRestorePassword(String restorePassword) {
		this.restorePassword = restorePassword;
	}

	public String getAuthenticationMode() {
		return authenticationMode;
	}

	public void setAuthenticationMode(String authenticationMode) {
		this.authenticationMode = authenticationMode;
	}

	public boolean isEnableStubAuth() {
		return enableStubAuth;
	}

	public void setEnableStubAuth(boolean enableStubAuth) {
		this.enableStubAuth = enableStubAuth;
	}

	public boolean isEnableSso() {
		return enableSso;
	}

	public void setEnableSso(boolean enableSso) {
		this.enableSso = enableSso;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getMaxNoOfTries() {
		return maxNoOfTries;
	}

	public void setMaxNoOfTries(String maxNoOfTries) {
		this.maxNoOfTries = maxNoOfTries;
	}

	public boolean isAuthenticationSuccess() {
		return authenticationSuccess;
	}

	public void setAuthenticationSuccess(boolean authenticationSuccess) {
		this.authenticationSuccess = authenticationSuccess;
	}

	public String getMaxSearchResults() {
		return maxSearchResults;
	}

	public void setMaxSearchResults(String maxSearchResults) {
		this.maxSearchResults = maxSearchResults;
	}

	public String getStubIconFilePath() {
		return stubIconFilePath;
	}

	public void setStubIconFilePath(String stubIconFilePath) {
		this.stubIconFilePath = stubIconFilePath;
	}

	public String getApplicationPath() {
		return applicationPath;
	}

	public void setApplicationPath(String applicationPath) {
		this.applicationPath = applicationPath;
	}

	public boolean isEnableDriveLetter() {
		return enableDriveLetter;
	}

	public void setEnableDriveLetter(boolean enableDriveLetter) {
		this.enableDriveLetter = enableDriveLetter;
	}
	

	public boolean isEnablePrivExport() {
		return enablePrivExport;
	}

	public void setEnablePrivExport(boolean enablePrivExport) {
		this.enablePrivExport = enablePrivExport;
	}

	public String getMaxResultsToExport() {
		return maxResultsToExport;
	}

	public void setMaxResultsToExport(String maxResultsToExport) {
		this.maxResultsToExport = maxResultsToExport;
	}

	public boolean isEnablePrivPrintReport() {
		return enablePrivPrintReport;
	}

	public void setEnablePrivPrintReport(boolean enablePrivPrintReport) {
		this.enablePrivPrintReport = enablePrivPrintReport;
	}

//	public boolean isEnableDocumentTagging() {
//		return enableDocumentTagging;
//	}
//
//	public void setEnableDocumentTagging(boolean enableDocumentTagging) {
//		this.enableDocumentTagging = enableDocumentTagging;
//	}

	public String getShowUserAuditHistory() {
		return showUserAuditHistory;
	}

	public void setShowUserAuditHistory(String showUserAuditHistory) {
		this.showUserAuditHistory = showUserAuditHistory;
	}

//	public boolean isShowTagInSearchResults() {
//		return showTagInSearchResults;
//	}
//
//	public void setShowTagInSearchResults(boolean showTagInSearchResults) {
//		this.showTagInSearchResults = showTagInSearchResults;
//	}

	public String getJobLoggingPath() {
		return jobLoggingPath;
	}

	public void setJobLoggingPath(String jobLoggingPath) {
		this.jobLoggingPath = jobLoggingPath;
	}

	public String getDnsName() {
		return dnsName;
	}

	public void setDnsName(String dnsName) {
		this.dnsName = dnsName;
	}

	public int getHttpPort() {
		return httpPort;
	}

	public void setHttpPort(int httpPort) {
		this.httpPort = httpPort;
	}

	public int getHttpsPort() {
		return httpsPort;
	}

	public void setHttpsPort(int httpsPort) {
		this.httpsPort = httpsPort;
	}

	public int getStubProtocol() {
		return stubProtocol;
	}

	public void setStubProtocol(int stubProtocol) {
		this.stubProtocol = stubProtocol;
	}

	public String getAccessDeniedMessage() {
		return accessDeniedMessage;
	}

	public void setAccessDeniedMessage(String accessDeniedMessage) {
		this.accessDeniedMessage = accessDeniedMessage;
	}

	public String getDocumentNotFoundMessage() {
		return documentNotFoundMessage;
	}

	public void setDocumentNotFoundMessage(String documentNotFoundMessage) {
		this.documentNotFoundMessage = documentNotFoundMessage;
	}

	public boolean isEnableSharePoint() {
		return enableSharePoint;
	}

	public void setEnableSharePoint(boolean enableSharePoint) {
		this.enableSharePoint = enableSharePoint;
	}

//	public boolean isEnableFolderSearch() {
//		return enableFolderSearch;
//	}
//
//	public void setEnableFolderSearch(boolean enableFolderSearch) {
//		this.enableFolderSearch = enableFolderSearch;
//	}
//
//	public boolean isEnableVideoStreaming() {
//		return enableVideoStreaming;
//	}
//
//	public void setEnableVideoStreaming(boolean enableVideoStreaming) {
//		this.enableVideoStreaming = enableVideoStreaming;
//	}

	public String getEmailFrom() {
		return emailFrom;
	}

	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}

	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	public String getEmailUserName() {
		return emailUserName;
	}

	public void setEmailUserName(String emailUserName) {
		this.emailUserName = emailUserName;
	}

	public String getEmailPassword() {
		return emailPassword;
	}

	public void setEmailPassword(String emailPassword) {
		this.emailPassword = emailPassword;
	}

	public String getSmptHost() {
		return smptHost;
	}

	public void setSmptHost(String smptHost) {
		this.smptHost = smptHost;
	}

	public Integer getSmptPort() {
		return smptPort;
	}

	public void setSmptPort(Integer smptPort) {
		this.smptPort = smptPort;
	}

	public boolean isEnableStatusMail() {
		return enableStatusMail;
	}

	public void setEnableStatusMail(boolean enableStatusMail) {
		this.enableStatusMail = enableStatusMail;
	}
	
	
//	public boolean isEnableLinksSharing() {
//		return enableLinksSharing;
//	}
//
//	public void setEnableLinksSharing(boolean enableLinksSharing) {
//		this.enableLinksSharing = enableLinksSharing;
//	}
//
//	public boolean isEnableFileSharing() {
//		return enableFileSharing;
//	}
//
//	public void setEnableFileSharing(boolean enableFileSharing) {
//		this.enableFileSharing = enableFileSharing;
//	}
//
//	public boolean isEnableContentTypeSearch() {
//		return enableContentTypeSearch;
//	}
//
//	public void setEnableContentTypeSearch(boolean enableContentTypeSearch) {
//		this.enableContentTypeSearch = enableContentTypeSearch;
//	}

	public String getEmailSecProtocol() {
		return emailSecProtocol;
	}

	public void setEmailSecProtocol(String emailSecProtocol) {
		this.emailSecProtocol = emailSecProtocol;
	}

	public String getStubAccessMode() {
		return stubAccessMode;
	}

	public void setStubAccessMode(String stubAccessMode) {
		this.stubAccessMode = stubAccessMode;
	}

	public boolean isEnablePrintArchiving() {
		return enablePrintArchiving;
	}

	public void setEnablePrintArchiving(boolean enablePrintArchiving) {
		this.enablePrintArchiving = enablePrintArchiving;
	}

	public boolean isEnableArchivingTab() {
		return enableArchivingTab;
	}

	public void setEnableArchivingTab(boolean enableArchivingTab) {
		this.enableArchivingTab = enableArchivingTab;
	}

	public boolean isEnableStubbingTab() {
		return enableStubbingTab;
	}

	public void setEnableStubbingTab(boolean enableStubbingTab) {
		this.enableStubbingTab = enableStubbingTab;
	}

	public boolean isEnableEvaluationTab() {
		return enableEvaluationTab;
	}

	public void setEnableEvaluationTab(boolean enableEvaluationTab) {
		this.enableEvaluationTab = enableEvaluationTab;
	}

	public boolean isEnableAnalysisTab() {
		return enableAnalysisTab;
	}

	public void setEnableAnalysisTab(boolean enableAnalysisTab) {
		this.enableAnalysisTab = enableAnalysisTab;
	}

	public boolean isEnableExportTab() {
		return enableExportTab;
	}

	public void setEnableExportTab(boolean enableExportTab) {
		this.enableExportTab = enableExportTab;
	}

	public boolean isEnableActiveArchivingTab() {
		return enableActiveArchivingTab;
	}

	public void setEnableActiveArchivingTab(boolean enableActiveArchivingTab) {
		this.enableActiveArchivingTab = enableActiveArchivingTab;
	}

	public boolean isEnableDynamicArchicingTab() {
		return enableDynamicArchicingTab;
	}

	public void setEnableDynamicArchicingTab(boolean enableDynamicArchicingTab) {
		this.enableDynamicArchicingTab = enableDynamicArchicingTab;
	}

	public boolean isEnableRetentiontab() {
		return enableRetentiontab;
	}

	public void setEnableRetentiontab(boolean enableRetentiontab) {
		this.enableRetentiontab = enableRetentiontab;
	}

	public boolean isEnableSchedulingtab() {
		return enableSchedulingtab;
	}

	public void setEnableSchedulingtab(boolean enableSchedulingtab) {
		this.enableSchedulingtab = enableSchedulingtab;
	}

	public Integer getMaxAttachmentSize() {
		return maxAttachmentSize;
	}

	public void setMaxAttachmentSize(Integer maxAttachmentSize) {
		this.maxAttachmentSize = maxAttachmentSize;
	}

	public String getLicenseStr() {
		return licenseStr;
	}

	public void setLicenseStr(String licenseStr) {
		this.licenseStr = licenseStr;
	}

	public boolean isEnableThumbnailsForSearch() {
		return enableThumbnailsForSearch;
	}

	public void setEnableThumbnailsForSearch(boolean enableThumbnailsForSearch) {
		this.enableThumbnailsForSearch = enableThumbnailsForSearch;
	}

	public boolean isEnableThumbnailsForStubs() {
		return enableThumbnailsForStubs;
	}

	public void setEnableThumbnailsForStubs(boolean enableThumbnailsForStubs) {
		this.enableThumbnailsForStubs = enableThumbnailsForStubs;
	}

	public boolean isEnableThumbnailsForSummary() {
		return enableThumbnailsForSummary;
	}

	public void setEnableThumbnailsForSummary(boolean enableThumbnailsForSummary) {
		this.enableThumbnailsForSummary = enableThumbnailsForSummary;
	}

	public boolean isEnableThumbnailsForFileList() {
		return enableThumbnailsForFileList;
	}

	public void setEnableThumbnailsForFileList(boolean enableThumbnailsForFileList) {
		this.enableThumbnailsForFileList = enableThumbnailsForFileList;
	}

	public boolean isShowPathInSearchResults() {
		return showPathInSearchResults;
	}

	public void setShowPathInSearchResults(boolean showPathInSearchResults) {
		this.showPathInSearchResults = showPathInSearchResults;
	}

	public boolean isShowExcerptInSearchResults() {
		return showExcerptInSearchResults;
	}

	public void setShowExcerptInSearchResults(boolean showExcerptInSearchResults) {
		this.showExcerptInSearchResults = showExcerptInSearchResults;
	}

	public boolean isEnableDefaultEmailClient() {
		return enableDefaultEmailClient;
	}

	public void setEnableDefaultEmailClient(boolean enableDefaultEmailClient) {
		this.enableDefaultEmailClient = enableDefaultEmailClient;
	}

	public String getMaxNoOfVersions() {
		return maxNoOfVersions;
	}

	public void setMaxNoOfVersions(String maxNoOfVersions) {
		this.maxNoOfVersions = maxNoOfVersions;
	}

	public boolean isLazyArchiving() {
		return lazyArchiving;
	}

	public void setLazyArchiving(boolean lazyArchiving) {
		this.lazyArchiving = lazyArchiving;
	}

	public boolean isLazyExport() {
		return lazyExport;
	}

	public void setLazyExport(boolean lazyExport) {
		this.lazyExport = lazyExport;
	}

	public boolean isIncludeLinkedFiles() {
		return includeLinkedFiles;
	}

	public void setIncludeLinkedFiles(boolean includeLinkedFiles) {
		this.includeLinkedFiles = includeLinkedFiles;
	}

	public boolean isCalculateFolderSize() {
		return calculateFolderSize;
	}

	public void setCalculateFolderSize(boolean calculateFolderSize) {
		this.calculateFolderSize = calculateFolderSize;
	}

	public boolean isCopyTargetFileToArchive() {
		return copyTargetFileToArchive;
	}

	public void setCopyTargetFileToArchive(boolean copyTargetFileToArchive) {
		this.copyTargetFileToArchive = copyTargetFileToArchive;
	}

	public boolean isEnableCifsFileSharing() {
		return enableCifsFileSharing;
	}

	public void setEnableCifsFileSharing(boolean enableCifsFileSharing) {
		this.enableCifsFileSharing = enableCifsFileSharing;
	}

	public String getMaxBatchSize() {
		return maxBatchSize;
	}

	public void setMaxBatchSize(String maxBatchSize) {
		this.maxBatchSize = maxBatchSize;
	}

	public boolean isEnableFileSharingSecurity() {
		return enableFileSharingSecurity;
	}

	public void setEnableFileSharingSecurity(boolean enableFileSharingSecurity) {
		this.enableFileSharingSecurity = enableFileSharingSecurity;
	}

	public String getMaxAnalysisResults() {
		return maxAnalysisResults;
	}

	public void setMaxAnalysisResults(String maxAnalysisResults) {
		this.maxAnalysisResults = maxAnalysisResults;
	}

	public boolean isEmulateEmailSender() {
		return emulateEmailSender;
	}

	public void setEmulateEmailSender(boolean emulateEmailSender) {
		this.emulateEmailSender = emulateEmailSender;
	}

	public String getCompanyConfigName() {
		return companyConfigName;
	}

	public void setCompanyConfigName(String companyConfigName) {
		this.companyConfigName = companyConfigName;
	}

	public String getCompanyPreferedName() {
		return companyPreferedName;
	}

	public void setCompanyPreferedName(String companyPreferedName) {
		this.companyPreferedName = companyPreferedName;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getCompanyTelephone() {
		return companyTelephone;
	}

	public void setCompanyTelephone(String companyTelephone) {
		this.companyTelephone = companyTelephone;
	}

	public String getCompanySupportContact() {
		return companySupportContact;
	}

	public void setCompanySupportContact(String companySupportContact) {
		this.companySupportContact = companySupportContact;
	}

	public String getCompanySupportEmail() {
		return companySupportEmail;
	}

	public void setCompanySupportEmail(String companySupportEmail) {
		this.companySupportEmail = companySupportEmail;
	}

	public boolean isRestrictCifsFilesSharingAD() {
		return restrictCifsFilesSharingAD;
	}

	public void setRestrictCifsFilesSharingAD(boolean restrictCifsFilesSharingAD) {
		this.restrictCifsFilesSharingAD = restrictCifsFilesSharingAD;
	}

	public boolean isRestrictArchiveFilesSharingAD() {
		return restrictArchiveFilesSharingAD;
	}

	public void setRestrictArchiveFilesSharingAD(
			boolean restrictArchiveFilesSharingAD) {
		this.restrictArchiveFilesSharingAD = restrictArchiveFilesSharingAD;
	}

	public String getSharedLinkValidity() {
		return sharedLinkValidity;
	}

	public void setSharedLinkValidity(String sharedLinkValidity) {
		this.sharedLinkValidity = sharedLinkValidity;
	}

	public String getGuardianFromAddress() {
		return guardianFromAddress;
	}

	public void setGuardianFromAddress(String guardianFromAddress) {
		this.guardianFromAddress = guardianFromAddress;
	}

	public boolean isEnableSearchByArchiveDate() {
		return enableSearchByArchiveDate;
	}

	public void setEnableSearchByArchiveDate(boolean enableSearchByArchiveDate) {
		this.enableSearchByArchiveDate = enableSearchByArchiveDate;
	}

	public String getServerWarningLimit() {
		return serverWarningLimit;
	}

	public void setServerWarningLimit(String serverWarningLimit) {
		this.serverWarningLimit = serverWarningLimit;
	}

	public String getServerCriticalLimit() {
		return serverCriticalLimit;
	}

	public void setServerCriticalLimit(String serverCriticalLimit) {
		this.serverCriticalLimit = serverCriticalLimit;
	}

	public boolean isEnableWithoutStubtab() {
		return enableWithoutStubtab;
	}

	public void setEnableWithoutStubtab(boolean enableWithoutStubtab) {
		this.enableWithoutStubtab = enableWithoutStubtab;
	}

	public boolean isEnableActiveArchivingNotification() {
		return enableActiveArchivingNotification;
	}

	public void setEnableActiveArchivingNotification(
			boolean enableActiveArchivingNotification) {
		this.enableActiveArchivingNotification = enableActiveArchivingNotification;
	}

	public boolean isEnableCifsSearch() {
		return enableCifsSearch;
	}

	public void setEnableCifsSearch(boolean enableCifsSearch) {
		this.enableCifsSearch = enableCifsSearch;
	}

	public boolean isEnableRepoSearch() {
		return enableRepoSearch;
	}

	public void setEnableRepoSearch(boolean enableRepoSearch) {
		this.enableRepoSearch = enableRepoSearch;
	}

	public String getPasswordExpiryTime() {
		return passwordExpiryTime;
	}

	public void setPasswordExpiryTime(String passwordExpiryTime) {
		this.passwordExpiryTime = passwordExpiryTime;
	}

	public String getNoOfTriesBeforAccountLock() {
		return noOfTriesBeforAccountLock;
	}

	public void setNoOfTriesBeforAccountLock(String noOfTriesBeforAccountLock) {
		this.noOfTriesBeforAccountLock = noOfTriesBeforAccountLock;
	}

	public String getAccountLockTimeoutPeriod() {
		return accountLockTimeoutPeriod;
	}

	public void setAccountLockTimeoutPeriod(String accountLockTimeoutPeriod) {
		this.accountLockTimeoutPeriod = accountLockTimeoutPeriod;
	}

	public String getPasswordMinimumLength() {
		return passwordMinimumLength;
	}

	public void setPasswordMinimumLength(String passwordMinimumLength) {
		this.passwordMinimumLength = passwordMinimumLength;
	}

	public boolean isEnforceCapsInPasswords() {
		return enforceCapsInPasswords;
	}

	public void setEnforceCapsInPasswords(boolean enforceCapsInPasswords) {
		this.enforceCapsInPasswords = enforceCapsInPasswords;
	}

	public boolean isEnforceNumbersInPasswords() {
		return enforceNumbersInPasswords;
	}

	public void setEnforceNumbersInPasswords(boolean enforceNumbersInPasswords) {
		this.enforceNumbersInPasswords = enforceNumbersInPasswords;
	}

	public boolean isEnforceSpecialCharsInPasswords() {
		return enforceSpecialCharsInPasswords;
	}

	public void setEnforceSpecialCharsInPasswords(
			boolean enforceSpecialCharsInPasswords) {
		this.enforceSpecialCharsInPasswords = enforceSpecialCharsInPasswords;
	}


}
