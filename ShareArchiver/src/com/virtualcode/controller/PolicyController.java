package com.virtualcode.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.swing.tree.TreeNode;

import org.richfaces.component.UITree;
import org.richfaces.event.TreeSelectionChangeEvent;

import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.DocumentCategory;
import com.virtualcode.vo.DocumentType;
import com.virtualcode.vo.Policy;

@ManagedBean(name="policyController")
@ViewScoped
public class PolicyController extends AbstractController {

	 public PolicyController(){
		super();
		policy=new Policy();
		policy.setActive(1);
	 }
	
     public void populatePolicyInfo(){
		
		if(policyId!=null && policyId.intValue()>0  && ! FacesContext.getCurrentInstance().isPostback()){
			policy=policyService.getPolicyById(policyId);
			if(policy!=null){
				
				String[] docAgeArr=policy.getDocumentAge().split(",");				
				policy.setDocAgeYears(VCSUtil.getIntValue(docAgeArr[0]));
				policy.setDocAgeMonths(VCSUtil.getIntValue(docAgeArr[1]));
				
				String[] lastAccessedArr=policy.getLastAccessedDate().split(",");				
				policy.setLastAccessedYears(VCSUtil.getIntValue(lastAccessedArr[0]));
				policy.setLastAccessedMonths(VCSUtil.getIntValue(lastAccessedArr[1]));
				
				String[] lastModArr=policy.getLastModifiedDate().split(",");				
				policy.setLastModifiedYears(VCSUtil.getIntValue(lastModArr[0]));
				policy.setLastModifiedMonths(VCSUtil.getIntValue(lastModArr[1]));	
				
				String[] sizeLargerThanArr=policy.getSizeLargerThan().split(",");
				policy.setSizeGreaterThan(VCSUtil.getIntValue(sizeLargerThanArr[0]));
				policy.setSizeUnits(sizeLargerThanArr[1]);
				
				Iterator<DocumentType> ite=policy.getPolicyDocumentTypes().iterator();
				 DocumentType selectedDocumentType;
				 DocumentCategory documentCategory;
				 List<DocumentType> docTypeList;
				 System.out.println("Setting selected document types");
				 while(ite!=null && ite.hasNext()){
					selectedDocumentType=(DocumentType)ite.next();
					for(int i=0;rootNodes!=null && i<rootNodes.size();i++){
						documentCategory=(DocumentCategory)rootNodes.get(i);
						docTypeList=documentCategory.getDocumentTypesList();
						for(int j=0;docTypeList!=null && j<docTypeList.size();j++){
						   if(docTypeList.get(j).getId().intValue()==selectedDocumentType.getId().intValue()){
							   docTypeList.get(j).setSelected(true);
							   
						   }
						}
						System.out.println("init------"+documentCategory);
					}
					
				}
				documentTypeSelected();
			}			
		}
	 }
     
     private List<TreeNode> rootNodes=new ArrayList<TreeNode>();
     
     private TreeNode currentSelection = null;
     
     public void selectionChanged(TreeSelectionChangeEvent selectionChangeEvent) {
         // considering only single selection
         List<Object> selection = new ArrayList<Object>(selectionChangeEvent.getNewSelection());
         Object currentSelectionKey = selection.get(0);
         UITree tree = (UITree) selectionChangeEvent.getSource();
  
         Object storedKey = tree.getRowKey();
         tree.setRowKey(currentSelectionKey);
         currentSelection = (TreeNode) tree.getRowData();
         
         if(! currentSelection.isLeaf()){
        	 DocumentCategory documentCategory=(DocumentCategory)currentSelection;
        	 List<DocumentType> documentTypesList=documentCategory.getDocumentTypesList();
        	 for(int i=0;documentTypesList!=null && i<documentTypesList.size();i++){
        		 documentTypesList.get(i).setSelected(true);
        	 }
        	 System.out.println("Document category selected..."+documentCategory.getName());
         }

         tree.setRowKey(storedKey);
     }
     
     public void categorySelected(){    	     	
    	 System.out.println("---Document category selected---------");
    	 DocumentCategory documentCategory;
    	 List<DocumentType> documentTypes;
    	 for(int i=0;i<rootNodes.size();i++){
    		 documentCategory=(DocumentCategory)rootNodes.get(i);
    		 documentTypes=documentCategory.getDocumentTypesList();
    		 for(int j=0;j<documentTypes.size();j++){
    			 documentTypes.get(j).setSelected(documentCategory.getSelected());
    			 
    		 }
    	 }
    	 
     }
     
     
     public void documentTypeSelected(){
    	    	 
    	 DocumentCategory documentCategory;
    	 List<DocumentType> documentTypes;
    	 DocumentType documentType;
    	 int counter=0;
    	 try{
	    	 for(int i=0;rootNodes!=null && i<rootNodes.size();i++){
	    		 documentCategory=(DocumentCategory)rootNodes.get(i);
	    		 
	    		 documentTypes=documentCategory.getDocumentTypesList();
	    		 
	    		 for(int j=0;documentTypes!=null && j<documentTypes.size();j++){
	    			 documentType=documentTypes.get(j);
	    			 if(documentType!=null && documentType.getSelected()==true){
	    				 System.out.println("docTypeSelected:->"+documentType.getName());
	    				counter++;
	    			 }  			 
	    		 }
	    		 if(documentTypes==null || documentTypes.size()==0 || counter ==documentTypes.size()){
					 documentCategory.setSelected(true);
				 }else{
					 documentCategory.setSelected(false);
				 }
	    		 
	    		 counter=0;
	    	 }
    	 }catch(Exception ex){
    		 ex.printStackTrace();
    	 }
    	 
     }    
     
     public void init(){
    	 System.out.println("init() method called...");
    	 if(FacesContext.getCurrentInstance().isPostback()){
    		 return;
    	 }
    	 rootNodes.clear();
    	 List<DocumentCategory> docCategoriesList=documentClassifyService.getAllDocumentCategories();
    	 List<DocumentType> documentTypesList;
    	 Iterator<DocumentType> documentTypeIterator;
    	 DocumentCategory documentCategory;
    	 DocumentType documentType;    	 
    	 
    	 for(int i=0;docCategoriesList!=null && i<docCategoriesList.size();i++){
    		 documentCategory= docCategoriesList.get(i);
    		 documentTypeIterator=documentCategory.getDocumentTypes().iterator();
    		 documentTypesList=new ArrayList<DocumentType>();
    		 while(documentTypeIterator!=null && documentTypeIterator.hasNext()){
    			 documentType=documentTypeIterator.next();
    			 documentType.setDocumentCategory(documentCategory);
    			 documentTypesList.add(documentType);
    		 }
    		 documentCategory.getDocumentTypesList().clear();
    		 documentCategory.setDocumentTypesList(documentTypesList);
    		 
    		 rootNodes.add(documentCategory);
    	 }
    	 
     }
     
     public List<TreeNode> getRootNodes() {    	 
    	 
    	 return rootNodes;
     }
  
     public void setRootNodes(List<TreeNode> rootNodes) {
         this.rootNodes = rootNodes;
     }
  
     public TreeNode getCurrentSelection() {
         return currentSelection;
     }
  
     public void setCurrentSelection(TreeNode currentSelection) {
         this.currentSelection = currentSelection;
     }
     
         
     public List<DocumentType> getAllDocumentTypes(){
     	return documentClassifyService.getAllDocumentTypes();  	
     
      }
	
	 public List<Policy> getAllPolicies(){
		 return policyService.getAllPolicies();
	 }
	
	 public String save(){
		 if(policy.getId()==null){
			 if(policyService.getPolicyByName(policy.getName())!=null){
				  System.out.println("Policy already exists");
				  FacesContext context = FacesContext.getCurrentInstance();
				  FacesMessage message = new FacesMessage("Policy name already exists");
				  context.addMessage(null, message);			 
				  return null;
			 }
		 }
		 
		 DocumentCategory documentCategory;
		 policy.getPolicyDocumentTypes().clear();
		 List<DocumentType> selectedDocumentTypes=null;
		 for(int i=0;rootNodes!=null && i<rootNodes.size();i++){			 
			 documentCategory=(DocumentCategory)rootNodes.get(i);
			 selectedDocumentTypes=documentCategory.getSelectedDocumentTypes();
			 if(selectedDocumentTypes!=null && selectedDocumentTypes.size()>0)
			 policy.getPolicyDocumentTypes().addAll(selectedDocumentTypes);			
		 }
		 
		 Iterator<DocumentType> ite=policy.getPolicyDocumentTypes().iterator();
		 while(ite.hasNext()){
			 System.out.println("selected document name :->"+ite.next().getId()+",id->");
		 }
		 
		 policy.setDocumentAge(policy.getDocAgeYears()+","+policy.getDocAgeMonths());
		 policy.setLastModifiedDate(policy.getLastModifiedYears()+","+policy.getLastModifiedMonths());
		 policy.setLastAccessedDate(policy.getLastAccessedYears()+","+policy.getLastAccessedMonths());
		 if(policy.getSizeGreaterThan()!=null && policy.getSizeGreaterThan().intValue()>0)
		     policy.setSizeLargerThan( policy.getSizeGreaterThan()+","+policy.getSizeUnits());
		 else
			 policy.setSizeLargerThan( 0+","+policy.getSizeUnits()); 
		 policyService.savePolicy(policy);
		 
		 return "Policies.faces?faces-redirect=true";
	 }
	
	 public String update(){
		 policyService.updatePolicy(policy);
		 return "Policies.faces?faces-redirect=true";
	 }
	
	 public String delete(){
		 List jobList = jobService.getJobByProperty("policy.id", policy.getId());
		 System.out.println("Job list size: " + jobList.size());
		 if(jobList != null && jobList.size()>0){
			 addErrorMessage("Policy with associated jobs cannot be deleted", "Policy is associated with jobs");
			 return null;
		 }
		 policyService.deletePolicy(policy);
		 return "Policies.faces?faces-redirect=true";
	 }
	 
	 
	
		
    private Integer policyId;
	
	private Policy policy;
	
	private List<Policy> policies;
		
	private List<DocumentCategory> docCategoriesList;
	
	

	public Integer getPolicyId() {
		return policyId;
	}

	public void setPolicyId(Integer policyId) {
		this.policyId = policyId;
	}

	public Policy getPolicy() {
		return policy;
	}

	public void setPolicy(Policy policy) {
		this.policy = policy;
	}

	public List<Policy> getPolicies() {
		return policies;
	}

	public void setPolicies(List<Policy> policies) {
		this.policies = policies;
	}
	
	
	private Integer[] yearsArr = new Integer[] {0,1,2,3,4,5,6,7,8,9,10};
	
	private Integer[] monthsArr = new Integer[] {0,1,2,3,4,5,6,7,8,9,10,11};

	public Integer[] getYearsArr() {
		return yearsArr;
	}

	public void setYearsArr(Integer[] yearsArr) {
		this.yearsArr = yearsArr;
	}

	public Integer[] getMonthsArr() {
		return monthsArr;
	}

	public void setMonthsArr(Integer[] monthsArr) {
		this.monthsArr = monthsArr;
	}

	public List<DocumentCategory> getDocCategoriesList() {
		return docCategoriesList;
	}

	public void setDocCategoriesList(List<DocumentCategory> docCategoriesList) {
		this.docCategoriesList = docCategoriesList;
	}

	
}
