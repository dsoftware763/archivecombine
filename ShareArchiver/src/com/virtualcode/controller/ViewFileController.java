package com.virtualcode.controller;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.jcr.RepositoryException;

import org.apache.commons.io.FileUtils;
import org.icepdf.core.exceptions.PDFException;
import org.icepdf.core.exceptions.PDFSecurityException;
import org.icepdf.core.pobjects.Document;
import org.icepdf.core.pobjects.Page;
import org.icepdf.core.util.GraphicsRenderingHints;

import net.coobird.thumbnailator.Thumbnails;

import com.virtualcode.cifs.SmbWriter;
import com.virtualcode.common.SecureURL;
import com.virtualcode.common.VCSConstants;
import com.virtualcode.pdfviewer.viewer.view.DocumentManager;
import com.virtualcode.repository.UserActivityService;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.RoleFeatureUtil;
import com.virtualcode.util.StringUtils;
import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.CustomTreeNode;
import com.virtualcode.vo.RoleFeature;
import com.virtualcode.vo.SystemCode;
import com.virtualcode.vo.SystemCodeType;

@ManagedBean(name = "viewFileController")
@ViewScoped
public class ViewFileController extends AbstractController {

	public ViewFileController() {
//		activeDeActivateUserControls();
    	com.virtualcode.pdfviewer.flexpaper.Config config = new com.virtualcode.pdfviewer.flexpaper.Config();
    	if(!"true".equals(config.getConfig("splitmode")))
    			pdfViewerUrl = "common/simple_document.jsp?doc";
    	else
    			pdfViewerUrl = "common/split_document.jsp?doc";
	}

	private String nodeID;

	private boolean authorize;

	private CustomTreeNode selectedNode;
		
//	private boolean buttonRendered = true;

	private boolean enabled = false;

	
	
    private String mediaFilePath;
    
    private String mediaFilePathLink;    
    
//	private boolean filesShareEnabled=true;
	
//	private boolean linksShareEnabled=true;
	
//	private boolean downloadEnabled=false;
	
//	private boolean fileRestoreEanbled=false;
	
	private boolean mediaStreamingEnabled=false;
	
//	private boolean enableDocumentTagging=false;
	
//	private boolean enablePdfView = false;
//	
//	private boolean showPdfIcon = false;
	
//	private boolean emailUsingDefaultClient = false; //non html client like Outlook etc.
	
	private String urlForPdf = "";
	
	private boolean showFrame = false;
	
	@ManagedProperty(value="#{documentManager}")
	private DocumentManager documentManager;
	
	@ManagedProperty(value="#{explorer}")
	private FileExplorerController explorer;
	
	private String emailString;

	private String pdfViewerUrl;
	
	public void populateFileInfo() {
		showFrame = false;
		
		if (nodeID == null || FacesContext.getCurrentInstance().isPostback())
			return;
		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		nodeID = StringUtils.decodeUrl(nodeID);
		

		System.out
				.println("username for file access : "
						+ getHttpSession().getAttribute(
								VCSConstants.SESSION_USER_NAME));
		String username = (String) getHttpSession().getAttribute(
				VCSConstants.SESSION_USER_NAME);
			selectedNode = fileExplorerService.getNodeByPath(nodeID);

		if(selectedNode.isMediaFile()){
			getHttpRequest().setAttribute("filePath", selectedNode.getPath());
			explorer.playMediaFile();
		}else{
			show();
			try {
				getHttpResponse().sendRedirect("common/simple_document.jsp?doc="+urlForPdf);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
//		if (selectedNode != null) {
//			String authType = (String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE);
//			if(authType!=null && authType.equals("db")){
//				authorize = true; // if the user is db
//			}else{
//				authorize = fileExplorerService.authorizeStub(nodeID, username);
//			}
//			 authorize=true;
//			if (!authorize) {
//				String accessDeniedMessage = ru.getCodeValue(
//						"accessDeniedMessage", SystemCodeType.GENERAL);
//
//				addCompErrorMessage("searchForm:hiddenPercent",accessDeniedMessage, accessDeniedMessage);
//				return;
//			}
			
			/*
			System.out.println("Decoded UUID:" + selectedNode.getPath());

			String showDriveLetter = ru.getCodeValue("enable.stub.driveLetter",
					SystemCodeType.GENERAL);

			String path = "";
			String[] elements = StringUtils.getPathElements(selectedNode
					.getPath());
			if (showDriveLetter != null && showDriveLetter.trim().equals("yes")) {

				if (!(selectedNode.getPath().contains("/SP/"))) {
					Map<String, String> mappingList = fileExplorerService
							.getDriveLettersMap();
					String matchStr = elements[1] + "/" + elements[2] + "/"
							+ elements[3] + "/" + elements[4]; // e.g.
																// TestCompany/FS/192.168.30.24/LargeFile
					if (mappingList.containsKey(matchStr.toLowerCase())) {
						String driveLtr = (String) mappingList.get(matchStr
								.toLowerCase());
						path = driveLtr;
					} else {
						path = "\\\\" + elements[3] + "\\" + elements[4];// if
																			// drive
																			// letter
																			// is
																			// enabled
																			// for
																			// stub
																			// access
																			// but
																			// not
																			// found
					}
				}
			} else {
				path = "\\\\" + elements[3] + "\\" + elements[4]; // if drive
																	// letter is
																	// disabled
																	// for stub
																	// access
			}

			path += "\\";

			for (int i = 5; i < elements.length; i++) {
				path += elements[i];
				if (i != elements.length - 1) {
					path += "\\";
				}
			}
			System.out.println(path);

			selectedNode.setCompressedURL(path);
		} else {
			String fileNotFoundMessage = ru.getCodeValue(
					"documentNotFoundMessage", SystemCodeType.GENERAL);			
			addCompErrorMessage("searchForm:hiddenPercent",fileNotFoundMessage, fileNotFoundMessage);
			return;
		}*/
		// authorize = true;

//		if (!authorize) {
//			String accessDeniedMessage = ru.getCodeValue("accessDeniedMessage",
//					SystemCodeType.GENERAL);
//
//			addCompErrorMessage("searchForm:hiddenPercent",accessDeniedMessage, accessDeniedMessage);
//			return;
//		}
		if (authorize && selectedNode != null) {
			UserActivityService userActivityService = serviceManager
					.getUserActivityService();
			userActivityService.recordDocAccessedActivity(username,
					getHttpRequest().getRemoteAddr(), getHttpSession().getId(),
					getHttpSession().getCreationTime(), selectedNode.getPath());
		}
		
	}

    
    public void show(){   		

    	documentManager.setDocumentPath(selectedNode.getDownloadUrl());
    	documentManager.openDocument();
    	String filename = selectedNode.getName();
    	filename = filename.substring(0, filename.lastIndexOf("."))+".pdf";
        MessageDigest md;
        StringBuffer sb = null;
		try {
			md = MessageDigest.getInstance("MD5");
			System.out.println(filename.substring(0, filename.indexOf(".")));
			md.update(filename.substring(0, filename.indexOf(".")).getBytes());
			byte[] digest = md.digest();
			sb = new StringBuffer();
			for (byte b : digest) {
				sb.append(Integer.toHexString((int) (b & 0xff)));
			}
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

        filename = sb.toString() + filename.substring(filename.indexOf("."));//System.currentTimeMillis() + filename.substring(filename.indexOf(".")); //to handle foreign characters

    	urlForPdf = filename.replaceAll(" ", "_").toLowerCase();
	
 	   showFrame = true;
    }



	
	 private boolean isLdapUser(){
		   String authMode=(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE);
		  if(authMode.equalsIgnoreCase("ad"))
			  return true;
		  else
		  return false;
	 } 
	
	

	public String getLoggedInRole(){
		   String roleName=(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_ROLE_NAME);
		   return roleName.toUpperCase();
    }
	private String getUserRole(){
		 String loggedInRole=getLoggedInRole();
		 if(loggedInRole.equalsIgnoreCase("ADMINISTRATOR")){
			 return loggedInRole;
		 }else{
			 String authMode=(String)getHttpSession().getAttribute(VCSConstants.SESSION_USER_AUTH_TYPE);
			  if(authMode.equalsIgnoreCase("ad")){
				  return "LDAP";
			  }else{
				  return "PRIV";
			  }				  
		 }
		
	 }
/*	
	private void activeDeActivateUserControls(){
	    RoleFeatureUtil rfUtil=RoleFeatureUtil.getRoleFeatureUtil();   
	    RoleFeature roleFeature;
    	String userRole=getUserRole();
      
    	if(userRole.equalsIgnoreCase("PRIV")){
    		 
    		 roleFeature=rfUtil.getFeature("email_link_sp");
	   		 this.linksShareEnabled=roleFeature!=null ?roleFeature.getPrivAllowed():false;
	   		 
	   		 roleFeature=rfUtil.getFeature("email_doc_sp");
	   		 this.filesShareEnabled=roleFeature!=null ?roleFeature.getPrivAllowed():false;
	   		 
	   		 roleFeature=rfUtil.getFeature("download_sp");
	   		 this.downloadEnabled=roleFeature!=null ?roleFeature.getPrivAllowed():false;
	   		 
	   		 roleFeature=rfUtil.getFeature("restore_file_sp");
    		 this.fileRestoreEanbled=roleFeature!=null ?roleFeature.getPrivAllowed():false;
    		 
	   		 roleFeature=rfUtil.getFeature("media_streaming_sp");
	   		 this.mediaStreamingEnabled=roleFeature!=null ?roleFeature.getPrivAllowed():false;
	   		 
	   		 roleFeature=rfUtil.getFeature("document_tagging");
	   		 this.enableDocumentTagging=roleFeature!=null ?roleFeature.getPrivAllowed():false;
	   		 
	   		 roleFeature=rfUtil.getFeature("document_viewing_sp");
	   		 this.enablePdfView=roleFeature!=null ?roleFeature.getPrivAllowed():false;
	   		 
    	}else if(userRole.equalsIgnoreCase("LDAP")){
    		
    		 roleFeature=rfUtil.getFeature("email_link_sp");
	   		 this.linksShareEnabled=roleFeature!=null ?roleFeature.getLdapAllowed():false;
	   		 
	   		 roleFeature=rfUtil.getFeature("email_doc_sp");
	   		 this.filesShareEnabled=roleFeature!=null ?roleFeature.getLdapAllowed():false;
	   		 
	   		 roleFeature=rfUtil.getFeature("download_sp");
	   		 this.downloadEnabled=roleFeature!=null ?roleFeature.getLdapAllowed():false;
	   		 
	   		 roleFeature=rfUtil.getFeature("restore_file_sp");
    		 this.fileRestoreEanbled=roleFeature!=null ?roleFeature.getLdapAllowed():false;
    		 
    		
	   		 roleFeature=rfUtil.getFeature("media_streaming_sp");
	   		 this.mediaStreamingEnabled=roleFeature!=null ?roleFeature.getLdapAllowed():false;
	   		 
	   		 roleFeature=rfUtil.getFeature("document_tagging");
	   		 this.enableDocumentTagging=roleFeature!=null ?roleFeature.getLdapAllowed():false;
	   		 
		   	roleFeature=rfUtil.getFeature("document_viewing_sp");
		   	this.enablePdfView=roleFeature!=null ?roleFeature.getLdapAllowed():false;
	   		 
    	}else{
    		 this.linksShareEnabled=false;
    		 this.filesShareEnabled=false;
    		 this.downloadEnabled=false;
    		 this.fileRestoreEanbled=false;    		
    		 this.mediaStreamingEnabled=false;   
    		 this.enableDocumentTagging=false;
    		 this.enablePdfView = false;
    	}
    	
    	ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
    	if(ru.getCodeValue("enable_default_email_client", SystemCodeType.GENERAL)!=null)
    		emailUsingDefaultClient = ru.getCodeValue("enable_default_email_client", SystemCodeType.GENERAL).equals("yes")?true:false;
	 
    }
	

	*/
	

	
	private boolean videoStreamingEnabled=false;
	
	private boolean imageFile = false;
	
	private String currentThumbnailPath;
	
	private String linkOption = "1"; 	// 1 =  download link, 2 = view document link
    

	public boolean isVideoStreamingEnabled() {
		return videoStreamingEnabled;
	}

	public void setVideoStreamingEnabled(boolean videoStreamingEnabled) {
		this.videoStreamingEnabled = videoStreamingEnabled;
	}

	public String getNodeID() {
		return nodeID;
	}

	public void setNodeID(String nodeID) {
		this.nodeID = nodeID;
	}

	public CustomTreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(CustomTreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public boolean isAuthorize() {
		return authorize;
	}

	public void setAuthorize(boolean authorize) {
		this.authorize = authorize;
	}


	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}



	
	public String getMediaFilePath() {
		return mediaFilePath;
	}
    
	public void setMediaFilePath(String mediaFilePath) {
		this.mediaFilePath = mediaFilePath;
	}



	public String getMediaFilePathLink() {
		return mediaFilePathLink;
	}

	public void setMediaFilePathLink(String mediaFilePathLink) {
		this.mediaFilePathLink = mediaFilePathLink;
	}


	public boolean isMediaStreamingEnabled() {
		return mediaStreamingEnabled;
	}
	
	

	public boolean isImageFile() {
		return imageFile;
	}

	public void setImageFile(boolean imageFile) {
		this.imageFile = imageFile;
	}

	public String getCurrentThumbnailPath() {
		return currentThumbnailPath;
	}

	public void setCurrentThumbnailPath(String currentThumbnailPath) {
		this.currentThumbnailPath = currentThumbnailPath;
	}


	public boolean isShowFrame() {
		return showFrame;
	}

	public void setShowFrame(boolean showFrame) {
		this.showFrame = showFrame;
	}


	public DocumentManager getDocumentManager() {
		return documentManager;
	}

	public void setDocumentManager(DocumentManager documentManager) {
		this.documentManager = documentManager;
	}

	public String getUrlForPdf() {
		return urlForPdf;
	}

	public void setUrlForPdf(String urlForPdf) {
		this.urlForPdf = urlForPdf;
	}

	public String getEmailString() {
		return emailString;
	}

	public void setEmailString(String emailString) {
		this.emailString = emailString;
	}

	public String getLinkOption() {
		return linkOption;
	}

	public void setLinkOption(String linkOption) {
		this.linkOption = linkOption;
	}


	public FileExplorerController getExplorer() {
		return explorer;
	}


	public void setExplorer(FileExplorerController explorer) {
		this.explorer = explorer;
	}


	public String getPdfViewerUrl() {
		return pdfViewerUrl;
	}


	public void setPdfViewerUrl(String pdfViewerUrl) {
		this.pdfViewerUrl = pdfViewerUrl;
	}
	
	

}
