package com.virtualcode.controller;

import java.text.NumberFormat;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import com.virtualcode.util.VCSUtil;

public class MemorySizeConverter implements  Converter {

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		return value;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
	
		NumberFormat nf = NumberFormat.getInstance();
	
		String id = arg1.getId();
		if(id.equals("freeMemory") || id.equals("totalMemory")){
			float mbUnit = (1024 * 1024);
			Float value=VCSUtil.getFloatValue(arg2.toString());
			if(value<0)
				value=0f;
			return (nf.format(value/mbUnit))+" MB";
		}
//		instead of 10,492.123MB show it in GB, anything less than 1GB should be in MB and anything larger than 1023GB should be in TB's
//		if(id.equals("totalFreeMemory")){
//			float mbUnit = (1024 * 1024);
//			Float value=VCSUtil.getFloatValue(arg2.toString());
//			if(value<0)
//				value=0f;
//			else
//				value = value/mbUnit;
//			if(value<=1024f)				
//				return (nf.format(value))+" MB";
//			
//			if(value>1024f){
//				value = value/1024f;	//convert to GB
//				if(value<=1024f){
//					return (nf.format(value))+" GB";
//				}else{
//					value = value/1024f; //convert to TB
//					return (nf.format(value))+" TB";
//				}
//					
//					
//				
//			}
//		}
		
		long value = Math.round(Double.parseDouble(arg2+""));
		return (nf.format(value))+" MB";
		
	}

}
