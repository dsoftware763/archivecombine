package com.virtualcode.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import org.apache.commons.io.IOUtils;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

import com.virtualcode.common.VCSConstants;
import com.virtualcode.repository.monitor.ArchiveDateRunnerMonitor;
import com.virtualcode.repository.monitor.FileSizeRunnerMonitor;
import com.virtualcode.schedular.FileSizeRunner;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.vo.SystemAlerts;
import com.virtualcode.vo.SystemCodeType;
 
@ManagedBean(name="logoUploader")
@SessionScoped
public class LogoUploadController extends AbstractController implements Serializable {
	
	private String loginLogoPath;
			
	public LogoUploadController(){		
		super();		
	}
	
	public void paint(OutputStream stream, Object object) throws IOException {
		ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
		String outputPath = "/opt/tomcat/logos/";//+item.getName();
		if(ru.getCodeValue("path_for_logos", SystemCodeType.GENERAL)!=null)
			outputPath = ru.getCodeValue("path_for_logos", SystemCodeType.GENERAL);
		
		if(!outputPath.endsWith("/"))
			outputPath +="/";
		outputPath += "centered-logo.png";
	    File outputFolder = new File(outputPath);
	    InputStream is = new FileInputStream(outputFolder);
	    byte[] bytes = IOUtils.toByteArray(is);
        stream.write(bytes);
        stream.close();
    }  
	
	public void paintLogo(OutputStream stream, Object object) throws IOException {
		ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
		String outputPath = "/opt/tomcat/logos/";//+item.getName();
		if(ru.getCodeValue("path_for_logos", SystemCodeType.GENERAL)!=null)
			outputPath = ru.getCodeValue("path_for_logos", SystemCodeType.GENERAL);
		
		if(!outputPath.endsWith("/"))
			outputPath +="/";
		outputPath += "logo.png";
	    File outputFolder = new File(outputPath);
	    InputStream is = new FileInputStream(outputFolder);
	    byte[] bytes = IOUtils.toByteArray(is);
        stream.write(bytes);
        stream.close();
    }
	
	public void paintLogo1(OutputStream stream, Object object) throws IOException {
		ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
		String outputPath = "/opt/tomcat/logos/";//+item.getName();
		if(ru.getCodeValue("path_for_logos", SystemCodeType.GENERAL)!=null)
			outputPath = ru.getCodeValue("path_for_logos", SystemCodeType.GENERAL);
		
		if(!outputPath.endsWith("/"))
			outputPath +="/";
		outputPath += "logo-1.png";
	    File outputFolder = new File(outputPath);
	    InputStream is = new FileInputStream(outputFolder);
	    byte[] bytes = IOUtils.toByteArray(is);
        stream.write(bytes);
        stream.close();
    }
	
	public void upload(FileUploadEvent event) throws Exception {
	        UploadedFile item = event.getUploadedFile();
	        String id = event.getComponent().getId();
	        System.out.println(event.getComponent().getId());
	        System.out.println(getRequestParameter("name"));
	        
//	        String outputPath = "/opt/tomcat/logos/";//+item.getName();
			ResourcesUtil ru =  ResourcesUtil.getResourcesUtil();
			String outputPath = "/opt/tomcat/logos/";//+item.getName();
			if(ru.getCodeValue("path_for_logos", SystemCodeType.GENERAL)!=null)
				outputPath = ru.getCodeValue("path_for_logos", SystemCodeType.GENERAL);
			
			if(!outputPath.endsWith("/"))
				outputPath +="/";
	        File outputFolder = new File(outputPath);
	        outputFolder.mkdirs();
	        if(id.equals("centered-logo"))
	        	outputPath += "centered-logo.png";
	        else if(id.equals("title-logo"))
	        	outputPath += "logo.png";
	        else
	        	outputPath += "logo-1.png";
	        OutputStream os = new FileOutputStream(new File(outputPath));
	        os.write(item.getData());
	        os.close();
	}
	
	
}
