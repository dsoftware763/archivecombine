package com.virtualcode.vo;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

import com.virtualcode.ws.AgentManagementService;

/**
 * Agent entity. @author MyEclipse Persistence Tools
 */
@XmlRootElement(name="agent")
public class Agent implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer active;
	private String description;
	private String login;
	private String name;
	private String password;
	private String type = "FS";
	private String allowedIps;
	private Set jobs = new HashSet(0);
	
	private boolean online = false;	
	
	public static Map<String, Long> agentLastCallMap;
	
	private String displayName;

	// Constructors

	/** default constructor */
	public Agent() {
	}

	/** minimal constructor */
	public Agent(Integer active, String login, String name, String password,
			String type, String allowedIps) {
		this.active = active;
		this.login = login;
		this.name = name;
		this.password = password;
		this.type = type;
		this.allowedIps = allowedIps;
	}

	/** full constructor */
	public Agent(Integer active, String description, String login, String name,
			String password, String type, String allowedIps, Set jobs) {
		this.active = active;
		this.description = description;
		this.login = login;
		this.name = name;
		this.password = password;
		this.type = type;
		this.allowedIps = allowedIps;
		this.jobs = jobs;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getActive() {
		return this.active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAllowedIps() {
		return this.allowedIps;
	}

	public void setAllowedIps(String allowedIps) {
		this.allowedIps = allowedIps;
	}

	public Set getJobs() {
		return this.jobs;
	}

	public void setJobs(Set jobs) {
		this.jobs = jobs;
	}

	public boolean isOnline() {
		Long lastActiveTime = agentLastCallMap.get(name);
		if(lastActiveTime!=null){
			Long diff = System.currentTimeMillis()-lastActiveTime;
			if(diff<=30000){	//if agent has made a call in the last hour or less
				return true;
			}
		}
		return online;
	}

	public void setOnline(boolean online) {
		this.online = online;
	}

	public String getDisplayName() {
		if(allowedIps.equals("127.0.0.1"))
			displayName = name + "- built in";
		else
			displayName = name + "- remote";
		return displayName;
	}

}