package com.virtualcode.vo;

import java.util.HashSet;
import java.util.Set;


/**
 * ErrorCode entity. @author MyEclipse Persistence Tools
 */

public class ErrorCode  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String description;
     private Set jobStatuses = new HashSet(0);
     private Set jobDocuments = new HashSet(0);


    // Constructors

    /** default constructor */
    public ErrorCode() {
    }

	/** minimal constructor */
    public ErrorCode(String description) {
        this.description = description;
    }
    
    /** full constructor */
    public ErrorCode(String description, Set jobStatuses, Set jobDocuments) {
        this.description = description;
        this.jobStatuses = jobStatuses;
        this.jobDocuments = jobDocuments;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    public Set getJobStatuses() {
        return this.jobStatuses;
    }
    
    public void setJobStatuses(Set jobStatuses) {
        this.jobStatuses = jobStatuses;
    }

    public Set getJobDocuments() {
        return this.jobDocuments;
    }
    
    public void setJobDocuments(Set jobDocuments) {
        this.jobDocuments = jobDocuments;
    }
   








}