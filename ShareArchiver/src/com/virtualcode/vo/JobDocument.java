package com.virtualcode.vo;

import java.sql.Timestamp;


/**
 * JobDocument entity. @author MyEclipse Persistence Tools
 */

public class JobDocument  implements java.io.Serializable {


    // Fields    

     private Long id;
     private Job job;
     private ErrorCode errorCode;
     private String docId;
     private String docSpPath;
     private Timestamp lastAccessed;
     private Timestamp lastModified;
     private String lastModifiedBy;
     private Timestamp schedularInsertDate;
     private Integer size;
     private String status;
     private String title;
     private String version;
     private String createdDate;


    // Constructors

    /** default constructor */
    public JobDocument() {
    }

	/** minimal constructor */
    public JobDocument(Job job, ErrorCode errorCode, String docId, String docSpPath, Timestamp lastAccessed, Timestamp lastModified, String lastModifiedBy, Timestamp schedularInsertDate, Integer size, String status, String title) {
        this.job = job;
        this.errorCode = errorCode;
        this.docId = docId;
        this.docSpPath = docSpPath;
        this.lastAccessed = lastAccessed;
        this.lastModified = lastModified;
        this.lastModifiedBy = lastModifiedBy;
        this.schedularInsertDate = schedularInsertDate;
        this.size = size;
        this.status = status;
        this.title = title;
    }
    
    /** full constructor */
    public JobDocument(Job job, ErrorCode errorCode, String docId, String docSpPath, Timestamp lastAccessed, Timestamp lastModified, String lastModifiedBy, Timestamp schedularInsertDate, Integer size, String status, String title, String version, String createdDate) {
        this.job = job;
        this.errorCode = errorCode;
        this.docId = docId;
        this.docSpPath = docSpPath;
        this.lastAccessed = lastAccessed;
        this.lastModified = lastModified;
        this.lastModifiedBy = lastModifiedBy;
        this.schedularInsertDate = schedularInsertDate;
        this.size = size;
        this.status = status;
        this.title = title;
        this.version = version;
        this.createdDate = createdDate;
    }

   
    // Property accessors

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public Job getJob() {
        return this.job;
    }
    
    public void setJob(Job job) {
        this.job = job;
    }

    public ErrorCode getErrorCode() {
        return this.errorCode;
    }
    
    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public String getDocId() {
        return this.docId;
    }
    
    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getDocSpPath() {
        return this.docSpPath;
    }
    
    public void setDocSpPath(String docSpPath) {
        this.docSpPath = docSpPath;
    }

    public Timestamp getLastAccessed() {
        return this.lastAccessed;
    }
    
    public void setLastAccessed(Timestamp lastAccessed) {
        this.lastAccessed = lastAccessed;
    }

    public Timestamp getLastModified() {
        return this.lastModified;
    }
    
    public void setLastModified(Timestamp lastModified) {
        this.lastModified = lastModified;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }
    
    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Timestamp getSchedularInsertDate() {
        return this.schedularInsertDate;
    }
    
    public void setSchedularInsertDate(Timestamp schedularInsertDate) {
        this.schedularInsertDate = schedularInsertDate;
    }

    public Integer getSize() {
        return this.size;
    }
    
    public void setSize(Integer size) {
        this.size = size;
    }

    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }

    public String getVersion() {
        return this.version;
    }
    
    public void setVersion(String version) {
        this.version = version;
    }

    public String getCreatedDate() {
        return this.createdDate;
    }
    
    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
   








}