package com.virtualcode.vo;
// default package

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;


/**
 * User entity. @author MyEclipse Persistence Tools
 */

public class User  implements java.io.Serializable {


    // Fields    

     private Long id;
     private String email;
     private String firstName;
     private String lastName;
     private String lockStatus;
     private String passwordHash;
     private String confirmPassword;
     private String username;
     private Set<Role> userRoles = new HashSet<Role>(0);
     private Set shiroUserPermissionses = new HashSet(0);
     
     private String userRolesStr;
     
     private String resetPassword;
     private String confirmResetPassword;
     
     private Timestamp lastPasswordChange;
     
     private Role role=new Role();//since we are not using many-to-many relationship in GUI
     

    // Constructors

    /** default constructor */
    public User() {
    }

	/** minimal constructor */
    public User(String email, String firstName, String lastName, String lockStatus, String passwordHash, String username) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.lockStatus = lockStatus;
        this.passwordHash = passwordHash;
        this.username = username;
    }
    
    /** full constructor */
    public User(String email, String firstName, String lastName, String lockStatus, String passwordHash, String username, Set<Role> shiroUserRoleses, Set shiroUserPermissionses) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.lockStatus = lockStatus;
        this.passwordHash = passwordHash;
        this.username = username;
        this.userRoles = shiroUserRoleses;
        this.shiroUserPermissionses = shiroUserPermissionses;
    }

   
    // Property accessors

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return this.firstName;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }
    
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLockStatus() {
        return this.lockStatus;
    }
    
    public void setLockStatus(String lockStatus) {
        this.lockStatus = lockStatus;
    }

    public String getPasswordHash() {
        return this.passwordHash;
    }
    
    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }
        
    public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getUsername() {
        return this.username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
  

    public Set<Role> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(Set<Role> userRoles) {
		this.userRoles = userRoles;
	}

	public Set getShiroUserPermissionses() {
        return this.shiroUserPermissionses;
    }
    
    public void setShiroUserPermissionses(Set shiroUserPermissionses) {
        this.shiroUserPermissionses = shiroUserPermissionses;
    }

	public String getUserRolesStr() {
		return userRolesStr;
	}

	public void setUserRolesStr(String userRolesStr) {
		this.userRolesStr = userRolesStr;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getResetPassword() {
		return resetPassword;
	}

	public void setResetPassword(String resetPassword) {
		this.resetPassword = resetPassword;
	}

	public String getConfirmResetPassword() {
		return confirmResetPassword;
	}

	public void setConfirmResetPassword(String confirmResetPassword) {
		this.confirmResetPassword = confirmResetPassword;
	}

	public Timestamp getLastPasswordChange() {
		return lastPasswordChange;
	}

	public void setLastPasswordChange(Timestamp lastPasswordChange) {
		this.lastPasswordChange = lastPasswordChange;
	}
	
	
   


}