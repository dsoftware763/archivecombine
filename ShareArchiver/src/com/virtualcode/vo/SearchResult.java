/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.virtualcode.vo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlRootElement(name="SearchResult")
@XmlSeeAlso({SearchDocument.class})
public class SearchResult {

    private List<SearchDocument> results=new ArrayList<SearchDocument>();
 
    private Integer totalRecordsFound;
    
    private long comulativeSizeFound;

    private String contextPath;
    
    private String searchQuery;
   
    /**
     * @return the searchResults
     */
     @XmlElementWrapper
     @XmlElement(name="SearchDocument")
    public List<SearchDocument> getResults() {
        return results;
    }

    /**
     * @param searchResults the searchResults to set
     */
    public void setResults(List<SearchDocument> results) {
        this.results=results;
    }
    
    /**
     * @return the totalRecordsFound
     */
    @XmlElement(name="totalRecordsFound")
    public Integer getTotalRecordsFound() {
        return totalRecordsFound;
    }

    /**
     * @param totalRecordsFound the totalRecordsFound to set
     */
    public void setTotalRecordsFound(Integer totalRecordsFound) {
        this.totalRecordsFound = totalRecordsFound;
    }

    /**
     * @return the contextPath
     */
    @XmlElement(name="contextPath")
    public String getContextPath() {
        return contextPath;
    }

    /**
     * @param contextPath the contextPath to set
     */
    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }

	public String getSearchQuery() {
		return searchQuery;
	}

	public void setSearchQuery(String searchQuery) {
		this.searchQuery = searchQuery;
	}

	public long getComulativeSizeFound() {
		return comulativeSizeFound;
	}

	public void setComulativeSizeFound(long comulativeSizeFound) {
		this.comulativeSizeFound = comulativeSizeFound;
	}

    

      

}