package com.virtualcode.vo;

/**
 * LdapGroups entity. @author MyEclipse Persistence Tools
 */

public class LdapGroups implements java.io.Serializable {

	// Fields

	private Integer id;
	private String groupName;
	private Integer restrictGroup;
	private Integer enableTranscript;
	
	private boolean selected;

	// Constructors

	/** default constructor */
	public LdapGroups() {
	}

	/** full constructor */
	public LdapGroups(String groupName, Integer restrictGroup,
			Integer enableTranscript) {
		this.groupName = groupName;
		this.restrictGroup = restrictGroup;
		this.enableTranscript = enableTranscript;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGroupName() {
		return this.groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getRestrictGroup() {
		return this.restrictGroup;
	}

	public void setRestrictGroup(Integer restrictGroup) {
		this.restrictGroup = restrictGroup;
	}

	public Integer getEnableTranscript() {
		return this.enableTranscript;
	}

	public void setEnableTranscript(Integer enableTranscript) {
		this.enableTranscript = enableTranscript;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}