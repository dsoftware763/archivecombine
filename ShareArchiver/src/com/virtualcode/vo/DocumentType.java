package com.virtualcode.vo;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.tree.TreeNode;

/**
 * DocumentType entity. 
 * @author AtiqurRehman
 */

public class DocumentType extends NamedNode  implements java.io.Serializable,javax.swing.tree.TreeNode {

    // Fields    

     private Integer id;
     private DocumentCategory documentCategory;
     private String value;
     private Set policyDocumentTypes = new HashSet(0);
     
     private Boolean selected=false;
     
    // Constructors

    /** default constructor */
    public DocumentType() {
    	setType("DocumentType");
    }
	  
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public DocumentCategory getDocumentCategory() {
        return this.documentCategory;
    }
    
    public void setDocumentCategory(DocumentCategory documentCategory) {
        this.documentCategory = documentCategory;
    }

    public String getValue() {
        return this.value;
    }
    
    public void setValue(String value) {
        this.value = value;
    }

    public Set getPolicyDocumentTypes() {
        return this.policyDocumentTypes;
    }
    
    public void setPolicyDocumentTypes(Set policyDocumentTypes) {
        this.policyDocumentTypes = policyDocumentTypes;
    }

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	
	public TreeNode getChildAt(int childIndex) {
        return null;
    }
 
    public int getChildCount() {
        return 0;
    }
 
    public TreeNode getParent() {
        return documentCategory;
    }
 
    public int getIndex(TreeNode node) {
        return 0;
    }
 
    public boolean getAllowsChildren() {
        return false;
    }
 
    public boolean isLeaf() {
        return true;
    }
 
    public Enumeration<DocTypeTreeNode> children() {
        return null;
    }
      
   

}