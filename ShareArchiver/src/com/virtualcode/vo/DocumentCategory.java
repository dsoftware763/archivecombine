package com.virtualcode.vo;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.tree.TreeNode;

import com.google.common.collect.Iterators;



/**
 * DocumentCategory entity. 
 * @author AtiqurRehman
 */

public class DocumentCategory  extends NamedNode  implements java.io.Serializable,TreeNode {

    // Fields  

     private Integer id;
     private String description;     
     private Set<DocumentType> documentTypes = new HashSet<DocumentType>(0);
     private List<DocumentType> documentTypesList=new ArrayList<DocumentType>(0);
     private List<String> docExtList=new ArrayList<String>();
     private Boolean selected=false;

    // Constructors

    /** default constructor */
    public DocumentCategory() {
    	setType("DocumentCategory");
    }
   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

  
    public Set<DocumentType> getDocumentTypes() {
        return this.documentTypes;
    }
    
    public void setDocumentTypes(Set<DocumentType> documentTypes) {
        this.documentTypes = documentTypes;
    }
    
	public List<DocumentType> getDocumentTypesList() {
		return documentTypesList;
	}

	public void setDocumentTypesList(List<DocumentType> documentTypesList) {
		this.documentTypesList = documentTypesList;
	}

	
	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public TreeNode getChildAt(int childIndex) {
        return documentTypesList.get(childIndex);
    }
 
    public int getChildCount() {
        return documentTypesList.size();
    }
 
    public TreeNode getParent() {
        return null;
    }
 
    public int getIndex(TreeNode node) {
        return documentTypesList.indexOf(node);
    }
 
    public boolean getAllowsChildren() {
        return true;
    }
 
    public boolean isLeaf() {
        return documentTypesList.isEmpty();
    }
 
    public Enumeration<DocumentType> children() {
        return Iterators.asEnumeration(documentTypesList.iterator());
    }
    
    
    public List<DocumentType>  getSelectedDocumentTypes(){
    	List<DocumentType> results=new ArrayList<DocumentType>();
    	
    	for(int i=0;documentTypesList!=null &&   i<documentTypesList.size();i++){
    		if(documentTypesList.get(i).getSelected()==true)
    			results.add(documentTypesList.get(i));
    	}    	
    	return results;    	
    }
    
        
    public List<String> getDocExtList() {
		return docExtList;
	}

	public void setDocExtList(List<String> docExtList) {
		this.docExtList = docExtList;
	}

	@Override
    public String toString(){
    	String str="\n\nCategory Name: --->"+this.getName();
    	for(int i=0;documentTypesList!=null && i<documentTypesList.size();i++){
           str+=" Document Type: -->"+documentTypesList.get(i).getName();
        }    	
    	return str;    	
    	
    }
    
    
    
    
    

}