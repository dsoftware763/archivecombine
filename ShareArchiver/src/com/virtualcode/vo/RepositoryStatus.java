package com.virtualcode.vo;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;


public class RepositoryStatus {

    protected long uniqueFiles;
    
    protected long dupFiles;
    
    protected long totalFolders;
    
    protected List<AgentDocument> agentDocuments;

    protected String repositoryStatus;
    
    protected double repositorySize;//changed to double
    
    protected long repositoryIndexSize;
   
    protected OSStatus osStatus;
   
    protected String repositoryVersion;
  
    protected String softwareVersion;

    
    private double duplicateDataSize;
    
    private long totalArchived;
    
    private long totalFreeSpaceOnDisk;
    
    private String totalSpace;
    
    private String freeSpace; 
    


	public long getUniqueFiles() {
		return uniqueFiles;
	}

	public void setUniqueFiles(long totalFiles) {
		this.uniqueFiles = totalFiles;
	}
	
	public long getDupFiles() {
		return dupFiles;
	}


	public void setDupFiles(long dupFiles) {
		this.dupFiles = dupFiles;
	}


	public long getTotalFolders() {
		return totalFolders;
	}


	public void setTotalFolders(long totalFolders) {
		this.totalFolders = totalFolders;
	}


	public List<AgentDocument> getAgentDocuments() {
		return agentDocuments;
	}


	public void setAgentDocuments(List<AgentDocument> agentDocuments) {
		this.agentDocuments = agentDocuments;
	}


	public String getRepositoryStatus() {
		return repositoryStatus;
	}


	public void setRepositoryStatus(String repositoryStatus) {
		this.repositoryStatus = repositoryStatus;
	}


	public double getRepositorySize() {
		return repositorySize;
	}


	public void setRepositorySize(double repositorySize) {
		this.repositorySize = repositorySize;
	}


	public long getRepositoryIndexSize() {
		return repositoryIndexSize;
	}


	public void setRepositoryIndexSize(long repositoryIndexSize) {
		this.repositoryIndexSize = repositoryIndexSize;
	}


	public OSStatus getOsStatus() {
		return osStatus;
	}


	public void setOsStatus(OSStatus osStatus) {
		this.osStatus = osStatus;
	}


	public String getRepositoryVersion() {
		return repositoryVersion;
	}


	public void setRepositoryVersion(String repositoryVersion) {
		this.repositoryVersion = repositoryVersion;
	}


	public String getSoftwareVersion() {
		return softwareVersion;
	}


	public void setSoftwareVersion(String softwareVersion) {
		this.softwareVersion = softwareVersion;
	}


	public double getDuplicateDataSize() {
		return duplicateDataSize;
	}


	public void setDuplicateDataSize(double duplicateDataSize) {
		this.duplicateDataSize = duplicateDataSize;
	}


	public double getTotalArchived() {
		return repositorySize+duplicateDataSize;
	}


	public void setTotalArchived(long totalArchived) {
		this.totalArchived = totalArchived;
	}

	public long getTotalFreeSpaceOnDisk() {
		return totalFreeSpaceOnDisk;
	}

	public void setTotalFreeSpaceOnDisk(long totalFreeSpaceOnDisk) {
		this.totalFreeSpaceOnDisk = totalFreeSpaceOnDisk;
	}

	public String getTotalSpace() {
		return totalSpace;
	}

	public void setTotalSpace(String totalSpace) {
		this.totalSpace = totalSpace;
	}

	public String getFreeSpace() {
		return freeSpace;
	}

	public void setFreeSpace(String freeSpace) {
		this.freeSpace = freeSpace;
	}
	
	

 }
