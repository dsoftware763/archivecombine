package com.virtualcode.vo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.swing.tree.TreeNode;

import org.apache.commons.lang.ArrayUtils;

import com.google.common.collect.Iterators;
import com.virtualcode.util.VCSUtil;

public class CustomTreeNode extends NamedNode  implements TreeNode {

	public CustomTreeNode(){	
			
	}
	
	private CustomTreeNode parentNode=null;
	private List<CustomTreeNode> childNodes=new ArrayList<CustomTreeNode>();

	private String versionOf	=	null;
	private List<CustomTreeNode> nextVersions	=	null;
		
	private String uuid;
	
	private String path;
	
	private String fileTypeDesc;	
	
	private String createdDate;
	
	private String archivedDate;
	
	private String  lastModified;
	
	private String lastAccessed;
	
	private String downloadUrl;
	
	private String iconUrl;
	
	private String securityAllowSIDs;
	
	private String securityDenySIDs;
	
	private String shareAllowSIDs;
	
	private String shareDenySIDs;
	
    private String securityAllowGroupNames;
	
	private String securityDenyGroupNames;
	
	private String shareAllowGroupNames;
	
	private String shareDenyGroupNames;
	
	
	private String mimeType;
	
	private String fileSize;
	
	private boolean selected=false;
	
	private String fileFoldersCount=null;
	
	private boolean linkedNode=false;
	
	private String tags;
	
	private String tagUser;
	
	private String tagTimeStamp;
	
	//for drive letters
	private String compressedURL;
	
	//for sorting based on size of docs
	private String sizeinKbs;
	
	private boolean mediaFile=false;
	
	private String contentType;
	
	private String spDocUrl;
	
	private String archivingMode; //normal/dynamic
	
	private boolean version = false;
	
	private boolean versionExpanded = false;
	
	private boolean archive = true;	//file from archive or from UNC
	
	private boolean pdfViewable = false;
	
	private String sharedBy;
	
	private String sharedOn;
	
	private String hash;	//only for remote files
	
	private String originalPath;
	
	private String originalName;
	
	//valid for print archived docs only
	private String printerName;
	
	private String machineName;
	
	private String username;
	
	private String totalPages;
	
	@Override
	public TreeNode getChildAt(int childIndex) {
		
		return childNodes.get(childIndex);
	}

	@Override
	public int getChildCount() {
		return childNodes.size();
	}

	@Override
	public TreeNode getParent() {
		return parentNode;
	}

	@Override
	public int getIndex(TreeNode node) {
		return childNodes.indexOf(node);
	}

	@Override
	public boolean getAllowsChildren() {
		return true;
	}

	@Override
	public boolean isLeaf() {
		if(this.getType().equalsIgnoreCase("nt:file"))
			return true;
				 
		 return false;
	}

	@Override
	public Enumeration<CustomTreeNode> children() {	
		System.out.println("Enumerating childs of : "+path+" selected: "+selected);		
		return Iterators.asEnumeration(childNodes.iterator());
	}
	 
		
	public CustomTreeNode getParentNode() {
		return parentNode;
	}

	public void setParentNode(CustomTreeNode parentNode) {
		this.parentNode = parentNode;
	}

	public List<CustomTreeNode> getChildNodes() {
		return childNodes;
	}

	public void setChildNodes(List<CustomTreeNode> childNodes) {
		this.childNodes = childNodes;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getFileTypeDesc() {
		return fileTypeDesc;
	}

	public void setFileTypeDesc(String fileTypeDesc) {
		this.fileTypeDesc = fileTypeDesc;
	}
	
	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	
	public String getArchivedDate() {
		return archivedDate;
	}

	public void setArchivedDate(String archivedDate) {
		this.archivedDate = archivedDate;
	}

	public String getLastModified() {
		return lastModified;
	}

	public void setLastModified(String lastModified) {
		this.lastModified = lastModified;
	}

	public String getLastAccessed() {
		return lastAccessed;
	}

	public void setLastAccessed(String lastAccessed) {
		this.lastAccessed = lastAccessed;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

		
	public String getSecurityAllowSIDs() {
		return securityAllowSIDs;
	}

	public void setSecurityAllowSIDs(String securityAllowSIDs) {
		this.securityAllowSIDs = securityAllowSIDs;
	}

	public String getSecurityDenySIDs() {
		return securityDenySIDs;
	}

	public void setSecurityDenySIDs(String securityDenySIDs) {
		this.securityDenySIDs = securityDenySIDs;
	}

	public String getShareAllowSIDs() {
		return shareAllowSIDs;
	}

	public void setShareAllowSIDs(String shareAllowSIDs) {
		this.shareAllowSIDs = shareAllowSIDs;
	}

	public String getShareDenySIDs() {
		return shareDenySIDs;
	}

	public void setShareDenySIDs(String shareDenySIDs) {
		this.shareDenySIDs = shareDenySIDs;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	

	public String getSecurityAllowGroupNames() {
		return securityAllowGroupNames;
	}

	public void setSecurityAllowGroupNames(String securityAllowGroupNames) {
		this.securityAllowGroupNames = securityAllowGroupNames;
	}

	public String getSecurityDenyGroupNames() {
		return securityDenyGroupNames;
	}

	public void setSecurityDenyGroupNames(String securityDenyGroupNames) {
		this.securityDenyGroupNames = securityDenyGroupNames;
	}

	public String getShareAllowGroupNames() {
		return shareAllowGroupNames;
	}

	public void setShareAllowGroupNames(String shareAllowGroupNames) {
		this.shareAllowGroupNames = shareAllowGroupNames;
	}

	public String getShareDenyGroupNames() {
		return shareDenyGroupNames;
	}

	public void setShareDenyGroupNames(String shareDenyGroupNames) {
		this.shareDenyGroupNames = shareDenyGroupNames;
	}
	

	public String getFileFoldersCount() {
		return fileFoldersCount;
	}

	public void setFileFoldersCount(String fileFoldersCount) {
		this.fileFoldersCount = fileFoldersCount;
	}
	
	public boolean isLinkedNode() {
		return linkedNode;
	}

	public void setLinkedNode(boolean linkedNode) {
		this.linkedNode = linkedNode;
	}

	public String getUNCPath(){
		String unc=null;
		if(path!=null){
			if(path.indexOf("/FS/")>0){
			  unc=path.substring(path.indexOf("/FS/")+4, path.length());
			}else if(path.indexOf("/SP/")>0){
				 unc=path.substring(path.indexOf("/SP/")+4, path.length());
		    }
		}
		if(unc!=null){
//			System.out.println("unc before replacing: "+unc);
			unc="\\\\"+unc.replaceAll("/", "\\\\");
			if(version){				 
    			if(unc.indexOf("-")!=-1){
    				System.out.println(unc.substring(0, unc.lastIndexOf("-")).trim());
    				System.out.println(unc.substring(unc.lastIndexOf("-")+4).trim());
    				unc = unc.substring(0, unc.lastIndexOf("-")).trim() + unc.substring(unc.lastIndexOf("-")+4).trim();
    			}

			}
		}
		return unc;
	}

	@Override
	public String toString(){
		
		return " Name: "+getName()+"  Path: "+path+" type: "+getType()+" selected: "+selected;
		
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getCompressedURL() {
		return compressedURL;
	}

	public void setCompressedURL(String compressedURL) {
		this.compressedURL = compressedURL;
	}

	public String getTagUser() {
		return tagUser;
	}

	public void setTagUser(String tagUser) {
		this.tagUser = tagUser;
	}

	public String getTagTimeStamp() {
		return tagTimeStamp;
	}

	public void setTagTimeStamp(String tagTimeStamp) {
		this.tagTimeStamp = tagTimeStamp;
	}

	public String getSizeinKbs() {
		return sizeinKbs;
	}

	public void setSizeinKbs(String sizeinKbs) {
		this.sizeinKbs = sizeinKbs;
	}
			
	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getSpDocUrl() {
		return spDocUrl;
	}

	public void setSpDocUrl(String spDocUrl) {
		this.spDocUrl = spDocUrl;
	}

	private List<String> mediaFiles=new ArrayList<String>(Arrays.asList("mp4", "m4v", "f4v", "mov", "aac", "m4a", "f4a","mp3","flv","ogg","oga","flv"));
	//;
	
	public void setMediaFile(boolean mediaFile) {
		this.mediaFile = mediaFile;
	}

	public boolean isMediaFile() {
		if(this.getType().equals("nt:file") ||this.getType().equals("nt:linkedFile") ){
				String fileExt=path.substring(path.lastIndexOf("/")+1);
				fileExt=fileExt.substring(fileExt.lastIndexOf(".")+1);
				if(mediaFiles.contains(fileExt.toLowerCase())){
					return true;
				}else{
					return false;
				}			
		}else{
		     return false;
		}
	}

	public String getArchivingMode() {
		return archivingMode;
	}

	public void setArchivingMode(String archivingMode) {
		this.archivingMode = archivingMode;
	}

	public int getNextVersionsSize() {
		return (nextVersions!=null)?nextVersions.size():0;
	}

	public List<CustomTreeNode> getNextVersions() {
		return nextVersions;
	}

	public void setNextVersions(List<CustomTreeNode> nextVersions) {
		this.nextVersions = nextVersions;
	}

	public String getVersionOf() {
		return versionOf;
	}

	public void setVersionOf(String versionOf) {
		this.versionOf = versionOf;
	}

	public boolean isVersion() {
		return version;
	}

	public void setVersion(boolean version) {
		this.version = version;
	}

	public boolean isVersionExpanded() {
		return versionExpanded;
	}

	public void setVersionExpanded(boolean versionExpanded) {
		this.versionExpanded = versionExpanded;
	}

	public boolean isArchive() {
		return archive;
	}

	public void setArchive(boolean archive) {
		this.archive = archive;
	}

	public boolean isPdfViewable() {
		pdfViewable = VCSUtil.isPdfViewable(getName());
		return pdfViewable;
	}

	public void setPdfViewable(boolean pdfViewable) {
		this.pdfViewable = pdfViewable;
	}

	public String getSharedBy() {
		return sharedBy;
	}

	public void setSharedBy(String sharedBy) {
		this.sharedBy = sharedBy;
	}

	public String getSharedOn() {
		return sharedOn;
	}

	public void setSharedOn(String sharedOn) {
		this.sharedOn = sharedOn;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getOriginalPath() {
		return originalPath;
	}

	public void setOriginalPath(String originalPath) {
		this.originalPath = originalPath;
	}

	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public String getPrinterName() {
		return printerName;
	}

	public void setPrinterName(String printerName) {
		this.printerName = printerName;
	}

	public String getMachineName() {
		return machineName;
	}

	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(String totalPages) {
		this.totalPages = totalPages;
	}
}
