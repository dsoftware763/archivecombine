package com.virtualcode.vo;

import java.util.Date;
import java.util.Map;

public class LockedUser {
	
	private String username;
	
	private int noOfTries = 0;
	
	private boolean lockStatus = false;
	
	private Date lockoutTime;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getNoOfTries() {
		return noOfTries;
	}

	public void setNoOfTries(int noOfTries) {
		this.noOfTries = noOfTries;
	}

	public boolean isLockStatus() {
		return lockStatus;
	}

	public void setLockStatus(boolean lockStatus) {
		this.lockStatus = lockStatus;
	}
	
	public void incNoOfTries(){
		noOfTries+=1;
	}
	
	public static Map<String, LockedUser> lockedUsersMap;

	public Date getLockoutTime() {
		return lockoutTime;
	}

	public void setLockoutTime(Date lockoutTime) {
		this.lockoutTime = lockoutTime;
	}

	
}
