package com.virtualcode.vo;


public class RoleFeature implements java.io.Serializable {

	private Integer id;
	private String name;
	private String displayName;
	private Boolean adminAllowed=true;
	private Boolean privAllowed=false;
	private Boolean ldapAllowed=false;
	
	private boolean adminEditable=false;
	private boolean privEditable=false;

	// Constructors

	/** default constructor */
	public RoleFeature() {
	}

	/** minimal constructor */
	public RoleFeature(String name) {
		this.name = name;
	}

	/** full constructor */
	public RoleFeature(String name,String displayName, Boolean adminAllowed, Boolean privAllowed, Boolean ldapAllowed) {
		this.name = name;
		this.displayName=displayName;
		this.adminAllowed = adminAllowed;
		this.privAllowed = privAllowed;
		this.ldapAllowed = ldapAllowed;
		
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Boolean getAdminAllowed() {
		return adminAllowed;
	}

	public void setAdminAllowed(Boolean adminAllowed) {
		this.adminAllowed = adminAllowed;
	}

	public Boolean getPrivAllowed() {
		return privAllowed;
	}

	public void setPrivAllowed(Boolean privAllowed) {
		this.privAllowed = privAllowed;
	}

	public Boolean getLdapAllowed() {
		return ldapAllowed;
	}

	public void setLdapAllowed(Boolean ldapAllowed) {
		this.ldapAllowed = ldapAllowed;
	}

	public void setAdminEditable(boolean adminEditable) {
		this.adminEditable = adminEditable;
	}

	public boolean isAdminEditable(){
		if(name.endsWith("_sp"))
		    adminEditable=false;
		else
			adminEditable=true;
		
		return adminEditable;
	}
	
	public boolean isPrivEditable(){
		if(name.endsWith("_fe"))
		    privEditable=false;
		else
			privEditable= true;		
		return privEditable;
	}
	
	public void setPrivEditable(boolean privEditable) {
		this.privEditable = privEditable;
	}
	
	

}