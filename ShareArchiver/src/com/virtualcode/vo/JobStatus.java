package com.virtualcode.vo;

import java.io.File;
import java.sql.Timestamp;

import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;

/**
 * JobStatus entity. @author MyEclipse Persistence Tools
 */

public class JobStatus implements java.io.Serializable {

	// Fields

	private Integer id;
	private Job job;
	private ErrorCode errorCode;
	private String description;
	private Timestamp execStartTime;
	private Timestamp execEndTime;//only for presentation layer
	private int current=0;
	private Integer previousJobStatusId;
	private String status;
	private String executionId;
	private String actionByUser;
	private Timestamp actionTime;
	
	private String serialNumber;//only for presentation layer

	private int numberOfPublicLogs	=	0;
	// Constructors

	/** default constructor */
	public JobStatus() {
	}

	/** minimal constructor */
	public JobStatus(Job job, ErrorCode errorCode, Timestamp execStartTime,
			int current, Integer previousJobStatusId, String status) {
		this.job = job;
		this.errorCode = errorCode;
		this.execStartTime = execStartTime;
		this.current = current;
		this.previousJobStatusId = previousJobStatusId;
		this.status = status;
	}

	/** full constructor */
	public JobStatus(Job job, ErrorCode errorCode, String description,
			Timestamp execStartTime, int current,
			Integer previousJobStatusId, String status, String executionId) {
		this.job = job;
		this.errorCode = errorCode;
		this.description = description;
		this.execStartTime = execStartTime;
		this.current = current;
		this.previousJobStatusId = previousJobStatusId;
		this.status = status;
		this.executionId = executionId;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Job getJob() {
		return this.job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public ErrorCode getErrorCode() {
		return this.errorCode;
	}

	public void setErrorCode(ErrorCode errorCode) {
		this.errorCode = errorCode;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getExecStartTime() {
		return this.execStartTime;
	}

	public void setExecStartTime(Timestamp execStartTime) {
		this.execStartTime = execStartTime;
	}


	public int getCurrent() {
		return current;
	}

	public void setCurrent(int current) {
		this.current = current;
	}

	public Integer getPreviousJobStatusId() {
		return this.previousJobStatusId;
	}

	public void setPreviousJobStatusId(Integer previousJobStatusId) {
		this.previousJobStatusId = previousJobStatusId;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getExecutionId() {
		return this.executionId;
	}

	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}

	public Timestamp getExecEndTime() {
		return execEndTime;
	}

	public void setExecEndTime(Timestamp execEndTime) {
		this.execEndTime = execEndTime;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	
	
	public int getNumberOfPublicLogs() {
		int result	=	this.numberOfPublicLogs;
		
		try {
			String basePath	=	Utility.GetProp("logBasePath");
			if(basePath==null || basePath.isEmpty())
				basePath     =   "/jobsLog";
			 
			else {
				basePath	=	basePath + ((basePath.endsWith("/"))?"":"/") +"jobsLog";
			}
			 
			String filePath	=	basePath + "/" + this.job.getId() + "/" + this.executionId + "/"+LoggingManager.PUBLIC_MSGS+".log";			
			File temp	=	new File(filePath);
			if(temp.exists()) {
				result	=	result+1;
			}
			temp	=	null;
				
		} catch(Exception ex){
			ex.printStackTrace();
		}
		
		return result;
	}

	public void setNumberOfPublicLogs(int numberOfPublicLogs) {
		this.numberOfPublicLogs = numberOfPublicLogs;
	}

	public String getActionByUser() {
		return actionByUser;
	}

	public void setActionByUser(String actionByUser) {
		this.actionByUser = actionByUser;
	}

	public Timestamp getActionTime() {
		return actionTime;
	}

	public void setActionTime(Timestamp actionTime) {
		this.actionTime = actionTime;
	}

	

}