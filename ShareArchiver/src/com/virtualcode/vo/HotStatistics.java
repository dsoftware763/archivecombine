package com.virtualcode.vo;

import java.util.concurrent.atomic.AtomicLong;

import com.virtualcode.agent.das.utils.MyHashMap;

public class HotStatistics {
	public static HotStatistics hotStatistics;
	
	private MyHashMap evaluatedCtr;
	private MyHashMap archivedCtr;
	private MyHashMap alreadyArchivedCtr;
	private MyHashMap stubbedCtr;
	private MyHashMap exportedCtr;	
	private MyHashMap failedCtr;
	private MyHashMap completedCtr;
	private MyHashMap hotStatisticsAvailable;
	
	private HotStatistics() {
		// TODO Auto-generated constructor stub		
		evaluatedCtr = new MyHashMap();
		archivedCtr = new MyHashMap();
		alreadyArchivedCtr = new MyHashMap();
		stubbedCtr = new MyHashMap();
		exportedCtr = new MyHashMap();
		failedCtr = new MyHashMap();
		completedCtr = new MyHashMap();
		
	}
	
	public static HotStatistics getInstance(){
		if(hotStatistics==null)
			hotStatistics = new HotStatistics();
		
		return hotStatistics;
	}
	
	public void setHotStatisticsAvailable(String execId, Long value ){
		hotStatisticsAvailable.put(execId, new AtomicLong(value));
	}
	
	public void setEvaluatedCtr(String execId, Long value ){
		evaluatedCtr.put(execId, new AtomicLong(value));
	}
	
	public void setArchivedCtr(String execId, Long value ){
		archivedCtr.put(execId, new AtomicLong(value));
	}
	
	public void setStubbedCtr(String execId, Long value ){
		stubbedCtr.put(execId, new AtomicLong(value));
	}
	
	public void setExportedCtr(String execId, Long value){
		exportedCtr.put(execId, new AtomicLong(value));
	}
	
	public void setFailedCtr(String execId, Long value ){
		failedCtr.put(execId, new AtomicLong(value));
	}
	
	public void setCompletedCtr(String execId, Long value ){
		completedCtr.put(execId, new AtomicLong(value));
	}

	public void setAlreadyArchivedCtr(String execId, Long value) {
		alreadyArchivedCtr.put(execId, new AtomicLong(value));
	}
	
	

	public String getEvaluatedCtr(String execId ){
		return evaluatedCtr.get(execId);
	}
	
	public String getArchivedCtr(String execId){
		return archivedCtr.get(execId);
	}
	
	public String getStubbedCtr(String execId){
		return stubbedCtr.get(execId);
	}
	
	public String getExportedCtr(String execId){
		return exportedCtr.get(execId);
	}
	
	public String getFailedCtr(String execId){
		return failedCtr.get(execId);
	}
	
	public String getCompletedCtr(String execId){
		return completedCtr.get(execId);
	}

	public String getAlreadyArchivedCtr(String execID) {
		return alreadyArchivedCtr.get(execID);
	}
}
