package com.virtualcode.vo;

import java.sql.Timestamp;

/**
 * SystemCode entity. @author MyEclipse Persistence Tools
 */

public class SystemCode implements java.io.Serializable {

	// Fields

	private Integer id;
	private String codename;
	private String codetype;
	private String codevalue;
	private Integer editable;
	private Timestamp etd;
	private Integer viewable;
	
	private String displayName;

	// Constructors

	/** default constructor */
	public SystemCode() {
	}

	/** full constructor */
	public SystemCode(String codename, String codetype, String codevalue,
			Integer editable, Timestamp etd, Integer viewable) {
		this.codename = codename;
		this.codetype = codetype;
		this.codevalue = codevalue;
		this.editable = editable;
		this.etd = etd;
		this.viewable = viewable;
	}

	  public SystemCode(int id, String codename, String codevalue, String codetype,String displayName){
	        this.id=id;
	        this.codename=codename;
	        this.codevalue=codevalue;
	        this.codetype=codetype;
	        this.displayName=displayName;
	    }

	
	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodename() {
		return this.codename;
	}

	public void setCodename(String codename) {
		this.codename = codename;
	}

	public String getCodetype() {
		return this.codetype;
	}

	public void setCodetype(String codetype) {
		this.codetype = codetype;
	}

	public String getCodevalue() {
		return this.codevalue;
	}

	public void setCodevalue(String codevalue) {
		this.codevalue = codevalue;
	}

	public Integer getEditable() {
		return this.editable;
	}

	public void setEditable(Integer editable) {
		this.editable = editable;
	}

	public Timestamp getEtd() {
		return this.etd;
	}

	public void setEtd(Timestamp etd) {
		this.etd = etd;
	}

	public Integer getViewable() {
		return this.viewable;
	}

	public void setViewable(Integer viewable) {
		this.viewable = viewable;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	
	
	
}