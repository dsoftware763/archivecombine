package com.virtualcode.vo;

import java.sql.Timestamp;

/**
 * LinkAccessDocuments entity. @author MyEclipse Persistence Tools
 */

public class LinkAccessDocuments implements java.io.Serializable {

	// Fields

	private Integer id;
	private LinkAccessUsers linkAccessUsers;
	private String uuidDocuments;
	private Timestamp sharedOn;
	private String sender;
	private String hash;
	private Long identifier;
	private Timestamp linkExpireDate;

	// Constructors

	/** default constructor */
	public LinkAccessDocuments() {
	}

	/** full constructor */
	public LinkAccessDocuments(LinkAccessUsers linkAccessUsers,
			String uuidDocuments) {
		this.linkAccessUsers = linkAccessUsers;
		this.uuidDocuments = uuidDocuments;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LinkAccessUsers getLinkAccessUsers() {
		return this.linkAccessUsers;
	}

	public void setLinkAccessUsers(LinkAccessUsers linkAccessUsers) {
		this.linkAccessUsers = linkAccessUsers;
	}

	public String getUuidDocuments() {
		return this.uuidDocuments;
	}

	public void setUuidDocuments(String uuidDocuments) {
		this.uuidDocuments = uuidDocuments;
	}

	public Timestamp getSharedOn() {
		return sharedOn;
	}

	public void setSharedOn(Timestamp sharedOn) {
		this.sharedOn = sharedOn;
	}
	
	public String getSender() {
		return this.sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public Long getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Long identifier) {
		this.identifier = identifier;
	}

	public Timestamp getLinkExpireDate() {
		return linkExpireDate;
	}

	public void setLinkExpireDate(Timestamp linkExpireDate) {
		this.linkExpireDate = linkExpireDate;
	}

}