/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.virtualcode.vo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.virtualcode.util.VCSUtil;

/**
 *
 * @author AtiqurRehman
 */
@XmlRootElement(name="node")
public class SearchDocument {

    private String documentName;
    private String documentPath;
    private String authorName;
    private String modifiedBy;
    private String lastModificationDate;
    private String lastAccessDate;

    private String documentSize;

    private String mimeType;

    private String documentUrl;

    private String excerptString;
    
    private String tags;
    
    private String tagUser;
    
    private String tagTimeStamp;
    
    //added for report only
    private String uid;
    private String archivedDate;
    private String createdDate;    

    private String documentSizeToDisplay;
    
    private boolean mediaFile=false;
    
    private boolean viewable = false;
    
    private String repoPath;
    
    private boolean image = false;
    
	private List<String> mediaFiles=new ArrayList<String>(Arrays.asList("mp4", "m4v", "f4v", "mov", "aac", "m4a", "f4a","mp3","flv","ogg","oga","flv"));
	
	private List<String> imageFiles=new ArrayList<String>(Arrays.asList(".jpeg", ".bmp", ".gif", ".png", ".jpg"));
	
	private boolean archive = true;
	
	private String securityAllowSids;
	
	private String shareAllowSids;
	
	private String securityDenySids;
	
	private String shareDenySids;
	

	//;
	
	public void setMediaFile(boolean mediaFile) {
		this.mediaFile = mediaFile;
	}

	public boolean isMediaFile() {
		
		String fileExt=documentPath.substring(documentPath.lastIndexOf("/")+1);
		fileExt=fileExt.substring(fileExt.lastIndexOf(".")+1);
		if(mediaFiles.contains(fileExt.toLowerCase())){
			return true;
		}else{
			return false;
		}			
		
	}
    /**
     * @return the documentName
     */
    @XmlElement(name="documentName")
    public String getDocumentName() {
        return documentName;
    }

    /**
     * @param documentName the documentName to set
     */
    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    /**
     * @return the documentPath
     */
    @XmlElement(name="documentPath")
    public String getDocumentPath() {
        return documentPath;
    }

    /**
     * @param documentPath the documentPath to set
     */
    public void setDocumentPath(String documentPath) {
        this.documentPath = documentPath;
    }

    /**
     * @return the authorName
     */
    @XmlElement(name="authorName")
    public String getAuthorName() {
        return authorName;
    }

    /**
     * @param authorName the authorName to set
     */
    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    /**
     * @return the modifiedBy
     */
    @XmlElement(name="modifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    /**
     * @param modifiedBy the modifiedBy to set
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the lastModifiedDate
     */
    @XmlElement(name="lastModificationDate")
    public String getLastModificationDate() {
        return lastModificationDate;
    }

    /**
     * @param lastModifiedDate the lastModifiedDate to set
     */
    public void setLastModificationDate(String lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }
    

    @XmlElement(name="lastAccessDate")
    public String getLastAccessDate() {
		return lastAccessDate;
	}

	public void setLastAccessDate(String lastAccessDate) {
		this.lastAccessDate = lastAccessDate;
	}

	/**
     * @return the documentSize
     */
    @XmlElement(name="documentSize")
    public String getDocumentSize() {
        return documentSize;
    }
    
    /**
     * @param documentSize the documentSize to set
     */
    public void setDocumentSize(String documentSize) {
        this.documentSize = documentSize;
    }

    /**
     * @return the mimeType
     */
    @XmlElement(name="mimeType")
    public String getMimeType() {
        return mimeType;
    }

    /**
     * @param mimeType the mimeType to set
     */
    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    /**
     * @return the documentUrl
     */
    @XmlElement(name="documentUrl")
    public String getDocumentUrl() {
        return documentUrl;
    }

    /**
     * @param documentUrl the documentUrl to set
     */
    public void setDocumentUrl(String documentUrl) {
        this.documentUrl = documentUrl;
    }

    /**
     * @return the excerptString
     */

    @XmlElement(name="excerptString")
    public String getExcerptString() {
        return excerptString;
    }

    /**
     * @param excerptString the excerptString to set
     */
    public void setExcerptString(String excerptString) {
        this.excerptString = excerptString;
    }

    @XmlElement(name="uid")
	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}
	
	@XmlElement(name="archivedDate")
	public String getArchivedDate() {
		return archivedDate;
	}
	
	public void setArchivedDate(String archivedDate) {
		this.archivedDate = archivedDate;
	}

	@XmlElement(name="createdDate")
	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	@XmlElement(name="tags")
	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getTagUser() {
		return tagUser;
	}

	public void setTagUser(String tagUser) {
		this.tagUser = tagUser;
	}

	public String getTagTimeStamp() {
		return tagTimeStamp;
	}

	public void setTagTimeStamp(String tagTimeStamp) {
		this.tagTimeStamp = tagTimeStamp;
	}

	public String getDocumentSizeToDisplay() {
		return documentSizeToDisplay;
	}

	public void setDocumentSizeToDisplay(String documentSizeToDisplay) {
		this.documentSizeToDisplay = documentSizeToDisplay;
	}

	public boolean isViewable() {		
		return VCSUtil.isPdfViewable(documentName);
	}

	public void setViewable(boolean viewable) {
		this.viewable = viewable;
	}

	public String getRepoPath() {
		return repoPath;
	}

	public void setRepoPath(String repoPath) {
		this.repoPath = repoPath;
	}

	public boolean isImage() {
		String fileExt=documentPath.substring(documentPath.lastIndexOf("/")+1);
		if(fileExt.indexOf(".")==-1)
			return false;
		fileExt=fileExt.substring(fileExt.lastIndexOf("."));
		if(imageFiles.contains(fileExt.toLowerCase())){
			return true;
		}else{
			return false;
		}		
	}

	public void setImage(boolean image) {
		this.image = image;
	}

	public boolean isArchive() {
		return archive;
	}

	public void setArchive(boolean archive) {
		this.archive = archive;
	}

	public String getSecurityAllowSids() {
		return securityAllowSids;
	}

	public void setSecurityAllowSids(String securityAllowSids) {
		this.securityAllowSids = securityAllowSids;
	}

	public String getShareAllowSids() {
		return shareAllowSids;
	}

	public void setShareAllowSids(String shareAllowSids) {
		this.shareAllowSids = shareAllowSids;
	}

	public String getSecurityDenySids() {
		return securityDenySids;
	}

	public void setSecurityDenySids(String securityDenySids) {
		this.securityDenySids = securityDenySids;
	}

	public String getShareDenySids() {
		return shareDenySids;
	}

	public void setShareDenySids(String shareDenySids) {
		this.shareDenySids = shareDenySids;
	}
  
}
