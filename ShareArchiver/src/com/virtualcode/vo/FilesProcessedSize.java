package com.virtualcode.vo;

/**
 * FilesProcessedSize entity. @author MyEclipse Persistence Tools
 */

public class FilesProcessedSize implements java.io.Serializable {

	// Fields

	private Integer id;
	private Long totalProcessedFiles = 0l;

	// Constructors

	/** default constructor */
	public FilesProcessedSize() {
	}

	/** full constructor */
	public FilesProcessedSize(Long totalProcessedFiles) {
		this.totalProcessedFiles = totalProcessedFiles;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getTotalProcessedFiles() {
		return this.totalProcessedFiles;
	}

	public void setTotalProcessedFiles(Long totalProcessedFiles) {
		this.totalProcessedFiles = totalProcessedFiles;
	}

}