package com.virtualcode.vo;

import java.sql.Timestamp;

/**
 * SystemAlerts entity. @author MyEclipse Persistence Tools
 */

public class SystemAlerts implements java.io.Serializable {

	// Fields

	private Integer id;
	private String username;
	private Timestamp alertTime;
	private String alertType;
	private String alertMessage;

	// Constructors

	/** default constructor */
	public SystemAlerts() {
	}

	/** minimal constructor */
	public SystemAlerts(Timestamp alertTime) {
		this.alertTime = alertTime;
	}

	/** full constructor */
	public SystemAlerts(String username, Timestamp alertTime, String alertType,
			String alertMessage) {
		this.username = username;
		this.alertTime = alertTime;
		this.alertType = alertType;
		this.alertMessage = alertMessage;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Timestamp getAlertTime() {
		return this.alertTime;
	}

	public void setAlertTime(Timestamp alertTime) {
		this.alertTime = alertTime;
	}

	public String getAlertType() {
		return this.alertType;
	}

	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}

	public String getAlertMessage() {
		return this.alertMessage;
	}

	public void setAlertMessage(String alertMessage) {
		this.alertMessage = alertMessage;
	}

}