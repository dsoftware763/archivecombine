package com.virtualcode.vo;

import java.util.HashSet;
import java.util.Set;

/**
 * LinkAccessUsers entity. @author MyEclipse Persistence Tools
 */

public class LinkAccessUsers implements java.io.Serializable {

	// Fields

	private Integer id;
	private String recipient;
	private String password;
	private String sender;
	private Set linkAccessDocumentses = new HashSet(0);

	// Constructors

	/** default constructor */
	public LinkAccessUsers() {
	}

	/** minimal constructor */
	public LinkAccessUsers(String recipient, String password) {
		this.recipient = recipient;
		this.password = password;
	}

	/** full constructor */
	public LinkAccessUsers(String recipient, String password, String sender,
			Set linkAccessDocumentses) {
		this.recipient = recipient;
		this.password = password;
		this.sender = sender;
		this.linkAccessDocumentses = linkAccessDocumentses;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRecipient() {
		return this.recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSender() {
		return this.sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public Set getLinkAccessDocumentses() {
		return this.linkAccessDocumentses;
	}

	public void setLinkAccessDocumentses(Set linkAccessDocumentses) {
		this.linkAccessDocumentses = linkAccessDocumentses;
	}

}