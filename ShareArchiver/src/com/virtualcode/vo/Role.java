package com.virtualcode.vo;

import java.util.HashSet;
import java.util.Set;


/**
 * Role entity. @author MyEclipse Persistence Tools
 */

public class Role  implements java.io.Serializable {


    // Fields    

     private Long id;
     private String name;
     private Set users = new HashSet(0);
     private Set shiroRolePermissionses = new HashSet(0);


    // Constructors

    /** default constructor */
    public Role() {
    }

	/** minimal constructor */
    public Role(String name) {
        this.name = name;
    }
    
    /** full constructor */
    public Role(String name, Set users, Set shiroRolePermissionses) {
        this.name = name;
        this.users = users;
        this.shiroRolePermissionses = shiroRolePermissionses;
    }

   
    // Property accessors

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public Set getUsers() {
        return this.users;
    }
    
    public void setUsers(Set users) {
        this.users = users;
    }

    public Set getShiroRolePermissionses() {
        return this.shiroRolePermissionses;
    }
    
    public void setShiroRolePermissionses(Set shiroRolePermissionses) {
        this.shiroRolePermissionses = shiroRolePermissionses;
    }
   


}