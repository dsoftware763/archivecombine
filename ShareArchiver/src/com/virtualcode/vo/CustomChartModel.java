package com.virtualcode.vo;

import org.primefaces.model.chart.CartesianChartModel;

public class CustomChartModel extends CartesianChartModel {

	public CustomChartModel(){
		super();
		
	}
	
	private String name;
	
	private String type;//category/volume
	
	private String sharePath;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSharePath() {
		return sharePath;
	}

	public void setSharePath(String sharePath) {
		this.sharePath = sharePath;
	}
	
	
	
	
	
}
