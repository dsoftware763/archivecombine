package com.virtualcode.vo;

public class ExportJobPath {
	
	private boolean recursive;
	private String source;
	private String path;
	private String destinationPath;
	private String folderSize;
	private String folderStubsSize;
	//for SP only
	private String guid;
	private String sitePath;
	
	public boolean isRecursive() {
		return recursive;
	}
	public void setRecursive(boolean recursive) {
		this.recursive = recursive;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getDestinationPath() {
		return destinationPath;
	}
	public void setDestinationPath(String destinationPath) {
		this.destinationPath = destinationPath;
	}
	public String getFolderSize() {
		return folderSize;
	}
	public void setFolderSize(String folderSize) {
		this.folderSize = folderSize;
	}
	public String getFolderStubsSize() {
		return folderStubsSize;
	}
	public void setFolderStubsSize(String folderStubsSize) {
		this.folderStubsSize = folderStubsSize;
	}
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public String getSitePath() {
		return sitePath;
	}
	public void setSitePath(String sitePath) {
		this.sitePath = sitePath;
	}
	
	
}
