package com.virtualcode.vo;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Policy entity. @author MyEclipse Persistence Tools
 */
@XmlRootElement(name="policy")
public class Policy implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer active;
	private String description;
	private String documentAge;
	private String lastAccessedDate;
	private String lastModifiedDate;
	private String name;
	private String sizeLargerThan;
	private Set policyDocumentTypes = new HashSet(0);
	private Set jobs = new HashSet(0);
	
	private Integer docAgeYears;
	private Integer docAgeMonths;
	
	private Integer lastAccessedYears;
	private Integer lastAccessedMonths;
	
	private Integer lastModifiedYears;
	private Integer lastModifiedMonths;

	private String sizeUnits;
	private Integer sizeGreaterThan;
	
	private String documentTypesStr;
	// Constructors

	/** default constructor */
	public Policy() {
	}

	/** minimal constructor */
	public Policy(Integer active, String documentAge, String lastAccessedDate,
			String lastModifiedDate, String name, String sizeLargerThan) {
		this.active = active;
		this.documentAge = documentAge;
		this.lastAccessedDate = lastAccessedDate;
		this.lastModifiedDate = lastModifiedDate;
		this.name = name;
		this.sizeLargerThan = sizeLargerThan;
	}

	/** full constructor */
	public Policy(Integer active, String description, String documentAge,
			String lastAccessedDate, String lastModifiedDate, String name,
			String sizeLargerThan, Set policyDocumentTypes, Set jobs) {
		this.active = active;
		this.description = description;
		this.documentAge = documentAge;
		this.lastAccessedDate = lastAccessedDate;
		this.lastModifiedDate = lastModifiedDate;
		this.name = name;
		this.sizeLargerThan = sizeLargerThan;
		this.policyDocumentTypes = policyDocumentTypes;
		this.jobs = jobs;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getActive() {
		return this.active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDocumentAge() {
		return this.documentAge;
	}

	public void setDocumentAge(String documentAge) {
		this.documentAge = documentAge;
	}

	public String getLastAccessedDate() {
		return this.lastAccessedDate;
	}

	public void setLastAccessedDate(String lastAccessedDate) {
		this.lastAccessedDate = lastAccessedDate;
	}

	public String getLastModifiedDate() {
		return this.lastModifiedDate;
	}

	public void setLastModifiedDate(String lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSizeLargerThan() {
		return this.sizeLargerThan;
	}

	public void setSizeLargerThan(String sizeLargerThan) {
		this.sizeLargerThan = sizeLargerThan;
	}

	public Set getPolicyDocumentTypes() {
		return this.policyDocumentTypes;
	}

	public void setPolicyDocumentTypes(Set policyDocumentTypes) {
		this.policyDocumentTypes = policyDocumentTypes;
	}

	public Set getJobs() {
		return this.jobs;
	}

	public void setJobs(Set jobs) {
		this.jobs = jobs;
	}

	public Integer getDocAgeYears() {
		return docAgeYears;
	}

	public void setDocAgeYears(Integer docAgeYears) {
		this.docAgeYears = docAgeYears;
	}

	public Integer getDocAgeMonths() {
		return docAgeMonths;
	}

	public void setDocAgeMonths(Integer docAgeMonths) {
		this.docAgeMonths = docAgeMonths;
	}

	public Integer getLastAccessedYears() {
		return lastAccessedYears;
	}

	public void setLastAccessedYears(Integer lastAccessedYears) {
		this.lastAccessedYears = lastAccessedYears;
	}

	public Integer getLastAccessedMonths() {
		return lastAccessedMonths;
	}

	public void setLastAccessedMonths(Integer lastAccessedMonths) {
		this.lastAccessedMonths = lastAccessedMonths;
	}

	public Integer getLastModifiedYears() {
		return lastModifiedYears;
	}

	public void setLastModifiedYears(Integer lastModifiedYears) {
		this.lastModifiedYears = lastModifiedYears;
	}

	public Integer getLastModifiedMonths() {
		return lastModifiedMonths;
	}

	public void setLastModifiedMonths(Integer lastModifiedMonths) {
		this.lastModifiedMonths = lastModifiedMonths;
	}

	public String getSizeUnits() {
		return sizeUnits;
	}

	public void setSizeUnits(String sizeUnits) {
		this.sizeUnits = sizeUnits;
	}

	public Integer getSizeGreaterThan() {
		return sizeGreaterThan;
	}

	public void setSizeGreaterThan(Integer sizeGreaterThan) {
		this.sizeGreaterThan = sizeGreaterThan;
	}

	public String getDocumentTypesStr() {
		return documentTypesStr;
	}

	public void setDocumentTypesStr(String documentTypesStr) {
		this.documentTypesStr = documentTypesStr;
	}

	

}