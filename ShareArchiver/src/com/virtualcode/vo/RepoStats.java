package com.virtualcode.vo;

/**
 * RepoStats entity. @author MyEclipse Persistence Tools
 */

public class RepoStats implements java.io.Serializable {

	// Fields

	private Integer id;
	private String repoPath;
	private Long totalFolders;
	private Long uniqueFiles;
	private Long dupFiles;
	private Long fsDocs;
	private Long spDocs;
	private Double usedSpace;
	private Double duplicateDataSize;

	// Constructors

	/** default constructor */
	public RepoStats() {
	}

	/** full constructor */
	public RepoStats(String repoPath, Long totalFolders, Long uniqueFiles, Long dupFiles,
			Long fsDocs, Long spDocs, Double usedSpace,
			Double duplicateDataSize) {
		this.repoPath = repoPath;
		this.totalFolders = totalFolders;
		this.uniqueFiles = uniqueFiles;
		this.dupFiles	=	dupFiles;
		this.fsDocs = fsDocs;
		this.spDocs = spDocs;
		this.usedSpace = usedSpace;
		this.duplicateDataSize = duplicateDataSize;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRepoPath() {
		return this.repoPath;
	}

	public void setRepoPath(String repoPath) {
		this.repoPath = repoPath;
	}

	public Long getTotalFolders() {
		return this.totalFolders;
	}

	public void setTotalFolders(Long totalFolders) {
		this.totalFolders = totalFolders;
	}

	public Long getUniqueFiles() {
		return this.uniqueFiles;
	}

	public void setUniqueFiles(Long uniqueFiles) {
		this.uniqueFiles = uniqueFiles;
	}

	public Long getDupFiles() {
		return dupFiles;
	}

	public void setDupFiles(Long dupFiles) {
		this.dupFiles = dupFiles;
	}

	public Long getFsDocs() {
		return this.fsDocs;
	}

	public void setFsDocs(Long fsDocs) {
		this.fsDocs = fsDocs;
	}

	public Long getSpDocs() {
		return this.spDocs;
	}

	public void setSpDocs(Long spDocs) {
		this.spDocs = spDocs;
	}

	public Double getUsedSpace() {
		return this.usedSpace;
	}

	public void setUsedSpace(Double usedSpace) {
		this.usedSpace = usedSpace;
	}

	public Double getDuplicateDataSize() {
		return this.duplicateDataSize;
	}

	public void setDuplicateDataSize(Double duplicateDataSize) {
		this.duplicateDataSize = duplicateDataSize;
	}

}