package com.virtualcode.vo;



/**
 * UserPermissions entity. @author MyEclipse Persistence Tools
 */

public class UserPermissions  implements java.io.Serializable {


    // Fields    

     private UserPermissionsId id;
     private User user;


    // Constructors

    /** default constructor */
    public UserPermissions() {
    }

	/** minimal constructor */
    public UserPermissions(UserPermissionsId id) {
        this.id = id;
    }
    
    /** full constructor */
    public UserPermissions(UserPermissionsId id, User user) {
        this.id = id;
        this.user = user;
    }

   
    // Property accessors

    public UserPermissionsId getId() {
        return this.id;
    }
    
    public void setId(UserPermissionsId id) {
        this.id = id;
    }

    public User getUser() {
        return this.user;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
   








}