package com.virtualcode.vo;



/**
 * RolePermissions entity. @author MyEclipse Persistence Tools
 */

public class RolePermissions  implements java.io.Serializable {


    // Fields    

     private RolePermissionsId id;
     private Role shiroRole;


    // Constructors

    /** default constructor */
    public RolePermissions() {
    }

	/** minimal constructor */
    public RolePermissions(RolePermissionsId id) {
        this.id = id;
    }
    
    /** full constructor */
    public RolePermissions(RolePermissionsId id, Role shiroRole) {
        this.id = id;
        this.shiroRole = shiroRole;
    }

   
    // Property accessors

    public RolePermissionsId getId() {
        return this.id;
    }
    
    public void setId(RolePermissionsId id) {
        this.id = id;
    }

    public Role getShiroRole() {
        return this.shiroRole;
    }
    
    public void setShiroRole(Role shiroRole) {
        this.shiroRole = shiroRole;
    }
   








}