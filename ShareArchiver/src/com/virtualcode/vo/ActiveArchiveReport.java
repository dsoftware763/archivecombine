package com.virtualcode.vo;

import java.sql.Timestamp;

/**
 * ActiveArchiveReport entity. @author MyEclipse Persistence Tools
 */

public class ActiveArchiveReport implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer dailyCount;
	private Long totalCount;
	private Timestamp reportDate;
	private Integer totalJobs;

	// Constructors

	/** default constructor */
	public ActiveArchiveReport() {
	}

	/** minimal constructor */
	public ActiveArchiveReport(Timestamp reportDate) {
		this.reportDate = reportDate;
	}

	/** full constructor */
	public ActiveArchiveReport(Integer dailyCount, Long totalCount,
			Timestamp reportDate, Integer totalJobs) {
		this.dailyCount = dailyCount;
		this.totalCount = totalCount;
		this.reportDate = reportDate;
		this.totalJobs = totalJobs;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDailyCount() {
		return this.dailyCount;
	}

	public void setDailyCount(Integer dailyCount) {
		this.dailyCount = dailyCount;
	}

	public Long getTotalCount() {
		return this.totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}

	public Timestamp getReportDate() {
		return this.reportDate;
	}

	public void setReportDate(Timestamp reportDate) {
		this.reportDate = reportDate;
	}

	public Integer getTotalJobs() {
		return this.totalJobs;
	}

	public void setTotalJobs(Integer totalJobs) {
		this.totalJobs = totalJobs;
	}

}