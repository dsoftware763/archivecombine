package com.virtualcode.vo;

/**
 * DriveLetters entity. @author MyEclipse Persistence Tools
 */

public class DriveLetters implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer spDocLibId;
	private String driveLetter;
	private String sharePath;
	private Boolean status;
	
	//added as per requirement 
	private String restoreUsername;
	private String restorePassword;
	private String restoreDomain;
	
	private int  shareUsedPercentage=0;
	
	private int  shareFreePercentage=0;
	
	private int warningLimit=50;
	
	private int criticalLimit=90;
	
	private boolean analyzeShare = false;
	
	private int warningColour=0;//0=no-warning, 1=warning,2=critical;
	
	private Float freeSpace=0f;
	
	private Float totalSpace=0f;
	
	private boolean triggerJobOnThreshhold=false;
	
	private Integer policyId;
	
	private boolean threshholdJob=false;
	
	private Boolean isFileShare = false;
	// Constructors

	/** default constructor */
	public DriveLetters() {
	}

	/** full constructor */
	public DriveLetters(String driveLetter, String sharePath, Boolean status) {
		this.driveLetter = driveLetter;
		this.sharePath = sharePath;
		this.status = status;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSpDocLibId() {
		return spDocLibId;
	}

	public void setSpDocLibId(Integer spDocLibId) {
		this.spDocLibId = spDocLibId;
	}

	public String getDriveLetter() {
		return this.driveLetter;
	}

	public void setDriveLetter(String driveLetter) {
		this.driveLetter = driveLetter;
	}

	public String getSharePath() {
		return this.sharePath;
	}

	public void setSharePath(String sharePath) {
		this.sharePath = sharePath;
	}

	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getRestoreUsername() {
		return restoreUsername;
	}

	public void setRestoreUsername(String restoreUsername) {
		this.restoreUsername = restoreUsername;
	}

	public String getRestorePassword() {
		return restorePassword;
	}

	public void setRestorePassword(String restorePassword) {
		this.restorePassword = restorePassword;
	}

	public String getRestoreDomain() {
		return restoreDomain;
	}

	public void setRestoreDomain(String restoreDomain) {
		this.restoreDomain = restoreDomain;
	}

	public int getShareUsedPercentage() {
		return shareUsedPercentage;
	}

	public void setShareUsedPercentage(int shareUsedPercentage) {
		this.shareUsedPercentage = shareUsedPercentage;
	}

	public int getShareFreePercentage() {
		return shareFreePercentage;
	}

	public void setShareFreePercentage(int shareFreePercentage) {
		this.shareFreePercentage = shareFreePercentage;
	}

	public int getWarningLimit() {
		return warningLimit;
	}

	public void setWarningLimit(int warningLimit) {
		this.warningLimit = warningLimit;
	}

	public int getCriticalLimit() {
		return criticalLimit;
	}

	public void setCriticalLimit(int criticalLimit) {
		this.criticalLimit = criticalLimit;
	}
	
	public boolean isAnalyzeShare() {
		return analyzeShare;
	}

	public void setAnalyzeShare(boolean analyzeShare) {
		this.analyzeShare = analyzeShare;
	}

	public int getWarningColour() {
		return warningColour;
	}

	public void setWarningColour(int warningColour) {
		this.warningColour = warningColour;
	}

	public Float getFreeSpace() {
		return freeSpace;
	}

	public void setFreeSpace(Float freeSpace) {
		this.freeSpace = freeSpace;
	}

	public Float getTotalSpace() {
		return totalSpace;
	}

	public void setTotalSpace(Float totalSpace) {
		this.totalSpace = totalSpace;
	}

	public boolean isTriggerJobOnThreshhold() {
		return triggerJobOnThreshhold;
	}

	public void setTriggerJobOnThreshhold(boolean triggerJobOnThreshhold) {
		this.triggerJobOnThreshhold = triggerJobOnThreshhold;
	}

	public Integer getPolicyId() {
		return policyId;
	}

	public void setPolicyId(Integer policyId) {
		this.policyId = policyId;
	}

	public Boolean getIsFileShare() {
		return isFileShare;
	}

	public void setIsFileShare(Boolean isFileShare) {
		this.isFileShare = isFileShare;
	}

	
	

}