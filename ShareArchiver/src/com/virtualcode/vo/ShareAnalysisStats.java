package com.virtualcode.vo;

/**
 * ShareAnalysisStats entity. @author MyEclipse Persistence Tools
 */

public class ShareAnalysisStats implements java.io.Serializable {

	// Fields

	private Long id=new Long(0);
	private DriveLetters driveLetters;
	private String documentCatName;
	private Long totalVolume=new Long(0);
	private Long lm3=new Long(0);
	private Long lm6=new Long(0);
	private Long lm12=new Long(0);
	private Long lm24=new Long(0);
	private Long lm36=new Long(0);
	
	private Long la3=new Long(0);
	private Long la6=new Long(0);
	private Long la12=new Long(0);
	private Long la24=new Long(0);
	private Long la36=new Long(0);
	
	private Long cd3=new Long(0);
	private Long cd6=new Long(0);
	private Long cd12=new Long(0);
	private Long cd24=new Long(0);
	private Long cd36=new Long(0);

	// Constructors

	/** default constructor */
	public ShareAnalysisStats() {
	}

	/** minimal constructor */
	public ShareAnalysisStats(DriveLetters driveLetters, String documentCatName) {
		this.driveLetters = driveLetters;
		this.documentCatName = documentCatName;
	}

	/** full constructor */
	public ShareAnalysisStats(DriveLetters driveLetters,
			String documentCatName, Long totalVolume, Long lm3,	Long lm6, Long lm12, Long lm24,Long lm36,
			Long la3,	Long la6, Long la12, Long la24,Long la36,Long cd3,	Long cd6, Long cd12, Long cd24,Long cd36) {
			this.driveLetters = driveLetters;
			this.documentCatName = documentCatName;
			this.totalVolume = totalVolume;
			this.lm3 = lm3;	this.lm6 = lm6;	this.lm12 = lm12; this.lm24 = lm24;	this.lm36 = lm36;		
			this.la3=la3;this.la6=la6;this.la12=la12;this.la24=la24;this.la36=la36;
			this.cd3=cd3;this.cd6=cd6;this.cd12=cd12;this.cd24=cd24;this.cd36=cd36;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DriveLetters getDriveLetters() {
		return this.driveLetters;
	}

	public void setDriveLetters(DriveLetters driveLetters) {
		this.driveLetters = driveLetters;
	}

	public String getDocumentCatName() {
		return this.documentCatName;
	}

	public void setDocumentCatName(String documentCatName) {
		this.documentCatName = documentCatName;
	}

	public Long getTotalVolume() {
		return this.totalVolume;
	}

	public void setTotalVolume(Long totalVolume) {
		this.totalVolume = totalVolume;
	}

	public Long getLm3() {
		return lm3;
	}

	public void setLm3(Long lm3) {
		this.lm3 = lm3;
	}

	public Long getLm6() {
		return lm6;
	}

	public void setLm6(Long lm6) {
		this.lm6 = lm6;
	}

	public Long getLm12() {
		return lm12;
	}

	public void setLm12(Long lm12) {
		this.lm12 = lm12;
	}

	public Long getLm24() {
		return lm24;
	}

	public void setLm24(Long lm24) {
		this.lm24 = lm24;
	}

	public Long getLm36() {
		return lm36;
	}

	public void setLm36(Long lm36) {
		this.lm36 = lm36;
	}

	public Long getLa3() {
		return la3;
	}

	public void setLa3(Long la3) {
		this.la3 = la3;
	}

	public Long getLa6() {
		return la6;
	}

	public void setLa6(Long la6) {
		this.la6 = la6;
	}

	public Long getLa12() {
		return la12;
	}

	public void setLa12(Long la12) {
		this.la12 = la12;
	}

	public Long getLa24() {
		return la24;
	}

	public void setLa24(Long la24) {
		this.la24 = la24;
	}

	public Long getLa36() {
		return la36;
	}

	public void setLa36(Long la36) {
		this.la36 = la36;
	}

	public Long getCd3() {
		return cd3;
	}

	public void setCd3(Long cd3) {
		this.cd3 = cd3;
	}

	public Long getCd6() {
		return cd6;
	}

	public void setCd6(Long cd6) {
		this.cd6 = cd6;
	}

	public Long getCd12() {
		return cd12;
	}

	public void setCd12(Long cd12) {
		this.cd12 = cd12;
	}

	public Long getCd24() {
		return cd24;
	}

	public void setCd24(Long cd24) {
		this.cd24 = cd24;
	}

	public Long getCd36() {
		return cd36;
	}

	public void setCd36(Long cd36) {
		this.cd36 = cd36;
	}

	

}