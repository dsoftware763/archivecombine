package com.virtualcode.vo;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Timestamp;

import javax.servlet.ServletException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;

/**
 * JobStatistics entity. @author MyEclipse Persistence Tools
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="jobStatistics")
public class JobStatistics implements java.io.Serializable {

	private Integer id;
	private int numberOfPublicLogs	=	0;
	
	private Job job;
	
	private String executionId;
	
	
	private Timestamp jobEndTime;
	
	
	private Timestamp jobStartTime;
	
	private Long skippedProcessfileSkippedHiddenlected=0l;
	private Long skippedProcessfileSkippedIsdir=0l;
	private Long skippedProcessfileSkippedMismatchAge=0l;
	private Long skippedProcessfileSkippedMismatchExtension=0l;
	private Long skippedProcessfileSkippedMismatchLastaccesstime=0l;
	private Long skippedProcessfileSkippedMismatchLastmodifiedtime=0l;
	private Long skippedProcessfileSkippedReadOnly=0l;
	private Long skippedProcessfileSkippedSymbollink=0l;
	private Long skippedProcessfileSkippedTemporary=0l;
	private Long skippedProcessfileSkippedTooLarge=0l;
	private Long archivedVolume=0l; 	//renamed from totalFilesSize to archivedVolume
	private Long totalFreshArchived=0l;
	private Long alreadyArchived=0l;
	private Long totalDuplicated=0l;
	private Long totalEvaluated=0l;
	private Long totalFailedArchiving=0l;
	private Long totalFailedDeduplication=0l;
	private Long totalFailedEvaluating=0l;
	private Long totalFailedStubbing=0l;
	private Long totalStubbed=0l;
	
	private String status;	//only for view not for DB
	private String serialNo;	//only for view not for DB
	private long totalSkipped=0l; //only for view not for DB
	private long totalFailed=0l;  //only for view not for DB

	private int throughput=0;
	// Constructors

	/** default constructor */
	public JobStatistics() {
	}

	/** full constructor */
	public JobStatistics(Job job, String executionId, Timestamp jobEndTime,
			Timestamp jobStartTime, Long skippedProcessfileSkippedHiddenlected,
			Long skippedProcessfileSkippedIsdir,
			Long skippedProcessfileSkippedMismatchAge,
			Long skippedProcessfileSkippedMismatchExtension,
			Long skippedProcessfileSkippedMismatchLastaccesstime,
			Long skippedProcessfileSkippedMismatchLastmodifiedtime,
			Long skippedProcessfileSkippedReadOnly,
			Long skippedProcessfileSkippedSymbollink,
			Long skippedProcessfileSkippedTemporary,
			Long skippedProcessfileSkippedTooLarge, Long totalFreshArchived,
			Long alreadyArchived,
			Long totalDuplicated, Long totalEvaluated,
			Long totalFailedArchiving, Long totalFailedDeduplication,
			Long totalFailedEvaluating, Long totalFailedStubbing,
			Long totalStubbed, Long totalFilesSize, Long archivedVolume) {
		this.job = job;
		this.executionId = executionId;
		this.jobEndTime = jobEndTime;
		this.jobStartTime = jobStartTime;
		this.skippedProcessfileSkippedHiddenlected = skippedProcessfileSkippedHiddenlected;
		this.skippedProcessfileSkippedIsdir = skippedProcessfileSkippedIsdir;
		this.skippedProcessfileSkippedMismatchAge = skippedProcessfileSkippedMismatchAge;
		this.skippedProcessfileSkippedMismatchExtension = skippedProcessfileSkippedMismatchExtension;
		this.skippedProcessfileSkippedMismatchLastaccesstime = skippedProcessfileSkippedMismatchLastaccesstime;
		this.skippedProcessfileSkippedMismatchLastmodifiedtime = skippedProcessfileSkippedMismatchLastmodifiedtime;
		this.skippedProcessfileSkippedReadOnly = skippedProcessfileSkippedReadOnly;
		this.skippedProcessfileSkippedSymbollink = skippedProcessfileSkippedSymbollink;
		this.skippedProcessfileSkippedTemporary = skippedProcessfileSkippedTemporary;
		this.skippedProcessfileSkippedTooLarge = skippedProcessfileSkippedTooLarge;
		this.totalFreshArchived = totalFreshArchived;
		this.alreadyArchived	=	alreadyArchived;
		this.totalDuplicated = totalDuplicated;
		this.totalEvaluated = totalEvaluated;
		this.totalFailedArchiving = totalFailedArchiving;
		this.totalFailedDeduplication = totalFailedDeduplication;
		this.totalFailedEvaluating = totalFailedEvaluating;
		this.totalFailedStubbing = totalFailedStubbing;
		this.totalStubbed = totalStubbed;
		this.archivedVolume=archivedVolume;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@XmlTransient
	public Job getJob() {
		return this.job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public String getExecutionId() {
		return this.executionId;
	}

	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}

	@XmlTransient
	public Timestamp getJobEndTime() {
		return this.jobEndTime;
	}

	public void setJobEndTime(Timestamp jobEndTime) {
		this.jobEndTime = jobEndTime;
	}

	@XmlTransient
	public Timestamp getJobStartTime() {
		return this.jobStartTime;
	}

	public void setJobStartTime(Timestamp jobStartTime) {
		this.jobStartTime = jobStartTime;
	}

	public Long getSkippedProcessfileSkippedHiddenlected() {
		return this.skippedProcessfileSkippedHiddenlected;
	}

	public void setSkippedProcessfileSkippedHiddenlected(
			Long skippedProcessfileSkippedHiddenlected) {
		this.skippedProcessfileSkippedHiddenlected = skippedProcessfileSkippedHiddenlected;
	}

	public Long getSkippedProcessfileSkippedIsdir() {
		return this.skippedProcessfileSkippedIsdir;
	}

	public void setSkippedProcessfileSkippedIsdir(
			Long skippedProcessfileSkippedIsdir) {
		this.skippedProcessfileSkippedIsdir = skippedProcessfileSkippedIsdir;
	}

	public Long getSkippedProcessfileSkippedMismatchAge() {
		return this.skippedProcessfileSkippedMismatchAge;
	}

	public void setSkippedProcessfileSkippedMismatchAge(
			Long skippedProcessfileSkippedMismatchAge) {
		this.skippedProcessfileSkippedMismatchAge = skippedProcessfileSkippedMismatchAge;
	}

	public Long getSkippedProcessfileSkippedMismatchExtension() {
		return this.skippedProcessfileSkippedMismatchExtension;
	}

	public void setSkippedProcessfileSkippedMismatchExtension(
			Long skippedProcessfileSkippedMismatchExtension) {
		this.skippedProcessfileSkippedMismatchExtension = skippedProcessfileSkippedMismatchExtension;
	}

	public Long getSkippedProcessfileSkippedMismatchLastaccesstime() {
		return this.skippedProcessfileSkippedMismatchLastaccesstime;
	}

	public void setSkippedProcessfileSkippedMismatchLastaccesstime(
			Long skippedProcessfileSkippedMismatchLastaccesstime) {
		this.skippedProcessfileSkippedMismatchLastaccesstime = skippedProcessfileSkippedMismatchLastaccesstime;
	}

	public Long getSkippedProcessfileSkippedMismatchLastmodifiedtime() {
		return this.skippedProcessfileSkippedMismatchLastmodifiedtime;
	}

	public void setSkippedProcessfileSkippedMismatchLastmodifiedtime(
			Long skippedProcessfileSkippedMismatchLastmodifiedtime) {
		this.skippedProcessfileSkippedMismatchLastmodifiedtime = skippedProcessfileSkippedMismatchLastmodifiedtime;
	}

	public Long getSkippedProcessfileSkippedReadOnly() {
		return this.skippedProcessfileSkippedReadOnly;
	}

	public void setSkippedProcessfileSkippedReadOnly(
			Long skippedProcessfileSkippedReadOnly) {
		this.skippedProcessfileSkippedReadOnly = skippedProcessfileSkippedReadOnly;
	}

	public Long getSkippedProcessfileSkippedSymbollink() {
		return this.skippedProcessfileSkippedSymbollink;
	}

	public void setSkippedProcessfileSkippedSymbollink(
			Long skippedProcessfileSkippedSymbollink) {
		this.skippedProcessfileSkippedSymbollink = skippedProcessfileSkippedSymbollink;
	}

	public Long getSkippedProcessfileSkippedTemporary() {
		return this.skippedProcessfileSkippedTemporary;
	}

	public void setSkippedProcessfileSkippedTemporary(
			Long skippedProcessfileSkippedTemporary) {
		this.skippedProcessfileSkippedTemporary = skippedProcessfileSkippedTemporary;
	}

	public Long getSkippedProcessfileSkippedTooLarge() {
		return this.skippedProcessfileSkippedTooLarge;
	}

	public void setSkippedProcessfileSkippedTooLarge(
			Long skippedProcessfileSkippedTooLarge) {
		this.skippedProcessfileSkippedTooLarge = skippedProcessfileSkippedTooLarge;
	}

	public Long getTotalFreshArchived() {
		return this.totalFreshArchived;
	}

	public void setTotalFreshArchived(Long totalFreshArchived) {
		this.totalFreshArchived = totalFreshArchived;
	}

	public Long getAlreadyArchived() {
		return alreadyArchived;
	}

	public void setAlreadyArchived(Long alreadyArchived) {
		this.alreadyArchived = alreadyArchived;
	}

	public Long getTotalDuplicated() {
		return this.totalDuplicated;
	}

	public void setTotalDuplicated(Long totalDuplicated) {
		this.totalDuplicated = totalDuplicated;
	}

	public Long getTotalEvaluated() {
		return this.totalEvaluated;
	}

	public void setTotalEvaluated(Long totalEvaluated) {
		this.totalEvaluated = totalEvaluated;
	}

	public Long getTotalFailedArchiving() {
		return this.totalFailedArchiving;
	}

	public void setTotalFailedArchiving(Long totalFailedArchiving) {
		this.totalFailedArchiving = totalFailedArchiving;
	}

	public Long getTotalFailedDeduplication() {
		return this.totalFailedDeduplication;
	}

	public void setTotalFailedDeduplication(Long totalFailedDeduplication) {
		this.totalFailedDeduplication = totalFailedDeduplication;
	}

	public Long getTotalFailedEvaluating() {
		return this.totalFailedEvaluating;
	}

	public void setTotalFailedEvaluating(Long totalFailedEvaluating) {
		this.totalFailedEvaluating = totalFailedEvaluating;
	}

	public Long getTotalFailedStubbing() {
		return this.totalFailedStubbing;
	}

	public void setTotalFailedStubbing(Long totalFailedStubbing) {
		this.totalFailedStubbing = totalFailedStubbing;
	}

	public Long getTotalStubbed() {
		return this.totalStubbed;
	}

	public void setTotalStubbed(Long totalStubbed) {
		this.totalStubbed = totalStubbed;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public long getTotalSkipped() {
		return totalSkipped;
	}

	public void setTotalSkipped(long totalSkipped) {
		this.totalSkipped = totalSkipped;
	}

	public long getTotalFailed() {
		return totalFailed;
	}

	public void setTotalFailed(long totalFailed) {
		this.totalFailed = totalFailed;
	}
	
	public Long getArchivedVolume() {
		return archivedVolume;
	}

	public void setArchivedVolume(Long archivedVolume) {
		this.archivedVolume = archivedVolume;
	}
	
	public void setNumberOfPublicLogs(int numberOfPublicLogs) {
		this.numberOfPublicLogs = numberOfPublicLogs;
	}
	
	public int getNumberOfPublicLogs() {
		int result	=	this.numberOfPublicLogs;
		
		try {
			String basePath	=	Utility.GetProp("logBasePath");
			if(basePath==null || basePath.isEmpty())
				basePath     =   "/jobsLog";
			 
			else {
				basePath	=	basePath + ((basePath.endsWith("/"))?"":"/") +"jobsLog";
			}
			 
			String filePath	=	basePath + "/" + this.job.getId() + "/" + this.executionId + "/"+LoggingManager.PUBLIC_MSGS+".log";			
			File temp	=	new File(filePath);
			if(temp.exists()) {
				result	=	result+1;
			}
			temp	=	null;
				
		} catch(Exception ex){
			ex.printStackTrace();
		}
		
		return result;
	}

	public int getThroughput() {
		if(jobEndTime!=null && jobStartTime!=null){
		  double time=jobEndTime.getTime()-jobStartTime.getTime();
		  time=time/60000.0;
		  if(archivedVolume>0){
			 int filesPerM=(int) Math.ceil(archivedVolume/time);
			 return filesPerM;
		  }
		}
		
		return 0;
	}

	public void setThroughput(int throughput) {
		this.throughput = throughput;
	}
	
	

	
}