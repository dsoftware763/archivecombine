package com.virtualcode.vo;

import java.io.Serializable;

public class NamedNode implements Serializable{
	
	private String type;
	
	private String name;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	 @Override
    public String toString() {
        return this.name+"---"+this.type;
    }
	

}
