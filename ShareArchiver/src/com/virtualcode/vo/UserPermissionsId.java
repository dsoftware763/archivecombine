package com.virtualcode.vo;



/**
 * UserPermissionsId entity. @author MyEclipse Persistence Tools
 */

public class UserPermissionsId  implements java.io.Serializable {


    // Fields    

     private User user;
     private String permissionsString;


    // Constructors

    /** default constructor */
    public UserPermissionsId() {
    }

    
    /** full constructor */
    public UserPermissionsId(User user, String permissionsString) {
        this.user = user;
        this.permissionsString = permissionsString;
    }

   
    // Property accessors

    public User getUser() {
        return this.user;
    }
    
    public void setUser(User user) {
        this.user = user;
    }

    public String getPermissionsString() {
        return this.permissionsString;
    }
    
    public void setPermissionsString(String permissionsString) {
        this.permissionsString = permissionsString;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof UserPermissionsId) ) return false;
		 UserPermissionsId castOther = ( UserPermissionsId ) other; 
         
		 return ( (this.getUser()==castOther.getUser()) || ( this.getUser()!=null && castOther.getUser()!=null && this.getUser().equals(castOther.getUser()) ) )
 && ( (this.getPermissionsString()==castOther.getPermissionsString()) || ( this.getPermissionsString()!=null && castOther.getPermissionsString()!=null && this.getPermissionsString().equals(castOther.getPermissionsString()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getUser() == null ? 0 : this.getUser().hashCode() );
         result = 37 * result + ( getPermissionsString() == null ? 0 : this.getPermissionsString().hashCode() );
         return result;
   }   





}