package com.virtualcode.vo;

import java.util.HashSet;
import java.util.Set;

/**
 * DelJob entity. @author MyEclipse Persistence Tools 
 */

public class DelJob implements java.io.Serializable {

	// Fields

	private Integer id;
	private DelPolicy delPolicy;
	private String actionType;
	private String name;
	private Set delJobFiledetailses = new HashSet(0);

	// Constructors

	/** default constructor */
	public DelJob() {
	}

	/** minimal constructor */
	public DelJob(DelPolicy delPolicy, String actionType, String name) {
		this.delPolicy = delPolicy;
		this.actionType = actionType;
		this.name = name;
	}

	/** full constructor */
	public DelJob(DelPolicy delPolicy, String actionType, String name,
			Set delJobFiledetailses) {
		this.delPolicy = delPolicy;
		this.actionType = actionType;
		this.name = name;
		this.delJobFiledetailses = delJobFiledetailses;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public DelPolicy getDelPolicy() {
		return this.delPolicy;
	}

	public void setDelPolicy(DelPolicy delPolicy) {
		this.delPolicy = delPolicy;
	}

	public String getActionType() {
		return this.actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set getDelJobFiledetailses() {
		return this.delJobFiledetailses;
	}

	public void setDelJobFiledetailses(Set delJobFiledetailses) {
		this.delJobFiledetailses = delJobFiledetailses;
	}

}