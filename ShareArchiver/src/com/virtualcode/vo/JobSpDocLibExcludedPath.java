package com.virtualcode.vo;



/**
 * JobSpDocLibExcludedPath entity. @author MyEclipse Persistence Tools
 */

public class JobSpDocLibExcludedPath  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private JobSpDocLib jobSpDocLib;
     private String path;


    // Constructors

    /** default constructor */
    public JobSpDocLibExcludedPath() {
    }

    
    /** full constructor */
    public JobSpDocLibExcludedPath(JobSpDocLib jobSpDocLib, String path) {
        this.jobSpDocLib = jobSpDocLib;
        this.path = path;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public JobSpDocLib getJobSpDocLib() {
        return this.jobSpDocLib;
    }
    
    public void setJobSpDocLib(JobSpDocLib jobSpDocLib) {
        this.jobSpDocLib = jobSpDocLib;
    }

    public String getPath() {
        return this.path;
    }
    
    public void setPath(String path) {
        this.path = path;
    }
   








}