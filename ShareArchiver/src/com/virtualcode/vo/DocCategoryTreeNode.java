package com.virtualcode.vo;

// default package

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.swing.tree.TreeNode;

import com.google.common.collect.Iterators;


/**
 * DocumentCategory entity. 
 * @author AtiqurRehman
 */
public class DocCategoryTreeNode extends NamedNode  implements java.io.Serializable,TreeNode {

     private Integer id;
     private String description;
     
   
     private List<DocTypeTreeNode> documentTypesList=new ArrayList<DocTypeTreeNode>(0);
     private boolean selected;

    // Constructors

    /** default constructor */
    public DocCategoryTreeNode() {
    	setType("DocumentCategory");
    }

	   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

   
    public List<DocTypeTreeNode> getDocumentTypesList() {
		return documentTypesList;
	}

	public void setDocumentTypesList(List<DocTypeTreeNode> documentTypesList) {
		this.documentTypesList = documentTypesList;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}


	public TreeNode getChildAt(int childIndex) {
        return documentTypesList.get(childIndex);
    }
 
    public int getChildCount() {
        return documentTypesList.size();
    }
 
    public TreeNode getParent() {
        return null;
    }
 
    public int getIndex(TreeNode node) {
        return documentTypesList.indexOf(node);
    }
 
    public boolean getAllowsChildren() {
        return true;
    }
 
    public boolean isLeaf() {
        return documentTypesList.isEmpty();
    }
 
    public Enumeration<DocTypeTreeNode> children() {
        return Iterators.asEnumeration(documentTypesList.iterator());
    }
 
 
}