package com.virtualcode.vo;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * JobSpDocLib entity. @author MyEclipse Persistence Tools
 */

public class JobSpDocLib  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private Job job;
     private String guid;
     private String name;
     private String type;
     private String destinationPath;
     private String site;
     private String drivePath;
     private String contentTypes;
     private Boolean recursive=false;
     private String tags;
     private Set jobSpDocLibExcludedPaths = new HashSet(0);
          
     private List<String> excludedPathList;//only for view


    // Constructors

    public List<String> getExcludedPathList() {
		return excludedPathList;
	}

	public void setExcludedPathList(List<String> excludedPathList) {
		this.excludedPathList = excludedPathList;
	}

	/** default constructor */
    public JobSpDocLib() {
    }

	/** minimal constructor */
    public JobSpDocLib(Job job, String name) {
        this.job = job;
        this.name = name;
    }
    
    /** full constructor */
    public JobSpDocLib(Job job, String guid, String name, String type, String destinationPath, Boolean recursive, Set jobSpDocLibExcludedPaths) {
        this.job = job;
        this.guid = guid;
        this.name = name;
        this.type = type;
        this.destinationPath = destinationPath;
        this.recursive = recursive;
        this.jobSpDocLibExcludedPaths = jobSpDocLibExcludedPaths;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public Job getJob() {
        return this.job;
    }
    
    public void setJob(Job job) {
        this.job = job;
    }

    public String getGuid() {
        return this.guid;
    }
    
    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return this.type;
    }
    
    public void setType(String type) {
        this.type = type;
    }

    public String getDestinationPath() {
        return this.destinationPath;
    }
    
    public void setDestinationPath(String destinationPath) {
        this.destinationPath = destinationPath;
    }

    public Boolean getRecursive() {
        return this.recursive;
    }
    
    public void setRecursive(Boolean recursive) {
        this.recursive = recursive;
    }

    public Set getJobSpDocLibExcludedPaths() {
        return this.jobSpDocLibExcludedPaths;
    }
    
    public void setJobSpDocLibExcludedPaths(Set jobSpDocLibExcludedPaths) {
        this.jobSpDocLibExcludedPaths = jobSpDocLibExcludedPaths;
    }

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getDrivePath() {
		return drivePath;
	}

	public void setDrivePath(String drivePath) {
		this.drivePath = drivePath;
	}

	public String getContentTypes() {
		return contentTypes;
	}

	public void setContentTypes(String contentTypes) {
		this.contentTypes = contentTypes;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}
   








}