package com.virtualcode.vo;

import java.util.ArrayList;
import java.util.List;

import com.virtualcode.util.ResourcesUtil;

public class SearchReport {
	
	private String searchString;
	private String startDate;
	private String endDate;
	private String totalRecords;
	
	private String username;
	private String date;
	private String keyword;
	private String fileTypes;
	
	private String tags;
	
	private List<SearchDocument> searchResults=new ArrayList<SearchDocument>();
	
	private static SearchReport searchReport;
	
	private SearchReport(){
		
	}
    public static SearchReport getInstance() {
        if(searchReport==null)
        	searchReport   =   new SearchReport();
        
        return searchReport;
    }

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(String totalRecords) {
		this.totalRecords = totalRecords;
	}

	public List<SearchDocument> getSearchResults() {
		return searchResults;
	}

	public void setSearchResults(List<SearchDocument> searchResults) {
		this.searchResults = searchResults;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getFileTypes() {
		return fileTypes;
	}
	public void setFileTypes(String fileTypes) {
		this.fileTypes = fileTypes;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	
	

}
