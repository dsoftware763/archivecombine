package com.virtualcode.vo;

public class SystemCodeType {
	
	public static final String GENERAL="General";
	
	public static final String JESPA="jespa";
	
	public static final String ACTIVE_DIR_AUTH="activedirauthenticator";
	
	public static final String ZUES_AGENT="ZuesAgent";
	
	public static final String COMPANY = "Company";
	
	public static final String ACCOUNT_POLICY = "AccountPolicy";

}
