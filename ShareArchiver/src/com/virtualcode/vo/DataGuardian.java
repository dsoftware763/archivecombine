package com.virtualcode.vo;

/**
 * DataGuardian entity. @author MyEclipse Persistence Tools
 */

public class DataGuardian implements java.io.Serializable {

	// Fields

	private Integer id;
	private String dataGuardianEmail;

	// Constructors

	/** default constructor */
	public DataGuardian() {
	}

	/** full constructor */
	public DataGuardian(String dataGuardianEmail) {
		this.dataGuardianEmail = dataGuardianEmail;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDataGuardianEmail() {
		return this.dataGuardianEmail;
	}

	public void setDataGuardianEmail(String dataGuardianEmail) {
		this.dataGuardianEmail = dataGuardianEmail;
	}

}