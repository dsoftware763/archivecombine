package com.virtualcode.vo;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;


/**
 * Job entity. @author MyEclipse Persistence Tools
 */
@XmlRootElement(name="job")
public class Job  implements java.io.Serializable {

     private Integer id;
     private Policy policy;
     private Agent agent;
     private String actionType="EXPORT";
     private Integer active=1;
     private String description;
     //private boolean runNow;
     private String name;
     private String spSiteId;
     private String spPath;
     private Boolean processCurrentPublishedVersion = true;
     private Boolean processLegacyDraftVersion = false;
     private Boolean processLegacyPublishedVersion = false;
     private Boolean processArchive=true;
     private Boolean processHidden=true;
     private Boolean processReadOnly=true;
     private Boolean readyToExecute=true;
     private Set jobDocuments = new HashSet(0);
     private Set jobStatisticses = new HashSet(0);
     private Set jobSpDocLibs = new HashSet(0);
     private Set jobStatuses = new HashSet(0);
     private Boolean isScheduled=false;
     private Double executionInterval;
     private Date execTimeStartDate;
     private Timestamp execTimeStart;
     private String activeActionType="ARCHIVEONLY";
     
     //for priv export only
     private boolean preserveFilePaths =  true;
     private boolean exportDuplicates = false;
     
     //for share analysis job
     private boolean isShareAnalysis = false;

     private String jobStatus;	//only for job status view page
     private List<JobSpDocLib> includedLibraries;
     
     private String scheduledLable=null;
     
     private String nextTimeOfRun;
     
     private boolean threshholdJob=false;
     
     private String scheduled="yes";
     
     private String tags;	
     
     private String nextAction	=	null;
     
     private String currentStatus;

    // Constructors

    public String getNextAction() {
		return nextAction;
	}

	public void setNextAction(String nextAction) {
		this.nextAction = nextAction;
	}

	public List<JobSpDocLib> getIncludedLibraries() {
		return includedLibraries;
	}

	public void setIncludedLibraries(List<JobSpDocLib> includedLibraries) {
		this.includedLibraries = includedLibraries;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	/** default constructor */
    public Job() {
    	policy=new Policy();
    	agent=new Agent();
    	
    }


    // Property accessors

    public Boolean getIsScheduled() {
		return isScheduled;
	}

	public Date getExecTimeStartDate() {
		return execTimeStartDate;
	}

	public void setExecTimeStartDate(Date execTimeStartDate) {
		this.execTimeStartDate = execTimeStartDate;
	}

	public void setIsScheduled(Boolean isScheduled) {
		this.isScheduled = isScheduled;
	}

	public Double getExecutionInterval() {
		return executionInterval;
	}

	public void setExecutionInterval(Double executionInterval) {
		this.executionInterval = executionInterval;
	}

	public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public Policy getPolicy() {
        return policy;
    }
    
    public void setPolicy(Policy policy) {
        this.policy = policy;
    }

    public Agent getAgent() {
        return agent;
    }
    
    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getActionType() {
        return actionType;
    }
    
    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public Integer getActive() {
        return active;
    }
    
    public void setActive(Integer active) {
        this.active = active;
    }

    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getExecTimeStart() {
        return execTimeStart;
    }
    
    public void setExecTimeStart(Timestamp execTimeStart) {
        this.execTimeStart = execTimeStart;
    }

    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getSpSiteId() {
        return spSiteId;
    }
    
    public void setSpSiteId(String spSiteId) {
        this.spSiteId = spSiteId;
    }

    public String getSpPath() {
        return spPath;
    }
    
    public void setSpPath(String spPath) {
        this.spPath = spPath;
    }

    public Boolean getProcessCurrentPublishedVersion() {
        return processCurrentPublishedVersion;
    }
    
    public void setProcessCurrentPublishedVersion(Boolean processCurrentPublishedVersion) {
        this.processCurrentPublishedVersion = processCurrentPublishedVersion;
    }

    public Boolean getProcessLegacyDraftVersion() {
        return processLegacyDraftVersion;
    }
    
    public void setProcessLegacyDraftVersion(Boolean processLegacyDraftVersion) {
        this.processLegacyDraftVersion = processLegacyDraftVersion;
    }

    public Boolean getProcessLegacyPublishedVersion() {
        return processLegacyPublishedVersion;
    }
    
    public void setProcessLegacyPublishedVersion(Boolean processLegacyPublishedVersion) {
        this.processLegacyPublishedVersion = processLegacyPublishedVersion;
    }

    public Boolean getProcessArchive() {
        return processArchive;
    }
    
    public void setProcessArchive(Boolean processArchive) {
        this.processArchive = processArchive;
    }

    public Boolean getProcessHidden() {
        return processHidden;
    }
    
    public void setProcessHidden(Boolean processHidden) {
        this.processHidden = processHidden;
    }

    public Boolean getProcessReadOnly() {
         return  processReadOnly;
    }
    
    public void setProcessReadOnly(Boolean processReadOnly) {
        this.processReadOnly = processReadOnly;
    }

    public Boolean getReadyToExecute() {
        return readyToExecute;
    }
    
    public void setReadyToExecute(Boolean readyToExecute) {
        this.readyToExecute = readyToExecute;
    }

    public Set getJobDocuments() {
         return  jobDocuments;
    }
    
    public void setJobDocuments(Set jobDocuments) {
        this.jobDocuments = jobDocuments;
    }

    public Set getJobStatisticses() {
         return  jobStatisticses;
    }
    
    public void setJobStatisticses(Set jobStatisticses) {
        this.jobStatisticses = jobStatisticses;
    }

    public Set getJobSpDocLibs() {
         return  jobSpDocLibs;
    }
    
    public void setJobSpDocLibs(Set jobSpDocLibs) {
        this.jobSpDocLibs = jobSpDocLibs;
    }

    public Set getJobStatuses() {
         return  jobStatuses;
    }
    
    public void setJobStatuses(Set jobStatuses) {
        this.jobStatuses = jobStatuses;
    }

	public boolean isPreserveFilePaths() {
		return preserveFilePaths;
	}

	public void setPreserveFilePaths(boolean preserveFilePaths) {
		this.preserveFilePaths = preserveFilePaths;
	}

	public boolean isExportDuplicates() {
		return exportDuplicates;
	}

	public void setExportDuplicates(boolean exportDuplicates) {
		this.exportDuplicates = exportDuplicates;
	}

	public String getActiveActionType() {
		return activeActionType;
	}

	public void setActiveActionType(String activeActionType) {
		this.activeActionType = activeActionType;
	}

	public boolean getIsShareAnalysis() {
		return isShareAnalysis;
	}

	public void setIsShareAnalysis(boolean isShareAnalysis) {
		this.isShareAnalysis = isShareAnalysis;
	}

	public String getScheduledLable() {
				
		if(executionInterval==1)
			scheduledLable="Daily";
		
		else if(executionInterval==7)
			scheduledLable="Weekly";
		
		else if(executionInterval==30)
			scheduledLable="Monthly";
		
		else if(executionInterval==120)
			scheduledLable="Quarterly";
		
		else if(executionInterval==365)
			scheduledLable="Yearly";
		
		else
			scheduledLable=null;
		 
		return scheduledLable;
	}

	public String getNextTimeOfRun() {
		return nextTimeOfRun;
	}

	public void setNextTimeOfRun(String nextTimeOfRun) {
		this.nextTimeOfRun = nextTimeOfRun;
	}

	public boolean isThreshholdJob() {
		return threshholdJob;
	}

	public void setThreshholdJob(boolean threshholdJob) {
		this.threshholdJob = threshholdJob;
	}

	public String getScheduled() {
		return scheduled;
	}

	public void setScheduled(String scheduled) {
		this.scheduled = scheduled;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

}