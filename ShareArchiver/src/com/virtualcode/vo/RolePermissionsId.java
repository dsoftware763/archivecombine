package com.virtualcode.vo;



/**
 * RolePermissionsId entity. @author MyEclipse Persistence Tools
 */

public class RolePermissionsId  implements java.io.Serializable {


    // Fields    

     private Role shiroRole;
     private String permissionsString;


    // Constructors

    /** default constructor */
    public RolePermissionsId() {
    }

    
    /** full constructor */
    public RolePermissionsId(Role shiroRole, String permissionsString) {
        this.shiroRole = shiroRole;
        this.permissionsString = permissionsString;
    }

   
    // Property accessors

    public Role getShiroRole() {
        return this.shiroRole;
    }
    
    public void setShiroRole(Role shiroRole) {
        this.shiroRole = shiroRole;
    }

    public String getPermissionsString() {
        return this.permissionsString;
    }
    
    public void setPermissionsString(String permissionsString) {
        this.permissionsString = permissionsString;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof RolePermissionsId) ) return false;
		 RolePermissionsId castOther = ( RolePermissionsId ) other; 
         
		 return ( (this.getShiroRole()==castOther.getShiroRole()) || ( this.getShiroRole()!=null && castOther.getShiroRole()!=null && this.getShiroRole().equals(castOther.getShiroRole()) ) )
 && ( (this.getPermissionsString()==castOther.getPermissionsString()) || ( this.getPermissionsString()!=null && castOther.getPermissionsString()!=null && this.getPermissionsString().equals(castOther.getPermissionsString()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getShiroRole() == null ? 0 : this.getShiroRole().hashCode() );
         result = 37 * result + ( getPermissionsString() == null ? 0 : this.getPermissionsString().hashCode() );
         return result;
   }   





}