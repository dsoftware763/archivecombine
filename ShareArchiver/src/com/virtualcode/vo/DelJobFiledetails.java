package com.virtualcode.vo;

/**
 * DelJobFiledetails entity.
 */

public class DelJobFiledetails implements java.io.Serializable {

	// Fileds

	private Integer id;
	private DelJob delJob;
	private String uuid;
	private String status;
	private String description;

	// Constructors

	/** default constructor */
	public DelJobFiledetails() {
	}

	/** full constructor */
	public DelJobFiledetails(DelJob delJob, String uuid, String status,
			String description) {
		this.delJob = delJob;
		this.uuid = uuid;
		this.status = status;
		this.description = description;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public DelJob getDelJob() {
		return this.delJob;
	}

	public void setDelJob(DelJob delJob) {
		this.delJob = delJob;
	}

	public String getUuid() {
		return this.uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}