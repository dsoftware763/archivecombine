package com.virtualcode.vo;

import org.springframework.web.servlet.handler.UserRoleAuthorizationInterceptor;

/**
 * PageRights entity. @author MyEclipse Persistence Tools
 */

public class PageRights implements java.io.Serializable {

	// Fields

	private Integer id;
	private Role role;
	private String pageUri;

	// Constructors

	/** default constructor */
	public PageRights() {
	}

	/** full constructor */
	public PageRights(Role role, String pageUri) {
		this.role = role;
		this.pageUri = pageUri;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getPageUri() {
		return this.pageUri;
	}

	public void setPageUri(String pageUri) {
		this.pageUri = pageUri;
	}

}