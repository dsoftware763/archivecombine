package com.virtualcode.vo;

import java.util.HashSet;
import java.util.Set;

/**
 * DelPolicy entity.
 */

public class DelPolicy implements java.io.Serializable {

	// Fields
	private Integer id;
	private String name;
	private String modifiedDate;
	private String accessDate;
	private String archivalDate;
	private String fileType;
	private String fileSize;
	private Set delJobs = new HashSet(0);

	// Constructor

	/** default constructor */
	public DelPolicy() {
	}

	/** minimal constructor */
	public DelPolicy(String name, String modifiedDate, String accessDate,
			String archivalDate, String fileType, String fileSize) {
		this.name = name;
		this.modifiedDate = modifiedDate;
		this.accessDate = accessDate;
		this.archivalDate = archivalDate;
		this.fileType = fileType;
		this.fileSize = fileSize;
	}

	/** full constructor */
	public DelPolicy(String name, String modifiedDate, String accessDate,
			String archivalDate, String fileType, String fileSize, Set delJobs) {
		this.name = name;
		this.modifiedDate = modifiedDate;
		this.accessDate = accessDate;
		this.archivalDate = archivalDate;
		this.fileType = fileType;
		this.fileSize = fileSize;
		this.delJobs = delJobs;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getAccessDate() {
		return this.accessDate;
	}

	public void setAccessDate(String accessDate) {
		this.accessDate = accessDate;
	}

	public String getArchivalDate() {
		return this.archivalDate;
	}

	public void setArchivalDate(String archivalDate) {
		this.archivalDate = archivalDate;
	}

	public String getFileType() {
		return this.fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFileSize() {
		return this.fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	public Set getDelJobs() {
		return this.delJobs;
	}

	public void setDelJobs(Set delJobs) {
		this.delJobs = delJobs;
	}

}