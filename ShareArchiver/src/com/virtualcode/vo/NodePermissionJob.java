package com.virtualcode.vo;

/**
 * NodePermissionJob entity. @author MyEclipse Persistence Tools
 */

public class NodePermissionJob implements java.io.Serializable {

	// Fields

	private Integer id;
	private String name;
	private String command;
	private Integer status;
	private String registerdate;
	private String startdate;
	private String finisheddate;
	private String secureinfo;
	private String sids;
	private String denysids;
	private String sharesids;
	private String denysharesids;
	private Integer isrecursive;

	// Constructors

	/** default constructor */
	public NodePermissionJob() {
	}

	/** full constructor */
	public NodePermissionJob(String name, String command, Integer status,
			String registerdate, String startdate, String finisheddate,
			String secureinfo, String sids, String denysids, String sharesids,
			String denysharesids, Integer isrecursive) {
		this.name = name;
		this.command = command;
		this.status = status;
		this.registerdate = registerdate;
		this.startdate = startdate;
		this.finisheddate = finisheddate;
		this.secureinfo = secureinfo;
		this.sids = sids;
		this.denysids = denysids;
		this.sharesids = sharesids;
		this.denysharesids = denysharesids;
		this.isrecursive = isrecursive;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCommand() {
		return this.command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getRegisterdate() {
		return this.registerdate;
	}

	public void setRegisterdate(String registerdate) {
		this.registerdate = registerdate;
	}

	public String getStartdate() {
		return this.startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getFinisheddate() {
		return this.finisheddate;
	}

	public void setFinisheddate(String finisheddate) {
		this.finisheddate = finisheddate;
	}

	public String getSecureinfo() {
		return this.secureinfo;
	}

	public void setSecureinfo(String secureinfo) {
		this.secureinfo = secureinfo;
	}

	public String getSids() {
		return this.sids;
	}

	public void setSids(String sids) {
		this.sids = sids;
	}

	public String getDenysids() {
		return this.denysids;
	}

	public void setDenysids(String denysids) {
		this.denysids = denysids;
	}

	public String getSharesids() {
		return this.sharesids;
	}

	public void setSharesids(String sharesids) {
		this.sharesids = sharesids;
	}

	public String getDenysharesids() {
		return this.denysharesids;
	}

	public void setDenysharesids(String denysharesids) {
		this.denysharesids = denysharesids;
	}

	public Integer getIsrecursive() {
		return this.isrecursive;
	}

	public void setIsrecursive(Integer isrecursive) {
		this.isrecursive = isrecursive;
	}

}