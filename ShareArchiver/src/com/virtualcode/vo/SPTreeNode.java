package com.virtualcode.vo;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;

import javax.swing.tree.TreeNode;

import com.google.common.collect.Iterators;

public class SPTreeNode extends NamedNode implements TreeNode{
	
	private String guid;
	private String path;
	private boolean subLibrary;
	private Boolean selected;
	private String parentPath;
	private String sitePath;

	private ArrayList<SPTreeNode> childNodes = new ArrayList<SPTreeNode>();
	private SPTreeNode parentNode=null;
	
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}	
	public String getSitePath() {
		return sitePath;
	}
	public void setSitePath(String sitePath) {
		this.sitePath = sitePath;
	}
		
	public ArrayList<SPTreeNode> getChildNodes() {
		return childNodes;
	}
	public void setChildNodes(ArrayList<SPTreeNode> childNodes) {
		this.childNodes = childNodes;
	}

	
	public SPTreeNode getParentNode() {
		return parentNode;
	}
	public void setParentNode(SPTreeNode parentNode) {
		this.parentNode = parentNode;
	}
	public boolean isSubLibrary() {
		return subLibrary;
	}
	public void setSubLibrary(boolean subLibrary) {
		this.subLibrary = subLibrary;
	}

	public Boolean getSelected() {
		return selected;
	}
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	public String getParentPath() {
		return parentPath;
	}
	public void setParentPath(String parentPath) {
		this.parentPath = parentPath;
	}
	
	@Override
	public TreeNode getChildAt(int childIndex) {
		// TODO Auto-generated method stub
		return childNodes.get(childIndex);
	}
	@Override
	public int getChildCount() {
		// TODO Auto-generated method stub
		return childNodes.size();
	}
	@Override
	public TreeNode getParent() {
		// TODO Auto-generated method stub
		return parentNode;
	}
	@Override
	public int getIndex(TreeNode node) {
		// TODO Auto-generated method stub
		return childNodes.indexOf(node);
	}
	@Override
	public boolean getAllowsChildren() {
		// TODO Auto-generated method stub
		return true;
	}
	@Override
	public boolean isLeaf() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public Enumeration children() {
		// TODO Auto-generated method stub
		return Iterators.asEnumeration(childNodes.iterator());
	}

}
