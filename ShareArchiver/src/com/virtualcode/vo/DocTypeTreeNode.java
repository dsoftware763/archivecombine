package com.virtualcode.vo;

// default package

import java.util.Enumeration;

import javax.swing.tree.TreeNode;


/**
 * DocumentType entity. 
 * @author AtiqurRehman
 */

public class DocTypeTreeNode extends NamedNode  implements java.io.Serializable,TreeNode {

     private Integer id;
     private String description;  
     
     private DocCategoryTreeNode docCategoryTreeNode;
         
     private boolean selected;

    // Constructors

    /** default constructor */
    public DocTypeTreeNode() {
    	setType("DocumentType");
    }
	   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

	
	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public TreeNode getChildAt(int childIndex) {
        return null;
    }
 
    public int getChildCount() {
        return 0;
    }
 
    public TreeNode getParent() {
        return docCategoryTreeNode;
    }
 
    public int getIndex(TreeNode node) {
        return 0;
    }
 
    public boolean getAllowsChildren() {
        return true;
    }
 
    public boolean isLeaf() {
        return true;
    }
 
    public Enumeration<DocTypeTreeNode> children() {
        return null;
    }

	public DocCategoryTreeNode getDocCategoryTreeNode() {
		return docCategoryTreeNode;
	}

	public void setDocCategoryTreeNode(DocCategoryTreeNode docCategoryTreeNode) {
		this.docCategoryTreeNode = docCategoryTreeNode;
	}
       

}