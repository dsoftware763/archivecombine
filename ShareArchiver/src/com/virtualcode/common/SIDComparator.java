/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.common;

import java.util.Comparator;

/**
*
* @author AtiqurRehman
*/

public class SIDComparator implements Comparator {

    public SIDComparator() {
    }

    public int compare(Object o1, Object o2) {
        SID first = (SID) o1;
        SID second = (SID) o2;
        return first.getDescription().compareToIgnoreCase(second.getDescription());
    }
}
