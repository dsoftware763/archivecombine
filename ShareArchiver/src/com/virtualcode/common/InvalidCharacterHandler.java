/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.virtualcode.common;

/**
 *
 * @author AtiqurRehman
 */ 
public class InvalidCharacterHandler {
    
    public static void main(String[] args) {
        String testString = "]]*++mango![[[[[[[[((&*^%";
        String resultant = replaceInvalidCharacters(testString);
        System.out.println(testString);
        System.out.println(resultant);
    }

    public static String replaceInvalidCharacters(String srcString){
        String toReturn = null;
        if(srcString == null || srcString.length() < 1)
            return srcString;

        char[] chars = srcString.toCharArray();
        int i = 0;
        for(i=0;i<chars.length;i++){
            switch(chars[i]) {
                case '/':
                    chars[i] = '~';
                case ':':
                    chars[i] = '_';
                    break;
                case '[':
                    chars[i] = '{';
                    break;
                case ']':
                    chars[i] = '}';
                    break;
                case '|':
                    chars[i] = '!';
                    break;
                case '*':
                    chars[i] = '^';
                    break;
            }
        }
        toReturn = String.valueOf(chars);
        return toReturn;
    }
}
