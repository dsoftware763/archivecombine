package com.virtualcode.common;

import java.text.DecimalFormat;
import java.util.List;

import jcifs.smb.SmbFile;

import com.virtualcode.util.VCSUtil;
import com.virtualcode.vo.DriveLetters;

public class ShareSizeRunner implements Runnable{

	private DriveLetters driveLetter=null;
	
	public DriveLetters getDriveLetter() {
		return driveLetter;
	}

	public void setDriveLetter(DriveLetters driveLetter) {
		this.driveLetter = driveLetter;
	}

	@Override
	public void run() {
		
		synchronized(driveLetter){
			
			String smbPath;
			int usedPer=0;
			int freePer=0;
			
			smbPath=VCSUtil.getSambaPath(driveLetter.getRestoreDomain(), driveLetter.getRestoreUsername(), driveLetter.getRestorePassword(), driveLetter.getSharePath());
			freePer=getShareFreeSpace(smbPath,driveLetter);
			
		}
				
	}
	
    public int getShareFreeSpace(String smbPath,DriveLetters driveLetter){
		
    	float freePer=0;
    	float freeSpace=0;
    	float totalSpace=0;
		try{				
				SmbFile smbFile = new SmbFile(smbPath);
				if (!smbFile.exists()) {
				         System.out.println("UNC Path Invalid....");				        
				} else {
					
					System.out.println(smbFile.list()); //throws exception access denied if user does not have permission
				
					totalSpace=smbFile.length();
					freeSpace=smbFile.getDiskFreeSpace();
										
					if(totalSpace>0 && totalSpace>freeSpace){
						driveLetter.setFreeSpace(roundTwoDecimals(freeSpace/(1024*1024*1024)));
						driveLetter.setTotalSpace(roundTwoDecimals(totalSpace/(1024*1024*1024)));
						
						freePer=(freeSpace/totalSpace);
						freePer=freePer*100;
						freePer=(freePer>0?freePer:0);
						int usedPer=100-(int)freePer;
						driveLetter.setShareFreePercentage((int)freePer);
						driveLetter.setShareUsedPercentage((100-(int)freePer));
						
						if(usedPer>=driveLetter.getCriticalLimit()){
							driveLetter.setWarningColour(2);//critical
						}			
						if(usedPer>=driveLetter.getWarningLimit() && usedPer<driveLetter.getCriticalLimit()){
							driveLetter.setWarningColour(1);//warning
						}			
						if(usedPer<driveLetter.getWarningLimit()){
							driveLetter.setWarningColour(0);//no-warning
						}				
						
					}else{
						driveLetter.setShareUsedPercentage(0);
					}
					
				}
		
		}catch(Exception ex){
			
		}		
		return 0;		
	}
    
    float roundTwoDecimals(float d) {
        DecimalFormat twoDForm = new DecimalFormat("#.##");
    return Float.valueOf(twoDForm.format(d));
   }


}
