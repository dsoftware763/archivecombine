/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.common;


/**
 * 
 CREATE TABLE node_permission_job (
 id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 name VARCHAR(300),
 command VARCHAR(500),
 status  INT,
 registerdate VARCHAR(50),
 startdate  VARCHAR(50),
 finisheddate VARCHAR(50),
 secureinfo VARCHAR(100),
 sids VARCHAR(500),
 denysids  VARCHAR(500),
 sharesids VARCHAR(500),
 denysharesids VARCHAR(500),
 isrecursive INT
 );
 
 */


/**
 *
 * @author AtiqurRehman
 */
public class UpdateNodePermissionJob {
    
    public static final int QUEUED_JOB=1;
    public static final int RUNNING_JOB=2;
    public static final int COMPELTED_JOB=3;

    private long id;
    private String name;
    private String command;
    private int status;
    private String registerDate;
    private String startedDate;
    private String finishedDate;
    private String secureinfo;
    private String sids;
    private String denysids;
    private String sharesids;
    private String denysharesids;
    private int isrecursive;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the command
     */
    public String getCommand() {
        return command;
    }

    /**
     * @param command the command to set
     */
    public void setCommand(String command) {
        this.command = command;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the registerDate
     */
    public String getRegisterDate() {
        return registerDate;
    }

    /**
     * @param registerDate the registerDate to set
     */
    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    /**
     * @return the startedDate
     */
    public String getStartedDate() {
        return startedDate;
    }

    /**
     * @param startedDate the startedDate to set
     */
    public void setStartedDate(String startedDate) {
        this.startedDate = startedDate;
    }

    /**
     * @return the finishedDate
     */
    public String getFinishedDate() {
        return finishedDate;
    }

    /**
     * @param finishedDate the finishedDate to set
     */
    public void setFinishedDate(String finishedDate) {
        this.finishedDate = finishedDate;
    }

    /**
     * @return the secureInfo
     */
    public String getSecureInfo() {
        return secureinfo;
    }

    /**
     * @param secureInfo the secureInfo to set
     */
    public void setSecureInfo(String secureinfo) {
        this.secureinfo = secureinfo;
    }

    /**
     * @return the sidsString
     */
    public String getSids() {
        return sids;
    }

    /**
     * @param sidsString the sidsString to set
     */
    public void setSids(String sidsString) {
        this.sids = sidsString;
    }

    /**
     * @return the denySidsString
     */
    public String getDenySids() {
        return denysids;
    }

    /**
     * @param denySidsString the denySidsString to set
     */
    public void setDenySids(String denySidsString) {
        this.denysids = denySidsString;
    }

    /**
     * @return the shareSidsString
     */
    public String getShareSids() {
        return sharesids;
    }

    /**
     * @param shareSidsString the shareSidsString to set
     */
    public void setShareSids(String shareSidsString) {
        this.sharesids = shareSidsString;
    }

    /**
     * @return the denyShareSidsString
     */
    public String getDenyShareSids() {
        return denysharesids;
    }

    /**
     * @param denyShareSidsString the denyShareSidsString to set
     */
    public void setDenyShareSids(String denyShareSidsString) {
        this.denysharesids = denyShareSidsString;
    }

    /**
     * @return the isRecursive
     */
    public int isRecursive() {
        return isrecursive;
    }

    /**
     * @param isRecursive the isRecursive to set
     */
    public void setRecursive(int isRecursive) {
        this.isrecursive = isRecursive;
    }
}
