package com.virtualcode.common;

public class PLFormatException  extends Exception {

	public PLFormatException() {
		super();
	}
	
	public PLFormatException(String message) {
		super(message);
	}
	
}
