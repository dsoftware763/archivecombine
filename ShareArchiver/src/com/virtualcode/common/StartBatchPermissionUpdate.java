/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.common;

//import com.virtualcode.base.core.db.SystemCodeType;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.SecurityPermissionUtilDaemon;
import com.virtualcode.vo.SystemCodeType;

import java.util.ResourceBundle;

import javax.jcr.Session;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.Logger;
import org.springmodules.jcr.JcrTemplate;

/**
 *
 * @author Admin
 */
public class StartBatchPermissionUpdate{
	
	private JcrTemplate jcrTemplate;	
    private final static Logger log=Logger.getLogger(StartBatchPermissionUpdate.class);
    private final String DELAY_TIME="delay.time";
    private final String REPEAT_TIME="repeat.time";
    /*
    ResourceBundle bundle = ResourceBundle.getBundle("com.virtualcode.resources.sharearchiver");
    int delay_time = Integer.parseInt(bundle.getString(DELAY_TIME));
    int repeat_time= Integer.parseInt(bundle.getString(REPEAT_TIME));
     */
    ResourcesUtil   ru  =   ResourcesUtil.getResourcesUtil();
    int delay_time = Integer.parseInt(ru.getCodeValue(DELAY_TIME, SystemCodeType.GENERAL));
    int repeat_time= Integer.parseInt(ru.getCodeValue(REPEAT_TIME, SystemCodeType.GENERAL));

    StartBatchPermissionUpdate(JcrTemplate jcrTemplate){
    	this.jcrTemplate = jcrTemplate;
    	contextInitialized(jcrTemplate);
    }
    
    public void contextDestroyed(ServletContextEvent scEvent)
    {
        log.debug("Stopping StartBatchPermissionUpdate successfully");
    }

    //public void contextInitialized(ServletContextEvent scEvent)
    public void contextInitialized(JcrTemplate jcrTemplate)
    {
    	System.out.println("StartBatchPermissionUpdate ....  thread starting");
       log.debug("Initializing StartBatchPermissionUpdate successfully");
       int delay = delay_time*1000;   // delay for 5 sec.
       int period = repeat_time*1000;  // repeat every sec.

       try{
           log.debug("Batch Update Delay :"+delay);
           log.debug("Batch Update Period :"+period);
           java.util.Timer timer = new java.util.Timer();  
           //SecurityPermissionUtilDaemon batchUpdateTask = new SecurityPermissionUtilDaemon(scEvent.getServletContext());  
           SecurityPermissionUtilDaemon batchUpdateTask = new SecurityPermissionUtilDaemon(jcrTemplate);
           timer.scheduleAtFixedRate(batchUpdateTask,delay,period); 
           log.debug("Batch Update Process Started");
         
       }
       catch(Exception e)
       {
           e.printStackTrace();
       }
     }
}
