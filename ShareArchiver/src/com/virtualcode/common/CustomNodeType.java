/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.common;

import javax.jcr.nodetype.NodeType;

/**
*
* @author AtiqurRehman
*/
public interface CustomNodeType extends NodeType {
    static final String VC_RESOURCE_BASE    = "vc:resource_base";
    static final String VC_FS_RESOURCE      = "vc:fs_resource";
    static final String VC_SP_RESOURCE      = "vc:sp_resource";
    static final String VC_STATUS_RESOURCE  = "vc:status_resource";
    static final String VC_RESOURCE_PERMISSIONS  = "vc:resource_permissions";
    
    static final String VC_USER    = "vc:user";
    static final String VC_USER_ACTIVITY    = "vc:user_activity";
}
