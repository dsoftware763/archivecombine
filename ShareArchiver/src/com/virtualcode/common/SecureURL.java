/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.common;

import javax.crypto.Cipher;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import javax.crypto.spec.SecretKeySpec;
import org.apache.log4j.Logger;

/**
 *
 * @author jawad
 */
public class SecureURL {

    static final Logger log = Logger.getLogger(SecureURL.class);
    private static String algorithm = "DESede";
    private Key key = null;
    private Cipher cipher = null;
    private static SecureURL _object = null;
    private static final String hexEncodedKey = "4f674951b97c7fb09bd34370fe15e9bf700d67c73470c767";

    private SecureURL() {
        try {
            byte[] raw = Hex.decodeHex(hexEncodedKey.toCharArray());
            key = new SecretKeySpec(raw, algorithm);
            //        key = KeyGenerator.getInstance(algorithm).generateKey();
            cipher = Cipher.getInstance(algorithm);
        } catch (NoSuchAlgorithmException ex) {
            log.error(ex);
        } catch (NoSuchPaddingException ex) {
            log.error(ex);
        } catch (DecoderException ex) {
            log.error(ex);
        }
    }

    public synchronized static SecureURL getInstance() {
        if (_object == null) {
            _object = new SecureURL();
        }
        return _object;
    }

    public void testDecode() throws Exception {
        String str = "cd168ce9c77773553446249de896b7ecc4fcd70fe1b5cf2c6c0c35361f84ddf3a33e937ea8094f2f";

        byte[] totalReverted = new Hex().decode(str.getBytes());
        for (byte encryptedByte : totalReverted) {
            System.out.print(encryptedByte);
        }
        log.debug("");

        log.debug("Recovered: " + decrypt(totalReverted));
    }

    public String getEncryptedURL(String input) throws Exception{
        String encryptedURL = null;

        byte[] encryptionBytes = encrypt(input);
        encryptedURL = new String(Hex.encodeHex(encryptionBytes));

        return encryptedURL;
    }

    public String getDecryptedURL(String encryptedURL) throws Exception {
        String decrypted = null;

        byte[] rawBytes = Hex.decodeHex(encryptedURL.toCharArray());
        decrypted = decrypt(rawBytes);

        return decrypted;
    }

    public static void main(String[] args) throws Exception{
        SecureURL secureURL = SecureURL.getInstance();
        String encrypted = null;
        String input = "/FS/c/e/folder_name1/folder_name2/filename.txt";
        encrypted = secureURL.getEncryptedURL(input);

        log.debug("Encrypted: " + encrypted);
//        encrypted = "cf22e2380191910d";
//        String raw = secureURL.getDecryptedURL(encrypted);
//        log.debug("Decrypted: " + raw);
        
    }


    public static void test(String[] args) throws Exception {
        SecureURL secureURL = SecureURL.getInstance();
        byte[] encryptionBytes = null;

        String input = "/url/folder/file-name%20V201.txt";
        log.debug("Input: " + input);

        encryptionBytes = secureURL.encrypt(input);

        log.debug("Encrypted URL: Length (" + encryptionBytes.length + ")");
        for (byte encryptedByte : encryptionBytes) {
            log.debug(encryptedByte);
        }
        log.debug("");

//        Hex hex = new Hex();
//        byte[] hexaBytes = hex.encode(encryptionBytes);
//        log.debug("Converted to Hexa representation");
//        log.debug("Encrypted URL: Length (" + hexaBytes.length + ")");
//        for (byte encryptedByte : hexaBytes) {
//            System.out.print(encryptedByte);
//        }
//        log.debug(hex.toString());



        String encryptedHexaStr = new String(Hex.encodeHex(encryptionBytes));
        log.debug("encrypted Hexa String: " + encryptedHexaStr);

//        byte[] revertedByptes = binCovStr.getBytes();
        byte[] totalReverted = new Hex().decode(encryptedHexaStr.getBytes());
        for (byte encryptedByte : totalReverted) {
            System.out.print(encryptedByte);
        }
        log.debug(
                "Recovered: " + secureURL.decrypt(totalReverted));

        secureURL.testDecode();
    }

    // Used to generate key for testing purpose
    private void generateKey() {
        String encodedKey = null;
        try {
            key = KeyGenerator.getInstance(algorithm).generateKey();
            log.debug("key length: " + key.getEncoded().length);
            encodedKey = new String(Hex.encodeHex(key.getEncoded()));
            log.debug(encodedKey);
            log.debug("Size: " + encodedKey.length());
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
    }

    private byte[] encrypt(String input)
            throws InvalidKeyException,
            BadPaddingException,
            IllegalBlockSizeException, UnsupportedEncodingException {
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] inputBytes = input.getBytes("UTF-8");
        return cipher.doFinal(inputBytes);
    }

    private String decrypt(byte[] encryptionBytes)
            throws InvalidKeyException,
            BadPaddingException,
            IllegalBlockSizeException, UnsupportedEncodingException {
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] recoveredBytes =
                cipher.doFinal(encryptionBytes);
        String recovered =
                new String(recoveredBytes, "UTF-8");
        return recovered;
    }
}
