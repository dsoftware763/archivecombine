/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.virtualcode.common;

/**
 *
 * @author AtiqurRehman
 */
public class FileTypeMapper{
        private String extention;
        private String typeText;
        private String iconName;

        public FileTypeMapper(){
            this.extention="";
            this.typeText="Unknown Document";
            this.iconName="unknown.gif";
        }
        public FileTypeMapper(String ext, String typeTxt,String icnName){
            this.extention=ext;
            this.typeText=typeTxt;
            this.iconName=icnName;
        }

        /**
         * @return the extention
         */
        public String getExtention() {
            return extention;
        }

        /**
         * @param extention the extention to set
         */
        public void setExtention(String extention) {
            this.extention = extention;
        }

        /**
         * @return the typeText
         */
        public String getTypeText() {
            return typeText;
        }

        /**
         * @param typeText the typeText to set
         */
        public void setTypeText(String typeText) {
            this.typeText = typeText;
        }

        /**
         * @return the iconName
         */
        public String getIconName() {
            return iconName;
        }

        /**
         * @param iconName the iconName to set
         */
        public void setIconName(String iconName) {
            this.iconName = iconName;
        }

    }