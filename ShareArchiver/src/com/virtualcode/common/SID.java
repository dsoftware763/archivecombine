/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.common;

/**
 *
 * @author AtiqurRehman
 */
public class SID {
    
    private String sid;
    private String description;
    
    //User or Group
    
    private boolean userSid = false;
    
	public SID(String sid,String desc){
        this.sid=sid;
        this.description=desc;
    }
	
	public SID(String sid,String desc, boolean userSid){
        this.sid=sid;
        this.description=desc;
        this.userSid = userSid;
    }

    /**
     * @return the sid
     */
    public String getSid() {
        return sid;
    }

    /**
     * @param sid the sid to set
     */
    public void setSid(String sid) {
        this.sid = sid;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    public static void main(String[] ar){
        System.out.println("&nbsp;");
    }
    public boolean isUserSid() {
		return userSid;
	}

	public void setUserSid(boolean userSid) {
		this.userSid = userSid;
	}
    @Override
    public boolean equals(Object o){
     //   System.out.println("Equals Calledddd");
        return (this.sid.equalsIgnoreCase(((SID)o).getSid()));
    }
}
