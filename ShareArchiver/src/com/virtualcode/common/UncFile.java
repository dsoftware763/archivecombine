package com.virtualcode.common;

import java.io.IOException;
import java.io.InputStream;

import javax.jcr.RepositoryException;

public class UncFile implements javax.jcr.Binary{
	private InputStream is;
	private long contentLength;
	private String name;
	
	public UncFile(InputStream is, long contentLength, String name){
		this.is = is;
		this.contentLength = contentLength;
		this.name = name;
	}
	
	
	public InputStream getIs() {
		return is;
	}
	public void setIs(InputStream is) {
		this.is = is;
	}

	public void setContentLength(int contentLength) {
		this.contentLength = contentLength;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public long getSize() throws RepositoryException {
		// TODO Auto-generated method stub
		return contentLength;
	}
	@Override
	public InputStream getStream() throws RepositoryException {
		// TODO Auto-generated method stub
		return is;
	}
	@Override
	public int read(byte[] arg0, long arg1) throws IOException,
			RepositoryException {
		// TODO Auto-generated method stub
		return 0;
	}

}
