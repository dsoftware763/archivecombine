/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.virtualcode.common;

import javax.jcr.Property;

/**
*
* @author AtiqurRehman
*/
public interface CustomProperty extends Property{
	    
	    static final String VC_ACCESSED_ON  = "vc:accessedOn";
	    static final String VC_SP_URL       = "vc:spURL";
	    static final String VC_DOC_URL      = "vc:docURL";
	    static final String VC_DOC_VERSION  = "vc:docVersion";
	    static final String VC_CREATION_DATE = "vc:creationDate";
	    static final String VC_LAST_MODIFIED_BY_USER = "vc:lastModifiedByUser";

	    static final String VC_ALLOWED_SIDS     = "vc:allowedSIDs";
	    static final String VC_DENIED_SIDS      = "vc:deniedSIDs";
	    static final String VC_S_ALLOWED_SIDS   = "vc:shareAllowedSIDs";
	    static final String VC_S_DENIED_SIDS    = "vc:shareDeniedSIDs";
	    
	    static final String VC_TOTAL_FOLDERS = "vc:totalFolders";
	    static final String VC_TOTAL_FILES   = "vc:totalFiles";
	    static final String VC_FS_DOCS       = "vc:fsDocuments";
	    static final String VC_SP_DOCS       = "vc:spDocuments";
	    static final String VC_USED_SPACE    = "vc:usedSpace";
	    static final String VC_LINKED_TO     = "vc:linkedToNode";
	    
	    static final String VC_NEXT_VERSIONS    = "vc:itsNextVersions";
	    static final String VC_VERSION_OF       = "vc:versionOf";
	    static final String VC_DOC_TAGS          = "vc:docTags";
	    
	    static final String VC_FILE_SIZE	= "vc:fileSize";
	    
	    static final String VC_EMAIL_USER_LIST = "vc:emailUserList";
	    static final String VC_ACCESS_LOGS 	= "vc:accessLogs";
	    
	    /*  User related properties  */
	    static final String VC_USER_NAME  ="vc:userName";
	    static final String VC_USER_IP  ="vc:userIP";
	    
	    static final String VC_USER_LOGIN_TIME  ="vc:userLoginTime";
	    static final String VC_USER_LOGOUT_TIME  ="vc:userLogoutTime";
	    static final String VC_USER_SEARCH  ="vc:userSearch";
	    static final String VC_USER_SEARCH_L_INDEX  ="vc:userSearchLIndex";
	   	    
	    static final String VC_USER_ACCESSED_DOCS  ="vc:userAccessedDocs";
	    static final String VC_USER_ACCESSED_DOCS_COUNT  ="vc:userAccessedDocsCount";
	    static final String VC_USER_ACCESSED_DOCS_L_INDEX  ="vc:userAccessedDocsLIndex";
	    
	    static final String VC_USER_DOWNLOADED_DOCS  ="vc:userDownloadedDocs";
	    static final String VC_USER_DOWNLOADED_DOCS_COUNT  ="vc:userDownloadedDocsCount";
	    static final String VC_USER_DOWNLOADED_DOCS_L_INDEX  ="vc:userDownloadedDocsLIndex";
	    
	    static final String VC_SP_DOC_CONTENT_TYPE  ="vc:spDocContentType";
	    
	    static final String VC_ARCHIVING_MODE = "vc:archivingMode";
	    
	    static final String VC_ARCHIVE_DATE = "vc:archiveDate";	  
	    
	    static final String VC_IS_REMOVED	=	"vc:isRemoved";
	    static final String VC_LINK_NODES	=	"vc:linkNodes";
//	    static final String VC_IS_STUBBED 	= 	"vc:isStubbed";
	    static final String VC_PRINTER_NAME = 	"vc:printerName";	    
	    static final String VC_MACHINE_NAME = 	"vc:machineName";
	    static final String VC_TOTAL_PAGES 	= 	"vc:totalPages";

}
