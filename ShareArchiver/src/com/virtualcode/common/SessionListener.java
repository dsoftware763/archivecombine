package com.virtualcode.common;

import java.io.File;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.virtualcode.repository.UserActivityService;
import com.virtualcode.services.ServiceManager;

public class SessionListener  implements HttpSessionListener{

	@Override
	public void sessionCreated(HttpSessionEvent event) {
		
		System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^ Session Created ^^^^^^^^^^^^^^^^^^^^^^^^^^^");
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^ Session Destroyed ^^^^^^^^^^^^^^^^^^^^^^^^^^^"+event.getSession().getId());
		String userName=null;
		
		try{
			 String httpSessionId=event.getSession().getId();
			 String contextPath=event.getSession().getServletContext().getRealPath("/");
			 clearCacheFiles(httpSessionId,contextPath);
			 
 			if(event.getSession().getAttribute(VCSConstants.SESSION_USER_NAME)!=null)
		        userName=event.getSession().getAttribute(VCSConstants.SESSION_USER_NAME).toString();
			if(userName==null){return;}
		  
	        String userIP="";//event.getSession().getAttribute(VCSConstants.SESSION_USER_IP).toString();
			
			 WebApplicationContext webAppContext=WebApplicationContextUtils.getWebApplicationContext(event.getSession().getServletContext());
			
	         ServiceManager serviceManager=(ServiceManager)webAppContext.getBean("serviceManager");
	         UserActivityService userActivityService=serviceManager.getUserActivityService();
	         userActivityService.userLogout(userName,userIP,httpSessionId,event.getSession().getCreationTime());
	     
		}catch(Exception ex){
			ex.printStackTrace();
		}	
	}
	
	
	private void clearCacheFiles(String sessionId,String contextPath){
		try{
		    File outputDir=new File(contextPath+"/streams/"+sessionId);
        
			if(outputDir.exists()){
			  String[] paths=outputDir.list();
	     	  for(int i=0;i<paths.length;i++){
	     		  new File(outputDir.getPath()+"/"+paths[i]).delete();
	     	  }
	     	  outputDir.delete();
	        }
		
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}
	
	

}
