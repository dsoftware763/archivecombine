package com.virtualcode.common;

public class VCSConstants {
	
	public static final int SEARCH_MAX_RESULTS=2000;
	
	public static final String SEACH_DATE_FORMAT="dd-MM-yyyy";
	
	public static final String DD_INVALID_CLIENT = "INVALID_CLIENT";
	
    public static final String DD_ERROR = "ERROR";
    
    public static final String DD_NOT_EXISTS = "NOT_EXISTS";
    
    public final static String AGENT_TYPE = "AgentType";
    
    public final static String AGENT_NAME = "AgentName";
    
    public final static String SECURE_INFO = "secureInfo";
    
    public final static String SECURE_GET_SERVLET_PATH = "/SecureGet/";
    
    public static final String SRC_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";//to support 24hrs Format
    
    public final static String SESSION_USER_NAME = "username";
    
    public static final String SESSION_USER_IP="userIP";
    
    public final static String SESSION_USER_ROLE_NAME = "userrolename";
    
    public final static String SESSION_USER_AUTH_TYPE = "userauthtype";
    
    public final static String ROLE_ADMIN = "ADMINISTRATOR";
    
    public final static String ROLE_PRIVILEGED = "PRIVILEGED";
    
    public final static String ROLE_EMAIL = "EMAIL";
    
    public final static String AUTH_DB_ONLY = "1";
    
    public final static String AUTH_DB_FIRST_WITH_AD = "2";
    
    public final static String AUTH_AD_FIRST_WITH_DB = "3";
    
    public final static String JOB_STATUS_PENDING = "PENDING";
    
    public final static String JOB_STATUS_PROCESSING = "PROCESSING";
    
    public final static String JOB_STATUS_FAILED = "FAILED";
    
    public final static String JOB_STATUS_COMPLETED = "COMPLETED";
    
    public final static String JOB_STATUS_CANCEL = "CANCEL";
    
    public final static String JOB_STATUS_CANCELED = "CANCELED";
    
    public final static String JOB_STATUS_PAUSE = "PAUSE";
    
    public final static String JOB_STATUS_PAUSED = "PAUSED";
    
    public final static String JOB_STATUS_RUN = "RUN";
    
    public final static String STUB_UUID="uuid";
    
    public final static String ARCHIVE = "ARCHIVE";
    
    public final static String ARCHIVE_AND_STUB = "STUB";

    public final static String ARCHIVE_WITHOUT_STUB = "WITHOUTSTUB";
    
    public final static String EXPORT = "EXPORT";
    
    public final static String EXPORT_STUB_ONLY = "EXPORTSTUB";
    
    public final static String DYNAMIC_ARCHIVING = "DYNARCH";
    
    public final static String INDEXING = "INDEXING";
    
    public final static String PRINT_ARCHIVING = "PRINTARCH";
    
    public final static String ACTIVE_ARCHIVING = "ACTIVEARCH";
    
    public final static String ARCHIVE_MODE_NORMAL = "normal";   
    
    public final static long ONE_MINUTE_IN_MILLIS = 60000;
    
    public final static long ONE_DAY_IN_MILLIS = 86400000;
	

}
