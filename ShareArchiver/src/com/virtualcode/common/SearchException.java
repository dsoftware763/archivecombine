/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.virtualcode.common;

/**
*
* @author AtiqurRehman
*/
public class SearchException  extends Exception{

    private String message;

    @Override
    public String toString() {
        return message;
    }
    public SearchException(){
        super();
    }
    public SearchException(String message){
        super(message);
        this.message=message;

    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage(){
        return this.message;
    }

}
