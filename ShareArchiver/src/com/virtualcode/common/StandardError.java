/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.virtualcode.common;

/**
*
* @author AtiqurRehman
*/
public class StandardError {

    public static String ERROR_KEY="SEARCH_ERROR";
    public static String LOGIN_ERROR_KEY="LOGIN_ERROR_KEY";
    public static String NO_RESULTS="Your search did not match any documents.";
    public static String EMPTY_QUERY="Advanced Search requires atleast one  parameter for searching, Please check inputs and try again.";
    public static String DATE_PARSING_ERROR="Invalid values for Date fields, Please check inputs try again";
    public static String INTERNAL_ERROR="Internal Server Error, Please report to system administrator and try again later.";
    public static String INVALID_CREDENTIAL="Invalid username/password, Please try again.";


}
