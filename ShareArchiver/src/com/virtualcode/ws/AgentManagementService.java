package com.virtualcode.ws;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.soap.MTOM;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.virtualcode.services.ErrorCodeService;
import com.virtualcode.services.JobService;
import com.virtualcode.services.JobStatisticsService;
import com.virtualcode.services.JobStatusService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.vo.Agent;
import com.virtualcode.vo.Job;
import com.virtualcode.vo.JobStatistics;
import com.virtualcode.vo.JobStatus;
import com.sun.xml.ws.developer.StreamingAttachment;

@StreamingAttachment(parseEagerly = true, memoryThreshold = 40000L)
@javax.jws.WebService(targetNamespace = "http://ws.virtualcode.com/", serviceName = "AgentManagementService", portName = "AgentManagementServicePort" )

@MTOM
public class AgentManagementService {

       private static boolean transactional = true;

	    // actionType values on GSP pages
	    private String  EVALUATING  =   "EVALUATING";
	    private String  RUNNING     =   "RUNNING";
	    private String  COMPLETED   =   "COMPLETED";

	    // actionType values in DB
	    private String  ARCHIVE_DB     =   "ARCHIVE";
		private String  ARCHIVE_AND_STUB_DB  =   "STUB";
		private String  ARCHIVE_WITHOUT_STUB_DB  =   "WITHOUTSTUB";
		private String  EVALUATE_ONLY_DB    =   "EVALUATE";
		private String  EXPORT_DB       =   "EXPORT";
		
		public static Map<String, Long> agentLastCallMp;
		
		@Resource
		private WebServiceContext context; 		
	    		
		@WebMethod(operationName = "getJobListForProcessing" )	
		
	    public  byte[] getJobListForProcessing(@WebParam(name = "agentName") String agentName, @WebParam(name = "actionType")String actionType) {
	    	
	    	 ServletContext sc = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
	         
	    	 jobService=getJobService(sc);	    	 
//		    	if(Agent.agentLastCallMap==null)
//		    		Agent.agentLastCallMap = new HashMap<String, Long>();
//		    	
//		    	Agent.agentLastCallMap.put(agentName, System.currentTimeMillis());
	    	 //modified by Jawwad to call backend service method
	    	 byte[] jobDTOBytesArray = null;
	    	 jobDTOBytesArray = agentManagementService.getJobListForProcessing(agentName, actionType);
	    	 
	    	 //end
	    	/* List<Job> jobsList;
	    	 
	    	 if(actionType.equalsIgnoreCase("EXPORT")){
	    	     jobsList=jobService.getAgentExportJobs(agentName);
	    	 }else{
	    		 jobsList=jobService.getAgentImportJobs(agentName); 
	    	 }
	    	
	    	if(jobsList==null || jobsList.size()==0){
	    		return null;
	    	}
	   
            
            Job job;
            Agent agent;
            Policy policy;
           
            List<Job> resultsList=new ArrayList<Job>();
            for(int i=0;i<jobsList.size();i++){
            	
            	job=jobsList.get(i);            	
            	policy=job.getPolicy();
            	if(policy!=null){
            		System.out.println("Policy name "+policy.getName());
            		Iterator<DocumentType> ite=policy.getPolicyDocumentTypes().iterator();
            		
	   				 DocumentType selectedDocumentType;	   					   				
	   				 System.out.println("Setting selected document types");
	   				 Set<DocumentType> docTypesSet=new HashSet<DocumentType>();
	   				 
	   				 while(ite!=null && ite.hasNext()){
	   					selectedDocumentType=(DocumentType)ite.next();
	   					DocumentCategory docCategory=selectedDocumentType.getDocumentCategory();
	   					selectedDocumentType.setDocumentCategory(docCategory);
	   					docTypesSet.add(selectedDocumentType);	
	   				 }
	   					
	   				 job.setJobDocuments(docTypesSet);
	   				
            	}
            	
            	agent=job.getAgent();
            	System.out.println("Agent name "+agent.getName());            	          	
            	
            	Iterator<JobSpDocLib> docLibsIterator=job.getJobSpDocLibs().iterator();
        		Iterator<JobSpDocLibExcludedPath> exPathIterator;
        		
        		Set<JobSpDocLib> jobSpDocLibSet=new HashSet<JobSpDocLib>();
        		
        		while(docLibsIterator!=null && docLibsIterator.hasNext()){
        			JobSpDocLib jobSpDocLib=(JobSpDocLib)docLibsIterator.next();
        			exPathIterator=jobSpDocLib.getJobSpDocLibExcludedPaths().iterator();
        			Set<JobSpDocLibExcludedPath> exPathSet=new HashSet<JobSpDocLibExcludedPath>();
        			while(exPathIterator!=null && exPathIterator.hasNext()){				
        				JobSpDocLibExcludedPath exPath=(JobSpDocLibExcludedPath)exPathIterator.next();
        				exPathSet.add(exPath);
        			}
        			jobSpDocLib.setJobSpDocLibExcludedPaths(exPathSet);
        			jobSpDocLibSet.add(jobSpDocLib);
        		}
        		
        		job.setJobSpDocLibs(jobSpDocLibSet);        		
            	
            	resultsList.add(job);
            }
            
            byte[] jobDTOBytesArray = null;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            try {
                ObjectOutputStream oos = new ObjectOutputStream(bos);
                oos.writeObject(resultsList);
                oos.flush();
                oos.close();
                bos.close();
                jobDTOBytesArray = bos.toByteArray ();
                //            println "conversion completed"
            }catch(Exception e){
                System.out.println( "********************** EXCEPTION **************************");
                System.out.println("Exception while converting jobList to byte[]");
                System.out.println("***********************************************************");
                return null;
            }*/
            //        println "process successfully completed"
            return jobDTOBytesArray; 
         	    	
	    }
		
		
		
		public  List<com.virtualcode.agent.das.archivePolicy.dto.Job> getJobListObjects(@WebParam(name = "agentName") String agentName, @WebParam(name = "actionType")String actionType) {
	    	
//	    	if(Agent.agentLastCallMap==null)
//	    		Agent.agentLastCallMap = new HashMap<String, Long>();
//	    	
//	    	Agent.agentLastCallMap.put(agentName, System.currentTimeMillis());
	    	 ServletContext sc = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
	         
	    	 jobService=getJobService(sc);
	    	 
	    	return agentManagementService.getJobListObjects(agentName, actionType);
	    	 
	    	
        	    	
	    }
		
		
		@WebMethod(operationName = "executionStarted")
	    public String executionStarted(@WebParam(name = "jobID") int jobID){
			
			String executionID=null;
			executionID = agentManagementService.executionStarted(jobID);
			/*Job jobInstance;
			ErrorCode errorCode=null;
			String executionID=null;
			JobStatus previousJobStatusInstance=null;
			Integer previousJobStatusID=null;
			  try{
		            jobInstance =   jobService.getJobById(jobID);
		            errorCode  =   errorCodeService.getById(0);

		        }catch(Exception e){
		            System.out.println("********************** EXCEPTION **************************");
		            System.out.println( "Exception while initializing instances in executionStarted() ... "+e);		            
		            System.out.println("***********************************************************");
		            return null;
		        }

		        //  initialize fields
		        try{
		            executionID                 =   generateExecutionID(jobID);
		            previousJobStatusInstance   =   getPreviousJobStatusInstance(jobInstance);
		            previousJobStatusID         =   previousJobStatusInstance.getId();
		            
		        }catch(Exception e){
		        	e.printStackTrace();
		        	System.out.println( "********************** EXCEPTION **************************");
		        	System.out.println( "Exception while initializing fields in executionStarted() ... "+e);
		        	System.out.println( "***********************************************************");

		            previousJobStatusID =   -1;
		        }

                String status;
		        //  calculate JobStatus.status
		        if (jobInstance.getActionType().equals(ARCHIVE_DB) || jobInstance.getActionType().equals(ARCHIVE_WITHOUT_STUB)){
		            status  =   "RUNNING";
		        }else if(jobInstance.getActionType().equals(ARCHIVE_AND_STUB_DB)){
		            status  =   "RUNNING";
		        }else if (jobInstance.getActionType().equals(EVALUATE_ONLY_DB)){
		            status  =   "EVALUATING";
		        }else if (jobInstance.getActionType().equals(EXPORT_DB)){
		            status  =   "RUNNING";
		        
		        }else{
		            return null;
		        }

		        
		        JobStatus jobStatus=new JobStatus();
		        
		        jobStatus.setExecutionId(executionID);
		        jobStatus.setErrorCode(errorCode);
                jobStatus.setJob(jobInstance);
                jobStatus.setStatus(status);
                jobStatus.setDescription("running ... > job.actionType: " + jobInstance.getActionType());
                jobStatus.setIsCurrent(true);
                jobStatusService.saveJobStatus(jobStatus);
                
                updateIsCurrent(previousJobStatusID);*/
                
                return executionID;

		}
		
		
		@WebMethod(operationName = "updateHotStatistics")
	    public boolean updateHotStatistics(@WebParam(name = "jobID") int jobID, @WebParam(name = "executionID") String executionID, @WebParam(name = "errorCodeID") int errorCodeID, @WebParam(name="jobStatistics")com.virtualcode.agent.das.archivePolicy.dto.JobStatistics jobStatistics){
			
			return agentManagementService.updateHotStatistics(jobID, executionID, errorCodeID, jobStatistics);
		}
		
		@WebMethod(operationName = "getCancelJobs")
		public List<Integer> getCancelJobs(){
			
			return agentManagementService.getCancelJobs();
		}
		@WebMethod(operationName = "updateStatus")
		public String updateStatus(@WebParam(name = "jobID")int jobID, @WebParam(name = "status")String status){
			return agentManagementService.updateStatus(jobID, status);
		}
		
		
		 private JobStatus getPreviousJobStatusInstance(Job jobInstance) throws NullPointerException{
				      
			      JobStatus jobStatusInstance   =   null;
                  String jobId=jobInstance.getId()+"";
			      List<JobStatus> jobStatusList=jobStatusService.getJobStatusByJob(jobId);

			        if(jobStatusList.size() >   0){
			            jobStatusInstance   =   (JobStatus) jobStatusList.get(0);
			            return jobStatusInstance;
			        
			        }else{
			            
			            throw new NullPointerException();
			        }
	     }
		 
		private boolean updateIsCurrent(int jobStatusID){
		      
		        JobStatus jobStatusInstance = jobStatusService.getJobStatusById(jobStatusID);
		        jobStatusInstance.setCurrent(1);
		        try{
		            jobStatusService.saveJobStatus(jobStatusInstance);
		            return true;
		        }catch(Exception e){
		            System.out.println("********************** EXCEPTION **************************");
		            System.out.println("Exception while updateIsCurrent() ...");		          
		            System.out.println( "***********************************************************");
		            return false;
		        }
		   
		}

		
		
		private String generateExecutionID(int jobID){
	      
	        int NO_OF_LAST_DIGITS_OF_JOB_ID=4;
	        int NO_OF_LAST_DIGITS=2;//same for day,month,hour and minute
	        int NO_OF_LAST_DIGHTS_FOR_YEAR=4;
	        String executionID;//format: JJJJDDMMYYYYHHMM
	        String lastFourDigitsOfJobID=getLastDigitsOfIntAsString(jobID,NO_OF_LAST_DIGITS_OF_JOB_ID);

	        Calendar calendar = new GregorianCalendar();

	        //converting one digit to two digit format e.g. converting "4" to "04"
	        //for generating proper executionID with 16 digits
	        String day = getLastDigitsOfIntAsString(calendar.get(Calendar.DATE),NO_OF_LAST_DIGITS);
	        String month = getLastDigitsOfIntAsString(calendar.get(Calendar.MONTH),NO_OF_LAST_DIGITS);
	        String  year = getLastDigitsOfIntAsString(calendar.get(Calendar.YEAR),NO_OF_LAST_DIGHTS_FOR_YEAR);
	        String  hour = getLastDigitsOfIntAsString(calendar.get(Calendar.HOUR),NO_OF_LAST_DIGITS);
	        String  minute = getLastDigitsOfIntAsString(calendar.get(Calendar.MINUTE),NO_OF_LAST_DIGITS);

	        executionID = lastFourDigitsOfJobID + day + month + year + hour + minute;
	   	        
	        System.out.println("executionID: " + executionID);

	        return executionID;
	    }
		
		private String getLastDigitsOfIntAsString(int jobID, int noOfLastDigits){
	       
	        String str= "0000"+jobID;
	        String strLastDigits= str.substring(str.length()-noOfLastDigits,str.length());
	        return strLastDigits;
	    }
		
		
		
		 @WebMethod(operationName="executionCompleted2")
		 public boolean executionCompleted2(@WebParam(name = "jobID") int jobID, @WebParam(name = "executionID") String executionID, @WebParam(name = "errorCodeID") int errorCodeID, @WebParam(name="jobStatistics")com.virtualcode.agent.das.archivePolicy.dto.JobStatistics jobStatistics){
		
		        return agentManagementService.executionCompleted(jobID, executionID, errorCodeID, jobStatistics);
		        
		    }
		 
		 @WebMethod(operationName="incrProActiveArchCount")
		 public boolean incrProActiveArchCount(@WebParam(name = "jobId") Integer jobId, @WebParam(name = "actionType") String actionType,@WebParam(name = "documentsCount") Integer documentsCount ){
             ServletContext sc = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
	         jobService=getJobService(sc);
		     String executionId=generateExecutionID(jobId);
		     return agentManagementService.incrProActiveArchCount(jobId,executionId,actionType,documentsCount);
		        
		    }
		 
		 
		 
		 private JobStatistics generateDummyJobStatisticsInstance(int jobID){
		        //        println "---------------------------------------------------------------"
		        //        println "generating dummy JobStatistics instance"

			    JobStatistics jobStatistics=new JobStatistics();
			    jobStatistics.setExecutionId(generateExecutionID(jobID));
			    jobStatistics.setExecutionId(generateExecutionID(jobID));
			    jobStatistics.setJobStartTime(getCurrentDateTime());
			    jobStatistics.setJobEndTime(getCurrentDateTime());
			    jobStatistics.setTotalEvaluated(0l);
			    jobStatistics.setTotalDuplicated(0l);
			    jobStatistics.setTotalFreshArchived(0l);
			    jobStatistics.setAlreadyArchived(0l);
			    jobStatistics.setTotalStubbed(0l);
			    jobStatistics.setTotalFailedArchiving(0l);
			    
			    jobStatistics.setTotalFailedEvaluating(0l);
			    jobStatistics.setTotalFailedStubbing(0l);
			    jobStatistics.setTotalFailedDeduplication(0l);
			    
			    jobStatistics.setSkippedProcessfileSkippedReadOnly(0l);
			    jobStatistics.setSkippedProcessfileSkippedHiddenlected(0l);
			    jobStatistics.setSkippedProcessfileSkippedTooLarge(0l);
			    
			    jobStatistics.setSkippedProcessfileSkippedTemporary(0l);
			    jobStatistics.setSkippedProcessfileSkippedSymbollink(0l);
			    jobStatistics.setSkippedProcessfileSkippedIsdir(0l);
			    
			    jobStatistics.setSkippedProcessfileSkippedMismatchAge(0l);
			    jobStatistics.setSkippedProcessfileSkippedMismatchLastaccesstime(0l);
			    jobStatistics.setSkippedProcessfileSkippedMismatchLastmodifiedtime(0l);
			    jobStatistics.setSkippedProcessfileSkippedMismatchExtension(0l);
			    
		       

		        return jobStatistics;
		    }
		 
		 private Timestamp getCurrentDateTime() {
		       
		        Calendar cal = new GregorianCalendar();
		        java.sql.Timestamp timeStampDate=null;
		        SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");

		        SimpleDateFormat dateFormat = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
		        Date convertedDate=null;
				try {
					convertedDate = dateFormat.parse(sdf.format(cal.getTime()));
					timeStampDate = new 	Timestamp(convertedDate.getTime());
				} catch (ParseException e) {				
					e.printStackTrace();
				}
		        return timeStampDate;
		    }

	    
	    
	    private ServiceManager serviceManager;
	    private JobService jobService;
	    private ErrorCodeService errorCodeService;
	    private JobStatusService jobStatusService;
	    private JobStatisticsService jobStatisticsService;
	    private com.virtualcode.services.AgentManagementService agentManagementService;
	    
	    private JobService getJobService(ServletContext sc){
	    	this.serviceManager=(ServiceManager)getWebAppContext(sc).getBean("serviceManager");
	    	this.jobService=serviceManager.getJobService();
	    	this.errorCodeService=serviceManager.getErrorCodeService();
	    	this.jobStatusService=serviceManager.getJobStatusService();
	    	this.jobStatisticsService=serviceManager.getJobStatisticsService();
	    	this.agentManagementService = serviceManager.getAgentManagementService();
	        return jobService;
	    }	    
	    
	    private WebApplicationContext getWebAppContext(ServletContext sc){
	    	
	    	WebApplicationContext webAppContext=WebApplicationContextUtils.getWebApplicationContext(sc);
	    	return webAppContext;

	    }

}
