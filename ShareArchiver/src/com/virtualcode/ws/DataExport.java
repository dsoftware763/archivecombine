/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.ws;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.activation.DataHandler;
import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.soap.MTOM;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.virtualcode.cifs.Document;
import com.virtualcode.repository.UploadDocumentService;
import com.virtualcode.services.ServiceManager;
import com.sun.xml.ws.developer.StreamingAttachment;
/**
 *
 * @author SE6
 */
@StreamingAttachment(parseEagerly = true, memoryThreshold = 40000L)
@WebService(portName = "dataExport")
@MTOM
public class DataExport {

    static final String SECURE_INFO = "secureInfo";
    static final Logger log = Logger.getLogger(DataExport.class);   
    @Resource
    private WebServiceContext context;

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getAllChildren")
    public List<Document> getAllChildren(@WebParam(name = "parent") String parent, @WebParam(name = "layers") String layersList) {
        //TODO write your implementation code here:
        //REMOVED synchronized (context) {
            log.info("getAllChilds: Started");
            log.debug("Base-64 encoded Path: " + parent);

            ServletContext sc = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
           
//          repoStats.csv
            ArrayList<Document> children = null;
//        String dataHandlerClassName = null;

            try {
            	uploadDocumentService = getUploadDocumentService(sc);

                log.debug("Encoded File Path: " + parent);
//            parent = SecureURL.getInstance().getDecryptedURL(parent);
//            log.debug("File Path: " + parent);
//                HashMap layerAttribs = StringUtils.getLayerAttribs(layersList, SECURE_INFO);

                children = uploadDocumentService.getAllChildren(parent, layersList);

            } catch (Exception ex) {
                log.error("Exception in WebService Method EX: " + ex);
//                ex.printStackTrace();
                throw new WebServiceException(ex);
            } finally {
//            try {
//                if (in != null) {
//                    in.close();
//                }
//            } catch (IOException ioe) {
//                log.error("Input Stream for the file was not created. Ex: " + ioe.getMessage());
//            }
//            try {
//                if (sdh != null && dataHandlerClassName.equals("com.sun.xml.ws.encoding.MIMEPartStreamingDataHandler")) {
//                    ((StreamingDataHandler) sdh).close();
//                }
//            } catch (IOException sdhEx) {
//                log.error("SDH was not created. Ex: " + sdhEx.getMessage());
//            }
            }
            return children;
        ////REMOVED }//ending Sync Block

    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "downloadContent")
//    not used now. All the work is done in the above method
//    @XmlMimeType("application/octet-stream")
//    public DataHandler downloadContent(@WebParam(name = "path") String path, @WebParam(name = "layers") String layersList) {
    public String downloadContent(@WebParam(name = "path") String path, @WebParam(name = "layers") String layersList) {
        //TODO write your implementation code here:
    	//REMOVED  synchronized (context) {
            log.info("downloadContent: Started");
            log.debug("Base-64 encoded Path: " + path);

            ServletContext sc = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
           

            InputStream in = null;
            DataHandler dh = null;
            String url="";

            byte[] data = null;
//            Document doc = new Document();


//        String dataHandlerClassName = null;

            try {
                uploadDocumentService=getUploadDocumentService(sc);

                log.debug("Encoded File Path: " + path);
//            parent = SecureURL.getInstance().getDecryptedURL(parent);
//            log.debug("File Path: " + parent);
//                HashMap layerAttribs = StringUtils.getLayerAttribs(layersList, SECURE_INFO);

                in = null;//
                url = uploadDocumentService.downloadDocument(path, layersList);
//                data = "test string".getBytes();
//                doc.setData(data);
//                File file = new File("E:\\apache-maven-3.0.3\\lib\\aether-connector-wagon-1.11.jar");
//                FileDataSource fs = new FileDataSource(file);
//                DataHandler content = new DataHandler(fs);
//                dh = content; //new DataHandler(content);//, "application/octet-stream");
//                dh = new DataHandler(url, "application/octet-stream");



            } catch (Exception ex) {
                log.error("Exception in WebService Method EX: " + ex);
                throw new WebServiceException(ex);
            } finally {
//            try {
//                if (in != null) {
//                    in.close();
//                }
//            } catch (IOException ioe) {
//                log.error("Input Stream for the file was not created. Ex: " + ioe.getMessage());
//            }
//            try {
//                if (sdh != null && dataHandlerClassName.equals("com.sun.xml.ws.encoding.MIMEPartStreamingDataHandler")) {
//                    ((StreamingDataHandler) sdh).close();
//                }
//            } catch (IOException sdhEx) {
//                log.error("SDH was not created. Ex: " + sdhEx.getMessage());
//            }
            }
            return url;
            
          //REMOVED}//ending sync block here
    }
    
    
    private ServiceManager serviceManager;
    private UploadDocumentService uploadDocumentService;
    private UploadDocumentService getUploadDocumentService(ServletContext sc){
    	this.serviceManager=(ServiceManager)getWebAppContext(sc).getBean("serviceManager");
    	this.uploadDocumentService=serviceManager.getUploadDocumentService();
        return uploadDocumentService;
    }
    
    
    private WebApplicationContext getWebAppContext(ServletContext sc){    	
    	WebApplicationContext webAppContext=WebApplicationContextUtils.getWebApplicationContext(sc);
    	return webAppContext;

    }

    
    
    
}
