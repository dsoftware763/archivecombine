package com.virtualcode.ws;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.HashMap;

import javax.activation.DataHandler;
import javax.annotation.Resource;
import javax.jcr.Binary;
import javax.jcr.Node;
import javax.jcr.Repository;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import javax.jcr.ValueFactory;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.servlet.ServletContext;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.soap.MTOM;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sun.xml.ws.developer.StreamingDataHandler;
import com.sun.xml.ws.developer.StreamingAttachment;
import com.virtualcode.cifs.Acl;
import com.virtualcode.cifs.CifsUtil;
import com.virtualcode.repository.UploadDocumentService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.util.StringUtils;

/**
 *
 * @author Administrator
 */

@StreamingAttachment(parseEagerly = true, memoryThreshold = 40000L)
@javax.jws.WebService(targetNamespace = "http://ws.virtualcode.com/", serviceName = "DocumentManagement", portName = "DMSoapHttpPort")

@MTOM
public class DocumentManagement {

    static final String SECURE_INFO = "secureInfo";
    static final Logger log = Logger.getLogger(DocumentManagement.class);    
     
    @Resource
    private WebServiceContext context;

    /**
     * Web service operation
     */    
    @WebMethod(operationName = "uploadDocument")
    public String uploadDocument(@WebParam(name = "fileContextPath") String fileContextPath,
            @XmlMimeType("application/octet-stream") @WebParam(name = "content") DataHandler content,
            @WebParam(name = "layers") String layersList,
            @WebParam(name = "acl") Acl acl) {
        
    	//REMOVED synchronized (context){
        log.info("uploadDocument: Started");
        log.debug("Base-64 encoded Path: " + fileContextPath);
        log.debug("File Path(b64): " + fileContextPath);
        log.debug("LayersList in Request : "+layersList);
               
        ServletContext sc = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
      //streaming
        uploadDocumentService=getUploadDocumentService(sc);
        InputStream in = null;
        DataHandler sdh = null;
        String url = null;
        String dataHandlerClassName = null;
        
        try {
           
            log.debug("Encoded File Path: " + fileContextPath);
            fileContextPath = new String(org.apache.commons.codec.binary.Base64.decodeBase64(fileContextPath.getBytes()));
            log.debug("File Path: " + fileContextPath);

            String[] pathElements = StringUtils.getPathElements(fileContextPath);
            if (pathElements == null || pathElements.length < 1) {
                throw new WebServiceException("Context Path of the file could not be parsed");
            }            

            HashMap layerAttribs = StringUtils.getLayerAttribs(layersList, SECURE_INFO);
            
            HashMap aclAttribs   =   CifsUtil.getAclAttribs(acl);

            //String scPath = sc.getContextPath();
            //log.debug("ContextPath: " + scPath);

            dataHandlerClassName = content.getClass().getName();
            log.debug("DataHandler Class Name: " + dataHandlerClassName);

            sdh = content;
            in = sdh.getInputStream();
            url = uploadDocumentService.uploadDocument(pathElements, in, layerAttribs, aclAttribs);

        } catch (Exception ex) {
            log.error("Exception in WebService Method EX: "+ex);
            throw new WebServiceException(ex);
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ioe) {
                log.error("Input Stream for the file was not created. Ex: " + ioe.getMessage());
            }

            try {
                if (sdh != null && dataHandlerClassName.equals("com.sun.xml.ws.encoding.MIMEPartStreamingDataHandler")) {
                    ((StreamingDataHandler) sdh).close();
                }
            } catch (IOException sdhEx) {
                log.error("SDH was not created. Ex: " + sdhEx.getMessage());
            }
        }
        return url;
    	//REMOVED }//ending Sync Block
   
    }

    public static void uploadToRepository(Repository repository, String fileName, InputStream in) throws Exception {

        SimpleCredentials creds = new SimpleCredentials("username","password".toCharArray());
        try {
            Session session = repository.login(creds);

            Node root = session.getRootNode();
            ValueFactory valueFactory = session.getValueFactory();
            Binary binaryValue = valueFactory.createBinary(in);
            String path = root.getPath();
            log.debug("Root Path: " + path);
            log.debug("Root Name: " + root.getName());

            Node file = root.addNode(fileName, "nt:file");
            final Node resource = file.addNode("jcr:content", "nt:resource");
            resource.setProperty("jcr:data", binaryValue);
            String mimeType = URLConnection.guessContentTypeFromName(fileName);
            if (mimeType == null) {
                if (fileName.endsWith(".doc")) {
                    mimeType = "application/msword";
                } else if (fileName.endsWith(".xls")) {
                    mimeType = "application/vnd.ms-excel";
                } else if (fileName.endsWith(".ppt")) {
                    mimeType = "application/mspowerpoint";
                } else {
                    mimeType = "application/octet-stream";
                }
            }
            resource.setProperty("jcr:mimeType", mimeType);
            Calendar lastModified = Calendar.getInstance();
            lastModified.setTimeInMillis(System.currentTimeMillis());
            resource.setProperty("jcr:lastModified", lastModified);

            session.save();
            session.logout();
        } finally {
            log.error("Closing InputStream...");
            in.close();
        }
    }
    
    @WebMethod(operationName = "documentExistsByProperties")
    public String documentExistsByProperties(@WebParam(name = "fileContextPath") String fileContextPath,            
            @WebParam(name = "layers") String layersList) {
        
    	//REMOVED synchronized (context){
        log.info("uploadDocument: Started");
        log.debug("Base-64 encoded Path: " + fileContextPath);
        log.debug("File Path(b64): " + fileContextPath);
        log.debug("LayersList in Request : "+layersList);
               
        ServletContext sc = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
      //streaming
        uploadDocumentService=getUploadDocumentService(sc);
        InputStream in = null;
        DataHandler sdh = null;
        String url = null;
        String dataHandlerClassName = null;
        
//        boolean exists = false;
        
        try {
           
            log.debug("Encoded File Path: " + fileContextPath);
            fileContextPath = new String(org.apache.commons.codec.binary.Base64.decodeBase64(fileContextPath.getBytes()));
            log.debug("File Path: " + fileContextPath);

            String[] pathElements = StringUtils.getPathElements(fileContextPath);
            if (pathElements == null || pathElements.length < 1) {
                throw new WebServiceException("Context Path of the file could not be parsed");
            }            

            HashMap layerAttribs = StringUtils.getLayerAttribs(layersList, SECURE_INFO);
            
//            HashMap aclAttribs   =   CifsUtil.getAclAttribs(acl);

            //String scPath = sc.getContextPath();
            //log.debug("ContextPath: " + scPath);

//            sdh = content;
            in = null;//sdh.getInputStream();
            url = uploadDocumentService.documentExistsByProperties(fileContextPath, layerAttribs);
//            if(exists)
//            	url = "true";
//            else
//            	url = "false";

        } catch (Exception ex) {
            log.error("Exception in WebService Method EX: "+ex);
            throw new WebServiceException(ex);
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ioe) {
                log.error("Input Stream for the file was not created. Ex: " + ioe.getMessage());
            }

            try {
                if (sdh != null && dataHandlerClassName.equals("com.sun.xml.ws.encoding.MIMEPartStreamingDataHandler")) {
                    ((StreamingDataHandler) sdh).close();
                }
            } catch (IOException sdhEx) {
                log.error("SDH was not created. Ex: " + sdhEx.getMessage());
            }
        }
        return url;
    	//REMOVED }//ending Sync Block
   
    }
    
    private ServiceManager serviceManager;
    private UploadDocumentService uploadDocumentService;
    private UploadDocumentService getUploadDocumentService(ServletContext sc){
    	this.serviceManager=(ServiceManager)getWebAppContext(sc).getBean("serviceManager");
    	this.uploadDocumentService=serviceManager.getUploadDocumentService();
        return uploadDocumentService;
    }
    
    
    private WebApplicationContext getWebAppContext(ServletContext sc){
    	
    	WebApplicationContext webAppContext=WebApplicationContextUtils.getWebApplicationContext(sc);
    	return webAppContext;

    }

    /**
     * Web service operation
     * This WS was being used for temporary Agent.. Now its not required to receive HexStringDoc... /
    @WebMethod(operationName = "uploadHexDocument")
    public String uploadHexDocument(@WebParam(name = "fileContextPath") String fileContextPath, 
    @WebParam(name = "docContent") String docContent, 
    @WebParam(name = "layers") String layersList) {
                
   synchronized (context){
        log.info("uploadDocument: Started");
        log.debug("Base-64 encoded Path: " + fileContextPath);
        log.debug("File Path(b64): " + fileContextPath);
        log.debug("LayersList in Request : "+layersList);

        ServletContext sc = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
        Repository repository = RepositoryAccessServlet.getRepository(sc);

        InputStream in = null;
        DataHandler sdh = null;
        String url = null;
        String dataHandlerClassName = null;

        try {
            opManager = RepositoryOpManager.getOpManager(repository);

            log.debug("Encoded File Path: " + fileContextPath);
            fileContextPath = new String(org.apache.commons.codec.binary.Base64.decodeBase64(fileContextPath.getBytes()));
            log.debug("File Path: " + fileContextPath);

            String[] pathElements = StringUtils.getPathElements(fileContextPath);
            if (pathElements == null || pathElements.length < 1) {
                throw new WebServiceException("Context Path of the file could not be parsed");
            }            

            Hashtable layerAttribs = StringUtils.getLayerAttribs(layersList, SECURE_INFO);

            byte[]  docBytes    =   TypeCaster.hexStringToByteArray(docContent);
            in      =   new ByteArrayInputStream(docBytes);
            url     =   opManager.uploadDocument(pathElements, in, layerAttribs);

        } catch (Exception ex) {
            log.error("Exception in WebService Method EX: "+ex);
            throw new WebServiceException(ex);
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ioe) {
                log.error("Input Stream for the file was not created. Ex: " + ioe.getMessage());
            }

            try {
                if (sdh != null && dataHandlerClassName.equals("com.sun.xml.ws.encoding.MIMEPartStreamingDataHandler")) {
                    ((StreamingDataHandler) sdh).close();
                }
            } catch (IOException sdhEx) {
                log.error("SDH was not created. Ex: " + sdhEx.getMessage());
            }
        }
        return url;
    }//ending Sync Block
    } */
}
