package com.virtualcode.ws;

import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.soap.MTOM;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sun.xml.ws.developer.StreamingAttachment;
import com.virtualcode.services.PolicyService;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.vo.Policy;

@StreamingAttachment(parseEagerly = true, memoryThreshold = 40000L)
@javax.jws.WebService(targetNamespace = "http://ws.virtualcode.com/", serviceName = "ArchivingPoliciesService", portName = "ArchivingPoliciesService" )

@MTOM
public class ArchivingPoliciesWebService {
	
	@Resource
	private WebServiceContext context; 		
    		
	@WebMethod(operationName = "getAllPolicies" )	
	
    public List<Policy> getAllArchivingPolicies() {
    	
    	 ServletContext sc = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
         
    	 PolicyService policyService =getPolicyService(sc);
    	 List<Policy> policiesList=policyService.getAllPolicies();
    	 
    	 return policiesList;
    	 
	}
	
	 private ServiceManager serviceManager;
	
	 private PolicyService getPolicyService(ServletContext sc){
	    	this.serviceManager=(ServiceManager)getWebAppContext(sc).getBean("serviceManager");	    
	        return serviceManager.getPolicyService();
	    }	    
	
	 
    private WebApplicationContext getWebAppContext(ServletContext sc){
    	
    	WebApplicationContext webAppContext=WebApplicationContextUtils.getWebApplicationContext(sc);
    	return webAppContext;

    }

}
