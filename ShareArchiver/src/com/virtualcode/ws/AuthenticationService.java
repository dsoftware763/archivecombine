package com.virtualcode.ws;

import java.util.Iterator;
import java.util.List;

import javax.jcr.SimpleCredentials;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.servlet.http.HttpSession;

import org.apache.shiro.crypto.hash.Sha256Hash;

import com.virtualcode.common.VCSConstants;
import com.virtualcode.security.VirtualCodeAuthenticator;
import com.virtualcode.security.impl.ActiveDirAuthenticator;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.services.UserService;
import com.virtualcode.util.ResourcesUtil;
import com.virtualcode.util.SpringApplicationContext;
import com.virtualcode.vo.Role;
import com.virtualcode.vo.SystemCodeType;
import com.virtualcode.vo.User;

@javax.jws.WebService(targetNamespace = "http://ws.virtualcode.com/", serviceName = "AuthenticationService", portName = "AuthenticationServicePort" )

public class AuthenticationService {
	
	@WebMethod(operationName="authenticateUser")
	public String authenticateUser(@WebParam(name = "username")String username, @WebParam(name = "password")String password){
		setServiceManager();
		boolean authenticate = authenticate(username, password);
		
		if(authenticate)
			return "success";
		
		return "failure";
	}
	
	public boolean authenticate(String username, String password){
		

		ResourcesUtil ru = ResourcesUtil.getResourcesUtil();
		
		boolean authenticate = false;
		
		String mode = "1";	// set default to DB only
		mode = ru.getCodeValue("authentication.mode", SystemCodeType.GENERAL);
		if(mode==null || mode.length()<1){			
			mode = "1";
		}
		//to make username and password case in-sensitive
		System.out.println("authentication mode: " + mode);
		if(mode.equals(VCSConstants.AUTH_DB_ONLY)){	//DB only
			System.out.println("Authentication mode: DB only");
			authenticate = authenticateDB(username, password);
		}else if(mode.equals(VCSConstants.AUTH_DB_FIRST_WITH_AD)){		//Both DB and AD -  DB first
			System.out.println("Authentication mode: DB and AD");
			authenticate = authenticateDB(username, password);
			if(!authenticate){
				System.out.println("DB authentication failed, going to AD");
				authenticate = authenticateAD(username, password);
			}
		}else if(mode.equals(VCSConstants.AUTH_AD_FIRST_WITH_DB)){		//Both DB and AD - AD first
			System.out.println("Authentication mode: DB and AD");
			authenticate = authenticateAD(username, password);
			if(!authenticate){
				System.out.println("AD authentication failed, going to DB");
				authenticate = authenticateDB(username, password);
			}
		}
		
		return authenticate;
	}
	public boolean authenticateDB(String username, String password){
		password=new Sha256Hash(password).toHex();
		User user= userService.login(username, password);
		if(user==null){

			return false;
		}else if(!user.getUsername().equals(username) || ! user.getPasswordHash().equals(password)){

			return false;
		}else if(user.getLockStatus().equalsIgnoreCase("INACTIVE")){
			   return false;
		}else {
			Iterator<Role> ite=user.getUserRoles().iterator();
			String roleName=null;
			while(ite.hasNext()){
			  roleName=ite.next().getName();
			   break;
			}

			return true;
		}
	}
	
	public boolean authenticateAD(String username, String password){
		boolean authenticate = false;
		  
		try {
			SimpleCredentials credentials = new SimpleCredentials(username, password.toCharArray());
			VirtualCodeAuthenticator authenticator = ActiveDirAuthenticator.getInstance();
	        authenticate = authenticator.authenticate(credentials);
	        
	        //log.debug("authenticate: " + authenticate);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return authenticate;
	}
	
	private ServiceManager serviceManager;
	private UserService userService;
	public void setServiceManager() {
		this.serviceManager = (ServiceManager)SpringApplicationContext.getBean("serviceManager");
		this.userService = this.serviceManager.getUserService();
	}
	
	
}
