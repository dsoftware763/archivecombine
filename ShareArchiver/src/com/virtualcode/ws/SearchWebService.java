/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.ws;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.soap.MTOM;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;


import com.sun.xml.ws.developer.StreamingAttachment;
import com.virtualcode.repository.SearchService;
import com.virtualcode.security.impl.VirtualCodeAuthorizatorImpl;
import com.virtualcode.services.ServiceManager;
import com.virtualcode.vo.SearchDocument;
import com.virtualcode.vo.SearchResult;

/**
 *
 * @author SE6
 */

@StreamingAttachment(parseEagerly = true, memoryThreshold = 40000L)

@WebService(targetNamespace = "http://ws.virtualcode.com/",    serviceName = "SearchService", portName = "SearchService" )
@MTOM
public class SearchWebService {

    static final String SECURE_INFO = "secureInfo";
    static final Logger log = Logger.getLogger(DataExport.class);   
    @Resource
    private WebServiceContext context;

   
    
    @WebMethod(operationName = "search")
    public byte[] search(@WebParam(name = "textPart") String textPart, @WebParam(name = "authorName") String authorName, @WebParam(name = "modifiedBy") String modifiedBy, @WebParam(name = "modifiedStartDate") String modifiedStartDate, @WebParam(name = "modifiedEndDate") String modifiedEndDate, @WebParam(name = "documentName") String documentName, @WebParam(name = "securitySids") String securitySids, @WebParam(name = "start") Integer start, @WebParam(name = "pageSize") Integer pageSize, @WebParam(name = "secureInfo") String secureInfo, @WebParam(name = "sortAscDesc") String sortAscDesc, @WebParam(name = "isLdap")boolean isLdap, @WebParam(name = "contentType") String contentType ) {
    	try{
	    	ServletContext sc = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);  
	    	SearchService  searchService=getSearchService(sc);
	    	String sidString = null;
	    	
	    	try{ 
	    		if(!isLdap){
	    			sidString = null;
	    		}else{
			    System.out.println("--getting sids for user: " + securitySids);
			    VirtualCodeAuthorizatorImpl authorizator = VirtualCodeAuthorizatorImpl.getInstance();
			    sidString = authorizator.sidString(securitySids);
	    		}
			   
	    	}catch(Exception ex){
	    		ex.printStackTrace();
	    		throw new WebServiceException();
	    	}
	    	System.out.println("***************** sids **************** : " + sidString);
	    		
	    	SearchResult searchResult= searchService.searchDocument("DISPLAY", textPart, authorName, modifiedBy, modifiedStartDate, modifiedEndDate, documentName, sidString, sortAscDesc,null, null,null,contentType, 1, 3000, 0l, 0l, null, null);
	    							   
	    	List<SearchDocument> searchDocList=searchResult.getResults();
	    	List<SearchDocument> resultsList = new ArrayList<SearchDocument>();
            if (searchDocList != null && start < searchDocList.size()) {
                for (int i = start; i < searchDocList.size() && i < (start + pageSize); i++) {
                    resultsList.add(searchDocList.get(i));
                }
            }

            searchResult.setResults(resultsList);
	    		    	
	    	String searchXml=null;
    	 
    	    searchXml = marshalSearch(searchResult);
    	    log.debug(searchXml);
            return searchXml.getBytes("UTF-8");
    	}catch(Exception ex){
    		 ex.printStackTrace();
    		// throw new WebServiceException();
	    }
    	
    	return null;
        
    }
    
    
    private String marshalSearch(SearchResult searchResults) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(SearchResult.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        java.io.Writer writer = new StringWriter();
        marshaller.marshal(searchResults, writer);
        return writer.toString();
    }
    
    private ServiceManager serviceManager;    
    private SearchService getSearchService(ServletContext sc){
    	this.serviceManager=(ServiceManager)getWebAppContext(sc).getBean("serviceManager");
    	return serviceManager.getSearchService();        
    }
    
    
    private WebApplicationContext getWebAppContext(ServletContext sc){    	
    	WebApplicationContext webAppContext=WebApplicationContextUtils.getWebApplicationContext(sc);
    	return webAppContext;

    }

    
    
    
}
