package com.virtualcode.ws;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.virtualcode.repository.FileExplorerService;
import com.virtualcode.services.ServiceManager;

@javax.jws.WebService(targetNamespace = "http://ws.virtualcode.com/", serviceName = "FileDataService", portName = "FileDataServicePort" )

public class FileDataService {
		
		@Resource
		private WebServiceContext context; 		
	    		
		@WebMethod(operationName = "getMetaDataOfStub" )			
	    public String getMetaDataOfStub(@WebParam(name = "secureID") String secureID) {
			System.out.println("ws invoked");
			
			ServletContext sc = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
			ServiceManager serviceManager=(ServiceManager)getWebAppContext(sc).getBean("serviceManager");
			FileExplorerService explorerService	=	serviceManager.getFileExplorerService();
			//jobService=getJobService(sc);
			 
			return explorerService.getStubMetaData(secureID);
		}
			

		@WebMethod(operationName = "restoreContentsOfStub" )			
	    public String restoreContentsOfStub(@WebParam(name = "secureID") String secureID,
	    		@WebParam(name = "destPath") String destPath,
	    		@WebParam(name = "userName") String userName) {
			System.out.println("ws invoked getContentOfStub");
			
			ServletContext sc = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
			ServiceManager serviceManager=(ServiceManager)getWebAppContext(sc).getBean("serviceManager");
			FileExplorerService explorerService	=	serviceManager.getFileExplorerService();
			//jobService=getJobService(sc);
			
			return explorerService.restoreContentsOfStub(secureID, destPath, userName, true);
		}
		
	    private WebApplicationContext getWebAppContext(ServletContext sc){
	    	
	    	WebApplicationContext webAppContext=WebApplicationContextUtils.getWebApplicationContext(sc);
	    	return webAppContext;

	    }
}
