/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package executors;

import com.sun.xml.xsom.impl.parser.state.Schema;
import com.virtualcode.agent.das.dataArchiver.JobStarter;
import com.virtualcode.agent.das.dataExporter.ExportStarter;
import com.virtualcode.agent.das.dataIndexer.IndexStarter;
import com.virtualcode.agent.das.dataRetainer.RetentionHandler;
import com.virtualcode.agent.das.database.SchemaCreator;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.agent.monitor.ActivelyStarter;
import com.virtualcode.agent.ssl.InstallCert;


/**
 *
 * @author Yawar
 */
public class Main {

    public static String versionStr    =   "2016.10.09 v1";
    
    public static void main(String[] args) {
        //String anyPath=args.length > 0 ? args[0] : "C:\\Users\\Seven\\Downloads\\CV";
        
        try {
            String AgentName    =   null;
            LicenceKeyForm.initialize();

            //TODO  local DB
            //Check local DB is created or not ,if not create it
            //Configure loggers for current executions
            LoggingManager.configureLogger(0, "", LoggingManager.STARTUPLOGS);
            SchemaCreator.deployDB();


            while(true) {//run until the Agent started successfully...
                try {
                    AgentName = Utility.getAgentConfigs().getName();
                    System.out.println("Agent("+versionStr+") Name: "+AgentName);

                    break;//terminate the loop to proceed further..
                    
                } catch (Exception uhe) {//if any errored occured, to retrieve configuration from Server
                    System.err.println("Error in Config: "+uhe.getMessage());
                    
                    // This code is Auto SSL Certificate Generation
                    String dasURL   =   Utility.getDASUrl();
                    if(dasURL.startsWith("https://")) {//if its a https url

                        System.out.println("Initializing agent for SSL/TLS...");
                        //Agent agentConfigs  =   Utility.getAgentConfigs();//getAgentConfigs VO
                        String sslStorePwd  =   (args.length > 0) ? args[0] : "changeit";//agentConfigs.getSslStorePwd();
                        Integer sslKeyEntry =   (args.length > 1) ? new Integer(args[1]) : new Integer("1");//agentConfigs.getSslKeyEntry();

                        dasURL  =   dasURL.substring(8); //                skip the https://
                        dasURL  =   dasURL.substring(0, dasURL.indexOf("/"));//   sharearchiver:8443

                        //create the client's certificate file
                        String filePath    =   InstallCert.createCertificateFile(dasURL, sslStorePwd, sslKeyEntry);

                        if(filePath!=null) {//if file created successfully than load it...
                            //Thread.sleep(3000);
                            System.out.println("NOW register certificate...");
                            DataLayer.registerSSLCertificate(filePath, sslStorePwd);

                        } else {
                            System.err.println("Error to access ShareArchiver server (Plz Restart Agent later) !");
                            return;
                        }
                    }
                    Thread.sleep(10000);//wait for 10sec
                }
            }
        
            if(AgentName!=null && !AgentName.isEmpty()) {
                //System.out.println(Utility.GetProp("versionInfo"));
                //System.out.println(Utility.GetProp("versionDescription"));

                //If all goes well on SSL, then start all types of supported Jobs
                JobStarter js   =   new JobStarter(AgentName);
                Thread importer   =   new Thread(js);
                importer.start(); //js.run();//Start the Job Executor

                Thread.currentThread().sleep(2000);//02sec

                ExportStarter es    =   new ExportStarter(AgentName);
                Thread exporter     =   new Thread(es);
                exporter.start(); //es.run();//Start the Export Job Executor

                Thread.currentThread().sleep(2000);//02sec

                ActivelyStarter aas     =   new ActivelyStarter(AgentName);
                Thread activelyArchiver =   new Thread(aas);
                activelyArchiver.start(); //es.run();//Start the ActivelyArchiver Executor

                Thread.currentThread().sleep(2000);//02sec

                IndexStarter is     =   new IndexStarter(AgentName);
                Thread indexer =   new Thread(is);
                indexer.start(); //es.run();//Start the Indexer Executor

                Thread.currentThread().sleep(2000);//02sec

                RetentionHandler srH	=	new RetentionHandler(AgentName);
                Thread retension	=	new Thread(srH);
                retension.start();
            }
        } catch (InterruptedException ie) {
            ie.getMessage();
            ie.printStackTrace();

        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }
    }
}
