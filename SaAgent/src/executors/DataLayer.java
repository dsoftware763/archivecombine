package executors;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.InetAddress;
import java.net.URL;
import java.util.*;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.soap.MTOMFeature;

import org.apache.log4j.Logger;

import com.sun.xml.ws.developer.JAXWSProperties;
import com.vcsl.sa.Acl;
import com.vcsl.sa.DocumentManagement;
import com.vcsl.sa.DocumentManagement_Service;
import com.vcsl.sa.export.DataExport;
import com.vcsl.sa.export.DataExportService;
import com.vcsl.sa.export.Document;
import com.virtualcode.agent.das.archivePolicy.dto.ActiveJob;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.archivePolicy.dto.JobStatistics;
import com.virtualcode.agent.das.archivePolicy.dto.SPDocLib;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.ws.AgentManagementService;
import com.virtualcode.ws.AgentManagementService_Service;

/**
 * @author FAisal
 */
public class DataLayer {

    private static AgentManagementService_Service amService = null;
    private static AgentManagementService amPort = null;

    private static DataExportService deService = null;
    private static DataExport dePort = null;

    private static DocumentManagement_Service dmService = null;
    private static DocumentManagement dmPort = null;

    public static List<String> interuptedJobs() throws Exception {
        List<String> cancelingJobs = DataLayer.getCancelledJobs();
        if (cancelingJobs == null)
            cancelingJobs = new ArrayList<String>();
        return cancelingJobs;
    }

    public static String requestForNewAgent(String token1) throws Exception {
        String agentName = InetAddress.getLocalHost().getHostName();
        String hostname = agentName;
        String sourceIP = InetAddress.getLocalHost().getHostAddress();
        String login = agentName.replace(" ", "");
        String password = agentName.trim().toLowerCase();

        return registerAgent(agentName, hostname, sourceIP, login, password, token1);
    }

    private static String registerAgent(java.lang.String agentName, java.lang.String hostname, java.lang.String sourceIP, java.lang.String login, java.lang.String password, java.lang.String companyTag) throws Exception {
        String wsdlURL = Utility.getDASUrl() + "/AgentManagementService?wsdl";
        amService = (amService == null) ? new AgentManagementService_Service(new URL(wsdlURL)) : amService;
        amPort = (amPort == null) ? amService.getAgentManagementServicePort(new WebServiceFeature[]{new MTOMFeature()}) : amPort;

        Map ctxt = ((BindingProvider) amPort).getRequestContext();
        ctxt.put(JAXWSProperties.HTTP_CLIENT_STREAMING_CHUNK_SIZE, 8192);
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsdlURL);
        //System.out.println(ctxt.get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY));

        return amPort.registerAgent(agentName, hostname, sourceIP, login, password, companyTag);
    }

    public static List<Document> getAllChildren(String parent, String secureInfo) throws Exception {
        String wsdlURL = Utility.getDASUrl() + "/DataExport?wsdl";
        deService = (deService == null) ? new DataExportService(new URL(wsdlURL)) : deService;
        dePort = (dePort == null) ? deService.getDataExport() : dePort;

        Map ctxt = ((BindingProvider) dePort).getRequestContext();
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, Utility.getDASUrl() + "/DataExport?wsdl");

        return dePort.getAllChildren(parent, secureInfo);
    }

    public static String uploadDocument(String fileContextPath, javax.activation.DataHandler content, String layers, Acl acl,int requestTimeOut ,int connectionTimeOut) throws Exception {

        String wsdlURL = Utility.getDASUrl() + "/DocumentManagement?wsdl";
        dmService = (dmService == null) ? new DocumentManagement_Service(new URL(wsdlURL)) : dmService;
        dmPort = (dmPort == null) ? dmService.getDMSoapHttpPort(new WebServiceFeature[]{new MTOMFeature()}) : dmPort;

        Map ctxt = ((BindingProvider) dmPort).getRequestContext();
        ctxt.put(JAXWSProperties.HTTP_CLIENT_STREAMING_CHUNK_SIZE, 8192);
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsdlURL);
        ctxt.put(JAXWSProperties.REQUEST_TIMEOUT, requestTimeOut*60000);//converting request time out minutes to milli seconds
        ctxt.put(JAXWSProperties.CONNECT_TIMEOUT, connectionTimeOut*60000);//converting connection time out minutes to milli seconds

        return dmPort.uploadDocument(fileContextPath, content, layers, acl);
    }

    public static List<String> getDocumentList(String spDocLibId, String nodePath, com.vcsl.sa.Policy policy, int currentPage, String companyTag) throws Exception {
        String wsdlURL = Utility.getDASUrl() + "/DocumentManagement?wsdl";
        dmService = (dmService == null) ? new DocumentManagement_Service(new URL(wsdlURL)) : dmService;
        dmPort = (dmPort == null) ? dmService.getDMSoapHttpPort(new WebServiceFeature[]{new MTOMFeature()}) : dmPort;

        Map ctxt = ((BindingProvider) dmPort).getRequestContext();
        ctxt.put(JAXWSProperties.HTTP_CLIENT_STREAMING_CHUNK_SIZE, 8192);
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsdlURL);

        return dmPort.getDocumentList(spDocLibId, nodePath, policy, currentPage, companyTag);

        //port    = (port == null) ? new DocumentManagementModule() : port;
        //return port.getDocumentList(spDocLibId, nodePath, policy, currentPage);
    }

    public static String documentExistsByProperties(String fileContextPath, String layers) throws Exception {
        String wsdlURL = Utility.getDASUrl() + "/DocumentManagement?wsdl";
        dmService = (dmService == null) ? new DocumentManagement_Service(new URL(wsdlURL)) : dmService;
        dmPort = (dmPort == null) ? dmService.getDMSoapHttpPort(new WebServiceFeature[]{new MTOMFeature()}) : dmPort;

        Map ctxt = ((BindingProvider) dmPort).getRequestContext();
        ctxt.put(JAXWSProperties.HTTP_CLIENT_STREAMING_CHUNK_SIZE, 8192);
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsdlURL);

        return dmPort.documentExistsByProperties(fileContextPath, layers);
    }

    public static boolean addMarkedPath(java.lang.String jobId, java.lang.String spDocLibPath) throws Exception {
        String wsdlURL = Utility.getDASUrl() + "/DocumentManagement?wsdl";
        dmService = (dmService == null) ? new DocumentManagement_Service(new URL(wsdlURL)) : dmService;
        dmPort = (dmPort == null) ? dmService.getDMSoapHttpPort(new WebServiceFeature[]{new MTOMFeature()}) : dmPort;

        Map ctxt = ((BindingProvider) dmPort).getRequestContext();
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsdlURL);

        return dmPort.addMarkedPath(jobId, spDocLibPath);
    }

    public static com.virtualcode.ws.Agent getConfigParams(java.lang.String encryptedID) throws Exception {
        String wsdlURL = Utility.getDASUrl() + "/AgentManagementService?wsdl";
        amService = (amService == null) ? new AgentManagementService_Service(new URL(wsdlURL)) : amService;
        amPort = (amPort == null) ? amService.getAgentManagementServicePort(new WebServiceFeature[]{new MTOMFeature()}) : amPort;

        Map ctxt = ((BindingProvider) amPort).getRequestContext();
        ctxt.put(JAXWSProperties.HTTP_CLIENT_STREAMING_CHUNK_SIZE, 8192);
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsdlURL);

        return amPort.getConfigParams(encryptedID);
    }

    public static String checkDeduplicate(java.lang.String nodeID, java.lang.String layersList) throws Exception {

        String wsdlURL = Utility.getDASUrl() + "/DocumentManagement?wsdl";
        dmService = (dmService == null) ? new DocumentManagement_Service(new URL(wsdlURL)) : dmService;
        dmPort = (dmPort == null) ? dmService.getDMSoapHttpPort(new WebServiceFeature[]{new MTOMFeature()}) : dmPort;

        Map ctxt = ((BindingProvider) dmPort).getRequestContext();
        ctxt.put(JAXWSProperties.HTTP_CLIENT_STREAMING_CHUNK_SIZE, 8192);
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsdlURL);

        return dmPort.checkDeduplicate(nodeID, layersList);
    }


    public static List<Job> JobsbyAgent(String AgentName, String actionType) throws Exception {
        //Todo Code to get Jobs from DB
        List<Job> jobs = null;//new ArrayList<Job>();

        try {
            byte[] jobListByteArray = getJobListForProcessing(AgentName, actionType);
            if (jobListByteArray != null && jobListByteArray.length > 0) {

                ByteArrayInputStream bis = new ByteArrayInputStream(jobListByteArray);
                ObjectInputStream ois = new ObjectInputStream(bis);
                jobs = (ArrayList<Job>) ois.readObject();
                ois.close();
                bis.close();
                bis = null;
                ois = null;

            } else {
                //System.out.println("job not found");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (NullPointerException e) {
            System.out.println("no job found");
        }
        return jobs;
    }

    public static HashMap<String, ActiveJob> getJobsToMonitor(HashMap<String, ActiveJob> alreadyAtiveJobs, String agentName, String actionType) throws Exception {

        List<Job> jobs = JobsbyAgent(agentName, actionType);
        HashMap<String, ActiveJob> newActiveJobs = new HashMap<String, ActiveJob>();

        ActiveJob activeJob = null;

        if (jobs != null) {
            for (Job j : jobs) {

                //adjust the action type, according to Job Type
                String jobActionType = (j.getActionType() == "PRINTARCH") ? "4" : j.getActiveActionType();
                boolean isNewJob = false;//by default we assume that its already running OLD active job

                Set<SPDocLib> spLib = j.getSpDocLibSet();
                for (SPDocLib path : spLib) {

                    //transform into custom ActiveJob object
                    activeJob = new ActiveJob(j.getJobName(),
                            path.getDrivePath(),
                            path.getLibraryName(),
                            jobActionType);

                    if (!alreadyAtiveJobs.containsKey(path.getId() + "")) {//if not already entered into List
                        System.out.println(activeJob.toString());
                        newActiveJobs.put(path.getId() + "", activeJob);
                        alreadyAtiveJobs.put(path.getId() + "", activeJob);
                        isNewJob = true;
                    }
                }

                if (isNewJob) {//Mark the status as running, if its a new job or has some new entry of Path
                    DataLayer.executionStarted(j.getId());//mark the status as Running for Active archving jobs
                }
            }
        }

        return newActiveJobs;

    }
    
    /*
     * *************************************************************************
     * web service methods
     * *************************************************************************
     */

    private static List<String> getCancelledJobs() throws Exception {
        String wsdlURL = Utility.getDASUrl() + "/AgentManagementService?wsdl";
        amService = (amService == null) ? new AgentManagementService_Service(new URL(wsdlURL)) : amService;
        amPort = (amPort == null) ? amService.getAgentManagementServicePort(new WebServiceFeature[]{new MTOMFeature()}) : amPort;

        Map ctxt = ((BindingProvider) amPort).getRequestContext();
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsdlURL);

        return amPort.getCancelledJobs();
    }

    private static byte[] getJobListForProcessing(java.lang.String arg0, java.lang.String actionType) throws Exception {
        String wsdlURL = Utility.getDASUrl() + "/AgentManagementService?wsdl";
        amService = (amService == null) ? new AgentManagementService_Service(new URL(wsdlURL)) : amService;
        amPort = (amPort == null) ? amService.getAgentManagementServicePort(new WebServiceFeature[]{new MTOMFeature()}) : amPort;

        Map ctxt = ((BindingProvider) amPort).getRequestContext();
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsdlURL);

        return amPort.getJobListForProcessing(arg0, actionType);
    }

    public static boolean registerSSLCertificate(String keystore, String storepass) {

        //String javaHomePath = System.getProperty("java.home");
        //String keystore = javaHomePath + "/lib/security/jssecacerts";  
        //String storepass= "changeit";  
        String storetype = "JKS";

        String[][] props = {
                {"javax.net.ssl.trustStore", keystore,},
                {"javax.net.ssl.keyStore", keystore,},
                {"javax.net.ssl.keyStorePassword", storepass,},
                {"javax.net.ssl.keyStoreType", storetype,},
        };

        for (int i = 0; i < props.length; i++)
            System.getProperties().setProperty(props[i][0], props[i][1]);

        //for (int i = 0; i < props.length; i++)  
        //System.out.println(props[i][0] + ": " +System.getProperties().getProperty(props[i][0]));

        return true;
    }

    public static String executionStarted(int arg0) throws Exception {
        String wsdlURL = Utility.getDASUrl() + "/AgentManagementService?wsdl";
        amService = (amService == null) ? new AgentManagementService_Service(new URL(wsdlURL)) : amService;
        amPort = (amPort == null) ? amService.getAgentManagementServicePort(new WebServiceFeature[]{new MTOMFeature()}) : amPort;

        Map ctxt = ((BindingProvider) amPort).getRequestContext();
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsdlURL);
        return amPort.executionStarted(arg0);
    }

    public static com.vcsl.sa.Policy mapPolicy(com.virtualcode.agent.das.archivePolicy.dto.Policy policy) {
        com.vcsl.sa.Policy mapedPolicy = new com.vcsl.sa.Policy();

        mapedPolicy.setId(policy.getId());
        mapedPolicy.setLastAccessedDate(policy.getLastAccessedDate());
        mapedPolicy.setLastRepoAccessedDate(policy.getLastRepoAccessedDate());
        mapedPolicy.setLastModifiedDate(policy.getLastModifiedDate());
        mapedPolicy.setPolicyName(policy.getPolicyName());
        mapedPolicy.setSizeLargerThan(policy.getSizeLargerThan());
        mapedPolicy.setActive(policy.getActive());
        mapedPolicy.setDescription(policy.getDescription());
        mapedPolicy.setDocumentAge(policy.getDocumentAge());

        return mapedPolicy;
    }

    public static boolean exportExecutionCompleted(int JobID, String ExecutionID, int ErrorCodeID, JobStatistics jobKPI) throws Exception {
        String wsdlURL = Utility.getDASUrl() + "/AgentManagementService?wsdl";
        amService = (amService == null) ? new AgentManagementService_Service(new URL(wsdlURL)) : amService;
        amPort = (amPort == null) ? amService.getAgentManagementServicePort(new WebServiceFeature[]{new MTOMFeature()}) : amPort;

        Map ctxt = ((BindingProvider) amPort).getRequestContext();
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsdlURL);
        Logger logger = LoggingManager.getLogger(LoggingManager.EXP_JOB_SUMMARY, ExecutionID);

        logger.info("KPIs are" + (jobKPI != null ? " not " : "") + "null  " + ", JobID=" + JobID);

        if (jobKPI != null) {
            com.virtualcode.ws.JobStatistics jobStatistics = mapStatisticsFields(ExecutionID, jobKPI);/*new com.ws.db.agent.JobStatistics();
            jobStatistics.setExecutionID(ExecutionID);
            jobStatistics.setJobStartTime(Utility.convertDate(jobKPI.getJobStartTime()));
            jobStatistics.setJobEndTime(Utility.convertDate(jobKPI.getJobEndTime()));
            jobStatistics.setSkippedProcessFileSkippedHiddenLected(jobKPI.getSkippedProcessFileSkippedHiddenLected());
            jobStatistics.setSkippedProcessFileSkippedIsDir(jobKPI.getSkippedProcessFileSkippedIsDir());
            jobStatistics.setSkippedProcessFileSkippedMismatchAge(jobKPI.getSkippedProcessFileSkippedMismatchAge());
            jobStatistics.setSkippedProcessFileSkippedMismatchExtension(jobKPI.getSkippedProcessFileSkippedMismatchExtension());
            jobStatistics.setSkippedProcessFileSkippedMismatchLastAccessTime(jobKPI.getSkippedProcessFileSkippedMismatchLastAccessTime());
            jobStatistics.setSkippedProcessFileSkippedMismatchLastModifiedTime(jobKPI.getSkippedProcessFileSkippedMismatchLastModifiedTime());
            jobStatistics.setSkippedProcessFileSkippedReadOnly(jobKPI.getSkippedProcessFileSkippedReadOnly());
            jobStatistics.setSkippedProcessFileSkippedSymbolLink(jobKPI.getSkippedProcessFileSkippedSymbolLink());
            jobStatistics.setSkippedProcessFileSkippedTemporary(jobKPI.getSkippedProcessFileSkippedTemporary());
            jobStatistics.setSkippedProcessFileSkippedTooLarge(jobKPI.getSkippedProcessFileSkippedTooLarge());
            jobStatistics.setTotalFreshArchived(jobKPI.getTotalFreshArchived());
            jobStatistics.setAlreadyArchived(jobKPI.getAlreadyArchived());
            jobStatistics.setTotalDuplicated(jobKPI.getTotalDuplicated());
            jobStatistics.setTotalEvaluated(jobKPI.getTotalEvaluated());
            jobStatistics.setTotalFailedArchiving(jobKPI.getTotalFailedArchiving());
            jobStatistics.setTotalFailedDeduplication(jobKPI.getTotalFailedDeduplication());
            jobStatistics.setTotalFailedEvaluating(0);
            jobStatistics.setTotalFailedStubbing(jobKPI.getTotalFailedStubbing());
            jobStatistics.setTotalStubbed(jobKPI.getTotalStubbed());
            jobStatistics.setArchivedVolume(jobKPI.getArchivedVolume());*/

            logger.info("Execution Statistics for Job  " + ", JobID=" + JobID);
            logger.info("=======================================");
            logger.info("Summary Statistics");
            logger.info("Total Evaluated: : " + jobStatistics.getTotalEvaluated());
            long failed = jobStatistics.getSkippedProcessFileSkippedReadOnly()
                    + jobStatistics.getSkippedProcessFileSkippedHiddenLected()
                    + jobStatistics.getSkippedProcessFileSkippedTooLarge()
                    + jobStatistics.getSkippedProcessFileSkippedTemporary()
                    + jobStatistics.getSkippedProcessFileSkippedSymbolLink()
                    + jobStatistics.getSkippedProcessFileSkippedIsDir()
                    + jobStatistics.getSkippedProcessFileSkippedMismatchAge()
                    + jobStatistics.getSkippedProcessFileSkippedMismatchLastAccessTime()
                    + jobStatistics.getSkippedProcessFileSkippedMismatchLastModifiedTime()
                    + jobStatistics.getSkippedProcessFileSkippedMismatchExtension();

            logger.info("Total Skipped: " + failed);
            logger.info("Total Duplicated: " + jobStatistics.getTotalDuplicated());
            logger.info("Total Exported: " + jobStatistics.getTotalFreshArchived());
            logger.info("Total Already Exported: " + jobStatistics.getAlreadyArchived());
            logger.info("Total Stubbed: " + jobStatistics.getTotalStubbed());
            logger.info("Total Volume: " + jobStatistics.getArchivedVolume());
            logger.info("Total Failed"
                    + (jobStatistics.getTotalFailedDeduplication()
                    + jobStatistics.getTotalFailedEvaluating()
                    + jobStatistics.getTotalFailedStubbing()
                    + jobStatistics.getTotalFailedArchiving()));
            logger.info("");
            logger.info("Failure Statistics");
            logger.info("Total Failed Archiving: " + jobStatistics.getTotalFailedArchiving());
            logger.info("Total Failed Evaluating: " + jobStatistics.getTotalFailedEvaluating());
            logger.info("Total Failed Stubbing: " + jobStatistics.getTotalFailedStubbing());
            logger.info("Total Failed Deduplication: " + jobStatistics.getTotalFailedDeduplication());
            logger.info("");
            logger.info("Skipped Detail");
            logger.info("Skipped ReadOnly: " + jobStatistics.getSkippedProcessFileSkippedReadOnly());
            logger.info("Skipped Hidden: " + jobStatistics.getSkippedProcessFileSkippedHiddenLected());
            logger.info("Skipped Too Large: " + jobStatistics.getSkippedProcessFileSkippedTooLarge());
            logger.info("Skipped Temporary: " + jobStatistics.getSkippedProcessFileSkippedTemporary());
            logger.info("Skipped Symbol Link: " + jobStatistics.getSkippedProcessFileSkippedSymbolLink());
            logger.info("Skipped Directory: " + jobStatistics.getSkippedProcessFileSkippedIsDir());
            logger.info("Skipped Age Mismatch: " + jobStatistics.getSkippedProcessFileSkippedMismatchAge());
            logger.info("Skipped Mismatch Last AccessTime: " + jobStatistics.getSkippedProcessFileSkippedMismatchLastAccessTime());
            logger.info("Skipped Mismatch Last ModifiedTime: " + jobStatistics.getSkippedProcessFileSkippedMismatchLastModifiedTime());
            logger.info("Skipped Mismatch Extension: " + jobStatistics.getSkippedProcessFileSkippedMismatchExtension());

            logger.info("Job Start Time :" + jobStatistics.getJobStartTime().toString());
            logger.info("Job End Time :" + jobStatistics.getJobEndTime().toString());


            return amPort.executionCompleted2(JobID, ExecutionID, ErrorCodeID, jobStatistics);
        }
        return amPort.executionCompleted2(JobID, ExecutionID, ErrorCodeID, null);
    }

    private static com.virtualcode.ws.JobStatistics mapStatisticsFields(String ExecutionID, JobStatistics jobKPI) throws Exception {
        com.virtualcode.ws.JobStatistics jobStatistics = new com.virtualcode.ws.JobStatistics();

        jobStatistics.setExecutionID(ExecutionID);
        if (jobKPI.getJobStartTime() != null)
            jobStatistics.setJobStartTime(Utility.convertDate(jobKPI.getJobStartTime()));
        if (jobKPI.getJobEndTime() != null)
            jobStatistics.setJobEndTime(Utility.convertDate(jobKPI.getJobEndTime()));
        jobStatistics.setSkippedProcessFileSkippedHiddenLected(jobKPI.getSkippedProcessFileSkippedHiddenLected());
        jobStatistics.setSkippedProcessFileSkippedIsDir(jobKPI.getSkippedProcessFileSkippedIsDir());
        jobStatistics.setSkippedProcessFileSkippedMismatchAge(jobKPI.getSkippedProcessFileSkippedMismatchAge());
        jobStatistics.setSkippedProcessFileSkippedMismatchExtension(jobKPI.getSkippedProcessFileSkippedMismatchExtension());
        jobStatistics.setSkippedProcessFileSkippedMismatchLastAccessTime(jobKPI.getSkippedProcessFileSkippedMismatchLastAccessTime());
        jobStatistics.setSkippedProcessFileSkippedMismatchLastModifiedTime(jobKPI.getSkippedProcessFileSkippedMismatchLastModifiedTime());
        jobStatistics.setSkippedProcessFileSkippedReadOnly(jobKPI.getSkippedProcessFileSkippedReadOnly());
        jobStatistics.setSkippedProcessFileSkippedSymbolLink(jobKPI.getSkippedProcessFileSkippedSymbolLink());
        jobStatistics.setSkippedProcessFileSkippedTemporary(jobKPI.getSkippedProcessFileSkippedTemporary());
        jobStatistics.setSkippedProcessFileSkippedTooLarge(jobKPI.getSkippedProcessFileSkippedTooLarge());
        jobStatistics.setTotalFreshArchived(jobKPI.getTotalFreshArchived());
        jobStatistics.setAlreadyArchived(jobKPI.getAlreadyArchived());
        jobStatistics.setTotalDuplicated(jobKPI.getTotalDuplicated());
        jobStatistics.setTotalEvaluated(jobKPI.getTotalEvaluated());
        jobStatistics.setTotalFailedArchiving(jobKPI.getTotalFailedArchiving());
        jobStatistics.setTotalFailedDeduplication(jobKPI.getTotalFailedDeduplication());
        jobStatistics.setTotalFailedEvaluating(0);
        jobStatistics.setTotalFailedStubbing(jobKPI.getTotalFailedStubbing());
        jobStatistics.setTotalStubbed(jobKPI.getTotalStubbed());
        jobStatistics.setArchivedVolume(jobKPI.getArchivedVolume());

        return jobStatistics;
    }

    public static boolean updateHotStatistics(int jobID, java.lang.String executionID, int errorCodeID, JobStatistics jobKPI) throws Exception {
        String wsdlURL = Utility.getDASUrl() + "/AgentManagementService?wsdl";
        amService = (amService == null) ? new AgentManagementService_Service(new URL(wsdlURL)) : amService;
        amPort = (amPort == null) ? amService.getAgentManagementServicePort(new WebServiceFeature[]{new MTOMFeature()}) : amPort;

        Map ctxt = ((BindingProvider) amPort).getRequestContext();
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsdlURL);

        com.virtualcode.ws.JobStatistics jobStatistics = mapStatisticsFields(executionID, jobKPI);

        return amPort.updateHotStatistics(jobID, executionID, errorCodeID, jobStatistics);
    }

    public static boolean executionCompleted(int JobID, String ExecutionID, int ErrorCodeID, JobStatistics jobKPI) throws Exception {
        String wsdlURL = Utility.getDASUrl() + "/AgentManagementService?wsdl";
        amService = (amService == null) ? new AgentManagementService_Service(new URL(wsdlURL)) : amService;
        amPort = (amPort == null) ? amService.getAgentManagementServicePort(new WebServiceFeature[]{new MTOMFeature()}) : amPort;

        Map ctxt = ((BindingProvider) amPort).getRequestContext();
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsdlURL);
        Logger logger = LoggingManager.getLogger(LoggingManager.JOB_SUMMARY, ExecutionID);

        logger.info("KPIs are" + (jobKPI != null ? " not " : "") + "null  " + ", JobID=" + JobID);

        if (jobKPI != null) {
            com.virtualcode.ws.JobStatistics jobStatistics = mapStatisticsFields(ExecutionID, jobKPI);

            logger.info("Execution Statistics for Job  " + ", JobID=" + JobID);
            logger.info("=======================================");
            logger.info("Summary Statistics");
            logger.info("Total Evaluated: : " + jobStatistics.getTotalEvaluated());
            long failed = jobStatistics.getSkippedProcessFileSkippedReadOnly()
                    + jobStatistics.getSkippedProcessFileSkippedHiddenLected()
                    + jobStatistics.getSkippedProcessFileSkippedTooLarge()
                    + jobStatistics.getSkippedProcessFileSkippedTemporary()
                    + jobStatistics.getSkippedProcessFileSkippedSymbolLink()
                    + jobStatistics.getSkippedProcessFileSkippedIsDir()
                    + jobStatistics.getSkippedProcessFileSkippedMismatchAge()
                    + jobStatistics.getSkippedProcessFileSkippedMismatchLastAccessTime()
                    + jobStatistics.getSkippedProcessFileSkippedMismatchLastModifiedTime()
                    + jobStatistics.getSkippedProcessFileSkippedMismatchExtension();

            logger.info("Total Skipped: " + failed);
            logger.info("Total Fresh Archived: " + jobStatistics.getTotalFreshArchived());
            logger.info("Total Duplicated: " + jobStatistics.getTotalDuplicated());
            logger.info("Total Already Archived: " + jobStatistics.getAlreadyArchived());
            logger.info("Total Stubbed: " + jobStatistics.getTotalStubbed());
            logger.info("Total Volume: " + jobStatistics.getArchivedVolume());
            logger.info("Total Failed"
                    + (jobStatistics.getTotalFailedDeduplication()
                    + jobStatistics.getTotalFailedEvaluating()
                    + jobStatistics.getTotalFailedStubbing()
                    + jobStatistics.getTotalFailedArchiving()));
            logger.info("");
            logger.info("Failure Statistics");
            logger.info("Total Failed Archiving: " + jobStatistics.getTotalFailedArchiving());
            logger.info("Total Failed Evaluating: " + jobStatistics.getTotalFailedEvaluating());
            logger.info("Total Failed Stubbing: " + jobStatistics.getTotalFailedStubbing());
            logger.info("Total Failed Deduplication: " + jobStatistics.getTotalFailedDeduplication());
            logger.info("");
            logger.info("Skipped Detail");
            logger.info("Skipped ReadOnly: " + jobStatistics.getSkippedProcessFileSkippedReadOnly());
            logger.info("Skipped Hidden: " + jobStatistics.getSkippedProcessFileSkippedHiddenLected());
            logger.info("Skipped Too Large: " + jobStatistics.getSkippedProcessFileSkippedTooLarge());
            logger.info("Skipped Temporary: " + jobStatistics.getSkippedProcessFileSkippedTemporary());
            logger.info("Skipped Symbol Link: " + jobStatistics.getSkippedProcessFileSkippedSymbolLink());
            logger.info("Skipped Directory: " + jobStatistics.getSkippedProcessFileSkippedIsDir());
            logger.info("Skipped Age Mismatch: " + jobStatistics.getSkippedProcessFileSkippedMismatchAge());
            logger.info("Skipped Mismatch Last AccessTime: " + jobStatistics.getSkippedProcessFileSkippedMismatchLastAccessTime());
            logger.info("Skipped Mismatch Last ModifiedTime: " + jobStatistics.getSkippedProcessFileSkippedMismatchLastModifiedTime());
            logger.info("Skipped Mismatch Extension: " + jobStatistics.getSkippedProcessFileSkippedMismatchExtension());

            logger.info("Job Start Time :" + jobStatistics.getJobStartTime().toString());
            logger.info("Job End Time :" + jobStatistics.getJobEndTime().toString());


            return amPort.executionCompleted2(JobID, ExecutionID, ErrorCodeID, jobStatistics);
        }
        return amPort.executionCompleted2(JobID, ExecutionID, ErrorCodeID, null);
    }


    public static String updateExecutionStatus(int arg0) throws Exception {
        String wsdlURL  =   Utility.getDASUrl() + "/AgentManagementService?wsdl";
        amService = (amService==null)?new AgentManagementService_Service(new URL(wsdlURL)):amService;
        amPort = (amPort==null)?amService.getAgentManagementServicePort(new WebServiceFeature[]{new MTOMFeature()}):amPort;

        Map ctxt = ((BindingProvider) amPort).getRequestContext();
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsdlURL);
        return amPort.updateExecutionStatus(arg0);
    }
}
