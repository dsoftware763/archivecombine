/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.monitor.jdefault;

import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.monitor.jdefault.dataobjects.DOManagedFile;

/**
 *
 * @author YAwar
 */
public class TestjDefaultNIO {
    
    public static void main(String args[]) throws Exception {
        LoggingManager.configureLogger(-01, "defaultNIO", LoggingManager.ACTIVE_ARCHIVING_DETAIL);
        
        DOManagedFile dmf   =   new DOManagedFile();
        dmf.setAbsolutePath("D:/Sharearchiver/tempFolder");
        dmf.setSharePath("smb://NETWORK;se9:s9*@localhost/");
        dmf.setWatchEnabled(true);
        dmf.setSubFoldersEnabled(true);
        dmf.setActionType("3");
        dmf.setJobName("defaultNIO");
        
        ManagedFolderWatcher dfw    =   new ManagedFolderWatcher(dmf);
        Thread t    =   new Thread(dfw);
        t.start();
    }
}
