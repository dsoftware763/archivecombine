/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.monitor.jdefault.dataobjects;

public class DOManagedFile {

    private boolean watchEnabled;
    private String absolutePath;
    private String sharePath;
    private boolean subFoldersEnabled;
    private String actionType;
    private String jobName;
    private boolean deduplicatedVolume;

    public DOManagedFile() {
        this(false, "", false, "0", "", "", false);//set default values
    }

    public DOManagedFile(boolean watchEnabled, String absolutePath, boolean subFoldersEnabled, String actionType, String sharePath, String jobName, boolean deduplicatedVolume){
        setWatchEnabled(watchEnabled);
        setAbsolutePath(absolutePath);
        setSubFoldersEnabled(subFoldersEnabled);
        setActionType(actionType);
        setSharePath(sharePath);
        setJobName(jobName);
        setDeduplicatedVolume(deduplicatedVolume);
    }

    public boolean isDeduplicatedVolume() {
        return deduplicatedVolume;
    }

    public void setDeduplicatedVolume(boolean deduplicatedVolume) {
        this.deduplicatedVolume = deduplicatedVolume;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public void setWatchEnabled(boolean watchEnabled) {
        this.watchEnabled = watchEnabled;
    }

    public boolean isWatchEnabled() {
        return watchEnabled;
    }

    public void setAbsolutePath(String folderPath) {
        this.absolutePath = folderPath;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setSubFoldersEnabled(boolean subFoldersEnabled) {
        this.subFoldersEnabled = subFoldersEnabled;
    }

    public boolean isSubFoldersEnabled() {
        return subFoldersEnabled;
    }

    public String getSharePath() {
        return sharePath;
    }

    public void setSharePath(String sharePath) {
        this.sharePath = sharePath;
    }
    
}