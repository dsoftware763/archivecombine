/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.monitor.jdefault.dataobjects;

import java.nio.file.Path;
import java.nio.file.WatchEvent.Kind;
import java.util.List;

/**
 *
 * @author YAwar
 */
public class EventEntry {

    public List<Kind<?>> events;
    public String filePath;
    public Path pathObj;
    public String oldPath=null;//it will be populated, if the EventEntry is fired more than One time...

    public EventEntry(List<Kind<?>> events, String filePath, Path pathObj) {
        this.events = events;
        this.filePath = filePath;
        this.pathObj    =   pathObj;
    }
    
    public boolean isSameNameAtSameLocation() {
        boolean output  =   false;
        
        //if the EventEntry is fired twice (or more than twice) for same file at same location
        if(oldPath!=null && pathObj!=null && oldPath.equals(pathObj.toString())) {
            output  =   true;
        }
        return output;
    }
}