/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.monitor.jdefault;

import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
import java.nio.file.*;
import java.nio.file.WatchEvent.Kind;

import static java.nio.file.StandardWatchEventKinds.*;
import static java.nio.file.LinkOption.*;
import java.nio.file.attribute.*;
import java.io.*;
import java.util.*;

import com.virtualcode.agent.monitor.jdefault.dataobjects.DOManagedFile;
import com.virtualcode.agent.monitor.jdefault.dataobjects.EventEntry;
import com.virtualcode.agent.monitor.jdefault.dataobjects.WatchedFileAction;
import com.virtualcode.agent.monitor.nio.InitialSyncHandler;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.log4j.Logger;

public class ManagedFolderWatcher implements Runnable {

    private Logger log = null;
    private final Map<WatchKey, Path> keys;
    private DOManagedFile managedFolder;
    private boolean isRunning;
    private WatchService watcher;
    //private static EventEntry evEntry;
    private static HashMap<String, EventEntry> entriesList  =   new HashMap<String, EventEntry>();
    private ExecutorService eventExecutorService;
    //private static Object eventEntrySyncObj = new Object();

    @SuppressWarnings("unchecked")
    static <T> WatchEvent<T> cast(WatchEvent<?> event) {
        return (WatchEvent<T>) event;
    }

    /**
     * Creates a WatchService and registers the given directory
     */
    public ManagedFolderWatcher(DOManagedFile managedFolder) throws IOException {
        this.managedFolder = managedFolder;
        this.log    =   LoggingManager.getLogger(LoggingManager.ACTIVE_ARCHIVING_DETAIL, managedFolder.getJobName());

        Path dir = Paths.get(managedFolder.getAbsolutePath());
        this.keys = new HashMap<WatchKey, Path>();
        int archiverPoolSize    =   Utility.getAgentConfigs().getArchiverpoolSize();//same as the Normal archiving Pool Size
        eventExecutorService = Executors.newFixedThreadPool(archiverPoolSize);
        
        try {
            watcher = FileSystems.getDefault().newWatchService();
            if (managedFolder.isSubFoldersEnabled()) {
                log.debug(String.format("Scanning %s ...", dir));
                registerAll(dir);
                log.debug("Done scanning.");
            } else {
                register(dir);
            }

        } catch (IOException e) {
            log.error("Failed to initialize watch service", e);
        }
    }

    /**
     * Register the given directory with the WatchService
     */
    private void register(Path dir) throws IOException {
        WatchKey key = dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
        if (log.isDebugEnabled()) {
            Path prev = keys.get(key);
            if (prev == null) {
                log.debug(String.format("Start watching folder: %s", dir));
            } else {
                if (!dir.equals(prev)) {
                    log.debug(String.format("Update watch folder: %s -> %s", prev, dir));
                }
            }
        }
        keys.put(key, dir);
    }

    /**
     * Register the given directory, and all its sub-directories, with the
     * WatchService.
     */
    private void registerAll(final Path start) throws IOException {

        // register directory and sub-directories
        Files.walkFileTree(start, new SimpleFileVisitor<Path>() {

            //preVisitDirectory is to register all directories to be registered
            @Override
            public FileVisitResult preVisitDirectory(Path dir,
                    BasicFileAttributes attrs) throws IOException {
                register(dir);
                return FileVisitResult.CONTINUE;
            }
            
            //visitFile is to verify that all the existing files are archived
            @Override
            public FileVisitResult visitFile(Path file,
                    BasicFileAttributes attrs) throws IOException {
                
                log.debug(String.format("Visiting file for initial syncronizations: %s", file));
                //generate a new Thread to archive the files
                eventExecutorService.execute(new InitialSyncHandler(file, managedFolder));
                
                return FileVisitResult.CONTINUE;
            }
        });
    }

    public boolean isProcessingEvents() {
        return isRunning;
    }

    public void stop() {
        try {
            isRunning = false;
            watcher.close();
        } catch (IOException e) {
            log.error("Failed to close watcher", e);
        }
    }

    @Override
    public void run() {
        isRunning = true;
        while (true) {

            // wait for key to be signalled
            WatchKey key;
            try {
                key = watcher.take();
            } catch (InterruptedException x) {
                break;
            } catch (ClosedWatchServiceException x) {
                break;
            }

            Path dir = keys.get(key);
            if (dir == null) {
                log.error(String.format("WatchKey not recognized! key='%s'", key));
                continue;
            }

            for (WatchEvent<?> event : key.pollEvents()) {
                Kind<?> kind = event.kind();

                // TBD - provide example of how OVERFLOW event is handled
                if (kind == OVERFLOW) {
                    continue;
                }

                // Context for directory entry event is the file name of entry
                WatchEvent<Path> ev = cast(event);
                Path name = ev.context();
                
                try {
                    Path child = dir.resolve(name);
                    
                    // if directory is created, and watching recursively, then
                    // register it and its sub-directories
                    if (Files.isDirectory(child, NOFOLLOW_LINKS)) {
                        if (managedFolder.isSubFoldersEnabled() && (kind == ENTRY_CREATE)) {
                            log.debug(String.format("Scanning %s...", child));
                            registerAll(child);
                            
                        } else {
                            log.debug(event.kind().name() + " is ignored on dir "+ child.toString());
                        }
                        
                    } else if(isEvaluatedToTrrigger(child.getFileName().toString())){//else if its evaluated FILE       
                        
                        // print out event
                        log.debug(String.format("%s: on %s", event.kind().name(), child));
                        
                        synchronized(ManagedFolderWatcher.class) {
                            if(!entriesList.containsKey(child.toString())) {//if (eventEntry == null) {
                                
                                //introduce a new EventEntry and List for current file
                                List<Kind<?>> kinds = new ArrayList<>();
                                kinds.add(event.kind());

                                EventEntry eventEntry = new EventEntry(kinds, child.toString(), child);
                                entriesList.put(child.toString(), eventEntry);

                            } else {
                                //update the already created EVentEntries list
                                EventEntry eventEntry   =   entriesList.get(child.toString());
                                eventEntry.events.add(event.kind());
                                eventEntry.oldPath = child.toString();
                            }
                        }
                        
                        //do delay the check a bit to have time to get some messages to allow single file operations.
                        //it will help to pick sudden or instant CREATE/MODIFY (as we have to avoid such MODIFY)
                        Thread delaidCheck = new Thread(new DelaidCheck(entriesList.get(child.toString())) {
                            @Override
                            public void run() {
                                
                                //Keep a lock for Delay, on each file (to capture its all events)
                                synchronized(DelaidCheck.class) {
                                    
                                    //Skip the current Thread, if a thread is already waiting for this eventEntry.file
                                    if(eventsInDelayPool.containsKey(eventEntry.filePath)) {
                                        log.debug("Already in DelayPool");//skip the Thread, if its sequence is already initiated
                                        return;
                                    } else {
                                        eventsInDelayPool.put(eventEntry.filePath,null);
                                    }
                                }
                                
                                try {
                                    long timeDifference = 0L;
                                    long sizeDifference = 0L;
                                    
                                    //delayTime for aprox 10MB file (in seconds)
                                    long delayTime  =   Utility.getAgentConfigs().getActiveArchiveDelay().longValue();
                                    long copySpeed  =   Utility.getAgentConfigs().getFileCopySpeed().longValue();//in MB per Sec
                                    int ctr = 0;
                                    
                                    do {//do wait, untill file completed its changes...
                                        
                                        //Grab the initial MetaData (size & Modified Date)
                                        File iF =   new File(this.eventEntry.filePath);
                                        long initTime   =   iF.lastModified();
                                        long initSize   =   iF.length();
                                        log.debug("Init :"+ctr +" "+ initTime +", "+initSize +" "+eventEntry.filePath);

                                        //Calculate estimated DelayTime (on the basis of FileSize and CopySpeed)
                                        long threshHoldSize =   delayTime*copySpeed*1024*1024;
                                        if(initSize > threshHoldSize) {
                                            //set the DelayTime to new value
                                            delayTime = Math.round(initSize/threshHoldSize) * delayTime;
                                            log.debug("Delay of "+delayTime+" seconds");
                                        }
                                        
                                        Thread.sleep(delayTime*1000);//Wait to capture sequence of all events on this file

                                        //Grab the MetaData (after waiting for X seconds)
                                        File endF =   new File(this.eventEntry.filePath);
                                        long endTime    =   endF.lastModified();
                                        long endSize    =   endF.length();
                                        log.debug("End :"+ctr +" "+ endTime + ", "+endSize +" " +eventEntry.filePath);
                                        
                                        timeDifference  =   endTime - initTime;
                                        sizeDifference  =   endSize - initSize;
                                        
                                        ctr++;
                                        iF      =   null;
                                        endF    =   null;
                                        
                                    } while (timeDifference!=0 || sizeDifference>0);//Do wait again, IF the MetaData is still being changed.
                                      
                                } catch (InterruptedException e) {
                                    log.warn("Exception: "+e);
                                }
                                
                                //remove lock from Pool
                                eventsInDelayPool.remove(eventEntry.filePath);
                                
                                //Now, check he sequence of Events - To triger appropriate action
                                checkEvents(this.eventEntry);
                            }
                        });
                        delaidCheck.setName("DelayedCheck-"+child.toString());
                        delaidCheck.start();                        
                        
                    } else { //ignored File (not matched the Policy criteria)                        
                        log.debug(event.kind().name() + " is ignored on TEMP file "+ child.toString());
                    }
                    
                } catch (IOException x) {
                    log.error(String.format("Failed to process event on %s with all of its children", name));
                }
            }

            // reset key and remove from set if directory no longer accessible
            boolean valid = key.reset();
            if (!valid) {
                keys.remove(key);

                // all directories are inaccessible
                if (keys.isEmpty()) {
                    break;
                }
            }
        }
        isRunning = false;
    }

    private boolean isEvaluatedToTrrigger(String fileName) {
        boolean output  =   true;
        if (fileName==null
                || fileName.isEmpty()
                || fileName.startsWith("~")
                || fileName.startsWith("$")
                //|| fileName.startsWith(".")
                || fileName.endsWith(".lnk")
                || fileName.endsWith(".tmp")
                || fileName.endsWith(".url")) {
            output   =   false;
        }
        
        return output;
    }
    
    private void checkEvents(EventEntry eventEntry) {
        synchronized (ManagedFolderWatcher.class) {
            if (eventEntry != null && entriesList.containsKey(eventEntry.filePath)) {
                List<Kind<?>> events = eventEntry.events;

                log.debug("eventsCount: "+events.size()+ " on "+eventEntry.filePath);
                if (events.size() == 1) {
                    if (events.get(0).equals(ENTRY_DELETE)) {
                        fireFileWatchAction(WatchedFileAction.Delete, eventEntry);
                        
                    } else if (events.get(0).equals(ENTRY_MODIFY)) {
                        fireFileWatchAction(WatchedFileAction.Modify, eventEntry);
                        
                    } else if (events.get(0).equals(ENTRY_CREATE)) {//handle only Creates as well...
                        /////Do nothing, bcoz we are not handling only creates - untill some content is added in it
                        fireFileWatchAction(WatchedFileAction.Create, eventEntry);
                    }
                    
                } else if(events.size() > 1) {
                    if (events.get(0).equals(ENTRY_CREATE) &&
                            events.get(1).equals(ENTRY_MODIFY)) {
                        fireFileWatchAction(WatchedFileAction.Create, eventEntry);
                        
                    } else if (events.get(0).equals(ENTRY_MODIFY) &&
                            events.get(1).equals(ENTRY_MODIFY)) {
                        fireFileWatchAction(WatchedFileAction.Modify, eventEntry);
                        
                    /*} else if (events.get(0).equals(ENTRY_DELETE) &&
                            events.get(1).equals(ENTRY_CREATE) &&
                            eventEntry.isSameNameAtSameLocation()) {//case for MS Office files (Modify event)
                        
                        //While modifying an MS-Office file (a sequence of DELETE->CREATE fired on same location/path
                        fireFileWatchAction(WatchedFileAction.Modify, eventEntry);
                      */  
                    } else {
                        
                        int deleteSequence   =   -1;//initialization values
                        int createSequence   =   -1;
                        
                        for(int i=0; i<events.size(); i++) {//iterate to get the position/sequence of Events
                            if(events.get(i).equals(ENTRY_DELETE)) {
                                deleteSequence   =   i;
                                
                            } else if(events.get(i).equals(ENTRY_CREATE) || events.get(i).equals(ENTRY_MODIFY)) {
                                createSequence   =   i;
                            }
                        }
                        
                        //if CREATE is fired, after DELETE. for same file
                        if(createSequence > deleteSequence &&
                            eventEntry.isSameNameAtSameLocation()) {
                            
                            fireFileWatchAction(WatchedFileAction.Create, eventEntry);
                            
                        } else {
                            log.debug("incorrect sequence of Events "+events.get(0)+" -> "+events.get(1));
                        }
                    }
                }
                
                /* //Figure out a way to handle this list properly if e.g. a folder is being renamed or multiple files being moved
                // Win7 sequences:
                // Create:
                // [ Watch9] DEBUG 2012-04-05 21:18:46.060 ENTRY_CREATE: C:\tmp\test (2011) - Copy.mkv
                // [ Watch9] DEBUG 2012-04-05 21:18:46.061 ENTRY_MODIFY: C:\tmp\test (2011) - Copy.mkv
                // [ Watch9] DEBUG 2012-04-05 21:19:49.779 ENTRY_MODIFY: C:\tmp\test (2011) - Copy.mkv
                //
                // Move in same watch folder:
                // [ Watch9] DEBUG 2012-04-05 21:20:27.952 ENTRY_DELETE: C:\tmp\test (2011) - Copy.mkv
                // [ Watch9] DEBUG 2012-04-05 21:20:27.953 ENTRY_CREATE: C:\tmp\tmp\test (2011) - Copy.mkv
                // [ Watch9] DEBUG 2012-04-05 21:20:27.953 ENTRY_MODIFY: C:\tmp\tmp\test (2011) - Copy.mkv
                // [ Watch9] DEBUG 2012-04-05 21:20:27.953 ENTRY_MODIFY: C:\tmp\tmp
                //
                // Move to other watch folder:
                // [ Watch5] DEBUG 2012-04-06 16:11:47.533 ENTRY_CREATE: C:\tmp\test (2011).mkv
                // [ Watch5] DEBUG 2012-04-06 16:11:47.544 ENTRY_MODIFY: C:\tmp\test (2011).mkv
                // [ Watch5] DEBUG 2012-04-06 16:12:15.707 ENTRY_MODIFY: C:\tmp\test (2011).mkv
                // [ Watch0] DEBUG 2012-04-06 16:12:15.912 ENTRY_DELETE: D:\tmp\test (2011).mkv
                //
                // Rename:
                // [ Watch9] DEBUG 2012-04-05 21:21:34.143 ENTRY_DELETE: C:\tmp\test (2011).mkv
                // [ Watch9] DEBUG 2012-04-05 21:21:34.144 ENTRY_CREATE: C:\tmp\test ().mkv
                // [ Watch9] DEBUG 2012-04-05 21:21:34.145 ENTRY_MODIFY: C:\tmp\test ().mkv
                //
                // Delete:
                // [ Watch9] DEBUG 2012-04-05 21:22:02.921 ENTRY_DELETE: C:\tmp\test (2011).mkv
                 
                if (events.size() > 3) {
                    if (events.get(0).equals(ENTRY_CREATE)
                            && events.get(1).equals(ENTRY_MODIFY)
                            && events.get(2).equals(ENTRY_MODIFY)
                            && events.get(3).equals(ENTRY_DELETE)) {
                        fireFileWatchAction(WatchedFileAction.Move, eventEntry.path, eventEntry.oldPath, name);
                    }
                } else if (events.size() > 2) {
                    if (events.get(0).equals(ENTRY_CREATE)
                            && events.get(1).equals(ENTRY_MODIFY)
                            && events.get(2).equals(ENTRY_MODIFY)) {
                        fireFileWatchAction(WatchedFileAction.Create, eventEntry.path, null, name);
                    } else if ((events.get(0).equals(ENTRY_DELETE)
                            && events.get(1).equals(ENTRY_CREATE)
                            && events.get(2).equals(ENTRY_MODIFY))
                            || (events.size() > 3
                            && events.get(0).equals(ENTRY_CREATE)
                            && events.get(1).equals(ENTRY_MODIFY)
                            && events.get(2).equals(ENTRY_MODIFY)
                            && events.get(3).equals(ENTRY_DELETE))) {
                        fireFileWatchAction(WatchedFileAction.Move, eventEntry.path, eventEntry.oldPath, name);
                    }
                } else if (events.size() == 1) {
                    if (events.get(0).equals(ENTRY_DELETE)) {
                        fireFileWatchAction(WatchedFileAction.Delete, eventEntry.path, null, name);
                    }
                }*/
                
                /*
                log.debug("eventsCount: "+events.size());
                for(int i=0; i<events.size(); i++) {
                    log.debug(i+"="+events.get(i)+" "+eventEntry.path);
                }*/
                
            } else {
                log.info("eventEntry already processed for "+((eventEntry!=null)?eventEntry.filePath:"NULL"));
            }
        }
    }

    private void fireFileWatchAction(WatchedFileAction watchFileAction, EventEntry event) {
        
        log.debug("fired "+watchFileAction.name()+" for "+event.filePath + "");
        if("Create".equalsIgnoreCase(watchFileAction.name()) ||
                "Modify".equalsIgnoreCase(watchFileAction.name())) {
            
            eventExecutorService.execute(new WatchEventHandler(watchFileAction, event, this.managedFolder));
            
        } else {
            //do nothing...  :D
        }
        
        //evEntry = null;
        entriesList.remove(event.filePath);
    }
}