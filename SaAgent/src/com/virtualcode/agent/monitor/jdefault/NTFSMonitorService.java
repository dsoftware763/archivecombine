/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.monitor.jdefault;

import com.virtualcode.agent.das.archivePolicy.dto.ActiveJob;
import com.virtualcode.agent.das.fileSystems.FileServiceInterface;
import com.virtualcode.agent.monitor.jdefault.ManagedFolderWatcher;
import com.virtualcode.agent.monitor.jdefault.dataobjects.DOManagedFile;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Abbas
 */
public class NTFSMonitorService implements FileServiceInterface {
    
    public void walkFileTree(String strPath, ArrayList<String> excPathList, boolean isRecursive, String thisDocDestPath) {
        //DO nothing
    }
    
    public void monitorFileTree(ActiveJob job) {
        
        DOManagedFile dmf   =   new DOManagedFile();
        dmf.setAbsolutePath(job.getAbsolutePath());
        dmf.setSharePath(job.getSharePath());
        dmf.setWatchEnabled(true);
        dmf.setSubFoldersEnabled(true);
        dmf.setActionType(job.getActionType());
        dmf.setJobName(job.getJobName());
        
        try {
            ManagedFolderWatcher dfw    =   new ManagedFolderWatcher(dmf);
            Thread t    =   new Thread(dfw);
            t.start();
        } catch(IOException ie) {
            ie.printStackTrace();
        }
    }
}
