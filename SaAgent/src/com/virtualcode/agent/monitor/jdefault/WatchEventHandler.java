/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.monitor.jdefault;

import com.virtualcode.agent.das.fileSystems.ntfs.NTFSTask;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.monitor.archiver.ActiveFileArchiver;
import com.virtualcode.agent.monitor.jdefault.dataobjects.DOManagedFile;
import com.virtualcode.agent.monitor.jdefault.dataobjects.EventEntry;
import com.virtualcode.agent.monitor.jdefault.dataobjects.WatchedFileAction;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import org.apache.log4j.Logger;

/**
 *
 * @author YAwar
 */
public class WatchEventHandler implements Runnable {
    
    private Logger log = null;
    private WatchedFileAction watchFileAction;
    private EventEntry event;
    private DOManagedFile managedFolder;
    
    public WatchEventHandler(WatchedFileAction watchFileAction, EventEntry event, DOManagedFile managedFolder) {
        this.watchFileAction    =   watchFileAction;
        this.event  =   event;
        this.managedFolder    =   managedFolder;
        this.log    =LoggingManager.getLogger(LoggingManager.ACTIVE_ARCHIVING_DETAIL, managedFolder.getJobName());
    }
    
    @Override
    public void run() {
        BasicFileAttributes attrs;
        NTFSTask    task    =   null;
        ActiveFileArchiver afa;
        try {
            Path eventOn    =   this.event.pathObj;
            log.debug("WatchEventHandler invoked for "+eventOn.toString());
            attrs   =   Files.readAttributes(eventOn, BasicFileAttributes.class);
            task    =   new NTFSTask(eventOn, attrs, managedFolder.getJobName(), "2",null,null,managedFolder.isDeduplicatedVolume(),null);//hardcoded syncMode=2 (means sync with dest)
            afa     =   new ActiveFileArchiver(task, this.managedFolder);
            afa.process();
            
        } catch(FileNotFoundException fnf) {
            log.warn(String.format("%s is not readable...[%s]" , ((task!=null)?task.getPathStr():"File"), fnf.getMessage() ));
            
        } catch (IOException ioe) {
            log.error("ERROR ioe: " + ioe.getMessage());
            ioe.printStackTrace();
            
        } catch (Exception e) {
            
            boolean isNormal    =   false;
            try {
                throw e.getCause();//throw the cause of exception
            } catch (FileNotFoundException fnf) {
                isNormal    =   true;
                log.warn(String.format("%s is not readable...[%s]" , ((task!=null)?task.getPathStr():"File"), fnf.getMessage() ));
            } catch (Throwable ce) {
            }
            
            if(!isNormal) {
                log.error("ERROR: " + e.getMessage());
                e.printStackTrace();
            }
            
        } finally {
            attrs   =   null;
            task    =   null;
            afa     =   null;
            
            this.watchFileAction=   null;
            this.event  =   null;
        }
    }
}
