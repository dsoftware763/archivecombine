/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.monitor.archiver;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.dataArchiver.FileExecutors;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;

import com.virtualcode.agent.das.utils.LocalMachineUtil;
import com.vcsl.sa.Acl;
import com.virtualcode.agent.das.dataArchiver.EvalutorFlag;
import com.virtualcode.agent.das.fileSystems.ntfs.NTFSTask;
import com.virtualcode.agent.monitor.jdefault.dataobjects.DOManagedFile;
import executors.DataLayer;
import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.attribute.BasicFileAttributes;
import org.apache.log4j.Logger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.activation.DataHandler;
/**
 *
 * @author YAwar
 */
public class ActiveFileArchiver {
    
    private Logger logger = null;//
    private NTFSTask task  =   null;
    private DOManagedFile managedFolder =   null;
    private int retryCtr    =   0;
    
    public ActiveFileArchiver(NTFSTask task, DOManagedFile managedFolder) {
        this.task   =   task;
        this.managedFolder  =   managedFolder;
        logger  =   LoggingManager.getLogger(LoggingManager.ACTIVE_ARCHIVING_DETAIL, managedFolder.getJobName());
    }
    
    public boolean process() throws Exception {
        boolean archived  =   false;
        
        boolean evaluated   =   this.evaluate();
        if(evaluated) {
            try {
                archived  =   this.archive();
            } catch (Exception e) {
                //move to a TEMP directory
                this.moveFile();
                
                throw e;//re-throw the exception to disrubt the execution
            }
        }
        
        String actionType   =   this.managedFolder.getActionType();
        if(archived) {//if archived successfully
            if("2".equals(actionType)) {
                //archive & stub
                this.stub();

            } else if("3".equals(actionType)) {
                //archive & delete
                this.deleteFile();

            } else if("1".equals(actionType)) {
                //archive a copy only
            }
            
        } else if(evaluated && !archived){//if Evaluated but Failed to archive
            
            if("2".equals(actionType)) {
                //move to a TEMP directorys
                this.moveFile();

            } else if("3".equals(actionType)) {
                //move to a TEMP directorys
                this.moveFile();

            } else if("1".equals(actionType)) {
                //move to a TEMP directorys
                this.moveFile();
            }
        }
        
        return archived;
    }
    
    private boolean moveFile() throws Exception {
        String src  =   task.getPathStr();
        String dest =   Utility.getAgentConfigs().getTempLocation();
        
        if(dest!=null && !dest.isEmpty() && !"none".equals(dest)) {//if its configured to MoveFiles
            
            dest    =   dest.replace("\\", "/");//replace All backslashes with FWD slash...
        
            int rootPathLength  =   managedFolder.getAbsolutePath().length();
            if(rootPathLength < src.length()) {
                dest    =   dest + "/" + src.substring(rootPathLength);
                dest    =   dest.replaceAll("//", "/");
            }
            logger.info(task.getUid() + " Moving : Dest Path : " + dest);

            Path srcPath  =   FileSystems.getDefault().getPath(src);
            Path destPath  =   FileSystems.getDefault().getPath(dest);

            File parentFolder   =   destPath.toFile().getParentFile();
            if(!parentFolder.exists()) {
                logger.info(task.getUid() + " Creating dir structure");
                parentFolder.mkdirs();            
            }
            Files.move(srcPath, destPath);
            
        } else {
            logger.info(task.getUid() + " Failed files are NOT configured to be moved at Temp location");
        }
        
        return true;
    }
    
    private boolean deleteFile() throws Exception {
        Path p = FileSystems.getDefault().getPath(task.getPathStr());
        Files.delete(p);
        logger.info(task.getUid() + " Delete : Path : " + task.getPathStr());
        
        return true;
    }
    
    private boolean stub() throws Exception {

       //boolean output = task.createInternetShortcut(task.getPathStr(), task.getSecureRepoPath());
        Job currentJob = FileExecutors.currentExecs.get(task.getExecID());
        boolean output = task.createInternetShortcut(task.getPathStr(), task.getSecureRepoPath(),currentJob);

        logger.info(task.getUid() + " Stubber : Output : " + output);
        
        return output;
    }
    
    private boolean evaluate() throws Exception {

        Path file = task.getPath();
        Path name = file.getFileName();
        logger.info(task.getUid().toString() + " : Evaluating : " + file.getFileName());
        PathMatcher matcher;
        String extensions = "*";
        String[] exts = extensions.split(",");

        if (exts.length == 0) {
            exts = "*.*,*.*".split(",");
        }

        for (String s : exts) {
            if (s == null || s.isEmpty()) {
                //   rp.skippedNullEmpty += 1;
                continue;
            }
            matcher = FileSystems.getDefault().getPathMatcher("glob:" + s);
            if (name != null && matcher.matches(name)) {
                logger.info(task.getUid().toString() + " : Matched : " + s);
                task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND;
                if (file.getFileName()==null
                        || file.getFileName().toString().isEmpty()
                        || file.getFileName().toString().startsWith("~")
                        || file.getFileName().toString().startsWith("$")
                        //|| file.getFileName().toString().startsWith(".")
                        || file.getFileName().toString().endsWith(".lnk")
                        || file.getFileName().toString().endsWith(".tmp")
                        || file.getFileName().toString().endsWith(".url")) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_TEMPORARY;
                    logger.info(" Link File OR Temporary File OR Office temporary File : " + file);
                    return false;

                }

                BasicFileAttributes attrs = task.getAttributes();
                if (attrs.isDirectory()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_ISDIR;
                    logger.info(task.getUid() + " Is a Directory " + file);
                    return false;

                }

                if (attrs.isSymbolicLink()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_SYMBOLLINK;
                    logger.info(task.getUid() + " Is a Symbolic Link " + file);
                    return false;

                }

                if (attrs.isOther()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_SYMBOLLINK;
                    logger.info(task.getUid() + " Is a Other File, no idea what does it mean, may be not regular " + file);
                    return false;
                }
                
                /*
                if (!ProcessHD && file.toFile().isHidden()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_HIDDENLECTED;
                    logger.info(task.getUid() + "Is a Hidden File " + file);
                    return false;
                }

                if (!ProcessRO && !file.toFile().canWrite()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_READ_ONLY;
                    logger.info(task.getUid() + "Is a Readonly File " + file);
                    return false;
                }


                if (docSize > 0 && attrs.size() < docSize) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_SMALL_SIZE;
                    logger.info(task.getUid() + "File Size : ( " + attrs.size() + " ) " + " Required Size : ( " + docSize + " ) ");
                    return false;
                } else if (docAge > 0 && attrs.creationTime().toMillis() > docAge) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_AGE;
                    logger.info(task.getUid() + "File Creation Time : ( " + attrs.creationTime().toMillis() + " ) " + " Required Time : ( " + docAge + " ) ");
                    return false;
                } else if (docLA > 0 && attrs.lastAccessTime().toMillis() > docLA) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_LASTACCESSTIME;
                    logger.info(task.getUid() + "File Last Access Time : ( " + attrs.lastAccessTime().toMillis() + " ) " + " Required Time : ( " + docLA + " ) ");
                    return false;
                } else if (docLM > 0 && attrs.lastModifiedTime().toMillis() > docLM) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_LASTMODIFIEDTIME;
                    logger.info(task.getUid() + "File Last Modified Time : ( " + attrs.lastModifiedTime().toMillis() + " ) " + " Required Size : ( " + docLM + " ) ");
                    return false;
                } else {
                    return false;
                }*/
            }
        }
        return true;
    }
    
    private boolean archive() throws Exception {
        
        retryCtr++;
        
        String documentPath = task.getNodeHeirarchy();
        //String actualPath   =   new String(documentPath);
        String actualPath    =   task.getPathStr();
        String sharePath    =   this.managedFolder.getSharePath();
        String printerName  =   null;
        String machineName  =   null;
        String username     =   null;
        String totalPages   =   null;
        
        //String documentPath =   "c:/test$/folder/p-path_/58$yawar$machine.name$printerName$YYYY$MM$DD$&hello-_ how are you.txt";
        if("4".equals(this.managedFolder.getActionType()) && documentPath.contains("$&")) {
            //case of Print Archiving

            String parentDirs   =   documentPath.substring(0, documentPath.lastIndexOf("/"));
            String docName      =   documentPath.substring(documentPath.lastIndexOf("/")+1);

            String temp1    =   docName.substring(0, docName.indexOf("$&"));
            docName         =   docName.substring(docName.indexOf("$&")+2);

            temp1    =   temp1.replaceAll("[$]", "/");//  58$yawar$machine.name$printerName$YYYY$MM$DD

            totalPages  =   temp1.substring(0, temp1.indexOf("/"));
            temp1   =   temp1.substring(temp1.indexOf("/")+1);//yawar$machine.name$printerName$YYYY$MM$DD

            username  =   temp1.substring(0, temp1.indexOf("/"));
            temp1   =   temp1.substring(temp1.indexOf("/")+1);//machine.name$printerName$YYYY$MM$DD
            String tempDocPath  =   temp1;

            machineName  =   temp1.substring(0, temp1.indexOf("/"));
            temp1   =   temp1.substring(temp1.indexOf("/")+1);//printerName$YYYY$MM$DD

            printerName  =   temp1.substring(0, temp1.indexOf("/"));
            temp1   =   temp1.substring(temp1.indexOf("/")+1);//YYYY$MM$DD

            //documentPath    =   parentDirs + "/" + temp1 + "/" +docName;
            documentPath    =   tempDocPath + "/" +docName;
            logger.info(task.getUid() + " File path for archiving is: " + documentPath);

            logger.info(task.getUid() + " printerName: "+printerName+", "+" machineName:"+machineName+", "+"user:"+username+", pages:"+totalPages);
            
        } else {//case of Active Archiving.. 
            
            if(actualPath.length() > managedFolder.getAbsolutePath().length()) {
                
                if(sharePath!=null && !sharePath.isEmpty()) {
                    //Convert the Absolute path to SharePath - for archiving in accordance with SharePath
                    documentPath    =   actualPath.substring(this.managedFolder.getAbsolutePath().length());
                    documentPath    =   sharePath + "/" + documentPath;
                    logger.info(task.getUid() + " Document path with SharePath : " + documentPath);
                    
                } else {//sharePath is set to empty, in creation of Job
                    documentPath    =   actualPath;//set the absolute path as SPURL
                    
                    //Get the IPAdr of current machine (on which the Agent is running)
                    String agentIP      =   LocalMachineUtil.getOwnIPAdr();
                    documentPath        =   agentIP + "/" + documentPath;                    
                    logger.info(task.getUid() + " Document path with SharePath : " + documentPath);
                }
                                
                if(documentPath.contains("@")) {//if Smb path is inserted in JobCreation as SharePath
                    //Remove the string before @ SIGN
                    documentPath       =   documentPath.substring(documentPath.indexOf("@")+1);
                    
                }
                documentPath        =   documentPath.replace(":", "").replace("\\\\", "").replace("\\", "/").replaceAll("//", "/"); 
                logger.info(task.getUid() + " SP URL becomes: " + documentPath);
            }
            /* /if(sharePath!=null && !sharePath.isEmpty()) {
                //use SharePath.. So that the nodes should be Restored back to orignal location
            //    sharePath   =   sharePath.substring(sharePath.indexOf("@")+1);
            //    documentPath    =   documentPath.replace(":", "").replace("\\\\", "").replace("\\", "/");
                logger.info("Share path for archiving is: " + documentPath);
                logger.info("Actual path for archiving is: " + actualPath);
            //}*/
        }
        
        //set the user credential to write some file in repository
        String authString   = "repoName="+Utility.getAgentConfigs().getCompany().getCompanyTag()+ "&username=" + Utility.getAgentConfigs().getLogin() + "&password=" + Utility.getAgentConfigs().getPassword() + "";
        task.setEncodedString(new String(Utility.encodetoBase64(authString.getBytes())) );
            
        String layers = "";

        Date modifiedDate = task.getLastModifiedDate();
        Date createdDate = task.getCreationDate();
        Date accessedDate = task.getLastAccessedDate();

        //We can use H for 24 hr formatting and h for 12hr formating
        //e.g. yyyy.MM.dd G 'at' HH:mm:ss z
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String owner = task.getOwner();
                
        layers += "ModifiedOn=" + df.format(modifiedDate) + "|";
        layers += "CreatedOn=" + df.format(createdDate) + "|";
        layers += "ModifiedBy=" + owner + "|";
        layers += "CreatedBy=" + owner + "|";
        layers += "AccessedOn=" + df.format(accessedDate) + "|";
        layers += "SourceAgent=FS|";
        layers += "AgentType=FS|";
        layers += "syncMode="+ task.getSyncMode() +"|";
        //layers += "sessionRepository="+Utility.GetProp("sessionRepository") +"|";
        layers += "archivingMode=dynamic|";
        if(printerName!=null && totalPages!=null) {//case of Print Archiving
            layers += "printerName=" + printerName + "|";
            layers += "machineName=" + machineName + "|";
            layers += "username=" + username + "|";
            layers += "totalPages=" + totalPages + "|";
        }
        //layers += "AgentName="+Utility.GetProp("AgentName") +"|";
        ////layers += "SPURL=" + task.getPathStr().replace(":", "") + "|";
        layers += "SPURL=" + documentPath + "|";
        layers += ("secureInfo=" + task.getEncodedString());//.encodedString);
        layers = layers.trim();

        logger.info(task.getUid() + " Layers : " + layers);

        String path64           = new String(Utility.encodetoBase64(documentPath.getBytes()));
        
        String DASUrl = null;
        //String isAlreadyArchived = "false";//by default - NOT already archived
        if(Utility.getAgentConfigs().isLazyArchive()) {//if LazyArchiving
            logger.info(task.getUid() + " trying for lazy archive ");
            DASUrl = DataLayer.documentExistsByProperties(path64, layers);
        }

        if(DASUrl == null || DASUrl.isEmpty()) {//if NOT already archived - and needs to archive
            logger.info(task.getUid() + " Uploading dataHandler ");
            DataHandler dataHandler =   task.getDataHandler();

            Acl acl =   task.getACL(actualPath);//Get ACE list

            DASUrl = DataLayer.uploadDocument(path64, dataHandler, layers, acl,Utility.getAgentConfigs().getUploadRequestTimeout(),Utility.getAgentConfigs().getUploadConnectionTimeout());

            dataHandler = null;
    //        dataSource = null;
    //        filePath = null;
        } else {
            logger.info(task.getUid() + " Already archived "+DASUrl);
        }
        
        path64 = null;
        layers = null;
        df = null;
        createdDate = null;
        modifiedDate = null;
        accessedDate = null;
        documentPath = null;
        owner = null;
        logger.info(task.getUid() + " Archive URL  : " + DASUrl);
                
        if (DASUrl != null && !DASUrl.isEmpty()) {
            task.setSecureRepoPath(DASUrl);   //task.repoPath =   DASUrl;
            DASUrl = null;
            return true;
            
        } else {
            logger.warn(task.getUid() + " Archive Attemp ["+retryCtr +"] failed as "+task.getTaskKpi().isArchived);
            int maxArchiveTries	=	(Utility.getAgentConfigs().getMaxNoOfTries()!=null)?Utility.getAgentConfigs().getMaxNoOfTries():2;
            if((DASUrl==null || DASUrl.isEmpty())&& retryCtr <maxArchiveTries ) {
                logger.info(task.getUid() + " Retrying for Archiver Process");
                return this.archive();
            }
        }

        DASUrl = null;
        return false;
    }
}
