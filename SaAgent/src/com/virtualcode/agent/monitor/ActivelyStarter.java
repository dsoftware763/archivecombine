/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.monitor;

import executors.DataLayer;
import com.virtualcode.agent.das.archivePolicy.dto.ActiveJob;
import com.virtualcode.agent.das.logging.LoggingManager;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import java.util.HashMap;
import java.util.Iterator;
import org.apache.log4j.Logger;
/**
 *
 * @author Yawar
 */
public class ActivelyStarter implements Runnable {
    
    private String AgentName    =   null;
    private String actionType   =   null;
    
    public ActivelyStarter(String AgentName) {
        this.AgentName  =   AgentName;
        this.actionType =   "ACTIVEARCH";
    }
    
    @Override
    public void run() {
        System.out.println("ACTIVELY: "+AgentName + " waiting for Active/Print Job...");
        try {
            
            HashMap<String, ActiveJob> alreadyActiveJobs   =   new HashMap<String, ActiveJob>();
            while (true) {
                try {
                    //refresh the Paths to be monitored list
                    HashMap<String, ActiveJob> newActiveJobs  =   DataLayer.getJobsToMonitor(alreadyActiveJobs, AgentName, actionType);
                    
                    if(newActiveJobs!=null && newActiveJobs.size()>0) {
                        Iterator iter = newActiveJobs.keySet().iterator();                    
                        while(iter.hasNext()) {

                            String key  = (String)iter.next();
                            ActiveJob j = (ActiveJob)newActiveJobs.get(key);

                            //logger is identified uniquely on the basis of JobName (in case of Activ Archiving jobs)
                            LoggingManager.configureLogger(-1, j.getJobName(), LoggingManager.ACTIVE_ARCHIVING_DETAIL);
                            Logger logger = LoggingManager.getLogger(LoggingManager.ACTIVE_ARCHIVING_DETAIL, j.getJobName());

                            try {

                                ActivelyExecutor ae = new ActivelyExecutor();
                                ae.startJob(j);
                                ae = null;

                            } catch (Exception ex) {

                                logger.fatal("ActivelyArchiveStarter : " + ex);
                                Writer writer = new StringWriter();
                                PrintWriter printWriter = new PrintWriter(writer);
                                ex.printStackTrace(printWriter);

                                logger.error("ActivelyArchiveStarter : " + "\n" + writer.toString());
                                writer = null;
                                printWriter = null;

                            } finally {
                                //Utility.resetUtilityResourceBundles();
                                ////TEST COMMENT, to see affects...  
                                ////LoggingManager.shutdownLogging(executionID);
                                //System.gc();
                            }
                        }                        
                    } else {//if newActiveJobs==null
                        System.out.println("ACTIVELY: "+"New Directory not found to monitor..");
                    }
                    
                    System.out.println("ACTIVELY: "+"Seeking for next Directory to Monitor...");
                    
                    //reset the activeJobs list to blank - TEMPORARILY
                    newActiveJobs  =   null;
                    
                } catch(Exception ex) {//if WS is down at Server side, then dont' Terminate...
                    ex.getMessage();
                    ex.printStackTrace();
                }
                System.gc();
                Thread.currentThread().sleep(1000 * 60);//after 60 secs
            }
            
        } catch (InterruptedException ex) {//if Thread.sleep is interupted
            ex.getMessage();
            ex.printStackTrace();
        }
    }
}
