/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.monitor.nio;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;

public class TestFolderWatcher {

    public static void main(String args[]) throws Exception {
        testFutureWatchService("D:/Sharearchiver/tempFolder");
        System.out.println("Main completed");
    }

    public static void testFutureWatchService(String path) throws IOException {
        FolderWatchers.getInstance().addFolderListener(Paths.get(path),
				new ChangeListener(ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY) {
					@Override
					public void onEvent(WatchEvent<Path> anEvent) {
                                            
                                            ChangeHandler handler  =   new ChangeHandler(anEvent,"testEvent");
                                            Thread handle     =   new Thread(handler);
                                            handle.start();
					}
				});
        /*
        FolderWatchers.getInstance().addFolderListener(Paths.get(path),
                        new ChangeListener("test", ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY) {
                                @Override
                                public void onEvent(WatchEvent<Path> anEvent) {
                                        System.out.println("LISTENER 1 " + anEvent.kind().name().toString()
                                                        + " " + anEvent.context());
                                }
                        });
        /*
        FolderWatchers.getInstance().addFolderListener(Paths.get("D:/Sharearchiver/tempFolder/"),
                        new ChangeListener(ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY) {
                                @Override
                                public void onEvent(WatchEvent<Path> anEvent) {
                                        System.out.println("LISTENER 2 " + anEvent.kind().name().toString()
                                                        + " " + anEvent.context());
                                }
                        });
        while (true) {
                ;
        }
         */
    }
}