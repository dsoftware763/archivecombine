/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.monitor.nio;

/**
 *
 * @author YAwar
 */
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;

public abstract class ChangeListener {

    private Kind[] eventTypes = null;
    
    public ChangeListener(Kind... eventTypes) {
        this.eventTypes = eventTypes;
    }

    public Kind[] getEventTypes() {
        return eventTypes;
    }

    public abstract void onEvent(WatchEvent<Path> anEvent);
}