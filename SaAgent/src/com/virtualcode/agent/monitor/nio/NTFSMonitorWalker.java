/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.monitor.nio;

import java.nio.file.WatchEvent;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.agent.monitor.nio.ChangeHandler;
import com.virtualcode.agent.monitor.nio.ChangeListener;
import com.virtualcode.agent.monitor.nio.FolderWatchers;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.FileSystemException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import org.apache.log4j.Logger;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

/**
 *
 * @author Abbas
 */
public class NTFSMonitorWalker extends SimpleFileVisitor<Path> {

    private ArrayList<String> excPathList;
    private Logger logger   =   null;
    private String execID   =   null;
    
    public NTFSMonitorWalker(ArrayList<String> excPathList, String execID) {
        this.excPathList    =   excPathList;
        this.execID =   execID;
        logger  =   LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL, execID);
    }
    
    @Override
    public FileVisitResult preVisitDirectory(Path filePath, BasicFileAttributes attr) {
        //Skip the Dir, if its added in Exculed list by User...
        
        if(Utility.containsIgnoreCase(excPathList, filePath.toString())) {
            logger.info("Skipped the excluded path: " + filePath.toString());
            return FileVisitResult.SKIP_SUBTREE;
            
        } else {
            
            try {
                logger.info("Monitoring... "+filePath.toString());
                
                //Introduce Event Listener on current folder
                FolderWatchers.getInstance().addFolderListener(filePath,
                        new ChangeListener(ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY) {
                            @Override
                            public void onEvent(WatchEvent<Path> anEvent) {
                                //registering multithreaded handler...
                                ChangeHandler ch  =   new ChangeHandler(anEvent, execID);
                                Thread handler     =   new Thread(ch);
                                handler.start();
                            }
                        });
                
            } catch (IOException ioe) {
                logger.error("Error in monitoring: "+ioe.getMessage());
                ioe.printStackTrace();
            }
            
            return FileVisitResult.CONTINUE;
        }
    }
    
    @Override
    public FileVisitResult visitFile(Path filePath, BasicFileAttributes attrs) {
        
        /*FileExecutors.fileWalkedCount += 1;
        NTFSTask task = new NTFSTask(filePath, attr);
        System.out.println("visiting NTFS: "+filePath);
        FileExecutors.fileEvaluator.execute(new NTFSEvaluater(task));
        */
        
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
        if (exc instanceof AccessDeniedException) {
            System.err.println(dir + ": cannot access directory");
        }
        System.err.println(exc);

        return FileVisitResult.CONTINUE;
    }

    public FileVisitResult preVisitDirectoryFailed(Path dir, IOException exc) {
        if (exc instanceof AccessDeniedException) {
            System.err.println(dir + ": cannot access directory");
        }
        System.err.println(exc);

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) {
        if (exc instanceof FileSystemException) {
            System.err.println(file.toString() + ": The device is not ready");
        }
        System.err.println(exc);

        return FileVisitResult.CONTINUE;
    }
}
