/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.monitor.nio;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.monitor.archiver.ActiveFileArchiver;
import com.virtualcode.agent.das.fileSystems.ntfs.NTFSTask;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import com.virtualcode.agent.das.logging.LoggingManager;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import org.apache.log4j.Logger;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
/**
 *
 * @author YAwar
 */
public class ChangeHandler implements Runnable {
    
    private WatchEvent<Path> anEvent    =   null;
    private Logger logger   =   null;
    private String execID   =   null;
    
    public ChangeHandler(WatchEvent<Path> anEvent, String execID) {
        this.anEvent    =   anEvent;
        logger  =   LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL, execID);
        execID  =   execID;
    }
    
    @Override
    public void run() {
        
        try {
            String eventType    =   anEvent.kind().name();
            Path eventOn        =   anEvent.context();
            String eventID      =   anEvent.toString().hashCode()+""+eventType.hashCode()+""+System.currentTimeMillis();

            logger.info("HANDLER INVOKED:  "+ eventType +" "+eventID
                    + " on " + eventOn.getFileName());
            
            if(eventOn.toFile().isDirectory()) {//case of event on some directory

                if(ENTRY_CREATE.toString().equals(eventType)) {
                    //apply a change monitor on this new dir as well

                } else if(ENTRY_DELETE.toString().equals(eventType)) {
                    //remove the change monitor from this directory, and its sibling dirs

                } else if(ENTRY_MODIFY.toString().equals(eventType)) {
                    //do nothing

                } else {
                    logger.warn("Unhandled EVENT on Dir: "+eventType);
                }

            } else {//case of files under some directory            
                
                Thread.sleep(10000);

                if(ENTRY_CREATE.toString().equals(eventType)) {
                    //archive the newly created file
                    //BasicFileAttributes attrs   = Files.readAttributes(eventOn, BasicFileAttributes.class);
                    //NTFSTask    task   =   new NTFSTask(eventOn, attrs);
                    //this.initFileArchiver(task);

                } else if(ENTRY_DELETE.toString().equals(eventType)) {
                    //do nothing...

                } else if(ENTRY_MODIFY.toString().equals(eventType)) {
                    //archive the file, as per rules (archive app will auto generate its version ;)
                    
                    BasicFileAttributes attrs   = Files.readAttributes(eventOn, BasicFileAttributes.class);
                    NTFSTask    task   =   new NTFSTask(eventOn, attrs, execID, "2", null, null, false,null);//syncMode=2 (destination syncronization)
                    this.initFileArchiver(task);

                } else {
                    logger.warn("Unhandled EVENT on File: "+eventType);
                }
            }
            
        } catch (Exception e) {
            logger.error("Event coult'nt be processed: "+e.getMessage());
            e.printStackTrace();
        }
    }
    
    private boolean initFileArchiver(NTFSTask task) throws Exception {
        /*DISABLED METHOD
        ActiveFileArchiver afa  =   new ActiveFileArchiver(task);
        return afa.process();
         * 
         */
        return true;
    }
}
