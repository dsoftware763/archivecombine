/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.monitor;

import executors.DataLayer;
import com.virtualcode.agent.das.archivePolicy.dto.ActiveJob;
import com.virtualcode.agent.das.archivePolicy.dto.ExcludedPath;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSService;
import com.virtualcode.agent.das.fileSystems.FileServiceInterface;
import com.virtualcode.agent.das.fileSystems.ntfs.NTFSService;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.archivePolicy.dto.SPDocLib;
import com.virtualcode.agent.das.documentManagement.dto.DocumentType;
import com.virtualcode.agent.monitor.jdefault.NTFSMonitorService;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;

/**
 *
 * @author Abbas
 */
public class ActivelyExecutor {

    public static long fileWalkedCount = 0L;
    void startJob(ActiveJob job) throws InterruptedException, Exception {
        Logger logger = LoggingManager.getLogger(LoggingManager.ACTIVE_ARCHIVING_DETAIL, job.getJobName());
        int errorCodeID = 0;
        int jID = -1;
        fileWalkedCount = 0L;

        try {
            logger.info("Job Info");
            logger.info("=======");
            //logger.info("Execution ID " + executionID);
            logger.info("Job " + job.toString());
            
            if(job.getAbsolutePath()!=null && !job.getAbsolutePath().isEmpty()) {

                NTFSMonitorService    fs  =   null;
                //if(job.getAbsolutePath().toLowerCase().contains("smb://")) {//if the path is type CIFS
                //    fs  =   new CIFSService();

                //} else {//if the path is type NTFS
                    fs  =   new NTFSMonitorService();
                //}

                //Prepare the list of Excuded sub-paths of current Path
                ArrayList<String> excPathList   =   new ArrayList();
                //for(ExcludedPath ep : p.getExcludedPathSet()) {
                //    excPathList.add(ep.getPath());
                //}  
                fs.monitorFileTree(job);
                
            } else {
                logger.error("Incorrect path entry, so Ignoring the path to be Monitored ");
            }

        } catch (Exception ex) {
            errorCodeID = 1;
            
            logger.fatal("JobID: " + jID);
            logger.fatal("ErrorID: " + errorCodeID);
            logger.fatal(ex.getMessage());
            Writer writer = new StringWriter();
            PrintWriter printWriter = new PrintWriter(writer);
            ex.printStackTrace(printWriter);

            LoggingManager.getLogger(LoggingManager.ACTIVITY_ERROR, job.getJobName()).error("Fatal : " + "\n" + writer.toString());
            writer = null;
            printWriter = null;

        } finally {
            //fileEvaluator = null;
            //fileStubber = null;
            ////fileDedupChecker = null;
            //fileArchiver = null;
            //statisticsCalculator = null;
            //currentJob = null;
        }
    }
}
