/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.logging;

import com.virtualcode.agent.das.utils.Utility;

import executors.Main;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import org.apache.log4j.Logger;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;
/**
 *
 * @author Abbas
 */
/**
  In our project, I would use logging as follows:

  1- At a time, only one job runs so I still would have 2 static objects for error and activity logger as i created here.
  2- Once a job is selected to process, I'll call configureLoggers and pass certain parameters for creating log in hierarchy as pointed out in slide-3 of "logging-mechanism":
     2.1 - For the file name of the activity log, i'll give the file path as job-name/activityLog/detail.log
     2.2 - For the error log, i'll give the file path as job-name/errorLog/error.log
     2.3 - For the task-summary log, i'll give teh file path as job-name/summaryLog/task-summary.log
  3- Once loggers are configured, they are ready to be called anywhere in the program by static function Logger.getLogger() as used in the code here.
  4- We need to pass Task-ID and ExecutionID in every log message that we store in log. For additional infomration, see logging-mechanism.pptx.
  5- Please suggest, if you want to make any changes in the behavior of this implementation.
 
  ** Things that we configure in the configure*Logger functions can obviously be placed in the configuration
     file and loaded from there. We can give only limited configurations e.g., the size of file, maximum file-size,
     name of the log file etc.
     I would not give user the control to configure the hierarchy of logging.
     However, I would allow him to configure the logging level.
 
  ** We need to ensure that any and all run time exception traces are logged in the error log, if they ever occur.
  ** If that's not possible, then those errors should still be logged in some exception_trace_error.log file.
 */

public class LoggingManager  {

    public final static String ACTIVITY_DETAIL  = "ACTIVITY_DETAIL";
    public final static String ACTIVITY_ERROR   = "ACTIVITY_ERROR";
    public final static String TASK_SUMMARY     = "TASK_SUMMARY";
    public final static String JOB_SUMMARY      = "JOB_SUMMARY";
    public final static String PAUSE_RESUME_DETAIL = "PAUSE_RESUME_DETAIL";
    public final static String ACTIVITY_DB_ERROR = "ACTIVITY_DB_ERROR";

    public final static String EXP_ACTIVITY_DETAIL  = "EXP_ACTIVITY_DETAIL";
    public final static String EXP_ACTIVITY_ERROR   = "EXP_ACTIVITY_ERROR";
    public final static String EXP_TASK_SUMMARY     = "EXP_TASK_SUMMARY";
    public final static String EXP_JOB_SUMMARY      = "EXP_JOB_SUMMARY";

    public final static String STARTUPLOGS	=	"STARTUPLOGS";

    public final static String ACTIVE_ARCHIVING_DETAIL  =   "ACTIVE_ARCHIVING_DETAIL";
    
    /*
    public static void shutdownLogging(String execID) {
        //currentJobs.remove(execID);
        
        //if(currentJobs.isEmpty())//Shutdown the current Appendars of Logging, if there is'nt any job in execution
            LogManager.shutdown();
    }*/
    
    public static void configureLogger(long jobID, String execID, String loggerName) {
        //ServletContext  sc  =   StartAgentJobs.getContext();
    	String basePath	=	Utility.getAgentConfigs().getLogBasePath();
    	if(basePath==null || basePath.isEmpty())
    		basePath     =   "ErrorLog";
    	else
    		basePath	=	basePath;
        
    	String logLvl	=	Utility.getAgentConfigs().getLogLevel();
    	if(logLvl==null || logLvl.isEmpty())
    		logLvl	=	"ALL";
    	
        Logger loggerObj = Logger.getLogger(loggerName+execID);
        loggerObj.setLevel(Level.toLevel(logLvl));
        RollingFileAppender fileAppender = null;
        try {
            int maxBackups = Utility.getAgentConfigs().getMaxBackupIndex();//100
            String maxFileSize = Utility.getAgentConfigs().getMaxFileSize();//"5MB";
            String conversionPattern = Utility.getAgentConfigs().getConversionPattern();//"%d %-5p=>%m%n";
            PatternLayout layout = new PatternLayout(conversionPattern);
            fileAppender = new RollingFileAppender(layout, basePath+"/"+jobID+"/"+execID+"/"+loggerName+".log", true);
            fileAppender.setMaxBackupIndex(maxBackups);
            fileAppender.setMaxFileSize(maxFileSize);





            loggerObj.addAppender(fileAppender);

            
            //Print the Version details, as first line of the Job_Summary.log
            if(loggerName.contains(LoggingManager.JOB_SUMMARY)) {
                loggerObj.info("Commercial Version ID = xx, Major Version ID = xx, Minor Version = xx");
                loggerObj.info("Commercial Version V xx.xx, RD "+Main.versionStr);
            }
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            loggerObj.addAppender(new ConsoleAppender(new PatternLayout(), "System.err"));
        } finally {
            //currentJobs.add(execID);
        }
    }
    
    public static Logger getLogger(String loggerName, String execID) {
        return Logger.getLogger(loggerName + execID);
    }
    /*
    public static Logger getLogger(String loggerName) {
        return Logger.getLogger(loggerName);
    }*/
    
    public static void printStackTrace(Exception ex, Logger logger) {
        
        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        ex.printStackTrace(printWriter);
        logger.error(result.toString());
    }
    
    public static void main(String argv[]) {
        String execID   =   "0";
        LoggingManager.configureLogger(0L,execID, EXP_ACTIVITY_DETAIL);
        LoggingManager.configureLogger(0L,execID, EXP_ACTIVITY_ERROR);
        LoggingManager.configureLogger(0L,execID, EXP_JOB_SUMMARY);
        LoggingManager.configureLogger(0L,execID, PAUSE_RESUME_DETAIL);
        LoggingManager.configureLogger(0L,execID, ACTIVITY_DB_ERROR);

        Logger detail = LoggingManager.getLogger(EXP_ACTIVITY_DETAIL,execID);
        Logger error = LoggingManager.getLogger(EXP_ACTIVITY_ERROR,execID);
        Logger info = LoggingManager.getLogger(PAUSE_RESUME_DETAIL,execID);
        for (int i = 0; i < 10; i++) {
            detail.warn("Logged " + i);
            error.debug("serious error raised in " + i);
            info.info("serious error raised in " + i);
        }
        //TEMPORARILY COMMENT....     
        //LoggingManager.shutdownLogging("0");
    }
}
