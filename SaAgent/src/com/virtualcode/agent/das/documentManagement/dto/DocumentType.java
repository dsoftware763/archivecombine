/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.documentManagement.dto;

/**
 *
 * @author faisal nawaz
 */
public class DocumentType implements java.io.Serializable {

    /*
     * *************************************************************************
     * fields
     * *************************************************************************
     */


    //  static fields
    private static final long serialVersionUID = 1051L;

    private int id;
    private String name;
    private String value;

    /*
     * *************************************************************************
     * constructor
     * *************************************************************************
     */
    public DocumentType() {
    }

    /*
     * *************************************************************************
     * getter
     * *************************************************************************
     */
    public int getId() {
        return id;
    }

    public String getTheName() {
        return name;
    }

    public String getTheValue() {
        return value;
    }

    /*
     * *************************************************************************
     * setter
     * *************************************************************************
     */
    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
