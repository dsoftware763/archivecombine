package com.virtualcode.agent.das.dataIndexer;

import com.vcsl.stats.IndexNode;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.fileSystems.FileTaskIndexingInterface;
import com.virtualcode.agent.das.threadsHandler.IndexingTPEList;
import com.virtualcode.agent.das.utils.MyHashMap;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.agent.das.archivePolicy.dto.Job;

import com.virtualcode.agent.das.utils.XMLGregorianCalendarConverter;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.ThreadPoolExecutor;
import org.apache.log4j.Logger;

/**
 *
 * @author Abbas
 */
public class FileIndexer implements Runnable {

    public static class Constants {
        public static String FILE_PATH	=	"filePath";
        public static String FILE_NAME	=	"fileName";
        public static String DATE_CREATE	=	"dateCreation";
        public static String DATE_MODIFY	=	"dateModification";
        public static String DATE_ACCESS	=	"dateLastAccess";
        public static String FILE_SIZE	=	"fileSize";
        public static String IS_ARCHIVED	=	"isArchived";
        public static String ALLOWED_SIDS	=	"allowedSids";
        public static String DENY_SIDS	=	"denySids";
        public static String SHARE_ALLOWED_SIDS	=	"shareAllowedSids";
        public static String SHARE_DENY_SIDS	=	"shareDenySids";
    }
    
    public FileIndexer(FileTaskIndexingInterface task) {
        this.task = task;
        logger  =   LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_DETAIL,task.getExecID());
    }
    //private final static AtomicLong executionCount = new AtomicLong(0);
    private static MyHashMap executionCount = new MyHashMap();
    FileTaskIndexingInterface task = null;
    Logger logger = null;

    private boolean Process() {
        boolean output  =   false;
        
        try {
            Job aJobDetail = IndexingExecutors.currentExecs.get(task.getExecID());
            String actionType       =	aJobDetail.getActionType();
            String activeActionType =	aJobDetail.getActiveActionType();
			
            IndexNode iNode	=	new IndexNode();

            String filePath	=	task.getPathStr();//task.getCIFSFile().getFileObj().getPath();
            if(!filePath.startsWith("smb://")) {//if its non Smb credentials path
                filePath    =   task.getNodeHeirarchy();//than get customized heirarchy of NTFS node
            }
            String fileName	=	task.getDocName();//task.getCIFSFile().getFileObj().getName();
            boolean isArchived	=	false;
            if(fileName.endsWith(".url") || task.isStub()) {//also index the stubs
                isArchived	=	true;
            }

            logger.info(this.task.getUid().toString() +" filePath: " + ((filePath!=null && filePath.contains("@"))?filePath.substring(filePath.indexOf("@")):filePath));
            iNode.setFilePath(filePath);//it will be used to Export File (so credentials required)

            logger.info(this.task.getUid().toString() +" fileName: " + fileName);
            iNode.setFileName(fileName);

            logger.info(this.task.getUid().toString() +" dateCreation: " + task.getCreationDate().getTime());
            iNode.setDateCreation(
                    XMLGregorianCalendarConverter.asXMLGregorianCalendar(task.getCreationDate()));

            logger.info(this.task.getUid().toString() +" dateModification: " +task.getLastModifiedDate().getTime());
            iNode.setDateModification(
                    XMLGregorianCalendarConverter.asXMLGregorianCalendar(task.getLastModifiedDate()));

            logger.info(this.task.getUid().toString() +" dateLastAccess: " + task.getLastAccessedDate().getTime());
            iNode.setDateLastAccess(
                    XMLGregorianCalendarConverter.asXMLGregorianCalendar(task.getLastAccessedDate()));

            logger.info(this.task.getUid().toString() +" fileSize: " +  task.getBytesCount());
            iNode.setFileSize(this.task.getBytesCount());

            logger.info(this.task.getUid().toString() +" isArchived: " +  isArchived);
            iNode.setIsArchived(isArchived+"");

            iNode.setExecID(task.getExecID());
            
            if("INDEXING".equalsIgnoreCase(actionType)) {
                logger.info(this.task.getUid().toString() +" case of IndexerManager for indexing..");
            	/*** ACL is always NULL for NTFS files
                CifsUtil	cifsUtil	=	new CifsUtil();
                Acl	acl	=	cifsUtil.getACL(task.getCIFSFile().getFileObj());
                if(acl!=null) {
                    HashMap<String, String> aclAttribs	=	CifsUtil.getAclAttribs(acl);
                    //logger.info(this.task.getUid().toString() +" VC_ALLOWED_SIDS: " +  aclAttribs.get(CustomProperty.VC_ALLOWED_SIDS));
                    iNode.setAllowedSids(aclAttribs.get(CustomProperty.VC_ALLOWED_SIDS));
                    iNode.setDenySids(aclAttribs.get(CustomProperty.VC_DENIED_SIDS));
                    iNode.setShareAllowedSids(aclAttribs.get(CustomProperty.VC_S_ALLOWED_SIDS));
                    iNode.setShareDenySids(aclAttribs.get(CustomProperty.VC_S_DENIED_SIDS));
                }*/

                //String extractContent	=	Utility.getAgentConfigs().getExtractContent();
                //if("yes".equalsIgnoreCase(extractContent)) {//Content extraction is optional
                if("WITHCONTENT".equalsIgnoreCase(activeActionType)) {//Content extraction is optional
                    //read the contents by tika API
                    
                    logger.info(this.task.getUid().toString() +" accessing binary for content and metadata: "+activeActionType);
                    String content	=	task.getReadableContent();
                    String metaData	=	task.getMetaData();

                    iNode.setReadableContent(content);
                    iNode.setMetaData(metaData);
                }

                //finally add it into IndexWriter
                logger.info(this.task.getUid().toString() +" indexID: "+task.getIndexID());
                IndexerManager.addDocument(task.getIndexID(), iNode, logger);
            }
            
            // also update the Stats - for summary/reporting
            if(!AnalysisManager.updateStats(iNode, task.getIndexID(), logger))//if returned FALSE
                logger.info(this.task.getUid().toString() +" saStats not exist for Indexing Counts...");
                        
            output  =   true;
            logger.info(this.task.getUid().toString() +" Added: " + fileName);
            
        } catch (Exception exp) {
            logger.error(this.task.getUid().toString() + " exp "+exp.getMessage());
            LoggingManager.printStackTrace(exp, logger);
        }        
        
        return output;
    }
      
    public static void resetExecutionCtr(String execID) {
        executionCount.put(execID, new AtomicLong(0));
    }
    public static String getExecutionCount(String execID) {
        return executionCount.get(execID);
    }

    @Override
    public void run() {

        try {

            task.getTaskKpi().exportStartTime = System.currentTimeMillis();
            String authString = "repoName="+Utility.getAgentConfigs().getCompany().getCompanyTag()+ "&username=" + Utility.getAgentConfigs().getLogin() + "&password=" + Utility.getAgentConfigs().getPassword() + "";
            task.setEncodedString(new String(Utility.encodetoBase64(authString.getBytes())) );
             
            boolean successStatus =   false;
            
            successStatus = Process();
                
            task.getTaskKpi().isExported    =   successStatus;
            task.getTaskKpi().exportEndTime = System.currentTimeMillis();
            
            if (task.getTaskKpi().isExported) {
                IndexingTPEList.startStatCalculator(task.getExecID(), task);
            } else {
                throw new Exception("Unable to index the file");
            }

        } catch (Exception ex) {
            
            task.getTaskKpi().failedExporting = true;
            task.getTaskKpi().errorDetails = ex;
            IndexingTPEList.startStatCalculator(task.getExecID(), task);
            
        } finally {
            executionCount.increment(task.getExecID());//.addAndGet(1);
        }
    }
}