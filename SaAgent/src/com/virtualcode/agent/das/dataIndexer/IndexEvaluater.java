/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataIndexer;

import com.virtualcode.agent.das.dataArchiver.EvalutorFlag;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.documentManagement.dto.DocumentType;

import com.virtualcode.agent.das.fileSystems.FileTaskIndexingInterface;
import com.virtualcode.agent.das.threadsHandler.IndexingTPEList;
import com.virtualcode.agent.das.utils.GlobMatch;
import com.virtualcode.agent.das.utils.MyHashMap;
import java.util.Iterator;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.log4j.Logger;

/**
 * @author Abbas
 */
public class IndexEvaluater implements Runnable {

    Logger logger = null;

    public IndexEvaluater(FileTaskIndexingInterface task) {
        this.task = task;
        logger  =   LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_DETAIL,task.getExecID());
    }
    public FileTaskIndexingInterface getFileTask() {
        return this.task;
    }
    
    private static MyHashMap executionCount = new MyHashMap();
    FileTaskIndexingInterface task = null;
    String extensions = "";
    private Long docAge = 0L;
    private Long docSize = 0L;
    private Long docLM = 0L;
    private Long docLA = 0L;
    private boolean ProcessHD = false;
    private boolean ProcessRO = false;
    private boolean processOfflineFiles =   false;
    
    private boolean Process() throws Exception {

        //Path file = task.getPath();
        //Path name = file.getFileName();
        String name =   task.getDocName();
        String path =   task.getPathStr();
        path	=	(path!=null && path.contains("@"))? path.substring(path.lastIndexOf("@")) : path;
        
        logger.info(task.getUid().toString() + " : Evaluating : " + name);
        //PathMatcher matcher;
        GlobMatch matcher   =   new GlobMatch();
        String[] exts = extensions.split(",");
        Exception ex = null;

        if (exts.length == 0) {
            exts = "*.*,*.*".split(",");
        }

        for (String s : exts) {
            if (s == null || s.isEmpty()) {
                //   rp.skippedNullEmpty += 1;
                continue;
            }
            //matcher = FileSystems.getDefault().getPathMatcher("glob:" + s);
            if (name != null && matcher.match(name, s)) {
                logger.info(task.getUid().toString() + " : Matched : " + s);
                task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND;
                if (name.isEmpty()
                        || name.startsWith("~")
                        || name.startsWith("$")
                        //|| file..getFileName()toString().startsWith(".")
                        || name.endsWith(".lnk")
                        || name.endsWith(".tmp")
                        || name.endsWith("Thumbs.db")) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_TEMPORARY;
                    logger.info("Link File OR Temporary File OR Office temporary File : " + path);
                    return true;

                }
                
                //BasicFileAttributes attrs = task.getAttributes();
                if (task.isDirectory()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_ISDIR;
                    logger.info("Is a Directory " + path);
                    return true;

                }
                
                boolean indexStubsAlso	=	true;
                logger.info(task.getUid() + " indexStubsAlso="+indexStubsAlso);
                if(!indexStubsAlso) {//if stubs are NOT to be indexed or RE-Indexed
                    // This block of code should NEVER be executed, because of hardcoded indexStubsAlso=true...
                	
                    if(!task.isDeduplicatedVolume() && task.isReparsePoint()) {//if not a dedup volumne than check only for reparse point, than skip the file
                        task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_TEMPORARY;
                        logger.info(task.getUid() + " Reparsepoint is set to true in a nonDeDupVolume : " + path);
                        return true;
                    }

                    if(task.isDeduplicatedVolume() && task.isReparsePoint() && task.isOfflineFile()) {//reparse point and Offline flags
                        task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_TEMPORARY;
                        logger.info(task.getUid() + " Reparsepoint & OfflineFlag is set to true in Win DeDupVolume : " + path);
                        return true;
                    }

                    if(processOfflineFiles==false) {//if Offline Files are not to be processed
                        if(task.isOfflineFile()) {//Dont invoke "SAFileStubbing".equalsIgnoreCase(task.getOwner())
                            task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_TEMPORARY;
                            logger.info(task.getUid() + " App generated offline Stub (i.e. shortcut file) : " + path);
                            return true;
                        }
                    } else {//if Offline files are to be processed, even if owner is SAFileStubbing
                        logger.info(task.getUid() + " Process Offline Files even if SAFileStubbing: ("+processOfflineFiles+") " + path);
                    }
                }

                /*
                if (attrs.isSymbolicLink()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_SYMBOLLINK;
                    logger.info("Is a Symbolic Link " + path);
                    return true;

                }

                if (attrs.isOther()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_SYMBOLLINK;
                    logger.info("Is a Other File, no idea what does it mean, may be not regular " + file);
                    return true;
                }*/

                if (!ProcessHD && task.isHidden()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_HIDDENLECTED;
                    logger.info("Is a Hidden File " + path);
                    return true;
                }

                if (!ProcessRO && task.isReadOnly()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_READ_ONLY;
                    logger.info("Is a Readonly File " + path);
                    return true;
                }

                if (docSize > 0 && task.getBytesCount() < docSize) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_SMALL_SIZE;
                    logger.info("File Size : ( " + task.getBytesCount() + " ) " + " Required Size : ( " + docSize + " ) ");
                    return true;
                } else if (docAge > 0 && task.getCreationDate().getTime() > docAge) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_AGE;
                    logger.info("File Creation Time : ( " + task.getCreationDate().getTime() + " ) " + " Required Time : ( " + docAge + " ) ");
                    return true;
                } else if (docLA > 0 && task.getLastAccessedDate().getTime() > docLA) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_LASTACCESSTIME;
                    logger.info("File Last Access Time : ( " + task.getLastAccessedDate().getTime() + " ) " + " Required Time : ( " + docLA + " ) ");
                    return true;
                } else if (docLM > 0 && task.getLastModifiedDate().getTime() > docLM) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_LASTMODIFIEDTIME;
                    logger.info("File Last Modified Time : ( " + task.getLastModifiedDate().getTime() + " ) " + " Required Size : ( " + docLM + " ) ");
                    return true;
                } else {
                    return true;
                }
            }
        }
        return false;
    }

    public static void resetExecutionCtr(String execID) {
        executionCount.put(execID, new AtomicLong(0));
    }
    public static String getExecutionCount(String execID) {
        return executionCount.get(execID);
    }

    @Override
    public void run() {
        try {

            task.getTaskKpi().evalutionStartTime = System.currentTimeMillis();

            Job aJobDetail = IndexingExecutors.currentExecs.get(task.getExecID());
            Iterator newIterator = aJobDetail.getPolicy().getDocumentTypeSet().iterator();
            while (newIterator.hasNext()) {
                DocumentType documentType = (DocumentType) newIterator.next();
                extensions = extensions + "*" + documentType.getTheValue() + ",";
            }
            if (extensions.isEmpty()) {
                extensions = "*";
            }

            String dAge = aJobDetail.getPolicy().getDocumentAge();
            String laDate = aJobDetail.getPolicy().getLastAccessedDate();
            String lmDate = aJobDetail.getPolicy().getLastModifiedDate();
            String sz = aJobDetail.getPolicy().getSizeLargerThan();
            docAge = Utility.calculateMilliSec(Utility.parseYear(dAge), Utility.parseMonth(dAge));
            docLM = Utility.calculateMilliSec(Utility.parseYear(lmDate), Utility.parseMonth(lmDate));
            docLA = Utility.calculateMilliSec(Utility.parseYear(laDate), Utility.parseMonth(laDate));
            docSize = Utility.calculateSize(sz);
            ProcessRO = aJobDetail.isProcessReadOnly();
            ProcessHD = aJobDetail.isProcessHidden();
            processOfflineFiles =   aJobDetail.isProcessOfflineFiles();
            boolean p = Process();
            task.getTaskKpi().evalutionEndTime = System.currentTimeMillis();

            if (!p && task.getTaskKpi().evaluatorFlag != EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND) {
                task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_EXTENSION;
            }

            //It should not get called
            if (task.getTaskKpi().evaluatorFlag != EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND) {
                throw new Exception("Task : " + task.getDocName() + " Reason: " + task.getTaskKpi().evaluatorFlag.toString());
            }
            
            //If current file is ready to archive / Stub / Evaluated
            if (task.getTaskKpi().evaluatorFlag == EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND) {
                
                //Get the file-size, to updated Evaluation vars
                task.getTaskKpi().setFileSize(this.task.getBytesCount());
                task.getTaskKpi().updateComulativeSize(this.task.getBytesCount());
            }

            if (task.getTaskKpi().evaluatorFlag == EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND
                    && (aJobDetail.getActionType().toLowerCase().trim().equals("indexing") ||
                    aJobDetail.getActionType().toLowerCase().trim().equals("analysis"))) {
                logger.info(task.getUid() + " Sending to Indexing/analysis ");
                IndexingTPEList.startIndexer(task.getExecID(), task);
              //  FileExecutors.statisticsCalculator.execute(new TaskStatistic(task));
            } else {

                logger.info(task.getUid() + " Sending to Task Statistic ");
                IndexingTPEList.startStatCalculator(task.getExecID(), task);
            }
        } catch (Exception ex) {
            task.getTaskKpi().errorDetails = ex;
            task.getTaskKpi().failEvaluation = true;
            IndexingTPEList.startStatCalculator(task.getExecID(), task);
        } finally {
            executionCount.increment(task.getExecID());//.addAndGet(1);

        }
    }

}