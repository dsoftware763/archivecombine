/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataIndexer;

import com.virtualcode.agent.das.fileSystems.ntfs.NTFSIndexingTask;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.threadsHandler.IndexingTPEList;
import com.virtualcode.agent.das.utils.Utility;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.FileSystemException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author HP
 */
public class NTFSIndexingWalker extends SimpleFileVisitor<Path> {

    private ArrayList<String> excPathList;
    private String execID   =   null;
    private String syncMode =   null;
    private String indexID   =   null;
    private boolean deduplicatedVolume  =   false;
    
    Logger logger   =   null;
    
    public NTFSIndexingWalker(ArrayList<String> excPathList, String execID, String syncMode, String indexID, boolean deduplicatedVolume) {
        this.excPathList    =   excPathList;
        this.execID  =   execID;
        this.syncMode   =   syncMode;
        this.indexID   =   indexID;
        this.deduplicatedVolume =   deduplicatedVolume;
        logger  =   LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_DETAIL, execID);
    }
    
    @Override
    public FileVisitResult preVisitDirectory(Path filePath, BasicFileAttributes attr) {
        
    	//IF interupted by user than, Stop the Recursive Walk of Files
        if(IndexStarter.isInterupted(this.execID)) {
            logger.info("File Visitor is being stopped for "+this.execID);
            return FileVisitResult.SKIP_SIBLINGS;//as its directory, so skip siblings and its subtree
        }
        
        //Skip the Dir, if its added in Exculed list by User...
        if(Utility.containsIgnoreCase(excPathList, filePath.toString())) {
            logger.info("Skip the excluded path: " + filePath.toString());
            return FileVisitResult.SKIP_SUBTREE;
        } else {
            return FileVisitResult.CONTINUE;
        }
    }
    
    @Override
    public FileVisitResult visitFile(Path filePath, BasicFileAttributes attr) {
        
    	//IF interupted by user than, Stop the Recursive Walk of Files
        if(IndexStarter.isInterupted(this.execID)) {
            logger.info("File Visitor is being stopped for "+this.execID);
            //cFile.resetLastAccessTime();
            return FileVisitResult.SKIP_SIBLINGS;
        }
        
        IndexingExecutors.incrementCounter(execID);//.fileWalkedCount += 1;
        NTFSIndexingTask task = new NTFSIndexingTask(filePath, attr, execID, syncMode, indexID, logger,deduplicatedVolume);
        logger.info("visiting NTFS: "+filePath);
        IndexingTPEList.startEvaluator(execID, task);
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
        if (exc instanceof AccessDeniedException) {
            LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_ERROR, execID).error(dir + ": cannot access directory");
        }
        LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_ERROR, execID).error(exc);

        return FileVisitResult.CONTINUE;
    }

    public FileVisitResult preVisitDirectoryFailed(Path dir, IOException exc) {
        if (exc instanceof AccessDeniedException) {
            LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_ERROR, execID).error(dir + ": cannot access directory");
        }
        LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_ERROR, execID).error(exc);

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) {
        if (exc instanceof FileSystemException) {
            LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_ERROR, execID).error(file.toString() + ": The device is not ready");
        }
        LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_ERROR, execID).error(exc);

        return FileVisitResult.CONTINUE;
    }
}
