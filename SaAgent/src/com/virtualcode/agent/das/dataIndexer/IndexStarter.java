/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataIndexer;


import executors.DataLayer;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.archivePolicy.dto.Job;

import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author HP
 */
public class IndexStarter implements Runnable {
    
    private String AgentName    =   null;
    private String actionType   =   null;
    public static ArrayList<String> underProcessJobsList   =   new ArrayList<String>();
    private static List<String> interuptedExecIDsList  =   null;
    
    public IndexStarter(String AgentName) {
        this.AgentName  =   AgentName;
        this.actionType =   "INDEXING";
    }
    
    @Override
    public void run() {
        System.out.println("INDEX: "+AgentName + " waiting for Indexing/Analysis Job...");
        try {
            while (true) {
                try {
                    
                    List<Job> jobsList  =   DataLayer.JobsbyAgent(AgentName, actionType);
                    if(jobsList!=null && !jobsList.isEmpty() && jobsList.size()>0) {
                        
                        interuptedExecIDsList  =   DataLayer.interuptedJobs();
                        //System.out.println("INDEX: "+"Interupted number of Jobs: "+interuptedExecIDsList.size());
                        
                        for (Job j : jobsList) {
                            int jID = j.getId();
                            
                            System.out.println("INDEX: "+"Indexing Job ID : " + jID);
                            if(underProcessJobsList.contains(jID+"")) {
                                System.out.println("INDEX: "+"Already processing: "+jID);

                            } else {
                                underProcessJobsList.add(jID+"");
                                String executionID = DataLayer.executionStarted(jID);
                                System.out.println("INDEX: "+"Indexing Execution ID : " + executionID);

                                LoggingManager.configureLogger(jID, executionID,LoggingManager.EXP_ACTIVITY_DETAIL);
                                LoggingManager.configureLogger(jID, executionID,LoggingManager.EXP_ACTIVITY_ERROR);
                                LoggingManager.configureLogger(jID, executionID,LoggingManager.EXP_JOB_SUMMARY);
                                LoggingManager.configureLogger(jID, executionID,LoggingManager.EXP_TASK_SUMMARY);

                                IndexingThread et =   new IndexingThread(j, executionID);
                                Thread exp  =   new Thread(et);
                                exp.start();
                            }
                            System.out.println("INDEX: "+"Seeking next indexing Job...");
                        }

                    } else {
                        System.out.println("INDEX: "+"New Indexing Job Not Found");
                    }
                } catch(Exception ex) {//if WS is down at Server side, then dont' Terminate...
                    ex.getMessage();
                    ex.printStackTrace();
                }
                System.gc();
                Thread.currentThread().sleep(1000 * 50);//50 secs
            }
            
        } catch (InterruptedException ex) {//if Thread.sleep is interupted
            ex.getMessage();
            ex.printStackTrace();
        }
    }
    
    public static boolean isInterupted(String execID) {
        
        //String curJobID =   IndexingExecutors.currentJobs.get(execID).getId()+"";
        boolean isInterupted =   false;
        
        synchronized(IndexStarter.class) {
            isInterupted =   IndexStarter.interuptedExecIDsList.contains(execID);
        }
        if(isInterupted)
            LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_DETAIL, execID).warn("execID ("+execID+") is Interupted by User while executing: ");
        
        return isInterupted;
    }
}