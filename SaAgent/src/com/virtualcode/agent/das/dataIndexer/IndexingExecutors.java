/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataIndexer;

import executors.DataLayer;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.archivePolicy.dto.ExcludedPath;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.agent.das.fileSystems.FileServiceInterface;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.archivePolicy.dto.SPDocLib;
import com.virtualcode.agent.das.dataExporter.ExportEvaluater;
import com.virtualcode.agent.das.dataExporter.FileExporter;
import com.virtualcode.agent.das.dataExporter.TaskStatisticsExport;
import com.virtualcode.agent.das.documentManagement.dto.DocumentType;
import com.virtualcode.agent.das.fileSystems.ntfs.NTFSIndexingService;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSIndexingService;
import com.virtualcode.agent.das.utils.MyHashMap;
import com.virtualcode.agent.das.threadsHandler.HotStatsUpdater;
import com.virtualcode.agent.das.threadsHandler.IndexingTPEList;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.log4j.Logger;

/**
 *
 * @author HP
 */
public class IndexingExecutors {
  
    public static HashMap<String, Job> currentExecs = new HashMap<String, Job>();
    public static MyHashMap fileWalkedCount = new MyHashMap();
    
    public void startJob(Job job, String executionID) throws InterruptedException, Exception {
        Logger logger = LoggingManager.getLogger(LoggingManager.EXP_JOB_SUMMARY,executionID);
        int errorCodeID = 0;
        int jID = -1;
        
        //Initialize the Counters for current Execution of cur Job
        fileWalkedCount.put(executionID, new AtomicLong(0));
        IndexEvaluater.resetExecutionCtr(executionID);
        FileIndexer.resetExecutionCtr(executionID);
        TaskStatisticsIndexed.resetExecutionCtr(executionID);
        TaskStatisticsIndexed.resetFailedCtr(executionID);
        TaskStatisticsIndexed.resetComulativeSize(executionID);

        Job currentJob  =   null;
        try {          
            jID = (int) job.getId();

            currentExecs.put(executionID, job);
            currentJob  =   currentExecs.get(executionID);
            currentJob.getJobStatistics().setJobStartTime(new Date());
            
            String authString   = "repoName="+Utility.getAgentConfigs().getCompany().getCompanyTag()+ "&username=" + Utility.getAgentConfigs().getLogin() + "&password=" + Utility.getAgentConfigs().getPassword() + "";
            String secureInfo   =   new String(Utility.encodetoBase64(authString.getBytes()));
            System.out.println("Encoded Secure Info: "+secureInfo);
            
            if (executionID == null) {
                throw new Exception("Execution ID is null");
            }
            
            // make/initiate the ThreadPools for current Execution of an Indexing Job
            IndexingTPEList threadsPoolSet  =   new IndexingTPEList(executionID, logger);

            logger.info("Job Info");
            logger.info("=======");
            logger.info("Execution ID " + executionID);
            logger.info("Job Name " + currentJob.getJobName());
            logger.info("Job Type " + currentJob.getActionType());
            logger.info("Extract Binary " + Utility.getAgentConfigs().getExtractContent());
            Iterator iterator = job.getSpDocLibSet().iterator();
            while (iterator.hasNext()) {
                SPDocLib spDocLib = (SPDocLib) iterator.next();
                logger.info("\t Index ID : " + spDocLib.getId());
                String libName  =   spDocLib.getLibraryName();
                logger.info("\t Index Path : " + ((libName!=null && libName.contains("@"))?libName.substring(libName.lastIndexOf("@")):libName));
                logger.info("\t isRecursive: " + spDocLib.isRecursive());
                /* 
                 * Not requiret yet, bcoz Export Jobs are not supporting Exclude option
                System.out.println("job -> Paths -> excluded paths:");
                Iterator excludePathIterator = spDocLib.getExcludedPathSet().iterator();
                while (excludePathIterator.hasNext()) {
                    ExcludedPath excludePath = (ExcludedPath) excludePathIterator.next();
                    System.out.println("\t \t" + excludePath.getPath());
                }
                 */
            }

            logger.info("job -> policy name : " + job.getPolicy().getPolicyName());
            logger.info("job -> policy -> document-types: ");
            Iterator newIterator = job.getPolicy().getDocumentTypeSet().iterator();
            while (newIterator.hasNext()) {
                DocumentType documentType = (DocumentType) newIterator.next();
                logger.info("\t documentType name: " + documentType.getTheValue());
            }

            //Initialize the Continous Stats update process - to provide HotStats to server...
            HotStatsUpdater hsUpdater   =   new HotStatsUpdater(jID, executionID, 
                    errorCodeID, currentJob.getJobStatistics(), 
                    LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_DETAIL, executionID));
            Thread th = new Thread(hsUpdater);
            th.start();
            
            boolean isIndexingJob   =   "INDEXING".equalsIgnoreCase(currentJob.getActionType());
            boolean isAnalysisJob   =   "ANALYSIS".equalsIgnoreCase(currentJob.getActionType());
            
            for (SPDocLib p : job.getSpDocLibSet()) {
                FileServiceInterface    fs  =   null;                
                if(isIndexingJob || isAnalysisJob) {//if this is Analysis Type Job
                    
                    if(isIndexingJob) {
                        //Initiate IndexWriter, and load in Pool
                        IndexerManager.initIndexWriter(p.getId()+"", true, logger);
                    }
                    AnalysisManager.initAnalysisWriter(p.getId()+"", logger);
                    
                    if(p.getLibraryName().toLowerCase().contains("smb://")) {//if the path is type CIFS
                        fs  =   new CIFSIndexingService(executionID, job.getSyncMode(), p.getId()+"", p.isDeduplicatedVolume());//smb://

                    } else {//if the path is type NTFS
                        fs  =   new NTFSIndexingService(executionID, job.getSyncMode(), p.getId()+"", p.isDeduplicatedVolume());//"E:/TestFiles/Exported"
                    }
                    
                } else {
                    logger.error("Indexing module dont support the Job of type "+currentJob.getActionType());
                }
                
                //Prepare the list of Excluded sub-paths of current Path
                ArrayList<String> excPathList   =   new ArrayList();
                for(ExcludedPath ep : p.getExcludedPathSet()) {
                    excPathList.add(ep.getPath());
                }
                fs.walkFileTree(p.getLibraryName(), excPathList, p.isRecursive(), p.getDestinationPath());
            }
            
            //Wait for shutting down the threads of current Execution
            threadsPoolSet.stopCurThreadPools();
            ////proceed further AFTER STOPPING THE THREAD POOL OF CUR JOB-EXEC   

            //Now close the IndexWriter's of all Shares...after completing the Indexes
            for (SPDocLib p : job.getSpDocLibSet()) {
                try {
                    if(isIndexingJob) {
                        IndexerManager.closeIndexWriter(p.getId()+"", logger);
                    }
                    AnalysisManager.closeAnalysisWriter(p.getId()+"", logger);
                    logger.info("closed the Index/analysis writer for "+p.getId());
                    
                } catch(IOException ioe) {
                    logger.error("Indexer not closed for ["+p.getId()+"] "+p.getLibraryName());
                    LoggingManager.printStackTrace(ioe, logger);
                }
            }
            
            //logger.info("Memory ... " + Runtime.getRuntime().totalMemory());
            logger.info("File Walked : " + fileWalkedCount.get(executionID));
            fileWalkedCount.put(executionID, new AtomicLong(0));//reset value to 0
            
            logger.info("");
            logger.info("Executors Report");
            logger.info("=================");
            logger.info("Repo Nodes Evaluted : " + IndexEvaluater.getExecutionCount(executionID));
            logger.info("Indexed : " + FileIndexer.getExecutionCount(executionID));
            //logger.info("Stubbed : " + FileStubber.getExecutionCount(executionID));
            logger.info("Failed : " + TaskStatisticsIndexed.getFailedCount(executionID));
            logger.info("Summary : " + TaskStatisticsIndexed.getExecutionCount(executionID));
            logger.info("Total Size : " + TaskStatisticsIndexed.getComulativeSize(executionID) + " bytes");

            currentJob.getJobStatistics().setJobEndTime(new Date());

            logger.info("Calling WS for executionCompleted with ID : " + executionID);

            boolean success = DataLayer.exportExecutionCompleted(jID, executionID, errorCodeID, currentJob.getJobStatistics());
            if (success) {
                logger.info("Successfull returned from execution Completed WS : " + jID);
            } else {
                throw new Exception("Oops.Error returned from execution Completed WS : " + jID);
            }

        } catch (Exception ex) {
            errorCodeID = 1;
            if (executionID != null) {

                if (currentJob == null) {
                    logger.info("Current Job is Null");
                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.exportExecutionCompleted(jID, executionID, errorCodeID, null);

                } else if (currentJob.getJobStatistics() == null) {
                    logger.info("currentJob.getJobStatistics() is Null");
                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.exportExecutionCompleted(jID, executionID, errorCodeID, null);
                } else {
                    currentJob.getJobStatistics().setJobEndTime(new Date());

                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.exportExecutionCompleted(jID, executionID, errorCodeID, currentJob.getJobStatistics());
                }
            }
            logger.fatal("ExecutionID: " + executionID);
            logger.fatal("JobID: " + jID);
            logger.fatal("ErrorID: " + errorCodeID);
            logger.fatal(ex.getMessage());
            
            LoggingManager.printStackTrace(ex, LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_ERROR,executionID));
            
        } finally {
            currentJob = null;
            currentExecs.remove(executionID);
            IndexStarter.underProcessJobsList.remove(jID+"");
            
            if(IndexStarter.underProcessJobsList!=null)
                IndexStarter.underProcessJobsList.remove(jID+"");
        }
    }
    
    public static void incrementCounter(String execID) {
        fileWalkedCount.increment(execID);
    }
}
