package com.virtualcode.agent.das.dataIndexer;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
 
import org.apache.solr.common.util.ContentStream;
 
public class ContentStreamWrapper implements ContentStream {
 
    private final InputStream is;
    private final String name;
 
    public ContentStreamWrapper(final InputStream is, final String name) {
	this.is = is;
	this.name = name;
    }
 
    @Override
    public Long getSize() {
	return null;
    }
 
    @Override
    public InputStream getStream() throws IOException {
	return is;
    }
 
    @Override
    public Reader getReader() throws IOException {
	return null;
    }
 
    @Override
    public String getName() {
	return name;
    }
 
    @Override
    public String getSourceInfo() {
	return null;
    }
 
    @Override
    public String getContentType() {
	return null;
    }
};
