package com.virtualcode.agent.das.dataIndexer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;

import com.vcsl.stats.IndexNode;
import com.virtualcode.agent.das.utils.Utility;
import org.apache.http.client.HttpClient;

public class IndexerManager {

    private static HashMap<String, SolrClient> iwPool   =   new HashMap<String, SolrClient>();
    private static SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    private static Integer docsCount    =   0;
    
    public static class Constants {
        public static String ID	=	"id";
        public static String DRIVE_ID	=	"sa_indexID";
        public static String FILE_PATH	=	"url";
        public static String FILE_NAME	=	"name";
        public static String CONTENT	=	"content";
        public static String TEXT_ALL	=	"text";
        public static String DATE_CREATE	=	"sa_date_creation";
        public static String DATE_MODIFY	=	"last_modified";
        public static String DATE_ACCESS	=	"sa_last_access";
        public static String FILE_SIZE		=	"sa_fileSize";
        public static String IS_ARCHIVED	=	"sa_isArchived";
        public static String META_DATA		=	"sa_MetaData";
        public static String ALLOWED_SIDS	=	"sa_allowedSids";
        public static String DENY_SIDS		=	"sa_denySids";
        public static String SHARE_ALLOWED_SIDS	=	"sa_shareAllowedSids";
        public static String SHARE_DENY_SIDS	=	"sa_shareDenySids";
    }

    public static void initIndexWriter(String indexID, boolean createNew, Logger logger) throws Exception {
    	
    	//create a single instance of solr server connection for a Job
        String urlString = Utility.getIndexesUrl(indexID);//("indexesURL");//"http://localhost:8080/solr";
    	logger.info("initializing SolrServer at: "+urlString+" for "+indexID);
        SolrClient solr = null;
        if(urlString!=null && urlString.startsWith("https://")) {//if SSL enabled solr
            logger.debug("initializing SSL enabled solr server : " + urlString);
            HttpClient httpclient	=	Utility.prepareSecureHttpClient();
            solr = new HttpSolrClient(urlString, httpclient);

        } else {
            logger.debug("initializing solr server : " + urlString);
            solr = new HttpSolrClient(urlString);
        }
        
    	iwPool.put(indexID, solr);
    }
    
    private static SolrClient getIndexWriter(String indexID) {
    	return iwPool.get(indexID);
    }
    
    public static void addDocument(String indexID, IndexNode iNode, Logger logger) throws IOException, SolrServerException {
    	SolrInputDocument doc = new SolrInputDocument();

    	String id	=	iNode.getFilePath();
    	if(id!=null && id.contains("@")) {
            id	=   id.substring(id.indexOf("@")+1);
    	}
    	
        doc.addField(IndexerManager.Constants.ID, id);//its the PK of document in Solr
        doc.addField(IndexerManager.Constants.DRIVE_ID, indexID);
        doc.addField(IndexerManager.Constants.FILE_PATH, iNode.getFilePath());		
        doc.addField(IndexerManager.Constants.FILE_NAME, iNode.getFileName());		
        doc.addField(IndexerManager.Constants.DATE_CREATE, dateParser.format(Utility.toDate(iNode.getDateCreation())));		
        doc.addField(IndexerManager.Constants.DATE_MODIFY, dateParser.format(Utility.toDate(iNode.getDateModification())));
        doc.addField(IndexerManager.Constants.DATE_ACCESS, dateParser.format(Utility.toDate(iNode.getDateLastAccess())));
        doc.addField(IndexerManager.Constants.FILE_SIZE, iNode.getFileSize());		
        doc.addField(IndexerManager.Constants.IS_ARCHIVED, iNode.getIsArchived()+"");
        doc.addField(IndexerManager.Constants.ALLOWED_SIDS, iNode.getAllowedSids());		
        doc.addField(IndexerManager.Constants.DENY_SIDS, iNode.getDenySids());
        doc.addField(IndexerManager.Constants.SHARE_ALLOWED_SIDS, iNode.getShareAllowedSids());		
        doc.addField(IndexerManager.Constants.SHARE_DENY_SIDS, iNode.getShareDenySids());	
        doc.addField(IndexerManager.Constants.META_DATA, iNode.getMetaData());

        String content	=	iNode.getReadableContent();
        if(content!=null && !content.isEmpty()) {//if the content is fetched by Tika
            doc.addField(IndexerManager.Constants.CONTENT, content);
        }

        SolrClient solrWriter	=	getIndexWriter(indexID);
        UpdateResponse response = solrWriter.add(doc);
        logger.info("solr response ("+indexID+"): "+response.toString());

        docsCount++;//increment the docsCount on each insertion
        if(docsCount%100==0) {
            solrWriter.commit();
        }
    }
    
    public static void closeIndexWriter(String indexID, Logger logger) throws SolrServerException, IOException {
    	
    	if(iwPool.get(indexID)!=null) {
            iwPool.get(indexID).commit();//if not null etc
            iwPool.remove(indexID);
    	}
    }
}
