package com.virtualcode.agent.das.dataIndexer;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import org.apache.log4j.Logger;

import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.logging.LoggingManager;
/**
 *
 * @author YAwar
 */
public class IndexingThread implements Runnable {
    
    private String executionID  =   null;
    private Job j   =   null;
    public IndexingThread(Job j, String execID) {
        this.executionID =   execID;
        this.j= j;
    }
    
    @Override
    public void run() {
        Logger logger = LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_ERROR, this.executionID);

        try {
            if (j == null) {
                throw new Exception("Indexing Job is null, Ops");
            }

            //execute the Job, in this newly created Thread for the Job
            IndexingExecutors ee = new IndexingExecutors();
            ee.startJob(j, this.executionID);
            ee = null;

        } catch (Exception ex) {

            logger.fatal("IndexingThread : " + ex);
            LoggingManager.printStackTrace(ex, logger);

        } finally {
        }
    }
}
