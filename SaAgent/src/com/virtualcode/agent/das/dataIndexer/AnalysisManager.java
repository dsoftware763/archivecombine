/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataIndexer;

import com.sun.xml.ws.developer.JAXWSProperties;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.vcsl.stats.IndexNode;
import com.vcsl.stats.StatsSyncronizerService_Service;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.soap.MTOMFeature;
/**
 *
 * @author HP
 */
public class AnalysisManager {
    
    private static HashMap<String, ArrayList<IndexNode>> indexNodes    =   new HashMap<String, ArrayList<IndexNode>>();
    private static ArrayList<String> metaKeys   =   new ArrayList<String>();
    
    private static StatsSyncronizerService_Service statsService   =   null;
    
    public static void initAnalysisWriter(String indexID, Logger logger) throws IOException {
        
        if(indexID!=null && Utility.isNumeric(indexID)) {//if the Analysis/Indexing is for each SP Doc Lib entry

            logger.info("indexID is not null and a number");
            boolean output   =   initStatsAnalyzer(indexID);//Invoke WS method
            if(output) {
                indexNodes.put(indexID, new ArrayList());//create arraylist to contain iNodes
                logger.info("statsAnalyzer and indexNodes created for "+indexID);
            }
        }
    }
    
    public static boolean updateStats(IndexNode iNode, String indexID, Logger logger) {
        boolean output  =   false;
        
        try {
            
            if(indexNodes.get(indexID)!=null) {
                indexNodes.get(indexID).add(iNode);//add the new current entry in arrayList

                if(indexNodes.get(indexID).size()%200==0) {//if 200 nodes/docs are indexed
                    
                    ArrayList<IndexNode> newTempList   =   new ArrayList<IndexNode>(indexNodes.get(indexID));//create separate instance of this arraylsit
                    indexNodes.get(indexID).clear();//clean the arraylist for next entries
                    
                    logger.debug("call WS and transfer stats to server for: "+indexID+" ("+newTempList.size()+")");
                    output = updateStatsAnalyzer(indexID, newTempList);

                } else {
                    output  =   true;
                }
            } else {
                logger.warn("iNodesList shouldn't be NULL for indexID: "+indexID);
            }
        } catch (MalformedURLException e) {
            output  =   false;
            LoggingManager.printStackTrace(e, logger);
        }
        
        return output;
    }

    public static void addMetaKeys(String key) {
    	if(!metaKeys.contains(key)) {
            metaKeys.add(key);
    	}
    }
    
    public static void closeAnalysisWriter(String indexID, Logger logger) throws IOException {
        boolean output  =   true;
        //upload any pending IndexNodes..
        if(indexNodes.get(indexID)!=null && indexNodes.get(indexID).size()>0) {
            logger.debug("call WS and transfer remaining stats to server for: "+indexID+" ("+indexNodes.get(indexID).size()+")");
            output = updateStatsAnalyzer(indexID, indexNodes.get(indexID));
            indexNodes.get(indexID).clear();//clean the arraylist for indexID
        }
        
        output  =   closeStatsAnalyzer(indexID, metaKeys);
        if(!output) {
            logger.warn("StatsAnalyzer is not closed properly for "+indexID);
        }
        
    }
    
    private static boolean initStatsAnalyzer(java.lang.String indexID) throws MalformedURLException {
        
        String wsdlURL  =   Utility.getDASUrl() + "/StatsSyncronizerService?wsdl";
        StatsSyncronizerService_Service service = (statsService==null)?new StatsSyncronizerService_Service(new URL(wsdlURL)):statsService;
        com.vcsl.stats.StatsSyncronizerService port = service.getStatsSyncronizerServicePort(new WebServiceFeature[]{new MTOMFeature()});
        
        Map ctxt = ((BindingProvider) port).getRequestContext();
        //System.out.println(ctxt.get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY));
        ctxt.put(JAXWSProperties.HTTP_CLIENT_STREAMING_CHUNK_SIZE, 8192);
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsdlURL);
        //System.out.println(ctxt.get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY));
        
        return port.initStatsAnalyzer(indexID);
    }

    private static boolean updateStatsAnalyzer(java.lang.String indexID, ArrayList<com.vcsl.stats.IndexNode> iNodesList) throws MalformedURLException {
        String wsdlURL  =   Utility.getDASUrl() + "/StatsSyncronizerService?wsdl";
        StatsSyncronizerService_Service service = (statsService==null)?new StatsSyncronizerService_Service(new URL(wsdlURL)):statsService;
        com.vcsl.stats.StatsSyncronizerService port = service.getStatsSyncronizerServicePort(new WebServiceFeature[]{new MTOMFeature()});
        
        Map ctxt = ((BindingProvider) port).getRequestContext();
        //System.out.println(ctxt.get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY));
        ctxt.put(JAXWSProperties.HTTP_CLIENT_STREAMING_CHUNK_SIZE, 8192);
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsdlURL);
        //System.out.println(ctxt.get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY));
        
        return port.updateStatsAnalyzer(indexID, iNodesList);
    }

    private static boolean closeStatsAnalyzer(java.lang.String indexID, java.util.List<java.lang.String> metaKeys) throws MalformedURLException {
        String wsdlURL  =   Utility.getDASUrl() + "/StatsSyncronizerService?wsdl";
        StatsSyncronizerService_Service service = (statsService==null)?new StatsSyncronizerService_Service(new URL(wsdlURL)):statsService;
        com.vcsl.stats.StatsSyncronizerService port = service.getStatsSyncronizerServicePort(new WebServiceFeature[]{new MTOMFeature()});
        
        Map ctxt = ((BindingProvider) port).getRequestContext();
        //System.out.println(ctxt.get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY));
        ctxt.put(JAXWSProperties.HTTP_CLIENT_STREAMING_CHUNK_SIZE, 8192);
        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsdlURL);
        //System.out.println(ctxt.get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY));
        
        return port.closeStatsAnalyzer(indexID, metaKeys);
    }
}
