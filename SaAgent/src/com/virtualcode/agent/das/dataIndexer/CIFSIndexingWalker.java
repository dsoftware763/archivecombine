/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataIndexer;

import com.virtualcode.agent.das.fileSystems.cifs.CIFSFile;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSIndexingTask;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.threadsHandler.IndexingTPEList;
import java.io.IOException;
import org.apache.log4j.Logger;

/**
 *
 * @author Abbas
 */
public class CIFSIndexingWalker {
    
    private String execID   =   null;
    private String syncMode =   null;
    private String indexID  =   null;
    private Logger logger   =   null;
    private boolean deduplicatedVolume  =   false;
    
    public CIFSIndexingWalker(String execID, String syncMode, String indexID, boolean deduplicatedVolume) {
        this.execID =   execID;
        this.syncMode   =   syncMode;
        this.indexID    =   indexID;
        this.deduplicatedVolume =   deduplicatedVolume;
        logger  =   LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_DETAIL, execID);
    }
    
    public void visitFile(CIFSFile cFile) throws IOException {
        
    	//IF interupted by user than, Stop the Recursive Walk of Files
        if(IndexStarter.isInterupted(this.execID)) {
            logger.info("File Visitor is being stopped for "+this.execID);
            cFile.resetLastAccessTime();
            return;
        }
        
        IndexingExecutors.incrementCounter(execID);//.fileWalkedCount += 1;
        CIFSIndexingTask task = new CIFSIndexingTask(cFile, execID, syncMode, indexID, logger, deduplicatedVolume);
        
        logger.info(task.getUid() + " visiting CIFSIndexing: ");
        IndexingTPEList.startEvaluator(task.getExecID(), task);
        
        return;
    }

    public void postVisitDirectory() {
    }

    public void preVisitDirectoryFailed() {
    }

    public void visitFileFailed() {
    }
}
