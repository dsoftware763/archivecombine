package com.virtualcode.agent.das.database;

import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
import org.apache.log4j.Logger;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


public class SchemaCreator {

    private static Logger logger = LoggingManager.getLogger(LoggingManager.STARTUPLOGS, "");


    private static final String DB_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    private static final String DB_URL = "jdbc:derby:";
    private static final String DB_NAME = "sharearchiver;create=true;";

    public static void main(String[] args) {
        // TODO Auto-generated method stub
//		deployDB("DataRepository");

    }

    public static void deployDB() {

        logger.info("Initializing derby for ......");
        logger.info("deploying db for :");
        Connection connection = null;
        Statement statement = null;
        String query = null;
        String basePath = Utility.getAgentConfigs().getLogBasePath();
        if (basePath == null || basePath.isEmpty())
            basePath = "ErrorLog";
        try {
            Class.forName(DB_DRIVER);
            String derbyPath = basePath + File.separator + "Derby";
            String dbURL = DB_URL + derbyPath + "/" + DB_NAME;
            connection = DriverManager.getConnection(dbURL);
            statement = connection.createStatement();

            query = "CREATE TABLE node_info("
                    + "vc_node_path varchar(3000) NOT NULL,"
                    + "vc_node_name varchar(150) NOT NULL,"
                    + "vc_creationDate timestamp,"    //can be null
                    + "vc_modifiedDate timestamp,"    //can be null
                    + "vc_archivedDate timestamp,"    //can be null
                    + "vc_fileSize bigint NOT NULL,"
                    // + "vc_jobId int NOT NULL,"
                    + "vc_dasurl varchar(3000),"//can be null
                    + "vc_process_flag int NOT NULL,"//0 evaluated 1 archived
                     + "vc_dest_path varchar(3000),"
                    + "U_VC_NODE_PATH GENERATED ALWAYS AS (UPPER(vc_node_path)),"
                    + "U_VC_PROCESS_FLAG GENERATED ALWAYS AS (vc_process_flag),"
                    + "U_VC_DEST_PATH GENERATED ALWAYS AS (UPPER(vc_dest_path)),"
                    //  + "U_VC_JOBID GENERATED ALWAYS AS (vc_jobId),"
                    + "PRIMARY KEY (vc_node_path,vc_dest_path)"
                    + ")";

            statement.execute(query);
            logger.info("Initialized derby for ......");
        } catch (SQLException ex) {
            logger.error(ex.getMessage());
            logger.error(ex.getCause());
            logger.error("Error while deploying DB: " + ex.getCause());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            logger.error("Error while deploying DB: " + e.getCause());
        }

        try {
            query = "CREATE INDEX NAME_INDEX on node_info(U_VC_NODE_PATH)";
            statement.execute(query);
        } catch (SQLException ex) {
            logger.error(ex.getMessage());
            logger.error(ex.getCause());
            logger.error("Error while deploying DB index: " + ex.getCause());
        }

        try {
            query = "CREATE INDEX NAME_INDEX on node_info(U_VC_DEST_PATH)";
            statement.execute(query);
        } catch (SQLException ex) {
            logger.error(ex.getMessage());
            logger.error(ex.getCause());
            logger.error("Error while deploying DB index: " + ex.getCause());
        }

        try {
            query = "CREATE INDEX PATH_INDEX on node_info(U_VC_PROCESS_FLAG)";
            statement.execute(query);
        } catch (SQLException ex) {
            logger.error(ex.getMessage());
            logger.error(ex.getCause());
            logger.error("Error while deploying DB index: " + ex.getCause());
        }

       /* try {
            query = "CREATE INDEX PATH_INDEX on node_info(U_VC_JOBID)";
            statement.execute(query);
        } catch (SQLException ex) {
            logger.error(ex.getMessage());
            logger.error(ex.getCause());
            logger.error("Error while deploying DB index: " + ex.getCause());
        }*/

        try {
            connection.close();
            logger.error("connection closed successfully");
        } catch (SQLException e) {
            logger.error("Error while insertion: " + e.getCause());
        }
    }


}
