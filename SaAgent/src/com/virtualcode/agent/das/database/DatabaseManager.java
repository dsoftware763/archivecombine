package com.virtualcode.agent.das.database;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.virtualcode.agent.das.utils.Utility;

public class DatabaseManager {


    private static final String DB_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    private static final String DB_URL = "jdbc:derby:";
    private static final String DB_NAME = "sharearchiver;create=false;territory=LC_ALL";


    public static Connection getConnection() {
        Connection connection = null;
        try {
            String basePath = Utility.getAgentConfigs().getLogBasePath();
            if (basePath == null || basePath.isEmpty())
                basePath = "ErrorLog";
            Class.forName(DB_DRIVER);
            String derbyPath = basePath + File.separator + "Derby";
            String dbURL = DB_URL + derbyPath + "/" + DB_NAME;
            connection = DriverManager.getConnection(dbURL);
            return connection;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }


    public static void addNode(NodeInfo nodeInfo) throws Exception{

        Connection connection = getConnection();
        String query = "INSERT INTO node_info ";
        query += "(vc_node_path,vc_node_name,vc_creationDate,vc_modifiedDate," +
                "vc_archivedDate,vc_fileSize,vc_dasurl," +
                "vc_process_flag,vc_dest_path) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?,?)";

            int index = 1;
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            String docPath = nodeInfo.getVcNodePath();

            if(docPath.toLowerCase().contains("smb://")){
                //if path is remote path remove original user name and password in the doc path
                String finalDocPath = docPath.substring(0,docPath.indexOf(';'))+";username:password"+docPath.substring(docPath.indexOf('@'), docPath.length());
                preparedStatement.setString(index++, finalDocPath);//vc_node_path
            }else{
                preparedStatement.setString(index++, docPath);//vc_node_path
            }
            preparedStatement.setString(index++, nodeInfo.getVcNodeName());//vc_node_name
            preparedStatement.setTimestamp(index++, nodeInfo.getVcCreationDate());//vc_creationDate
            preparedStatement.setTimestamp(index++, nodeInfo.getVcModifiedDate()); //vc_modifiedDate
            preparedStatement.setTimestamp(index++, nodeInfo.getVcArchivedDate()); //vc_archivedDate
            preparedStatement.setLong(index++, nodeInfo.getVcFileSize());//vc_fileSize
            preparedStatement.setString(index++, nodeInfo.getVcDasURL()); //vc_dasurl
            preparedStatement.setInt(index++, nodeInfo.getVcProcessFlag());//vc_process_flag
            preparedStatement.setString(index++, nodeInfo.getVcDestPath());//vc_dest_path
            // preparedStatement.setInt(index++, nodeInfo.getJobId());//vc_jobId
            //preparedStatement.setString(index++, nodeInfo.getBasePath()); //vc_base_path
            preparedStatement.executeUpdate();
            preparedStatement.close();
             connection.close();


    }

    //get das url
    public static String getDasUrlByPath(String destPath,int processFlag, Logger logger,String uid) {

        Connection connection = getConnection();
        if (destPath.endsWith("/"))
            destPath = destPath.substring(0, destPath.length() - 1);

        logger.info(uid+" Finding in local db with dest path "+destPath);
        String query = "Select vc_dasurl from node_info where U_VC_DEST_PATH = ? and U_VC_PROCESS_FLAG =?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, destPath.toUpperCase());
            preparedStatement.setInt(2, processFlag);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                return rs.getString("vc_dasurl");
            }
            rs.close();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            logger.error(uid+" Error finding node with ID ");
            logger.error(uid+" Error : " + e.getCause());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                //do nothing
            }
        }
        return null;
    }

    public static List<NodeInfo> getListNodeInfoByPath(String nodePath,int processFlag, Logger logger) {

        Connection connection = getConnection();
        if (nodePath.endsWith("/"))
            nodePath = nodePath.substring(0, nodePath.length() - 1);

        if(nodePath.toLowerCase().contains("smb://")){
            //if path is remote path remove original user name and password in the nodePath path
            nodePath = nodePath.substring(0,nodePath.indexOf(';'))+";username:password"+nodePath.substring(nodePath.indexOf('@'), nodePath.length());
        }

        logger.info("Finding in local db with node path "+nodePath );
        String query = "Select *  from node_info where U_VC_NODE_PATH like ? and U_VC_PROCESS_FLAG =?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, nodePath.toUpperCase()+"%");
            preparedStatement.setInt(2, processFlag);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs !=null) {
                return mapSqlRowToObject(rs);
            }
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            logger.error("Error finding node with ID " );
            logger.error("Error : " + e.getCause());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                //do nothing
            }
        }
        return null;
    }

    //get das url
    public static boolean checkDataBaseByPath(String nodePath,String destPath,Logger logger,String uid) {

        Connection connection = getConnection();
        boolean result = false;
        if (nodePath.endsWith("/"))
            nodePath = nodePath.substring(0, nodePath.length() - 1);

        if(nodePath.toLowerCase().contains("smb://")){
            //if path is remote path remove original user name and password in the nodePath path
            nodePath = nodePath.substring(0,nodePath.indexOf(';'))+";username:password"+nodePath.substring(nodePath.indexOf('@'), nodePath.length());
        }
        //  logger.info(uid+"Finding in local db with node path: " + nodePath);
        String query = "Select vc_dasurl from node_info where U_VC_NODE_PATH = ? and U_VC_DEST_PATH = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, nodePath.toUpperCase());
            preparedStatement.setString(2, destPath.toUpperCase());
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result= true;
            }
            rs.close();
            preparedStatement.close();
            connection.close();
            logger.info(uid+" Result : " + result);
            return result;
        } catch (SQLException e) {
            logger.error(uid+" Error finding node with ID: ");
            logger.error(uid+" Error : " + e.getCause());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                //do nothing
            }
        }
        logger.info(uid+" Result : " + result);
        return result;
    }


    public static void updateNodeInfo(String nodePath, String dasUrl, int processFlag, Timestamp archiveDate) {

        Connection connection = getConnection();
        String query = "UPDATE node_info SET  vc_dasurl = ?, vc_archivedDate = ?,vc_process_flag = ? WHERE U_VC_NODE_PATH = ?";
        if (nodePath.endsWith("/"))
            nodePath = nodePath.substring(0, nodePath.length() - 1);

        if(nodePath.toLowerCase().contains("smb://")){
            //if path is remote path remove original user name and password in the nodePath path
            nodePath = nodePath.substring(0,nodePath.indexOf(';'))+";username:password"+nodePath.substring(nodePath.indexOf('@'), nodePath.length());
        }

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, dasUrl);
            preparedStatement.setTimestamp(2, archiveDate);
            preparedStatement.setInt(3, processFlag);
            preparedStatement.setString(4, nodePath.toUpperCase());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                //do nothing
            }
        }


    }

    private static List<NodeInfo> mapSqlRowToObject(ResultSet rs) throws SQLException {
        List<NodeInfo> nodeList = new ArrayList<NodeInfo>();

        while (rs.next()) {
            NodeInfo nodeInfo = new NodeInfo();
            nodeInfo.setVcNodePath(rs.getString("vc_node_path"));
            nodeInfo.setVcProcessFlag(rs.getInt("vc_process_flag"));
            nodeInfo.setVcFileSize(rs.getLong("vc_fileSize"));
            nodeInfo.setVcDestPath(rs.getString("vc_dest_path"));
            nodeList.add(nodeInfo);
        }
        return nodeList;
    }


}
