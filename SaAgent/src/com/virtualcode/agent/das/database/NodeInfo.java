package com.virtualcode.agent.das.database;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by sivakumar on 28/1/2017.
 */
public class NodeInfo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String vcNodePath;
    private String vcNodeName;
    private Timestamp vcCreationDate;
    private Timestamp vcModifiedDate;
    private Timestamp vcArchivedDate;
    private Long vcFileSize;
    private String vcDasURL;
    private int vcProcessFlag;
    private int jobId;
    private String vcDestPath;


    public String getVcNodePath() {
        return vcNodePath;
    }

    public void setVcNodePath(String vcNodePath) {
        this.vcNodePath = vcNodePath;
    }

    public String getVcNodeName() {
        return vcNodeName;
    }

    public void setVcNodeName(String vcNodeName) {
        this.vcNodeName = vcNodeName;
    }


    public Timestamp getVcCreationDate() {
        return vcCreationDate;
    }

    public void setVcCreationDate(Timestamp vcCreationDate) {
        this.vcCreationDate = vcCreationDate;
    }

    public Timestamp getVcModifiedDate() {
        return vcModifiedDate;
    }

    public void setVcModifiedDate(Timestamp vcModifiedDate) {
        this.vcModifiedDate = vcModifiedDate;
    }

    public Timestamp getVcArchivedDate() {
        return vcArchivedDate;
    }

    public void setVcArchivedDate(Timestamp vcArchivedDate) {
        this.vcArchivedDate = vcArchivedDate;
    }

    public Long getVcFileSize() {
        return vcFileSize;
    }

    public void setVcFileSize(Long vcFileSize) {
        this.vcFileSize = vcFileSize;
    }

    public int getVcProcessFlag() {
        return vcProcessFlag;
    }

    public void setVcProcessFlag(int vcProcessFlag) {
        this.vcProcessFlag = vcProcessFlag;
    }

    public String getVcDasURL() {
        return vcDasURL;
    }

    public void setVcDasURL(String vcDasURL) {
        this.vcDasURL = vcDasURL;
    }


    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }



    public String getVcDestPath() {
		return vcDestPath;
	}

	public void setVcDestPath(String vcDestPath) {
		this.vcDestPath = vcDestPath;
	}

    public NodeInfo() {
    }



	public NodeInfo(String vcNodePath, String vcNodeName,
			Timestamp vcCreationDate, Timestamp vcModifiedDate,
			Timestamp vcArchivedDate, Long vcFileSize, String vcDasURL,
			int vcProcessFlag, int jobId, String vcDestPath) {
		super();
        this.vcNodePath = vcNodePath;
        this.vcNodeName = vcNodeName;
        this.vcCreationDate = vcCreationDate;
        this.vcModifiedDate = vcModifiedDate;
        this.vcArchivedDate = vcArchivedDate;
        this.vcFileSize = vcFileSize;
        this.vcDasURL = vcDasURL;
        this.vcProcessFlag = vcProcessFlag;
        this.jobId = jobId;
		this.vcDestPath = vcDestPath;
    }

    @Override
    public String toString() {
		return "NodeInfo [vcNodePath=" + vcNodePath + ", vcNodeName="
				+ vcNodeName + ", vcCreationDate=" + vcCreationDate
				+ ", vcModifiedDate=" + vcModifiedDate + ", vcArchivedDate="
				+ vcArchivedDate + ", vcFileSize=" + vcFileSize + ", vcDasURL="
				+ vcDasURL + ", vcProcessFlag=" + vcProcessFlag + ", jobId="
				+ jobId + ", vcDestPath=" + vcDestPath + "]";
    }


}
