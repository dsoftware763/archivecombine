/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataRetainer;

import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import executors.DataLayer;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.archivePolicy.dto.JobStatistics;
import com.virtualcode.agent.das.archivePolicy.dto.SPDocLib;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Constants;
import com.virtualcode.agent.das.utils.Utility;
import java.io.File;
import java.io.IOException;
import java.util.Set;
import jcifs.smb.SmbFile;

/**
 *
 * @author Abbas
 */
public class RemoverExecutor {

    //save the job instance of current executions, to be used in Stats etc
    public static HashMap<String, Job> currentExecs = new HashMap<String, Job>();

    void startJob(Job job, String executionID) throws InterruptedException, Exception {

        Logger logger = LoggingManager.getLogger(LoggingManager.JOB_SUMMARY, executionID);
        Logger loginfo = LoggingManager.getLogger(LoggingManager.ACTIVITY_ERROR, executionID);

        int errorCodeID = 0;
        int jID = -1;

        Job currentJob = null;
        try {
            jID = (int) job.getId();

            currentExecs.put(executionID, job);
            currentJob = currentExecs.get(executionID);
            currentJob.getJobStatistics().setJobStartTime(new Date());

            if (executionID == null) {
                throw new Exception("Execution ID is null for removal");
            }

            logger.info("Job Info");
            logger.info("=======");
            logger.info("Execution ID " + executionID);
            logger.info("Job Name " + currentJob.getJobName());
            logger.info("Job Type " + currentJob.getActionType());
            logger.info("Company Tag " + currentJob.getCompanyTags());
            loginfo.info(currentJob.getJobName() + " started with ID=" + executionID);

            logger.info("job -> policy name : " + job.getPolicy().getPolicyName());
            logger.info("job -> policy -> document-types: ");

            long removedCount = this.removeAllDocuments(job.getSpDocLibSet(), executionID);

            logger.info("Memory ... " + Runtime.getRuntime().totalMemory());

            logger.info("");
            logger.info("Executors Report");
            logger.info("=================");
            
            JobStatistics jobStatistics = currentJob.getJobStatistics();
            jobStatistics.setTotalEvaluated(job.getSpDocLibSet().size());
            jobStatistics.setTotalFreshArchived(removedCount);
            jobStatistics.setTotalFailedArchiving(job.getSpDocLibSet().size() - removedCount);
            jobStatistics.setTotalDuplicated(0);
            jobStatistics.setAlreadyArchived(0);
            jobStatistics.setTotalStubbed(0);

            jobStatistics.setTotalFailedEvaluating(0);
            jobStatistics.setTotalFailedDeduplication(0);
            jobStatistics.setTotalFailedStubbing(0);
            jobStatistics.setJobEndTime(new Date());

            logger.info("Calling WS for executionCompleted with ID : " + executionID);

            boolean success = DataLayer.executionCompleted(jID, executionID, errorCodeID, currentJob.getJobStatistics());
            if (success) {
                logger.info("Successfull returned from execution Completed WS : " + jID);
            } else {
                //throw exception, to report into FailedExecutionCompleted stats
                throw new Exception("Oops.Error returned from execution Completed WS : " + jID);
            }

        } catch (Exception ex) {
            errorCodeID = 1;
            if (executionID != null) {

                if (currentJob == null) {
                    loginfo.error("CODE[" + errorCodeID + "]: Fatal Error!");

                    logger.info("Current Job is Null");
                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.executionCompleted(jID, executionID, errorCodeID, null);

                } else if (currentJob.getJobStatistics() == null) {
                    loginfo.error("CODE[" + errorCodeID + "]: Fatal Error!");

                    logger.info("currentJob.getJobStatistics() is Null");
                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.executionCompleted(jID, executionID, errorCodeID, null);
                } else {
                    currentJob.getJobStatistics().setJobEndTime(new Date());
                    loginfo.error("CODE[" + errorCodeID + "]: Fatal Error!");

                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.executionCompleted(jID, executionID, errorCodeID, currentJob.getJobStatistics());
                }
            }
            loginfo.error("CODE[" + errorCodeID + "]: Fatal Error!");
            loginfo.error(ex.getMessage());

            logger.fatal("ExecutionID: " + executionID);
            logger.fatal("JobID: " + jID);
            logger.fatal("ErrorID: " + errorCodeID);

            logger.fatal(ex.getMessage());
            LoggingManager.printStackTrace(ex, logger);
            LoggingManager.getLogger(LoggingManager.ACTIVITY_ERROR, executionID).error("Fatal : " + "\n" + ex.getMessage());

        } finally {
            currentJob = null;
            currentExecs.remove(executionID);
            RetentionHandler.underProcessJobsList.remove(jID + "");//remove from the volatile list at Agent side...
        }
    }
    
    public long removeAllDocuments(Set<SPDocLib> allPaths, String executionID) {

        long removedCount = 0l;
        Logger logger   =   LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL, executionID);
        try {

            logger.info("B4 removal folders: ");
            long count = 0l;
            for (SPDocLib p : allPaths) {

                if(!Constants.pathTypeRepo.equals(p.getLibraryType())) {//ignore, if NOT a REPO type path
                    logger.info("not a valid path for Removal thread.."+p.getLibraryName());
                    continue;
                }
                count++;
                /*/Terminate the current Thread, if CANCEL is requested by User
                //check for cancellation after each 50 files (to keep optimum performance)
                if (count % 50 == 0 && RetentionHandler.isInterupted(executionID)) {
                    logger.info("Interupted by User to Cancle the job");
                    break;//break the iterations further
                }*/

                String uid = p.getLibraryName();//.getDestinationPath();
                if (p.isRecursive() && Constants.pathTypeRepo.equals(p.getLibraryType())) {//if marked for removal by the Approval authority
                    removedCount += this.removeDocument(uid, logger);

                } else {
                    logger.info("Not marked for removal: " + uid);
                }
            }//ending loop

            logger.info("After removal folders: ");

            //Run the Garbage collector to permanently delete files from Data Store
            ////////////////////////////
            logger.info("Garbage collector started at: " + System.currentTimeMillis());

            //session.logout();
            //////////rm.stop();
            logger.info("Garbage collected at: " + System.currentTimeMillis());
            ///////////

        } finally {
            ////yawarz - ALIT ///if(parent!=null) {
            ////yawarz - ALIT ///releaseLock(parent.getIdentifier());
            ////yawarz - ALIT ///}
        }

        return removedCount;
    }
    
    private long removeDocument(String uid, Logger logger) {

        long result = 0l;
        //uid	=	"a18166f0-0b00-46aa-82a5-c3782294e673";
        logger.info("Removing: " + uid);
        try {
            
            if(uid!=null && uid.startsWith("smb://")) {//if CIFS..
                SmbFile    file    =   new SmbFile(uid);
                if (file.exists()) {
                    logger.info("Target node: " + file.getParent());

                    this.deleteSmb(file, logger);
                    result = 1l;
                } else {
                    logger.info("Not Found: " + uid);
                    result = 0;
                }
                
            } else {//if NTFS..
                
                File    file    =   new File(uid);
                if (file.exists()) {
                    logger.info("Target node: " + file.getAbsolutePath());

                    this.delete(file, logger);
                    result = 1l;
                } else {
                    logger.info("Not Found: " + uid);
                    result = 0;
                }
            }
            
        } catch (IOException pne) {
            logger.info("Node already removed or Not exist: " + uid + " - " + pne.getMessage());
            result = 0;
        }
        
        return result;
    }
    
    public void delete(File file, Logger logger) throws IOException {

        if (file.isDirectory()) { 
            
            if (file.list().length == 0) { //directory is empty, then delete it
                file.delete();
                logger.info("Directory is deleted : " + file.getAbsolutePath());

            } else {
                //list all the directory contents
                String files[] = file.list();

                for (String temp : files) {
                    //construct the file structure
                    File fileDelete = new File(file, temp);

                    //recursive delete
                    this.delete(fileDelete, logger);
                }

                //check the directory again, if empty then delete it
                if (file.list().length == 0) {
                    file.delete();
                    logger.info("Directory is deleted : "+ file.getAbsolutePath());
                }
            }

        } else {
            file.delete();
            logger.info("File is deleted : " + file.getAbsolutePath());
        }
    }
    
    public void deleteSmb(SmbFile file, Logger logger) throws IOException {

        if (file.isDirectory()) {
            if (file.list().length == 0) { //directory is empty, then delete it
                file.delete();
                logger.info("SmbDirectory is deleted : " + file.getPath());

            } else {
                //list all the directory contents
                String files[] = file.list();

                for (String temp : files) {
                    //construct the file structure
                    SmbFile fileDelete = new SmbFile(file, temp);

                    //recursive delete
                    this.deleteSmb(fileDelete, logger);
                }

                //check the directory again, if empty then delete it
                if (file.list().length == 0) {
                    file.delete();
                    logger.info("SmbDirectory is deleted : "+ file.getPath());
                }
            }

        } else {
            file.delete();
            logger.info("SmbFile is deleted : " + file.getPath());
        }
    }
}