package com.virtualcode.agent.das.dataRetainer;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import org.apache.log4j.Logger;

import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.logging.LoggingManager;

/**
 *
 * @author YAwar
 */
public class RemovePermanentThread implements Runnable {

    private Job j;
    private String executionID;

    public RemovePermanentThread(Job j, String executionID) {
        this.j = j;
        this.executionID = executionID;
    }

    @Override
    public void run() {
        Logger logger = LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL, this.executionID);

        try {
            if (j == null) {
                throw new Exception("Job is null, Ops");
            }

            RemoverExecutor fe = new RemoverExecutor();
            fe.startJob(j, this.executionID);
            fe = null;

        } catch (Exception ex) {

            logger.fatal("JobStarter : " + ex);
            LoggingManager.printStackTrace(ex, logger);

        } finally {
            //Utility.resetUtilityResourceBundles();
            ////TEST COMMENT, to see affects...  
            ////LoggingManager.shutdownLogging(executionID);
            //System.gc();
        }
    }
}
