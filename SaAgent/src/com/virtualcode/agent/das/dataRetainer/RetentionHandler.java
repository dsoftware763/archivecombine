package com.virtualcode.agent.das.dataRetainer;

import java.util.List;

import executors.DataLayer;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.dataArchiver.FileExecutors;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Constants;
import java.util.ArrayList;

/**
 *
 * @author Abbas
 */
public class RetentionHandler implements Runnable {

    private String AgentName = null;
    public static ArrayList<String> underProcessJobsList = new ArrayList<String>();
    //private static List<Integer> interuptedJobIDsList  =   null;

    public RetentionHandler(String AgentName) {
        this.AgentName = AgentName;
    }

    @Override
    public void run() {
        try {

            Thread.currentThread().sleep(5 * 1000);// 5 sec
            System.out.println("RETN: "+AgentName + " waiting for next Retension Job...");

            while (true) {
                try {

                    List<Job> jobsList = DataLayer.JobsbyAgent(AgentName, Constants.jobTypeRetension);
                    if (jobsList != null && !jobsList.isEmpty() && jobsList.size() > 0) {

                        System.out.println("RETN: "+jobsList.size() + " new Retension Jobs Found.");

                        //interuptedJobIDsList  =   DataLayer.interuptedJobs();
                        //logger.info("Interupted number of Jobs: "+interuptedJobIDsList.size());

                        for (Job j : jobsList) {
                            int jID = j.getId();

                            System.out.println("RETN: "+"Retension Job ID : " + jID + " for " + j.getActiveActionType());
                            if (underProcessJobsList.contains(jID + "")) {
                                System.out.println("RETN: "+"Already processing: " + jID);

                                //} else if(interuptedJobIDsList.contains(jID+"")) {
                                //    logger.warn("Job is Canceled / Interupted: "+jID);

                            } else {
                                underProcessJobsList.add(jID + "");
                                String executionID = DataLayer.executionStarted(jID);
                                System.out.println("RETN: "+"Retension Execution ID : " + executionID);

                                //LoggingManager.configureLogger(jID, executionID,LoggingManager.PUBLIC_MSGS);
                                LoggingManager.configureLogger(jID, executionID, LoggingManager.ACTIVITY_DETAIL);
                                LoggingManager.configureLogger(jID, executionID, LoggingManager.ACTIVITY_ERROR);
                                LoggingManager.configureLogger(jID, executionID, LoggingManager.JOB_SUMMARY);
                                LoggingManager.configureLogger(jID, executionID,LoggingManager.TASK_SUMMARY);

                                //if its to Mark only...
                                if (Constants.jobTypeMarkOnly.equals(j.getActiveActionType())) {
                                    //MarkForRemovalThread et = new MarkForRemovalThread(j, executionID);
                                    //Thread exp = new Thread(et);
                                    //exp.start();
                                    
                                    FileExecutors fe = new FileExecutors();
                                    fe.startJob(j, executionID);
                                    fe = null;

                                } else if (Constants.jobTypeRemovePerm.equals(j.getActiveActionType())) {

                                    RemovePermanentThread rmpt = new RemovePermanentThread(j, executionID);
                                    Thread exp = new Thread(rmpt);
                                    exp.start();
                                } else {
                                    System.out.println("RETN: "+"Action Type is NOT allowed..");
                                }

                            }
                            System.out.println("RETN: "+"Seeking next Retension Job...");
                        }

                    } else {
                        System.out.println("RETN: "+"New Retension Job Not Found");
                    }
                } catch (Exception ex) {//if WS is down at Server side, then dont' Terminate...
                    ex.getMessage();
                    ex.printStackTrace();

                }
                System.gc();
                Thread.currentThread().sleep(1000 * 60);//wait for 60sec
            }

        } catch (InterruptedException ex) {//if Thread.sleep is interupted
            ex.getMessage();
            ex.printStackTrace();
        }
    }
}
