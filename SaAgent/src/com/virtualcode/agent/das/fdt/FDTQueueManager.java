/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.fdt;

import com.virtualcode.agent.das.utils.Utility;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

/**
 *
 * @author HP
 */
public class FDTQueueManager {

    private static final int MAX_LIMIT   =   10;
    private static HashMap<String, ArrayList<String>> fdtQueue    =   new HashMap<String, ArrayList<String>>();
    private static HashMap<String, String> destPaths    =   new HashMap<String, String>();
    private static HashMap<String, String> fdtHosts    =   new HashMap<String, String>();
    
    public static void initFDTManager(String execID, String destPath, String fdtHost) {
        destPaths.put(execID, destPath);
        fdtHosts.put(execID, fdtHost);
        fdtQueue.put(execID, new ArrayList<String>());
    }
    
    public static boolean putInQueue(String execID, String srcPath, Logger logger, String transID) throws Exception {
        boolean result  =   true;
        
        //put or update the fdtList in queue
        if(fdtQueue.get(execID)==null) {
            logger.warn(transID + " FDT queue is invoked without initialization for "+execID);
            result  =   false;
            
        } else {
            fdtQueue.get(execID).add(srcPath);
            //fdtQueue.put(execID, fdtSrcList);
            logger.info(transID + " path added for ("+execID+") in fdt queue "+srcPath);
            
            //process the queue - if its the Threshold limit
            //if(fdtQueue.get(execID).size() % 2==0) {
                result  =   processQueue(execID, logger, transID);
            //}
        }
        return result;
    }
    
    public static boolean processQueue(String execID, Logger logger, String transID) throws Exception {
        boolean result  =   false;
        
        ArrayList<String> srcPathsList   =   (ArrayList<String>)fdtQueue.get(execID).clone();
        fdtQueue.get(execID).clear();//immediately clear the list for next use
        
        if(srcPathsList!=null && srcPathsList.size()>0) {
            
            //create a temporary file, and insert the filesList into it (One file per line)
            String tempFile =   "processing_"+System.currentTimeMillis()+".txt";
            Utility.putPathsInFile(tempFile, srcPathsList);
            logger.info(transID+" executing the filesList of "+tempFile);
            
            //execute the FDT client request
            result  =   Utility.runFDTPoolClient(tempFile, destPaths.get(execID), fdtHosts.get(execID), logger, transID);
            
            //remove the temporary file
            File tempF  =   new File(tempFile);
            if(tempF.exists())
                tempF.delete();
            
        } else {
            logger.warn(transID + " FDT list does not exist against this execID: "+execID);
            result  =   false;
        }
        
        return result;
    }
}
