/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.fdt;

import com.virtualcode.agent.das.utils.Utility;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

/**
 *
 * @author HP
 */
public class FDTManager {

    private static final int MAX_LIMIT   =   10;
    private static String fdtHosts  =   null;
    
    public static void initFDTManager(String fdtHost) {
        fdtHosts = fdtHost;
    }
        
    public static boolean processFDT(String execID, String srcPath, String guid, Logger logger, String transID) throws Exception {
		/*
        boolean result  =   false;
        
        if(srcPath!=null) {
            //create destPath for this file
            String destPath =   Utility.getIDBasedFolderPath(guid, false);
            logger.info(transID + " dest path at filestore will be: "+destPath);
            
            ////create a temporary file, and insert the filesList into it (One file per line)
            //String tempFile =   guid+".txt";
            //Utility.putPathInFile(tempFile, srcPath);
            //logger.info(transID+" executing the filesList of "+tempFile);
            
            //execute the FDT client request
            result  =   Utility.runFDTPoolClient(srcPath, destPath, fdtHosts, logger, transID);
            
            ////remove the temporary file
            //File tempF  =   new File(tempFile);
            //if(tempF.exists())
            //    tempF.delete();
            
        } else {
            logger.warn(transID + " FDT list does not exist against this execID: "+execID);
            result  =   false;
        }
        */
        return true;
    }
}
