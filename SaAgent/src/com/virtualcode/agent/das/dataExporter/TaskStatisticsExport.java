/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataExporter;

import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.archivePolicy.dto.JobStatistics;
import com.virtualcode.agent.das.dataArchiver.TaskKPI;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.utils.MyHashMap;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Formatter;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.log4j.Logger;

/**
 *
 * @author se7
 */
public class TaskStatisticsExport implements Runnable {

    private static MyHashMap executionCount = new MyHashMap();
    private static MyHashMap failedCount = new MyHashMap();
    private static MyHashMap comulativeSize =   new MyHashMap();
    
    FileTaskInterface task = null;
    Logger errorLog = null;
    Logger taskSummary = null;

    public static void resetExecutionCtr(String execID) {
        executionCount.put(execID, new AtomicLong(0));
    }
    public static String getExecutionCount(String execID) {
        return executionCount.get(execID);
    }

    public static void resetFailedCtr(String execID) {
        failedCount.put(execID, new AtomicLong(0));
    }
    public static String getFailedCount(String execID) {
        return failedCount.get(execID);
    }
    
    public static void resetComulativeSize(String execID) {
        comulativeSize.put(execID, new AtomicLong(0));
    }
    public static String getComulativeSize(String execID) {
        return comulativeSize.get(execID);
    }

    public TaskStatisticsExport(FileTaskInterface task) {
        this.task = task;
        errorLog =   LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_ERROR,task.getExecID());
        taskSummary =   LoggingManager.getLogger(LoggingManager.EXP_TASK_SUMMARY, task.getExecID());
    }

    @Override
    public void run() {
        TaskKPI taskKPI =   task.getTaskKpi();
        
        AtomicLong	tempSize	=	new AtomicLong(Long.parseLong(comulativeSize.get(task.getExecID())));
        if(task.getTaskKpi().isExported)//if Exported
        	tempSize.addAndGet(task.getTaskKpi().getFileSize());
        comulativeSize.put(task.getExecID(), tempSize);//Store it into Static var, to be used in JobSummary
        
        Job j = ExportExecutors.currentExecs.get(task.getExecID());
        JobStatistics s =   j.getJobStatistics();
        s.setArchivedVolume(tempSize.longValue());
        s.update(taskKPI);

        StringBuilder sb = new StringBuilder();
        Formatter formatter = new Formatter(sb, Locale.US);

        formatter.format("{ID:%s},{ExEval: %d secs},{FSize: %d},{Exp: %d secs}",
                task.getUid().toString(),
                (task.getTaskKpi().evalutionEndTime - task.getTaskKpi().evalutionStartTime) / 1000,
                (task.getTaskKpi().getFileSize()),
                (task.getTaskKpi().exportEndTime - task.getTaskKpi().exportStartTime) / 1000);

        taskSummary.info(sb.toString());
        
        if (task.getTaskKpi().errorDetails != null) {
            failedCount.increment(task.getExecID());//.addAndGet(1);
            Writer writer = new StringWriter();
            PrintWriter printWriter = new PrintWriter(writer);
            task.getTaskKpi().errorDetails.printStackTrace(printWriter);
            
            errorLog.error("F : " + task.getUid().toString() + " : " + task.getTaskKpi().errorDetails.getMessage() + "\n" + writer.toString());
            writer = null;
            printWriter = null;
            
        }
        executionCount.increment(task.getExecID());//.addAndGet(1);
        
        task.closeCurrentTask(false);
        task = null;
    }
}
