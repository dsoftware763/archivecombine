/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataExporter;

import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.fileSystems.repo.RepoTask;
import com.virtualcode.agent.das.utils.CommonJobUtils;
import com.virtualcode.agent.das.utils.MyHashMap;
import com.virtualcode.agent.das.utils.Utility;

import com.virtualcode.agent.das.threadsHandler.ExportTPEList;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.log4j.Logger;

/**
 *
 * @author Abbas
 */
public class FileExporter implements Runnable {

    public FileExporter(FileTaskInterface task, String actionType) {
        this.task = task;
        this.actionType =   actionType;
        logger  =   LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_DETAIL,task.getExecID());
        if(MAX_NO_OF_TRIES==-1) {//set from *.properties file, if its the v first object of FileExporter
            String temp =   Utility.getAgentConfigs().getMaxNoOfTries()+"";
            MAX_NO_OF_TRIES =   Integer.parseInt(temp);
        }
    }
    private static int MAX_NO_OF_TRIES =   -1;
    //private final static AtomicLong executionCount = new AtomicLong(0);
    private static MyHashMap executionCount = new MyHashMap();
    FileTaskInterface task = null;
    String actionType   =   null;
    Logger logger = null;

    private boolean Process() {
        boolean output  =   false;
        
        CommonJobUtils	cju	=	new CommonJobUtils(logger, task);
        if("export".equalsIgnoreCase(actionType)) {
            String repoPath =   task.getSecureRepoPath();
            //"/SecureGet/f9b1fe21d2467eff1c9f32b5e0542ac83dc339e6d68a4b49f1864e492b4ac123818c98bf7a7910c570ccf7adb4f5e2684965f2bedd44330afdbb587d4f0fe17e8b60508bf0874e10f28e8a4c90392b1052d94bb6065228ce7517a2069d2fced56e38ccebe8ad485afe6c5280f1f152c9808bf63a5838a9593f08190627d1c1ea2fe6efbda17a3b0a0b72d93245247b44738f7bbbdf0f99d9";
            //SecureGet/f9b1fe21d2467eff1c9f32b5e0542ac83dc339e6d68a4b49f1864e492b4ac123818c98bf7a7910c555348f8bcb1b31cda47d73da488f4347fff482f2ad80303a0ca800a233c4a226987bb1fcf08bc630d25d218c8dcf2e5f93793c569b93e20336a3a5ea6c417780093a2d822e837d9126fcb178d5ae9c49beb85adeacfd87eb79b70813e50aa314c7bc8236b98474063097bfbf87716560752ee01cd7a2011f");
            repoPath        =   Utility.getDASUrl()+repoPath;
            logger.info(this.task.getUid().toString() + " Repo URL: " + repoPath);
            
            output	=	cju.fetchContentsForStub(repoPath, 
                            task.getDestPath(),
                            task.getActiveActionType());
        
        } else if("EXPORTSTUB".equalsIgnoreCase(actionType)) {
            
            RepoTask repoTask   =   (RepoTask)task;//we know that, here the instance of Task is only of RepoTask...
            output	=	cju.createURLStub(task.getDestPath(), repoTask.getDocument().getStubURL());
            
        } else {
            logger.error(this.task.getUid() + " Incorrect action Type: "+actionType);
        }
        /* OLD MECHANISM of Remote Agent is replaced with CommonJobUtils for Export/Restore..  :)
         * Hence the following code is NO more functional
         * 
        OutputStream fos    =   null;//its the Parent class of both SmbFileOutputStream and FileOutputStream
        DataInputStream dis     =   null;
        
        try {
            //prepare the directory, where has to write the data stream
            String destPath =   task.getDestPath();
            destPath =   createDirectoryStructure(destPath);
            logger.info(this.task.getUid().toString() + " Exporting at: "+destPath);
            
            //build connections to read file
            URL                url; 
            URLConnection      urlConn; 
            
            url     = new URL(repoPath); 
            urlConn = url.openConnection(); 
            urlConn.setDoInput(true); 
            urlConn.setUseCaches(false);

            //destPath    =   "smb://network.vcsl;se7:s7*@vc7.network.vcsl/adTariz/";
            String destFile =   getModifiedFileName(destPath, this.task.getDocName(), 0);
            
            if(destPath.toLowerCase().contains("smb://")) {//if Dest Path is CIFS
                fos =   new SmbFileOutputStream(destFile);
            } else {
                fos =   new FileOutputStream(destFile);
            }
            dis =   new DataInputStream(urlConn.getInputStream());

            // Now copy bytes from the URL to the output stream
            byte[] buffer = new byte[4096];
            int bytes_read;
            while((bytes_read = dis.read(buffer)) != -1)
                fos.write(buffer, 0, bytes_read);
            
            //Set the resultant output to True
            output  =   true;
            destFile    =   null;
            
            String stubPath =   destPath + "/" + this.task.getDocName() + ".url";
            //stubPath    =   stubPath.substring(0, stubPath.lastIndexOf(".")) + ".url";
            logger.info(this.task.getUid().toString() + " Deleting Stub: "+stubPath);
            
            try {
                if(stubPath.toLowerCase().contains("smb://")) {//if Dest Path is CIFS
                    SmbFile f =   new SmbFile(stubPath);
                    f.delete();
                    f = null;
                } else {
                    File f =   new File(stubPath);
                    f.delete();
                    f = null;
                }
            } catch (Exception ie) {
                //ie.printStackTrace();
                logger.error(this.task.getUid().toString() +" Deletion Failed: " +ie.getMessage());
            }
            
        } catch (IOException ioe) {
            logger.error(this.task.getUid().toString() +" : "+ ioe.getMessage());
            ioe.printStackTrace();
            output  =   false;
            
        } finally {
            if (dis!=null) 
                try {
                    dis.close();
                } catch (IOException e) {}
            if (fos!=null) 
                try {
                    fos.close();
                } catch (IOException e) {}
        }
        */
        return output;
    }

    /*
    private String getModifiedFileName(String destPath, String docName, int ver) throws MalformedURLException, SmbException {
        String fullName   =   destPath + "/" +docName;
        
        boolean isExist =   false;
        if(fullName.toLowerCase().contains("smb://")) {//if Dest Path is CIFS
            
            synchronized (this.getClass()) {//Only one instance of following block should execute at a time                
                SmbFile file    =   new SmbFile(fullName);
                if(file.exists()) {  
                    isExist =   true;
                }
                file    =   null;
            }
            
        } else {//case of NTFS
           
            File file    =   new File(fullName);
            if(file.exists()) {  
                isExist =   true;
            }
            file    =   null;
        }
        
        if(isExist) {
            logger.info(this.task.getUid().toString() +" Already Exist: "+fullName);
            String temp =   null;

            if(docName!=null && 
                    docName.contains(".")) {//case to deal with *.EXT

                temp =   docName.substring(0, docName.lastIndexOf("."))
                         +"_"+System.currentTimeMillis()+ ver + 
                         docName.substring(docName.lastIndexOf("."));

            } else {//if file name dont have extension

                logger.info(this.task.getUid().toString() +" Not have any Extension... ");
                temp =   docName +"_"+ System.currentTimeMillis()+ ver;

            }
            //ReRun to see if this file still already exist...
            fullName    =   getModifiedFileName(destPath, temp, ++ver);
            logger.info(this.task.getUid().toString() +" Full name becomes: "+fullName);
        }
        
        return fullName;
    }
    
    private String createDirectoryStructure(String destPath) {
        
        try {
            
            if(destPath.toLowerCase().contains("smb://")) {//if Dest Path is CIFS
                
                synchronized (this.getClass()) {//Only one instance of following block should execute at a time
                    SmbFile file    =   new SmbFile(destPath);
                    if(!file.exists())
                        file.mkdirs();
                }
                
            } else {
                File file   =   new File(destPath);
                if(!file.exists())
                    file.mkdirs();
            }
        } catch( MalformedURLException | SmbException mue) {
            mue.printStackTrace();
            logger.error(this.task.getUid().toString() + " : " + mue.getMessage());
        }
        return destPath;
    }*/
    
    public static void resetExecutionCtr(String execID) {
        executionCount.put(execID, new AtomicLong(0));
    }
    public static String getExecutionCount(String execID) {
        return executionCount.get(execID);
    }

    @Override
    public void run() {

        try {

            task.getTaskKpi().exportStartTime = System.currentTimeMillis();
            String authString = "repoName="+Utility.getAgentConfigs().getCompany().getCompanyTag()+ "&username=" + Utility.getAgentConfigs().getLogin() + "&password=" + Utility.getAgentConfigs().getPassword() + "";
            task.setEncodedString(new String(Utility.encodetoBase64(authString.getBytes())) );
             
            boolean successStatus =   false;
            int ctr =   1;
            while (!successStatus && ctr<=MAX_NO_OF_TRIES) {
                logger.info("No of Try to Export: "+ctr);
                successStatus = Process();
                ctr++;
            }
            task.getTaskKpi().isExported    =   successStatus;
            task.getTaskKpi().exportEndTime = System.currentTimeMillis();
            /*
            if (task.getTaskKpi().isExported && FileExecutors.currentJob.getActionType().toLowerCase().trim().equals("stub")) {
                nextExecutor.execute(new FileStubber(task));
            } else 
             */
            if (task.getTaskKpi().isExported) {
                ExportTPEList.startStatCalculator(task.getExecID(), task);
            } else {
                throw new Exception("Unable to export the file");
            }

        } catch (Exception ex) {
            
            task.getTaskKpi().failedExporting = true;
            task.getTaskKpi().errorDetails = ex;
            ExportTPEList.startStatCalculator(task.getExecID(), task);
            
        } finally {
            executionCount.increment(task.getExecID());//.addAndGet(1);
        }
    }
}