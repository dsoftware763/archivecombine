/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataExporter;

import com.virtualcode.agent.das.fileSystems.repo.RepoTask;
import com.vcsl.sa.export.Document;
import com.virtualcode.agent.das.threadsHandler.ExportTPEList;

/**
 *
 * @author Abbas
 */
public class RepoWalker {
    
    private String thisDocDestPath =   null;
    private String execID   =   null;
    private String activeActionType =   null;
    private boolean deduplicatedVolume    =   false;
    private String strPath;

    public RepoWalker(String thisDocDestPath, String execID, String activeActionType, boolean deduplicatedVolume,String strPath) {
        this.thisDocDestPath   =   thisDocDestPath;
        this.execID   =   execID;
        this.activeActionType   =   activeActionType;
        this.deduplicatedVolume=  deduplicatedVolume;
        this.strPath = strPath;
    }
    
    public boolean visitFile(Document doc) {
        
        ExportExecutors.incrementCounter(this.execID);//.fileWalkedCount += 1;
        RepoTask task = new RepoTask(doc, this.thisDocDestPath, this.execID, this.activeActionType, deduplicatedVolume,this.strPath);
        System.out.println("visiting RepoDoc: "+doc.getUrl());
        ExportTPEList.startFileEvaluator(task.getExecID(), task);
        
        return true;
    }

    public void postVisitDirectory() {
    }

    public void preVisitDirectoryFailed() {
    }

    public void visitFileFailed() {
    }
}
