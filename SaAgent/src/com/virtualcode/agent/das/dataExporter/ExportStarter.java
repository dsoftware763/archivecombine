/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataExporter;


import executors.DataLayer;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.archivePolicy.dto.Job;

import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Abbas
 */
public class ExportStarter implements Runnable {
    
    private String AgentName    =   null;
    private String actionType   =   null;
    
    //private static Logger logger    =   Logger.getLogger(ExportStarter.class);
    public static ArrayList<String> underProcessExpJobsList   =   new ArrayList<String>();
    private static List<String> interuptedExecIDsList  =   null;
    
    public ExportStarter(String AgentName) {
        this.AgentName  =   AgentName;
        this.actionType =   "EXPORT";
    }
    
    @Override
    public void run() {
        System.out.println("EXPORT: "+AgentName + " waiting for Export Job...");
        try {
            while (true) {
                try {
                    
                    List<Job> jobsList  =   DataLayer.JobsbyAgent(AgentName, actionType);
                    if(jobsList!=null && !jobsList.isEmpty() && jobsList.size()>0) {
                                                
                        interuptedExecIDsList  =   DataLayer.interuptedJobs();
                        
                        for (Job j : jobsList) {
                            int jID = j.getId();
                            System.out.println("EXPORT: "+"Export Job ID : " + jID);

                            if(underProcessExpJobsList.contains(jID+"")) {
                                System.out.println("EXPORT: "+"Already processing: "+jID);

                            }/* else if(interuptedJobIDsList.contains(jID+"")) {
                                logger.warn("Job is Canceled / Interupted: "+jID);
                                
                            } */ else {
                                underProcessExpJobsList.add(jID+"");

                                String executionID = DataLayer.executionStarted(jID);
                                System.out.println("EXPORT: "+"Export Execution ID : " + executionID);

                                LoggingManager.configureLogger(jID, executionID,LoggingManager.EXP_ACTIVITY_DETAIL);
                                LoggingManager.configureLogger(jID, executionID,LoggingManager.EXP_ACTIVITY_ERROR);
                                LoggingManager.configureLogger(jID, executionID,LoggingManager.EXP_JOB_SUMMARY);
                                LoggingManager.configureLogger(jID, executionID,LoggingManager.EXP_TASK_SUMMARY);
                                //Logger logger = LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_ERROR, executionID);

                                ExportThread jt    =   new ExportThread(j, executionID);
                                Thread job      =   new Thread(jt);
                                job.start();
                            }
                            System.out.println("EXPORT: "+"Seeking next export Job...");
                        }
                    }
                }catch(Exception ex) {//if WS is down at Server side, then dont' Terminate...
                    ex.getMessage();
                    ex.printStackTrace();
                    //LoggingManager.printStackTrace(ex, logger);
                    
                } finally {
                    /*
                    fileEvaluator = null;
                    fileExporter = null;
                    statisticsCalculator = null;
                     */
                }
                System.gc();
                Thread.sleep(1000 * 60);//wait for 60sec
            }
            
        } catch (InterruptedException ex) {//if Thread.sleep is interupted
            ex.getMessage();
            ex.printStackTrace();
            //LoggingManager.printStackTrace(ex, logger);
        }
    }
    
    public static boolean isInterupted(String execID) {
        
        //String curJobID =   ExportExecutors.currentJobs.get(execID).getId()+"";
        boolean isInterupted =   false;
        
        synchronized(ExportStarter.class) {
            isInterupted =   ExportStarter.interuptedExecIDsList.contains(execID);
        }
        if(isInterupted)
            LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_DETAIL, execID).warn("ExecID ("+execID+") is Interupted by User while executing: "+execID);
        
        return isInterupted;
    }
}