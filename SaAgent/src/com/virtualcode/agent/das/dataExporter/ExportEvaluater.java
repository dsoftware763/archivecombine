package com.virtualcode.agent.das.dataExporter;

import com.virtualcode.agent.das.dataArchiver.EvalutorFlag;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.fileSystems.repo.RepoTask;
import com.virtualcode.agent.das.utils.Utility;
import com.vcsl.sa.export.Document;
import com.virtualcode.agent.das.utils.GlobMatch;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.documentManagement.dto.DocumentType;

import com.virtualcode.agent.das.utils.MyHashMap;
import com.virtualcode.agent.das.threadsHandler.ExportTPEList;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.log4j.Logger;

/**
 * @author Abbas
 */
public class ExportEvaluater implements Runnable {

    Logger logger = null;

    public ExportEvaluater(RepoTask task) {
        this.task = task;
        logger  =   LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_DETAIL,task.getExecID());
    }
    public FileTaskInterface getFileTask() {
        return this.task;
    }
    
    //private final static AtomicLong executionCount = new AtomicLong(0);
    private static MyHashMap executionCount = new MyHashMap();
    RepoTask task = null;
    String extensions = "";
    private Long docAge = 0L;
    private Long docSize = 0L;
    private Long docLM = 0L;
    private Long docLA = 0L;
    private boolean ProcessHD = false;
    private boolean ProcessRO = false;

    private boolean Process() throws Exception {

        //Path file = task.getPath();
        //Path name = file.getFileName();
        Document doc      =   this.task.getDocument();
        String docName    =   doc.getTitle();
        String docPath    =   doc.getUrl();//path of doc in Repository
        
        
        logger.info(task.getUid().toString() + " : Evaluating : " + docPath);
        GlobMatch matcher   =   new GlobMatch();
        String[] exts = extensions.split(",");
        Exception ex = null;

        if (exts.length == 0) {
            exts = "*.*,*.*".split(",");
        }

        for (String s : exts) {
            if (s == null || s.isEmpty()) {
                /////////   rp.skippedNullEmpty += 1;
                continue;
            }
            
            //matcher = FileSystems.getDefault().getPathMatcher("glob:" + s);
            if (docName != null && matcher.match(docName, s)) {
                logger.info(task.getUid().toString() + " : Matched : " + s);
                task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND;
                if (docName.isEmpty()
                        //|| docName.startsWith("~")
                        //|| docName.startsWith("$")
                        //|| cFileName.startsWith(".")
                        //|| docName.endsWith(".lnk")
                        //|| docName.endsWith(".tmp")
                        //|| docName.endsWith(".url")
                        ) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_TEMPORARY;
                    logger.info("Link File OR Temporary File OR Office temporary File : " + docName);
                    return true;
                }

                if (doc.isFolderYN()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_ISDIR;
                    logger.info("Is a Directory" + docPath);
                    return true;
                }

                /*
                if (doc.isLinkedNodeYN()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_SYMBOLLINK;
                    logger.info("Is a Linked Node " + docPath);
                    return true;
                }
                
                if (attrs.isOther()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_SYMBOLLINK;
                    logger.info("Is a Other File, no idea what does it mean, may be not regular " + file);
                    return true;
                }

                if (!ProcessHD && doc.isHidden()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_HIDDENLECTED;
                    logger.info("Is a Hidden File " + cFilePath);
                    return true;
                }

                if (!ProcessRO && !smbFile.canWrite()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_READ_ONLY;
                    logger.info("Is a Readonly File " + cFilePath);
                    return true;
                }
                 * 
                 */

                if (docSize > 0 && doc.getSize() < docSize) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_SMALL_SIZE;
                    logger.info("File Size : ( " + doc.getSize() + " ) " + " Required Size : ( " + docSize + " ) ");
                    return true;
                }/* else if (docAge > 0 && attrs.creationTime().toMillis() > docAge) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_AGE;
                    logger.info("File Creation Time : ( " + attrs.creationTime().toMillis() + " ) " + " Required Time : ( " + docAge + " ) ");
                    return true;
                }*/ else if (docLA > 0 && Long.parseLong(doc.getAccessedOn()) > docLA) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_LASTACCESSTIME;
                    logger.info("File Last Access Time : ( " + doc.getAccessedOn() + " ) " + " Required : ( " + docLA + " ) ");
                    return true;
                } else if (docLM > 0 && Long.parseLong(doc.getModifiedOn()) > docLM) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_LASTMODIFIEDTIME;
                    logger.info("File Last Modified Time : ( " + doc.getModifiedOn() + " ) " + " Required : ( " + docLM + " ) ");
                    return true;
                } else {
                    return true;
                }
            }
        }
        return false;
    }

    public static void resetExecutionCtr(String execID) {
        executionCount.put(execID, new AtomicLong(0));
    }
    public static String getExecutionCount(String execID) {
        return executionCount.get(execID);
    }

    @Override
    public void run() {
        try {
            task.getTaskKpi().evalutionStartTime = System.currentTimeMillis();

            Job aJobDetail = ExportExecutors.currentExecs.get(task.getExecID());
            Iterator newIterator = aJobDetail.getPolicy().getDocumentTypeSet().iterator();
            while (newIterator.hasNext()) {
                DocumentType documentType = (DocumentType) newIterator.next();
                extensions = extensions + "*" + documentType.getTheValue() + ",";
            }
            if (extensions.isEmpty()) {
                extensions = "*";
            }

            String dAge = aJobDetail.getPolicy().getDocumentAge();
            String laDate = aJobDetail.getPolicy().getLastAccessedDate();
            String lmDate = aJobDetail.getPolicy().getLastModifiedDate();
            String sz = aJobDetail.getPolicy().getSizeLargerThan();
            docAge = Utility.calculateMilliSec(Utility.parseYear(dAge), Utility.parseMonth(dAge));
            docLM = Utility.calculateMilliSec(Utility.parseYear(lmDate), Utility.parseMonth(lmDate));
            docLA = Utility.calculateMilliSec(Utility.parseYear(laDate), Utility.parseMonth(laDate));
            docSize = Utility.calculateSize(sz);
            ProcessRO = aJobDetail.isProcessReadOnly();
            ProcessHD = aJobDetail.isProcessHidden();
            boolean p = Process();
            task.getTaskKpi().evalutionEndTime = System.currentTimeMillis();

            if (!p && task.getTaskKpi().evaluatorFlag != EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND) {
                task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_EXTENSION;
            }

            //IT should not get called
            if (task.getTaskKpi().evaluatorFlag != EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND) {
                throw new Exception("Task : " + this.task.getSecureRepoPath() + " Reason: " + task.getTaskKpi().evaluatorFlag.toString());
            }

            //If current file is ready to export
            if (task.getTaskKpi().evaluatorFlag == EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND) {
                
                //Get the file-size, to updated Evaluation vars
                Long curSize =   this.task.getDocument().getSize();
                if(curSize!=null && curSize.intValue() > 0) {//it can be NULL or 0, in case of Linked Nodes
                    task.getTaskKpi().setFileSize(curSize);
                    task.getTaskKpi().updateComulativeSize(curSize);
                }
            }
            
            if (task.getTaskKpi().evaluatorFlag == EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND
                    && (aJobDetail.getActionType().trim().equalsIgnoreCase("export") ||
                    aJobDetail.getActionType().trim().equalsIgnoreCase("exportstub"))) {
                logger.info(task.getUid() + " Getting from Repository ");
                
                ExportTPEList.startFileExporter(task.getExecID(), task, aJobDetail.getActionType());
                //ExportExecutors.statisticsCalculator.execute(new TaskStatisticsExport(task));
            } else {
                logger.info(task.getUid() + " Sending to Task Statistic ");
                ExportTPEList.startStatCalculator(task.getExecID(), task);
            }
        } catch (Exception ex) {
            task.getTaskKpi().errorDetails = ex;
            task.getTaskKpi().failEvaluation = true;
            ExportTPEList.startStatCalculator(task.getExecID(), task);
        } finally {
            executionCount.increment(task.getExecID());//.addAndGet(1);
        }
    }
}