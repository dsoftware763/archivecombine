/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataExporter;

import executors.DataLayer;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.archivePolicy.dto.ExcludedPath;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.agent.das.fileSystems.FileServiceInterface;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.archivePolicy.dto.SPDocLib;
import com.virtualcode.agent.das.documentManagement.dto.DocumentType;
import com.virtualcode.agent.das.fileSystems.repo.RepoService;
import com.virtualcode.agent.das.utils.MyHashMap;
import com.virtualcode.agent.das.threadsHandler.ExportTPEList;
import com.virtualcode.agent.das.threadsHandler.HotStatsUpdater;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.log4j.Logger;

/**
 *
 * @author se7
 */
public class ExportExecutors {

    public static HashMap<String, Job> currentExecs = new HashMap<String, Job>();
    //public static long fileWalkedCount = 0L;
    private static MyHashMap fileWalkedCount = new MyHashMap();
    
    void startJob(Job job, String executionID) throws InterruptedException, Exception {
        Logger logger = LoggingManager.getLogger(LoggingManager.EXP_JOB_SUMMARY, executionID);
        int errorCodeID = 0;
        int jID = -1;
        
        //Initialize the Counters for current Execution of cur Job
        fileWalkedCount.put(executionID, new AtomicLong(0));
        ExportEvaluater.resetExecutionCtr(executionID);
        FileExporter.resetExecutionCtr(executionID);
        TaskStatisticsExport.resetExecutionCtr(executionID);
        TaskStatisticsExport.resetFailedCtr(executionID);
        TaskStatisticsExport.resetComulativeSize(executionID);

        Job currentJob  =   null;
        try {          
            jID = (int) job.getId();

            currentExecs.put(executionID, job);
            currentJob  =   currentExecs.get(executionID);
            currentJob.getJobStatistics().setJobStartTime(new Date());
            
            String authString   = "repoName="+Utility.getAgentConfigs().getCompany().getCompanyTag()+ "&username=" + Utility.getAgentConfigs().getLogin() + "&password=" + Utility.getAgentConfigs().getPassword() + "";
            String secureInfo   =   new String(Utility.encodetoBase64(authString.getBytes()));
            System.out.println("Encoded Secure Info: "+secureInfo);
            
            // make/initiate the ThreadPools for current Execution of an Export Job
            ExportTPEList threadsPoolSet  =   new ExportTPEList(executionID, logger);
            
            if (executionID == null) {
                throw new Exception("Execution ID is null");
            }

            logger.info("Job Info");
            logger.info("=======");
            logger.info("Execution ID " + executionID);
            logger.info("Job Name " + currentJob.getJobName());
            logger.info("Job Type " + currentJob.getActionType());
            Iterator iterator = job.getSpDocLibSet().iterator();
            while (iterator.hasNext()) {
                SPDocLib spDocLib = (SPDocLib) iterator.next();
                logger.info("\t Repo Path : " + spDocLib.getLibraryName());
                String libName  =   spDocLib.getDestinationPath();
                logger.info("\t Dest Path : " + ((libName!=null && libName.contains("@"))?libName.substring(libName.lastIndexOf("@")):libName));
                logger.info("\t isRecursive: " + spDocLib.isRecursive());
                logger.info("\t isDeduplicatedVolume: " + spDocLib.isDeduplicatedVolume());
                /* 
                 * Not requiret yet, bcoz Export Jobs are not supporting Exclude option
                System.out.println("job -> Paths -> excluded paths:");
                Iterator excludePathIterator = spDocLib.getExcludedPathSet().iterator();
                while (excludePathIterator.hasNext()) {
                    ExcludedPath excludePath = (ExcludedPath) excludePathIterator.next();
                    System.out.println("\t \t" + excludePath.getPath());
                }
                 */
            }

            logger.info("job -> policy name : " + job.getPolicy().getPolicyName());
            logger.info("job -> policy -> document-types: ");
            Iterator newIterator = job.getPolicy().getDocumentTypeSet().iterator();
            while (newIterator.hasNext()) {
                DocumentType documentType = (DocumentType) newIterator.next();
                logger.info("\t documentType name: " + documentType.getTheValue());
            }

            //Initialize the Continous Stats update process - to provide HotStats to server...
            HotStatsUpdater hsUpdater   =   new HotStatsUpdater(jID, executionID, 
                    errorCodeID, currentJob.getJobStatistics(), 
                    LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_DETAIL, executionID));
            Thread th = new Thread(hsUpdater);
            th.start();
            
            for (SPDocLib p : job.getSpDocLibSet()) {
                FileServiceInterface    fs  =   null;
                if("EXPORT".equalsIgnoreCase(currentJob.getActionType()) ||
                        "EXPORTSTUB".equalsIgnoreCase(currentJob.getActionType())) {//if this is Export Type Job
                    fs  =   new RepoService(secureInfo, executionID, currentJob.getActiveActionType(), p.isDeduplicatedVolume());//"E:/TestFiles/Exported"
                } else {
                    logger.error("Export module dont support the Job of type "+currentJob.getActionType());
                }
                
                //Prepare the list of Excluded sub-paths of current Path
                ArrayList<String> excPathList   =   new ArrayList();
                for(ExcludedPath ep : p.getExcludedPathSet()) {
                    excPathList.add(ep.getPath());
                }
                fs.walkFileTree(p.getLibraryName(), excPathList, p.isRecursive(), p.getDestinationPath());
            }
            
            //Wait for shutting down the threads of current Execution
            threadsPoolSet.stopCurThreadPools();
            ////PROCEED FURTHER AFTER STOPPING THE THREAD POOL OF CUR JOB-EXEC
            
            // clear executors
            //logger.info("Memory ... " + Runtime.getRuntime().totalMemory());
            logger.info("File Walked : " + fileWalkedCount.get(executionID));
            fileWalkedCount.put(executionID, new AtomicLong(0));//reset value to 0
            
            logger.info("");
            logger.info("Executors Report");
            logger.info("=================");
            logger.info("Repo Nodes Evaluted : " + ExportEvaluater.getExecutionCount(executionID));
            logger.info("Exported : " + FileExporter.getExecutionCount(executionID));
            //logger.info("Stubbed : " + FileStubber.getExecutionCount(executionID));
            logger.info("Failed : " + TaskStatisticsExport.getFailedCount(executionID));
            logger.info("Summary : " + TaskStatisticsExport.getExecutionCount(executionID));
            logger.info("Total Size : " + TaskStatisticsExport.getComulativeSize(executionID) + " bytes");

            currentJob.getJobStatistics().setJobEndTime(new Date());

            logger.info("Calling WS for executionCompleted with ID : " + executionID);

            boolean success = DataLayer.exportExecutionCompleted(jID, executionID, errorCodeID, currentJob.getJobStatistics());
            if (success) {
                logger.info("Successfull returned from execution Completed WS : " + jID);
            } else {
                throw new Exception("Oops.Error returned from execution Completed WS : " + jID);
            }

        } catch (Exception ex) {
            errorCodeID = 1;
            if (executionID != null) {

                if (currentJob == null) {
                    logger.info("Current Job is Null");
                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.exportExecutionCompleted(jID, executionID, errorCodeID, null);

                } else if (currentJob.getJobStatistics() == null) {
                    logger.info("currentJob.getJobStatistics() is Null");
                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.exportExecutionCompleted(jID, executionID, errorCodeID, null);
                } else {
                    currentJob.getJobStatistics().setJobEndTime(new Date());

                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.exportExecutionCompleted(jID, executionID, errorCodeID, currentJob.getJobStatistics());
                }
            }
            logger.fatal("ExecutionID: " + executionID);
            logger.fatal("JobID: " + jID);
            logger.fatal("ErrorID: " + errorCodeID);
            logger.fatal(ex.getMessage());
            Writer writer = new StringWriter();
            PrintWriter printWriter = new PrintWriter(writer);
            ex.printStackTrace(printWriter);

            LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_ERROR, executionID).error("Fatal : " + "\n" + writer.toString());
            writer = null;
            printWriter = null;

        } finally {
            currentJob = null;
            currentExecs.remove(executionID);
            
            if(ExportStarter.underProcessExpJobsList!=null)
                ExportStarter.underProcessExpJobsList.remove(jID+"");
        }
    }
    
    public static void incrementCounter(String execID) {
        fileWalkedCount.increment(execID);
    }
}
