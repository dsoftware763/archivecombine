/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.threadsHandler;

import java.util.HashMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.virtualcode.agent.das.dataExporter.ExportEvaluater;
import com.virtualcode.agent.das.dataExporter.ExportStarter;
import com.virtualcode.agent.das.dataExporter.FileExporter;
import com.virtualcode.agent.das.dataExporter.TaskStatisticsExport;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.fileSystems.repo.RepoTask;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
/**
 *
 * @author YAwar
 */
public class ExportTPEList {
    
    private final static int maxEQSize = Utility.getAgentConfigs().getExportEvalutorQueueSize() + 1;
    private final static int maxAQSize = Utility.getAgentConfigs().getExporterQueueSize() + 1;
    private final static int maxTQSize = Utility.getAgentConfigs().getExportJobStatisticQueueSize() + 1;
    private final static ArrayBlockingQueue<Runnable> evalutorQueue = new ArrayBlockingQueue<Runnable>(maxEQSize);
    private final static ArrayBlockingQueue<Runnable> exporterQueue = new ArrayBlockingQueue<Runnable>(maxAQSize);
    private final static ArrayBlockingQueue<Runnable> jobStatisticQueue = new ArrayBlockingQueue<Runnable>(maxTQSize);
        
    private static HashMap<String, ThreadPoolExecutor> fileEvaluatorList =   new HashMap<String, ThreadPoolExecutor>();
    private static HashMap<String, ThreadPoolExecutor> fileExporterList =   new HashMap<String, ThreadPoolExecutor>();
    private static HashMap<String, ThreadPoolExecutor> statisticsCalculatorList =   new HashMap<String, ThreadPoolExecutor>();
    
    private Logger logger   =   null;
    //non-static values, for each instance of execution...
    private String curExecID   =   null;
    
    public ExportTPEList(String execID, Logger logger) {
        this.logger =   logger;
        this.curExecID  =   execID;
        
        ThreadPoolExecutor fileEvaluator = null;
        ThreadPoolExecutor fileExporter = null;
        ThreadPoolExecutor statisticsCalculator = null;
    
        int ePoolSize = Utility.getAgentConfigs().getExportEvalutorpoolSize();
        int eMaxPoolSize = Utility.getAgentConfigs().getExportEvalutormaxPoolSize();
        int eTimeout = (Utility.getAgentConfigs().getExportEvalutorkeepAliveTime());
        String eTimeoutUnitStr = Utility.getAgentConfigs().getExportEvalutorTimeAliveUnit();
        TimeUnit eTimeoutUnit = TimeUnit.valueOf(eTimeoutUnitStr);
        ThreadPoolExecutor.CallerRunsPolicy policy = new ThreadPoolExecutor.CallerRunsPolicy();
        fileEvaluator = new ThreadPoolExecutor(
                ePoolSize,
                eMaxPoolSize,
                eTimeout,
                eTimeoutUnit,
                evalutorQueue);
        fileExporter = new ThreadPoolExecutor(
                Utility.getAgentConfigs().getExporterpoolSize(),
                Utility.getAgentConfigs().getExportermaxPoolSize(),
                (Utility.getAgentConfigs().getExporterkeepAliveTime()),
                TimeUnit.valueOf(Utility.getAgentConfigs().getExporterTimeAliveUnit()),
                exporterQueue);
        statisticsCalculator = new ThreadPoolExecutor(
                Utility.getAgentConfigs().getExportJobStatisticpoolSize(),
                Utility.getAgentConfigs().getExportJobStatisticmaxPoolSize(),
                (Utility.getAgentConfigs().getExportJobStatistickeepAliveTime()),
                TimeUnit.valueOf(Utility.getAgentConfigs().getExportJobStatisticTimeAliveUnit()),
                jobStatisticQueue);

        fileEvaluator.setRejectedExecutionHandler(policy);
        statisticsCalculator.setRejectedExecutionHandler(policy);
        fileExporter.setRejectedExecutionHandler(policy);
        
        fileEvaluatorList.put(execID, fileEvaluator);
        fileExporterList.put(execID, fileExporter);
        statisticsCalculatorList.put(execID, statisticsCalculator);
    }
    
    public void stopCurThreadPools() {
        try {
            
            logger.info("FileEvaluator Shutting Down ... ");
            fileEvaluatorList.get(this.curExecID).shutdown();
            long timeout = 20;
            int counter = 0;
            while (!fileEvaluatorList.get(this.curExecID).awaitTermination(timeout, TimeUnit.SECONDS)) {
                logger.info(" FileEvaluator is waiting to sutting down for : " + timeout * counter + " secs");
                counter++;
            }

            logger.info("FileExporter Shutting Down ... ");
            fileExporterList.get(this.curExecID).shutdown();
            counter = 0;
            while (!fileExporterList.get(this.curExecID).awaitTermination(timeout, TimeUnit.SECONDS)) {
                logger.info(" FileExporter ThreadPool is waiting to sutting down for : " + timeout * counter + " secs");
                counter++;
            }

            logger.info("TaskSummary Shutting Down ... ");
            statisticsCalculatorList.get(this.curExecID).shutdown();
            counter = 0;
            while (!statisticsCalculatorList.get(this.curExecID).awaitTermination(timeout, TimeUnit.SECONDS)) {
                logger.info(" TaskSummary ThreadPool is waiting to sutting down for : " + timeout * counter + " secs");
                counter++;
            }
            
        } catch (Exception ie) {
            LoggingManager.printStackTrace(ie, logger);
            
        } finally {
            fileEvaluatorList.put(this.curExecID, null);
            fileExporterList.put(this.curExecID, null);
            statisticsCalculatorList.put(this.curExecID, null);
            
            fileEvaluatorList.remove(this.curExecID);
            fileExporterList.remove(this.curExecID);
            statisticsCalculatorList.remove(this.curExecID);
        }
    }
    
    public static void startFileEvaluator(String execID, RepoTask task) {
        if(!ExportStarter.isInterupted(execID)) {
            fileEvaluatorList.get(execID).execute(new ExportEvaluater(task));
            
        } else {//if interupted, than go to keep Stats for current TASK
            startStatCalculator(execID, task);
        }
    }
    
    public static void startFileExporter(String execID, FileTaskInterface task, String actionType) {
        if(!ExportStarter.isInterupted(execID)) {
            fileExporterList.get(execID).execute(new FileExporter(task, actionType));
            
        } else {//if interupted, than go to keep Stats for current TASK
            startStatCalculator(execID, task);
        }
    }
    
    public static void startStatCalculator(String execID, FileTaskInterface task) {
        statisticsCalculatorList.get(execID).execute(new TaskStatisticsExport(task));
    }
}
