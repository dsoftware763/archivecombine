/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.threadsHandler;

import com.virtualcode.agent.das.archivePolicy.dto.JobStatistics;
import com.virtualcode.agent.das.dataArchiver.FileExecutors;
import com.virtualcode.agent.das.dataExporter.ExportExecutors;
import com.virtualcode.agent.das.dataIndexer.IndexingExecutors;
import com.virtualcode.agent.das.logging.LoggingManager;
import executors.DataLayer;
import org.apache.log4j.Logger;

/**
 *
 * @author HP
 */
public class HotStatsUpdater extends Thread {
    
    private int jID;
    private String executionID;
    private int errorCodeID;
    private JobStatistics jobKPI;
    private Logger logger;
    
    public HotStatsUpdater(int jID, String executionID, int errorCodeID, JobStatistics jobKPI, Logger logger) {
        this.jID =   jID;
        this.executionID = executionID;
        this.errorCodeID = errorCodeID;
        this.jobKPI = jobKPI;
        this.logger =   logger;        
    }
    
    public void run() {
        
        while(true) {
            try {                
                if(FileExecutors.currentExecs.containsKey(executionID) 
                        || ExportExecutors.currentExecs.containsKey(executionID)
                        || IndexingExecutors.currentExecs.containsKey(executionID)
                        ) {
                    
                    logger.info("updating Hot stats for "+executionID);
                    DataLayer.updateHotStatistics(jID, executionID, errorCodeID, jobKPI);
                    
                    Thread.sleep(10*1000);//wait for 10 seconds
                    
                } else {
                    logger.info("Terminating Hot Stats Thread for "+executionID);
                    break;//terminate the Thread's Loop
                }
                
            } catch (Exception e) {
                LoggingManager.printStackTrace(e, logger);
            }
        }
    }    
}
