/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.threadsHandler;

import com.virtualcode.agent.das.dataArchiver.*;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSTask;
import com.virtualcode.agent.das.fileSystems.ntfs.NTFSTask;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author YAwar
 */
public class TPEList {

    final private static int maxEQSize = Utility.getAgentConfigs().getEvalutorQueueSize() + 1;
    final private static int maxAQSize = Utility.getAgentConfigs().getArchiverQueueSize() + 1;
    //    final private static int maxDQSize = Utility.GetPoolProperty("DedupCheckerQueueSize")+1;
    final private static int maxSQSize = Utility.getAgentConfigs().getStubberQueueSize() + 1;
    final private static int maxTQSize = Utility.getAgentConfigs().getJobStatisticQueueSize() + 1;
    final private static ArrayBlockingQueue<Runnable> evalutorQueue = new ArrayBlockingQueue<Runnable>(maxEQSize);
    final private static ArrayBlockingQueue<Runnable> archiverQueue = new ArrayBlockingQueue<Runnable>(maxAQSize);
    final private static ArrayBlockingQueue<Runnable> stubberQueue = new ArrayBlockingQueue<Runnable>(maxSQSize);
    final private static ArrayBlockingQueue<Runnable> jobStatisticQueue = new ArrayBlockingQueue<Runnable>(maxTQSize);

    private static HashMap<String, ThreadPoolExecutor> fileEvaluatorList = new HashMap<String, ThreadPoolExecutor>();
    private static HashMap<String, ThreadPoolExecutor> fileStubberList = new HashMap<String, ThreadPoolExecutor>();
    private static HashMap<String, ThreadPoolExecutor> fileArchiverList = new HashMap<String, ThreadPoolExecutor>();
    private static HashMap<String, ThreadPoolExecutor> statisticsCalculatorList = new HashMap<String, ThreadPoolExecutor>();

    private Logger logger = null;

    //non-static values, for each instance of execution...
    private String curExecID = null;

    //Constructor will start anther new PoolSet, and puts its reference in already existing List
    public TPEList(String execID, Logger logger) {
        this.logger = logger;

        this.curExecID = execID;

        ThreadPoolExecutor fileEvaluator = null;
        ThreadPoolExecutor fileStubber = null;
        ThreadPoolExecutor fileArchiver = null;
        ThreadPoolExecutor statisticsCalculator = null;


        int ePoolSize = Utility.getAgentConfigs().getEvalutorpoolSize();
        int eMaxPoolSize = Utility.getAgentConfigs().getEvalutormaxPoolSize();
        int eTimeout = (Utility.getAgentConfigs().getEvalutorkeepAliveTime());
        String eTimeoutUnitStr = Utility.getAgentConfigs().getEvalutorTimeAliveUnit();
        TimeUnit eTimeoutUnit = TimeUnit.valueOf(eTimeoutUnitStr);
        ThreadPoolExecutor.CallerRunsPolicy policy = new ThreadPoolExecutor.CallerRunsPolicy();
        fileEvaluator = new ThreadPoolExecutor(
                ePoolSize,
                eMaxPoolSize,
                eTimeout,
                eTimeoutUnit,
                evalutorQueue);

        fileStubber = new ThreadPoolExecutor(
                Utility.getAgentConfigs().getStubberpoolSize(),
                Utility.getAgentConfigs().getStubbermaxPoolSize(),
                (Utility.getAgentConfigs().getStubberkeepAliveTime()),
                TimeUnit.valueOf(Utility.getAgentConfigs().getStubberTimeAliveUnit()),
                stubberQueue);
//            fileDedupChecker = new ThreadPoolExecutor(
//                    Utility.GetPoolSize("DedupCheckerpoolSize"),
//                    Utility.GetPoolProperty("DedupCheckermaxPoolSize"),
//                    (Utility.GetPoolProperty("DedupCheckerkeepAliveTime")),
//                    TimeUnit.valueOf(Utility.GetPoolPropertyAsString("DedupCheckerTimeAliveUnit")),
//                    dedupQueue);
        fileArchiver = new ThreadPoolExecutor(
                Utility.getAgentConfigs().getArchiverpoolSize(),
                Utility.getAgentConfigs().getArchivermaxPoolSize(),
                (Utility.getAgentConfigs().getArchiverkeepAliveTime()),
                TimeUnit.valueOf(Utility.getAgentConfigs().getArchiverTimeAliveUnit()),
                archiverQueue);
        statisticsCalculator = new ThreadPoolExecutor(
                Utility.getAgentConfigs().getJobStatisticpoolSize(),
                Utility.getAgentConfigs().getJobStatisticmaxPoolSize(),
                (Utility.getAgentConfigs().getJobStatistickeepAliveTime()),
                TimeUnit.valueOf(Utility.getAgentConfigs().getJobStatisticTimeAliveUnit()),
                jobStatisticQueue);

        fileEvaluator.setRejectedExecutionHandler(policy);
        fileArchiver.setRejectedExecutionHandler(policy);
        fileStubber.setRejectedExecutionHandler(policy);
        statisticsCalculator.setRejectedExecutionHandler(policy);


        fileEvaluatorList.put(execID, fileEvaluator);
        fileStubberList.put(execID, fileStubber);
        fileArchiverList.put(execID, fileArchiver);
        statisticsCalculatorList.put(execID, statisticsCalculator);
    }

    //Following method will Stop the Pool Set of only current instance's Execution ID
    public void stopCurThreadPools() {
        try {

            logger.info("FileEvaluator Shutting Down ... ");
            fileEvaluatorList.get(this.curExecID).shutdown();
            long timeout = 20;
            int counter = 0;
            while (!fileEvaluatorList.get(this.curExecID).awaitTermination(timeout, TimeUnit.SECONDS)) {
                logger.info(" FileEvaluator is waiting to shutting down for : " + timeout * counter + " secs");
                counter++;
            }

            logger.info("FileArchiver Shutting Down ... ");
            fileArchiverList.get(this.curExecID).shutdown();
            counter = 0;
            while (!fileArchiverList.get(this.curExecID).awaitTermination(timeout, TimeUnit.SECONDS)) {
                int activeCount = fileArchiverList.get(this.curExecID).getActiveCount();
                logger.info("activeCount : " + activeCount);
                logger.info("FileArchiver ThreadPool is waiting to shutting down for : " + timeout * counter + " secs");
                counter++;
            }

            logger.info("FileStubber Shutting Down ... ");
            fileStubberList.get(this.curExecID).shutdown();
            counter = 0;
            while (!fileStubberList.get(this.curExecID).awaitTermination(timeout, TimeUnit.SECONDS)) {
                logger.info("FileStubber ThreadPool is waiting to shutting down for : " + timeout * counter + " secs");
                counter++;
            }

            logger.info("TaskSummary Shutting Down ... ");
            statisticsCalculatorList.get(this.curExecID).shutdown();
            counter = 0;
            while (!statisticsCalculatorList.get(this.curExecID).awaitTermination(timeout, TimeUnit.SECONDS)) {
                logger.info("TaskSummary ThreadPool is waiting to shutting down for : " + timeout * counter + " secs");
                counter++;
            }

        } catch (Exception ie) {
            LoggingManager.printStackTrace(ie, logger);

        } finally {
            fileEvaluatorList.put(this.curExecID, null);
            fileStubberList.put(this.curExecID, null);
            fileArchiverList.put(this.curExecID, null);
            statisticsCalculatorList.put(this.curExecID, null);

            fileEvaluatorList.remove(this.curExecID);
            fileStubberList.remove(this.curExecID);
            fileArchiverList.remove(this.curExecID);
            statisticsCalculatorList.remove(this.curExecID);
        }
    }

    public static void startCIFSEvaluator(String execID, CIFSTask task) {


        //if(!JobStarter.isInterupted(execID, task.getUid())) {
        //pause and resume
        if (true) {
            fileEvaluatorList.get(execID).execute(new CIFSEvaluater(task));

        } else {//if interrupted, than go to keep Stats for current TASK
            startStatCalculator(execID, task);
        }
    }

    public static void startNTFSEvaluator(String execID, NTFSTask task) {

        //if(!JobStarter.isInterupted(execID, task.getUid())) {
        //pause and resume
        if (true) {
            fileEvaluatorList.get(execID).execute(new NTFSEvaluater(task));

        } else {//if interrupted, than go to keep Stats for current TASK
            startStatCalculator(execID, task);
        }

    }

    public static void startFileStubber(String execID, FileTaskInterface task) {

        if (!JobStarter.isInterupted(execID, task.getUid())) {
            fileStubberList.get(execID).execute(new FileStubber(task));

        } else {//if interupted, than go to keep Stats for current TASK
            startStatCalculator(execID, task);
        }
    }

    public static void startFileArchiver(String execID, FileTaskInterface task) {

        if (!JobStarter.isInterupted(execID, task.getUid())) {
            fileArchiverList.get(execID).execute(new FileArchiver(task));

        } else {//if interupted, than go to keep Stats for current TASK
            startStatCalculator(execID, task);
        }
    }

    public static void startStatCalculator(String execID, FileTaskInterface task) {
        statisticsCalculatorList.get(execID).execute(new TaskStatistic(task));
    }
}
