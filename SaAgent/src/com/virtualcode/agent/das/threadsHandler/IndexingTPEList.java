/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.threadsHandler;

import java.util.HashMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.virtualcode.agent.das.dataIndexer.FileIndexer;
import com.virtualcode.agent.das.dataIndexer.IndexEvaluater;
import com.virtualcode.agent.das.dataIndexer.IndexStarter;
import com.virtualcode.agent.das.dataIndexer.TaskStatisticsIndexed;
import com.virtualcode.agent.das.fileSystems.FileTaskIndexingInterface;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSTask;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;

/**
 *
 * @author YAwar
 */
public class IndexingTPEList {
    
    private final static int maxEQSize = Utility.getAgentConfigs().getExportEvalutorQueueSize() + 1;
    private final static int maxAQSize = Utility.getAgentConfigs().getExporterQueueSize() + 1;
    private final static int maxTQSize = Utility.getAgentConfigs().getExportJobStatisticQueueSize() + 1;
    final private static ArrayBlockingQueue<Runnable> evalutorQueue = new ArrayBlockingQueue<Runnable>(maxEQSize);
    final private static ArrayBlockingQueue<Runnable> indexerQueue = new ArrayBlockingQueue<Runnable>(maxAQSize);
    final private static ArrayBlockingQueue<Runnable> jobStatisticQueue = new ArrayBlockingQueue<Runnable>(maxTQSize);
    
    private static HashMap<String, ThreadPoolExecutor> fileEvaluatorList =   new HashMap<String, ThreadPoolExecutor>();
    private static HashMap<String, ThreadPoolExecutor> fileIndexerList =   new HashMap<String, ThreadPoolExecutor>();
    private static HashMap<String, ThreadPoolExecutor> statisticsCalculatorList =   new HashMap<String, ThreadPoolExecutor>();
    
    private Logger logger   =   null;
    
    //non-static values, for each instance of execution...
    private String curExecID   =   null;
    
    //Constructor will start anther new PoolSet, and puts its reference in already existing List
    public IndexingTPEList(String execID, Logger logger) {
        this.logger =   logger;
        
        this.curExecID =   execID;
        
        ThreadPoolExecutor fileEvaluator = null;
        ThreadPoolExecutor fileIndexer = null;
        ThreadPoolExecutor statisticsCalculator = null;

        int ePoolSize = Utility.getAgentConfigs().getExportEvalutorpoolSize();
        int eMaxPoolSize = Utility.getAgentConfigs().getExportEvalutormaxPoolSize();
        int eTimeout = (Utility.getAgentConfigs().getExportEvalutorkeepAliveTime());
        String eTimeoutUnitStr = Utility.getAgentConfigs().getExportEvalutorTimeAliveUnit();
        TimeUnit eTimeoutUnit = TimeUnit.valueOf(eTimeoutUnitStr);
        ThreadPoolExecutor.CallerRunsPolicy policy = new ThreadPoolExecutor.CallerRunsPolicy();
        fileEvaluator = new ThreadPoolExecutor(
                ePoolSize,
                eMaxPoolSize,
                eTimeout,
                eTimeoutUnit,
                evalutorQueue);
        fileIndexer = new ThreadPoolExecutor(
                Utility.getAgentConfigs().getExporterpoolSize(),
                Utility.getAgentConfigs().getExportermaxPoolSize(),
                (Utility.getAgentConfigs().getExporterkeepAliveTime()),
                TimeUnit.valueOf(Utility.getAgentConfigs().getExporterTimeAliveUnit()),
                indexerQueue);
        statisticsCalculator = new ThreadPoolExecutor(
                Utility.getAgentConfigs().getExportJobStatisticpoolSize(),
                Utility.getAgentConfigs().getExportJobStatisticmaxPoolSize(),
                (Utility.getAgentConfigs().getExportJobStatistickeepAliveTime()),
                TimeUnit.valueOf(Utility.getAgentConfigs().getExportJobStatisticTimeAliveUnit()),
                jobStatisticQueue);

        fileEvaluator.setRejectedExecutionHandler(policy);
        statisticsCalculator.setRejectedExecutionHandler(policy);
        fileIndexer.setRejectedExecutionHandler(policy);
                
        fileEvaluatorList.put(execID, fileEvaluator);
        fileIndexerList.put(execID, fileIndexer);
        statisticsCalculatorList.put(execID, statisticsCalculator);
    }
    
    //Following method will Stop the Pool Set of only current instance's Execution ID
    public void stopCurThreadPools() {
        try {
        	logger.info("IndexEvaluator Shutting Down ... ");
            fileEvaluatorList.get(this.curExecID).shutdown();
            long timeout = 20;
            int counter = 0;
            while (!fileEvaluatorList.get(this.curExecID).awaitTermination(timeout, TimeUnit.SECONDS)) {
            	logger.info(" IndexEvaluator is waiting to sutting down for : " + timeout * counter + " secs");
            	counter++;
            }

            logger.info("FileIndexer Shutting Down ... ");
            fileIndexerList.get(this.curExecID).shutdown();
            counter = 0;
            while (!fileIndexerList.get(this.curExecID).awaitTermination(timeout, TimeUnit.SECONDS)) {
            	logger.info(" FileIndexer ThreadPool is waiting to sutting down for : " + timeout * counter + " secs");
            	counter++;
            }

            logger.info("TaskSummary Shutting Down ... ");
            statisticsCalculatorList.get(this.curExecID).shutdown();
            counter = 0;
            while (!statisticsCalculatorList.get(this.curExecID).awaitTermination(timeout, TimeUnit.SECONDS)) {
            	logger.info(" TaskSummary ThreadPool is waiting to sutting down for : " + timeout * counter + " secs");
            	counter++;
            }
            
        } catch (Exception ie) {
            LoggingManager.printStackTrace(ie, logger);
            
        } finally {
            fileEvaluatorList.put(this.curExecID, null);
            fileIndexerList.put(this.curExecID, null);
            statisticsCalculatorList.put(this.curExecID, null);
            
            fileEvaluatorList.remove(this.curExecID);
            fileIndexerList.remove(this.curExecID);
            statisticsCalculatorList.remove(this.curExecID);
        }
    }
        
    public static void startEvaluator(String execID, FileTaskIndexingInterface task) {
        
        if(!IndexStarter.isInterupted(execID)) {
            fileEvaluatorList.get(execID).execute(new IndexEvaluater(task));
            
        } else {//if interupted, than go to keep Stats for current TASK
            startStatCalculator(execID, task);
        }
    }
    
    public static void startIndexer(String execID, FileTaskIndexingInterface task) {
        
        if(!IndexStarter.isInterupted(execID)) {
            fileIndexerList.get(execID).execute(new FileIndexer(task));
            
        } else {//if interupted, than go to keep Stats for current TASK
            startStatCalculator(execID, task);
        }
    }
    
    public static void startStatCalculator(String execID, FileTaskIndexingInterface task) {
        statisticsCalculatorList.get(execID).execute(new TaskStatisticsIndexed(task));
    }
}
