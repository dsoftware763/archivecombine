package com.virtualcode.agent.das.archivePolicy.dto;



import java.sql.Timestamp;

/**
 * Created by siva on 12/11/2016.
 */
public class JobStatus implements java.io.Serializable {

    //  static fields
    private static final long serialVersionUID = 101L;

    private Integer id;
    private Integer jobId;   
    private String executionId;
    private String description;
    private int current=0;
    private Integer previousJobStatusId;
    private String status;
    private String actionByUser;


    public JobStatus() {
    }

    public JobStatus(Integer id, Integer jobId,  String executionId, String description, Timestamp execStartTime, int current, Integer previousJobStatusId, String status, String actionByUser, Timestamp actionTime) {
        this.id = id;
        this.jobId = jobId;       
        this.executionId = executionId;
        this.description = description;
        this.current = current;
        this.previousJobStatusId = previousJobStatusId;
        this.status = status;
        this.actionByUser = actionByUser;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

  

    public String getExecutionId() {
        return executionId;
    }

    public void setExecutionId(String executionId) {
        this.executionId = executionId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public Integer getPreviousJobStatusId() {
        return previousJobStatusId;
    }

    public void setPreviousJobStatusId(Integer previousJobStatusId) {
        this.previousJobStatusId = previousJobStatusId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getActionByUser() {
        return actionByUser;
    }

    public void setActionByUser(String actionByUser) {
        this.actionByUser = actionByUser;
    }


    @Override
    public String toString() {
        return "JobStatus{" +
                "id=" + id +
                ", jobId=" + jobId +
                ", executionId='" + executionId + '\'' +
                ", description='" + description + '\'' +
                ", current=" + current +
                ", previousJobStatusId=" + previousJobStatusId +
                ", status='" + status + '\'' +
                ", actionByUser='" + actionByUser + '\'' +
                '}';
    }
}
