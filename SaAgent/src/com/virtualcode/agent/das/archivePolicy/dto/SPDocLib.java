/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.archivePolicy.dto;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author faisal nawaz
 */
public class SPDocLib implements java.io.Serializable {

    /*
     * *************************************************************************
     * fields
     * *************************************************************************
     */
    //  static fields
    private static final long serialVersionUID = 104L;
    private int id;
    private String libraryName;
    private String libraryGUID;
    private String libraryType;
    private String destinationPath;
    private boolean recursive;
    private Set<ExcludedPath> excludedPathSet = new HashSet<ExcludedPath>(0);
    private String sitePath;
    private String drivePath;
    private boolean deduplicatedVolume=false;

    /*
     * *************************************************************************
     * constructors
     * *************************************************************************
     */
    public SPDocLib() {
    }

    public boolean isDeduplicatedVolume() {
        return deduplicatedVolume;
    }

    public void setDeduplicatedVolume(boolean deduplicatedVolume) {
        this.deduplicatedVolume = deduplicatedVolume;
    }

    /*
     * *************************************************************************
     * getter
     * *************************************************************************
     */
    public int getId() {
        return id;
    }

    public String getLibraryGUID() {
        return libraryGUID;
    }

    public String getLibraryName() {
        return libraryName;
    }

    public String getLibraryType() {
        return libraryType;
    }

    public String getDestinationPath() {
        return destinationPath;
    }

    public boolean isRecursive() {
        return recursive;
    }

    public Set<ExcludedPath> getExcludedPathSet() {
        return excludedPathSet;
    }

    /*
     * *************************************************************************
     * setter
     * *************************************************************************
     */
    public void setId(int id) {
        this.id = id;
    }

    public void setLibraryGUID(String libraryGUID) {
        this.libraryGUID = libraryGUID;
    }

    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }

    public void setLibraryType(String libraryType) {
        this.libraryType = libraryType;
    }

    public void setDestinationPath(String destinationPath) {
        this.destinationPath = destinationPath;
    }

    public void setRecursive(boolean recursive) {
        this.recursive = recursive;
    }

    public void setExcludedPathSet(Set<ExcludedPath> excludedPathSet) {
        this.excludedPathSet = excludedPathSet;
    }

	public String getSitePath() {
		return sitePath;
	}

	public void setSitePath(String sitePath) {
		this.sitePath = sitePath;
	}

	public String getDrivePath() {
		return drivePath;
	}

	public void setDrivePath(String drivePath) {
		this.drivePath = drivePath;
	}
}
