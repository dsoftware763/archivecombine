/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.archivePolicy.dto;

/**
 *
 * @author YAwar
 */
public class ActiveJob {
    
    private String jobName  =   null;
    private String absolutePath =   null;
    private String sharePath    =   null;
    private String actionType   =   null;

    public ActiveJob(String jobName, String absolutePath, String sharePath, String actionType) {
        this.jobName        =   jobName;
        this.absolutePath   =   absolutePath;
        this.sharePath      =   sharePath;
        this.actionType     =   actionType;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getSharePath() {
        return sharePath;
    }

    public void setSharePath(String sharePath) {
        this.sharePath = sharePath;
    }

    public String toString() {
        return "ActiveJob{" + "jobName=" + jobName + ", absolutePath=" + absolutePath + ", sharePath=" + sharePath + ", actionType=" + actionType + '}';
    }
}
