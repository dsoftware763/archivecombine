/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.archivePolicy.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author faisal nawaz
 */
public class Job implements java.io.Serializable {

    //  static fields
    private static final long serialVersionUID = 101L;

    //Fields
    private int id;
    private int active = 1;
    private String jobName;
    private String description;
    private Date execTimeStart;
    private String actionType;
    private String siteId;
    private String sitePath;
    private boolean processCurrentPublishedVersion = true;
    private boolean processLegacyPublishedVersion = true;
    private boolean processLegacyDraftVersion = true;
    private boolean processReadOnly = true;
    private boolean processArchive = true;
    private boolean processHidden = false;
    private boolean readyToExecute = true;
    private Policy policy;
    private Set<SPDocLib> spDocLibSet = new HashSet<SPDocLib>(0);
    private JobStatistics jobStatistics = new JobStatistics();
    private String activeActionType="ARCHIVEONLY";
    private String filestoreTag=null;
    private JobStatus jobStatus;

    //pause and resume
    private boolean scheduled;

    private String tags;

    private String currentStatus;
    private String companyTags;

    private String syncMode = "1";

    private boolean checkDeDuplication = false;

    private boolean processOfflineFiles = false;

    private boolean archiveFromAnalysis =  false;

    private Integer stagingJob = 0;

    private String stagingPath;

    private boolean createUrlFiles = false;
    /*
     * *************************************************************************
     * constructors
     * *************************************************************************
     */
    public Job() {
    }

    public Job(int id,
               int active, String jobName, String description, Date execTimeStart,
               String actionType, String siteId, String sitePath,
               boolean processCurrentPublishedVersion, boolean processLegacyPublishedVersion,
               boolean processLegacyDraftVersion, boolean processReadOnly,String companyTags,
               boolean processArchive, boolean processHidden, boolean readyToExecute,
               Policy policy,
               Set<SPDocLib> spDocLibSet,boolean scheduled,JobStatus jobStatus
    ) {

        this.id = id;
        this.active = active;
        this.jobName = jobName;
        this.description = description;
        this.execTimeStart = execTimeStart;
        this.actionType = actionType;
        this.siteId = siteId;
        this.sitePath = sitePath;
        this.processCurrentPublishedVersion = processCurrentPublishedVersion;
        this.processLegacyPublishedVersion = processLegacyPublishedVersion;
        this.processLegacyDraftVersion = processLegacyDraftVersion;
        this.processReadOnly = processReadOnly;
        this.processArchive = processArchive;
        this.processHidden = processHidden;
        this.readyToExecute = readyToExecute;
        this.policy = policy;
        this.spDocLibSet = spDocLibSet;
        this.companyTags=companyTags;
        // this.jobStatistics = jobStatistics;
        this.scheduled = scheduled;
        this.jobStatus = jobStatus;
    }

    /*
     * *************************************************************************
     * getters
     * *************************************************************************
     */
    public Set<SPDocLib> getSpDocLibSet() {
        return spDocLibSet;
    }

    public JobStatistics getJobStatistics() {
        return jobStatistics;
    }

    public Policy getPolicy() {
        return policy;
    }

    public String getActionType() {
        return actionType;
    }

    public int getActive() {
        return active;
    }

    public String getDescription() {
        return description;
    }

    public Date getExecTimeStart() {
        return execTimeStart;
    }

    public int getId() {
        return id;
    }

    public String getJobName() {
        return jobName;
    }

    public boolean isProcessArchive() {
        return processArchive;
    }

    public boolean isProcessCurrentPublishedVersion() {
        return processCurrentPublishedVersion;
    }

    public boolean isProcessHidden() {
        return processHidden;
    }

    public boolean isProcessLegacyDraftVersion() {
        return processLegacyDraftVersion;
    }

    public boolean isProcessLegacyPublishedVersion() {
        return processLegacyPublishedVersion;
    }

    public boolean isProcessReadOnly() {
        return processReadOnly;
    }

    public boolean isReadyToExecute() {
        return readyToExecute;
    }

    public String getSiteId() {
        return siteId;
    }

    public String getSitePath() {
        return sitePath;
    }

    /*
     * *************************************************************************
     * setters
     * *************************************************************************
     */
    public void setSpDocLibSet(Set<SPDocLib> spDocLibSet) {
        this.spDocLibSet = spDocLibSet;
    }

//    public void setJobStatistics(JobStatistics jobStatistics) {
//        this.jobStatistics = jobStatistics;
//    }

    public void setPolicy(Policy policy) {
        this.policy = policy;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setExecTimeStart(Date execTimeStart) {
        this.execTimeStart = execTimeStart;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public void setProcessArchive(boolean processArchive) {
        this.processArchive = processArchive;
    }

    public void setProcessCurrentPublishedVersion(boolean processCurrentPublishedVersion) {
        this.processCurrentPublishedVersion = processCurrentPublishedVersion;
    }

    public void setProcessHidden(boolean processHidden) {
        this.processHidden = processHidden;
    }

    public void setProcessLegacyDraftVersion(boolean processLegacyDraftVersion) {
        this.processLegacyDraftVersion = processLegacyDraftVersion;
    }

    public void setProcessLegacyPublishedVersion(boolean processLegacyPublishedVersion) {
        this.processLegacyPublishedVersion = processLegacyPublishedVersion;
    }

    public void setProcessReadOnly(boolean processReadOnly) {
        this.processReadOnly = processReadOnly;
    }

    public void setReadyToExecute(boolean readyToExecute) {
        this.readyToExecute = readyToExecute;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public void setSitePath(String sitePath) {
        this.sitePath = sitePath;
    }

    public String getActiveActionType() {
        return activeActionType;
    }

    public void setActiveActionType(String activeActionType) {
        this.activeActionType = activeActionType;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getCompanyTags() {
        return companyTags;
    }

    public void setCompanyTags(String companyTags) {
        this.companyTags = companyTags;
    }

    public String getSyncMode() {
        return syncMode;
    }

    public void setSyncMode(String syncMode) {
        this.syncMode = syncMode;
    }

    public boolean isCheckDeDuplication() {
        return checkDeDuplication;
    }

    public void setCheckDeDuplication(boolean checkDeDuplication) {
        this.checkDeDuplication = checkDeDuplication;
    }

    public String getFilestoreTag() {
        return filestoreTag;
    }

    public void setFilestoreTag(String filestoreTag) {
        this.filestoreTag = filestoreTag;
    }

    public boolean isProcessOfflineFiles() {
        return processOfflineFiles;
    }

    public void setProcessOfflineFiles(boolean processOfflineFiles) {
        this.processOfflineFiles = processOfflineFiles;
    }

    public boolean isArchiveFromAnalysis() {
        return archiveFromAnalysis;
    }

    public void setArchiveFromAnalysis(boolean archiveFromAnalysis) {
        this.archiveFromAnalysis = archiveFromAnalysis;
    }

    public JobStatus getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(JobStatus jobStatus) {
        this.jobStatus = jobStatus;
    }

    public boolean isScheduled() {
        return scheduled;
    }

    public void setScheduled(boolean scheduled) {
        this.scheduled = scheduled;
    }

    public Integer getStagingJob() {
        return stagingJob;
    }

    public void setStagingJob(Integer stagingJob) {
        this.stagingJob = stagingJob;
    }

    public String getStagingPath() {
        return stagingPath;
    }

    public void setStagingPath(String stagingPath) {
        this.stagingPath = stagingPath;
    }

    public boolean isCreateUrlFiles() {
        return createUrlFiles;
    }

    public void setCreateUrlFiles(boolean createUrlFiles) {
        this.createUrlFiles = createUrlFiles;
    }

    @Override
    public String toString() {
        return "Job{" +
                "id=" + id +
                ", active=" + active +
                ", jobName='" + jobName + '\'' +
                ", description='" + description + '\'' +
                ", execTimeStart=" + execTimeStart +
                ", actionType='" + actionType + '\'' +
                ", readyToExecute=" + readyToExecute +
                ", jobStatus=" + jobStatus +
                ", scheduled=" + scheduled +
                ", createUrlFiles=" + createUrlFiles +
                ", checkDeDuplication=" + checkDeDuplication +
                ", processOfflineFiles=" + processOfflineFiles +
                ", archiveFromAnalysis=" + archiveFromAnalysis +
                ", stagingJob=" + stagingJob +
                ", stagingPath='" + stagingPath + '\'' +
                '}';
    }
}