package com.virtualcode.agent.das.utils;

import com.google.common.io.Files;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.logging.LoggingManager;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.Map;

/**
 * Created by sivakumar on 29/12/2016.
 */
public class FileUtil {




    public void writeIntoPropertiesFile(Map<String, String> map, Job job, String executionId, String fileType, int fileCount,boolean interruptFlag,boolean currentFlag,int processedCount) {

        synchronized (this.getClass()) {
            Logger pauseResumeLogger = LoggingManager.getLogger(LoggingManager.PAUSE_RESUME_DETAIL, executionId);
            pauseResumeLogger.info("************************************************ ");
            pauseResumeLogger.info("****FileUtil : writeIntoPropertiesFile**** " + fileType + " map size " + map.size() + " file count " + fileCount +" interruptFlag "+interruptFlag+" currentFlag "+currentFlag +" processedCount "+processedCount);
            pauseResumeLogger.info("************************************************ ");

            //load custom properties class
            MyProperties prop = new MyProperties();
            OutputStream output = null;
            try {
                String basePath = Utility.getAgentConfigs().getLogBasePath();
                if (basePath == null || basePath.isEmpty())
                    basePath = "ErrorLog";

                String fileName = fileType + fileCount + ".properties";
                String filePath = basePath + File.separator + job.getId() + File.separator + executionId ;
                String finalFilePath = filePath+ File.separator + fileName;
                output = new FileOutputStream(finalFilePath);

                for (Map.Entry<String, String> entry : map.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    prop.setProperty(key, value);

                }
                prop.store(output, executionId);
                //close output stream other wise delete not work
                output.close();

                if(!interruptFlag){

                    //Do compress using gzip, file name should be .done
                    GZipFile gzipFile = new GZipFile(pauseResumeLogger, 1024);
                    InputStream inputStream = new FileInputStream(new File(finalFilePath));
                    gzipFile.gZipIt(inputStream, new File(finalFilePath + ".done.gz"));

                    //Delete evaluate properties file no longer required
                    boolean result = doDeleteFile(new File(finalFilePath), pauseResumeLogger);
                    pauseResumeLogger.info("FileUtil :doDeleteFile result " + result);

                }else if(currentFlag){

                   //rename evaluate properties file
                    renameFile(finalFilePath,filePath+File.separator+fileType + fileCount + ".current."+processedCount+".properties", pauseResumeLogger);

                }else if(!currentFlag){
                    //Do compress using gzip, file name not done
                    GZipFile gzipFile = new GZipFile(pauseResumeLogger, 1024);
                    InputStream inputStream = new FileInputStream(new File(finalFilePath));
                    gzipFile.gZipIt(inputStream, new File(finalFilePath + ".gz"));

                    //Delete evaluate properties file no longer required
                    boolean result = doDeleteFile(new File(finalFilePath), pauseResumeLogger);
                    pauseResumeLogger.info("FileUtil :doDeleteFile result " + result);

                }




            } catch (IOException io) {
                io.printStackTrace();
            } finally {
                if (output != null) {
                    try {
                        output.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public boolean doDeleteFile(File deleteFile, Logger pauseResumeLogger) {
        pauseResumeLogger.info("FileUtil : Deleting file started for " + deleteFile);

        try {
            if (deleteFile.isDirectory()) {
                //directory is empty, then delete it
                if (deleteFile.list().length == 0) {
                    deleteFile.delete();
                    return true;
                } else {
                    //list all the directory contents
                    String files[] = deleteFile.list();
                    for (String temp : files) {
                        //construct the file structure
                        File fileDelete = new File(deleteFile, temp);
                        //recursive delete
                        doDeleteFile(fileDelete, pauseResumeLogger);
                    }
                    //check the directory again, if empty then delete it
                    if (deleteFile.list().length == 0) {
                        deleteFile.delete();
                        pauseResumeLogger.info("FileUtil : Deleting file completed  for " + deleteFile.getPath());
                    }
                }

            } else {
                //if file, then delete it
                pauseResumeLogger.info("FileUtil : Deleting file completed for " + deleteFile.getPath());
                deleteFile.delete();
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    public boolean renameFile(String oldFileName, String newFileName, Logger pauseResumeLogger) {

        pauseResumeLogger.info("FileUtil :renameFile  oldFileName" + oldFileName + " newFileName " + newFileName);

        File oldFile = new File(oldFileName);
        File newFile = new File(newFileName);

        if (oldFile.renameTo(newFile)) {
            pauseResumeLogger.info("File name changed successful");
            return true;
        } else {
            pauseResumeLogger.info("Rename failed");
            return false;
        }
    }

    public void compareFiles(String filePath, Logger pauseResumeLogger) {

        pauseResumeLogger.info("FileUtil : compareFiles " + filePath);
        File file = new File(filePath);
        //Get Archive Files
        for (final File fileEntry : file.listFiles()) {
            if (fileEntry.isDirectory()) {
                continue;
            } else {
                String fileName = fileEntry.getName();
                if (fileName.contains("archive")) {

                    File archiveFile = new File(filePath+File.separator+fileName);
                    //compare with evaluate files
                    for (final File evaluateEntry : file.listFiles()) {

                        if (!evaluateEntry.isDirectory()) {
                            String subFileName = evaluateEntry.getName();
                            if (subFileName.contains("evaluate")) {
                                File evaluateFile = new File(filePath+File.separator+subFileName);
                                try {
                                    boolean result = Files.equal(archiveFile, evaluateFile);

                                    if (result) {
                                        //delete archive file and make evaluate file is .done
                                        renameFile(filePath+File.separator+subFileName, filePath+File.separator+subFileName + ".done", pauseResumeLogger);
                                        doDeleteFile(archiveFile, pauseResumeLogger);
                                        break;
                                    }

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                            }

                        }

                    }


                }

            }


        }


    }


    public void compressEvaluateFiles(String filePath, Logger pauseResumeLogger){

        pauseResumeLogger.info("FileUtil : compressEvaluateFiles " + filePath);

        try{
            File file = new File(filePath);
            //Get Evaluate Files
            for (final File fileEntry : file.listFiles()) {
                if (fileEntry.isDirectory()) {
                    continue;
                } else {
                    String fileName = fileEntry.getName();
                    if (fileName.contains("evaluate") && !fileName.contains("gz")) {
                        //Do archive using gzip
                        GZipFile gzipFile = new GZipFile(pauseResumeLogger, 1024);
                        InputStream inputStream = new FileInputStream(new File(filePath+File.separator+fileName));
                        gzipFile.gZipIt(inputStream, new File(filePath+File.separator+fileName + ".gz"));

                        //Delete properties file no longer required
                        boolean result = doDeleteFile(new File(filePath+File.separator+fileName), pauseResumeLogger);
                        pauseResumeLogger.info("FileUtil :doDeleteFile result " + result);
                    }
                }
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }


}
