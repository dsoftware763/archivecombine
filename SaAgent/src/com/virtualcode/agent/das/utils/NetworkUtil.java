/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.utils;

import com.virtualcode.agent.das.logging.LoggingManager;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.TreeSet;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.spi.NamingManager;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SID;
import org.apache.log4j.Logger;

/**
 *
 * @author HP
 */
public class NetworkUtil {
    
    public static void main(String args[]) throws Exception {
        String path    =   "smb://Test182;administrator:cryo@Test182/1/Stub/i.txt";
        /*String[] cred =   Utility.getCredentialsFromSmb(path);
        for(int i=0; i<cred.length; i++){
            System.out.println(i + "_" + cred[i]+"_");
        }*/
        
        String[] credentials    =   Utility.getCredentialsFromSmb(path);
                    String domain   =   credentials[0];
                    String userName =   credentials[1];
                    String pwd      =   credentials[2];
                    
        String authServer   =   getLdapAuthorityServer(domain, Logger.getLogger(NetworkUtil.class));
        System.out.println("authServer: "+authServer);
        
        if(authServer==null) {//if Authority server not found on any Network interface
            System.out.println(" LDAP Authority server not found over the Network, so resolving SID on local machine");
            authServer  =   InetAddress.getLocalHost().getHostName();//Resolve by local machine..
        }

        SID sid =   new SID("S-1-5-32-544");
        NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(domain, userName, pwd);
        sid.resolve(authServer, auth);
        System.out.println(" Resolved: "+sid.toString()+", "+sid.getAccountName()+" at "+authServer);
        
    }
    
    public static String getLdapAuthorityServer(String domainName, Logger logger) {
        
        String ldapAuthHost =   null;
        try {
            Enumeration e = NetworkInterface.getNetworkInterfaces();
            while(e.hasMoreElements()) {//iterate for all Network Interfaces

                NetworkInterface n = (NetworkInterface) e.nextElement();
                Enumeration ee = n.getInetAddresses();
                
                while (ee.hasMoreElements()) { //iterate for all IPs of this interface
                    
                    InetAddress i = (InetAddress) ee.nextElement();
                    if(i.isSiteLocalAddress()) {//we are ONLY interested in LAN IPs
                        logger.info("Retrieving Host from: " + i.getHostAddress()+" | "+i.getHostName() );
                        ldapAuthHost    =   getLdapHostByInetAddress(i, domainName, logger);

                        if(ldapAuthHost!=null && !ldapAuthHost.isEmpty()) {//Terminate further iterations (if LDAP Host found on this IP.
                            logger.info("LDAP Auth Host found as: "+ldapAuthHost);
                            break;
                        }
                    }
                }

                if(ldapAuthHost!=null && !ldapAuthHost.isEmpty()) {//Terminate (if Host found on this interface.
                    break;
                }
            }
            
            if(ldapAuthHost!=null && ldapAuthHost.contains(":")) {//Trim the PORT number
                ldapAuthHost    =   ldapAuthHost.substring(0, ldapAuthHost.indexOf(":"));
            }
        } catch (Exception ex) {
            logger.warn("Error in getting LDAP authority server..");
            LoggingManager.printStackTrace(ex, logger);
        }
        
        return ldapAuthHost;
    }
    
    private static String getLdapHostByInetAddress(InetAddress address, String domain, Logger logger) {
        try {
            Hashtable<String, String> env = new Hashtable();
            env.put( "java.naming.factory.initial", "com.sun.jndi.dns.DnsContextFactory" );
            DirContext dns = new InitialDirContext( env );

            //InetAddress address = InetAddress.getLocalHost();
            //String domain = address.getCanonicalHostName();

            if( domain.equals( address.getHostAddress() ) ) {
                //domain is a ip address
                domain = getDnsPtr( dns );
            }

            int idx = domain.indexOf( '.' );
            if( idx < 0 ) {
                //computer is not in a domain? We will look in the DNS self.
                domain = getDnsPtr( dns );
                idx = domain.indexOf( '.' );
                if( idx < 0 ) {
                    //computer is not in a domain
                    return null;
                }
            }
            domain = domain.substring( idx + 1 );

            Attributes attrs = dns.getAttributes( "_ldap._tcp." + domain, new String[] { "SRV" } );

            Attribute attr = attrs.getAll().nextElement();
            String srv = attr.get().toString();

            String[] parts = srv.split( " " );
            return parts[3] + ":" + parts[2];
        } catch( Exception ex ) {
            logger.warn("Error in getting SRV details...");
            LoggingManager.printStackTrace(ex, logger);
            return null;
        }
    }

    /**
     * Look for a reverse PTR record on any available ip address
     * @param dns DNS context
     * @return the PTR value
     * @throws Exception if the PTR entry was not found
     */
    private static String getDnsPtr( DirContext dns ) throws Exception {
        Exception exception = null;
        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
        while(interfaces.hasMoreElements()) {
            NetworkInterface nif = interfaces.nextElement();
            if( nif.isLoopback() ) {
                continue;
            }
            Enumeration<InetAddress> adresses = nif.getInetAddresses();
            while(adresses.hasMoreElements()) {
                InetAddress address = adresses.nextElement();
                if( address.isLoopbackAddress() || address instanceof Inet6Address) {
                    continue;
                }
                String domain = address.getCanonicalHostName();
                if( !domain.equals( address.getHostAddress() ) && (domain.indexOf( '.' ) > 0) ) {
                    return domain;
                }

                String ip = address.getHostAddress();
                String[] digits = ip.split( "\\." );
                StringBuilder builder = new StringBuilder();
                builder.append( digits[3] ).append( '.' );
                builder.append( digits[2] ).append( '.' );
                builder.append( digits[1] ).append( '.' );
                builder.append( digits[0] ).append( ".in-addr.arpa." );
                try {
                    Attributes attrs = dns.getAttributes( builder.toString(), new String[] { "PTR" } );
                    return attrs.get( "PTR" ).get().toString();
                } catch( Exception ex ) {
                    exception = ex;
                }
            }
        }
        if( exception != null ) {
            throw exception;
        }
        throw new IllegalStateException("No network");
    }
    
    private static Collection<String> getSRVRecords(String ldapDomain)
            throws NamingException {
        DirContext context = (DirContext) NamingManager.getURLContext("dns",
                new Hashtable<String, Object>());
        String ldapDNSUrl = "dns:///_ldap._tcp." + ldapDomain;
        String[] attrIds = { "SRV" };
        Attributes attributes = context.getAttributes(ldapDNSUrl, attrIds);
        Attribute servers = attributes.get("SRV");
        int L = servers.size();
        Collection<String> serverRecords = new TreeSet<String>();
        for (int i = 0; i < L; i++) {
            String s = (String) servers.get(i);
            //if (PATTERN.matcher(s).find()) {
                serverRecords.add(s);
                System.out.println("LDAP: "+s);
            //}
        }
        return serverRecords;
    }
    
}
