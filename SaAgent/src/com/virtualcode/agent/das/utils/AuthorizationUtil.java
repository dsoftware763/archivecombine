/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.utils;

import com.vcsl.sa.Ace;
import com.vcsl.sa.Acl;
import com.virtualcode.agent.das.logging.LoggingManager;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SID;
import jcifs.smb.SmbFile;
import org.apache.log4j.Logger;

/**
 *
 * @author se7
 */
public class AuthorizationUtil {
    public static String newLineSeparator=System.getProperty("line.separator");
    private static HashMap<String, String> mapedSIDs    =   new HashMap<String, String>();

    public static synchronized String resolveSIDValue(Logger logger, String smbPathStr, SmbFile smbFile, UUID taskID) {
        String owner    =   null;
        String ownerSID =   null;
        try {
            SID sid =   smbFile.getOwnerUser();
            ownerSID    =   sid.toString();
            owner   =   mapedSIDs.get(ownerSID);//Try to get for already populated List...
                        
            if(owner==null || owner.isEmpty()) {
                
                // Dont perform any execution, if the SID is matched with configured Agent value...
               // String saFileStubbingSID   =   Utility.getPropFromFile("saFileStubbingSID");
                String saFileStubbingSID   =   null;
                if(ownerSID!=null && !ownerSID.isEmpty() && ownerSID.equalsIgnoreCase(saFileStubbingSID)) {
                    
                    logger.info(taskID + " Matched with configured value of OwnerSID: "+saFileStubbingSID);
                    owner   =   "SaFileStubbing";
                    mapedSIDs.put(saFileStubbingSID, owner);//also set it in Mapped list, for future reference...
                
                } else if(sid.getAccountName()==null || sid.getAccountName().isEmpty()) {
                    
                    /***  DONT perform SID resolution in Agent jobs *****
                    logger.debug(taskID + " SID: "+sid.toDisplayString());//+" SID: "+sid.toString());
                    
                    String[] credentials    =   Utility.getCredentialsFromSmb(smbPathStr);
                    String domain   =   credentials[0];
                    String userName =   credentials[1];
                    String pwd      =   credentials[2];
                    
                    String authServer   =   NetworkUtil.getLdapAuthorityServer(domain, logger);
                    if(authServer==null) {//if Authority server not found on any Network interface
                        logger.debug(taskID + " LDAP Authority server not found over the Network, so resolving SID on local machine");
                        authServer  =   InetAddress.getLocalHost().getHostName();//Resolve by local machine..
                    }

                    NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(domain, userName, pwd);
                    sid.resolve(authServer, auth);
                    logger.debug(taskID + " Resolved: "+sid.toString()+", "+sid.getAccountName()+" at "+authServer);
                    owner     =   sid.getAccountName();//.toDisplayString();
                    mapedSIDs.put(sid.toString(), owner);
                     * ********/
                    
                    owner   =   sid.toString();//pass the SID value as it is to server...
                    logger.debug(taskID + " not resolving SID "+owner);

                } else {
                    owner     =   sid.getAccountName();//.toDisplayString();
                    logger.debug(taskID + " Already Resolved: " +sid.toString()+", "+owner);
                }                
                
            } else {
                logger.debug(taskID + " Resolved previously: " +sid.toString()+", "+owner );
            }
        } catch(Exception e) {
            LoggingManager.printStackTrace(e, logger);
        }
        
        //if the resolved value is SaFileStubbing (case-insensitive)
        /*if("SaFileStubbing".equalsIgnoreCase(owner) && ownerSID!=null) {
            Utility.updateInPropFile(null, null, null, ownerSID);//permanently save the SID of owner in some prop file
        }*/
        
        return owner;
    }
    
    public static Acl transformACE(jcifs.smb.ACE[] acl, String mapingEntry) {

        //System.out.println("Starting Transforming JCIFS ACE to VC ACE");
        Acl vcACL =   new Acl();
        
        //Segrigate and set values in vcACL, to be used at Server side
        segrigateAllowDeny(vcACL.getAllowAcl(), vcACL.getDenyAcl(), acl);
        
        //Segrigate and set values in vcACL, to be used at Server side
        segrigateAllowDenyFromString(vcACL.getShareAllowAcl(), vcACL.getShareDenyAcl(), mapingEntry);
        
        //System.out.println("Transformation from JCIFS ACE to VC ACE Finished");
        return vcACL;
    }
    
    public static Acl transformACE(jcifs.smb.ACE[] acl, jcifs.smb.ACE[] shareAcl) {

        //System.out.println("Starting Transforming JCIFS ACE to VC ACE");
        Acl vcACL =   new Acl();
        
        //Segrigate and set values in vcACL, to be used at Server side
        segrigateAllowDeny(vcACL.getAllowAcl(), vcACL.getDenyAcl(), acl);
        
        //Segrigate and set values in vcACL, to be used at Server side
        segrigateAllowDeny(vcACL.getShareAllowAcl(), vcACL.getShareDenyAcl(), shareAcl);
        
        //System.out.println("Transformation from JCIFS ACE to VC ACE Finished");
        return vcACL;
    }
    
    private static void segrigateAllowDeny(List<Ace> allowAcl, List<Ace> denyAcl, jcifs.smb.ACE[] acl) {
        
        for (int i = 0; acl!=null && i<acl.length; i++) {
            jcifs.smb.ACE ace = acl[i];
            Ace vcACE=new Ace();
            vcACE.setSid(ace.getSID().toString());
            vcACE.setSidName(ace.getSID().toDisplayString());
            vcACE.setIsAllow(ace.isAllow());
            vcACE.setIsInherited(ace.isInherited());
            vcACE.setAccessMask(ace.getAccessMask());

            /*
             * Its a technique to directly add/insert values into ArrayList of Server side... 
             * Bcoz we can't call any Setter method(e.g. setArrayListObj ) of server side class's obj...
             * objOfServerSideClassDTO.getArrayListObj().add(anyObjectToAddInList);
             */
            if(vcACE.isIsAllow() && ((jcifs.smb.ACE.FILE_READ_DATA&vcACE.getAccessMask())==jcifs.smb.ACE.FILE_READ_DATA)){
                allowAcl.add(vcACE);
            } else {
                denyAcl.add(vcACE);
            }
            //System.out.println(vcACE);
        }
    }
    
    private static void segrigateAllowDenyFromString(List<Ace> allowAcl, List<Ace> denyAcl, String mapingEntry) {
        
        if(mapingEntry!=null) {//if mapping Entry found in mappingFile.properties, for current directory/file
            StringTokenizer acl =   new StringTokenizer(mapingEntry, "|", false);
            if(acl.hasMoreTokens()) {//if first Token exists
                String allowedToken =   acl.nextToken();
                setValuesInList(allowAcl, denyAcl, allowedToken, true);
            }
            
            if(acl.hasMoreTokens()) {//if 2nd token also exists
                String deniedToken  =   acl.nextToken();
                setValuesInList(allowAcl, denyAcl, deniedToken, false);
            }
        }
    }
    
    private static void setValuesInList(List<Ace> allowAcl, List<Ace> denyAcl, String aclToken, boolean isAllow) {
        StringTokenizer acl  =   new StringTokenizer(aclToken, ",", false);
        while(acl.hasMoreTokens()) {
            String ace = acl.nextToken();
            Ace vcACE=new Ace();
            vcACE.setSid(ace);
            vcACE.setSidName(ace);
            vcACE.setIsAllow(isAllow);
            vcACE.setIsInherited(true);
            vcACE.setAccessMask(0);

            /*
             * Its a technique to directly add/insert values into ArrayList of Server side... 
             * Bcoz we can't call any Setter method(e.g. setArrayListObj ) of server side class's obj...
             * objOfServerSideClassDTO.getArrayListObj().add(anyObjectToAddInList);
             */
            if(vcACE.isIsAllow()){
                allowAcl.add(vcACE);
            } else {
                denyAcl.add(vcACE);
            }
            //System.out.println(vcACE);
        }
    }
    /*
    public static HashMap segrigateAllowDeny(List completeACL) {

//        System.out.println("Start Seprating Read ALLOW/DENY ACE's, Following ACE's detail");
        HashMap returnMap=new HashMap();
        
        ArrayList<Ace> readAllowAcls = new ArrayList<Ace>();
        ArrayList<Ace> readDenyAcls = new ArrayList<Ace>();
        
        if (completeACL != null && completeACL.size() > 0) {
            
            for (int i = 0; i < completeACL.size(); i++) {
                Ace ace=(Ace)completeACL.get(i);
                if(ace!=null && ace.isIsAllow() && ((jcifs.smb.ACE.FILE_READ_DATA&ace.getAccessMask())==jcifs.smb.ACE.FILE_READ_DATA)){
                    readAllowAcls.add(ace);
//                    System.out.println("\t\t"+(i+1)+"-ALLOW -->"+ace.getSidName());
                }else  if(ace!=null && !ace.isIsAllow()){
                    readDenyAcls.add(ace);
//                    System.out.println("\t\t"+(i+1)+"-DENY -->"+ace.getSidName());
                }
            }
        }
        
        returnMap.put(""+true, readAllowAcls);
        returnMap.put(""+false, readDenyAcls);
        
//        System.out.println("ACE's Segrigation Finished");
        return returnMap;
    }
    
    
    
    public static String printList(HashMap segratedList,String tabValue) throws IOException{
        
        String output="";
        
        ArrayList<Ace> readAllowAcls = (ArrayList<Ace>)segratedList.get(""+true);
         output+=tabValue+"ALLOW"+newLineSeparator;
         if(readAllowAcls!=null && readAllowAcls.size()>0){
             for(int i=0;i<readAllowAcls.size();i++){
                 output+=tabValue+"\t"+(i+1)+"-"+readAllowAcls.get(i).getSidName()+newLineSeparator;
              }
         }
         
         ArrayList<Ace> readDenyAcls = (ArrayList<Ace>)segratedList.get(""+false);
         
         output+=tabValue+"DENY"+newLineSeparator;
         
         if(readDenyAcls!=null && readDenyAcls.size()>0){
             for(int i=0;i<readDenyAcls.size();i++){
                 output+=tabValue+"\t"+(i+1)+"-"+readDenyAcls.get(i).getSidName()+newLineSeparator;
            }
         }
         return output;
    }
    */
}
