package com.virtualcode.agent.das.utils;


import com.google.common.io.ByteStreams;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class GZipFile {

    private Logger logger = null;
    private int buffSize = 1024;

    public GZipFile(Logger logger, int buffSize) {
        this.logger = logger;
        this.buffSize = buffSize;
    }

    /**
     * GunZip it
     */
    public String gUnzipIt(String inputFilePath) throws IOException {

        byte[] buffer = new byte[buffSize];
        String outputFile = inputFilePath.substring(0, inputFilePath.lastIndexOf("."));//trim the last .gz

        GZIPInputStream gzis = new GZIPInputStream(new FileInputStream(
                inputFilePath));

        FileOutputStream out = new FileOutputStream(outputFile);

        int len;
        while ((len = gzis.read(buffer)) > 0) {
            out.write(buffer, 0, len);
        }

        gzis.close();
        out.close();

        logger.info("Decompression Done " + outputFile);

        return outputFile;
    }

    public String gZipIt(InputStream sourceFileStream, File dest) throws IOException {

        byte[] buffer = new byte[buffSize];

        GZIPOutputStream gzos = new GZIPOutputStream(new FileOutputStream(dest));

        //FileInputStream in = new FileInputStream(sourceFilePath);

        int len;
        while ((len = sourceFileStream.read(buffer)) > 0) {
            gzos.write(buffer, 0, len);
        }

        sourceFileStream.close();

        gzos.finish();
        gzos.close();

        logger.debug("Compression Done " + dest.getAbsolutePath());

        return dest.getAbsolutePath();
    }

    public byte[] gZipIt(InputStream sourceFileStream) throws IOException {


        long startTime = System.nanoTime();
        byte[] source = IOUtils.toByteArray(sourceFileStream);

        if (source == null || source.length == 0) {
            return source;
        }

        ByteArrayInputStream sourceStream = new ByteArrayInputStream(source);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(source.length );
        OutputStream compressor = null;


        try {
            compressor = new GZIPOutputStream(outputStream);
            ByteStreams.copy(sourceStream, compressor);

        } finally {
            compressor.close();
            outputStream.close();
            sourceStream.close();
        }

        long endTime = System.nanoTime();
        long diff	=	endTime - startTime;
        //System.out.println("gZipIt " + diff/1000000);

        return outputStream.toByteArray();
    }


}
