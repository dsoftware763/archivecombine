/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.utils;
// This is futureistic class
import java.util.Hashtable;

/**
 *
 * @author Saim
 */
public class LocaMD5Cache {
    static Hashtable cache = new Hashtable();
     
    public static String getUrlIfExists(String Md5){
        
        String url = null;
        if(cache.containsKey(Md5))
            url = (String)cache.get(Md5);
        return url;
    }
    
    public static void putUrl(String Md5, String URL){
        cache.put(Md5, URL);
    }
    
}
