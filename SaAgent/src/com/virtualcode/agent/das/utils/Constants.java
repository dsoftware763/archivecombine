/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.utils;

/**
 *
 * @author HP
 */
public class Constants {
    
    
    public static String jobTypeRetension   =   "RETENSION";
    public static String jobTypeMarkOnly   =   "MARKONLY";
    public static String jobTypeArchive     =   "ARCHIVE";
    public static String jobTypeStub        =   "STUB";
    public static String jobTypeWithoutStub =   "WITHOUTSTUB";
    public static String jobTypeRemovePerm  =   "REMOVENOW";
    public static String pathTypeRepo       =   "REPO";
    public static String pathTypeFS         =   "FS";
    public static String pathTypeSP         =   "SP";
    
}
