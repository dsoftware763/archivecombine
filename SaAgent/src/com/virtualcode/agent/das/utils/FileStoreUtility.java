/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;

/**
 *
 * @author HP
 */
public class FileStoreUtility {

    private static int BUFF_SIZE	=	8192;//defaultBufferSize
    
    public static String getHashIDForNIO(File file) {
        String output   =   null;
        try {
            
            BufferedInputStream istream = new BufferedInputStream(new FileInputStream(file), BUFF_SIZE);
            output   = prepareNodeID(istream, file.length());
            
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return output;
    }
    
    public static String getHashIDForCIFS(SmbFile file) {
        String output   =   null;
        try {
            
            BufferedInputStream istream = new BufferedInputStream(new SmbFileInputStream(file), BUFF_SIZE);
            output   = prepareNodeID(istream, file.length());
            
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return output;
    }
    private static String prepareNodeID(BufferedInputStream istream, long offset) throws NoSuchAlgorithmException, FileNotFoundException, IOException {

        String hashValue = null;
            
        //BufferedInputStream istream = new BufferedInputStream(new SmbFileInputStream(file), BUFF_SIZE);

        long startTime = System.nanoTime();
        MessageDigest hashSum = MessageDigest.getInstance("SHA-1");

        byte[] buffer = new byte[BUFF_SIZE];
        byte[] partialHash = null;

        long read = 0;

        // calculate the hash of the hole file for the test
        //long offset = file.length();
        int unitsize;
        while (read < offset) {
            unitsize = (int) (((offset - read) >= BUFF_SIZE) ? BUFF_SIZE : (offset - read));
            istream.read(buffer, 0, unitsize);

            hashSum.update(buffer, 0, unitsize);

            read += unitsize;
        }

        istream.close();
        partialHash = new byte[hashSum.getDigestLength()];
        partialHash = hashSum.digest();

        long endTime = System.nanoTime();

        long diff = endTime - startTime;
        //Date date = new Date(diff);
        //DateFormat formatter = new SimpleDateFormat("HH:mm:ss:SSS");
        //String dateFormatted = formatter.format(date);

        //System.out.println("hash time is " + diff / 1000000);

        BigInteger i = new BigInteger(1, partialHash);
        hashValue = String.format("%1$032X", i);

        return hashValue;
    }
}
