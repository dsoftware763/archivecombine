package com.virtualcode.agent.das.utils;

import java.util.HashMap;

/**
 * Created by siva on 12/12/2016.
 */
public class MyEvaluateHashMap {

    private HashMap<String, Object> hashMap;

    public MyEvaluateHashMap() {
        hashMap = new HashMap<String, Object>();
    }

    public void put(String key, Object value) {

        synchronized (this.getClass()) {

            hashMap.put(key, value);
           /* HashMap<String, String> map = (HashMap<String, String>) hashMap.get(key);
            if (map != null && map.size() == 10000) {
                System.out.println(" Inside MyEvaluateHashMap 100 If  : " + map.size() + "");
                hashMap.put(key, value);
                HashMap<String, String> newMap = new HashMap<String, String>();
                hashMap.put(key, newMap);
            } else {
                hashMap.put(key, value);
            }*/
        }
    }

    public Object get(String key) {
        return hashMap.get(key);
    }


}
