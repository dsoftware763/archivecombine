/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.utils;

import com.virtualcode.agent.das.logging.LoggingManager;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.FileSystems;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Properties;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javax.mail.internet.MimeUtility;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.virtualcode.ws.Agent;
import org.apache.commons.io.FilenameUtils;

import executors.DataLayer;
import java.io.File;
import java.util.UUID;
import java.util.ArrayList;
import java.util.Iterator;
import javax.activation.DataHandler;
import org.apache.log4j.Logger;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import lia.util.net.copy.FDT;


import javax.net.ssl.SSLContext;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
/**
 *
 * author Administrator
 */
public class Utility {

    public static String zeroGUID = "00000000-0000-0000-0000-000000000000";
    //private static DataExportService expService = null;
    //private static DataExport expPort = null;
    public static ResourceBundle mapBdl = null;
    public static ResourceBundle propFileBdl = null;
    private static Agent agentConfigs    =   null;
    private static String configFile    =   "saAgent.properties";

    /**
     * <p>Returns an upper case hexadecimal <code>String</code> for the given
     * character.</p>
     *
     * param ch The character to convert.
     * return An upper case hexadecimal <code>String</code>
     */
    private static String hex(char ch) {
        return Integer.toHexString(ch).toUpperCase(Locale.ENGLISH);
    }
    
    public static boolean isNumeric(String str) {
        return str.matches("\\d+(\\d+)?");  //match a number
    }
        
    public static Date toDate(XMLGregorianCalendar calendar){
        if(calendar == null) {
            return null;
        }
        return calendar.toGregorianCalendar().getTime();
    }
    
    public static String getIDBasedFolderPath(String id, boolean isTemp) {
        String lvl1	=	id.substring(0, 2);
        String lvl2 =	id.substring(2, 4);
        String lvl3	=	id.substring(4, 6);

        //create three level fodlers
        String folderPath	=	Utility.getAgentConfigs().getFileStorePath() + "content/" + ((isTemp)?"tmp/":"") + lvl1+"/"+lvl2+"/"+lvl3+"/";	//out folder path	

        return folderPath;
    }
    
    public static String[] getCredentialsFromSmb(String path) {
        String credentials[]    =   new String[4];
        
        //path  =   smb://vcsl95.local;mazhar:password@snapserver.virtualcode.co.uk/secgrpA/
        if(path!=null && path.contains("@") && path.trim().toLowerCase().startsWith("smb://")) {
            path    =   path.trim().substring(6);//trim the first smb://
            
            String tmp[]  =   path.split("@");
            //tmp[0]= vcsl95.local;administrator:cryo
            //tmp[1]= snapserver.virtualcode.co.uk/secgrpA/
            
            if(tmp[0]!=null && !tmp[0].isEmpty() && (tmp[0].contains(";") || tmp[0].contains(":"))) {
                
                String cred[]  =   tmp[0].split(";");
                //cred[0]=vcsl95.local
                //cred[1]=administrator:cryo
                
                int lastIndx    =   cred.length-1;
                if(!cred[lastIndx].isEmpty() && cred[lastIndx].contains(":")) {
                    String credTemp[]   =   cred[lastIndx].split(":");
                    
                    credentials[0]  =   (cred.length>1)?cred[0]:null;//DOMAIN can be empty (null)
                    credentials[1]  =   credTemp[0];
                    credentials[2]  =   credTemp[1];
                }
                
                if(tmp.length>1 && !tmp[1].isEmpty()) {
                    if(tmp[1].contains("/")) {
                        credentials[3]   =   tmp[1].substring(0, tmp[1].indexOf("/"));
                    } else {
                        credentials[3]  =   tmp[1];
                    }
                }
            }
        }
        
        return credentials;
    }
    
    public static String getDASUrl() {
        String dasURL   =   Utility.getPropFromFile("serverURL");
        dasURL  =   dasURL + ((dasURL!=null && dasURL.endsWith("/"))? "" : "/" ) + "archive";
        return dasURL;
    }
    
    public static String getDASHost() throws MalformedURLException {
        String dasURL   =   Utility.getPropFromFile("serverURL");
        URL aURL = new URL(dasURL);//"http://example.com:80/docs/books/tutorial/index.html?name=networking#DOWNLOADING");

        //System.out.println("protocol = " + aURL.getProtocol()); //http
        //System.out.println("authority = " + aURL.getAuthority()); //example.com:80
        //System.out.println("host = " + aURL.getHost()); //example.com
        //System.out.println("port = " + aURL.getPort()); //80
        //System.out.println("path = " + aURL.getPath()); ///docs/books/tutorial/index.html
        //System.out.println("query = " + aURL.getQuery()); // name=networking
        //System.out.println("filename = " + aURL.getFile()); // /docs/books/tutorial/index.html?name=networking
        //System.out.println("ref = " + aURL.getRef());  //  DOWNLOADING

        return aURL.getHost();
    }
    
     // is type of number 
     public static boolean isInteger(String s) 
     {
         boolean isInteger = true;
         for(int i = 0; i < s.length() && isInteger; i++) 
         {
             char c = s.charAt(i);
             isInteger = isInteger & ((c >= '0' && c <= '9'));
         }
         return isInteger;
     }
     
    public static boolean allowedForContentIndexing(String cFileName, Logger logger) {
    	
    	boolean output	=	false;
    	
        if (cFileName.isEmpty()
                || cFileName.startsWith("~")
                || cFileName.startsWith("$")
                || cFileName.startsWith(".")
                || cFileName.endsWith(".lnk")
                || cFileName.endsWith(".tmp")
                || cFileName.endsWith(".url")) {

            logger.info("Link File OR Temporary File OR Office temporary File : " + cFileName);
            output	=	false;
            
        } else {
        	//GlobMatch matcher   =   new GlobMatch();
            String[] exts = {".odt", ".ods", ".odp", ".pdf", ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".txt", ".log", ".rtf", ".csv", ".xml", ".htm", ".html", ".xhtm"};
                         
	        for (String s : exts) {
	            if (s == null || s.isEmpty()) {
	                continue;
	            }
		    	if (cFileName != null && cFileName.toLowerCase().endsWith(s)) {
		            logger.info("Matched : " + s);
		            output	=	true;
		            break;
		    	}
	        }
        }
        
        return output;
    }
    
    public static boolean moveFile(DataHandler src, String dest, UUID transID, Logger logger) throws Exception {
        
        boolean result  =   false;
        if(dest!=null && !dest.isEmpty() && !"NA".equals(dest) && src!=null) {//if its configured to MoveFiles
            
            dest    =   dest.replace("\\", "/");//replace All backslashes with FWD slash...
        
            //int rootPathLength  =   managedFolder.getAbsolutePath().length();
            //if(rootPathLength < src.length()) {
            //    dest    =   dest + "/" + src.substring(rootPathLength);
            //    dest    =   dest.replaceAll("//", "/");
            //}
            logger.info(transID + " Moving : Dest Path : " + dest);

            //Path srcPath  =   FileSystems.getDefault().getPath(src);
            Path destPath  =   FileSystems.getDefault().getPath(dest);

            File parentFolder   =   destPath.toFile().getParentFile();
            if(!parentFolder.exists()) {
                logger.info(transID + " Creating dir structure");
                parentFolder.mkdirs();            
            }
            
            File file = new File(dest);
            file.createNewFile();
            FileOutputStream fop = new FileOutputStream(file);    
            src.writeTo(fop);           
            
            result  =   true;
        } else {
            logger.info(transID + " Failed to move at loc: "+dest);
        }
        
        return result;
    }
        
    public static String getIndexesUrl(String coreID) {
        
        String dasURL   =   Utility.getAgentConfigs().getIndexesURL();

        /*
        String dasURL   =   Utility.getPropFromFile("serverURL");
        dasURL  =   dasURL + ((dasURL!=null && dasURL.endsWith("/"))? "" : "/" ) + "solr";
        
        //append hard-coded credentials for solr
        if(dasURL.startsWith("http://"))
            dasURL  =   "http://solr_user:s0lR123@" + dasURL.substring(7);
        else if(dasURL.startsWith("https://"))
            dasURL  =   "https://solr_user:s0lR123@" + dasURL.substring(8);
        */

        if(coreID!=null && !coreID.contains(",") && !Utility.isInteger(coreID)) {//if its the companyTag
            dasURL = dasURL + ((!dasURL.endsWith("/"))?"/":"") + coreID;
        } else {
            //default collection core (for analysis paths and for coma seprated coreID)
            //dasURL = dasURL + ((!dasURL.endsWith("/"))?"/":"") + "collection1";
            dasURL = dasURL + ((!dasURL.endsWith("/"))?"/":"") + Utility.getAgentConfigs().getCompany().getCompanyTag()+"SI";
        }
        return dasURL;
    }
    
    
    /*
     * *************************************************************************
     * Utility Function for executionCompleted
     * *************************************************************************
     */
    public static XMLGregorianCalendar convertDate(Date date) throws Exception {
        GregorianCalendar gc = (GregorianCalendar) GregorianCalendar.getInstance();
        gc.setTime(date);
        DatatypeFactory dataTypeFactory = null;
        dataTypeFactory = DatatypeFactory.newInstance();
        return dataTypeFactory.newXMLGregorianCalendar(gc);
    }

    public static BasicFileAttributes getAttributeObject(Path file) {
        try {
            // System.out.println("Create Attribute View:");

            BasicFileAttributeView view =
                    Files.getFileAttributeView(file, BasicFileAttributeView.class, LinkOption.NOFOLLOW_LINKS);
// bulk read of basic attributes
            //System.out.println("Trying to read Attributes:");

            return view.readAttributes();

        } catch (Exception ex) {
            System.out.println("Exception:" + ex.toString());
        }
        return null;
    }
    
    public static String encodedURL(String url, String encoding) throws UnsupportedEncodingException {
        if (url == null || url.length() < 2) {
            return url;
        }

        String[] tokens = url.substring(1).split("/");
        String[] correctedTokens = new String[tokens.length];
        int i = 0;

        for (String t : tokens) {
            correctedTokens[i++] = URLEncoder.encode(t, encoding);
        }

        StringBuilder stb = new StringBuilder();
        for (String t : correctedTokens) {
            stb.append("/").append(t);
        }
        return stb.toString();
    }

    public static String toProperCase(String name) {
        name = name.toLowerCase();
        name = name.substring(0, 1).toUpperCase() + name.substring(1, name.length());
        return name;
    }

    public static void WriteFile(String fileName, byte[] data) throws Exception {
        String strFilePath = fileName;
        FileOutputStream fos = new FileOutputStream(strFilePath);
        fos.write(data);
        fos.close();
    }

    public static String now() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yy/MM/dd HH:mm:ss");
        return sdf.format(cal.getTime());

    }

    public static Long calculateMilliSec(int y, int m) {
        if (y + m == 0) {
            return 0L;
        } else {
            Calendar cal = Calendar.getInstance();
            cal.add(cal.YEAR, -1 * y);
            cal.add(cal.MONTH, -1 * m);
            return cal.getTimeInMillis();
        }
    }

    public static int parseYear(String yearMonth) throws Exception {
        String[] params = yearMonth.split(",");
        if (params.length > 1) {
            return Integer.parseInt(params[0]);
        } else {
            throw new Exception("Parse Year Not Correct Format: " + yearMonth);
        }
    }

    public static int parseMonth(String yearMonth) throws Exception {
        String[] params = yearMonth.split(",");
        if (params.length > 1) {
            return Integer.parseInt(params[1]);
        } else {
            throw new Exception("Parse Month Not Correct Format: " + yearMonth);
        }
    }

    public static long calculateSize(String size) throws Exception {
        String[] params = size.split(",");
        if (params.length > 1) {
            return (params[1].toUpperCase().equals("MB")) ? 1024 * 1024 * Long.parseLong(params[0]) : Long.parseLong(params[0]) * 1024;
        } else {
            throw new Exception("Calculate Size Not Correct Format: " + size);
        }
    }

    public static Date CurrDate() throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date = new java.util.Date();
        String dateStr = dateFormat.format(date);
        date = null;
        return dateFormat.parse(dateStr);

    }

    public static String getPropFromFile(String prop) {

        if (propFileBdl == null) {
            
            InputStream is = null;
            //java.util.logging.LoggingManager.getLogger(Utility.class.getName()).log(Level.INFO, "Loading "+filePath+"...");
            try {
                File f  =   new File(configFile);
                if(f.exists()) {
                    is = new FileInputStream(configFile);
                    propFileBdl = new PropertyResourceBundle(is);
                } else {
                    String fileContent = 
                         "localArchivePath=NA\n" + 
                         "serverURL=http://sharearchiver:8080/\n" +
                         "encodedKey=Paste your Encrypted Key here...\n" + 
                         "saFileStubbingSID=";
                    is  = new ByteArrayInputStream(fileContent.getBytes("UTF-8"));
                    propFileBdl = new PropertyResourceBundle(is);
                }
            } catch (IOException ie) {
                propFileBdl = null;
                java.util.logging.Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ie);
            } finally {
                if (is != null) {
                    try {
                        is.close();
                        is = null;
                    } catch (IOException ie) {
                    }
                }
            }
        }
        String toRet = propFileBdl.getString(prop);
        if(toRet!=null)
            toRet   =   toRet.trim();

        return toRet;
    }

    public static void updateInPropFile(String serverURL, String encodedKey) {

        Properties prop = new Properties();
        OutputStream output = null;

        try {

            File f =    new File(configFile);
            if(!f.exists()) {
                System.out.println("Creating new file "+configFile);
                f.createNewFile();
            }

            output = new FileOutputStream(configFile);

            // load the properties value (if someone is Unknown)
           /* if(localArchivePath==null)
                localArchivePath    =   Utility.getPropFromFile("localArchivePath");*/
            if(serverURL==null)
                serverURL           =   Utility.getPropFromFile("serverURL");
            if(encodedKey==null)
                encodedKey          =   Utility.getPropFromFile("encodedKey");
           /* if(saFileStubbingSID==null)
                saFileStubbingSID   =   Utility.getPropFromFile("saFileStubbingSID");*/

            // set the properties value
            //  prop.setProperty("localArchivePath", localArchivePath);
            prop.setProperty("serverURL", serverURL);
            prop.setProperty("encodedKey", encodedKey);
            // prop.setProperty("saFileStubbingSID", saFileStubbingSID);

            // save properties to project root folder
            prop.store(output, null);
            propFileBdl =   null;//Reset the propBundle to NULL
            agentConfigs    =   null;//Reset the AgentConfigs to NULL

        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
    
    public static String getLocalArchivePath() {
        String localArchivePath =   Utility.getPropFromFile("localArchivePath");
        if(localArchivePath==null || localArchivePath.isEmpty()) {//if its not defined by user, or empty
            localArchivePath    =   "NA";
        }
        return localArchivePath;
    }

    public static Agent getAgentConfigs() {
        if(agentConfigs==null) {
            try {
                String encryptedID  =   Utility.getPropFromFile("encodedKey");
                agentConfigs    =   DataLayer.getConfigParams(encryptedID);
                
                if(agentConfigs==null) { //if Agent is NOT registered with this encodedKey
                    //means its Token1 (containing only CompanyTag)
                    ////String serverURL    =   Utility.getPropFromFile("serverURL");
                    ////String localArchivePath =   Utility.getPropFromFile("localArchivePath");
                    ////System.out.println("Registering New Agent at... "+serverURL);
                    String newEncodedKey =   DataLayer.requestForNewAgent(encryptedID);
                    ////String saFileStubbingSID    =   Utility.getPropFromFile("saFileStubbingSID");
                    
                    if(newEncodedKey!=null) {
                        Utility.updateInPropFile(null, newEncodedKey);//Old/existing values will be used for NULL's

                        //Now try to get agentConfigs with newEncodedKey
                        agentConfigs    =   DataLayer.getConfigParams(newEncodedKey);
                    } else {
                        System.out.println("Agent couldnt be registered: "+encryptedID);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        return agentConfigs;
    }

    public static String getMapingEntry(String mapFilePath, String entryFor) {

        String toRet = null;
        try {
            FileInputStream is = null;
            if (mapBdl == null) {
                //java.util.logging.LoggingManager.getLogger(Utility.class.getName()).log(Level.INFO, "Loading "+mapFilePath+"...");
                is = new FileInputStream(mapFilePath);
                mapBdl = new PropertyResourceBundle(is);
            }

            //toRet   =   bdl.getString(entryFor).trim();
            Enumeration<String> bdlEnum = mapBdl.getKeys();//Get all Keys, i.e. entryFor
            String curKey = "";
            while (bdlEnum.hasMoreElements()) {
                curKey = bdlEnum.nextElement();
                if (curKey != null && curKey.equalsIgnoreCase(entryFor.trim())) {//Compare all Keys with Path, to ignore the Case-Issue
                    toRet = mapBdl.getString(curKey);
                    break;
                }
            }

            if (is != null) {
                is.close();
                is = null;
            }

        } catch (Exception ex) {
            toRet = null;
            java.util.logging.Logger.getLogger(Utility.class.getName()).log(Level.INFO, "Entry in " + mapFilePath + " not found for " + entryFor);
        }
        return toRet;
    }

    public static void resetUtilityResourceBundles() {

        //Reset the values to NULL, so that the Static Var could read new file...
        propFileBdl = null;
        mapBdl = null;
    }

    public static int nthOccurrence(String str, char c, int n) {
        int pos = str.indexOf(c, 0);
        while (n-- > 0 && pos != -1) {
            pos = str.indexOf(c, pos + 1);
        }
        return pos;
    }
    
    public static String GetIcon(String fileName) throws Exception {
        String extension = FilenameUtils.getExtension(fileName).trim();
        String ApplicationStartPath = getAgentConfigs().getStubPath();//GetProp("StubPath");
        return (extension != null && extension.length() > 1)
                ? ApplicationStartPath + "\\" + extension + ".ico"
                : ApplicationStartPath + "\\app.ico";

    }

    public static String createChecksum(String filename) throws
            Exception {
        InputStream fis = new FileInputStream(filename);

        byte[] buffer = new byte[1024];
        MessageDigest complete = MessageDigest.getInstance("MD5");
        int numRead;
        do {
            numRead = fis.read(buffer);
            if (numRead > 0) {
                complete.update(buffer, 0, numRead);
            }
        } while (numRead != -1);
        fis.close();
        BigInteger i = new BigInteger(1, complete.digest());
        buffer = null;
        return String.format("%1$032X", i).toLowerCase();

    }

    public static byte[] encodetoBase64(byte[] b) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        OutputStream b64os = MimeUtility.encode(baos, "base64");
        b64os.write(b);
        b64os.close();
        byte[] res = new byte[baos.toByteArray().length];
        res = baos.toByteArray();
        baos.close();
        return res;
    }

    public static byte[] decodetoBase64(byte[] b) throws Exception {
        ByteArrayInputStream bais = new ByteArrayInputStream(b);
        InputStream b64is = MimeUtility.decode(bais, "base64");
        byte[] tmp = new byte[b.length];
        int n = b64is.read(tmp);
        byte[] res = new byte[n];
        System.arraycopy(tmp, 0, res, 0, n);
        b64is.close();
        bais.close();
        tmp = null;
        return res;
    }

    public static boolean getInterfacesAndMatchIP(String IP) {
        boolean bRet = false;
        if (IP.equals("127.0.0.1")) {
            System.out.println("Local Loop Address is not Allowed");
            return bRet;
        }

        try {
            Enumeration e = NetworkInterface.getNetworkInterfaces();

            while (e.hasMoreElements()) {
                NetworkInterface ni = (NetworkInterface) e.nextElement();
                //System.out.println("Net interface: "+ni.getName());

                Enumeration e2 = ni.getInetAddresses();

                while (e2.hasMoreElements()) {
                    InetAddress ip = (InetAddress) e2.nextElement();
                    if (ip.toString().contains(":")) {
                        continue;
                    }
                    System.out.println("IP address: " + ip.toString().replace("/", ""));
                    if (IP.equals(ip.toString().replace("/", ""))) {
                        bRet = true;
                        break;
                    }
                }
                if (bRet) {
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bRet;
    }

    public static boolean containsIgnoreCase(ArrayList<String> list, String str) {

        if (list != null && list.size() > 0) {
            Iterator<String> itr = list.iterator();
            while (itr.hasNext()) {
                if (itr.next().equalsIgnoreCase(str)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void main(String args[]) {
        ArrayList<String> list = new ArrayList<String>();
        list.add("TEST/test");
        list.add("TEST/ting");
        list.add("test/testing");
        list.add("TeST/ABC/testing/");

        if (containsIgnoreCase(list, null)) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }

    public static boolean portIsOpen(String ip, int port, int timeout) {
        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(ip, port), timeout);
            socket.close();
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
    
    private static void runFDTClient(String[] args) throws Exception {
        FDT.main(args);
    }
    
    public static void putPathInFile(String file, String singleLine)
            throws IOException {
        
        FileWriter writer = new FileWriter(file);
        int size = 1;//hard-coded single line...   arrData.size();
        for (int i=0;i<size;i++) {
            String str = singleLine;
            writer.write(str);
            if(i < size-1)//This prevent creating a blank like at the end of the file**
                writer.write("\n");
        }
        writer.close();
    }
    
    public static void putPathsInFile(String file, ArrayList<String> arrData)
            throws IOException {
        
        FileWriter writer = new FileWriter(file);
        int size = arrData.size();
        for (int i=0;i<size;i++) {
            String str = "\\\\"+arrData.get(i).toString();
            writer.write(str);
            if(i < size-1)//This prevent creating a blank like at the end of the file**
                writer.write("\n");
        }
        writer.close();
    }
    
    public static boolean validIP(String ip) {
        if(ip == null || ip.length() < 7 || ip.length() > 15) return false;

        try {
            int x = 0;
            int y = ip.indexOf('.');

            if (y == -1 || ip.charAt(x) == '-' || Integer.parseInt(ip.substring(x, y)) > 255) return false;

            x = ip.indexOf('.', ++y);
            if (x == -1 || ip.charAt(y) == '-' || Integer.parseInt(ip.substring(y, x)) > 255) return false;

            y = ip.indexOf('.', ++x);
            return  !(y == -1 ||
                    ip.charAt(x) == '-' ||
                    Integer.parseInt(ip.substring(x, y)) > 255 ||
                    ip.charAt(++y) == '-' ||
                    Integer.parseInt(ip.substring(y, ip.length())) > 255 ||
                    ip.charAt(ip.length()-1) == '.');

        } catch (NumberFormatException e) {
            return false;
        }
    }
    
    public static boolean runFDTPoolClient(String filePath, String destPath, String fdtHost, Logger logger, String transID) throws IOException {
        boolean result  =   false;
        if(filePath!=null && destPath!=null && fdtHost!=null) {

            int limit=Utility.getAgentConfigs().getFdtBandwidth();
            //String fdtRequest   =   "java -jar \"C:/Users/HP/Desktop/fdt.jar\" -c 192.168.30.50 \"\\\\192.168.30.24/test_new/ShareArchiver.zip\" -d D:/opt/12121212asdf/";
            String fdtRequest   =   "java -jar lib/fdt.jar -c "+ fdtHost +" -d \""+destPath+"\" \""+filePath+"\" -limit "+limit+"M";
            
            logger.info(transID + " using FDT cmd request: "+fdtRequest);
            String cmd[] = new String[3];//{ "cmd", "/c", fdtRequest };
            if(OSValidator.isWindows()) {
                cmd[0]	=	"cmd";
                cmd[1] =	"/c";
                cmd[2]	=	fdtRequest;
            } else {
                cmd[0]	=	"bash";
                cmd[1] =	"-c";
                cmd[2]	=	fdtRequest;
            }
            result  =   Utility.runCommandInShell(cmd, logger, transID);//start a separate process
            //logger.info(transID + " executed command ");
            
            /*** FDT will do System.exit() - so dont call it as method in RemotAgent
            String[] fdtArgs    =   {"-c", fdtHost, "\\\\"+srcPath, "-d", destPath};
            for(int i=0; i<fdtArgs.length; i++) {
                logger.info(task.getUid() + " calling FDT main: "+fdtArgs[i]);
            }
            Utility.runFDTClient(fdtArgs);
            ***/
        } else {
            logger.warn(transID + " invalid parameters for runFDTPoolClient");
        }
        return result;
    }
    
    private static boolean runCommandInShell(String[] cmd, Logger logger, String transID) {

        boolean exitStatus  =   false;
        try {
            String s = null;

            // using the Runtime exec method:
            Process p = Runtime.getRuntime().exec(cmd);
            p.waitFor();

            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

            // read the output from the command
            logger.info(transID+" Here is the standard output of the command:");
            while ((s = stdInput.readLine()) != null) {
                if(s!=null && s.contains("Exit Status: OK")) {
                    exitStatus  =   true;//if its executed successfully
                }
                logger.info(transID+" "+s);
            }

            // read any errors from the attempted command
            logger.info(transID+" Here is the standard error of the command (if any):");
            while ((s = stdError.readLine()) != null) {
                logger.info(transID+" "+s);
            }

        } catch (IOException e) {
            logger.error(transID+" exception happened - here's what I know: ");
            LoggingManager.printStackTrace(e, logger);

        } catch (InterruptedException e) {
            logger.error(transID+" exception happened - here's what I know: ");
            LoggingManager.printStackTrace(e, logger);

        }
        
        return exitStatus;
    }
    
    public static HttpClient prepareSecureHttpClient() throws KeyStoreException, KeyManagementException, NoSuchAlgorithmException {

        org.apache.http.ssl.SSLContextBuilder context_b = SSLContextBuilder.create();
        context_b.loadTrustMaterial(new org.apache.http.conn.ssl.TrustSelfSignedStrategy());
        SSLContext ssl_context = context_b.build();
        org.apache.http.conn.ssl.SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(ssl_context,
                new org.apache.http.conn.ssl.DefaultHostnameVerifier());

        HttpClientBuilder builder = HttpClients.custom().setSSLSocketFactory(sslSocketFactory);
        CloseableHttpClient httpclient = builder.build();

        return httpclient;
    }
}
