/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataArchiver;

import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSTask;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSFile;
import com.virtualcode.agent.das.threadsHandler.TPEList;
import java.io.IOException;
import org.apache.log4j.Logger;

/**
 *
 * @author Abbas
 */
public class CIFSWalker {
    
    private String execID   =   null;
    private String syncMode =   null;
    private Logger logger   =   null;
    private String thisDocDestPath  =   null;
    private String activeActionType =   null;
    private String indexID  =   null;
    private boolean deduplicatedVolume  =   false;
    private String strPath;
    
    public CIFSWalker(String execID, String syncMode, Logger logger, String thisDocDestPath, String activeActionType, String indexID, boolean deduplicatedVolume,String strPath) {
        this.execID =   execID;
        this.syncMode   =   syncMode;
        this.logger =   logger;
        this.thisDocDestPath=   thisDocDestPath;
        this.activeActionType=  activeActionType;
        this.indexID    =   indexID;
        this.deduplicatedVolume =   deduplicatedVolume;
        this.strPath = strPath;
    }
    
    public boolean visitFile(CIFSFile cFile) throws IOException {
        
        //System.out.println("Visiting File: "+cFile.getFileObj().getName());

        //pause and resume
        Job aJobDetail = FileExecutors.currentExecs.get(this.execID);

        //IF interrupted by user than, Stop the Recursive Walk of Files
       if(JobStarter.isInterupted(this.execID, null)&& (!aJobDetail.isScheduled() ||(aJobDetail.getJobStatus() != null && aJobDetail.getJobStatus().getStatus().equals("PAUSED")))) {
            logger.debug("File Visitor is being stopped for "+this.execID);
            cFile.resetLastAccessTime();
            return false;
        }
        
        FileExecutors.incrementCounter(execID);//.fileWalkedCount += 1;
        CIFSTask task = new CIFSTask(cFile, execID, syncMode, logger, thisDocDestPath,activeActionType, deduplicatedVolume,this.strPath);
        task.setIndexID(indexID);
        //System.out.println("visiting CIFS: "+cFile.getFileObj().getPath());
        TPEList.startCIFSEvaluator(task.getExecID(), task);//FileExecutors.fileEvaluator.execute(new CIFSEvaluater(task));
        
        /*
        FileExecutors.fileWalkedCount += 1;
        NTFSTask task = new NTFSTask(filePath, attr);
        FileExecutors.fileEvaluator.execute(new NTFSEvaluater(task));
        */
        return true;
    }

    public void postVisitDirectory() {
    }

    public void preVisitDirectoryFailed() {
    }

    public void visitFileFailed() {
    }
}
