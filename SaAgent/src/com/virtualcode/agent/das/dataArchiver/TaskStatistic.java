/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataArchiver;

import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.utils.MyHashMap;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Formatter;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.log4j.Logger;

/**
 *
 * @author Saim
 */
public class TaskStatistic implements Runnable {

    //private final static AtomicLong executionCount = new AtomicLong(0);
    //private final static AtomicLong failedCount = new AtomicLong(0);
    //private static long comulativeSize =   0;
    private static MyHashMap executionCount = new MyHashMap();
    private static MyHashMap failedCount = new MyHashMap();
    private static MyHashMap comulativeSize = new MyHashMap();
    
    FileTaskInterface task = null;
    Logger errorLog = null;
    Logger taskSummary = null;

    public static void resetExecutionCtr(String execID) {
        executionCount.put(execID, new AtomicLong(0));
    }
    public static String getExecutionCount(String execID) {
        return executionCount.get(execID);
    }
    
    public static void resetFailedCtr(String execID) {
        failedCount.put(execID, new AtomicLong(0));
    }
    public static String getFailedCount(String execID) {
        return failedCount.get(execID);
    }
    
    public static void resetComulativeSize(String execID) {
        comulativeSize.put(execID, new AtomicLong(0));
    }
    public static String getComulativeSize(String execID) {
        return comulativeSize.get(execID);
    }

    public TaskStatistic(FileTaskInterface task) {
        this.task = task;
        errorLog    =   LoggingManager.getLogger(LoggingManager.ACTIVITY_ERROR, task.getExecID());
        taskSummary =   LoggingManager.getLogger(LoggingManager.TASK_SUMMARY, task.getExecID());
    }

    @Override
    public void run() {

        String eID  =   task.getExecID();
        Job j   =   FileExecutors.currentExecs.get(eID);
        TaskKPI taskKPI =   task.getTaskKpi();
        //comulativeSize  =   task.getTaskKpi().getComulativeSize();//Store it into Static var, to be used in JobSummary
        AtomicLong	tempSize	=	new AtomicLong(Long.parseLong(comulativeSize.get(eID)));
        if((!"NO".equals(task.getTaskKpi().isArchived) && 
        		!"AE".equals(task.getTaskKpi().isArchived) && 
        		!"LN".equals(task.getTaskKpi().isArchived)) ||	//if freshly archived successfuly
        		
        		(task.getTaskKpi().evaluatorFlag == EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND
                && j.getActionType().toLowerCase().trim().equals("evaluate")) //OR if Evaluate_Only Job
                ) {
        	
        	tempSize.addAndGet(task.getTaskKpi().getFileSize());
        }
        comulativeSize.put(eID, tempSize);//Store it into Static var, to be used in JobSummary
        FileExecutors.currentExecs.get(task.getExecID()).getJobStatistics().setArchivedVolume(tempSize.longValue());
        FileExecutors.currentExecs.get(task.getExecID()).getJobStatistics().update(taskKPI);

        StringBuilder sb = new StringBuilder();
        Formatter formatter = new Formatter(sb, Locale.US);

        formatter.format("{ID:%s},{Eval: %d secs},{FSize: %d},{DeDup: %d secs},{Arch: %d secs},{Stub: %d secs}",
                task.getUid().toString(),
                (task.getTaskKpi().evalutionEndTime - task.getTaskKpi().evalutionStartTime) / 1000,
                (task.getTaskKpi().getFileSize()),
                (task.getTaskKpi().deDupCheckerEndTime - task.getTaskKpi().deDupCheckerStartTime) / 1000,
                (task.getTaskKpi().archiveEndTime - task.getTaskKpi().archiveStartTime) / 1000,
                (task.getTaskKpi().stubberEndTime - task.getTaskKpi().stubberStartTime) / 1000);

        taskSummary.info(sb.toString());
        
        //taskSummary.debug(sb.toString());
        
        /*
        //This is a useless code - safe to delete
        taskSummary.info(" Ev-P : " + NTFSEvaluater.getExecutionCount()
                + //(" DeDupd: " + FileDeDupChecker.getExecutionCount())+
                " Ev-D : " + FileExecutors.currentJob.getJobStatistics().getTotalEvaluated()
                + " Ar-P : " + FileArchiver.getExecutionCount()
                + " Ar-D : " + FileExecutors.currentJob.getJobStatistics().getTotalArchived()
                + (" St-P : " + FileStubber.getExecutionCount())
                + " St-D : " + FileExecutors.currentJob.getJobStatistics().getTotalStubbed()
                + (" Ta-Pf : " + TaskStatistic.getFailedCount())
                + (" Ta-P : " + TaskStatistic.getExecutionCount()));
         */

        if (task.getTaskKpi().errorDetails != null) {
            failedCount.increment(task.getExecID());//.addAndGet(1);
            Writer writer = new StringWriter();
            PrintWriter printWriter = new PrintWriter(writer);
            task.getTaskKpi().errorDetails.printStackTrace(printWriter);
            
            errorLog.error("F : " + task.getUid().toString() + " : " + task.getTaskKpi().errorDetails.getMessage() + "\n" + writer.toString());
            writer = null;
            printWriter = null;
            
        }
        executionCount.increment(task.getExecID());//.addAndGet(1);
        
        task.closeCurrentTask(true);
        task = null;
    }
}
