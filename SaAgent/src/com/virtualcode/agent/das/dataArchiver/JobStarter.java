/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataArchiver;

import com.virtualcode.agent.das.archivePolicy.dto.JobStatus;
import executors.DataLayer;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.archivePolicy.dto.Job;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
/**
 *
 * @author se7
 */
public class JobStarter implements Runnable {
    
    private String AgentName    =   null;
    private String actionType   =   null;
    
    //private static Logger logger   =   Logger.getLogger(JobStarter.class);
    public static ArrayList<String> underProcessJobsList   =   new ArrayList<String>();
    private static List<String> interuptedExecIDsList  =   null;
    
    public JobStarter(String AgentName) {
        this.AgentName  =   AgentName;
        this.actionType  =   "IMPORT";
    }
    
    @Override
    public void run() {
        System.out.println("ARCHIVE: " +AgentName + " waiting for Job...");
        try {
            while (true) {
                try {
                    //Get the Jobs list from Sharearchiver WS 
                    List<Job> jobsList  =   DataLayer.JobsbyAgent(AgentName, actionType);
                    if(jobsList!=null && !jobsList.isEmpty() && jobsList.size()>0) {//if atleast One Job Exist
                        
                        //System.out.println("ARCHIVE: "+jobsList.size() + " new Jobs Found.");
                        
                        interuptedExecIDsList  =   DataLayer.interuptedJobs();
                        //System.out.println("Interupted number of Jobs: "+interuptedJobIDsList.size());
                        
                        //Traverse all the Jobs
                        for (Job j : jobsList) {
                            
                            int jID = j.getId();
                           // System.out.println("ARCHIVE: "+"Job ID : " + jID);
                            if(underProcessJobsList.contains(jID+"")) {
                                System.out.println("ARCHIVE: "+"Already processing: "+jID);
                                
                            }/* else if(interuptedJobIDsList.contains(jID+"")) {
                                System.out.println("Job is Canceled / Interupted: "+jID);
                                
                            }*/  else {
                                underProcessJobsList.add(jID+"");

                                JobStatus jobStatus  =   j.getJobStatus();
                                System.out.println("pausedJobList number of Jobs: "+jobStatus);
                                String executionID = null;
                                if( jobStatus == null){
                                    executionID = DataLayer.executionStarted(jID);
                                    System.out.println("ARCHIVE: "+"Execution ID : " + executionID);
                                }else{
                                    DataLayer.updateExecutionStatus(jID);
                                    executionID = jobStatus.getExecutionId();
                                    System.out.println("ARCHIVE: "+"Execution ID : " + executionID);
                                }

                                //Configure loggers for current executions
                                LoggingManager.configureLogger(jID, executionID, LoggingManager.ACTIVITY_DETAIL);
                                LoggingManager.configureLogger(jID, executionID, LoggingManager.ACTIVITY_ERROR);
                                LoggingManager.configureLogger(jID, executionID, LoggingManager.TASK_SUMMARY);
                                LoggingManager.configureLogger(jID, executionID, LoggingManager.JOB_SUMMARY);
                                LoggingManager.configureLogger(jID, executionID, LoggingManager.ACTIVITY_DB_ERROR);


                                JobThread jt    =   new JobThread(j, executionID);
                                Thread job      =   new Thread(jt);
                                job.start();
                            }
                            
                            //System.out.println("ARCHIVE: "+"Goto next Job...");
                        }
                        
                    } else {
                        //System.out.println("ARCHIVE: "+"New Jobs not found");
                        interuptedExecIDsList  =   DataLayer.interuptedJobs();
                        System.out.println("Interrupted number of Jobs: "+interuptedExecIDsList.size());
                    }
                    
                } catch(Exception ex) {//if WS is down at Server side, then dont' Terminate...
                    ex.getMessage();
                    ex.printStackTrace();
                }
                System.gc();
                Thread.currentThread().sleep(1000 * 60);
            }
            
        } catch (InterruptedException ex) {//if Thread.sleep is interupted
            ex.getMessage();
            ex.printStackTrace();
        }
    }
    
    public static boolean isInterupted(String execID, UUID uuid) {
        
        //Integer curJobID =   FileExecutors.currentExecs.get(execID).getId();
        boolean isInterupted =   false;
        
        synchronized(JobStarter.class) {
        	if(JobStarter.interuptedExecIDsList!=null)
        		isInterupted =   JobStarter.interuptedExecIDsList.contains(execID);
        }
        if(isInterupted)
            LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL, execID).warn(((uuid!=null)?uuid:"NO_TASK") + " job is Interupted : "+execID);
        
        return isInterupted;
    }
}
