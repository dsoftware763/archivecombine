/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataArchiver;

/**
 *
 * @author Saim
 */
public class TaskKPI {
 
    public long evalutionStartTime;
    public long evalutionEndTime;
    private static long comulativeSize    =   0;
    private long fileSize   =   0;
   
    long deDupCheckerStartTime;
    long deDupCheckerEndTime;
   
    long archiveStartTime;
    long archiveEndTime;
    
    public long exportStartTime;
    public long exportEndTime;
   
    long stubberStartTime;
    long stubberEndTime;
    
    public boolean isDuplicate = false;
    public String isArchived = "NO";
    public boolean isExported   =   false;
    public boolean isStubbed = false;
    
    public boolean failedDuplication = false;
    public boolean failedArchiving = false;
    public boolean failedExporting = false;
    public boolean failStubbing = false;
    public boolean failEvaluation = false;
    
    
    public Exception errorDetails;
    public EvalutorFlag evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SELECTED;
    
    public long getComulativeSize() {
        return comulativeSize;
    }
    
    public void updateComulativeSize(long newSize) {
        comulativeSize  +=  newSize;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }
}
