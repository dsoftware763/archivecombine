/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataArchiver;

import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.documentManagement.dto.DocumentType;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.fileSystems.ntfs.NTFSTask;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.threadsHandler.TPEList;
import com.virtualcode.agent.das.utils.*;
import org.apache.log4j.Logger;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Abbas
 */
public class NTFSEvaluater implements Runnable {

    Logger logger = null;

    public NTFSEvaluater(NTFSTask task) {
        this.task = task;
        logger = LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL, task.getExecID());
    }

    public FileTaskInterface getFileTask() {
        return this.task;
    }

    //private final static AtomicLong executionCount = new AtomicLong(0);
    private static MyHashMap executionCount = new MyHashMap();


    //pause and resume
    private static MyEvaluateHashMap propertiesMap = new MyEvaluateHashMap();
    private static int fileCount = 0;
    private static boolean currentFlag = false;
    private static int processedCount = 0;


    NTFSTask task = null;
    String extensions = "";
    private Long docAge = 0L;
    private Long docSize = 0L;
    private Long docLM = 0L;
    private Long docLA = 0L;
    private boolean ProcessHD = false;
    private boolean ProcessRO = false;
    private boolean processOfflineFiles = false;
    //ThreadPoolExecutor nextExecutor = FileExecutors.fileArchiver;

    private boolean Process() throws Exception {

        Path file = task.getPath();
        Path name = file.getFileName();
        logger.info(task.getUid().toString() + " : Evaluating : " + file.getFileName());
        PathMatcher matcher;
        String[] exts = extensions.split(",");
        Exception ex = null;

        if (exts.length == 0) {
            exts = "*.*,*.*".split(",");
        }

        for (String s : exts) {
            if (s == null || s.isEmpty()) {
                //   rp.skippedNullEmpty += 1;
                continue;
            }
            matcher = FileSystems.getDefault().getPathMatcher("glob:" + s);
            if (name != null && matcher.matches(name)) {
                logger.info(task.getUid().toString() + " : Matched : " + s);
                task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND;
                if (file.toString().isEmpty()
                        || file.toString().startsWith("~")
                        || file.toString().startsWith("$")
                        //|| file.toString().startsWith(".")
                        || file.toString().endsWith(".lnk")
                        || file.toString().endsWith(".tmp")
                        || file.toString().endsWith(".url")
                        || file.toString().endsWith("Thumbs.db")) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_TEMPORARY;
                    logger.info(task.getUid() + " Link File OR Temporary File OR Office temporary File : " + file);
                    return true;

                }

                BasicFileAttributes attrs = task.getAttributes();
                if (attrs.isDirectory()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_ISDIR;
                    logger.info(task.getUid() + " Is a Directory " + file);
                    return true;
                }

                if (attrs.isSymbolicLink()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_SYMBOLLINK;
                    logger.info(task.getUid() + " Is a Symbolic Link " + file);
                    return true;
                }

                /* Allow flagged files, to be archived... (These can be potentially Temporary Files - But allow)
                if (attrs.isOther()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_SYMBOLLINK;
                    logger.info(task.getUid() + " Is a Other File, no idea what does it mean, may be not regular " + file);
                    return true;
                }*/

                if (!attrs.isRegularFile()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_SYMBOLLINK;
                    logger.info(task.getUid() + " Is NOT a regular file " + file);
                    return true;
                }

                if (!task.isDeduplicatedVolume() && task.isReparsePoint()) {//if not a dedup volumne than check only for reparse point, than skip the file
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_TEMPORARY;
                    logger.info(task.getUid() + " Reparsepoint is set to true in a nonDeDupVolume : " + file);
                    return true;
                }

                if (task.isDeduplicatedVolume() && task.isReparsePoint() && task.isOfflineFile()) {//reparse point and Offline flags
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_TEMPORARY;
                    logger.info(task.getUid() + " Reparsepoint & OfflineFlag is set to true in Win DeDupVolume : " + file);
                    return true;
                }

                if (processOfflineFiles == false) {//if Offline Files are not to be processed
                    if (task.isOfflineFile() ||
                            "SAFileStubbing".equalsIgnoreCase(task.getOwner())) {
                        task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_TEMPORARY;
                        logger.info(task.getUid() + " App generated Stub (i.e. shortcut file) (" + processOfflineFiles + ") : " + file);
                        return true;
                    }
                } else {//if Offline files are to be processed, even if owner is SAFileStubbing
                    logger.info(task.getUid() + " Process Offline Files even if SAFileStubbing: (" + processOfflineFiles + ") " + file);
                }

                if (!ProcessHD && file.toFile().isHidden()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_HIDDENLECTED;
                    logger.info(task.getUid() + " Is a Hidden File " + file);
                    return true;
                }

                if (!ProcessRO && !file.toFile().canWrite()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_READ_ONLY;
                    logger.info(task.getUid() + " Is a Readonly File " + file);
                    return true;
                }

                if (docSize > 0 && attrs.size() < docSize) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_SMALL_SIZE;
                    logger.info(task.getUid() + " File Size : ( " + attrs.size() + " ) " + " Required Size : ( " + docSize + " ) ");
                    return true;
                } else if (docAge > 0 && attrs.creationTime().toMillis() > docAge) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_AGE;
                    logger.info(task.getUid() + " File Creation Time : ( " + attrs.creationTime().toMillis() + " ) " + " Required Time : ( " + docAge + " ) ");
                    return true;
                } else if (docLA > 0 && attrs.lastAccessTime().toMillis() > docLA) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_LASTACCESSTIME;
                    logger.info(task.getUid() + " File Last Access Time : ( " + attrs.lastAccessTime().toMillis() + " ) " + " Required Time : ( " + docLA + " ) ");
                    return true;
                } else if (docLM > 0 && attrs.lastModifiedTime().toMillis() > docLM) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_LASTMODIFIEDTIME;
                    logger.info(task.getUid() + " File Last Modified Time : ( " + attrs.lastModifiedTime().toMillis() + " ) " + " Required Size : ( " + docLM + " ) ");
                    return true;
                } else {
                    return true;
                }
            }
        }
        return false;
    }

    public static void resetExecutionCtr(String execID) {
        executionCount.put(execID, new AtomicLong(0));
    }

    public static String getExecutionCount(String execID) {
        return executionCount.get(execID);
    }

    public static Object getEvaluatedFilesMap(String execID) {
        return propertiesMap.get(execID);
    }

    public static void initializePropertiesMap(String execID) {
        propertiesMap.put(execID, new HashMap<String, String>());
        fileCount=0;
        processedCount=0;
        currentFlag=false;
    }



    public static int getFileCount() {
        return fileCount;
    }

    public static int getProcessedCount() {
        return processedCount;
    }


    @Override
    public void run() {

        try {
            //pause and resume
            Job aJobDetail = FileExecutors.currentExecs.get(task.getExecID());

            //Terminate the current Thread, if CANCEL is requested by User
            if (JobStarter.isInterupted(task.getExecID(), task.getUid()) && !aJobDetail.isScheduled()) {
                return;
            }
            task.getTaskKpi().evalutionStartTime = System.currentTimeMillis();


            Iterator newIterator = aJobDetail.getPolicy().getDocumentTypeSet().iterator();
            while (newIterator.hasNext()) {
                DocumentType documentType = (DocumentType) newIterator.next();
                extensions = extensions + "*" + documentType.getTheValue() + ",";
            }
            if (extensions.isEmpty()) {
                extensions = "*";
            }

            String dAge = aJobDetail.getPolicy().getDocumentAge();
            String laDate = aJobDetail.getPolicy().getLastAccessedDate();
            String lmDate = aJobDetail.getPolicy().getLastModifiedDate();
            String sz = aJobDetail.getPolicy().getSizeLargerThan();
            docAge = Utility.calculateMilliSec(Utility.parseYear(dAge), Utility.parseMonth(dAge));
            docLM = Utility.calculateMilliSec(Utility.parseYear(lmDate), Utility.parseMonth(lmDate));
            docLA = Utility.calculateMilliSec(Utility.parseYear(laDate), Utility.parseMonth(laDate));
            docSize = Utility.calculateSize(sz);
            ProcessRO = aJobDetail.isProcessReadOnly();
            ProcessHD = aJobDetail.isProcessHidden();
            boolean p = Process();
            task.getTaskKpi().evalutionEndTime = System.currentTimeMillis();

            if (!p && task.getTaskKpi().evaluatorFlag != EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND) {
                task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_EXTENSION;
            }

            //IT should not get called
            if (task.getTaskKpi().evaluatorFlag != EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND) {
                throw new Exception(task.getUid() + " Task : " + task.getPath().getFileName().toString() + " Reason: " + task.getTaskKpi().evaluatorFlag.toString());
            }

            //If current file is ready to archive / Stub / Evaluated
            if (task.getTaskKpi().evaluatorFlag == EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND) {

                //Get the file-size, to updated Evaluation vars
                task.getTaskKpi().setFileSize(this.task.getAttributes().size());
                task.getTaskKpi().updateComulativeSize(this.task.getAttributes().size());
            }

            if (task.getTaskKpi().evaluatorFlag == EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND
                    && (aJobDetail.getActionType().equalsIgnoreCase(Constants.jobTypeArchive)
                    || aJobDetail.getActionType().equalsIgnoreCase(Constants.jobTypeStub)
                    || aJobDetail.getActionType().equalsIgnoreCase(Constants.jobTypeWithoutStub))) {


                //TODO  local DB
                //add record in local db ,insert status is evaluated

                if (aJobDetail.isScheduled()) {
                    addToProperties();
                }
                logger.info(task.getUid() + " Sending to Archiving ");
                TPEList.startFileArchiver(task.getExecID(), task);//nextExecutor.execute(new FileArchiver(task));
                //  FileExecutors.statisticsCalculator.execute(new TaskStatistic(task));

            } else if (task.getTaskKpi().evaluatorFlag == EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND
                    && (aJobDetail.getActionType().equalsIgnoreCase(Constants.jobTypeMarkOnly))) {
                logger.info(task.getUid() + " Sending to Marking ");
                TPEList.startFileArchiver(task.getExecID(), task);//nextExecutor.execute(new FileArchiver(task));//Marking is defined in same pool of Archiving

            } else {

                logger.info(task.getUid() + " Sending to Task Statistic ");
                TPEList.startStatCalculator(task.getExecID(), task);//FileExecutors.statisticsCalculator.execute(new TaskStatistic(task));
            }
        } catch (Exception ex) {
            task.getTaskKpi().errorDetails = ex;
            task.getTaskKpi().failEvaluation = true;
            TPEList.startStatCalculator(task.getExecID(), task);//FileExecutors.statisticsCalculator.execute(new TaskStatistic(task));
        } finally {
            executionCount.increment(task.getExecID());//.addAndGet(1);

        }
    }

    //pause and resume
    private void addToProperties() {

        synchronized (this.getClass()) {

            Job job = FileExecutors.currentExecs.get(task.getExecID());
            HashMap<String, String> map = (HashMap<String, String>) propertiesMap.get(task.getExecID());
            boolean interruptFlag  = false;
            if (JobStarter.isInterupted(task.getExecID(), task.getUid()) ) {
                interruptFlag = true;
                if (processedCount == 0) {
                    processedCount = map.size();
                    currentFlag = true;
                }
            }

            Path file = task.getPath();


            //Task details
            String info = task.getExecID()
                    + "|" + task.getSyncMode()
                    + "|" + task.getThisDocDestPath()
                    + "|" + task.getActiveActionType()
                    + "|" + task.isDeduplicatedVolume()
                    + "|" + task.getStrPath()
                    + "|" + task.getTaskKpi().getFileSize();

            map.put(task.getUid().toString(), file.toString()+ "|"+ info);
            propertiesMap.put(task.getExecID(), map);
            if (map.size() == 10000) {
                this.fileCount++;
                FileUtil fileUtil = new FileUtil();
                fileUtil.writeIntoPropertiesFile(map, job, task.getExecID(), "evaluate", NTFSEvaluater.fileCount,interruptFlag,currentFlag,processedCount);
                propertiesMap.put(task.getExecID(), new HashMap<String, String>());
                currentFlag = false;

            }
        }

    }
}