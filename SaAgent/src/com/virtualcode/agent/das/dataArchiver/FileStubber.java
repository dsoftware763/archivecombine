/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataArchiver;

import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.utils.MyHashMap;
import com.virtualcode.agent.das.threadsHandler.TPEList;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.log4j.Logger;

/**
 *
 * @author Saim, Abbas
 */
public class FileStubber implements Runnable {

    public FileStubber(FileTaskInterface task) {
        this.task = task;
        logger  =   LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL, task.getExecID());
    }
    //private final static AtomicLong executionCount = new AtomicLong(0);
    private static MyHashMap executionCount = new MyHashMap();
    
    FileTaskInterface task = null;
    //ThreadPoolExecutor nextExecutor = FileExecutors.statisticsCalculator;
    Logger logger = null;

    private boolean Process() throws Exception {
        boolean output  =   true;
        if(FileExecutors.currentExecs.get(task.getExecID()).getActionType().toLowerCase().trim().equals("withoutstub")) {
    		//simply delete the original file
    		logger.info(task.getUid() + " Deleting original file... ");
    		task.deleteOriginalFile();
    		output	=	true;
    		
    	} else {//if have to create Stub...
            //String path = Utility.createInternetShortcut(task.getPathStr(), task.getRepoPath());

            //output = task.createInternetShortcut(task.getPathStr(), task.getSecureRepoPath());
            Job currentJob = FileExecutors.currentExecs.get(task.getExecID());
            if(currentJob.isCreateUrlFiles())
            {
                output = task.createInternetShortcut(task.getPathStr(), task.getSecureRepoPath(),currentJob);
                logger.info(task.getUid() + " Stub : Output : " + output);
            }

            /*if (path == null || path.equals("")) {
                output  =   false;
            } else {
                output  =   true;
            }*/
        }
        return output;
    }

    public static void resetExecutionCtr(String execID) {
        executionCount.put(execID, new AtomicLong(0));
    }
    public static String getExecutionCount(String execID) {
        return executionCount.get(execID);
    }

    @Override
    public void run() {

        try {
            //Terminate the current Thread, if CANCEL is requested by User
            if(JobStarter.isInterupted(task.getExecID(), task.getUid())) {
                    return;
            }
            task.getTaskKpi().stubberStartTime = System.currentTimeMillis();
            task.getTaskKpi().isStubbed = Process();
            task.getTaskKpi().stubberEndTime = System.currentTimeMillis();


            if (task.getTaskKpi().isStubbed) {
//                int taskInQueue = nextExecutor.getQueue().size();
//                while (taskInQueue >= (FileExecutors.maxTQSize - 1)) {
//                    try {
//                        LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL).debug("Stubber->Stats is sleeping now : Queue Size " + taskInQueue);
//                        Thread.sleep(5000);
//                    } catch (InterruptedException ex1) {
//                        LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL).debug(ex1);
//                    }
//                }
                TPEList.startStatCalculator(task.getExecID(), task);//nextExecutor.execute(new TaskStatistic(task));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Stubber Failed : " + task.getUid());

            task.getTaskKpi().failStubbing = true;
            task.getTaskKpi().errorDetails = ex;
//            int taskInQueue = FileExecutors.statisticsCalculator.getQueue().size();
//                while (taskInQueue >= (FileExecutors.maxTQSize - 1)) {
//                    try {
//                        LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL).debug("Stubber->Stats is sleeping now : Queue Size " + taskInQueue);
//                        Thread.sleep(5000);
//                    } catch (InterruptedException ex1) {
//                        LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL).debug(ex1);
//                    }
//                }
            TPEList.startStatCalculator(task.getExecID(), task);//FileExecutors.statisticsCalculator.execute(new TaskStatistic(task));
        } finally {
            executionCount.increment(task.getExecID());//.addAndGet(1);
        }
    }
}
