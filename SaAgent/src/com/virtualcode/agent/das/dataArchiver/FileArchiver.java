/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataArchiver;

import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

import javax.activation.DataHandler;

import com.virtualcode.agent.das.utils.LocalMachineUtil;
import jcifs.smb.SmbFile;

import org.apache.log4j.Logger;

import com.vcsl.sa.Acl;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.database.DatabaseManager;
import com.virtualcode.agent.das.database.NodeInfo;
import com.virtualcode.agent.das.fdt.FDTManager;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSTask;
import com.virtualcode.agent.das.fileSystems.ntfs.NTFSTask;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.threadsHandler.TPEList;
import com.virtualcode.agent.das.utils.Constants;
import com.virtualcode.agent.das.utils.MyHashMap;
import com.virtualcode.agent.das.utils.Utility;

import executors.DataLayer;

/**
 * @author Saim, Abbas
 */
public class FileArchiver implements Runnable {

    public FileArchiver(FileTaskInterface task) {
        this.task = task;
        logger = LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL, task.getExecID());
        dbErrorLogger = LoggingManager.getLogger(LoggingManager.ACTIVITY_DB_ERROR, task.getExecID());
    }

    //private final static AtomicLong executionCount = new AtomicLong(0);
    private static MyHashMap executionCount = new MyHashMap();
    private static MyHashMap alreadyExecCtr = new MyHashMap();
    private static MyHashMap dupContentCtr = new MyHashMap();

    FileTaskInterface task = null;
    //ThreadPoolExecutor nextExecutor = FileExecutors.fileStubber;
    Logger logger = null;
    Logger dbErrorLogger = null;
    private int tryCount = 0;


    private String checkDeduplicate(String calculatedHashID, String layers) throws Exception {

        //Has to verify for dedup at Agent level
        String repoUNCPath = null;
        if (calculatedHashID != null) {//&& (task.getJob.IsDedupVerificationRequired())) {
            logger.info(task.getUid() + " Sending request to server for content dedup verification " + calculatedHashID);
            repoUNCPath = DataLayer.checkDeduplicate(calculatedHashID, layers);

            if (repoUNCPath != null) {
                logger.info(task.getUid() + " Its a Deduplicate node at path " + repoUNCPath);
            } else {
                logger.info(task.getUid() + " its NOT a duplicate");
            }
        } else {
            if (calculatedHashID == null)
                logger.warn(task.getUid() + " Hash is not calculated");
            else
                logger.info(task.getUid() + " dedup verification is not required for this job");
        }

        return repoUNCPath;
    }

    private String Process() throws Exception {
        String documentPath = task.getNodeHeirarchy();
        String mapedDestPath = task.getURIMapping();
        mapedDestPath = ((mapedDestPath != null && !mapedDestPath.startsWith("null")) ? mapedDestPath : documentPath);

        String layers = "";

        Date modifiedDate = task.getLastModifiedDate();
        Date createdDate = task.getCreationDate();
        Date accessedDate = task.getLastAccessedDate();

        String fileStoreTag = FileExecutors.currentExecs.get(task.getExecID()).getFilestoreTag();

        String calculatedHashID = null;
        // TODO (UNCOMMENT THE IF BLOCK FOR BETTER PERFORMANCE...)
        //if(job.checkForDeDuplicate()==true)
        calculatedHashID = task.getCalculatedHashID();

        //hashCalculation is lenghty process, so terminate the current Thread, if CANCEL is requested by User
        if (JobStarter.isInterupted(task.getExecID(), task.getUid())) {
            return "NO";
        }

        //StagingPath job and staging path
        Job currentJob = FileExecutors.currentExecs.get(task.getExecID());

        //We can use H for 24 hr formatting and h for 12hr formating
        //e.g. yyyy.MM.dd G 'at' HH:mm:ss z
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String owner = task.getOwner();
        layers += "ModifiedOn=" + df.format(modifiedDate) + "|";
        layers += "CreatedOn=" + df.format(createdDate) + "|";
        layers += "ModifiedBy=" + owner + "|";
        layers += "CreatedBy=" + owner + "|";
        layers += "AccessedOn=" + df.format(accessedDate) + "|";
        layers += "SourceAgent=FS|";
        layers += "AgentType=FS|";
        //layers += "AgentName="+Utility.GetProp("AgentName") +"|";
        layers += "syncMode=" + task.getSyncMode() + "|";
        //layers += "sessionRepository="+Utility.GetProp("sessionRepository") +"|";
        //layers += "SPURL=" + task.getPathStr().replace(":", "") + "|";
        if (fileStoreTag != null && !fileStoreTag.isEmpty())//dont sent empty value to Server, in fileStoreTag attribute
            layers += "fileStoreTag=" + fileStoreTag + "|";
        layers += "fileSize=" + task.getBytesCount() + "|";
        layers += (calculatedHashID != null) ? ("calculatedHashID=" + calculatedHashID + "|") : "";
        layers += "SPURL=" + mapedDestPath + "|";
        layers += "partiallyArchived=" + (currentJob.getStagingJob() == 0 ? "0" : "1") + "|";
        layers += ("secureInfo=" + task.getEncodedString())+"|";//.encodedString);

        if(FileExecutors.currentExecs.get(task.getExecID()).getActionType().equalsIgnoreCase(Constants.jobTypeStub) ||
                FileExecutors.currentExecs.get(task.getExecID()).getActionType().equalsIgnoreCase(Constants.jobTypeWithoutStub)){
            layers += "is_stub="+"1|";
        }else{
            layers += "is_stub="+"0|";
        }
        layers = layers.trim();


        //logger.info(task.getUid() + " Owner : " + owner);
        logger.info(task.getUid() + " Layers : " + layers);

        String path64 = new String(Utility.encodetoBase64(mapedDestPath.getBytes("UTF-8")));

        String DASUrl = null;
        String transactionFlag = null;//
        //String isAlreadyArchived = "false";//by default - NOT already archived
        //if("true".equalsIgnoreCase(Utility.GetProp("LazyArchive"))) {//if LazyArchiving
        if ((Utility.getAgentConfigs().isLazyArchive()) && //or if its an App generated Stub
                currentJob.getStagingJob() == 0 && //if its not local archiving
                !JobStarter.isInterupted(task.getExecID(), task.getUid())) {//if LazyArchiving


            //TODO  local DB
            logger.info(task.getUid() + " Checking with  with local DB ");
            //Check in local db file archived or not
            DASUrl = DatabaseManager.getDasUrlByPath(mapedDestPath,1, logger,task.getUid().toString());

            if(DASUrl == null) {
                logger.info(task.getUid() + " Record not found in local DB ");
                //If record not found , Call server and check ,server side not found proceed for archive ,if found add details into local db
                //if (Utility.GetProp("CM").equals("1")) {
                logger.info(task.getUid() + " trying for lazy archive ");
                logger.info(task.getUid() + " Checking with  with Server DB "+path64);
                DASUrl = DataLayer.documentExistsByProperties(path64, layers);
                logger.info(task.getUid() + " Result from server DB DASUrl : " + DASUrl);
                if(DASUrl != null){
                    //TODO  local DB
                    addRecordToLocalDB(task.getPathStr(),task.getDocName(), modifiedDate, createdDate,DASUrl,currentJob.getId(),mapedDestPath);

                }
            }else{
                logger.info(task.getUid() + " Record found in local DB ");
            }


            //} else {
            //    doArchive = uploadDocument(path64, dataHandler, layers, acl);
            //}
        }

        Acl acl = task.getACL(documentPath);//Get ACE list



        if ((DASUrl == null || DASUrl.isEmpty()) &&
                currentJob.getStagingJob() == 0 && //if its not local archiving
                !JobStarter.isInterupted(task.getExecID(), task.getUid())) {//if NOT already archived - and needs to archive to Server

            String repoUNCPath = null;
            // TODO (UNCOMMENT THE IF BLOCK FOR BETTER PERFORMANCE...)
            if(currentJob.isCheckDeDuplication()==true)
            repoUNCPath = checkDeduplicate(calculatedHashID, layers);

            DataHandler dataHandler = null;
            if (repoUNCPath == null) {

                String fdtHost = Utility.getDASHost();
                int fdtPort = Utility.getAgentConfigs().getFdtPort();//default 54321
                boolean useFDT = false;//dont use FDT by default
                int minFileForFDT = 20;//MB
                if (Utility.getAgentConfigs().isEnableFDT() && Utility.portIsOpen(fdtHost, fdtPort, 200)) {//if FDT service is available
                    useFDT = true;
                    minFileForFDT = Utility.getAgentConfigs().getMinFileSize();
                } else {
                    logger.warn(task.getUid() + " FDT is down at: " + fdtHost + ":" + fdtPort);
                }

                if (task.getPathStr().startsWith("smb://")) {

                    String[] array = documentPath.split("/");
                    layers += "hostname=" + array[0] + "|";
                }else{
                    layers += "hostname="+ LocalMachineUtil.getOwnIPAdr()+"|";
                }

                //use FDT, if the file size is greater than 20 MB
                if (useFDT && calculatedHashID != null &&
                        task.getBytesCount() > minFileForFDT * 1024 * 1024) {
                    //if file is required to be transferd using FDT

                    boolean fdtResult = false;
                    try {
                        //String destPath =   Utility.getIDBasedFolderPath(calculatedHashID, false);
                        String srcPath = task.getPathStr();
                        //String origName =   null;
                        if (srcPath.startsWith("smb://")) {

                            //rename the file to its GUID
                            CIFSTask cifsTask = (CIFSTask) task;
                            String tempStr = cifsTask.getCIFSFile().getFileObj().getParent() + calculatedHashID;
                            logger.info(task.getUid() + " MinFileSize=(" + minFileForFDT + "), temporary rename to : " + calculatedHashID);

                            SmbFile tempFile = new SmbFile(tempStr);
                            cifsTask.getCIFSFile().getFileObj().renameTo(tempFile);

                            srcPath = tempStr.substring(tempStr.indexOf("@") + 1).replace("\\", "/");//trim the text before first @
                            String host = srcPath.substring(0, srcPath.indexOf("/"));

                            if (Utility.validIP(host)) {//if its a valid IP adress
                                logger.info(task.getUid() + " a valid IP, so get hostName");
                                InetAddress iAdr = InetAddress.getByName(host);
                                String ipAdr = iAdr.getHostName();//get the name of this IP

                                srcPath = srcPath.substring(srcPath.indexOf("/") + 1);
                                srcPath = ipAdr + "/" + srcPath;
                            }
                            srcPath = "\\\\" + srcPath;

                            //upload the binary, by using FDT
                            fdtResult = FDTManager.processFDT(task.getExecID(), srcPath, calculatedHashID, logger, task.getUid().toString());

                            //revert the file to its original name
                            tempFile.renameTo(cifsTask.getCIFSFile().getFileObj());
                            logger.info(task.getUid() + " reverted to original name: ");

                        } else {

                            //rename the file to its GUID
                            NTFSTask ntfsTask = (NTFSTask) task;
                            String tempStr = calculatedHashID;
                            logger.info(task.getUid() + " MinFileSize=(" + minFileForFDT + "), temporary rename to : " + calculatedHashID);

                            //rename at same level
                            String origName = ntfsTask.getPath().getFileName().toString();
                            Path tempName = ntfsTask.getPath().resolveSibling(tempStr);
                            Files.move(ntfsTask.getPath(), tempName);

                            srcPath = tempName.toString();//.replace("\\", "/");

                            //upload the binary, by using FDT
                            fdtResult = FDTManager.processFDT(task.getExecID(), srcPath, calculatedHashID, logger, task.getUid().toString());

                            //revert the file to its original name
                            Files.move(tempName, ntfsTask.getPath().resolveSibling(origName));
                            logger.info(task.getUid() + " reverted to original name: ");
                        }

                    } catch (Exception exp) {
                        logger.warn(task.getUid() + " FDT exception, so try by webservice " + exp.getMessage());
                        LoggingManager.printStackTrace(exp, logger);
                    }

                    if (fdtResult) { //if transferred by using FDT successfully
                        dataHandler = null;
                    } else {
                        logger.warn(task.getUid() + " FDT error, so upload by stream webservice ");
                        dataHandler = task.getDataHandler();//pass the dataHandler for webservice
                    }

                } else {
                    dataHandler = task.getDataHandler();//if the dataHandler is to be transfered through SOAP
                }

                transactionFlag = "NN";//data transferred to archive server
            } else {
                dataHandler = null;
                transactionFlag = "LN";//linked node (duplicate content)
            }
            logger.info(task.getUid() + " uploading request with transactionFlag " + transactionFlag);

            layers += "staging_path="+mapedDestPath;
            layers= layers.trim();
            logger.info(task.getUid() + " Final Layers : " + layers);
            //if (Utility.getAgentConfigs().getCM()==1) {
            DASUrl = DataLayer.uploadDocument(path64, dataHandler, layers, acl, Utility.getAgentConfigs().getUploadRequestTimeout(), Utility.getAgentConfigs().getUploadConnectionTimeout());

            //TODO  local DB
            //After successfully upload file into server add record into database
            logger.info(task.getUid() + " DASUrl " + DASUrl);
            addRecordToLocalDB(task.getPathStr(),task.getDocName(), modifiedDate, createdDate,DASUrl,currentJob.getId(),mapedDestPath);



            // addToProperties();
            //} else {
            //    DASUrl = uploadDocument(path64, dataHandler, layers, acl);
            //}

            dataHandler = null;
            //        dataSource = null;
            //        filePath = null;

        } else if ((DASUrl == null || DASUrl.isEmpty()) && //not already archived on server
                currentJob.getStagingJob() == 1 && //if its a local drive archiving
                !JobStarter.isInterupted(task.getExecID(), task.getUid())) {//have to archive content on local drive

            //String completePath = Utility.getLocalArchivePath();
            String completePath = currentJob.getStagingPath();
            if (completePath != null && (completePath.endsWith("/") || completePath.endsWith("\\"))) {
                completePath = completePath + mapedDestPath;
            } else {
                completePath = completePath + "/" + mapedDestPath;
            }
            layers += "hostname="+ LocalMachineUtil.getOwnIPAdr()+"|";
            layers += "staging_path="+completePath;
            layers= layers.trim();
            logger.info(task.getUid() + " Final Layers : " + layers);
            logger.info(task.getUid() + " case of Local Archive at " + completePath);
            if (Utility.moveFile(task.getDataHandler(), completePath, task.getUid(), logger)) {
                //the flag value of partiallyArchived should be 1
                DASUrl = DataLayer.uploadDocument(path64, null, layers, acl, Utility.getAgentConfigs().getUploadRequestTimeout(), Utility.getAgentConfigs().getUploadConnectionTimeout());//upload with dataHandler=NULL...

                logger.info(task.getUid() + " service returned DASUrl: " + DASUrl);

                DASUrl = completePath + "|" + DASUrl;//append the temporary path create appropriate stub files, by FileStubber...
                //TODO  local DB
                //After successfully upload file into server check and add record into database
                checkAndAddRecordToLocalDB(task.getPathStr(),task.getDocName(), modifiedDate, createdDate,DASUrl,currentJob.getId(),mapedDestPath);

            } else {
                DASUrl = null;
                logger.info(task.getUid() + " failed in local transfer.. ");
            }

        } else if (!JobStarter.isInterupted(task.getExecID(), task.getUid())) {
            transactionFlag = "AE";//already exist
            logger.info(task.getUid() + " Already archived " + DASUrl);


        } else {
            //job is interupted, by the user
            return "NO";
        }

//        dataSource = null;
//        filePath = null;
        path64 = null;
        layers = null;
        df = null;
        createdDate = null;
        modifiedDate = null;
        accessedDate = null;
        documentPath = null;
        owner = null;
        logger.info(task.getUid() + " Archive URL  : " + DASUrl);
        if (DASUrl != null && !DASUrl.isEmpty()) {
            task.setSecureRepoPath(DASUrl);   //task.repoPath =   DASUrl;
            DASUrl = null;

        } else {//in case of Any Error in archive process
            transactionFlag = "NO";
        }

        DASUrl = null;
        return transactionFlag;//false - error
    }

    private String ProcessMarking() throws Exception {
        String documentPath = task.getPathStr();
        String mapedDestPath = task.getURIMapping();
        mapedDestPath = ((mapedDestPath != null && !mapedDestPath.startsWith("null")) ? mapedDestPath : documentPath);

        String layers = "";

        Date modifiedDate = task.getLastModifiedDate();
        Date createdDate = task.getCreationDate();
        Date accessedDate = task.getLastAccessedDate();

        //We can use H for 24 hr formatting and h for 12hr formating
        //e.g. yyyy.MM.dd G 'at' HH:mm:ss z
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String owner = task.getOwner();
        layers += "ModifiedOn=" + df.format(modifiedDate) + "|";
        layers += "CreatedOn=" + df.format(createdDate) + "|";
        layers += "ModifiedBy=" + owner + "|";
        layers += "CreatedBy=" + owner + "|";
        layers += "AccessedOn=" + df.format(accessedDate) + "|";
        layers += "SourceAgent=FS|";
        layers += "AgentType=FS|";
        //layers += "AgentName="+Utility.GetProp("AgentName") +"|";
        layers += "syncMode=" + task.getSyncMode() + "|";
        //layers += "sessionRepository="+Utility.GetProp("sessionRepository") +"|";
        //layers += "SPURL=" + task.getPathStr().replace(":", "") + "|";
        layers += "SPURL=" + mapedDestPath + "|";
        layers += ("secureInfo=" + task.getEncodedString());//.encodedString);
        layers = layers.trim();

        DataLayer.addMarkedPath("" + FileExecutors.currentExecs.get(task.getExecID()).getId(), documentPath);
        logger.info(task.getUid() + " Marked : " + layers);

        return "TRUE";//if all goes fine
    }

    public static void resetExecutionCtr(String execID) {
        executionCount.put(execID, new AtomicLong(0));
        alreadyExecCtr.put(execID, new AtomicLong(0));
        dupContentCtr.put(execID, new AtomicLong(0));
    }

    public static String getExecutionCount(String execID) {
        return executionCount.get(execID);
    }

    public static String getAlreadyArchviedCount(String execID) {
        return alreadyExecCtr.get(execID);
    }

    public static String getDuplicateContentCount(String execID) {
        return dupContentCtr.get(execID);
    }


    @Override
    public void run() {

        try {
            runner();

        } catch (Exception ex) {

            logger.error(task.getUid() + " failed due to " + ex.getMessage());
            LoggingManager.printStackTrace(ex, logger);

            task.getTaskKpi().failedArchiving = true;
            task.getTaskKpi().errorDetails = ex;
            TPEList.startStatCalculator(task.getExecID(), task);//FileExecutors.statisticsCalculator.execute(new TaskStatistic(task));
        } finally {
            executionCount.increment(task.getExecID());//.addAndGet(1);

        }
    }

    private void runner() throws Exception {

        try {
            //Terminate the current Thread, if CANCEL is requested by User
            if (JobStarter.isInterupted(task.getExecID(), task.getUid())) {
                return;
            }

            Job job = FileExecutors.currentExecs.get(task.getExecID());

          /*  if (tryCount == 0 && job.isScheduled()) {
                addToProperties();
            }*/


            tryCount++;

            task.getTaskKpi().archiveStartTime = System.currentTimeMillis();
            //String authString = "repoName="+Utility.GetProp("sessionRepository")+ "&username=" + Utility.GetProp("AgentLogin") + "&password=" + Utility.GetProp("AgentPassword") + "";
            String authString = "repoName=" + Utility.getAgentConfigs().getCompany().getCompanyTag() + "&username=" + Utility.getAgentConfigs().getLogin() + "&password=" + Utility.getAgentConfigs().getPassword() + "";
            task.setEncodedString(new String(Utility.encodetoBase64(authString.getBytes())));


            if (Constants.jobTypeRetension.equalsIgnoreCase(FileExecutors.currentExecs.get(task.getExecID()).getActionType())) {
                task.getTaskKpi().isArchived = ProcessMarking();//case of Marking for Removal (i.e. Retension)
            } else {//case of Archiving
                task.getTaskKpi().isArchived = Process();
            }

            task.getTaskKpi().archiveEndTime = System.currentTimeMillis();

            if ("AE".equals(task.getTaskKpi().isArchived)) {//if alredy archived
                alreadyExecCtr.increment(this.task.getExecID());//inc by 1
            }
            if ("LN".equals(task.getTaskKpi().isArchived)) {//if archived, and the content is duplicated
                dupContentCtr.increment(this.task.getExecID());//inc by 1
            }

            if (!"NO".equals(task.getTaskKpi().isArchived) &&
                    (FileExecutors.currentExecs.get(task.getExecID()).getActionType().equalsIgnoreCase(Constants.jobTypeStub) ||
                            FileExecutors.currentExecs.get(task.getExecID()).getActionType().equalsIgnoreCase(Constants.jobTypeWithoutStub))) {
                TPEList.startFileStubber(task.getExecID(), task);//nextExecutor.execute(new FileStubber(task));

            } else if (!"NO".equals(task.getTaskKpi().isArchived)) {//nextExecutor of Retension type Job is also the TaskStatistic
                TPEList.startStatCalculator(task.getExecID(), task);//FileExecutors.statisticsCalculator.execute(new TaskStatistic(task));
            } else {
                throw new Exception(task.getUid() + " Archive attempt failed at server side..");
            }

        } catch (Exception ex) {
            logger.warn(task.getUid() + " Attempt [" + tryCount + "] failed as " + task.getTaskKpi().isArchived);
            int maxArchiveTries = (Utility.getAgentConfigs().getMaxNoOfTries() != null) ? Utility.getAgentConfigs().getMaxNoOfTries().intValue() : 2;
            if (tryCount < maxArchiveTries &&
                    !JobStarter.isInterupted(task.getExecID(), task.getUid())) {// Dont retry, if the Job is Cancelled..

                long waitTime = computeWaitTimeInMillis(tryCount);
                logger.info(task.getUid() + " Retrying for the Process in " + waitTime / 1000 + " secs");
                Thread.sleep(waitTime);//wait for X seconds...
                runner();

            } else {
                throw ex;
            }
        }
    }

    private long computeWaitTimeInMillis(int tryNumber) {
        long waitTime = 5000 * tryNumber;//Default is 5 sec for First Try...
        try {
            double fileSize = task.getBytesCount();
            fileSize = fileSize / (1024 * 1024);//in MBs

            if (fileSize > 1) {
                waitTime = waitTime * (long) Math.ceil(fileSize);
            }


            long maxWaitTime = 5 * 60 * 1000;//maxWaitTime for a file is 5 minutes...
            if (waitTime > maxWaitTime) {
                waitTime = maxWaitTime;
            }

        } catch (Exception ex) {
            LoggingManager.printStackTrace(ex, logger);
        }

        return waitTime;
    }

    private void checkAndAddRecordToLocalDB(String docPath,String docName, Date modifiedDate, Date createdDate,String DASUrl,int jobId,String destPath){
        logger.info(task.getUid() + " : Checking and Adding new record into local DB ");
        try{

            if(!DatabaseManager.checkDataBaseByPath(docPath,destPath,logger,task.getUid().toString()))
            {
                Timestamp timestamp=null;
                NodeInfo nodeInfo = new NodeInfo();
                nodeInfo.setVcNodePath(docPath);
                nodeInfo.setVcNodeName(docName);
                timestamp = new Timestamp(createdDate.getTime());
                nodeInfo.setVcCreationDate(timestamp);
                timestamp = new Timestamp(modifiedDate.getTime());
                nodeInfo.setVcModifiedDate(timestamp);
                timestamp = new Timestamp(new Date().getTime());
                nodeInfo.setVcArchivedDate(timestamp);
                nodeInfo.setVcFileSize(task.getBytesCount());
                nodeInfo.setVcProcessFlag(1);//0 evaluated 1 archived
                nodeInfo.setVcDasURL(DASUrl);
                nodeInfo.setJobId(jobId);
                nodeInfo.setVcDestPath(destPath);
                DatabaseManager.addNode(nodeInfo);
            }
        }catch(Exception e){
            if(docPath.toLowerCase().contains("smb://")){
                //if path is remote path remove original user name and password in the nodePath path
                docPath = docPath.substring(0,docPath.indexOf(';'))+";username:password"+docPath.substring(docPath.indexOf('@'), docPath.length());
            }
            dbErrorLogger.error(task.getUid()+" : Error while adding record to local DB"+docPath);

        }
    }

    private void addRecordToLocalDB(String docPath,String docName, Date modifiedDate, Date createdDate, String DASUrl,int jobId,String destPath){
        logger.info(task.getUid() + " : Adding new record into local DB ");
        try{
            Timestamp timestamp=null;
            NodeInfo nodeInfo = new NodeInfo();
            nodeInfo.setVcNodePath(docPath);
            nodeInfo.setVcNodeName(docName);
            timestamp = new Timestamp(createdDate.getTime());
            nodeInfo.setVcCreationDate(timestamp);
            timestamp = new Timestamp(modifiedDate.getTime());
            nodeInfo.setVcModifiedDate(timestamp);
            timestamp = new Timestamp(new Date().getTime());
            nodeInfo.setVcArchivedDate(timestamp);
            nodeInfo.setVcFileSize(task.getBytesCount());
            nodeInfo.setVcProcessFlag(1);//0 evaluated 1 archived
            nodeInfo.setVcDasURL(DASUrl);
            nodeInfo.setJobId(jobId);
            nodeInfo.setVcDestPath(destPath);
            DatabaseManager.addNode(nodeInfo);

        }catch(Exception e){
            if(docPath.toLowerCase().contains("smb://")){
                //if path is remote path remove original user name and password in the nodePath path
                docPath = docPath.substring(0,docPath.indexOf(';'))+";username:password"+docPath.substring(docPath.indexOf('@'), docPath.length());
            }
            dbErrorLogger.error(task.getUid()+" : Error while adding record to local DB "+docPath);
            dbErrorLogger.error(task.getUid()+" : "+e.getMessage());

        }
    }



    private void updateRecordInLocalDB(String docPath, String DASUrl) {
        logger.info(task.getUid() + "Updating record in local DB ");
        try {

            Timestamp timestamp = new Timestamp(new Date().getTime());
            DatabaseManager.updateNodeInfo(docPath, DASUrl, 1, timestamp);
        } catch (Exception e) {
            if(docPath.toLowerCase().contains("smb://")){
                //if path is remote path remove original user name and password in the nodePath path
                docPath = docPath.substring(0,docPath.indexOf(';'))+";username:password"+docPath.substring(docPath.indexOf('@'), docPath.length());
            }
            dbErrorLogger.error(task.getUid()+"Error while adding record to local DB"+docPath);

        }
    }


}
