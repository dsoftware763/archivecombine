/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataArchiver;

import com.vcsl.sa.Acl;
import com.vcsl.stats.IndexNode;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSTask;
import com.virtualcode.agent.das.utils.*;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSFile;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.dataIndexer.IndexerManager;
import com.virtualcode.agent.das.documentManagement.dto.DocumentType;

import com.virtualcode.agent.das.threadsHandler.TPEList;

import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicLong;
import jcifs.smb.SmbFile;
import org.apache.log4j.Logger;
import org.apache.poi.hpsf.CustomProperty;

/**
 * @author Abbas
 */
public class CIFSEvaluater implements Runnable {

    Logger logger = null;

    public CIFSEvaluater(CIFSTask task) {
        this.task = task;
        logger  =   LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL, task.getExecID());
    }
    public FileTaskInterface getFileTask() {
        return this.task;
    }

    //private final static AtomicLong executionCount = new AtomicLong(0);
    private static MyHashMap executionCount = new MyHashMap();
    CIFSTask task = null;
    String extensions = "";
    private Long docAge = 0L;
    private Long docSize = 0L;
    private Long docLM = 0L;
    private Long docLA = 0L;
    private boolean ProcessHD = false;
    private boolean ProcessRO = false;
    private boolean processOfflineFiles =   false;
    //ThreadPoolExecutor nextExecutor = FileExecutors.fileArchiver;

    //pause and resume
    private static MyEvaluateHashMap propertiesMap = new MyEvaluateHashMap();
    private static HashMap<String,Object> evaluateProperties = new HashMap<String,Object>();
    private static HashMap<String,Object> archiveProperties = new HashMap<String,Object>();

    private static int fileCount = 0;
    private static boolean currentFlag = false;
    private static int processedCount = 0;

    private boolean Process() throws Exception {

        //Path file = task.getPath();
        //Path name = file.getFileName();
        CIFSFile    cFile   =   this.task.getCIFSFile();
        String cFileName    =   cFile.getFileObj().getName();
        String cFilePath    =   cFile.getFileObj().getPath();
        cFilePath   =   (cFilePath!=null && cFilePath.contains("@"))?cFilePath.substring(cFilePath.lastIndexOf("@")) : cFilePath;

        logger.info(task.getUid().toString() + " : Evaluating : " + cFilePath);
        GlobMatch matcher   =   new GlobMatch();
        String[] exts = extensions.split(",");
        Exception ex = null;

        if (exts.length == 0) {
            exts = "*.*,*.*".split(",");
        }

        for (String s : exts) {
            if (s == null || s.isEmpty()) {
                /////////   rp.skippedNullEmpty += 1;
                continue;
            }

            //matcher = FileSystems.getDefault().getPathMatcher("glob:" + s);
            if (cFileName != null && matcher.match(cFileName, s)) {

                logger.info(task.getUid().toString() + " : Matched : " + s);
                task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND;

                if (cFileName.isEmpty()
                        || cFileName.startsWith("~")
                        || cFileName.startsWith("$")
                        //|| cFileName.startsWith(".")
                        || cFileName.endsWith(".lnk")
                        || cFileName.endsWith(".tmp")
                        || cFileName.endsWith(".url")
                        || cFileName.endsWith("Thumbs.db")) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_TEMPORARY;
                    logger.info(task.getUid() + " Link File OR Temporary File OR Office temporary File : " + cFileName);
                    return true;
                }

                //BasicFileAttributes attrs = task.getAttributes();
                SmbFile smbFile =   cFile.getFileObj();
                if (smbFile.exists() && smbFile.isDirectory()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_ISDIR;
                    logger.info(task.getUid() + " Is a Directory " + cFilePath);
                    return true;
                }

                if(!task.isDeduplicatedVolume() && task.isReparsePoint()) {//if not a dedup volumne than check only for reparse point, than skip the file
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_TEMPORARY;
                    logger.info(task.getUid() + " Reparsepoint is set to true in a nonDeDupVolume : " + cFileName);
                    return true;
                }

                if(task.isDeduplicatedVolume() && task.isReparsePoint() && task.isOfflineFile()) {//reparse point and Offline flags
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_TEMPORARY;
                    logger.info(task.getUid() + " Reparsepoint & OfflineFlag is set to true in Win DeDupVolume : " + cFileName);
                    return true;
                }

                /*if(processOfflineFiles==false) {//if Offline Files are not to be processed
                    if(task.isOfflineFile() ||
                            "SAFileStubbing".equalsIgnoreCase(task.getOwner())) {
                        task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_TEMPORARY;
                        logger.info(task.getUid() + " App generated Stub (i.e. shortcut file) ("+processOfflineFiles+") : " + cFileName);
                        return true;
                    }
                } else {//if Offline files are to be processed, even if owner is SAFileStubbing
                	logger.info(task.getUid() + " Process Offline Files even if SAFileStubbing: ("+processOfflineFiles+") " + cFileName);
                }
*/
                /*
                if (attrs.isSymbolicLink()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_SYMBOLLINK;
                    logger.info("Is a Symbolic Link " + file);
                    return true;

                }

                if (attrs.isOther()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_SYMBOLLINK;
                    logger.info("Is a Other File, no idea what does it mean, may be not regular " + file);
                    return true;
                }*/

                if (!ProcessHD && smbFile.isHidden()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_HIDDENLECTED;
                    logger.info(task.getUid() + " Is a Hidden File " + cFilePath);
                    return true;
                }

                if (!ProcessRO && !smbFile.canWrite()) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_READ_ONLY;
                    logger.info(task.getUid() + " Is a Readonly File " + cFilePath);
                    return true;
                }

                if (docSize > 0 && smbFile.length() < docSize) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_SMALL_SIZE;
                    logger.info(task.getUid() + " File Size : ( " + smbFile.length() + " ) " + " Required Size : ( " + docSize + " ) ");
                    return true;
                } else if (docAge > 0 && smbFile.createTime() > docAge) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_AGE;
                    logger.info(task.getUid() + " File Creation Time : ( " + smbFile.createTime() + " ) " + " Required Time : ( " + docAge + " ) ");
                    return true;
                } else if (docLA > 0 && smbFile.getLastAccess() > docLA) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_LASTACCESSTIME;
                    logger.info(task.getUid() + " File Last Access Time : ( " + smbFile.getLastAccess() + " ) " + " Required Time : ( " + docLA + " ) ");
                    return true;
                } else if (docLM > 0 && smbFile.getLastModified() > docLM) {
                    task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_LASTMODIFIEDTIME;
                    logger.info(task.getUid() + " File Last Modified Time : ( " + smbFile.getLastModified() + " ) " + " Required Time : ( " + docLM + " ) ");
                    return true;
                } else {
                    return true;
                }
            }
        }
        return false;
    }

    public static void resetExecutionCtr(String execID) {
        executionCount.put(execID, new AtomicLong(0));
    }
    public static String getExecutionCount(String execID) {
        return executionCount.get(execID);
    }

    public static Object getEvaluatedFilesMap(String execID) {
        return propertiesMap.get(execID);
    }


    public static void initializePropertiesMap(String execID) {
        propertiesMap.put(execID,new HashMap<String,String>());
        fileCount=0;
        processedCount=0;
        currentFlag=false;
    }


    public static int getFileCount() {
        return fileCount;
    }

    public static int getProcessedCount() {
        return processedCount;
    }

    @Override
    public void run() {

        try {

            //pause and resume
            Job aJobDetail = FileExecutors.currentExecs.get(task.getExecID());

            //Terminate the current Thread, if CANCEL is requested by User
            if (JobStarter.isInterupted(task.getExecID(), task.getUid()) && !aJobDetail.isScheduled()) {
                return;
            }

            task.getTaskKpi().evalutionStartTime = System.currentTimeMillis();


            Iterator newIterator = aJobDetail.getPolicy().getDocumentTypeSet().iterator();
            while (newIterator.hasNext()) {
                DocumentType documentType = (DocumentType) newIterator.next();
                extensions = extensions + "*" + documentType.getTheValue() + ",";
            }
            if (extensions.isEmpty()) {
                extensions = "*";
            }

            String dAge = aJobDetail.getPolicy().getDocumentAge();
            String laDate = aJobDetail.getPolicy().getLastAccessedDate();
            String lmDate = aJobDetail.getPolicy().getLastModifiedDate();
            String sz = aJobDetail.getPolicy().getSizeLargerThan();
            docAge = Utility.calculateMilliSec(Utility.parseYear(dAge), Utility.parseMonth(dAge));
            docLM = Utility.calculateMilliSec(Utility.parseYear(lmDate), Utility.parseMonth(lmDate));
            docLA = Utility.calculateMilliSec(Utility.parseYear(laDate), Utility.parseMonth(laDate));
            docSize = Utility.calculateSize(sz);
            ProcessRO = aJobDetail.isProcessReadOnly();
            ProcessHD = aJobDetail.isProcessHidden();
            processOfflineFiles =   aJobDetail.isProcessOfflineFiles();
            boolean p = Process();
            task.getTaskKpi().evalutionEndTime = System.currentTimeMillis();

            /*** Dont Analyze/index the files during Archive or Evaluation type jobs ***
             ///////////////////////
             ///////if(p && (its a EvaluatedOnly & Index Job)) {
             if(!Constants.jobTypeRetension.equalsIgnoreCase(aJobDetail.getActionType())) {//if Non-Retention job
             try {
             buildLuceneIndexes();
             } catch (Exception e) {
             logger.warn(task.getUid() + " Analysis failed during Evaluation, but continuing for archiver..");
             }
             }
             ///////}
             //////////////
             *******/

            if (!p && task.getTaskKpi().evaluatorFlag != EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND) {
                task.getTaskKpi().evaluatorFlag = EvalutorFlag.FLAG_PROCESSFILE_SKIPPED_MISMATCH_EXTENSION;
            }

            //IT should not get called
            if (task.getTaskKpi().evaluatorFlag != EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND) {
                throw new Exception(task.getUid() + " Reason: " + task.getTaskKpi().evaluatorFlag.toString());
            }

            //If current file is ready to archive / Stub / Evaluated
            if (task.getTaskKpi().evaluatorFlag == EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND) {

                //Get the file-size, to updated Evaluation vars
                task.getTaskKpi().setFileSize(this.task.getCIFSFile().getFileObj().length());
                task.getTaskKpi().updateComulativeSize(this.task.getCIFSFile().getFileObj().length());
            }

            if (task.getTaskKpi().evaluatorFlag == EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND
                    && (aJobDetail.getActionType().equalsIgnoreCase(Constants.jobTypeArchive)
                    || aJobDetail.getActionType().equalsIgnoreCase(Constants.jobTypeStub)
                    || aJobDetail.getActionType().equalsIgnoreCase(Constants.jobTypeWithoutStub))) {

                if (aJobDetail.isScheduled()) {
                    addToProperties();
                }
                logger.info(task.getUid() + " Sending to Archiving ");
                TPEList.startFileArchiver(task.getExecID(), task);//nextExecutor.execute(new FileArchiver(task));
                //  FileExecutors.statisticsCalculator.execute(new TaskStatistic(task));

            } else if (task.getTaskKpi().evaluatorFlag == EvalutorFlag.FLAG_PROCESSFILE_EXTENSIONFOUND
                    && (aJobDetail.getActionType().equalsIgnoreCase(Constants.jobTypeRetension) &&
                    aJobDetail.getActiveActionType().equalsIgnoreCase(Constants.jobTypeMarkOnly))) {
                logger.info(task.getUid() + " Sending to Marking ");
                TPEList.startFileArchiver(task.getExecID(), task);//nextExecutor.execute(new FileArchiver(task));//Marking is defined in same pool of Archiving

            } else {

                logger.info(task.getUid() + " Sending to Task Statistic " + aJobDetail.getActionType());
                TPEList.startStatCalculator(task.getExecID(), task);
            }
        } catch (Exception ex) {
            task.getTaskKpi().errorDetails = ex;
            task.getTaskKpi().failEvaluation = true;
            logger.warn(task.getUid() + "exception occured: "+ex.getMessage());
            //ex.printStackTrace();
            TPEList.startStatCalculator(task.getExecID(), task);
        } finally {
            executionCount.increment(task.getExecID());//.addAndGet(1);

        }
    }


    //pause and resume
    private void addToProperties() {
        synchronized (this.getClass()) {
            Job job = FileExecutors.currentExecs.get(task.getExecID());
            HashMap<String, String> map = (HashMap<String, String>) propertiesMap.get(task.getExecID());
            boolean interruptFlag  = false;
            if (JobStarter.isInterupted(task.getExecID(), task.getUid()) ) {
                interruptFlag = true;
                if (processedCount == 0) {
                    processedCount = map.size();
                    currentFlag = true;
                }
            }

            //Task details
            String info = task.getExecID()
                    + "|" + task.getSyncMode()
                    + "|" + task.getThisDocDestPath()
                    + "|" + task.getActiveActionType()
                    + "|" + task.isDeduplicatedVolume();



            String cFilePath = this.task.getCIFSFile().getFileObj().getPath();
            String pathString = task.getStrPath();

            map.put(task.getUid().toString(), cFilePath.substring(cFilePath.indexOf('@'), cFilePath.length()) + "|"+info+"|"+pathString.substring(pathString.indexOf('@'), pathString.length())+ "|" + task.getTaskKpi().getFileSize() );
            propertiesMap.put(task.getExecID(), map);
            if (map.size()== 10000) {
                this.fileCount++;
                FileUtil fileUtil = new FileUtil();
                fileUtil.writeIntoPropertiesFile(map, job, task.getExecID(), "evaluate", fileCount,interruptFlag,currentFlag,processedCount);
                propertiesMap.put(task.getExecID(), new HashMap<String, String>());
                currentFlag = false;
            }


        }
    }

    /*** Dont Analyze/index the files during Archive or Evaluation type jobs ***
     private void buildLuceneIndexes() throws Exception {

     IndexNode iNode = new IndexNode();

     String filePath	=	task.getNodeHeirarchy();//task.getCIFSFile().getFileObj().getPath();
     String fileName	=	task.getDocName();//task.getCIFSFile().getFileObj().getName();
     boolean isArchived	=	false;
     if(fileName.endsWith(".url")) {//also index the stubs
     isArchived	=	true;
     }

     logger.info(this.task.getUid().toString() +" filePath: " + filePath);
     iNode.setFilePath(filePath);

     logger.info(this.task.getUid().toString() +" fileName: " + fileName);
     iNode.setFileName(fileName);

     logger.info(this.task.getUid().toString() +" dateCreation: " + task.getCreationDate().getTime());
     iNode.setDateCreation(
     XMLGregorianCalendarConverter.asXMLGregorianCalendar(task.getCreationDate()));

     logger.info(this.task.getUid().toString() +" dateModification: " +task.getLastModifiedDate().getTime());
     iNode.setDateModification(
     XMLGregorianCalendarConverter.asXMLGregorianCalendar(task.getLastModifiedDate()));

     logger.info(this.task.getUid().toString() +" dateLastAccess: " + task.getLastAccessedDate().getTime());
     iNode.setDateLastAccess(
     XMLGregorianCalendarConverter.asXMLGregorianCalendar(task.getLastAccessedDate()));

     logger.info(this.task.getUid().toString() +" fileSize: " +  task.getBytesCount());
     iNode.setFileSize(this.task.getBytesCount());

     logger.info(this.task.getUid().toString() +" isArchived: " +  isArchived);
     iNode.setIsArchived(isArchived+"");

     /*** ACL is always NULL for NTFS files
     CifsUtil	cifsUtil	=	new CifsUtil();
     Acl	acl	=	cifsUtil.getACL(task.getCIFSFile().getFileObj());
     if(acl!=null) {
     HashMap<String, String> aclAttribs	=	CifsUtil.getAclAttribs(acl);
     //logger.info(this.task.getUid().toString() +" VC_ALLOWED_SIDS: " +  aclAttribs.get(CustomProperty.VC_ALLOWED_SIDS));
     iNode.setAllowedSids(aclAttribs.get(CustomProperty.VC_ALLOWED_SIDS));
     iNode.setDenySids(aclAttribs.get(CustomProperty.VC_DENIED_SIDS));
     iNode.setShareAllowedSids(aclAttribs.get(CustomProperty.VC_S_ALLOWED_SIDS));
     iNode.setShareDenySids(aclAttribs.get(CustomProperty.VC_S_DENIED_SIDS));
     }*** /

     /**** Dont try to extractContent in Archiving jobs..
     //read the contents by tika API
     String extractContent	=	Utility.getAgentConfigs().getExtractContent();
     if("yes".equalsIgnoreCase(extractContent)) {//Content extraction is optional

     logger.info(this.task.getUid().toString() +" accessing binary for content or metadata: "+extractContent);
     String content	=	task.getReadableContent();
     String metaData	=	task.getMetaData();

     iNode.setReadableContent(content);
     iNode.setMetaData(metaData);
     } *** /

     //finally add it into IndexWriter
     logger.info(this.task.getUid().toString() +" indexID: "+task.getIndexID());
     IndexerManager.addDocument(task.getIndexID(), iNode, logger);

     // also update the Stats - for summary/reporting
     if (!IndexerManager.updateStats(iNode, task.getIndexID(), logger))//if returned FALSE
     {
     logger.info(this.task.getUid().toString() + " saStats not exist for CIFSEvaluater Counts...");
     }

     logger.info(this.task.getUid().toString() + " Added: " + fileName);
     }
     ****/
}