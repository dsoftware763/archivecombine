/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataArchiver;

import com.virtualcode.agent.das.archivePolicy.dto.ExcludedPath;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.archivePolicy.dto.SPDocLib;
import com.virtualcode.agent.das.dataRetainer.RetentionHandler;
import com.virtualcode.agent.das.documentManagement.dto.DocumentType;
import com.virtualcode.agent.das.fdt.FDTManager;
import com.virtualcode.agent.das.fileSystems.FileServiceInterface;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSFile;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSService;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSTask;
import com.virtualcode.agent.das.fileSystems.ntfs.NTFSService;
import com.virtualcode.agent.das.fileSystems.ntfs.NTFSTask;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.threadsHandler.HotStatsUpdater;
import com.virtualcode.agent.das.threadsHandler.TPEList;
import com.virtualcode.agent.das.utils.*;
import executors.DataLayer;
import org.apache.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Saim
 */
public class FileExecutors {


    //save the job instance of current executions, to be used in Stats etc
    public static HashMap<String, Job> currentExecs = new HashMap<String, Job>();

    //static Job currentJob = null;
    //public static long fileWalkedCount = 0L;
    private static MyHashMap fileWalkedCtr = new MyHashMap();


    public void startJob(Job job, String executionID) throws InterruptedException, Exception {
        Logger logger = LoggingManager.getLogger(LoggingManager.JOB_SUMMARY, executionID);
        Logger pauseResumeLogger = null;

        logger.info("===============================================");
        logger.info("Job Info" + job);
        logger.info("================================================");
        int errorCodeID = 0;
        int jID = -1;

        //Initialize the Counters for current Execution of cur Job
        fileWalkedCtr.put(executionID, new AtomicLong(0));
        NTFSEvaluater.resetExecutionCtr(executionID);
        CIFSEvaluater.resetExecutionCtr(executionID);//initialize the counter for CIFS evaluater
        FileArchiver.resetExecutionCtr(executionID);
        TaskStatistic.resetExecutionCtr(executionID);
        TaskStatistic.resetFailedCtr(executionID);
        TaskStatistic.resetComulativeSize(executionID);
        FileStubber.resetExecutionCtr(executionID);


        NTFSEvaluater.initializePropertiesMap(executionID);
        CIFSEvaluater.initializePropertiesMap(executionID);

        Job currentJob = null;

        currentExecs.put(executionID, job);
        currentJob = currentExecs.get(executionID);
        currentJob.getJobStatistics().setJobStartTime(new Date());

        if (executionID == null) {
            throw new Exception("Execution ID is null");
        }

        // make/initiate the ThreadPools for current Execution of an archive Job
        TPEList threadsPoolSet = new TPEList(executionID, logger);

        logger.info("Job Info");
        logger.info("=======");
        logger.info("Execution ID " + executionID);
        logger.info("Job Name " + currentJob.getJobName());
        logger.info("Job Type " + currentJob.getActionType() + " syncMode: " + currentJob.getSyncMode());
        logger.info("WriteableFilestore " + currentJob.getFilestoreTag());

        //Initialize the Continuous Stats update process - to provide HotStats to server...
        HotStatsUpdater hsUpdater = new HotStatsUpdater(jID, executionID,
                errorCodeID, currentJob.getJobStatistics(),
                LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL, executionID));
        Thread th = new Thread(hsUpdater);
        th.start();

        //initialize the FDT Manager
        String fdtHost = Utility.getDASHost();
        //String baseDestPath =	Utility.getAgentConfigs().getFileStorePath() + "content/";
        //FDTQueueManager.initFDTManager(executionID, destPath, fdtHost);
        FDTManager.initFDTManager(fdtHost);


        try {
            //Print the version of Agent, along with each Job
            //logger.info("Commercial Version ID = 1, Major Version ID = 1, Minor Version = 2");
            //logger.info("Commercial Version V 1.2.0, RD 16 Dec 2011");

            jID = (int) job.getId();
            if (job.isScheduled()) {
                LoggingManager.configureLogger(jID, executionID, LoggingManager.PAUSE_RESUME_DETAIL);
                pauseResumeLogger = LoggingManager.getLogger(LoggingManager.PAUSE_RESUME_DETAIL, executionID);
                pauseResumeLogger.info("===============================================");
                pauseResumeLogger.info("Job Info" + job);
                pauseResumeLogger.info("================================================");
            }


            if (job.getJobStatus() != null && job.getJobStatus().getStatus().equals("PAUSED")) {


                //Define boolean variable to hold interrupt flag
                boolean interruptFlag = false;
                boolean CIFSFlag = false;
                boolean NTFSFlag = false;
                String domain = null;
                String userName = null;
                String pwd  = null;

                for (SPDocLib p : job.getSpDocLibSet()) {

                    if (p.getLibraryName().toLowerCase().contains("smb://")) {//if the path is type CIFS
                        CIFSFlag= true;
                        String[] credentials = Utility.getCredentialsFromSmb(p.getLibraryName());
                        domain = credentials[0];
                        userName = credentials[1];
                        pwd = credentials[2];
                    } else {//if the path is type NTFS
                        NTFSFlag = true;
                    }
                }

                pauseResumeLogger.info(" NTFSFlag " + NTFSFlag +" CIFSFlag "+CIFSFlag);

                String basePath = Utility.getAgentConfigs().getLogBasePath();
                if (basePath == null || basePath.isEmpty())
                    basePath = "ErrorLog";
                String folderPath = basePath + File.separator + job.getId() + File.separator + executionID;
                File file = new File(folderPath);


                //Read evaluate file process one by one
                for (final File fileEntry : file.listFiles()) {
                    if (fileEntry.isDirectory()) {
                        continue;
                    } else {
                        String fileName = fileEntry.getName();
                        if (fileName.contains("evaluate") && fileName.contains("current") && !fileName.contains("done") && !interruptFlag) {


                            pauseResumeLogger.info("Reading file " + fileName);
                            //Read file and load key and values into evaluateProperties
                            Properties properties = new Properties();
                            InputStream input = new FileInputStream(folderPath + File.separator + fileName);
                            properties.load(input);
                            //load archive file  from folder Path
                            Properties evaluateProperties = new Properties();
                            evaluateProperties.putAll(properties);

                            //Split file name by .(dot) and get processed count
                            String[] splitArray = fileName.split("\\.");
                            //System.out.println("splitArray " + splitArray.length);//TODO
                            String zero = splitArray[0];
                            String first = splitArray[1];
                            String second = splitArray[2];
                            String third = splitArray[3];
                            //System.out.println(zero + " " + first + " " + second + " " + third);//TODO

                            //Convert processed count into integer and initialize integer variable with 1
                            int i = 1;
                            int processCount = Integer.parseInt(second);

                            //Loop evaluationProperties inside only balance lines process
                            for (final String name : evaluateProperties.stringPropertyNames()) {

                                if (i > processCount) {

                                    //get value from evaluate properties
                                    String value = evaluateProperties.getProperty(name);

                                    String filePath = null;
                                    String execID = null;
                                    String syncMode = null;
                                    String thisDocDestPath = null;
                                    String activeActionType = null;
                                    boolean deduplicateVolume = false;
                                    String pathStr = null;
                                    Path path = null;
                                    BasicFileAttributes bfa = null;
                                    CIFSFile cifsFile = null;
                                    String fileSize="0";

                                    if (NTFSFlag) {
                                        //Split value using comma
                                        String[] strArray = value.split("\\|");
                                        filePath = strArray[0];
                                        execID = strArray[1];
                                        syncMode = strArray[2];
                                        thisDocDestPath = strArray[3];
                                        activeActionType = strArray[4];
                                        deduplicateVolume = Boolean.parseBoolean(strArray[5]);
                                        pathStr = strArray[6];
                                        fileSize =strArray[7];
                                        path = Paths.get(filePath);
                                        bfa = Files.readAttributes(path, BasicFileAttributes.class);
                                    }else if(CIFSFlag){
                                        //Split value using comma
                                        String[] strArray = value.split("\\|");
                                        filePath = "smb://" + domain + ";" + userName + ":" + pwd  + strArray[0].replace("\\", "/");
                                        execID = strArray[1];
                                        syncMode = strArray[2];
                                        thisDocDestPath = strArray[3];
                                        //thisDocDestPath = thisDocDestPath != null ?"smb://" + domain + ";" + userName + ":" + pwd  +thisDocDestPath :thisDocDestPath;
                                        activeActionType = strArray[4];
                                        deduplicateVolume = Boolean.parseBoolean(strArray[5]);
                                        pathStr ="smb://" + domain + ";" + userName + ":" + pwd  +strArray[6].replace("\\", "/");
                                        fileSize =strArray[7];
                                        cifsFile = new CIFSFile(filePath);
                                    }


                                    //Check job is interrupted by server
                                    if (!JobStarter.isInterupted(execID, null)) {

                                        if (NTFSFlag) {

                                            pauseResumeLogger.info("Processing  NTFSFile path " + path );
                                            //Prepare NTFSTask and process it
                                            NTFSTask ntfsTask = new NTFSTask(path, bfa, execID, syncMode, thisDocDestPath, activeActionType, deduplicateVolume, pathStr);
                                            ntfsTask.getTaskKpi().setFileSize(Long.parseLong(fileSize));
                                            ntfsTask.getTaskKpi().updateComulativeSize(Long.parseLong(fileSize));
                                            //Start Archive process
                                            TPEList.startFileArchiver(ntfsTask.getExecID(), ntfsTask);//nextExecutor.execute(new FileArchiver(task));
                                        }else if(CIFSFlag){

                                            String securePath = filePath.substring(filePath.indexOf('@'), filePath.length());
                                            pauseResumeLogger.info("Processing  CIFSFile path " + securePath);
                                            //Prepare NTFSTask and process it
                                            CIFSTask cifsTask = new CIFSTask(cifsFile, execID, syncMode, logger, thisDocDestPath,activeActionType, deduplicateVolume,pathStr);
                                            cifsTask.getTaskKpi().setFileSize(Long.parseLong(fileSize));
                                            cifsTask.getTaskKpi().updateComulativeSize(Long.parseLong(fileSize));
                                            //Start Archive process
                                            TPEList.startFileArchiver(cifsTask.getExecID(), cifsTask);//nextExecutor.execute(new FileArchiver(task));
                                        }

                                    } else {
                                        //Close input stream
                                        input.close();
                                        //rename file with current process number
                                        FileUtil fileUtil = new FileUtil();
                                        String newFileName = folderPath + File.separator + zero + "." + first + "." + i + "." + third;
                                        fileUtil.renameFile(folderPath + File.separator + fileName, newFileName, pauseResumeLogger);
                                        interruptFlag = true;
                                        break;
                                    }
                                }
                                //increment each time
                                i++;
                            }


                            //check interrupt flag and i value is reached final target value
                            if (!interruptFlag) {
                                //close input stream
                                input.close();
                                //remove current and rename file with normal evaluate{no}.properties
                                String newFileName = folderPath + File.separator + zero + "." + third;
                                FileUtil fileUtil = new FileUtil();
                                fileUtil.renameFile(folderPath + File.separator + fileName, newFileName, pauseResumeLogger);

                                //Do compress using gzip, file name should be .done
                                GZipFile gzipFile = new GZipFile(pauseResumeLogger, 1024);
                                InputStream inputStream = new FileInputStream(new File(newFileName));
                                gzipFile.gZipIt(inputStream, new File(newFileName + ".done.gz"));

                                //Delete evaluate properties file no longer required
                                boolean result = fileUtil.doDeleteFile(new File(newFileName), pauseResumeLogger);
                                pauseResumeLogger.info("FileUtil :doDeleteFile result " + result);


                            }


                        }

                    }
                }

                File file1 = new File(folderPath);
                //Read evaluate file process one by one
                for (final File fileEntry : file1.listFiles()) {
                    if (fileEntry.isDirectory()) {
                        continue;
                    } else {
                        String fileName = fileEntry.getName();

                        if (fileName.contains("evaluate") && !fileName.contains("done") && !interruptFlag) {

                            String plainFile = null;
                            FileUtil fileUtil = new FileUtil();
                            GZipFile gzipFile = new GZipFile(pauseResumeLogger, 1024);

                            if(fileName.contains(".gz")){
                                //Uncompress file get plain file to process file
                                plainFile = gzipFile.gUnzipIt(folderPath + File.separator + fileName);

                                //delete compressed file no longer needed
                                fileUtil.doDeleteFile(new File(folderPath + File.separator + fileName), pauseResumeLogger);
                            }else{
                                plainFile = folderPath + File.separator + fileName;
                            }



                            //Read file and load key and values into evaluateProperties
                            Properties properties = new Properties();
                            InputStream input = new FileInputStream(plainFile);
                            properties.load(input);
                            //load archive file  from folder Path
                            Properties evaluateProperties = new Properties();
                            evaluateProperties.putAll(properties);


                            //Split file name by .(dot) and get processed count
                            String[] splitArray = plainFile.split("\\.");
                            //System.out.println("splitArray " + splitArray.length);//TODO
                            String zero = splitArray[0];
                            String first = splitArray[1];
                            //System.out.println(zero + " " + first + " ");//TODO

                            //Convert processed count into integer and initialize integer variable with 1
                            int i = 1;

                            //Loop evaluationProperties inside only balance lines process
                            for (final String name : evaluateProperties.stringPropertyNames()) {


                                //get value from evaluate properties
                                String value = evaluateProperties.getProperty(name);
                                String filePath = null;
                                String execID = null;
                                String syncMode = null;
                                String thisDocDestPath = null;
                                String activeActionType = null;
                                boolean deduplicateVolume = false;
                                String pathStr = null;
                                Path path = null;
                                BasicFileAttributes bfa = null;
                                CIFSFile cifsFile = null;
                                String fileSize="0";

                                if (NTFSFlag) {
                                    //Split value using comma
                                    String[] strArray = value.split("\\|");
                                    execID = strArray[1];
                                    syncMode = strArray[2];
                                    thisDocDestPath = strArray[3];
                                    activeActionType = strArray[4];
                                    deduplicateVolume = Boolean.parseBoolean(strArray[5]);
                                    pathStr = strArray[6];
                                    path = Paths.get(pathStr);
                                    fileSize =strArray[7];
                                    bfa = Files.readAttributes(path, BasicFileAttributes.class);
                                }else if(CIFSFlag){
                                    //Split value using comma
                                    String[] strArray = value.split("\\|");
                                    filePath = "smb://" + domain + ";" + userName + ":" + pwd  + strArray[0].replace("\\", "/");
                                    execID = strArray[1];
                                    syncMode = strArray[2];
                                    thisDocDestPath = strArray[3];
                                    //thisDocDestPath = thisDocDestPath != null ?"smb://" + domain + ";" + userName + ":" + pwd  +thisDocDestPath :thisDocDestPath;
                                    activeActionType = strArray[4];
                                    deduplicateVolume = Boolean.parseBoolean(strArray[5]);
                                    pathStr = "smb://" + domain + ";" + userName + ":" + pwd  +strArray[6].replace("\\", "/");
                                    fileSize =strArray[7];
                                    cifsFile = new CIFSFile(filePath);
                                }


                                //Check job is interrupted by server
                                if (!JobStarter.isInterupted(execID, null)) {

                                    if (NTFSFlag) {
                                        pauseResumeLogger.info("Processing  NTFSFile path " + path );
                                        //Prepare NTFSTask and process it
                                        NTFSTask ntfsTask = new NTFSTask(path, bfa, execID, syncMode, thisDocDestPath, activeActionType, deduplicateVolume, pathStr);
                                        ntfsTask.getTaskKpi().setFileSize(Long.parseLong(fileSize));
                                        ntfsTask.getTaskKpi().updateComulativeSize(Long.parseLong(fileSize));
                                        //Start Archive process
                                        TPEList.startFileArchiver(ntfsTask.getExecID(), ntfsTask);//nextExecutor.execute(new FileArchiver(task));
                                    }else if(CIFSFlag){
                                        String securePath = filePath.substring(filePath.indexOf('@'), filePath.length());
                                        pauseResumeLogger.info("Processing  CIFSFile path " + securePath);
                                        //Prepare NTFSTask and process it
                                        CIFSTask cifsTask = new CIFSTask(cifsFile, execID, syncMode, logger, thisDocDestPath,activeActionType, deduplicateVolume,pathStr);
                                        cifsTask.getTaskKpi().setFileSize(Long.parseLong(fileSize));
                                        cifsTask.getTaskKpi().updateComulativeSize(Long.parseLong(fileSize));
                                        //Start Archive process
                                        TPEList.startFileArchiver(cifsTask.getExecID(), cifsTask);//nextExecutor.execute(new FileArchiver(task));
                                    }

                                } else {
                                    //Close input stream
                                    input.close();
                                    //rename file with current process number
                                    String newFileName =  zero + ".current." + i + "." + first;
                                    fileUtil.renameFile( plainFile, newFileName, pauseResumeLogger);
                                    interruptFlag = true;
                                    break;
                                }

                                //increment each time
                                i++;
                            }


                            //check interrupt flag and i value is reached final target value
                            if (!interruptFlag) {
                                //close input stream
                                input.close();

                                //Do compress using gzip, file name should be .done
                                InputStream inputStream = new FileInputStream(new File(plainFile));
                                gzipFile.gZipIt(inputStream, new File(plainFile + ".done.gz"));

                                //Delete evaluate properties file no longer required
                                boolean result = fileUtil.doDeleteFile(new File(plainFile), pauseResumeLogger);
                                pauseResumeLogger.info("FileUtil :doDeleteFile result " + result);


                            }
                        }
                    }
                }


            } else {
                Iterator iterator = job.getSpDocLibSet().iterator();
                while (iterator.hasNext()) {
                    SPDocLib spDocLib = (SPDocLib) iterator.next();

                    if (!Constants.pathTypeRepo.equalsIgnoreCase(spDocLib.getLibraryType())) {//if pathType=REPO
                        String libName = spDocLib.getLibraryName();
                        logger.info("\t Path (" + spDocLib.getLibraryType() + ") : " + ((libName != null && libName.startsWith("smb://")) ? libName.substring(libName.indexOf("@")) : libName));//Paths of type REPO should NOT be available here
                        logger.info("\t Dest Path : " + spDocLib.getDestinationPath());

                    }
                    logger.info("job -> Paths -> excluded paths:");
                    Iterator excludePathIterator = spDocLib.getExcludedPathSet().iterator();
                    while (excludePathIterator.hasNext()) {
                        ExcludedPath excludePath = (ExcludedPath) excludePathIterator.next();
                        String libName = excludePath.getPath();
                        logger.info("\t \t" + ((libName != null && libName.startsWith("smb://")) ? libName.substring(libName.indexOf("@")) : libName));
                    }
                }

                logger.info("job -> policy name : " + job.getPolicy().getPolicyName());
                logger.info("job -> policy -> document-types: ");
                Iterator newIterator = job.getPolicy().getDocumentTypeSet().iterator();
                while (newIterator.hasNext()) {
                    DocumentType documentType = (DocumentType) newIterator.next();
                    logger.info("\t documentType name: " + documentType.getTheValue());
                }


                for (SPDocLib p : job.getSpDocLibSet()) {

                    if (Constants.pathTypeRepo.equalsIgnoreCase(p.getLibraryType())) {//if pathType=REPO
                        logger.info("\n skipping this path to be observed, bcoz of its type=REPO");
                        continue;
                    }

                    FileServiceInterface fs = null;
                    if (p.getLibraryName().toLowerCase().contains("smb://")) {//if the path is type CIFS
                        fs = new CIFSService(executionID, job.getSyncMode(), job.getActiveActionType(), p.getId() + "", p.isDeduplicatedVolume());

                    } else {//if the path is type NTFS
                        fs = new NTFSService(executionID, job.getSyncMode(), job.getActiveActionType(), p.isDeduplicatedVolume());
                    }

                    /*** Dont Analyze/index the files during Archive or Evaluation type jobs ***
                     if(!Constants.jobTypeRetension.equalsIgnoreCase(currentJob.getActionType())) {// if Non-Retention Job
                     logger.info("Non-Retention Job, so initialse IndexWriter");

                     //Initiate IndexWriter, and load in Pool
                     ///DriveLetters driveLtr	=	IndexingExecutors.getDriveLetter(p);//driveLtr can be NULL
                     //logger.info("drive ltr: "+driveLtr);
                     //IndexerManager.initIndexWriter(driveLtr, p.getId()+"", true, logger);
                     IndexerManager.initIndexWriter(p.getId()+"", true, logger);
                     AnalysisManager.initAnalysisWriter(p.getId()+"", logger);
                     }
                     ****/

                    //Prepare the list of Excuded sub-paths of current Path
                    ArrayList<String> excPathList = new ArrayList();
                    for (ExcludedPath ep : p.getExcludedPathSet()) {
                        excPathList.add(ep.getPath());
                    }
                    String mapedDestPath = (p.getDestinationPath() != null) ? p.getDestinationPath().trim() : null;
                    //fs.walkFileTree(p.getLibraryName(), excPathList, true, mapedDestPath);
                    if (job.isArchiveFromAnalysis() && p.getLibraryName().toLowerCase().contains("smb://")) {// if to be archived by Analysis

                        logger.info("Performance Optimized Mode for Archive job");
                        int currentPage = 1;
                        List<String> filesList = null;
                        com.vcsl.sa.Policy mapedPolicy = DataLayer.mapPolicy(job.getPolicy());

                        String[] credentials = Utility.getCredentialsFromSmb(p.getLibraryName());
                        String domain = credentials[0];
                        String userName = credentials[1];
                        String pwd = credentials[2];

                        do {
                            filesList = DataLayer.getDocumentList("" + p.getId(), p.getLibraryName(), mapedPolicy, currentPage, job.getCompanyTags());
                            logger.info("Documents list retrieved from service " + currentPage);

                            if (filesList != null && filesList.size() > 0) {
                                for (int i = 0; i < filesList.size(); i++) {
                                    String filePath = filesList.get(i);
                                    logger.info("Filtered Path: " + filePath);

                                    filePath = "smb://" + domain + ";" + userName + ":" + pwd + "@" + filePath.replace("\\", "/");

                                    fs.walkFileTree(filePath, excPathList, false, mapedDestPath);//non-recursive calls
                                }
                                currentPage++;

                            } else {
                                logger.info("NO result found, matching the Policy for SpDocLibID: " + p.getId());
                            }

                        } while (filesList != null && filesList.size() > 0);//do untill we found entries on pages

                    } else {//if not to be archived from Analysis
                        fs.walkFileTree(p.getLibraryName(), excPathList, true, mapedDestPath);
                    }
                }
            }


            logger.warn("Stop request to Thread Pool");
            //Wait for shutting down the threads of current Execution
            threadsPoolSet.stopCurThreadPools();
            ////PROCEED FURTHER AFTER STOPPING THE THREAD POOL OF CUR JOB-EXEC

            //logger.info("Flushing of the FDTManager queue...");//if some files are left in Queue (to be uploaded)
            //FDTManager.processQueue(executionID, logger, "FileExecutors");

            /*** Dont Analyze/index the files during Archive or Evaluation type jobs ***
             if(!Constants.jobTypeRetension.equalsIgnoreCase(currentJob.getActionType())) {// if Non-Retention Job
             //Now close the IndexWriter's of all Shares...after completing the Indexes
             for (SPDocLib p : job.getSpDocLibSet()) {
             try {
             IndexerManager.closeIndexWriter(p.getId()+"", logger);
             AnalysisManager.closeAnalysisWriter(p.getId()+"", logger);
             } catch(IOException ioe) {
             logger.error("Indexer not closed for ["+p.getId()+"] "+p.getLibraryName());
             LoggingManager.printStackTrace(ioe, logger);
             }
             }
             }
             * *******/

            // clear executors
            logger.info("Memory ... " + Runtime.getRuntime().totalMemory());
            logger.info("File Walked : " + fileWalkedCtr.get(executionID));
            fileWalkedCtr.put(executionID, new AtomicLong(0));//reset value to 0

            logger.info("");
            logger.info("Executors Report");
            logger.info("=================");
            logger.info("NTFS Evaluated : " + NTFSEvaluater.getExecutionCount(executionID));
            logger.info("CIFS Evaluated : " + CIFSEvaluater.getExecutionCount(executionID));
//            logger.info("DeDup Checked : " + FileDeDupChecker.getExecutionCount(executionID));
            logger.info("Archived/Marked : " + FileArchiver.getExecutionCount(executionID));
            logger.info("Stubbed : " + FileStubber.getExecutionCount(executionID));
            logger.info("Failed : " + TaskStatistic.getFailedCount(executionID));
            logger.info("Summary : " + TaskStatistic.getExecutionCount(executionID));
            logger.info("Total Size : " + TaskStatistic.getComulativeSize(executionID) + " bytes");

            if (job.isScheduled()) {

                boolean interruptFlag = false;
                if (JobStarter.isInterupted(executionID, null)) {
                    interruptFlag = true;
                }
                HashMap<String, String> ntfsMap = (HashMap<String, String>) NTFSEvaluater.getEvaluatedFilesMap(executionID);
                pauseResumeLogger.info("NTFS Evaluated Size : " + ntfsMap.size() + "");
                int ntfsFileCount = NTFSEvaluater.getFileCount();
                int ntfsProcessedCount = NTFSEvaluater.getProcessedCount();
                if (ntfsMap != null && ntfsMap.size() > 0) {
                    FileUtil fileUtil = new FileUtil();
                    //check file count is zero or more than zero ,if zero then evaluate file is current file need to put current flag is true and send processed count
                    if(ntfsFileCount ==0) {
                        fileUtil.writeIntoPropertiesFile(ntfsMap, job, executionID, "evaluate", ntfsFileCount + 1, interruptFlag, true,ntfsProcessedCount);
                    }else{
                        fileUtil.writeIntoPropertiesFile(ntfsMap, job, executionID, "evaluate", ntfsFileCount + 1, interruptFlag, false, 0);
                    }

                }

                HashMap<String, String> cifsMap = (HashMap<String, String>) CIFSEvaluater.getEvaluatedFilesMap(executionID);
                int cifsFileCount = CIFSEvaluater.getFileCount();
                int cifsProcessedCount = CIFSEvaluater.getProcessedCount();
                pauseResumeLogger.info("CIFS Evaluated Size : " + cifsMap.size() + "");
                if (cifsMap != null && cifsMap.size() > 0) {
                    FileUtil fileUtil = new FileUtil();
                    //check file count is zero or more than zero ,if zero then evaluate file is current file need to put current flag is true and send processed count
                    if(cifsFileCount ==0){
                        fileUtil.writeIntoPropertiesFile(cifsMap, job, executionID, "evaluate", cifsFileCount + 1, interruptFlag, true, cifsProcessedCount);
                    }else{
                        fileUtil.writeIntoPropertiesFile(cifsMap, job, executionID, "evaluate", cifsFileCount + 1, interruptFlag, false, 0);
                    }

                }

            }


            currentJob.getJobStatistics().setJobEndTime(new Date());

            logger.info("Calling WS for executionCompleted with ID : " + executionID);

            boolean success = DataLayer.executionCompleted(jID, executionID, errorCodeID, currentJob.getJobStatistics());
            if (success) {
                logger.info("Successfully returned from execution Completed WS : " + jID);
            } else {
                throw new Exception("Oops.Error returned from execution Completed WS : " + jID);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            errorCodeID = 1;
            if (executionID != null) {

                if (currentJob == null) {
                    logger.info("Current Job is Null");
                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.executionCompleted(jID, executionID, errorCodeID, null);

                } else if (currentJob.getJobStatistics() == null) {
                    logger.info("currentJob.getJobStatistics() is Null");
                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.executionCompleted(jID, executionID, errorCodeID, null);
                } else {
                    currentJob.getJobStatistics().setJobEndTime(new Date());

                    logger.info("Calling WS for failed executionCompleted with ErrorCode : " + errorCodeID);
                    DataLayer.executionCompleted(jID, executionID, errorCodeID, currentJob.getJobStatistics());
                }
            }
            logger.fatal("ExecutionID: " + executionID);
            logger.fatal("JobID: " + jID);
            logger.fatal("ErrorID: " + errorCodeID);
            logger.fatal(ex.getMessage());
            Writer writer = new StringWriter();
            PrintWriter printWriter = new PrintWriter(writer);
            ex.printStackTrace(printWriter);

            LoggingManager.getLogger(LoggingManager.ACTIVITY_ERROR, executionID).error("Fatal : " + "\n" + writer.toString());
            writer = null;
            printWriter = null;

        } finally {

            currentExecs.remove(executionID);

            //remove the entry from alreadyProcessing jobs
            if (JobStarter.underProcessJobsList != null)
                JobStarter.underProcessJobsList.remove(jID + "");

            if (RetentionHandler.underProcessJobsList != null)
                RetentionHandler.underProcessJobsList.remove(jID + "");//remove from the volatile list at Agent side...

        }
    }

    public static void incrementCounter(String execID) {
        fileWalkedCtr.increment(execID);
    }


}
