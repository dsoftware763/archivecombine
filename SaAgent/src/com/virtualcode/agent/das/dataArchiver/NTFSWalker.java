/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.dataArchiver;

import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.fileSystems.ntfs.NTFSTask;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.agent.das.threadsHandler.TPEList;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.FileSystemException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author Abbas
 */
public class NTFSWalker extends SimpleFileVisitor<Path> {

    private ArrayList<String> excPathList;
    private String execID   =   null;
    private String syncMode =   null;
    private String thisDocDestPath  =   null;
    private String activeActionType =   null;
    private boolean deduplicatedVolume  =   false;
    private String strPath;

    
    public NTFSWalker(ArrayList<String> excPathList, String execID, String syncMode, String thisDocDestPath,
            String activeActionType, boolean deduplicatedVolume,String strPath) {
        this.excPathList    =   excPathList;
        this.execID =   execID;
        this.syncMode   =   syncMode;
        this.thisDocDestPath=thisDocDestPath;
        this.activeActionType=activeActionType;
        this.deduplicatedVolume =   deduplicatedVolume;
        this.strPath = strPath;
    }
    
    @Override
    public FileVisitResult preVisitDirectory(Path filePath, BasicFileAttributes attr) {

        //IF interrupted by user than, Stop the Recursive Walk of Files
        //pause and resume
        Job aJobDetail = FileExecutors.currentExecs.get(this.execID);

        if(JobStarter.isInterupted(this.execID, null)&& (!aJobDetail.isScheduled() ||(aJobDetail.getJobStatus() != null && aJobDetail.getJobStatus().getStatus().equals("PAUSED")))) {
            System.out.println("File/Dir Walker is being stopped for "+this.execID);
            return FileVisitResult.SKIP_SIBLINGS;//skip siblings and its subtree
        }

        //Skip the Dir, if its added in Exculed list by User...
        if(Utility.containsIgnoreCase(excPathList, filePath.toString())) {
            System.out.println("Skip the excluded path: " + filePath.toString());
            return FileVisitResult.SKIP_SUBTREE;
        } else {
            return FileVisitResult.CONTINUE;
        }
    }
    
    @Override
    public FileVisitResult visitFile(Path filePath, BasicFileAttributes attr) {


        //IF interrupted by user than, Stop the Recursive Walk of Files
        //pause and resume
        Job aJobDetail = FileExecutors.currentExecs.get(this.execID);

        if(JobStarter.isInterupted(this.execID, null)&& (!aJobDetail.isScheduled() ||(aJobDetail.getJobStatus() != null && aJobDetail.getJobStatus().getStatus().equals("PAUSED")))) {
            System.out.println("File Visitor is being stopped for "+this.execID);
            //cFile.resetLastAccessTime();
            return FileVisitResult.SKIP_SUBTREE;
        }
        
        FileExecutors.incrementCounter(execID);//.fileWalkedCount += 1;
        NTFSTask task = new NTFSTask(filePath, attr, execID, syncMode, thisDocDestPath, activeActionType, deduplicatedVolume,strPath);
        System.out.println("visiting NTFS: "+filePath);
        TPEList.startNTFSEvaluator(task.getExecID(), task);//FileExecutors.fileEvaluator.execute(new NTFSEvaluater(task));
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
        if (exc instanceof AccessDeniedException) {
            System.err.println(dir + ": cannot access directory");
        }
        System.err.println(exc);

        return FileVisitResult.CONTINUE;
    }

    public FileVisitResult preVisitDirectoryFailed(Path dir, IOException exc) {
        if (exc instanceof AccessDeniedException) {
            System.err.println(dir + ": cannot access directory");
        }
        System.err.println(exc);

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) {
        if (exc instanceof FileSystemException) {
            System.err.println(file.toString() + ": The device is not ready");
        }
        System.err.println(exc);

        return FileVisitResult.CONTINUE;
    }
}
