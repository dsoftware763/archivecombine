/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.fileSystems.cifs;

import com.virtualcode.agent.das.dataIndexer.CIFSIndexingWalker;
import com.virtualcode.agent.das.fileSystems.FileServiceInterface;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import org.apache.log4j.Logger;

/**
 *
 * @author se7
 */
public class CIFSIndexingService implements FileServiceInterface {
    
    Logger logger = null;
    private String execID   =   null;
    private String syncMode =   null;
    private String indexID  =   null;
    private boolean deduplicatedVolume  =   false;
    
    public CIFSIndexingService(String execID, String syncMode, String indexID, boolean deduplicatedVolume) {
        this.execID =   execID;
        this.syncMode   =   syncMode;
        this.indexID    =   indexID;
        this.deduplicatedVolume =   deduplicatedVolume;
        logger  =   LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL, execID);
    }
    
    public void walkFileTree(String strPath, ArrayList<String> excPathList, boolean isRecursive, String thisDocDestPath) {
        
        try {
            
            CIFSFile cFile   =   new CIFSFile(strPath);
            //cFile.login("10.1.165.15", "mazhar", "newbuild");

            if(cFile.getFileObj().isDirectory() && isRecursive) {//Perform Recursion, if its a Directory
                logger.info("Visiting Dir: "+cFile.getFileObj().getName());

                /*if(cFile.getFileObj().getName().startsWith(".")) {//if its invalid directory
                    logger.warn("Skipped Dir..(bcoz of error in its Name)");
                } else*/ 
                
                if(Utility.containsIgnoreCase(excPathList,strPath)) {
                    logger.info("Skip the Excluded Dir: " + strPath);
                    
                } else {
                    LinkedList<String> cFileList =   cFile.getStrList();
                    for(int i=0; i<cFileList.size(); i++) {
                        String tempPath =   thisDocDestPath + "/" + cFile.getFileObj().getName();
                        this.walkFileTree(cFileList.get(i), excPathList, isRecursive, tempPath);
                    }
                }
            } else {//if its file
                String tempPath =   thisDocDestPath + "/" + cFile.getFileObj().getName();
                logger.info("Visiting File for: "+tempPath);
                CIFSIndexingWalker  cw  =   new CIFSIndexingWalker(execID, syncMode, indexID, deduplicatedVolume);
                cw.visitFile(cFile);
            }
            
        } catch (jcifs.smb.SmbAuthException sae){
            logger.error("Error: "+sae.getMessage());
        } catch (java.io.IOException ie) {
            logger.error("Error: "+ie.getMessage());
        } catch (Exception e) {
            logger.error("Error: "+e.getMessage());
        }
    }
    
}
