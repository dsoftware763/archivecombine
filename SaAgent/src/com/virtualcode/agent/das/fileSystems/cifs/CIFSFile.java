/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.fileSystems.cifs;

import com.virtualcode.agent.das.utils.Utility;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import jcifs.UniAddress;
import jcifs.smb.*;
import org.apache.log4j.Logger;

/**
 *
 * @author Yawarx
 */
public class CIFSFile
{
    private UniAddress domain;
    private NtlmPasswordAuthentication authentication;
    private final SmbFile fileObj;
    private final long actualLastAccessTime;
    private static Logger logger    =   Logger.getLogger(CIFSFile.class);

    public CIFSFile(String path) throws IOException {
        jcifs.Config.setProperty("jcifs.smb.client.disablePlainTextPasswords", "false");//allow the PlainTextPassword Authentication
        this.fileObj    =   new SmbFile(path, authentication);
        this.actualLastAccessTime	=	this.fileObj.getLastAccess();
    }
    
    public void resetLastAccessTime() throws SmbException {
    	if(this.fileObj!=null && 
    			this.fileObj.exists() && 
    			!this.fileObj.isDirectory() && 
    			this.actualLastAccessTime>0) {
    		
            /** DONT SET the Last Access Time of CIFS files.. to avoid Write operations
             * Write operation on File, changes the FLAGS (LAPO) on file Headers...
    		this.fileObj.setAccessTime(this.actualLastAccessTime);
             * */
    	}
    }

    public void login(String address, String username, String password) throws Exception {

        setDomain(UniAddress.getByName(address));
        setAuthentication(new NtlmPasswordAuthentication(address, username, password));
        SmbSession.logon(getDomain(), authentication);
    }
    
    public LinkedList<String> getStrList() throws Exception {
        LinkedList<String> fList = new LinkedList<String>();
        SmbFile[] fArr = this.fileObj.listFiles();

        for(int a = 0; a < fArr.length; a++) {
            fList.add(fArr[a].getPath());
            //System.out.println(fArr[a].getPath());
        }

        return fList;
    }

    public boolean checkDirectory() throws Exception {
        if(!this.fileObj.exists()) {
            logger.info(this.fileObj.getName() + " not exist");
            return false;
        }

        if(!this.fileObj.isDirectory()) {
            logger.info(this.fileObj.getName() + " not a directory");
            return false;
        }

        return true;
    }

    public UniAddress getDomain() {
        return domain;
    }

    public void setDomain(UniAddress domain) {
        this.domain = domain;
    }

    public NtlmPasswordAuthentication getAuthentication() {
        return authentication;
    }

    public void setAuthentication(NtlmPasswordAuthentication authentication) {
        this.authentication = authentication;
    }
    
    public SmbFile getFileObj() {
        return this.fileObj;
    }
}
