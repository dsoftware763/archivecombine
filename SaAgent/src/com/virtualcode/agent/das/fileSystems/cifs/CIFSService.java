/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.fileSystems.cifs;

import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.dataArchiver.CIFSWalker;
import com.virtualcode.agent.das.dataArchiver.FileExecutors;
import com.virtualcode.agent.das.dataArchiver.JobStarter;
import com.virtualcode.agent.das.fileSystems.FileServiceInterface;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import org.apache.log4j.Logger;

/**
 *
 * @author se7
 */
public class CIFSService implements FileServiceInterface {
    
    private Logger logger = null;
    private String execID   =   null;
    private String syncMode =   null;
    private boolean isMainRootFolder    =   true;
    private String activeActionType =   null;
    private String indexID  =   null;
    private boolean deduplicatedVolume=false;
    
    public CIFSService(String execID, String syncMode, String activeActionType, String indexID, boolean deduplicateVolume) {
        this.execID =   execID;
        this.syncMode   =   syncMode;
        logger  =   LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL, execID);
        this.isMainRootFolder   =   true;
        this.activeActionType=  activeActionType;
        this.indexID    =   indexID;
        this.deduplicatedVolume =   deduplicateVolume;
    }
    
    public void walkFileTree(String strPath, ArrayList<String> excPathList, boolean isRecursive, String thisDocDestPath) {
        
        try {
            //IF interupted by user than, Stop the Recursive Walk of Files
            //pause and resume
            Job aJobDetail = FileExecutors.currentExecs.get(this.execID);
            if(JobStarter.isInterupted(this.execID, null)&& (!aJobDetail.isScheduled() ||(aJobDetail.getJobStatus() != null && aJobDetail.getJobStatus().getStatus().equals("PAUSED")))) {

                logger.debug("File/Dir Walker is being stopped for "+this.execID);
                return;
            }
            
            CIFSFile cFile   =   new CIFSFile(strPath);
            //cFile.login("10.1.165.15", "mazhar", "newbuild");


            if(cFile.getFileObj().isDirectory() && isRecursive) {//Perform Recursion, if its a Directory
                //logger.debug("Visiting Dir: "+cFile.getFileObj().getName());

                /*if(cFile.getFileObj().getName().startsWith(".")) {//if its invalid directory
                    logger.warn("Skipped Dir..(bcoz of error in its Name)");
                } else*/ 
                
                if(Utility.containsIgnoreCase(excPathList,strPath)) {
                    logger.debug("Skip the Excluded Dir: " + strPath);
                    
                } else {
                    
                    boolean hasToAppendParent	=	true;//to create appropriate heirarchy
                    if(this.isMainRootFolder) {
                        hasToAppendParent	=	false;
                    }
                    this.isMainRootFolder   =false;//set it to false for this Instance (Bcoz only ONE folder can be a root folder)

                    LinkedList<String> cFileList =   cFile.getStrList();
                    for(int i=0; i<cFileList.size(); i++) {//iterate for all children of the folder
                        String tempPath =   null;
                    	if(hasToAppendParent) {//append the current folder name, in preceeding folder heirarchy	
                            tempPath    =   new String((thisDocDestPath + "/" + cFile.getFileObj().getName()).getBytes("UTF-8"));
                        
                        } else {//dont append root folder again in DestPath
                            tempPath    =   thisDocDestPath;
                        }
                        this.walkFileTree(cFileList.get(i), excPathList, isRecursive, tempPath);
                    }
                }
            } else {//if its file
                String tempPath =   new String((thisDocDestPath + "/" + cFile.getFileObj().getName()).getBytes("UTF-8"));
                //logger.debug("Visiting File for location: "+tempPath);
                CIFSWalker  cw  =   new CIFSWalker(execID, syncMode, logger, tempPath, activeActionType, indexID, deduplicatedVolume,strPath);
                cw.visitFile(cFile);
            }
            
        } catch (jcifs.smb.SmbAuthException sae){
            logger.error("Error: "+sae.getMessage());
            LoggingManager.printStackTrace(sae, logger);
            
        } catch (java.io.IOException ie) {
            logger.error("Error: "+ie.getMessage());
            LoggingManager.printStackTrace(ie, logger);
            
        } catch (Exception e) {
            logger.error("Error: "+e.getMessage());
            LoggingManager.printStackTrace(e, logger);
        }
    }
    
    
    /*
    public String createInternetShortcut(String name, String target) throws Exception {
        
        //Path p = FileSystems.getDefault().getPath(name);
        
        
        target = Utility.GetProp("DASUrl") + target;
                
        String icon = Utility.GetIcon(name);
        
        name = name.replace(".", "") + ".url";
        
        FileWriter fw = new FileWriter(name);
        fw.write("[InternetShortcut]\n");
        fw.write("URL=" + target + "\n");
        
        if (!icon.isEmpty()) {
            fw.write("IconFile=" + icon + "\n");
            fw.write("IconIndex=0" + "\n");
        }
        
        fw.flush();
        fw.close();
        
        if (Utility.GetProp("DeleteFiles").equals("1"))  {
            //Files.delete(p);
            System.out.println("yawarz - Write Here to Del Smb File...");
        }
         
        fw = null;
        icon = name = null;

        return target;
    }
    */
}
