/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.fileSystems.cifs;

import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.dataArchiver.TaskKPI;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.utils.Utility;
import com.virtualcode.agent.das.utils.AuthorizationUtil;
import com.vcsl.sa.Acl;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.FileStoreUtility;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.util.Date;
import java.util.UUID;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import jcifs.smb.ACE;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SID;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;
import org.apache.log4j.Logger;

/**
 *
 * @author Abbas
 */
public class CIFSTask implements FileTaskInterface {

    private final CIFSFile cFile;
    private String repoPath="";
    private UUID uid = null;
    private String encodedString = null;
    protected TaskKPI taskKpi = new TaskKPI();
    private String execID   =   null;
    private String syncMode =   null;
    private String calculatedHash   =   null;
    private Logger logger   =   null;
    private final int SMB_OFFLINE =   0x1000;
    private final int ATTR_OFFLINE = 4096;
    private final int SMB_REPARSE_POINT  =   0x400;
    private final int ATTR_REPARSE_POINT =   1024;
    private String thisDocDestPath  =   null;
    private String activeActionType =   null;
    protected String indexID	=	"-1";//proper indexID only be used in IndexingServices
    
    private boolean deduplicatedVolume=false;

    private String strPath;
    
    public void setIndexID(String indexID) {
        this.indexID    =   indexID;
    }
    
    public String getIndexID() {
        return indexID;
    }
    
    public String getExecID() {
        return execID;
    }
    
    public CIFSFile getCIFSFile() {
        return this.cFile;
    }
    
    public CIFSTask(CIFSFile cFile, String execID, String syncMode, Logger logger, String docDestPath,
            String activeActionType, boolean deduplicatedVolume,String strPath) throws IOException {
        this.logger =   logger;
        
        if(uid == null)
            uid = UUID.randomUUID();
        this.cFile    =   cFile;
        this.execID =   execID;
        this.syncMode   =   syncMode;
        this.thisDocDestPath=   (docDestPath!=null)?docDestPath:"NULL";
        this.activeActionType   =   activeActionType;
        
        this.deduplicatedVolume=    deduplicatedVolume;
        this.strPath = strPath;
    }

    public boolean isDeduplicatedVolume() {
        return deduplicatedVolume;
    }

    public String getSyncMode() {
        return this.syncMode;
    }
    
    public String getDestPath() {
        return null;    //This is required in case of DataExport in RepoTask.class only
    }
    
    public String getDocName() {
        return this.getCIFSFile().getFileObj().getName();
    }
    
    public String getEncodedString() {
        return encodedString;
    }

    public void setEncodedString(String encodedString) {
        this.encodedString = encodedString;
    }

    public String getActiveActionType() {
        return activeActionType;
    }

    public String getSecureRepoPath() {
        return repoPath;
    }

    public void setSecureRepoPath(String repoPath) {
        this.repoPath = repoPath;
    }

    public UUID getUid() {
        return uid;
    }

    public void setUid(UUID uid) {
        this.uid = uid;
    }
    
    public TaskKPI getTaskKpi() {
        return taskKpi;
    }

    public void setTaskKpi(TaskKPI taskKpi) {
        this.taskKpi = taskKpi;
    }
    
    public Date getLastAccessedDate() {
        return new Date(this.cFile.getFileObj().getLastAccess());
    }
    
    public Date getCreationDate() {
        Date   date =   null;
        try {
            date    =   new Date(this.cFile.getFileObj().createTime());
            
        } catch (SmbException se){
            System.out.println("Error: " + se.getMessage());
            date    =   null;
        }
        return date;
    }
    
    public Date getLastModifiedDate() {
        return new Date(this.cFile.getFileObj().getLastModified());
    }
    
    /*
     * Its a memory intensive method to resolve SID of owner
     */
    public String getOwner() {
        String owner    =   AuthorizationUtil.resolveSIDValue(logger, this.getPathStr(), this.cFile.getFileObj(), this.getUid());
        
        if(owner==null || "".equals(owner.trim()) || owner.isEmpty()) {
            owner   =   "unknown";
        }
        
        return owner;
    }
    
    public String getURIMapping() {
        
        //Remove the string before @ SIGN
        String nodeHeirarchy       =   thisDocDestPath.replace(":", "").replace("\\\\", "").replace("\\", "/");
//        if(nodeHeirarchy!=null && nodeHeirarchy.startsWith("smb://"))
//            nodeHeirarchy.substring(thisDocDestPath.indexOf("@")+1);
//        nodeHeirarchy       =   nodeHeirarchy.replace(":", "").replace("\\\\", "").replace("\\", "/");

        String result =  nodeHeirarchy + "/" + getPathStr().replace(this.strPath,"").replace("\\", "/");
        return result;
        //return nodeHeirarchy;
    }
    
    public String getPathStr() {
        return this.cFile.getFileObj().getPath();
    }
    
    public String getNodeHeirarchy() {
        String nodeHeirarchy = this.getPathStr();
        
        //Remove the string before @ SIGN
        if(nodeHeirarchy!=null && nodeHeirarchy.startsWith("smb://"))
            nodeHeirarchy       =   nodeHeirarchy.substring(nodeHeirarchy.indexOf("@")+1);
        nodeHeirarchy       =   nodeHeirarchy.replace(":", "").replace("\\\\", "").replace("\\", "/");
        
        return nodeHeirarchy;
    }
    
    public DataHandler getDataHandler() throws IOException {
        
        DataSource dataSource  =   null;
        try {
            
            InputStream is  =   this.cFile.getFileObj().getInputStream();
            dataSource      =   new CIFSDataSource(is);
            
        } catch  (Exception se) {
            throw new IOException("Cant read SmbFileInputStream for " + this.getPathStr());
        }
        
        return new DataHandler(dataSource);
    }
    
    public long getBytesCount() throws Exception {
        return this.cFile.getFileObj().length();
    }
    
    public boolean isDirectory() throws Exception {
        return this.getCIFSFile().getFileObj().isDirectory();
    }
    
    public boolean isHidden() throws Exception {
        return this.getCIFSFile().getFileObj().isHidden();
    }
    
    public boolean isReadOnly() throws Exception {
        return !this.getCIFSFile().getFileObj().canWrite();
    }
    
    public boolean createInternetShortcut(String shortCutAt, String shortCutTo,Job currentJob) throws Exception {
        
        boolean output  =   true;
        String icon = Utility.GetIcon(shortCutAt);
        
        shortCutAt = shortCutAt + ".url";
        
        //System.out.println("creating ShortCut at: " + shortCutAt);
        SmbFile shortCut        =   new SmbFile(shortCutAt);
        SmbFileOutputStream fw  =   new SmbFileOutputStream(shortCut);
        //fw.write("Yawar Bukhari".getBytes());
        
        //Write the URL and Icon etc on STUB
        fw.write("[InternetShortcut]\n".getBytes());
        
        //if("NA".equalsIgnoreCase(Utility.getLocalArchivePath())) {
        if(currentJob.getStagingJob()==0) {
            shortCutTo = Utility.getDASUrl() + shortCutTo;
            fw.write(("URL=" + shortCutTo + "\n").getBytes());
            
        } else {//case of local archiving stubs
            //fw.write(("URL=file://" + new String(shortCutTo.getBytes("UTF-8")) + "\n").getBytes());
            String[] shortCutArr = shortCutTo.split("[|]");
            if(shortCutArr!=null && shortCutArr.length==2) {
                fw.write(("URL=file://" + shortCutArr[0] + "\n").getBytes());//set Local drive path
                fw.write(("REMOTE_URL=" + shortCutArr[1] + "\n").getBytes());//set remote SA path
            } else {
                output  =   false;
                throw new Exception("Unexpected DASUrl to generate stub...");
            }
        }
        if (!icon.isEmpty()) {
            fw.write(("IconFile=" + icon + "\n").getBytes());
            fw.write(("IconIndex=0" + "\n").getBytes());
        }
        
        fw.flush();
        fw.close();
        
        //Apply the MetaData of Orignal File on STUB
        try {
            shortCut.setCreateTime(this.getCIFSFile().getFileObj().createTime());
            shortCut.setLastModified(this.getCIFSFile().getFileObj().getLastModified());
        } catch(Exception e) {
            output   =   false;
            e.printStackTrace();
        }
        
        //Delete the Original file, if have to contain only Stub
        if (Utility.getAgentConfigs().getDeleteFiles().intValue()==1) 
            this.getCIFSFile().getFileObj().delete();
         
        fw = null;
        icon = null;
        shortCut = null;

        return output;
    }
    
    public Acl getACL(String documentPath) throws IOException {
        Acl acl =   null;

        try{
            //Get the Original Security ACL
            ACE[] aceList    =   this.getCIFSFile().getFileObj().getSecurity();

            Integer mapShareInfo =   Utility.getAgentConfigs().getMapShareInfo();//"mapShareInfo");
            if (mapShareInfo!=null && mapShareInfo.intValue()==1) {//if has to retrieve the ShareSecurity from file
                String mapFilePath  =   Utility.getAgentConfigs().getMapFilePath();//GetProp("mapFilePath");

                //System.out.println("Full doc path: "+documentPath);
                String entryFor     =   documentPath.substring(0, Utility.nthOccurrence(documentPath, '/', 1));//
                //System.out.println("Get entry from mapFilePath: "+entryFor);
                String mapingEntry  =   Utility.getMapingEntry(mapFilePath, entryFor);

                //if(mapingEntry!=null) {
                //Transform the original ACL into customized ACL
                acl =   AuthorizationUtil.transformACE(aceList, mapingEntry);
                //}
            } else {//if get original ShareSecurity, instead of maping file...
                //Get the Share Security ACL
                ACE[] aceShareList    =  this.getCIFSFile().getFileObj().getShareSecurity(true);

                //Transform the original ACL into customized ACL
                acl =   AuthorizationUtil.transformACE(aceList, aceShareList);
            }

            return acl;
        }catch(SmbException e){
            logger.error(getUid() + " error " +  e.getMessage());
            return acl;
        }catch(Exception e){
            logger.error(getUid() + " error " +  e.getMessage());
            return acl;
        }

    }
    
    public boolean deleteOriginalFile() {
        boolean output  =   true;
        try {
            this.cFile.getFileObj().delete();
        } catch (SmbException se) {
            output  =   false;
            System.err.println(se.getMessage());
        }
        return output;
    }
    
    public void closeCurrentTask(boolean syncLastAccessTime) {
    	try {
            if(syncLastAccessTime && this.getCIFSFile()!=null)
                this.getCIFSFile().resetLastAccessTime();
    	} catch (SmbException se) {
            System.out.println(this.getExecID() +" Task NOT closed properly: "+se.getMessage());
    	}
    }
    
    public String getCalculatedHashID() {
        if(this.calculatedHash==null && this.getCIFSFile().getFileObj()!=null)
            calculatedHash =   FileStoreUtility.getHashIDForCIFS(this.getCIFSFile().getFileObj());
        
        return this.calculatedHash;
    }
    
    public boolean isOfflineFile() throws SmbException {
        boolean result  =   false;
        if((this.getCIFSFile().getFileObj().getAttributes() & SMB_OFFLINE) == ATTR_OFFLINE){
            result  =   true;
        }
        return result;
    }
    
    public boolean isReparsePoint() throws SmbException {
        boolean result  =   false;
        if((this.getCIFSFile().getFileObj().getAttributes() & SMB_REPARSE_POINT) == ATTR_REPARSE_POINT) {
            result  =   true;
        }
        return result;
    }

    public String getStrPath()
    {
        return  this.strPath;
    }
    public String getThisDocDestPath()
    {
        return  this.thisDocDestPath;
    }
}
