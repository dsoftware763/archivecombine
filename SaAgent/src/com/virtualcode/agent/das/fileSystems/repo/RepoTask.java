/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.fileSystems.repo;

import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.dataArchiver.TaskKPI;
import com.vcsl.sa.export.Document;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.vcsl.sa.Acl;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;
import javax.activation.DataHandler;

/**
 *
 * @author Abbas
 */
public class RepoTask implements FileTaskInterface {

    private final Document doc;
    private String thisDocDestPath="";
    private UUID uid = null;
    private String encodedString = null;
    private TaskKPI taskKpi = new TaskKPI();
    private String execID   =   null;
    private String activeActionType =   null;
    
    private boolean deduplicatedVolume=false;

    private String strPath;
    
    public String getExecID() {
        return execID;
    }
    
    public Document getDocument() {
        return this.doc;
    }
    
    public String getDocName() {
        return this.doc.getTitle();
    }
    
    public boolean isDeduplicatedVolume() {
        return deduplicatedVolume;
    }

    public RepoTask(Document doc, String thisDocDestPath, String execID, String activeActionType, boolean deduplicatedVolume,String strPath) {
        if(uid == null)
            uid = UUID.randomUUID();
        this.doc        =   doc;
        this.thisDocDestPath   =   thisDocDestPath;
        this.execID =   execID;
        this.activeActionType   =   activeActionType;
        this.deduplicatedVolume=    deduplicatedVolume;
        this.strPath= strPath;
    }
    
    public String getSyncMode() {//useless method for Export etc (only required for archiving)
        return null;
    }

    public String getActiveActionType() {
        return activeActionType;
    }
    
    public String getEncodedString() {
        return encodedString;
    }

    public void setEncodedString(String encodedString) {
        this.encodedString = encodedString;
    }

    public String getDestPath() {
        return this.thisDocDestPath;
    }

    public String getSecureRepoPath() {
        return this.doc.getSecureURL();
    }

    public void setSecureRepoPath(String repoPath) {
        this.doc.setSecureURL(repoPath);
    }

    public UUID getUid() {
        return uid;
    }

    public void setUid(UUID uid) {
        this.uid = uid;
    }
    
    public TaskKPI getTaskKpi() {
        
        /* As the Statistics module is Mapping Export Stats to Archive Stats,
         * So copy Export specific Values into Archive Variables (in case of
         * Export Job), to save correct Stats into DB.
         * And this change is applicable only in RepoTask, bcoz its related to ExportJob
         */
        taskKpi.isArchived      =   (taskKpi.isExported) ? "TRUE" : "NO";
        taskKpi.failedArchiving =   taskKpi.failedExporting;
        
        return taskKpi;
    }

    public void setTaskKpi(TaskKPI taskKpi) {
        this.taskKpi = taskKpi;
    }
    
    public Date getLastAccessedDate() {
        long accessedOn =   Long.parseLong(this.doc.getAccessedOn());
        return new Date(accessedOn);
    }
    
    public Date getCreationDate() {
        long creationDate   =   Long.parseLong(this.doc.getModifiedOn());
        return new Date(creationDate);
    }
    
    public Date getLastModifiedDate() {
        long modifDate  =   Long.parseLong(this.doc.getModifiedOn());
        return new Date(modifDate);
    }
    
    public String getOwner() {
        return this.doc.getAuthor();
    }
    
    public String getURIMapping() {
        return null;// only required in case of CIFSTask.class and NTFSTask.class
    }
    
    public String getPathStr() {
        return this.doc.getUrl();
    }
    
    public String getNodeHeirarchy() {
        return this.getPathStr();//in case of Export, its same as passed from Repo
    }
    
    public DataHandler getDataHandler() throws IOException {
        /*
        DataSource dataSource  =   null;
        try {
            
            InputStream is  =   this.cFile.getFileObj().getInputStream();
            dataSource      =   new CIFSDataSource(is);
            
        } catch  (Exception se) {
            throw new IOException("Cant read SmbFileInputStream for " + this.getPathStr());
        }
        
        return new DataHandler(dataSource);
         * 
         */
        return null;
    }
    
    public long getBytesCount() throws Exception {
        long temp   =   0;
        if(this.doc!=null) 
            temp    =   this.doc.getSize().longValue();
        return temp;
    }
    
    public boolean isDirectory() throws Exception {
        return this.doc.isFolderYN();
    }
    
    public boolean isHidden() throws Exception {
        return false;//repo nodes can't be Hidden
    }
    
    public boolean isReadOnly() throws Exception {
        return false;//repo nodes, can't be readonly
    }
    
    public boolean createInternetShortcut(String shortCutAt, String shortCutTo,Job currentJob) throws Exception {
        /*
        shortCutTo = Utility.GetProp("DASUrl") + shortCutTo;
                
        String icon = Utility.GetIcon(shortCutAt);
        
        shortCutAt = shortCutAt + ".url";
        
        //System.out.println("creating ShortCut at: " + shortCutAt);
        SmbFile shortCut        =   new SmbFile(shortCutAt);
        SmbFileOutputStream fw  = new SmbFileOutputStream(shortCut);
        //fw.write("Yawar Bukhari".getBytes());
        
        //System.out.println("creating ShortCut to: "+shortCutTo);
        fw.write("[InternetShortcut]\n".getBytes());
        fw.write(("URL=" + shortCutTo + "\n").getBytes());
        
        if (!icon.isEmpty()) {
            fw.write(("IconFile=" + icon + "\n").getBytes());
            fw.write(("IconIndex=0" + "\n").getBytes());
        }
        
        fw.flush();
        fw.close();
        
        if (Utility.GetProp("DeleteFiles").equals("1")) 
            this.getCIFSFile().getFileObj().delete();
         
        fw = null;
        icon = null;
        shortCut = null;

        return shortCutTo;
         *
         */
        return true;
    }
    
    public Acl getACL(String documentPath) throws IOException {
        Acl acl =   null;
        /*
        //Get the Original Security ACL
        ACE[] aceList    =   this.getCIFSFile().getFileObj().getSecurity();
        
        String mapShareInfo =   Utility.GetProp("mapShareInfo");
        if ("1".equals(mapShareInfo)) {//if has to retrieve the ShareSecurity from file
            String mapFilePath  =   Utility.GetProp("mapFilePath");
            
            //System.out.println("Full doc path: "+documentPath);
            String entryFor     =   documentPath.substring(0, Utility.nthOccurrence(documentPath, '/', 1));//
            //System.out.println("Get entry from mapFilePath: "+entryFor);
            String mapingEntry  =   Utility.getMapingEntry(mapFilePath, entryFor);
            
            //if(mapingEntry!=null) {
                //Transform the original ACL into customized ACL
                acl =   AuthorizationUtil.transformACE(aceList, mapingEntry);
            //}
        } else {//if get original ShareSecurity, instead of maping file...
            //Get the Share Security ACL
            ACE[] aceShareList    =   this.getCIFSFile().getFileObj().getShareSecurity(true);
            
            //Transform the original ACL into customized ACL
            acl =   AuthorizationUtil.transformACE(aceList, aceShareList);
        }
        */
        return acl;
    }
    
    public boolean deleteOriginalFile() {
        return false;//RepoTask is not to delete file (bcoz its dealing with Nodes..)
    }
    
    public void closeCurrentTask(boolean syncLastAccessTime) {
    	/* mainly being used to reset the LastAccessTime of CIFS files
    	 * try {
    		this.getCIFSFile().resetLastAccessTime();
    	} catch (SmbException se) {
    		logger.warn(this.getExecID() +" Task NOT closed propertly");
    	}*/
    }
    
    public String getCalculatedHashID() {
        //this method is not required for Export/Repo
        //its only required in CIFSTask and NTFSTask for dedup verfication in archiving purpose
        return this.doc.getId();
    }
    
    public boolean isOfflineFile() throws Exception {
        boolean result  =   false;
        
        //Offline flag is required for CIFS/NTFS files
        
        return result;
    }
    
    public boolean isReparsePoint() throws Exception {
        boolean result  =   false;
        
        //Offline flag is required for CIFS/NTFS files
        
        return result;
    }

    public String getStrPath()
    {
        return  this.strPath;
    }
}
