/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.fileSystems.repo;

import com.virtualcode.agent.das.dataExporter.RepoWalker;
import com.virtualcode.agent.das.fileSystems.FileServiceInterface;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.Utility;
import com.vcsl.sa.export.Document;
import com.virtualcode.agent.das.dataExporter.ExportStarter;
import executors.DataLayer;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Abbas
 */
public class RepoService implements FileServiceInterface {
    
    Logger logger = null;
    private String secureInfo   =   null;
    //private String destPath     =   null;
    private String execID   =   null;
    private String activeActionType =   null;
    private boolean deduplicatedVolume    =   false;
    
    public RepoService(String secureInfo, String execID, String activeActionType, boolean deduplicatedVolume) {
        this.secureInfo =   secureInfo;
        //this.destPath   =   destPath;
        this.execID =   execID;
        this.activeActionType   =   activeActionType;
        this.deduplicatedVolume=  deduplicatedVolume;
        logger  =   LoggingManager.getLogger(LoggingManager.EXP_ACTIVITY_DETAIL,execID);
    }
    
    public void walkFileTree(String strPath, ArrayList<String> excPathList, boolean isRecursive, String destPath) {
        
        try {
            logger.info("getAllChidren from WS invoked for "+strPath);
            List<Document> docsList    =   DataLayer.getAllChildren(strPath, this.secureInfo);
            String thisLevelFiles   =   destPath;
            
            if(docsList==null || docsList.size()<=0) {
                //its alraming/warning if the child content NOT exist for root path...
                logger.warn("Childnodes NOT exist for "+strPath);
            }
            
            for(int i=0; i<docsList.size(); i++) {
                Document doc    = docsList.get(i);
                if(doc.isFolderYN() && isRecursive) {//if have to visit the main root path recursively
                    //logger.info("Visiting Dir: "+doc.getUrl());
                    String temp =   destPath + "/" + doc.getTitle();//.replaceAll("[.]", "_");
                    this.recursiveFileWalker(doc, excPathList, temp);

                } else {
                    this.visitFile(doc, thisLevelFiles);
                }
            }
        }catch (Exception e) {
            logger.error(e.getMessage());
        }
    }
    
    private void recursiveFileWalker(Document parentDoc, ArrayList<String> excPathList, String thisDocDestPath) throws Exception {
        List<Document> docsList    =   DataLayer.getAllChildren(parentDoc.getUrl(), this.secureInfo);
        
        if(docsList==null || docsList.size()<=0) {
            logger.info("subfolders not exist for "+parentDoc.getUrl());
        }
        
        for(int i=0; i<docsList.size(); i++) {
            Document doc    = docsList.get(i);
            if(doc.isFolderYN()) {
                //logger.info("Visiting Dir: "+doc.getUrl());
                if(Utility.containsIgnoreCase(excPathList, doc.getUrl()))
                    logger.info("Skip the Excluded Dir: " + doc.getUrl());
                
                else {    //if current is a folder and not in Excluded list
                    String temp =   thisDocDestPath + "/" + doc.getTitle();//.replaceAll("[.]", "_");
                    this.recursiveFileWalker(doc, excPathList, temp);
                }

            } else {
                this.visitFile(doc, thisDocDestPath);
            }
        }
    }
    
    private void visitFile(Document doc, String thisDocDestPath) {
        if(ExportStarter.isInterupted(this.execID)) {
            logger.info("File Visitor is being stopped for "+this.execID);
            return;
        }
        //logger.info("Visiting File "+doc.getTitle() + ", of size "+doc.getSize());
        RepoWalker  rw  =   new RepoWalker(thisDocDestPath, execID, activeActionType, deduplicatedVolume,null);
        rw.visitFile(doc);
    }
    
    /*
    public String createInternetShortcut(String name, String target) throws Exception {
        
        //Path p = FileSystems.getDefault().getPath(name);
        
        
        target = Utility.GetProp("DASUrl") + target;
                
        String icon = Utility.GetIcon(name);
        
        name = name.replace(".", "") + ".url";
        
        FileWriter fw = new FileWriter(name);
        fw.write("[InternetShortcut]\n");
        fw.write("URL=" + target + "\n");
        
        if (!icon.isEmpty()) {
            fw.write("IconFile=" + icon + "\n");
            fw.write("IconIndex=0" + "\n");
        }
        
        fw.flush();
        fw.close();
        
        if (Utility.GetProp("DeleteFiles").equals("1"))  {
            //Files.delete(p);
            System.out.println("yawarz - Write Here to Del Smb File...");
        }
         
        fw = null;
        icon = name = null;

        return target;
    }
    */
}
