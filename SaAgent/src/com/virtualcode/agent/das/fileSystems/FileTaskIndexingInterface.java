/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.fileSystems;

/**
 *
 * @author HP
 */
public interface FileTaskIndexingInterface extends FileTaskInterface {
    public String getIndexID();
    public String getReadableContent();
    public String getMetaData();
    public boolean isStub() throws Exception;
}
