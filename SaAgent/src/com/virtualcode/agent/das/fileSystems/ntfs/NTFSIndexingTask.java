/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.fileSystems.ntfs;

import com.virtualcode.agent.das.dataArchiver.TaskKPI;
import com.virtualcode.agent.das.dataIndexer.AnalysisManager;
import com.virtualcode.agent.das.fileSystems.FileTaskIndexingInterface;
import com.virtualcode.agent.das.logging.LoggingManager;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;

import org.apache.log4j.Logger;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.html.HtmlParser;
import org.apache.tika.parser.microsoft.ooxml.OOXMLParser;
import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;

/**
 *
 * @author Abbas
 */
public class NTFSIndexingTask extends NTFSTask implements FileTaskIndexingInterface {

    private String indexID	=	"-1";//proper indexID only be used in FSIndexingServices
    private boolean isParsedByTika	=	false;
    private String metaData	=	"";
    private Logger logger   =   null;
    
    public NTFSIndexingTask(Path p,BasicFileAttributes att,String execID,String syncMode, String indexID, Logger logger, boolean deduplicatedVolume){
        super(p,att,execID,syncMode,null,null,deduplicatedVolume,null);
        this.logger =   logger;
        this.indexID   =   indexID;
    }
    
    public String getIndexID() {
        return indexID;
    }
    
    @Override
    public TaskKPI getTaskKpi() {
        
        /* As the Statistics module is Mapping Export Stats to Archive Stats,
         * So copy Export specific Values into Archive Variables (in case of
         * Export Job), to save correct Stats into DB.
         * And this change is applicable only in RepoTask & NTFSIndexingTask, bcoz its related to ExportJob & IndexingJob
         */
        taskKpi.isArchived      =   (taskKpi.isExported) ? "TRUE" : "NO";
        taskKpi.failedArchiving =   taskKpi.failedExporting;
        
        return taskKpi;
    }
    
    
    public String getReadableContent() {//can return NULL

        String content	=	null;
        String fileName	=	this.getDocName();//getCIFSFile().getFileObj().getName();

        if(!isParsedByTika && fileName!=null) {			
            isParsedByTika	=	true;//set to TRUE, to not parse on each call of this function....

            try {
                InputStream is = this.getDataHandler().getInputStream();//getCIFSFile().getFileObj().getInputStream();//new FileInputStream(filePath);
                //ContentHandler contenthandler = new BodyContentHandler();
                ContentHandler contenthandler   =   new BodyContentHandler(10000000);
                Metadata metadata = new Metadata();

                Parser parser = null; 
                if(fileName.endsWith("pdf")) {//case of PDF file 
                    parser  =   new PDFParser();

                } else if(fileName.endsWith("html") || fileName.endsWith("htm")) {
                    parser  =   new HtmlParser();

                } else if(fileName.endsWith("pdf")) {
                    parser  =   new PDFParser();

                } else if(fileName.endsWith("xlsx")) {
                    parser  =   new OOXMLParser();

                } else {
                    parser  =   new AutoDetectParser();
                }

                parser.parse(is, contenthandler, metadata, new ParseContext());
                content	=	contenthandler.toString();

                ////print the MetaData in log file
                listMetadata(metadata);

            } catch (Exception ex) {
                    logger.error(this.getExecID() +" Unable to fetch textual content");
                    LoggingManager.printStackTrace(ex, logger);
            }

        } else {
            logger.error("Invalid path to fetch text by Tika");
        }

        return content;
    }

    public String getMetaData() {
            return metaData;
    }

    private void listMetadata(Metadata metadata) throws Exception {
    	
        try {
            for (String key : metadata.names()) {
                if("X-Parsed-By".equals(key)) {
                    continue;
                }
                String val=metadata.get(key);
                metaData	+=	"" + key + ": "+val+" $";
                //logger.info("Metadata \'" + key + "\': "+ val);
                
                //prepare the KeysList
                AnalysisManager.addMetaKeys(key);//add it into keys list (if NOT already exists)
            }
        } catch (  Exception e) {
            logger.error(e.toString());
            LoggingManager.printStackTrace(e, logger);
        }
    }
    
    public boolean isStub() throws Exception {
    	boolean output	=	false;
	    if(!this.isDeduplicatedVolume() && this.isReparsePoint()) {//if not a dedup volumne than check only for reparse point, than skip the file
	        logger.info(this.getUid() + " Reparsepoint is set to true in a nonDeDupVolume.");
	        output	=	true;
	    }
	
	    if(this.isDeduplicatedVolume() && this.isReparsePoint() && this.isOfflineFile()) {//reparse point and Offline flags
	        logger.info(this.getUid() + " Reparsepoint & OfflineFlag is set to true in Win DeDupVolume.");
	        output	=	true;
	    }
	    
        if(this.isOfflineFile()) {//Dont invoke "SAFileStubbing".equalsIgnoreCase(task.getOwner())
            logger.info(this.getUid() + " App generated offline Stub (i.e. shortcut file).");
            output	=	true;
        }
	    
        return output;
    }
}
