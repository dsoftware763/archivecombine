/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.fileSystems.ntfs;

import com.vcsl.sa.Ace;
import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.dataArchiver.TaskKPI;
import com.virtualcode.agent.das.fileSystems.FileTaskInterface;
import com.virtualcode.agent.das.utils.GZipFile;
import com.virtualcode.agent.das.utils.LocalMachineUtil;
import com.virtualcode.agent.das.utils.Utility;
import com.vcsl.sa.Acl;
import com.virtualcode.agent.das.utils.FileStoreUtility;

import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Date;
import java.util.UUID;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.util.ByteArrayDataSource;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.AclEntry;
import java.nio.file.attribute.AclEntryPermission;
import java.nio.file.attribute.AclFileAttributeView;
import java.nio.file.attribute.GroupPrincipal;
import java.nio.file.attribute.UserPrincipal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Abbas
 */
public class NTFSTask implements FileTaskInterface {

    private final Path path;
    private BasicFileAttributes attributes;
    private String repoPath="";
    private UUID uid = null;
    private String encodedString = null;
    protected TaskKPI taskKpi = new TaskKPI();
    private String execID   =   null;
    private String syncMode =   null;
    private String calculatedHash   =   null;
    private String thisDocDestPath  =   null;
    private String activeActionType =   null;
    private String strPath;
    
    private boolean deduplicatedVolume=false;
    private static final long FILE_ATTRIBUTE_OFFLINE        = 0x00001000;
    //private static final long FILE_ATTRIBUTE_REPARSE_POINT  = 0x00000400;//1024 (0x400)
    private static final int FILE_ATTRIBUTE_REPARSE_POINT    = 0x400;
    
    public String getExecID() {
        return execID;
    }
    
    public BasicFileAttributes getAttributes() {
        return attributes;
    }
    
    public Path getPath() {
        return path;
    }
    
    public boolean isDeduplicatedVolume() {
        return deduplicatedVolume;
    }

    public NTFSTask(Path p,BasicFileAttributes att,String execID, String syncMode, String docDestPath,
            String activeActionType, boolean deduplicatedVolume,String strPath){
        if(uid == null)
            uid = UUID.randomUUID();
        this.path = p;
        this.attributes=att;
        this.execID =   execID;
        this.syncMode   =   syncMode;
        this.thisDocDestPath=   (docDestPath!=null)?docDestPath:"";
        this.activeActionType   =   activeActionType;
        this.deduplicatedVolume=    deduplicatedVolume;
        this.strPath = strPath;
    }

    public String getSyncMode() {
        return this.syncMode;
    }

    public String getEncodedString() {
        return encodedString;
    }

    public void setEncodedString(String encodedString) {
        this.encodedString = encodedString;
    }

    public String getActiveActionType() {
        return activeActionType;
    }
    
    public String getDestPath() {
        return null;    //This is required in case of DataExport in RepoTask.class only
    }
    
    public String getSecureRepoPath() {
        return repoPath;
    }

    public String getDocName() {
        return this.path.getFileName().toString();
    }
    
    public void setSecureRepoPath(String repoPath) {
        this.repoPath = repoPath;
    }

    public UUID getUid() {
        return uid;
    }

    public void setUid(UUID uid) {
        this.uid = uid;
    }
    
    public TaskKPI getTaskKpi() {
        return taskKpi;
    }

    public void setTaskKpi(TaskKPI taskKpi) {
        this.taskKpi = taskKpi;
    }
    
    public Date getLastAccessedDate() {
        return new Date(this.attributes.lastAccessTime().toMillis());
    }
    
    public Date getCreationDate() {
        return new Date(this.attributes.creationTime().toMillis());
    }
    
    public Date getLastModifiedDate() {
        return new Date(this.attributes.lastModifiedTime().toMillis());
    }
    
    public String getOwner() {
        String owner    =   "unknown";
        try {
            owner   =   Files.getOwner(this.path).getName();
        }catch(Exception e) {}
        
        return owner;
    }
    
    public String getURIMapping() {
        String nHrchy   =   this.thisDocDestPath.replace(":", "").replace("\\\\", "").replace("\\", "/");
        //Check UNC path is null and empty space
        if(nHrchy != null && !nHrchy.equals("")){
            return nHrchy + "/" + getPathStr().replace(this.strPath,"").replace("\\", "/");
        }else {
            return nHrchy + "/" + this.getNodeHeirarchy();
        }
    }
    
    public String getPathStr() {
        return path.toString();
    }
    
    public String getNodeHeirarchy() {
        //Get the IPAdr of current machine (on which the Agent is running)
        String agentIP  =   LocalMachineUtil.getOwnIPAdr();
        String nHrchy   =   this.getPathStr();
        //try {
        //    nHrchy  =   new String(this.getPathStr().getBytes("UTF-8"));
        //} catch (UnsupportedEncodingException uee) {
        //    System.out.println(uee.getMessage());
        //}
        
        //Prepend the IP of Agent machine in case of NTFS
        nHrchy          =   agentIP + "/" +nHrchy.replace(":", "").replace("\\\\", "").replace("\\", "/");
        
        return nHrchy;
    }
    
    public DataHandler getDataHandler() throws IOException {
        String filePath = this.path.toUri().getPath();
        DataSource dataSource   =   null;
        
        try {
            dataSource = new FileDataSource(filePath);
            //Call compression here
           /* int buffSize =1024;
            GZipFile gZipFile = new GZipFile(null,buffSize);
            FileInputStream inputStream = new FileInputStream(new File(filePath));
            byte[] byteArray = gZipFile.gZipIt(inputStream);

            dataSource = new ByteArrayDataSource(byteArray, "text/plain");*/


        } catch (Exception e) {
            throw new IOException("Cant read FileDataSource "+ filePath);
        }
        return new DataHandler(dataSource);
    }
    
    public long getBytesCount() throws Exception {
        return this.getPath().toFile().length();
    }
    
    public boolean isDirectory() {
        return this.getPath().toFile().isDirectory();
    }
    
    public boolean isHidden() {
        return this.getPath().toFile().isHidden();
    }
    
    public boolean isReadOnly() {
        return !this.getPath().toFile().canWrite();
    }
    
    public boolean createInternetShortcut(String shortCutAt, String shortCutTo,Job currentJob) throws Exception {
        
        boolean output   =   true;
        Path p = FileSystems.getDefault().getPath(shortCutAt);
                        
        String icon = Utility.GetIcon(p.getFileName().toString());
        
        shortCutAt = shortCutAt + ".url";
        
        FileWriter fw = new FileWriter(shortCutAt);
        fw.write("[InternetShortcut]\n");
        
       // if("NA".equalsIgnoreCase(Utility.getLocalArchivePath())) {
        if(currentJob.getStagingJob()==0) {
            shortCutTo = Utility.getDASUrl() + shortCutTo;
            fw.write(("URL=" + shortCutTo + "\n"));//set remote SA path
            
        } else {//case of local archiving stubs
            
            //fw.write(("URL=file://" + new String(shortCutTo.getBytes("UTF-8")) + "\n"));
            String[] shortCutArr = shortCutTo.split("[|]");
            if(shortCutArr!=null && shortCutArr.length==2) {
                fw.write(("URL=file://" + shortCutArr[0] + "\n"));//set Local drive path
                fw.write(("REMOTE_URL=" + shortCutArr[1] + "\n"));//set remote SA path
            } else {
                output  =   false;
                throw new Exception("Unexpected DASUrl to generate stub...");
            }
        }
        
        if (!icon.isEmpty()) {
            fw.write("IconFile=" + icon + "\n");
            fw.write("IconIndex=0" + "\n");
        }
        
        fw.flush();
        fw.close();
        
        //Apply the MetaData of Orignal File on STUB
        try {
            File    shortCut    =   new File(shortCutAt);
            shortCut.setLastModified(this.getAttributes().lastModifiedTime().toMillis());
        } catch(Exception e) {
            output  =   false;
        }
        
        //Delete the Original file, if have to contain only Stub
        if (Utility.getAgentConfigs().getDeleteFiles().intValue()==1) 
            Files.delete(p);
         
        fw = null;
        icon = shortCutAt = null;

        return output;
    }
    
    public Acl getACL(String documentPath) throws IOException {
        
        return getMappedList(getPath());
    }
    private Acl getMappedList(Path path)
    {
        // List of the ACL enum of read permission
        List<AclEntryPermission> readAcl = new ArrayList<AclEntryPermission>();
        readAcl.add(AclEntryPermission.READ_ATTRIBUTES);
        readAcl.add(AclEntryPermission.READ_DATA);
        readAcl.add(AclEntryPermission.SYNCHRONIZE);
        readAcl.add(AclEntryPermission.READ_NAMED_ATTRS);

        AclFileAttributeView acl = Files.getFileAttributeView(path,
                AclFileAttributeView.class);
        //List<String> names = new ArrayList<String>();
        
        Acl vcAcl   =   null;
        if (acl!=null)
        {   
            vcAcl   =   new Acl();//create fresh instance
            try {
                List<AclEntry> aclList = acl.getAcl();
                
                for (AclEntry entry: aclList)
                {
                    //System.out.println(a.principal() + ":");
                    //System.out.println(entry.permissions() + "\n");
                    if (entry.permissions().containsAll(readAcl))
                    {   
                        Ace vcAllowACE  =   new Ace();
                        Ace vcDenyACE   =   new Ace();
                        Ace vcShareAllowACE =   new Ace();
                        Ace vcShareDenyACE  =   new Ace();
                        
                        // Only add principal if it is not a group
                        if (entry.principal() instanceof GroupPrincipal) {
                            
                            vcAllowACE.setSidName(entry.principal().getName());
                            vcAllowACE.setIsAllow(true);
                            vcAllowACE.setSid(entry.principal().toString());
                            
                            //names.add(entry.principal().getName());
                        } else {
                            vcAllowACE.setSidName(entry.principal().getName());
                            vcAllowACE.setIsAllow(true);
                            vcAllowACE.setSid(entry.principal().toString());
                            
                            //names.add(entry.principal().getName());
                        }
                        
                        vcAcl.getAllowAcl().add(vcAllowACE);
                        //vcAcl.getDenyAcl().add(vcDenyACE);
                    }
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return vcAcl;//names.toArray(new String[]{});
    }
    
    public boolean deleteOriginalFile() {
        return this.path.toFile().delete();
    }
    
    public void closeCurrentTask(boolean syncLastAccessTime) {
    	/* mainly being used to reset the LastAccessTime of CIFS files
    	 * try {
    		this.getCIFSFile().resetLastAccessTime();
    	} catch (SmbException se) {
    		logger.warn(this.getExecID() +" Task NOT closed propertly");
    	}*/
    }
    
    public String getCalculatedHashID() {
        if(this.calculatedHash==null && this.path.toFile()!=null)
            calculatedHash =   FileStoreUtility.getHashIDForNIO(this.path.toFile());
        
        return this.calculatedHash;
    }
    
    public boolean isOfflineFile() throws Exception {
        boolean result = false;
        
        int attribute = (int) Files.getAttribute(this.path, "dos:attributes");
        result  =   ((attribute & FILE_ATTRIBUTE_OFFLINE) != 0);
        
        return result;
    }
    
    public boolean isReparsePoint() throws Exception {
        boolean result = false;
        
        //FILE_ATTRIBUTE_REPARSE_POINT
        //1024 (0x400)        
        int attribute = (int) Files.getAttribute(this.path, "dos:attributes");
        result  =   ((attribute & FILE_ATTRIBUTE_REPARSE_POINT) != 0);
        
        return result;
    }

    public String getStrPath()
    {
        return  this.strPath;
    }
    public String getThisDocDestPath()
    {
        return  this.thisDocDestPath;
    }
}
