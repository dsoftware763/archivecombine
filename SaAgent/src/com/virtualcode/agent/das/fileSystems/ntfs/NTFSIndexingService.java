package com.virtualcode.agent.das.fileSystems.ntfs;

import com.virtualcode.agent.das.dataIndexer.NTFSIndexingWalker;
import com.virtualcode.agent.das.fileSystems.FileServiceInterface;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 *
 * @author HP
 */
public class NTFSIndexingService implements FileServiceInterface {
    
    private String execID   =   null;
    private String syncMode =   null;
    private String indexID  =   null;
    private boolean deduplicatedVolume  =   false;
    
    public NTFSIndexingService(String execID, String syncMode, String indexID, boolean deduplicatedVolume) {
        this.execID =   execID;
        this.syncMode   =   syncMode;
        this.indexID    =   indexID;
        this.deduplicatedVolume =   deduplicatedVolume;
    }
    public void walkFileTree(String strPath, ArrayList<String> excPathList, boolean isRecursive, String thisDocDestPath) {
        try {
            Files.walkFileTree(Paths.get(strPath), new NTFSIndexingWalker(excPathList, execID, syncMode, indexID, deduplicatedVolume));
        }catch(IOException ie) {
            
        }
    }
}
