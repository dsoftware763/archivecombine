package com.virtualcode.agent.das.fileSystems.ntfs;

import com.virtualcode.agent.das.dataArchiver.NTFSWalker;
import com.virtualcode.agent.das.fileSystems.FileServiceInterface;
import com.virtualcode.agent.das.logging.LoggingManager;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author se7
 */
public class NTFSService implements FileServiceInterface {
    
    private String execID   =   null;
    private String syncMode =   null;
    private Logger logger  =    null;
    private String activeActionType =   null;
    private boolean deduplicatedVolume  =   false;
    public NTFSService(String execID, String syncMode, String activeActionType, boolean deduplicatedVolume) {
        this.execID =   execID;
        this.syncMode   =   syncMode;
        this.activeActionType=  activeActionType;
        this.deduplicatedVolume=    deduplicatedVolume;
        logger  =   LoggingManager.getLogger(LoggingManager.ACTIVITY_DETAIL, execID);
    }
    
    public void walkFileTree(String strPath, ArrayList<String> excPathList, boolean isRecursive, String thisDocDestPath) {
        try {
            Files.walkFileTree(Paths.get(strPath), new NTFSWalker(excPathList, execID, syncMode, thisDocDestPath, activeActionType, deduplicatedVolume,strPath));
        }catch(IOException ie) {
            LoggingManager.printStackTrace(ie, logger);
        }
    }
    
    /*
    public String createInternetShortcut(String name, String target) throws Exception {
        
        Path p = FileSystems.getDefault().getPath(name);
        
        
        target = Utility.GetProp("DASUrl") + target;
                
        String icon = Utility.GetIcon(p.getFileName().toString());
        
        name = name.replace(".", "") + ".url";
        
        FileWriter fw = new FileWriter(name);
        fw.write("[InternetShortcut]\n");
        fw.write("URL=" + target + "\n");
        
        if (!icon.isEmpty()) {
            fw.write("IconFile=" + icon + "\n");
            fw.write("IconIndex=0" + "\n");
        }
        
        fw.flush();
        fw.close();
        
        if (Utility.GetProp("DeleteFiles").equals("1")) 
            Files.delete(p);
         
        fw = null;
        icon = name = null;

        return target;
    }
    */
}
