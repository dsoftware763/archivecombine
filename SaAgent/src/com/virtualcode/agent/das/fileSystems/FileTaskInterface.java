/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.virtualcode.agent.das.fileSystems;

import com.virtualcode.agent.das.archivePolicy.dto.Job;
import com.virtualcode.agent.das.dataArchiver.TaskKPI;
import com.vcsl.sa.Acl;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;
import javax.activation.DataHandler;

/**
 *
 * @author Abbas
 */
public interface FileTaskInterface {
    
    public String getEncodedString();
    public void setEncodedString(String encodedString);
    public String getSecureRepoPath();
    public void setSecureRepoPath(String repoPath);
    public UUID getUid();
    public void setUid(UUID uid);
    public TaskKPI getTaskKpi();
    public String getExecID();
    public void setTaskKpi(TaskKPI TaskKpi);
    public Acl getACL(String documentPath) throws IOException;
    public String getDocName();
    public String getSyncMode();
    public String getActiveActionType();
    
    public boolean createInternetShortcut(String shortCutAt, String shortCutTo,Job currentJob)throws Exception;
    public DataHandler getDataHandler() throws IOException;
    public String getPathStr();
    public String getNodeHeirarchy();
    public String getDestPath();
    public String getOwner();
    public String getURIMapping();
    public Date getLastModifiedDate();
    public Date getCreationDate();
    public Date getLastAccessedDate();
    public long getBytesCount() throws Exception;
    public boolean isDirectory() throws Exception;
    public boolean isHidden() throws Exception;
    public boolean isReadOnly() throws Exception;
    public boolean deleteOriginalFile();
    public void closeCurrentTask(boolean syncLastAccessTime);
    public String getCalculatedHashID();
    
    public boolean isOfflineFile() throws Exception;
    public boolean isReparsePoint() throws Exception;
    public boolean isDeduplicatedVolume();

    public String getStrPath();
}
