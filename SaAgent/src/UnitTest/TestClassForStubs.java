/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UnitTest;

import com.virtualcode.agent.das.fileSystems.cifs.CIFSFile;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSTask;
import com.virtualcode.agent.das.fileSystems.ntfs.NTFSTask;
import java.io.File;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import org.apache.log4j.Logger;

/**
 *
 * @author YAwar
 */
public class TestClassForStubs {
    public static void mainx(String args[]) throws Exception {
        
        String linkStr  =   "D:/Sharearchiver/test/test.js";
        String targetStr    =   "D:/Sharearchiver/test/bugs_16012013.pdf";
        
        Path    link        =   FileSystems.getDefault().getPath(linkStr);
        Path    target        =   FileSystems.getDefault().getPath(targetStr);
        Files.createSymbolicLink(link, target);
        
    }
    public static void main_y(String args[]) throws Exception {
        
        String folder   =   "D:/Sharearchiver/test/";
        String shortcutName =   "myShortCut";
        String target   =   "D:/Sharearchiver/test/bugs_16012013.pdf";
        String arguments    =   "";
        String icon =   "";
        String description  =   "Dexcription of file or stub";
        String workingDir  =   folder;
        
        File scriptFile = new File("whatever.js");

        try (PrintWriter script = new PrintWriter(scriptFile)) {
            script.printf("try {\n");
            script.printf("wshshell = WScript.CreateObject(\"WScript.Shell\")\n");
            script.printf("specDir = wshshell.SpecialFolders(\"%s\")\n", folder);
            script.printf("shortcut = wshshell.CreateShortcut(specDir + \"\\\\%s.lnk\")\n", shortcutName);
            script.printf("shortcut.TargetPath = \"%s\"\n", target);
            script.printf("shortcut.Arguments = \"%s\"\n", arguments);
            script.printf("shortcut.WindowStyle = 1\n");
            script.printf("shortcut.HotKey = \"\"\n");
            if (icon.length() > 0)
                script.printf("shortcut.IconLocation = \"%s\"\n", icon);
            script.printf("shortcut.Description = \"%s\"\n", description);
            script.printf("shortcut.WorkingDirectory = \"%s\"\n", workingDir);
            script.printf("shortcut.Save()\n");
            script.printf("} catch (err) {\n");
            // Commented by default
            script.printf("/*WScript.Echo(\"name:\")\nWScript.Echo(err.name)\n");
            script.printf("WScript.Echo(\"message:\")\nWScript.Echo(err.message)\n");
            script.printf("WScript.Echo(\"description:\")\nWScript.Echo(err.description)\n");
            script.printf("WScript.Echo(\"stack:\")\nWScript.Echo(err.stack)\n");
            script.printf("*/\n");
            script.printf("WScript.Quit(1)\n");
            script.printf("}\n");
            script.close();

            // now run cscript.exe with arguments "//nologo" and the full
            // path to 'script', using something like ProcessBuilder and Process
        }
    }
    
    public static void main(String args[]) throws Exception {
        //String url="D:\\TEST149/newtest/error.png|/SecureGet/c9a86d1e6d242921409a2e02a91964a26275470efd745d7974e4c07035108e944b7c679761608f6e1ac56ef40c5696d167d78f526507440f807fcee3a9d92ff241275ddde0b6a9a0b40fb232cc3c9e5fbfbc5d1242ab8714e731d2b2acece146a2ca9891bdf3002b79fad235f4a9218a4f450a59a5f880430d726049f83f0ccc";
       // String[] arr  =   url.split("[|]");
        //System.out.println(arr.length);
        
        URL aURL = new URL("http://example.com:80/docs/books/tutorial/index.html?name=networking#DOWNLOADING");

        System.out.println("protocol = " + aURL.getProtocol()); //http
        System.out.println("authority = " + aURL.getAuthority()); //example.com:80
        System.out.println("host = " + aURL.getHost()); //example.com
        System.out.println("port = " + aURL.getPort()); //80
        System.out.println("path = " + aURL.getPath()); ///docs/books/tutorial/index.html
        System.out.println("query = " + aURL.getQuery()); // name=networking
        System.out.println("filename = " + aURL.getFile()); // /docs/books/tutorial/index.html?name=networking
        System.out.println("ref = " + aURL.getRef());  //  DOWNLOADING
    }
    
    public static void main_x(String args[]) throws Exception {
        String pathStr =   "smb://network.vcsl;se7:s7*@vc7.network.vcsl/adTariz/MergeUtility.zip";
        
        CIFSFile    cFile   =   new CIFSFile(pathStr);
        CIFSTask    cTask   =   new CIFSTask(cFile, "testExecID", "1", Logger.getLogger(TestClassForStubs.class), null,null, false,null);
        
       // cTask.createInternetShortcut(pathStr, "http://www.etaxpk.com");
        
        pathStr =   "D:/Sharearchiver/TestFiles/myLog2.log";
        Path    path        =   FileSystems.getDefault().getPath(pathStr);
        NTFSTask    nTask   =   new NTFSTask(path, Files.readAttributes(path, BasicFileAttributes.class), "testExecID2", "1", pathStr, null, false,null);
       // nTask.createInternetShortcut(pathStr, "TEST");
        
        System.exit(0);
    }
}
