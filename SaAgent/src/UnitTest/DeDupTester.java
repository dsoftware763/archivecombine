package UnitTest;

import com.virtualcode.agent.das.utils.Utility;
import java.io.File;

///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package UnitTest;
//
//import executors.Utility;
//import com.das.ws.DuplicationCheck.DuplicationDetection;
//import com.das.ws.DuplicationCheck.DuplicationDetectionService;
//import java.util.Map;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.xml.ws.BindingProvider;
//
///**
// *
// * @author Saim
// */
public class DeDupTester {
//    
//    private static String detectDuplication(String checkSum, String srcURL, String secureInfo) throws Exception {
//        String res = null;
//        DuplicationDetectionService service = new DuplicationDetectionService();
//        DuplicationDetection port = service.getDMSoapHttpPort();
//        Map ctxt = ((BindingProvider) port).getRequestContext();
//        ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
//                Utility.GetProp("DASUrl") + "/DuplicationDetection?wsdl");
//        res = port.detectDuplication(checkSum, srcURL, secureInfo);
//        service = null;
//        port = null;
//        return res;
//    }
//    
//    
//    public static void main(String[] args) {
//        for(;;){
//            try {
//                String md5 = "ZmE5YWUyNzEtNDc5ZC1hMWVmLWQ0MzUtOTk3MGQzNWIyNzQ0";
//                detectDuplication(md5, "C\testFile.txt", "dXNlcm5hbWU9YWRtaW4mcGFzc3dvcmQ9YWRtaW4=");
//            } catch (Exception ex) {
//                LoggingManager.getLogger(DeDupTester.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }
    public static void main(String[] args) throws Exception {
//        String authString   = "repoName="+Utility.GetProp("sessionRepository")+ "&username=" + Utility.GetProp("AgentLogin") + "&password=" + Utility.GetProp("AgentPassword") + "";
//        System.out.println("Auth String: "+authString);
//            String secureInfo   =   new String(Utility.encodetoBase64(authString.getBytes()));
//            System.out.println("Encoded Secure Info: "+secureInfo);
        
        String documentPath =   "c:/test$/folder/p-path_/YYYY$MM$DD$machine.name|$hello-_ how are you.txt";
        if(documentPath.contains("|$")) {
            String parentDirs   =   documentPath.substring(0, documentPath.lastIndexOf("/"));
            String docName      =   documentPath.substring(documentPath.lastIndexOf("/")+1);
            
            String temp1    =   docName.substring(0, docName.indexOf("|$"));
            docName         =   docName.substring(docName.indexOf("|$")+2);

            temp1    =   temp1.replaceAll("[$]", "/");
            
            documentPath    =   parentDirs + "/" + temp1 + "/" +docName;
            System.out.println(documentPath);
        }
        //createDirectoryStructure("/TestCompany/FS/vc7.network.vcsl/3/0003151020110155");
    }
    
    
    private static String createDirectoryStructure(String repoPath) {
        
        repoPath    =   "E:\\TestFiles" + repoPath.replaceAll("[.]", "_");
        System.err.println("createDirStr " + repoPath);
        File file   =   new File(repoPath);
        //file.mkdir();
        
        if(!file.mkdirs())
            repoPath    =   null;
        
        return repoPath;
    }
}
