/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UnitTest;

import com.virtualcode.agent.das.utils.Utility;
import javax.net.ssl.SSLContext;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest.METHOD;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;

/**
 *
 * @author HP
 */
public class SSLTest {
    public static void main(String args[]) throws Exception {
        
        String urlString = "https://localhost:8089/solr/new_core/";
        
        HttpClient httpclient   =   Utility.prepareSecureHttpClient();
        
        
          SolrInputDocument doc = new SolrInputDocument();    	
        doc.addField("id", "test_id2");//its the PK of document in Solr
        
          SolrClient solr = new HttpSolrClient(urlString, httpclient);
          UpdateResponse sresponse = solr.add(doc);
          
          System.out.println("solr "+sresponse.toString());
          solr.commit();
          
          SolrQuery solrquery = new SolrQuery();
	    solrquery.set("fl", "score");//,"+selectFields); // Get only required fields for VO
	    solrquery.set("start",0);//startLimit	/** yawarz- for manual paging and size computation
    	solrquery.set("rows", 10);//itemsPerPage  /** yawarz- for manual paging and size computation
	    //solrquery.set("sort", sortField+" "+sortMode.toLowerCase());
	    solrquery.set("debugQuery", "off");//on
	    solrquery.setQuery("*:*");

	    System.out.println("Query: " + solrquery);

		QueryResponse response = solr.query(solrquery, METHOD.POST);
		SolrDocumentList docs = response.getResults();

                //document count
		int docCount = docs.size();
		// ** yawarz- for manual paging and size computation  //long totalResults =	(maxResults!=null && maxResults>0)?maxResults:docs.getNumFound();
		long totalResults =	docCount;//(maxResults!=null && maxResults>0)?maxResults:docs.getNumFound();	** yawarz- for manual paging and size computation
		//totalResults	=	(docCount<itemsPerPage && startLimit==0)?docCount:totalResults;
		System.out.println("Doc Count (current page): " + docCount); 
		System.out.println("Total Matching Results: " + totalResults);

    }
}
