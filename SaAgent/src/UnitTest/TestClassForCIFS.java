/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UnitTest;

import com.virtualcode.agent.das.fileSystems.cifs.CIFSDataSource;
import com.virtualcode.agent.das.fileSystems.cifs.CIFSFile;
import com.virtualcode.agent.das.logging.LoggingManager;
import com.virtualcode.agent.das.utils.AuthorizationUtil;
import com.virtualcode.agent.das.utils.Utility;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.StringTokenizer;
import jcifs.smb.ACE;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbFileOutputStream;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import lia.util.net.copy.FDT;
import org.apache.log4j.Logger;

/**
 *
 * @author se7
 */
public class TestClassForCIFS {
    
    
    public static void main(String args[]) throws Exception {
        String fdtHost  =   "192.168.30.50";
        String destPath =   "D:/opt/";
        
        String[] fdtArgs    =   {"-c", fdtHost, "\\\\192.168.30.24/test_new/ShareArchiver.zip", "-d", ""+destPath+""};
        for(int i=0; i<fdtArgs.length; i++) {
            System.out.println("calling FDT main: "+fdtArgs[i]);
        }
        
        FDT.main(fdtArgs);
    }
    
    public static void maincmd(String args[]) throws Exception {
        
        String fdtRequest   =   "java -jar \"C:/Users/HP/Desktop/fdt.jar\" -c 192.168.30.50 \"\\\\192.168.30.24/test_new/ShareArchiver.zip\" -d D:/opt/12121212asdf/";
        System.out.append("task.getUid()" + " using FDT request: "+fdtRequest);
                    
        String cmd[] = { "cmd", "/c", fdtRequest };
        //Utility.runCommandInShell(cmd, null, null);
    }
    
    public static void mainzz(String args[]) throws Exception {
        String dasURL   =   "https://locahost/solr";
        if(dasURL.startsWith("http://"))
            dasURL  =   "http://solr_user:s0lR123@" + dasURL.substring(7);
        else if(dasURL.startsWith("https://"))
            dasURL  =   "https://solr_user:s0lR123@" + dasURL.substring(8);
        
        System.out.println(" "+dasURL);
    }
    public static void mainz(String args[]) throws Exception {
        /*
        String text =   "dXNlcm5hbWU9c2hhcmVhcmNoaXZlciZwYXNzd29yZD1zaGFyZWFyY2hpdmVy";
        System.out.println(new String(Utility.decodetoBase64(text.getBytes())));
        
        text    =   "repoName=DataRepository&username=sharearchiver&password=sharearchiver";
        System.out.println(new String(Utility.encodetoBase64(text.getBytes())));
        
        text    =   new String(Utility.encodetoBase64(text.getBytes()));
        System.out.println(new String(Utility.decodetoBase64(text.getBytes())));
        */
        
        
        String path =   "smb://mazhar:newbuild@10.1.165.15:/mazhar/testfiles/Copy of Roadmap/";
        //path    =   "smb://vcsl95;administrator:cryo@fsln.virtualcode.co.uk/vc_share/Technical/";
        //path    =   "smb://vcsl95.local;administrator:cryo@10.1.165.217:445/vc_share/Technical/";
        //path    =    "smb://vcsl95.local;administrator:cryo@10.1.165.217/share/";
        //path    =   "smb://vcsl95.local;administrator:cryo@10.1.165.217/share/Agent212";
        //path      =     "smb://vcsl95.local;mazhar:password@snapserver.virtualcode.co.uk/";
        //path       =    "smb://vcsl95.local;mazhar:password@snapserver.virtualcode.co.uk/secgrpA/HP P3005 Laserjet/";
        //path        =   "smb://Bukhari:password@yawar-pc/temp/";
        //path        =   "smb://network.vcsl;se6:jwdmuz1@vc6.network.vcsl/Everyone/";
        //path        =   "smb://NETWORK;se9:s9*@vc9/Files/Books/";
        //path    =   Utility.GetProp("path");
        //path    =   "smb://vcsl95.local;administrator:cryo@10.1.165.217:445/FolderC/";
        path    =   "smb://Test182;administrator:cryo@Test182/1/Stub/i.txt";
        String[] cred =   Utility.getCredentialsFromSmb(path);
        for(int i=0; i<cred.length; i++){
            System.out.println(i + "_" + cred[i]+"_");
        }
        
        int SMB_OFFLINE =   0x1000;
        SmbFile cFileObj  =   new SmbFile(path);
        System.out.println("SmbFile.ATTR_OFFLINE: " +  (cFileObj.getAttributes() & SMB_OFFLINE));
        System.out.println("SmbFile.ATTR_HIDDEN: " +    (cFileObj.getAttributes() & SmbFile.ATTR_HIDDEN));
        System.out.println("SmbFile.ATTR_DIRECTORY: " + (cFileObj.getAttributes() & SmbFile.ATTR_DIRECTORY));
        System.out.println("SmbFile.ATTR_SYSTEM: " +    (cFileObj.getAttributes() & SmbFile.ATTR_SYSTEM));
        System.out.println("SmbFile.ATTR_ARCHIVE: " +   (cFileObj.getAttributes() & SmbFile.ATTR_ARCHIVE));
        cFileObj.setAttributes(SMB_OFFLINE | cFileObj.getAttributes());
        
        
        /*
         * 
         * DataSource dataSource  =   null;
        try {
            long tempTime    =   cFileObj.getLastAccess();
            
            System.out.println("SID "+cFileObj.getOwnerUser());
            String owner    =   AuthorizationUtil.resolveSIDValue(Logger.getLogger(TestClassForCIFS.class), cFileObj.getPath(), cFileObj, null);
            System.out.println("reslved "+owner);
            
            InputStream is  =   cFileObj.getInputStream();
            System.out.println("IS  :"+new Date(cFileObj.getLastAccess()));
            
            is.read();
            System.out.println("ISrd:"+new Date(cFileObj.getLastAccess()));
            
            dataSource      =   new CIFSDataSource(is);
            System.out.println("DS:  "+new Date(cFileObj.getLastAccess()));
            
            DataHandler dh  =   new DataHandler(dataSource);
            System.out.println("DH:  "+new Date(cFileObj.getLastAccess()));
            
            is.close();
            System.out.println("IScl:"+new Date(cFileObj.getLastAccess()));
            
            //this line changes the behavior of Stubs and perform write operation on file
            cFileObj.setAccessTime(tempTime);
            
        } catch  (Exception se) {
            throw new IOException("Cant read SmbFileInputStream for " + cFileObj.getName());
        }
         * 
        walkFileTree(path);        
         *
         */
        /*
        //path    =   "C:/Users/YAwar/Desktop/RPs";
        //System.out.println(getModifiedFileName(path, "abc.txt",0));
        //makeDirStructure(path, "/Company/Type/pcnAME/shareName/ahead/2/3/");
         *
         */
    }
    
    private static String getModifiedFileName(String destPath, String docName, int ver) throws MalformedURLException, SmbException {
        String fullName   =   destPath + "/" +docName;
        
        boolean isExist =   false;
        if(fullName.toLowerCase().contains("smb://")) {//if Dest Path is CIFS
            
            SmbFile file    =   new SmbFile(fullName);
            if(file.exists()) {  
                isExist =   true;
            }
            file    =   null;
            
        } else {//case of NTFS
           
            File file    =   new File(fullName);
            if(file.exists()) {  
                isExist =   true;
            }
            file    =   null;
        }
        
        if(isExist) {
            System.out.println("" +" Already exist "+fullName);
            String temp =   null;

            if(docName!=null && 
                    docName.contains(".")) {//case to deal with *.EXT

                temp =   docName.substring(0, docName.lastIndexOf("."))
                         +"_"+ System.currentTimeMillis()+ver + 
                         docName.substring(docName.lastIndexOf("."));

            } else {//if file name dont have extension

                System.out.println("".toString() +" Not have any Extension... ");
                temp =   docName +"_"+ System.currentTimeMillis()+ver;

            }
            //ReRun to see if this file still already exist...
            fullName    =   getModifiedFileName(destPath, temp, ++ver);
            System.out.println("" +" Full name becomes: "+fullName);
        }
        
        return fullName;
    }
    
    private static void makeDirStructure(String destPath, String repoPath) throws Exception {
        //synchronized (this) {
        
        StringTokenizer st = new StringTokenizer(repoPath, "/");
        int i=0;
        repoPath    =   "";
        while(st.hasMoreTokens()) {
            if(i<4) {
                st.nextToken();
            } else {
                repoPath    +=  "/" + st.nextToken();
            }
            i++;
        }
        System.out.println(repoPath);
        /*
            String cur  =   destPath+repoPath;
            System.err.println(cur);
            SmbFile file    =   new SmbFile(cur);
            if(!file.exists())
                file.mkdirs();
            
            cur =   destPath+repoPath+"/myTest";
            System.err.println(cur);
            SmbFile file2    =   new SmbFile(cur);
            if(!file2.exists())
                file2.mkdirs();
            
            cur =   destPath+"file4"+repoPath;
            System.err.println(cur);
            SmbFile file3    =   new SmbFile(cur);
            if(!file3.exists())
                file3.mkdirs();
            
            /*
            StringTokenizer st = new StringTokenizer(repoPath, "/");
            while(st.hasMoreTokens()) {
                System.out.println("mkdirs : " +destPath);

                String curDir   =   st.nextToken();
                destPath        =   destPath + "/" + curDir;
                SmbFile file    =   new SmbFile(destPath);
                if(!file.exists())
                    file.mkdir();
            }
             * 
             */
        //}
    }
    
    private static void walkFileTree(String strPath) {
        
        //System.out.println("begin for"+strPath);
        try {
            
            CIFSFile cFile   =   new CIFSFile(strPath);
            //cFile.login("vcsl95", "administrator", "cryo");

            if(cFile.getFileObj().isDirectory()) {//Perform Recursion, if its a Directory
                System.out.println("Visiting Dir: "+cFile.getFileObj().getName());

                ACE[]   aceList =   cFile.getFileObj().getSecurity(false);
                for (ACE al : aceList) {
                    System.out.println("PermD: " + al.getSID().toDisplayString());
                }

                LinkedList<String> cFileList =   cFile.getStrList();
                for(int i=0; i<cFileList.size(); i++) {
                    walkFileTree(cFileList.get(i));
                }
            } else {//if its file
                //System.out.println("Visiting File: "+cFile.getFileObj().getOwnerUser(true).getAccountName());
                Long lastAccess =   new Long(cFile.getFileObj().getLastAccess());

                System.out.println("Visiting File: "+cFile.getFileObj().getName()+" - "+(new Date(cFile.getFileObj().getLastAccess())));
                //long    time    =   Long.parseLong("1351560261407");
                //cFile.getFileObj().setCreateTime(time);
                //cFile.getFileObj().setLastModified(time);

                ACE[]   aceList =   cFile.getFileObj().getSecurity(false);
                for (ACE al : aceList) {
                    //System.out.println("Perm: " + al.getSID().toDisplayString());
                }

                //System.out.println("... "+cFile.getFileObj().length() + "|");

                //createInternetShortcut(strPath, "/Path of Repo SecureInfo/");

                String fName    =   cFile.getFileObj().getName();
                ACE[] acl   =   cFile.getFileObj().getSecurity(true);

                for(int i=0; i<acl.length; i++){
                    ACE ace =   acl[i];
                    ace.toString();
                }

                readFile(cFile.getFileObj(), lastAccess);
                
                //cFile.getFileObj().setLastModified(lastAccess.longValue());
                cFile.getFileObj().setAccessTime(lastAccess.longValue());
                System.out.println("aTime reset: ");
            }
        } catch (Exception e) {
            System.err.println("Err in: " + strPath);
            e.printStackTrace();
        }
    }
    
    private static void readFile(SmbFile cFileObj, Long lastAccess) throws IOException {
        
        DataSource dataSource  =   null;
        try {
            System.out.println("SID "+cFileObj.getOwnerUser());
            String owner    =   AuthorizationUtil.resolveSIDValue(Logger.getLogger(TestClassForCIFS.class), cFileObj.getPath(), cFileObj, null);
            System.out.println("reslved "+owner);
            
            InputStream is  =   cFileObj.getInputStream();
            System.out.println("IS  :"+new Date(cFileObj.getLastAccess()));
            
            is.read();
            System.out.println("ISrd:"+new Date(cFileObj.getLastAccess()));
            
            dataSource      =   new CIFSDataSource(is);
            System.out.println("DS:  "+new Date(cFileObj.getLastAccess()));
            
            DataHandler dh  =   new DataHandler(dataSource);
            System.out.println("DH:  "+new Date(cFileObj.getLastAccess()));
            
            is.close();
            System.out.println("IScl:"+new Date(cFileObj.getLastAccess()));
            
        } catch  (Exception se) {
            throw new IOException("Cant read SmbFileInputStream for " + cFileObj.getName());
        }
        
        
    }
    
    private static String createInternetShortcut(String shortCutAt, String shortCutTo) throws Exception {
        
        shortCutTo = Utility.getDASUrl() + shortCutTo;
                
        String icon = Utility.GetIcon(shortCutAt);
        
        shortCutAt = shortCutAt + ".url";
        
        System.out.println("creating ShortCut at: " + shortCutAt);
        SmbFile shortCut        =   new SmbFile(shortCutAt);
        SmbFileOutputStream fw  = new SmbFileOutputStream(shortCut);
        //fw.write("Yawar Bukhari".getBytes());
        
        System.out.println("creating ShortCut to: "+shortCutTo);
        fw.write("[InternetShortcut]\n".getBytes());
        fw.write(("URL=" + shortCutTo + "\n").getBytes());
        
        if (!icon.isEmpty()) {
            fw.write(("IconFile=" + icon + "\n").getBytes());
            fw.write(("IconIndex=0" + "\n").getBytes());
        }
        
        fw.flush();
        fw.close();
        
        /////////////////if (Utility.GetProp("DeleteFiles").equals("1")) 
            /////////////////this.getCIFSFile().getFileObj().delete();
        
        fw = null;
        icon = null;
        shortCut = null;

        return shortCutTo;
    }
}