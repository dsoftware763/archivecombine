/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UnitTest;
import java.io.File;
import java.nio.file.Files;
import java.util.Map;
 
public class NTFSAttribsTest {
     
    public static final long FILE_ATTRIBUTE_OFFLINE = 0x00001000;
    public static final long FILE_ATTRIBUTE_READONLY = 0x00000001;
    public static final long FILE_ATTRIBUTE_HIDDEN = 0x00000002;
    public static final long FILE_ATTRIBUTE_SYSTEM = 0x00000004;
    public static final long FILE_ATTRIBUTE_DIRECTORY = 0x00000010;
    public static final long FILE_ATTRIBUTE_ARCHIVE = 0x00000020;
     
    public static void main(String[] args) {
        File file = new File("D:/Sharearchiver/test/whatever.js");
        System.out.println("file: " + file.getAbsolutePath());
        System.out.println("------------------");
        try {
            Map<String, Object> map = Files.readAttributes(file.toPath(), "dos:*");
            for (String key : map.keySet()) {
                System.out.println(key + ": " + map.get(key));
            }
            System.out.println("------------------");
            int attribute = (int) Files.getAttribute(file.toPath(), "dos:attributes");
 
            System.out.println("dos:attributes: " + attribute);
            System.out.println("offline: " + ((attribute & FILE_ATTRIBUTE_OFFLINE) != 0));
            System.out.println("online: " + ((attribute & FILE_ATTRIBUTE_OFFLINE) == 0));
            System.out.println("readonly: " + ((attribute & FILE_ATTRIBUTE_READONLY) != 0));
            System.out.println("hidden: " + ((attribute & FILE_ATTRIBUTE_HIDDEN) != 0));
            System.out.println("system: " + ((attribute & FILE_ATTRIBUTE_SYSTEM) != 0));
            System.out.println("directory: " + ((attribute & FILE_ATTRIBUTE_DIRECTORY) != 0));
            System.out.println("archive: " + ((attribute & FILE_ATTRIBUTE_ARCHIVE) != 0));
 
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}