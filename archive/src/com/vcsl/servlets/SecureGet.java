package com.vcsl.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SecureGet extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public SecureGet() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		processRequest(request, response);
		/*
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("  <HEAD><TITLE>A Servlet</TITLE></HEAD>");
		out.println("  <BODY>");
		out.print("    This is ");
		out.print(this.getClass());
		out.println(", using the GET method http://"+request.getLocalName()+":"+request.getLocalPort()+request.getRequestURI().replace("/archive/SecureGet/", "/ShareArchiver/SecureGet/"));
		out.println("  </BODY>");
		out.println("</HTML>");
		out.flush();
		out.close();
		*/
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
/*
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("  <HEAD><TITLE>A Servlet</TITLE></HEAD>");
		out.println("  <BODY>");
		out.print("    This is ");
		out.print(this.getClass());
		out.println(", using the POST method");
		out.println("  </BODY>");
		out.println("</HTML>");
		out.flush();
		out.close();
		*/
	}

	/**
	  * Handle all HTTP <tt>GET</tt> and <tt>POST</tt> requests.
	  */
	protected void processRequest(
			HttpServletRequest aRequest, HttpServletResponse aResponse
			) throws ServletException, IOException {
		
		//PrintWriter out	=	aResponse.getWriter();
		System.out.println("archive: FIRST METHOD...");
		String curURL	=	"http://"+aRequest.getLocalName()+":"+aRequest.getLocalPort()+aRequest.getRequestURI();
		System.out.println("archive: curURL: "+curURL);
		String newURL	=	curURL.replace("/archive/SecureGet/", "/ShareArchiver/SecureGet/");
		System.out.println("archive: newURL: "+newURL);

		System.out.println("archive: 2ND METHOD...");

		String url			= ((HttpServletRequest)aRequest).getRequestURL().toString();
		String queryString	= ((HttpServletRequest)aRequest).getQueryString();
		
		curURL	=	url + "?" + queryString;
		System.out.println("archive: curURL: "+curURL);
		newURL	=	curURL.replace("/archive/SecureGet/", "/ShareArchiver/SecureGet/");
		System.out.println("archive: newURL: "+newURL);
		/*
		RequestDispatcher dispatcher = aRequest.getRequestDispatcher(newURL);
		dispatcher.forward(aRequest, aResponse); */
		
		aResponse.sendRedirect(newURL);
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
